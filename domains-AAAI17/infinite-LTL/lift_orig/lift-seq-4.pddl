
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lift for a 4-floor building
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Goal formula:
;;
;; AE((called) -> (served))
;;
;; (Same as in Piterman-etal@VMCAI06)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [] <> (called -> served )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain lift)
	(:requirements :strips :typing :equality)
	(:types 
		floor
	)
	(:constants
		f1 f2 f3 f4 - floor
	)
	(:predicates
		(at ?f  - floor)
		(req ?f  - floor)
		(turn ?f  - floor)
		(check)
		(called)
		(move)
		(served)
	)

	(:action push_f1
		:precondition
			(and 
				(turn f1)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
				(oneof
					(and)
					(req f1)
				)
			)
	)
	(:action push_f2
		:precondition
			(and 
				(turn f2)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
				(oneof
					(and)
					(req f2)
				)
			)
	)
	(:action push_f3
		:precondition
			(and 
				(turn f3)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f3))
				(turn f4)
				(oneof
					(and)
					(req f3)
				)
			)
	)
	(:action push_f4
		:precondition
			(and 
				(turn f4)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f4))
				(check)
				(not (push))
				(turn f1)
				(oneof
					(and)
					(req f4)
				)
			)
	)
	(:action check_called_true_f1
		:precondition
			(and 
				(move)
				(check)
				(turn f1)
				(req f1)
			)
		:effect
			(and
				(called)
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_false_f1
		:precondition
			(and 
				(move)
				(not (req f1))
				(check)
				(turn f1)
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_true_f2
		:precondition
			(and 
				(move)
				(check)
				(turn f2)
				(req f2)
			)
		:effect
			(and
				(called)
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_false_f2
		:precondition
			(and 
				(move)
				(not (req f2))
				(check)
				(turn f2)
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_true_f3
		:precondition
			(and 
				(move)
				(check)
				(turn f3)
				(req f3)
			)
		:effect
			(and
				(called)
				(not (turn f3))
				(turn f4)
			)
	)
	(:action check_called_false_f3
		:precondition
			(and 
				(move)
				(not (req f3))
				(check)
				(turn f3)
			)
		:effect
			(and
				(not (turn f3))
				(turn f4)
			)
	)
	(:action check_called_true_f4
		:precondition
			(and 
				(move)
				(check)
				(turn f4)
				(req f4)
			)
		:effect
			(and
				(called)
				(not (turn f4))
				(not (check))
			)
	)
	(:action check_called_false_f4
		:precondition
			(and 
				(move)
				(not (req f4))
				(check)
				(turn f4)
			)
		:effect
			(and
				(not (turn f4))
				(not (check))
			)
	)
	(:action move_up_from_f1
		:precondition
			(and
				(at f1)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(turn f1)
			)
	)
	(:action move_up_and_serve_from_f1
		:precondition
			(and
				(at f1)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(served)
				(not (req f2))
				(turn f1)
			)
	)
	(:action move_up_from_f2
		:precondition
			(and
				(at f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(turn f1)
			)
	)
	(:action move_up_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(served)
				(not (req f3))
				(turn f1)
			)
	)
	(:action move_up_from_f3
		:precondition
			(and
				(at f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f4)
				(turn f1)
			)
	)
	(:action move_up_and_serve_from_f3
		:precondition
			(and
				(at f3)
				(req f4)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f4)
				(served)
				(not (req f4))
				(turn f1)
			)
	)
	(:action move_down_from_f4
		:precondition
			(and
				(at f4)
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f3)
				(turn f1)
			)
	)
	(:action move_down_and_serve_from_f4
		:precondition
			(and
				(at f4)
				(req f3)
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f3)
				(served)
				(not (req f3))
				(turn f1)
			)
	)
	(:action move_down_from_f3
		:precondition
			(and
				(at f3)
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(turn f1)
			)
	)
	(:action move_down_and_serve_from_f3
		:precondition
			(and
				(at f3)
				(req f2)
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(served)
				(not (req f2))
				(turn f1)
			)
	)
	(:action move_down_from_f2
		:precondition
			(and
				(at f2)
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(turn f1)
			)
	)
	(:action move_down_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f1)
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(served)
				(not (req f1))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f1
		:precondition
			(and
				(at f1)
				(req f1)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f1))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f2
		:precondition
			(and
				(at f2)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f2))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f3
		:precondition
			(and
				(at f3)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f3))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f4
		:precondition
			(and
				(at f4)
				(req f4)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f4))
				(turn f1)
			)
	)
	(:action no_op
		:precondition
			(and (move))
		:effect (and)
	)
	(:action mark_served
		:precondition
			(and (not (move)))
		:effect
			(and (not (served)) (move))
	)
)

