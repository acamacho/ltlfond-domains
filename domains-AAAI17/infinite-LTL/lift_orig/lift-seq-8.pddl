
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lift for a 8-floor building
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Goal formula:
;;
;; AE((called) -> (served))
;;
;; (Same as in Piterman-etal@VMCAI06)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [] <> (called -> served )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain lift)
	(:requirements :strips :typing :equality)
	(:types 
		floor
	)
	(:constants
		f1 f2 f3 f4 f5 f6 f7 f8 - floor
	)
	(:predicates
		(at ?f  - floor)
		(req ?f  - floor)
		(turn ?f  - floor)
		(check)
		(called)
		(move)
		(served)
	)

	(:action push_f1
		:precondition
			(and 
				(turn f1)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
				(oneof
					(and)
					(req f1)
				)
			)
	)
	(:action push_f2
		:precondition
			(and 
				(turn f2)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
				(oneof
					(and)
					(req f2)
				)
			)
	)
	(:action push_f3
		:precondition
			(and 
				(turn f3)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f3))
				(turn f4)
				(oneof
					(and)
					(req f3)
				)
			)
	)
	(:action push_f4
		:precondition
			(and 
				(turn f4)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f4))
				(turn f5)
				(oneof
					(and)
					(req f4)
				)
			)
	)
	(:action push_f5
		:precondition
			(and 
				(turn f5)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f5))
				(turn f6)
				(oneof
					(and)
					(req f5)
				)
			)
	)
	(:action push_f6
		:precondition
			(and 
				(turn f6)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f6))
				(turn f7)
				(oneof
					(and)
					(req f6)
				)
			)
	)
	(:action push_f7
		:precondition
			(and 
				(turn f7)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f7))
				(turn f8)
				(oneof
					(and)
					(req f7)
				)
			)
	)
	(:action push_f8
		:precondition
			(and 
				(turn f8)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f8))
				(check)
				(not (push))
				(turn f1)
				(oneof
					(and)
					(req f8)
				)
			)
	)
	(:action check_called_true_f1
		:precondition
			(and 
				(move)
				(check)
				(turn f1)
				(req f1)
			)
		:effect
			(and
				(called)
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_false_f1
		:precondition
			(and 
				(move)
				(not (req f1))
				(check)
				(turn f1)
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_true_f2
		:precondition
			(and 
				(move)
				(check)
				(turn f2)
				(req f2)
			)
		:effect
			(and
				(called)
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_false_f2
		:precondition
			(and 
				(move)
				(not (req f2))
				(check)
				(turn f2)
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_true_f3
		:precondition
			(and 
				(move)
				(check)
				(turn f3)
				(req f3)
			)
		:effect
			(and
				(called)
				(not (turn f3))
				(turn f4)
			)
	)
	(:action check_called_false_f3
		:precondition
			(and 
				(move)
				(not (req f3))
				(check)
				(turn f3)
			)
		:effect
			(and
				(not (turn f3))
				(turn f4)
			)
	)
	(:action check_called_true_f4
		:precondition
			(and 
				(move)
				(check)
				(turn f4)
				(req f4)
			)
		:effect
			(and
				(called)
				(not (turn f4))
				(turn f5)
			)
	)
	(:action check_called_false_f4
		:precondition
			(and 
				(move)
				(not (req f4))
				(check)
				(turn f4)
			)
		:effect
			(and
				(not (turn f4))
				(turn f5)
			)
	)
	(:action check_called_true_f5
		:precondition
			(and 
				(move)
				(check)
				(turn f5)
				(req f5)
			)
		:effect
			(and
				(called)
				(not (turn f5))
				(turn f6)
			)
	)
	(:action check_called_false_f5
		:precondition
			(and 
				(move)
				(not (req f5))
				(check)
				(turn f5)
			)
		:effect
			(and
				(not (turn f5))
				(turn f6)
			)
	)
	(:action check_called_true_f6
		:precondition
			(and 
				(move)
				(check)
				(turn f6)
				(req f6)
			)
		:effect
			(and
				(called)
				(not (turn f6))
				(turn f7)
			)
	)
	(:action check_called_false_f6
		:precondition
			(and 
				(move)
				(not (req f6))
				(check)
				(turn f6)
			)
		:effect
			(and
				(not (turn f6))
				(turn f7)
			)
	)
	(:action check_called_true_f7
		:precondition
			(and 
				(move)
				(check)
				(turn f7)
				(req f7)
			)
		:effect
			(and
				(called)
				(not (turn f7))
				(turn f8)
			)
	)
	(:action check_called_false_f7
		:precondition
			(and 
				(move)
				(not (req f7))
				(check)
				(turn f7)
			)
		:effect
			(and
				(not (turn f7))
				(turn f8)
			)
	)
	(:action check_called_true_f8
		:precondition
			(and 
				(move)
				(check)
				(turn f8)
				(req f8)
			)
		:effect
			(and
				(called)
				(not (turn f8))
				(not (check))
			)
	)
	(:action check_called_false_f8
		:precondition
			(and 
				(move)
				(not (req f8))
				(check)
				(turn f8)
			)
		:effect
			(and
				(not (turn f8))
				(not (check))
			)
	)
	(:action move_up_from_f1
		:precondition
			(and
				(at f1)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(turn f1)
			)
	)
	(:action move_up_and_serve_from_f1
		:precondition
			(and
				(at f1)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(served)
				(not (req f2))
				(turn f1)
			)
	)
	(:action move_up_from_f2
		:precondition
			(and
				(at f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(turn f1)
			)
	)
	(:action move_up_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(served)
				(not (req f3))
				(turn f1)
			)
	)
	(:action move_up_from_f3
		:precondition
			(and
				(at f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f4)
				(turn f1)
			)
	)
	(:action move_up_and_serve_from_f3
		:precondition
			(and
				(at f3)
				(req f4)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f4)
				(served)
				(not (req f4))
				(turn f1)
			)
	)
	(:action move_up_from_f4
		:precondition
			(and
				(at f4)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f5)
				(turn f1)
			)
	)
	(:action move_up_and_serve_from_f4
		:precondition
			(and
				(at f4)
				(req f5)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f5)
				(served)
				(not (req f5))
				(turn f1)
			)
	)
	(:action move_up_from_f5
		:precondition
			(and
				(at f5)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f5))
				(at f6)
				(turn f1)
			)
	)
	(:action move_up_and_serve_from_f5
		:precondition
			(and
				(at f5)
				(req f6)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f5))
				(at f6)
				(served)
				(not (req f6))
				(turn f1)
			)
	)
	(:action move_up_from_f6
		:precondition
			(and
				(at f6)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f6))
				(at f7)
				(turn f1)
			)
	)
	(:action move_up_and_serve_from_f6
		:precondition
			(and
				(at f6)
				(req f7)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f6))
				(at f7)
				(served)
				(not (req f7))
				(turn f1)
			)
	)
	(:action move_up_from_f7
		:precondition
			(and
				(at f7)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f7))
				(at f8)
				(turn f1)
			)
	)
	(:action move_up_and_serve_from_f7
		:precondition
			(and
				(at f7)
				(req f8)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f7))
				(at f8)
				(served)
				(not (req f8))
				(turn f1)
			)
	)
	(:action move_down_from_f8
		:precondition
			(and
				(at f8)
				(move)
			)
		:effect
			(and
				(not (at f8))
				(at f7)
				(turn f1)
			)
	)
	(:action move_down_and_serve_from_f8
		:precondition
			(and
				(at f8)
				(req f7)
				(move)
			)
		:effect
			(and
				(not (at f8))
				(at f7)
				(served)
				(not (req f7))
				(turn f1)
			)
	)
	(:action move_down_from_f7
		:precondition
			(and
				(at f7)
				(move)
			)
		:effect
			(and
				(not (at f7))
				(at f6)
				(turn f1)
			)
	)
	(:action move_down_and_serve_from_f7
		:precondition
			(and
				(at f7)
				(req f6)
				(move)
			)
		:effect
			(and
				(not (at f7))
				(at f6)
				(served)
				(not (req f6))
				(turn f1)
			)
	)
	(:action move_down_from_f6
		:precondition
			(and
				(at f6)
				(move)
			)
		:effect
			(and
				(not (at f6))
				(at f5)
				(turn f1)
			)
	)
	(:action move_down_and_serve_from_f6
		:precondition
			(and
				(at f6)
				(req f5)
				(move)
			)
		:effect
			(and
				(not (at f6))
				(at f5)
				(served)
				(not (req f5))
				(turn f1)
			)
	)
	(:action move_down_from_f5
		:precondition
			(and
				(at f5)
				(move)
			)
		:effect
			(and
				(not (at f5))
				(at f4)
				(turn f1)
			)
	)
	(:action move_down_and_serve_from_f5
		:precondition
			(and
				(at f5)
				(req f4)
				(move)
			)
		:effect
			(and
				(not (at f5))
				(at f4)
				(served)
				(not (req f4))
				(turn f1)
			)
	)
	(:action move_down_from_f4
		:precondition
			(and
				(at f4)
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f3)
				(turn f1)
			)
	)
	(:action move_down_and_serve_from_f4
		:precondition
			(and
				(at f4)
				(req f3)
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f3)
				(served)
				(not (req f3))
				(turn f1)
			)
	)
	(:action move_down_from_f3
		:precondition
			(and
				(at f3)
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(turn f1)
			)
	)
	(:action move_down_and_serve_from_f3
		:precondition
			(and
				(at f3)
				(req f2)
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(served)
				(not (req f2))
				(turn f1)
			)
	)
	(:action move_down_from_f2
		:precondition
			(and
				(at f2)
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(turn f1)
			)
	)
	(:action move_down_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f1)
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(served)
				(not (req f1))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f1
		:precondition
			(and
				(at f1)
				(req f1)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f1))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f2
		:precondition
			(and
				(at f2)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f2))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f3
		:precondition
			(and
				(at f3)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f3))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f4
		:precondition
			(and
				(at f4)
				(req f4)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f4))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f5
		:precondition
			(and
				(at f5)
				(req f5)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f5))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f6
		:precondition
			(and
				(at f6)
				(req f6)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f6))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f7
		:precondition
			(and
				(at f7)
				(req f7)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f7))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f8
		:precondition
			(and
				(at f8)
				(req f8)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f8))
				(turn f1)
			)
	)
	(:action no_op
		:precondition
			(and (move))
		:effect (and)
	)
	(:action mark_served
		:precondition
			(and (not (move)))
		:effect
			(and (not (served)) (move))
	)
)

