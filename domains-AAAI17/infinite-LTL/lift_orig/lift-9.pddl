(define (problem lift)
	(:domain lift)
	(:objects )
	(:INIT
		(at f1) (move) 
	)
	(:goal (and (always (eventually (or (not (req f1) ) (at f1) ) ) ) (always (eventually (or (not (req f2) ) (at f2) ) ) ) (always (eventually (or (not (req f3) ) (at f3) ) ) ) (always (eventually (or (not (req f4) ) (at f4) ) ) ) (always (eventually (or (not (req f5) ) (at f5) ) ) ) (always (eventually (or (not (req f6) ) (at f6) ) ) ) (always (eventually (or (not (req f7) ) (at f7) ) ) ) (always (eventually (or (not (req f8) ) (at f8) ) ) ) (always (eventually (or (not (req f9) ) (at f9) ) ) ) (always (eventually (or (at f1) (called) ) ) )))
)
