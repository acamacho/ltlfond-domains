(define (problem lift)
	(:domain lift)
	(:objects )
	(:INIT
		(at f1) (move) 
	)
	(:goal (and (always (eventually (or (not (req f1) ) (at f1) ) ) ) (always (eventually (or (not (req f2) ) (at f2) ) ) ) (always (eventually (or (not (req f3) ) (at f3) ) ) ) (always (eventually (or (not (req f4) ) (at f4) ) ) ) (always (eventually (or (at f1) (called) ) ) )))
)
