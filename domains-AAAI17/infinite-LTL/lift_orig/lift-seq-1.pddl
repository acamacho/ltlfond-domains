
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lift for a 1-floor building
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Goal formula:
;;
;; AE((called) -> (served))
;;
;; (Same as in Piterman-etal@VMCAI06)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [] <> (called -> served )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain lift)
	(:requirements :strips :typing :equality)
	(:types 
		floor
	)
	(:constants
		f1 - floor
	)
	(:predicates
		(at ?f  - floor)
		(req ?f  - floor)
		(turn ?f  - floor)
		(check)
		(called)
		(move)
		(served)
	)

	(:action push_f1
		:precondition
			(and 
				(turn f1)
				(push)
				(move)
			)
		:effect
			(and
				(not (turn f1))
				(check)
				(not (push))
				(turn f1)
				(oneof
					(and)
					(req f1)
				)
			)
	)
	(:action check_called_true_f1
		:precondition
			(and 
				(move)
				(check)
				(turn f1)
				(req f1)
			)
		:effect
			(and
				(called)
				(not (turn f1))
				(not (check))
			)
	)
	(:action check_called_false_f1
		:precondition
			(and 
				(move)
				(not (req f1))
				(check)
				(turn f1)
			)
		:effect
			(and
				(not (turn f1))
				(not (check))
			)
	)
	(:action stay_and_serve_at_f1
		:precondition
			(and
				(at f1)
				(req f1)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(served)
				(not (req f1))
				(turn f1)
			)
	)
	(:action no_op
		:precondition
			(and (move))
		:effect (and)
	)
	(:action mark_served
		:precondition
			(and (not (move)))
		:effect
			(and (not (served)) (move))
	)
)

