(define (problem 'packages-2-packages-seq-2_dp1')

  (:domain packages)
  (:init 
    (in_store p1)
    (pkg_at l1)
    (in_store p2)
    (pkg_at l2)
    (hello)
    (robot_at desk)
    (q_3)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)