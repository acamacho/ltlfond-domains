
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Store example for 3 packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Goal formula:
;;
;; A( (pkg_requested) OR (pkg_arrived) --> E (pkg_served) OR E (pkg_stored ) )
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [](  active_request -> ( <> pkg_stored | <> pkg_served) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain packages)
	(:requirements :strips :typing :equality)
	(:types 
		pkg
		loc
	)
	(:constants
		p1 p2 p3 - pkg
		l1 l2 l3 desk - loc
	)
	(:predicates
		(in_store ?p  - pkg)
		(want ?p  - pkg)
		(active_request)
		(pkg_served)
		(pkg_requested)
		(pkg_arrived)
		(pkg_stored)
		(holding ?p - pkg)
		(pkg_at ?l - loc)
		(robot_at ?l - loc)
		(f_copy)
		(f_sync)
		(f_world)
		(f_ok)
		(q_1)
		(q_1s)
		(q_2)
		(q_2s)
		(q_3)
		(q_3s)
		(q_4)
		(q_4s)
		(q_5)
		(q_5s)
		(q_6)
		(q_6s)
		(q_7)
		(q_7s)
		(q_1token)
		(q_2token)
		(q_3token)
		(q_4token)
		(q_5token)
		(q_6token)
		(q_7token)
	)

	(:action serve-p1
		:precondition
			(and 
				(not (pkg_served))
				(f_ok)
				(f_world)
				(in_store p1)
				(want p1)
				(holding p1)
				(robot_at desk)
			)
		:effect
			(and
				(not (f_world))
				(f_copy)
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p1))
				(not (want p1))
				(not (holding p1))
			)
	)


	(:action pickup-p1
		:precondition
			(and 
				(not (pkg_served))
				(f_ok)
				(f_world)
				(in_store p1)
				(want p1)
				(robot_at l1)
			)
		:effect
			(and
				(not (pkg_at l1))
				(holding p1)
			)
	)


	(:action putdown-p1
		:precondition
			(and 
				(pkg_arrived)
				(f_ok)
				(f_world)
				(holding p1)
				(robot_at l1)
			)
		:effect
			(and
				(in_store p1)
				(pkg_at l1)
				(not (holding p1))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
				(not (f_world))
				(f_copy)
			)
	)


	(:action restock-p1
		:precondition
			(and 
				(not (pkg_served))
				(f_ok)
				(f_world)
				(not (in_store p1))
				(want p1)
			)
		:effect
			(and
				(in_store p1)
			)
	)


	(:action serve-p2
		:precondition
			(and 
				(not (pkg_served))
				(f_ok)
				(f_world)
				(in_store p2)
				(want p2)
				(holding p2)
				(robot_at desk)
			)
		:effect
			(and
				(not (f_world))
				(f_copy)
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p2))
				(not (want p2))
				(not (holding p2))
			)
	)


	(:action pickup-p2
		:precondition
			(and 
				(not (pkg_served))
				(f_ok)
				(f_world)
				(in_store p2)
				(want p2)
				(robot_at l2)
			)
		:effect
			(and
				(not (pkg_at l2))
				(holding p2)
			)
	)


	(:action putdown-p2
		:precondition
			(and 
				(pkg_arrived)
				(f_ok)
				(f_world)
				(holding p2)
				(robot_at l2)
			)
		:effect
			(and
				(in_store p2)
				(pkg_at l2)
				(not (holding p2))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
				(not (f_world))
				(f_copy)
			)
	)


	(:action restock-p2
		:precondition
			(and 
				(not (pkg_served))
				(f_ok)
				(f_world)
				(not (in_store p2))
				(want p2)
			)
		:effect
			(and
				(in_store p2)
			)
	)


	(:action serve-p3
		:precondition
			(and 
				(not (pkg_served))
				(f_ok)
				(f_world)
				(in_store p3)
				(want p3)
				(holding p3)
				(robot_at desk)
			)
		:effect
			(and
				(not (f_world))
				(f_copy)
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p3))
				(not (want p3))
				(not (holding p3))
			)
	)


	(:action pickup-p3
		:precondition
			(and 
				(not (pkg_served))
				(f_ok)
				(f_world)
				(in_store p3)
				(want p3)
				(robot_at l3)
			)
		:effect
			(and
				(not (pkg_at l3))
				(holding p3)
			)
	)


	(:action putdown-p3
		:precondition
			(and 
				(pkg_arrived)
				(f_ok)
				(f_world)
				(holding p3)
				(robot_at l3)
			)
		:effect
			(and
				(in_store p3)
				(pkg_at l3)
				(not (holding p3))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
				(not (f_world))
				(f_copy)
			)
	)


	(:action restock-p3
		:precondition
			(and 
				(not (pkg_served))
				(f_ok)
				(f_world)
				(not (in_store p3))
				(want p3)
			)
		:effect
			(and
				(in_store p3)
			)
	)


	(:action move1_2
		:precondition
			(and 
				(robot_at l1)
				(f_ok)
				(f_world)
			)
		:effect
			(and
				(robot_at l2)
				(not (robot_at l1))
			)
	)


	(:action move2_1
		:precondition
			(and 
				(robot_at l2)
				(f_ok)
				(f_world)
			)
		:effect
			(and
				(robot_at l1)
				(not (robot_at l2))
			)
	)


	(:action move2_3
		:precondition
			(and 
				(robot_at l2)
				(f_ok)
				(f_world)
			)
		:effect
			(and
				(robot_at l3)
				(not (robot_at l2))
			)
	)


	(:action move3_2
		:precondition
			(and 
				(robot_at l3)
				(f_ok)
				(f_world)
			)
		:effect
			(and
				(robot_at l2)
				(not (robot_at l3))
			)
	)


	(:action movedesk_l1
		:precondition
			(and 
				(robot_at desk)
				(f_ok)
				(f_world)
			)
		:effect
			(and
				(robot_at l1)
				(not (robot_at desk))
			)
	)


	(:action movel1_desk
		:precondition
			(and 
				(robot_at l1)
				(f_ok)
				(f_world)
			)
		:effect
			(and
				(robot_at desk)
				(not (robot_at l1))
			)
	)


	(:action request
		:precondition
			(and 
				(robot_at desk)
				(not (active_request))
				(f_ok)
				(f_world)
			)
		:effect
			(and
				(not (f_world))
				(f_copy)
				(not (pkg_served))
				(not (pkg_stored))
				(active_request)
				(oneof
					(and
						(pkg_requested)
						(want p1)
					)
					(and
						(pkg_requested)
						(want p2)
					)
					(and
						(pkg_requested)
						(want p3)
					)
					(and
						(pkg_arrived)
						(holding p1)
					)
					(and
						(pkg_arrived)
						(holding p2)
					)
					(and
						(pkg_arrived)
						(holding p3)
					)
				)
			)
	)

  (:action o_copy
	:parameters ()
	:precondition 
	  (and
		(f_ok)
		(f_copy))
	:effect
	  (and
		(f_sync)
		(when
		  (q_1)
		  (q_1s))
		(when
		  (q_2)
		  (q_2s))
		(when
		  (q_3)
		  (q_3s))
		(when
		  (q_4)
		  (q_4s))
		(when
		  (q_5)
		  (q_5s))
		(when
		  (q_6)
		  (q_6s))
		(when
		  (q_7)
		  (q_7s))
		(not 
		  (f_copy))
		(not 
		  (q_1))
		(not 
		  (q_2))
		(not 
		  (q_3))
		(not 
		  (q_4))
		(not 
		  (q_5))
		(not 
		  (q_6))
		(not 
		  (q_7))
	  )
	)
  (:action o_world
	:parameters ()
	:precondition 
	  (and
		(f_sync)
		(f_ok)
		(not 
		  (q_1s))
		(not 
		  (q_2s))
		(not 
		  (q_3s))
		(not 
		  (q_4s))
		(not 
		  (q_5s))
		(not 
		  (q_6s))
		(not 
		  (q_7s)))
	:effect
	  (and
		(f_world)
		(not 
		  (f_sync))
	  )
	)
  (:action o_sync_q_1s
	:parameters ()
	:precondition 
	  (and
		(pkg_served)
		(f_sync)
		(f_ok)
		(q_1s))
	:effect
	  (and
		(not 
		  (q_1s))
		(when
		  (q_1token)
		  (not 
			(q_1token)))
	  )
	)
  (:action o_sync_q_2s
	:parameters ()
	:precondition 
	  (and
		(pkg_stored)
		(f_sync)
		(f_ok)
		(q_2s))
	:effect
	  (and
		(not 
		  (q_2s))
		(when
		  (q_2token)
		  (not 
			(q_2token)))
	  )
	)
  (:action o_sync_q_3s
	:parameters ()
	:precondition 
	  (and
		(f_sync)
		(f_ok)
		(q_3s))
	:effect
	  (and
		(q_7s)
		(q_3)
		(not 
		  (q_3s))
		(not 
		  (q_3token))
	  )
	)
  (:action o_sync_q_4s_1
	:parameters ()
	:precondition 
	  (and
		(f_sync)
		(f_ok)
		(q_4s))
	:effect
	  (and
		(q_6s)
		(when
		  (q_4token)
		  (q_6token))
		(not 
		  (q_4s))
		(when
		  (q_4token)
		  (not 
			(q_4token)))
	  )
	)
  (:action o_sync_q_4s_2
	:parameters ()
	:precondition 
	  (and
		(f_sync)
		(f_ok)
		(q_4s))
	:effect
	  (and
		(q_4)
		(not 
		  (q_4s))
	  )
	)
  (:action o_sync_q_5s
	:parameters ()
	:precondition 
	  (and
		(not 
		  (active_request))
		(f_sync)
		(f_ok)
		(q_5s))
	:effect
	  (and
		(not 
		  (q_5s))
		(when
		  (q_5token)
		  (not 
			(q_5token)))
	  )
	)
  (:action o_sync_q_6s_1
	:parameters ()
	:precondition 
	  (and
		(f_sync)
		(f_ok)
		(q_6s))
	:effect
	  (and
		(q_1s)
		(when
		  (q_6token)
		  (q_1token))
		(not 
		  (q_6s))
		(when
		  (q_6token)
		  (not 
			(q_6token)))
	  )
	)
  (:action o_sync_q_6s_2
	:parameters ()
	:precondition 
	  (and
		(f_sync)
		(f_ok)
		(q_6s))
	:effect
	  (and
		(q_2s)
		(when
		  (q_6token)
		  (q_2token))
		(not 
		  (q_6s))
		(when
		  (q_6token)
		  (not 
			(q_6token)))
	  )
	)
  (:action o_sync_q_7s_1
	:parameters ()
	:precondition 
	  (and
		(f_sync)
		(f_ok)
		(q_7s))
	:effect
	  (and
		(q_4s)
		(when
		  (q_7token)
		  (q_4token))
		(not 
		  (q_7s))
		(when
		  (q_7token)
		  (not 
			(q_7token)))
	  )
	)
  (:action o_sync_q_7s_2
	:parameters ()
	:precondition 
	  (and
		(f_sync)
		(f_ok)
		(q_7s))
	:effect
	  (and
		(q_5s)
		(when
		  (q_7token)
		  (q_5token))
		(not 
		  (q_7s))
		(when
		  (q_7token)
		  (not 
			(q_7token)))
	  )
	)
  (:action jaces_DETDUP_1
	:parameters ()
	:precondition 
	  (and
		(f_sync)
		(f_ok)
		(not 
		  (q_7token))
		(not 
		  (q_6token))
		(not 
		  (q_5token))
		(not 
		  (q_4token))
		(not 
		  (q_2token))
		(not 
		  (q_1token))
		(not 
		  (q_1s))
		(not 
		  (q_2s))
		(not 
		  (q_3s))
		(not 
		  (q_4s))
		(not 
		  (q_5s))
		(not 
		  (q_6s))
		(not 
		  (q_7s)))
	:effect
	  (and
		(f_goal)
		(f_world)
		(not 
		  (f_sync))
	  )
	)
  (:action jaces_DETDUP_2
	:parameters ()
	:precondition 
	  (and
		(f_sync)
		(f_ok)
		(not 
		  (q_7token))
		(not 
		  (q_6token))
		(not 
		  (q_5token))
		(not 
		  (q_4token))
		(not 
		  (q_2token))
		(not 
		  (q_1token))
		(not 
		  (q_1s))
		(not 
		  (q_2s))
		(not 
		  (q_3s))
		(not 
		  (q_4s))
		(not 
		  (q_5s))
		(not 
		  (q_6s))
		(not 
		  (q_7s)))
	:effect
	  (and
		(when
		  (q_7)
		  (q_7token))
		(when
		  (q_6)
		  (q_6token))
		(when
		  (q_5)
		  (q_5token))
		(when
		  (q_4)
		  (q_4token))
		(when
		  (q_2)
		  (q_2token))
		(when
		  (q_1)
		  (q_1token))
		(f_world)
		(not 
		  (f_sync))
	  )
	)

)

