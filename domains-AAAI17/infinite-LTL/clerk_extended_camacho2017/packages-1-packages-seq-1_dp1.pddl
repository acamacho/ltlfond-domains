(define (problem 'packages-1-packages-seq-1_dp1')

  (:domain packages)
  (:init 
    (in_store p1)
    (pkg_at l1)
    (hello)
    (robot_at desk)
    (q_3)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)