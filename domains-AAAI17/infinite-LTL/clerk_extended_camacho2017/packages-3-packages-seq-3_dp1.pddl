(define (problem 'packages-3-packages-seq-3_dp1')

  (:domain packages)
  (:init 
    (in_store p1)
    (pkg_at l1)
    (in_store p2)
    (pkg_at l2)
    (in_store p3)
    (pkg_at l3)
    (hello)
    (robot_at desk)
    (q_3)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)