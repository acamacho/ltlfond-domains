(define (problem packages)
	(:domain packages)
	(:objects )
	(:INIT
		(in_store p1)
		(pkg_at l1)
		(hello)
		(robot_at desk)
	)
	(:goal 
		(always 
			(or 
				(not 
					(active_request) 
				) 
				(eventually 
					(or 
						(pkg_served) 
						(pkg_stored) 
					) 
				) 
			) 
		) 
	)
)
