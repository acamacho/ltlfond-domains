
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Store example for 1 packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Goal formula:
;;
;; A( (pkg_requested) OR (pkg_arrived) --> E (pkg_served) OR E (pkg_stored ) )
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [](  active_request -> ( <> pkg_stored | <> pkg_served) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain packages)
	(:requirements :strips :typing :equality)
	(:types 
		pkg
		loc
	)
	(:constants
		p1 - pkg
		l1 desk - loc
	)
	(:predicates
		(in_store ?p  - pkg)
		(want ?p  - pkg)
		(active_request)
		(pkg_served)
		(pkg_requested)
		(pkg_arrived)
		(pkg_stored)
		(holding ?p - pkg)
		(pkg_at ?l - loc)
		(robot_at ?l - loc)
	)

	(:action serve-p1
		:precondition
			(and 
				(not (pkg_served))
				(in_store p1)
				(want p1)
				(holding p1)
				(robot_at desk)
			)
		:effect
			(and
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p1))
				(not (want p1))
				(not (holding p1))
			)
	)


	(:action pickup-p1
		:precondition
			(and 
				(not (pkg_served))
				(in_store p1)
				(want p1)
				(robot_at l1)
			)
		:effect
			(and
				(not (pkg_at l1))
				(holding p1)
			)
	)


	(:action putdown-p1
		:precondition
			(and 
				(pkg_arrived)
				(holding p1)
				(robot_at l1)
			)
		:effect
			(and
				(in_store p1)
				(pkg_at l1)
				(not (holding p1))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
			)
	)


	(:action restock-p1
		:precondition
			(and 
				(not (pkg_served))
				(not (in_store p1))
				(want p1)
			)
		:effect
			(and
				(in_store p1)
			)
	)


	(:action movedesk_l1
		:precondition
			(and 
				(robot_at desk)
			)
		:effect
			(and
				(robot_at l1)
				(not (robot_at desk))
			)
	)


	(:action movel1_desk
		:precondition
			(and 
				(robot_at l1)
			)
		:effect
			(and
				(robot_at desk)
				(not (robot_at l1))
			)
	)


	(:action request
		:precondition
			(and 
				(robot_at desk)
				(not (active_request))
			)
		:effect
			(and
				(not (pkg_served))
				(not (pkg_stored))
				(active_request)
				(oneof
					(and
						(pkg_requested)
						(want p1)
					)
					(and
						(pkg_arrived)
						(holding p1)
					)
				)
			)
	)


)

