(define (problem waldo-problem)
	(:domain waldo)
	(:objects )
	(:INIT
		(in r1)
	)
	(:goal (always 
			(eventually  
				(or 
					(search_again) 
					(seen) 
				)
			)
			) 
	)

)
