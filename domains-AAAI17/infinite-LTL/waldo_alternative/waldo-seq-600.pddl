
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Waldo example for 600 rooms
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Taken from
;; Kress-Gazit, H., Fainekos, G.E., Pappas, G.J.
;; Where's Waldo? Sensor-Based Temporal Logic Motion Planning.
;; ICRA 2007: 3116-3121
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Goal formula:
;;
;; AE(r300 || seen) && AE(r600 || seen)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; []<> (search_again | seen)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain waldo)
	(:requirements :strips :typing :equality)
	(:types 
		room
	)
	(:constants
		r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 r11 r12 r13 r14 r15 r16 r17 r18 r19 r20 r21 r22 r23 r24 r25 r26 r27 r28 r29 r30 r31 r32 r33 r34 r35 r36 r37 r38 r39 r40 r41 r42 r43 r44 r45 r46 r47 r48 r49 r50 r51 r52 r53 r54 r55 r56 r57 r58 r59 r60 r61 r62 r63 r64 r65 r66 r67 r68 r69 r70 r71 r72 r73 r74 r75 r76 r77 r78 r79 r80 r81 r82 r83 r84 r85 r86 r87 r88 r89 r90 r91 r92 r93 r94 r95 r96 r97 r98 r99 r100 r101 r102 r103 r104 r105 r106 r107 r108 r109 r110 r111 r112 r113 r114 r115 r116 r117 r118 r119 r120 r121 r122 r123 r124 r125 r126 r127 r128 r129 r130 r131 r132 r133 r134 r135 r136 r137 r138 r139 r140 r141 r142 r143 r144 r145 r146 r147 r148 r149 r150 r151 r152 r153 r154 r155 r156 r157 r158 r159 r160 r161 r162 r163 r164 r165 r166 r167 r168 r169 r170 r171 r172 r173 r174 r175 r176 r177 r178 r179 r180 r181 r182 r183 r184 r185 r186 r187 r188 r189 r190 r191 r192 r193 r194 r195 r196 r197 r198 r199 r200 r201 r202 r203 r204 r205 r206 r207 r208 r209 r210 r211 r212 r213 r214 r215 r216 r217 r218 r219 r220 r221 r222 r223 r224 r225 r226 r227 r228 r229 r230 r231 r232 r233 r234 r235 r236 r237 r238 r239 r240 r241 r242 r243 r244 r245 r246 r247 r248 r249 r250 r251 r252 r253 r254 r255 r256 r257 r258 r259 r260 r261 r262 r263 r264 r265 r266 r267 r268 r269 r270 r271 r272 r273 r274 r275 r276 r277 r278 r279 r280 r281 r282 r283 r284 r285 r286 r287 r288 r289 r290 r291 r292 r293 r294 r295 r296 r297 r298 r299 r300 r301 r302 r303 r304 r305 r306 r307 r308 r309 r310 r311 r312 r313 r314 r315 r316 r317 r318 r319 r320 r321 r322 r323 r324 r325 r326 r327 r328 r329 r330 r331 r332 r333 r334 r335 r336 r337 r338 r339 r340 r341 r342 r343 r344 r345 r346 r347 r348 r349 r350 r351 r352 r353 r354 r355 r356 r357 r358 r359 r360 r361 r362 r363 r364 r365 r366 r367 r368 r369 r370 r371 r372 r373 r374 r375 r376 r377 r378 r379 r380 r381 r382 r383 r384 r385 r386 r387 r388 r389 r390 r391 r392 r393 r394 r395 r396 r397 r398 r399 r400 r401 r402 r403 r404 r405 r406 r407 r408 r409 r410 r411 r412 r413 r414 r415 r416 r417 r418 r419 r420 r421 r422 r423 r424 r425 r426 r427 r428 r429 r430 r431 r432 r433 r434 r435 r436 r437 r438 r439 r440 r441 r442 r443 r444 r445 r446 r447 r448 r449 r450 r451 r452 r453 r454 r455 r456 r457 r458 r459 r460 r461 r462 r463 r464 r465 r466 r467 r468 r469 r470 r471 r472 r473 r474 r475 r476 r477 r478 r479 r480 r481 r482 r483 r484 r485 r486 r487 r488 r489 r490 r491 r492 r493 r494 r495 r496 r497 r498 r499 r500 r501 r502 r503 r504 r505 r506 r507 r508 r509 r510 r511 r512 r513 r514 r515 r516 r517 r518 r519 r520 r521 r522 r523 r524 r525 r526 r527 r528 r529 r530 r531 r532 r533 r534 r535 r536 r537 r538 r539 r540 r541 r542 r543 r544 r545 r546 r547 r548 r549 r550 r551 r552 r553 r554 r555 r556 r557 r558 r559 r560 r561 r562 r563 r564 r565 r566 r567 r568 r569 r570 r571 r572 r573 r574 r575 r576 r577 r578 r579 r580 r581 r582 r583 r584 r585 r586 r587 r588 r589 r590 r591 r592 r593 r594 r595 r596 r597 r598 r599 r600 - room
	)
	(:predicates
		(seen) ;; found Waldo
		(search_again) ;; searching for Waldo
		(in ?r  - room)
		(searched_in ?r  - room)
	)

	(:action move-right-from-r1
		:precondition
			(and 
				(in r1)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1))
				(in r2)
			)
	)


	(:action move-left-from-r1
		:precondition
			(and 
				(in r1)
				(not (searched_in r600))
			)
		:effect
			(and
				(not (search_again))
				(not (in r1))
				(in r600)
				(oneof (not (seen)) (seen))
				(searched_in r600)
			)
	)

	(:action move-right-from-r2
		:precondition
			(and 
				(in r2)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2))
				(in r3)
			)
	)


	(:action move-left-from-r2
		:precondition
			(and 
				(in r2)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2))
				(in r1)
			)
	)

	(:action move-right-from-r3
		:precondition
			(and 
				(in r3)
			)
		:effect
			(and
				(not (search_again))
				(not (in r3))
				(in r4)
			)
	)


	(:action move-left-from-r3
		:precondition
			(and 
				(in r3)
			)
		:effect
			(and
				(not (search_again))
				(not (in r3))
				(in r2)
			)
	)

	(:action move-right-from-r4
		:precondition
			(and 
				(in r4)
			)
		:effect
			(and
				(not (search_again))
				(not (in r4))
				(in r5)
			)
	)


	(:action move-left-from-r4
		:precondition
			(and 
				(in r4)
			)
		:effect
			(and
				(not (search_again))
				(not (in r4))
				(in r3)
			)
	)

	(:action move-right-from-r5
		:precondition
			(and 
				(in r5)
			)
		:effect
			(and
				(not (search_again))
				(not (in r5))
				(in r6)
			)
	)


	(:action move-left-from-r5
		:precondition
			(and 
				(in r5)
			)
		:effect
			(and
				(not (search_again))
				(not (in r5))
				(in r4)
			)
	)

	(:action move-right-from-r6
		:precondition
			(and 
				(in r6)
			)
		:effect
			(and
				(not (search_again))
				(not (in r6))
				(in r7)
			)
	)


	(:action move-left-from-r6
		:precondition
			(and 
				(in r6)
			)
		:effect
			(and
				(not (search_again))
				(not (in r6))
				(in r5)
			)
	)

	(:action move-right-from-r7
		:precondition
			(and 
				(in r7)
			)
		:effect
			(and
				(not (search_again))
				(not (in r7))
				(in r8)
			)
	)


	(:action move-left-from-r7
		:precondition
			(and 
				(in r7)
			)
		:effect
			(and
				(not (search_again))
				(not (in r7))
				(in r6)
			)
	)

	(:action move-right-from-r8
		:precondition
			(and 
				(in r8)
			)
		:effect
			(and
				(not (search_again))
				(not (in r8))
				(in r9)
			)
	)


	(:action move-left-from-r8
		:precondition
			(and 
				(in r8)
			)
		:effect
			(and
				(not (search_again))
				(not (in r8))
				(in r7)
			)
	)

	(:action move-right-from-r9
		:precondition
			(and 
				(in r9)
			)
		:effect
			(and
				(not (search_again))
				(not (in r9))
				(in r10)
			)
	)


	(:action move-left-from-r9
		:precondition
			(and 
				(in r9)
			)
		:effect
			(and
				(not (search_again))
				(not (in r9))
				(in r8)
			)
	)

	(:action move-right-from-r10
		:precondition
			(and 
				(in r10)
			)
		:effect
			(and
				(not (search_again))
				(not (in r10))
				(in r11)
			)
	)


	(:action move-left-from-r10
		:precondition
			(and 
				(in r10)
			)
		:effect
			(and
				(not (search_again))
				(not (in r10))
				(in r9)
			)
	)

	(:action move-right-from-r11
		:precondition
			(and 
				(in r11)
			)
		:effect
			(and
				(not (search_again))
				(not (in r11))
				(in r12)
			)
	)


	(:action move-left-from-r11
		:precondition
			(and 
				(in r11)
			)
		:effect
			(and
				(not (search_again))
				(not (in r11))
				(in r10)
			)
	)

	(:action move-right-from-r12
		:precondition
			(and 
				(in r12)
			)
		:effect
			(and
				(not (search_again))
				(not (in r12))
				(in r13)
			)
	)


	(:action move-left-from-r12
		:precondition
			(and 
				(in r12)
			)
		:effect
			(and
				(not (search_again))
				(not (in r12))
				(in r11)
			)
	)

	(:action move-right-from-r13
		:precondition
			(and 
				(in r13)
			)
		:effect
			(and
				(not (search_again))
				(not (in r13))
				(in r14)
			)
	)


	(:action move-left-from-r13
		:precondition
			(and 
				(in r13)
			)
		:effect
			(and
				(not (search_again))
				(not (in r13))
				(in r12)
			)
	)

	(:action move-right-from-r14
		:precondition
			(and 
				(in r14)
			)
		:effect
			(and
				(not (search_again))
				(not (in r14))
				(in r15)
			)
	)


	(:action move-left-from-r14
		:precondition
			(and 
				(in r14)
			)
		:effect
			(and
				(not (search_again))
				(not (in r14))
				(in r13)
			)
	)

	(:action move-right-from-r15
		:precondition
			(and 
				(in r15)
			)
		:effect
			(and
				(not (search_again))
				(not (in r15))
				(in r16)
			)
	)


	(:action move-left-from-r15
		:precondition
			(and 
				(in r15)
			)
		:effect
			(and
				(not (search_again))
				(not (in r15))
				(in r14)
			)
	)

	(:action move-right-from-r16
		:precondition
			(and 
				(in r16)
			)
		:effect
			(and
				(not (search_again))
				(not (in r16))
				(in r17)
			)
	)


	(:action move-left-from-r16
		:precondition
			(and 
				(in r16)
			)
		:effect
			(and
				(not (search_again))
				(not (in r16))
				(in r15)
			)
	)

	(:action move-right-from-r17
		:precondition
			(and 
				(in r17)
			)
		:effect
			(and
				(not (search_again))
				(not (in r17))
				(in r18)
			)
	)


	(:action move-left-from-r17
		:precondition
			(and 
				(in r17)
			)
		:effect
			(and
				(not (search_again))
				(not (in r17))
				(in r16)
			)
	)

	(:action move-right-from-r18
		:precondition
			(and 
				(in r18)
			)
		:effect
			(and
				(not (search_again))
				(not (in r18))
				(in r19)
			)
	)


	(:action move-left-from-r18
		:precondition
			(and 
				(in r18)
			)
		:effect
			(and
				(not (search_again))
				(not (in r18))
				(in r17)
			)
	)

	(:action move-right-from-r19
		:precondition
			(and 
				(in r19)
			)
		:effect
			(and
				(not (search_again))
				(not (in r19))
				(in r20)
			)
	)


	(:action move-left-from-r19
		:precondition
			(and 
				(in r19)
			)
		:effect
			(and
				(not (search_again))
				(not (in r19))
				(in r18)
			)
	)

	(:action move-right-from-r20
		:precondition
			(and 
				(in r20)
			)
		:effect
			(and
				(not (search_again))
				(not (in r20))
				(in r21)
			)
	)


	(:action move-left-from-r20
		:precondition
			(and 
				(in r20)
			)
		:effect
			(and
				(not (search_again))
				(not (in r20))
				(in r19)
			)
	)

	(:action move-right-from-r21
		:precondition
			(and 
				(in r21)
			)
		:effect
			(and
				(not (search_again))
				(not (in r21))
				(in r22)
			)
	)


	(:action move-left-from-r21
		:precondition
			(and 
				(in r21)
			)
		:effect
			(and
				(not (search_again))
				(not (in r21))
				(in r20)
			)
	)

	(:action move-right-from-r22
		:precondition
			(and 
				(in r22)
			)
		:effect
			(and
				(not (search_again))
				(not (in r22))
				(in r23)
			)
	)


	(:action move-left-from-r22
		:precondition
			(and 
				(in r22)
			)
		:effect
			(and
				(not (search_again))
				(not (in r22))
				(in r21)
			)
	)

	(:action move-right-from-r23
		:precondition
			(and 
				(in r23)
			)
		:effect
			(and
				(not (search_again))
				(not (in r23))
				(in r24)
			)
	)


	(:action move-left-from-r23
		:precondition
			(and 
				(in r23)
			)
		:effect
			(and
				(not (search_again))
				(not (in r23))
				(in r22)
			)
	)

	(:action move-right-from-r24
		:precondition
			(and 
				(in r24)
			)
		:effect
			(and
				(not (search_again))
				(not (in r24))
				(in r25)
			)
	)


	(:action move-left-from-r24
		:precondition
			(and 
				(in r24)
			)
		:effect
			(and
				(not (search_again))
				(not (in r24))
				(in r23)
			)
	)

	(:action move-right-from-r25
		:precondition
			(and 
				(in r25)
			)
		:effect
			(and
				(not (search_again))
				(not (in r25))
				(in r26)
			)
	)


	(:action move-left-from-r25
		:precondition
			(and 
				(in r25)
			)
		:effect
			(and
				(not (search_again))
				(not (in r25))
				(in r24)
			)
	)

	(:action move-right-from-r26
		:precondition
			(and 
				(in r26)
			)
		:effect
			(and
				(not (search_again))
				(not (in r26))
				(in r27)
			)
	)


	(:action move-left-from-r26
		:precondition
			(and 
				(in r26)
			)
		:effect
			(and
				(not (search_again))
				(not (in r26))
				(in r25)
			)
	)

	(:action move-right-from-r27
		:precondition
			(and 
				(in r27)
			)
		:effect
			(and
				(not (search_again))
				(not (in r27))
				(in r28)
			)
	)


	(:action move-left-from-r27
		:precondition
			(and 
				(in r27)
			)
		:effect
			(and
				(not (search_again))
				(not (in r27))
				(in r26)
			)
	)

	(:action move-right-from-r28
		:precondition
			(and 
				(in r28)
			)
		:effect
			(and
				(not (search_again))
				(not (in r28))
				(in r29)
			)
	)


	(:action move-left-from-r28
		:precondition
			(and 
				(in r28)
			)
		:effect
			(and
				(not (search_again))
				(not (in r28))
				(in r27)
			)
	)

	(:action move-right-from-r29
		:precondition
			(and 
				(in r29)
			)
		:effect
			(and
				(not (search_again))
				(not (in r29))
				(in r30)
			)
	)


	(:action move-left-from-r29
		:precondition
			(and 
				(in r29)
			)
		:effect
			(and
				(not (search_again))
				(not (in r29))
				(in r28)
			)
	)

	(:action move-right-from-r30
		:precondition
			(and 
				(in r30)
			)
		:effect
			(and
				(not (search_again))
				(not (in r30))
				(in r31)
			)
	)


	(:action move-left-from-r30
		:precondition
			(and 
				(in r30)
			)
		:effect
			(and
				(not (search_again))
				(not (in r30))
				(in r29)
			)
	)

	(:action move-right-from-r31
		:precondition
			(and 
				(in r31)
			)
		:effect
			(and
				(not (search_again))
				(not (in r31))
				(in r32)
			)
	)


	(:action move-left-from-r31
		:precondition
			(and 
				(in r31)
			)
		:effect
			(and
				(not (search_again))
				(not (in r31))
				(in r30)
			)
	)

	(:action move-right-from-r32
		:precondition
			(and 
				(in r32)
			)
		:effect
			(and
				(not (search_again))
				(not (in r32))
				(in r33)
			)
	)


	(:action move-left-from-r32
		:precondition
			(and 
				(in r32)
			)
		:effect
			(and
				(not (search_again))
				(not (in r32))
				(in r31)
			)
	)

	(:action move-right-from-r33
		:precondition
			(and 
				(in r33)
			)
		:effect
			(and
				(not (search_again))
				(not (in r33))
				(in r34)
			)
	)


	(:action move-left-from-r33
		:precondition
			(and 
				(in r33)
			)
		:effect
			(and
				(not (search_again))
				(not (in r33))
				(in r32)
			)
	)

	(:action move-right-from-r34
		:precondition
			(and 
				(in r34)
			)
		:effect
			(and
				(not (search_again))
				(not (in r34))
				(in r35)
			)
	)


	(:action move-left-from-r34
		:precondition
			(and 
				(in r34)
			)
		:effect
			(and
				(not (search_again))
				(not (in r34))
				(in r33)
			)
	)

	(:action move-right-from-r35
		:precondition
			(and 
				(in r35)
			)
		:effect
			(and
				(not (search_again))
				(not (in r35))
				(in r36)
			)
	)


	(:action move-left-from-r35
		:precondition
			(and 
				(in r35)
			)
		:effect
			(and
				(not (search_again))
				(not (in r35))
				(in r34)
			)
	)

	(:action move-right-from-r36
		:precondition
			(and 
				(in r36)
			)
		:effect
			(and
				(not (search_again))
				(not (in r36))
				(in r37)
			)
	)


	(:action move-left-from-r36
		:precondition
			(and 
				(in r36)
			)
		:effect
			(and
				(not (search_again))
				(not (in r36))
				(in r35)
			)
	)

	(:action move-right-from-r37
		:precondition
			(and 
				(in r37)
			)
		:effect
			(and
				(not (search_again))
				(not (in r37))
				(in r38)
			)
	)


	(:action move-left-from-r37
		:precondition
			(and 
				(in r37)
			)
		:effect
			(and
				(not (search_again))
				(not (in r37))
				(in r36)
			)
	)

	(:action move-right-from-r38
		:precondition
			(and 
				(in r38)
			)
		:effect
			(and
				(not (search_again))
				(not (in r38))
				(in r39)
			)
	)


	(:action move-left-from-r38
		:precondition
			(and 
				(in r38)
			)
		:effect
			(and
				(not (search_again))
				(not (in r38))
				(in r37)
			)
	)

	(:action move-right-from-r39
		:precondition
			(and 
				(in r39)
			)
		:effect
			(and
				(not (search_again))
				(not (in r39))
				(in r40)
			)
	)


	(:action move-left-from-r39
		:precondition
			(and 
				(in r39)
			)
		:effect
			(and
				(not (search_again))
				(not (in r39))
				(in r38)
			)
	)

	(:action move-right-from-r40
		:precondition
			(and 
				(in r40)
			)
		:effect
			(and
				(not (search_again))
				(not (in r40))
				(in r41)
			)
	)


	(:action move-left-from-r40
		:precondition
			(and 
				(in r40)
			)
		:effect
			(and
				(not (search_again))
				(not (in r40))
				(in r39)
			)
	)

	(:action move-right-from-r41
		:precondition
			(and 
				(in r41)
			)
		:effect
			(and
				(not (search_again))
				(not (in r41))
				(in r42)
			)
	)


	(:action move-left-from-r41
		:precondition
			(and 
				(in r41)
			)
		:effect
			(and
				(not (search_again))
				(not (in r41))
				(in r40)
			)
	)

	(:action move-right-from-r42
		:precondition
			(and 
				(in r42)
			)
		:effect
			(and
				(not (search_again))
				(not (in r42))
				(in r43)
			)
	)


	(:action move-left-from-r42
		:precondition
			(and 
				(in r42)
			)
		:effect
			(and
				(not (search_again))
				(not (in r42))
				(in r41)
			)
	)

	(:action move-right-from-r43
		:precondition
			(and 
				(in r43)
			)
		:effect
			(and
				(not (search_again))
				(not (in r43))
				(in r44)
			)
	)


	(:action move-left-from-r43
		:precondition
			(and 
				(in r43)
			)
		:effect
			(and
				(not (search_again))
				(not (in r43))
				(in r42)
			)
	)

	(:action move-right-from-r44
		:precondition
			(and 
				(in r44)
			)
		:effect
			(and
				(not (search_again))
				(not (in r44))
				(in r45)
			)
	)


	(:action move-left-from-r44
		:precondition
			(and 
				(in r44)
			)
		:effect
			(and
				(not (search_again))
				(not (in r44))
				(in r43)
			)
	)

	(:action move-right-from-r45
		:precondition
			(and 
				(in r45)
			)
		:effect
			(and
				(not (search_again))
				(not (in r45))
				(in r46)
			)
	)


	(:action move-left-from-r45
		:precondition
			(and 
				(in r45)
			)
		:effect
			(and
				(not (search_again))
				(not (in r45))
				(in r44)
			)
	)

	(:action move-right-from-r46
		:precondition
			(and 
				(in r46)
			)
		:effect
			(and
				(not (search_again))
				(not (in r46))
				(in r47)
			)
	)


	(:action move-left-from-r46
		:precondition
			(and 
				(in r46)
			)
		:effect
			(and
				(not (search_again))
				(not (in r46))
				(in r45)
			)
	)

	(:action move-right-from-r47
		:precondition
			(and 
				(in r47)
			)
		:effect
			(and
				(not (search_again))
				(not (in r47))
				(in r48)
			)
	)


	(:action move-left-from-r47
		:precondition
			(and 
				(in r47)
			)
		:effect
			(and
				(not (search_again))
				(not (in r47))
				(in r46)
			)
	)

	(:action move-right-from-r48
		:precondition
			(and 
				(in r48)
			)
		:effect
			(and
				(not (search_again))
				(not (in r48))
				(in r49)
			)
	)


	(:action move-left-from-r48
		:precondition
			(and 
				(in r48)
			)
		:effect
			(and
				(not (search_again))
				(not (in r48))
				(in r47)
			)
	)

	(:action move-right-from-r49
		:precondition
			(and 
				(in r49)
			)
		:effect
			(and
				(not (search_again))
				(not (in r49))
				(in r50)
			)
	)


	(:action move-left-from-r49
		:precondition
			(and 
				(in r49)
			)
		:effect
			(and
				(not (search_again))
				(not (in r49))
				(in r48)
			)
	)

	(:action move-right-from-r50
		:precondition
			(and 
				(in r50)
			)
		:effect
			(and
				(not (search_again))
				(not (in r50))
				(in r51)
			)
	)


	(:action move-left-from-r50
		:precondition
			(and 
				(in r50)
			)
		:effect
			(and
				(not (search_again))
				(not (in r50))
				(in r49)
			)
	)

	(:action move-right-from-r51
		:precondition
			(and 
				(in r51)
			)
		:effect
			(and
				(not (search_again))
				(not (in r51))
				(in r52)
			)
	)


	(:action move-left-from-r51
		:precondition
			(and 
				(in r51)
			)
		:effect
			(and
				(not (search_again))
				(not (in r51))
				(in r50)
			)
	)

	(:action move-right-from-r52
		:precondition
			(and 
				(in r52)
			)
		:effect
			(and
				(not (search_again))
				(not (in r52))
				(in r53)
			)
	)


	(:action move-left-from-r52
		:precondition
			(and 
				(in r52)
			)
		:effect
			(and
				(not (search_again))
				(not (in r52))
				(in r51)
			)
	)

	(:action move-right-from-r53
		:precondition
			(and 
				(in r53)
			)
		:effect
			(and
				(not (search_again))
				(not (in r53))
				(in r54)
			)
	)


	(:action move-left-from-r53
		:precondition
			(and 
				(in r53)
			)
		:effect
			(and
				(not (search_again))
				(not (in r53))
				(in r52)
			)
	)

	(:action move-right-from-r54
		:precondition
			(and 
				(in r54)
			)
		:effect
			(and
				(not (search_again))
				(not (in r54))
				(in r55)
			)
	)


	(:action move-left-from-r54
		:precondition
			(and 
				(in r54)
			)
		:effect
			(and
				(not (search_again))
				(not (in r54))
				(in r53)
			)
	)

	(:action move-right-from-r55
		:precondition
			(and 
				(in r55)
			)
		:effect
			(and
				(not (search_again))
				(not (in r55))
				(in r56)
			)
	)


	(:action move-left-from-r55
		:precondition
			(and 
				(in r55)
			)
		:effect
			(and
				(not (search_again))
				(not (in r55))
				(in r54)
			)
	)

	(:action move-right-from-r56
		:precondition
			(and 
				(in r56)
			)
		:effect
			(and
				(not (search_again))
				(not (in r56))
				(in r57)
			)
	)


	(:action move-left-from-r56
		:precondition
			(and 
				(in r56)
			)
		:effect
			(and
				(not (search_again))
				(not (in r56))
				(in r55)
			)
	)

	(:action move-right-from-r57
		:precondition
			(and 
				(in r57)
			)
		:effect
			(and
				(not (search_again))
				(not (in r57))
				(in r58)
			)
	)


	(:action move-left-from-r57
		:precondition
			(and 
				(in r57)
			)
		:effect
			(and
				(not (search_again))
				(not (in r57))
				(in r56)
			)
	)

	(:action move-right-from-r58
		:precondition
			(and 
				(in r58)
			)
		:effect
			(and
				(not (search_again))
				(not (in r58))
				(in r59)
			)
	)


	(:action move-left-from-r58
		:precondition
			(and 
				(in r58)
			)
		:effect
			(and
				(not (search_again))
				(not (in r58))
				(in r57)
			)
	)

	(:action move-right-from-r59
		:precondition
			(and 
				(in r59)
			)
		:effect
			(and
				(not (search_again))
				(not (in r59))
				(in r60)
			)
	)


	(:action move-left-from-r59
		:precondition
			(and 
				(in r59)
			)
		:effect
			(and
				(not (search_again))
				(not (in r59))
				(in r58)
			)
	)

	(:action move-right-from-r60
		:precondition
			(and 
				(in r60)
			)
		:effect
			(and
				(not (search_again))
				(not (in r60))
				(in r61)
			)
	)


	(:action move-left-from-r60
		:precondition
			(and 
				(in r60)
			)
		:effect
			(and
				(not (search_again))
				(not (in r60))
				(in r59)
			)
	)

	(:action move-right-from-r61
		:precondition
			(and 
				(in r61)
			)
		:effect
			(and
				(not (search_again))
				(not (in r61))
				(in r62)
			)
	)


	(:action move-left-from-r61
		:precondition
			(and 
				(in r61)
			)
		:effect
			(and
				(not (search_again))
				(not (in r61))
				(in r60)
			)
	)

	(:action move-right-from-r62
		:precondition
			(and 
				(in r62)
			)
		:effect
			(and
				(not (search_again))
				(not (in r62))
				(in r63)
			)
	)


	(:action move-left-from-r62
		:precondition
			(and 
				(in r62)
			)
		:effect
			(and
				(not (search_again))
				(not (in r62))
				(in r61)
			)
	)

	(:action move-right-from-r63
		:precondition
			(and 
				(in r63)
			)
		:effect
			(and
				(not (search_again))
				(not (in r63))
				(in r64)
			)
	)


	(:action move-left-from-r63
		:precondition
			(and 
				(in r63)
			)
		:effect
			(and
				(not (search_again))
				(not (in r63))
				(in r62)
			)
	)

	(:action move-right-from-r64
		:precondition
			(and 
				(in r64)
			)
		:effect
			(and
				(not (search_again))
				(not (in r64))
				(in r65)
			)
	)


	(:action move-left-from-r64
		:precondition
			(and 
				(in r64)
			)
		:effect
			(and
				(not (search_again))
				(not (in r64))
				(in r63)
			)
	)

	(:action move-right-from-r65
		:precondition
			(and 
				(in r65)
			)
		:effect
			(and
				(not (search_again))
				(not (in r65))
				(in r66)
			)
	)


	(:action move-left-from-r65
		:precondition
			(and 
				(in r65)
			)
		:effect
			(and
				(not (search_again))
				(not (in r65))
				(in r64)
			)
	)

	(:action move-right-from-r66
		:precondition
			(and 
				(in r66)
			)
		:effect
			(and
				(not (search_again))
				(not (in r66))
				(in r67)
			)
	)


	(:action move-left-from-r66
		:precondition
			(and 
				(in r66)
			)
		:effect
			(and
				(not (search_again))
				(not (in r66))
				(in r65)
			)
	)

	(:action move-right-from-r67
		:precondition
			(and 
				(in r67)
			)
		:effect
			(and
				(not (search_again))
				(not (in r67))
				(in r68)
			)
	)


	(:action move-left-from-r67
		:precondition
			(and 
				(in r67)
			)
		:effect
			(and
				(not (search_again))
				(not (in r67))
				(in r66)
			)
	)

	(:action move-right-from-r68
		:precondition
			(and 
				(in r68)
			)
		:effect
			(and
				(not (search_again))
				(not (in r68))
				(in r69)
			)
	)


	(:action move-left-from-r68
		:precondition
			(and 
				(in r68)
			)
		:effect
			(and
				(not (search_again))
				(not (in r68))
				(in r67)
			)
	)

	(:action move-right-from-r69
		:precondition
			(and 
				(in r69)
			)
		:effect
			(and
				(not (search_again))
				(not (in r69))
				(in r70)
			)
	)


	(:action move-left-from-r69
		:precondition
			(and 
				(in r69)
			)
		:effect
			(and
				(not (search_again))
				(not (in r69))
				(in r68)
			)
	)

	(:action move-right-from-r70
		:precondition
			(and 
				(in r70)
			)
		:effect
			(and
				(not (search_again))
				(not (in r70))
				(in r71)
			)
	)


	(:action move-left-from-r70
		:precondition
			(and 
				(in r70)
			)
		:effect
			(and
				(not (search_again))
				(not (in r70))
				(in r69)
			)
	)

	(:action move-right-from-r71
		:precondition
			(and 
				(in r71)
			)
		:effect
			(and
				(not (search_again))
				(not (in r71))
				(in r72)
			)
	)


	(:action move-left-from-r71
		:precondition
			(and 
				(in r71)
			)
		:effect
			(and
				(not (search_again))
				(not (in r71))
				(in r70)
			)
	)

	(:action move-right-from-r72
		:precondition
			(and 
				(in r72)
			)
		:effect
			(and
				(not (search_again))
				(not (in r72))
				(in r73)
			)
	)


	(:action move-left-from-r72
		:precondition
			(and 
				(in r72)
			)
		:effect
			(and
				(not (search_again))
				(not (in r72))
				(in r71)
			)
	)

	(:action move-right-from-r73
		:precondition
			(and 
				(in r73)
			)
		:effect
			(and
				(not (search_again))
				(not (in r73))
				(in r74)
			)
	)


	(:action move-left-from-r73
		:precondition
			(and 
				(in r73)
			)
		:effect
			(and
				(not (search_again))
				(not (in r73))
				(in r72)
			)
	)

	(:action move-right-from-r74
		:precondition
			(and 
				(in r74)
			)
		:effect
			(and
				(not (search_again))
				(not (in r74))
				(in r75)
			)
	)


	(:action move-left-from-r74
		:precondition
			(and 
				(in r74)
			)
		:effect
			(and
				(not (search_again))
				(not (in r74))
				(in r73)
			)
	)

	(:action move-right-from-r75
		:precondition
			(and 
				(in r75)
			)
		:effect
			(and
				(not (search_again))
				(not (in r75))
				(in r76)
			)
	)


	(:action move-left-from-r75
		:precondition
			(and 
				(in r75)
			)
		:effect
			(and
				(not (search_again))
				(not (in r75))
				(in r74)
			)
	)

	(:action move-right-from-r76
		:precondition
			(and 
				(in r76)
			)
		:effect
			(and
				(not (search_again))
				(not (in r76))
				(in r77)
			)
	)


	(:action move-left-from-r76
		:precondition
			(and 
				(in r76)
			)
		:effect
			(and
				(not (search_again))
				(not (in r76))
				(in r75)
			)
	)

	(:action move-right-from-r77
		:precondition
			(and 
				(in r77)
			)
		:effect
			(and
				(not (search_again))
				(not (in r77))
				(in r78)
			)
	)


	(:action move-left-from-r77
		:precondition
			(and 
				(in r77)
			)
		:effect
			(and
				(not (search_again))
				(not (in r77))
				(in r76)
			)
	)

	(:action move-right-from-r78
		:precondition
			(and 
				(in r78)
			)
		:effect
			(and
				(not (search_again))
				(not (in r78))
				(in r79)
			)
	)


	(:action move-left-from-r78
		:precondition
			(and 
				(in r78)
			)
		:effect
			(and
				(not (search_again))
				(not (in r78))
				(in r77)
			)
	)

	(:action move-right-from-r79
		:precondition
			(and 
				(in r79)
			)
		:effect
			(and
				(not (search_again))
				(not (in r79))
				(in r80)
			)
	)


	(:action move-left-from-r79
		:precondition
			(and 
				(in r79)
			)
		:effect
			(and
				(not (search_again))
				(not (in r79))
				(in r78)
			)
	)

	(:action move-right-from-r80
		:precondition
			(and 
				(in r80)
			)
		:effect
			(and
				(not (search_again))
				(not (in r80))
				(in r81)
			)
	)


	(:action move-left-from-r80
		:precondition
			(and 
				(in r80)
			)
		:effect
			(and
				(not (search_again))
				(not (in r80))
				(in r79)
			)
	)

	(:action move-right-from-r81
		:precondition
			(and 
				(in r81)
			)
		:effect
			(and
				(not (search_again))
				(not (in r81))
				(in r82)
			)
	)


	(:action move-left-from-r81
		:precondition
			(and 
				(in r81)
			)
		:effect
			(and
				(not (search_again))
				(not (in r81))
				(in r80)
			)
	)

	(:action move-right-from-r82
		:precondition
			(and 
				(in r82)
			)
		:effect
			(and
				(not (search_again))
				(not (in r82))
				(in r83)
			)
	)


	(:action move-left-from-r82
		:precondition
			(and 
				(in r82)
			)
		:effect
			(and
				(not (search_again))
				(not (in r82))
				(in r81)
			)
	)

	(:action move-right-from-r83
		:precondition
			(and 
				(in r83)
			)
		:effect
			(and
				(not (search_again))
				(not (in r83))
				(in r84)
			)
	)


	(:action move-left-from-r83
		:precondition
			(and 
				(in r83)
			)
		:effect
			(and
				(not (search_again))
				(not (in r83))
				(in r82)
			)
	)

	(:action move-right-from-r84
		:precondition
			(and 
				(in r84)
			)
		:effect
			(and
				(not (search_again))
				(not (in r84))
				(in r85)
			)
	)


	(:action move-left-from-r84
		:precondition
			(and 
				(in r84)
			)
		:effect
			(and
				(not (search_again))
				(not (in r84))
				(in r83)
			)
	)

	(:action move-right-from-r85
		:precondition
			(and 
				(in r85)
			)
		:effect
			(and
				(not (search_again))
				(not (in r85))
				(in r86)
			)
	)


	(:action move-left-from-r85
		:precondition
			(and 
				(in r85)
			)
		:effect
			(and
				(not (search_again))
				(not (in r85))
				(in r84)
			)
	)

	(:action move-right-from-r86
		:precondition
			(and 
				(in r86)
			)
		:effect
			(and
				(not (search_again))
				(not (in r86))
				(in r87)
			)
	)


	(:action move-left-from-r86
		:precondition
			(and 
				(in r86)
			)
		:effect
			(and
				(not (search_again))
				(not (in r86))
				(in r85)
			)
	)

	(:action move-right-from-r87
		:precondition
			(and 
				(in r87)
			)
		:effect
			(and
				(not (search_again))
				(not (in r87))
				(in r88)
			)
	)


	(:action move-left-from-r87
		:precondition
			(and 
				(in r87)
			)
		:effect
			(and
				(not (search_again))
				(not (in r87))
				(in r86)
			)
	)

	(:action move-right-from-r88
		:precondition
			(and 
				(in r88)
			)
		:effect
			(and
				(not (search_again))
				(not (in r88))
				(in r89)
			)
	)


	(:action move-left-from-r88
		:precondition
			(and 
				(in r88)
			)
		:effect
			(and
				(not (search_again))
				(not (in r88))
				(in r87)
			)
	)

	(:action move-right-from-r89
		:precondition
			(and 
				(in r89)
			)
		:effect
			(and
				(not (search_again))
				(not (in r89))
				(in r90)
			)
	)


	(:action move-left-from-r89
		:precondition
			(and 
				(in r89)
			)
		:effect
			(and
				(not (search_again))
				(not (in r89))
				(in r88)
			)
	)

	(:action move-right-from-r90
		:precondition
			(and 
				(in r90)
			)
		:effect
			(and
				(not (search_again))
				(not (in r90))
				(in r91)
			)
	)


	(:action move-left-from-r90
		:precondition
			(and 
				(in r90)
			)
		:effect
			(and
				(not (search_again))
				(not (in r90))
				(in r89)
			)
	)

	(:action move-right-from-r91
		:precondition
			(and 
				(in r91)
			)
		:effect
			(and
				(not (search_again))
				(not (in r91))
				(in r92)
			)
	)


	(:action move-left-from-r91
		:precondition
			(and 
				(in r91)
			)
		:effect
			(and
				(not (search_again))
				(not (in r91))
				(in r90)
			)
	)

	(:action move-right-from-r92
		:precondition
			(and 
				(in r92)
			)
		:effect
			(and
				(not (search_again))
				(not (in r92))
				(in r93)
			)
	)


	(:action move-left-from-r92
		:precondition
			(and 
				(in r92)
			)
		:effect
			(and
				(not (search_again))
				(not (in r92))
				(in r91)
			)
	)

	(:action move-right-from-r93
		:precondition
			(and 
				(in r93)
			)
		:effect
			(and
				(not (search_again))
				(not (in r93))
				(in r94)
			)
	)


	(:action move-left-from-r93
		:precondition
			(and 
				(in r93)
			)
		:effect
			(and
				(not (search_again))
				(not (in r93))
				(in r92)
			)
	)

	(:action move-right-from-r94
		:precondition
			(and 
				(in r94)
			)
		:effect
			(and
				(not (search_again))
				(not (in r94))
				(in r95)
			)
	)


	(:action move-left-from-r94
		:precondition
			(and 
				(in r94)
			)
		:effect
			(and
				(not (search_again))
				(not (in r94))
				(in r93)
			)
	)

	(:action move-right-from-r95
		:precondition
			(and 
				(in r95)
			)
		:effect
			(and
				(not (search_again))
				(not (in r95))
				(in r96)
			)
	)


	(:action move-left-from-r95
		:precondition
			(and 
				(in r95)
			)
		:effect
			(and
				(not (search_again))
				(not (in r95))
				(in r94)
			)
	)

	(:action move-right-from-r96
		:precondition
			(and 
				(in r96)
			)
		:effect
			(and
				(not (search_again))
				(not (in r96))
				(in r97)
			)
	)


	(:action move-left-from-r96
		:precondition
			(and 
				(in r96)
			)
		:effect
			(and
				(not (search_again))
				(not (in r96))
				(in r95)
			)
	)

	(:action move-right-from-r97
		:precondition
			(and 
				(in r97)
			)
		:effect
			(and
				(not (search_again))
				(not (in r97))
				(in r98)
			)
	)


	(:action move-left-from-r97
		:precondition
			(and 
				(in r97)
			)
		:effect
			(and
				(not (search_again))
				(not (in r97))
				(in r96)
			)
	)

	(:action move-right-from-r98
		:precondition
			(and 
				(in r98)
			)
		:effect
			(and
				(not (search_again))
				(not (in r98))
				(in r99)
			)
	)


	(:action move-left-from-r98
		:precondition
			(and 
				(in r98)
			)
		:effect
			(and
				(not (search_again))
				(not (in r98))
				(in r97)
			)
	)

	(:action move-right-from-r99
		:precondition
			(and 
				(in r99)
			)
		:effect
			(and
				(not (search_again))
				(not (in r99))
				(in r100)
			)
	)


	(:action move-left-from-r99
		:precondition
			(and 
				(in r99)
			)
		:effect
			(and
				(not (search_again))
				(not (in r99))
				(in r98)
			)
	)

	(:action move-right-from-r100
		:precondition
			(and 
				(in r100)
			)
		:effect
			(and
				(not (search_again))
				(not (in r100))
				(in r101)
			)
	)


	(:action move-left-from-r100
		:precondition
			(and 
				(in r100)
			)
		:effect
			(and
				(not (search_again))
				(not (in r100))
				(in r99)
			)
	)

	(:action move-right-from-r101
		:precondition
			(and 
				(in r101)
			)
		:effect
			(and
				(not (search_again))
				(not (in r101))
				(in r102)
			)
	)


	(:action move-left-from-r101
		:precondition
			(and 
				(in r101)
			)
		:effect
			(and
				(not (search_again))
				(not (in r101))
				(in r100)
			)
	)

	(:action move-right-from-r102
		:precondition
			(and 
				(in r102)
			)
		:effect
			(and
				(not (search_again))
				(not (in r102))
				(in r103)
			)
	)


	(:action move-left-from-r102
		:precondition
			(and 
				(in r102)
			)
		:effect
			(and
				(not (search_again))
				(not (in r102))
				(in r101)
			)
	)

	(:action move-right-from-r103
		:precondition
			(and 
				(in r103)
			)
		:effect
			(and
				(not (search_again))
				(not (in r103))
				(in r104)
			)
	)


	(:action move-left-from-r103
		:precondition
			(and 
				(in r103)
			)
		:effect
			(and
				(not (search_again))
				(not (in r103))
				(in r102)
			)
	)

	(:action move-right-from-r104
		:precondition
			(and 
				(in r104)
			)
		:effect
			(and
				(not (search_again))
				(not (in r104))
				(in r105)
			)
	)


	(:action move-left-from-r104
		:precondition
			(and 
				(in r104)
			)
		:effect
			(and
				(not (search_again))
				(not (in r104))
				(in r103)
			)
	)

	(:action move-right-from-r105
		:precondition
			(and 
				(in r105)
			)
		:effect
			(and
				(not (search_again))
				(not (in r105))
				(in r106)
			)
	)


	(:action move-left-from-r105
		:precondition
			(and 
				(in r105)
			)
		:effect
			(and
				(not (search_again))
				(not (in r105))
				(in r104)
			)
	)

	(:action move-right-from-r106
		:precondition
			(and 
				(in r106)
			)
		:effect
			(and
				(not (search_again))
				(not (in r106))
				(in r107)
			)
	)


	(:action move-left-from-r106
		:precondition
			(and 
				(in r106)
			)
		:effect
			(and
				(not (search_again))
				(not (in r106))
				(in r105)
			)
	)

	(:action move-right-from-r107
		:precondition
			(and 
				(in r107)
			)
		:effect
			(and
				(not (search_again))
				(not (in r107))
				(in r108)
			)
	)


	(:action move-left-from-r107
		:precondition
			(and 
				(in r107)
			)
		:effect
			(and
				(not (search_again))
				(not (in r107))
				(in r106)
			)
	)

	(:action move-right-from-r108
		:precondition
			(and 
				(in r108)
			)
		:effect
			(and
				(not (search_again))
				(not (in r108))
				(in r109)
			)
	)


	(:action move-left-from-r108
		:precondition
			(and 
				(in r108)
			)
		:effect
			(and
				(not (search_again))
				(not (in r108))
				(in r107)
			)
	)

	(:action move-right-from-r109
		:precondition
			(and 
				(in r109)
			)
		:effect
			(and
				(not (search_again))
				(not (in r109))
				(in r110)
			)
	)


	(:action move-left-from-r109
		:precondition
			(and 
				(in r109)
			)
		:effect
			(and
				(not (search_again))
				(not (in r109))
				(in r108)
			)
	)

	(:action move-right-from-r110
		:precondition
			(and 
				(in r110)
			)
		:effect
			(and
				(not (search_again))
				(not (in r110))
				(in r111)
			)
	)


	(:action move-left-from-r110
		:precondition
			(and 
				(in r110)
			)
		:effect
			(and
				(not (search_again))
				(not (in r110))
				(in r109)
			)
	)

	(:action move-right-from-r111
		:precondition
			(and 
				(in r111)
			)
		:effect
			(and
				(not (search_again))
				(not (in r111))
				(in r112)
			)
	)


	(:action move-left-from-r111
		:precondition
			(and 
				(in r111)
			)
		:effect
			(and
				(not (search_again))
				(not (in r111))
				(in r110)
			)
	)

	(:action move-right-from-r112
		:precondition
			(and 
				(in r112)
			)
		:effect
			(and
				(not (search_again))
				(not (in r112))
				(in r113)
			)
	)


	(:action move-left-from-r112
		:precondition
			(and 
				(in r112)
			)
		:effect
			(and
				(not (search_again))
				(not (in r112))
				(in r111)
			)
	)

	(:action move-right-from-r113
		:precondition
			(and 
				(in r113)
			)
		:effect
			(and
				(not (search_again))
				(not (in r113))
				(in r114)
			)
	)


	(:action move-left-from-r113
		:precondition
			(and 
				(in r113)
			)
		:effect
			(and
				(not (search_again))
				(not (in r113))
				(in r112)
			)
	)

	(:action move-right-from-r114
		:precondition
			(and 
				(in r114)
			)
		:effect
			(and
				(not (search_again))
				(not (in r114))
				(in r115)
			)
	)


	(:action move-left-from-r114
		:precondition
			(and 
				(in r114)
			)
		:effect
			(and
				(not (search_again))
				(not (in r114))
				(in r113)
			)
	)

	(:action move-right-from-r115
		:precondition
			(and 
				(in r115)
			)
		:effect
			(and
				(not (search_again))
				(not (in r115))
				(in r116)
			)
	)


	(:action move-left-from-r115
		:precondition
			(and 
				(in r115)
			)
		:effect
			(and
				(not (search_again))
				(not (in r115))
				(in r114)
			)
	)

	(:action move-right-from-r116
		:precondition
			(and 
				(in r116)
			)
		:effect
			(and
				(not (search_again))
				(not (in r116))
				(in r117)
			)
	)


	(:action move-left-from-r116
		:precondition
			(and 
				(in r116)
			)
		:effect
			(and
				(not (search_again))
				(not (in r116))
				(in r115)
			)
	)

	(:action move-right-from-r117
		:precondition
			(and 
				(in r117)
			)
		:effect
			(and
				(not (search_again))
				(not (in r117))
				(in r118)
			)
	)


	(:action move-left-from-r117
		:precondition
			(and 
				(in r117)
			)
		:effect
			(and
				(not (search_again))
				(not (in r117))
				(in r116)
			)
	)

	(:action move-right-from-r118
		:precondition
			(and 
				(in r118)
			)
		:effect
			(and
				(not (search_again))
				(not (in r118))
				(in r119)
			)
	)


	(:action move-left-from-r118
		:precondition
			(and 
				(in r118)
			)
		:effect
			(and
				(not (search_again))
				(not (in r118))
				(in r117)
			)
	)

	(:action move-right-from-r119
		:precondition
			(and 
				(in r119)
			)
		:effect
			(and
				(not (search_again))
				(not (in r119))
				(in r120)
			)
	)


	(:action move-left-from-r119
		:precondition
			(and 
				(in r119)
			)
		:effect
			(and
				(not (search_again))
				(not (in r119))
				(in r118)
			)
	)

	(:action move-right-from-r120
		:precondition
			(and 
				(in r120)
			)
		:effect
			(and
				(not (search_again))
				(not (in r120))
				(in r121)
			)
	)


	(:action move-left-from-r120
		:precondition
			(and 
				(in r120)
			)
		:effect
			(and
				(not (search_again))
				(not (in r120))
				(in r119)
			)
	)

	(:action move-right-from-r121
		:precondition
			(and 
				(in r121)
			)
		:effect
			(and
				(not (search_again))
				(not (in r121))
				(in r122)
			)
	)


	(:action move-left-from-r121
		:precondition
			(and 
				(in r121)
			)
		:effect
			(and
				(not (search_again))
				(not (in r121))
				(in r120)
			)
	)

	(:action move-right-from-r122
		:precondition
			(and 
				(in r122)
			)
		:effect
			(and
				(not (search_again))
				(not (in r122))
				(in r123)
			)
	)


	(:action move-left-from-r122
		:precondition
			(and 
				(in r122)
			)
		:effect
			(and
				(not (search_again))
				(not (in r122))
				(in r121)
			)
	)

	(:action move-right-from-r123
		:precondition
			(and 
				(in r123)
			)
		:effect
			(and
				(not (search_again))
				(not (in r123))
				(in r124)
			)
	)


	(:action move-left-from-r123
		:precondition
			(and 
				(in r123)
			)
		:effect
			(and
				(not (search_again))
				(not (in r123))
				(in r122)
			)
	)

	(:action move-right-from-r124
		:precondition
			(and 
				(in r124)
			)
		:effect
			(and
				(not (search_again))
				(not (in r124))
				(in r125)
			)
	)


	(:action move-left-from-r124
		:precondition
			(and 
				(in r124)
			)
		:effect
			(and
				(not (search_again))
				(not (in r124))
				(in r123)
			)
	)

	(:action move-right-from-r125
		:precondition
			(and 
				(in r125)
			)
		:effect
			(and
				(not (search_again))
				(not (in r125))
				(in r126)
			)
	)


	(:action move-left-from-r125
		:precondition
			(and 
				(in r125)
			)
		:effect
			(and
				(not (search_again))
				(not (in r125))
				(in r124)
			)
	)

	(:action move-right-from-r126
		:precondition
			(and 
				(in r126)
			)
		:effect
			(and
				(not (search_again))
				(not (in r126))
				(in r127)
			)
	)


	(:action move-left-from-r126
		:precondition
			(and 
				(in r126)
			)
		:effect
			(and
				(not (search_again))
				(not (in r126))
				(in r125)
			)
	)

	(:action move-right-from-r127
		:precondition
			(and 
				(in r127)
			)
		:effect
			(and
				(not (search_again))
				(not (in r127))
				(in r128)
			)
	)


	(:action move-left-from-r127
		:precondition
			(and 
				(in r127)
			)
		:effect
			(and
				(not (search_again))
				(not (in r127))
				(in r126)
			)
	)

	(:action move-right-from-r128
		:precondition
			(and 
				(in r128)
			)
		:effect
			(and
				(not (search_again))
				(not (in r128))
				(in r129)
			)
	)


	(:action move-left-from-r128
		:precondition
			(and 
				(in r128)
			)
		:effect
			(and
				(not (search_again))
				(not (in r128))
				(in r127)
			)
	)

	(:action move-right-from-r129
		:precondition
			(and 
				(in r129)
			)
		:effect
			(and
				(not (search_again))
				(not (in r129))
				(in r130)
			)
	)


	(:action move-left-from-r129
		:precondition
			(and 
				(in r129)
			)
		:effect
			(and
				(not (search_again))
				(not (in r129))
				(in r128)
			)
	)

	(:action move-right-from-r130
		:precondition
			(and 
				(in r130)
			)
		:effect
			(and
				(not (search_again))
				(not (in r130))
				(in r131)
			)
	)


	(:action move-left-from-r130
		:precondition
			(and 
				(in r130)
			)
		:effect
			(and
				(not (search_again))
				(not (in r130))
				(in r129)
			)
	)

	(:action move-right-from-r131
		:precondition
			(and 
				(in r131)
			)
		:effect
			(and
				(not (search_again))
				(not (in r131))
				(in r132)
			)
	)


	(:action move-left-from-r131
		:precondition
			(and 
				(in r131)
			)
		:effect
			(and
				(not (search_again))
				(not (in r131))
				(in r130)
			)
	)

	(:action move-right-from-r132
		:precondition
			(and 
				(in r132)
			)
		:effect
			(and
				(not (search_again))
				(not (in r132))
				(in r133)
			)
	)


	(:action move-left-from-r132
		:precondition
			(and 
				(in r132)
			)
		:effect
			(and
				(not (search_again))
				(not (in r132))
				(in r131)
			)
	)

	(:action move-right-from-r133
		:precondition
			(and 
				(in r133)
			)
		:effect
			(and
				(not (search_again))
				(not (in r133))
				(in r134)
			)
	)


	(:action move-left-from-r133
		:precondition
			(and 
				(in r133)
			)
		:effect
			(and
				(not (search_again))
				(not (in r133))
				(in r132)
			)
	)

	(:action move-right-from-r134
		:precondition
			(and 
				(in r134)
			)
		:effect
			(and
				(not (search_again))
				(not (in r134))
				(in r135)
			)
	)


	(:action move-left-from-r134
		:precondition
			(and 
				(in r134)
			)
		:effect
			(and
				(not (search_again))
				(not (in r134))
				(in r133)
			)
	)

	(:action move-right-from-r135
		:precondition
			(and 
				(in r135)
			)
		:effect
			(and
				(not (search_again))
				(not (in r135))
				(in r136)
			)
	)


	(:action move-left-from-r135
		:precondition
			(and 
				(in r135)
			)
		:effect
			(and
				(not (search_again))
				(not (in r135))
				(in r134)
			)
	)

	(:action move-right-from-r136
		:precondition
			(and 
				(in r136)
			)
		:effect
			(and
				(not (search_again))
				(not (in r136))
				(in r137)
			)
	)


	(:action move-left-from-r136
		:precondition
			(and 
				(in r136)
			)
		:effect
			(and
				(not (search_again))
				(not (in r136))
				(in r135)
			)
	)

	(:action move-right-from-r137
		:precondition
			(and 
				(in r137)
			)
		:effect
			(and
				(not (search_again))
				(not (in r137))
				(in r138)
			)
	)


	(:action move-left-from-r137
		:precondition
			(and 
				(in r137)
			)
		:effect
			(and
				(not (search_again))
				(not (in r137))
				(in r136)
			)
	)

	(:action move-right-from-r138
		:precondition
			(and 
				(in r138)
			)
		:effect
			(and
				(not (search_again))
				(not (in r138))
				(in r139)
			)
	)


	(:action move-left-from-r138
		:precondition
			(and 
				(in r138)
			)
		:effect
			(and
				(not (search_again))
				(not (in r138))
				(in r137)
			)
	)

	(:action move-right-from-r139
		:precondition
			(and 
				(in r139)
			)
		:effect
			(and
				(not (search_again))
				(not (in r139))
				(in r140)
			)
	)


	(:action move-left-from-r139
		:precondition
			(and 
				(in r139)
			)
		:effect
			(and
				(not (search_again))
				(not (in r139))
				(in r138)
			)
	)

	(:action move-right-from-r140
		:precondition
			(and 
				(in r140)
			)
		:effect
			(and
				(not (search_again))
				(not (in r140))
				(in r141)
			)
	)


	(:action move-left-from-r140
		:precondition
			(and 
				(in r140)
			)
		:effect
			(and
				(not (search_again))
				(not (in r140))
				(in r139)
			)
	)

	(:action move-right-from-r141
		:precondition
			(and 
				(in r141)
			)
		:effect
			(and
				(not (search_again))
				(not (in r141))
				(in r142)
			)
	)


	(:action move-left-from-r141
		:precondition
			(and 
				(in r141)
			)
		:effect
			(and
				(not (search_again))
				(not (in r141))
				(in r140)
			)
	)

	(:action move-right-from-r142
		:precondition
			(and 
				(in r142)
			)
		:effect
			(and
				(not (search_again))
				(not (in r142))
				(in r143)
			)
	)


	(:action move-left-from-r142
		:precondition
			(and 
				(in r142)
			)
		:effect
			(and
				(not (search_again))
				(not (in r142))
				(in r141)
			)
	)

	(:action move-right-from-r143
		:precondition
			(and 
				(in r143)
			)
		:effect
			(and
				(not (search_again))
				(not (in r143))
				(in r144)
			)
	)


	(:action move-left-from-r143
		:precondition
			(and 
				(in r143)
			)
		:effect
			(and
				(not (search_again))
				(not (in r143))
				(in r142)
			)
	)

	(:action move-right-from-r144
		:precondition
			(and 
				(in r144)
			)
		:effect
			(and
				(not (search_again))
				(not (in r144))
				(in r145)
			)
	)


	(:action move-left-from-r144
		:precondition
			(and 
				(in r144)
			)
		:effect
			(and
				(not (search_again))
				(not (in r144))
				(in r143)
			)
	)

	(:action move-right-from-r145
		:precondition
			(and 
				(in r145)
			)
		:effect
			(and
				(not (search_again))
				(not (in r145))
				(in r146)
			)
	)


	(:action move-left-from-r145
		:precondition
			(and 
				(in r145)
			)
		:effect
			(and
				(not (search_again))
				(not (in r145))
				(in r144)
			)
	)

	(:action move-right-from-r146
		:precondition
			(and 
				(in r146)
			)
		:effect
			(and
				(not (search_again))
				(not (in r146))
				(in r147)
			)
	)


	(:action move-left-from-r146
		:precondition
			(and 
				(in r146)
			)
		:effect
			(and
				(not (search_again))
				(not (in r146))
				(in r145)
			)
	)

	(:action move-right-from-r147
		:precondition
			(and 
				(in r147)
			)
		:effect
			(and
				(not (search_again))
				(not (in r147))
				(in r148)
			)
	)


	(:action move-left-from-r147
		:precondition
			(and 
				(in r147)
			)
		:effect
			(and
				(not (search_again))
				(not (in r147))
				(in r146)
			)
	)

	(:action move-right-from-r148
		:precondition
			(and 
				(in r148)
			)
		:effect
			(and
				(not (search_again))
				(not (in r148))
				(in r149)
			)
	)


	(:action move-left-from-r148
		:precondition
			(and 
				(in r148)
			)
		:effect
			(and
				(not (search_again))
				(not (in r148))
				(in r147)
			)
	)

	(:action move-right-from-r149
		:precondition
			(and 
				(in r149)
			)
		:effect
			(and
				(not (search_again))
				(not (in r149))
				(in r150)
			)
	)


	(:action move-left-from-r149
		:precondition
			(and 
				(in r149)
			)
		:effect
			(and
				(not (search_again))
				(not (in r149))
				(in r148)
			)
	)

	(:action move-right-from-r150
		:precondition
			(and 
				(in r150)
			)
		:effect
			(and
				(not (search_again))
				(not (in r150))
				(in r151)
			)
	)


	(:action move-left-from-r150
		:precondition
			(and 
				(in r150)
			)
		:effect
			(and
				(not (search_again))
				(not (in r150))
				(in r149)
			)
	)

	(:action move-right-from-r151
		:precondition
			(and 
				(in r151)
			)
		:effect
			(and
				(not (search_again))
				(not (in r151))
				(in r152)
			)
	)


	(:action move-left-from-r151
		:precondition
			(and 
				(in r151)
			)
		:effect
			(and
				(not (search_again))
				(not (in r151))
				(in r150)
			)
	)

	(:action move-right-from-r152
		:precondition
			(and 
				(in r152)
			)
		:effect
			(and
				(not (search_again))
				(not (in r152))
				(in r153)
			)
	)


	(:action move-left-from-r152
		:precondition
			(and 
				(in r152)
			)
		:effect
			(and
				(not (search_again))
				(not (in r152))
				(in r151)
			)
	)

	(:action move-right-from-r153
		:precondition
			(and 
				(in r153)
			)
		:effect
			(and
				(not (search_again))
				(not (in r153))
				(in r154)
			)
	)


	(:action move-left-from-r153
		:precondition
			(and 
				(in r153)
			)
		:effect
			(and
				(not (search_again))
				(not (in r153))
				(in r152)
			)
	)

	(:action move-right-from-r154
		:precondition
			(and 
				(in r154)
			)
		:effect
			(and
				(not (search_again))
				(not (in r154))
				(in r155)
			)
	)


	(:action move-left-from-r154
		:precondition
			(and 
				(in r154)
			)
		:effect
			(and
				(not (search_again))
				(not (in r154))
				(in r153)
			)
	)

	(:action move-right-from-r155
		:precondition
			(and 
				(in r155)
			)
		:effect
			(and
				(not (search_again))
				(not (in r155))
				(in r156)
			)
	)


	(:action move-left-from-r155
		:precondition
			(and 
				(in r155)
			)
		:effect
			(and
				(not (search_again))
				(not (in r155))
				(in r154)
			)
	)

	(:action move-right-from-r156
		:precondition
			(and 
				(in r156)
			)
		:effect
			(and
				(not (search_again))
				(not (in r156))
				(in r157)
			)
	)


	(:action move-left-from-r156
		:precondition
			(and 
				(in r156)
			)
		:effect
			(and
				(not (search_again))
				(not (in r156))
				(in r155)
			)
	)

	(:action move-right-from-r157
		:precondition
			(and 
				(in r157)
			)
		:effect
			(and
				(not (search_again))
				(not (in r157))
				(in r158)
			)
	)


	(:action move-left-from-r157
		:precondition
			(and 
				(in r157)
			)
		:effect
			(and
				(not (search_again))
				(not (in r157))
				(in r156)
			)
	)

	(:action move-right-from-r158
		:precondition
			(and 
				(in r158)
			)
		:effect
			(and
				(not (search_again))
				(not (in r158))
				(in r159)
			)
	)


	(:action move-left-from-r158
		:precondition
			(and 
				(in r158)
			)
		:effect
			(and
				(not (search_again))
				(not (in r158))
				(in r157)
			)
	)

	(:action move-right-from-r159
		:precondition
			(and 
				(in r159)
			)
		:effect
			(and
				(not (search_again))
				(not (in r159))
				(in r160)
			)
	)


	(:action move-left-from-r159
		:precondition
			(and 
				(in r159)
			)
		:effect
			(and
				(not (search_again))
				(not (in r159))
				(in r158)
			)
	)

	(:action move-right-from-r160
		:precondition
			(and 
				(in r160)
			)
		:effect
			(and
				(not (search_again))
				(not (in r160))
				(in r161)
			)
	)


	(:action move-left-from-r160
		:precondition
			(and 
				(in r160)
			)
		:effect
			(and
				(not (search_again))
				(not (in r160))
				(in r159)
			)
	)

	(:action move-right-from-r161
		:precondition
			(and 
				(in r161)
			)
		:effect
			(and
				(not (search_again))
				(not (in r161))
				(in r162)
			)
	)


	(:action move-left-from-r161
		:precondition
			(and 
				(in r161)
			)
		:effect
			(and
				(not (search_again))
				(not (in r161))
				(in r160)
			)
	)

	(:action move-right-from-r162
		:precondition
			(and 
				(in r162)
			)
		:effect
			(and
				(not (search_again))
				(not (in r162))
				(in r163)
			)
	)


	(:action move-left-from-r162
		:precondition
			(and 
				(in r162)
			)
		:effect
			(and
				(not (search_again))
				(not (in r162))
				(in r161)
			)
	)

	(:action move-right-from-r163
		:precondition
			(and 
				(in r163)
			)
		:effect
			(and
				(not (search_again))
				(not (in r163))
				(in r164)
			)
	)


	(:action move-left-from-r163
		:precondition
			(and 
				(in r163)
			)
		:effect
			(and
				(not (search_again))
				(not (in r163))
				(in r162)
			)
	)

	(:action move-right-from-r164
		:precondition
			(and 
				(in r164)
			)
		:effect
			(and
				(not (search_again))
				(not (in r164))
				(in r165)
			)
	)


	(:action move-left-from-r164
		:precondition
			(and 
				(in r164)
			)
		:effect
			(and
				(not (search_again))
				(not (in r164))
				(in r163)
			)
	)

	(:action move-right-from-r165
		:precondition
			(and 
				(in r165)
			)
		:effect
			(and
				(not (search_again))
				(not (in r165))
				(in r166)
			)
	)


	(:action move-left-from-r165
		:precondition
			(and 
				(in r165)
			)
		:effect
			(and
				(not (search_again))
				(not (in r165))
				(in r164)
			)
	)

	(:action move-right-from-r166
		:precondition
			(and 
				(in r166)
			)
		:effect
			(and
				(not (search_again))
				(not (in r166))
				(in r167)
			)
	)


	(:action move-left-from-r166
		:precondition
			(and 
				(in r166)
			)
		:effect
			(and
				(not (search_again))
				(not (in r166))
				(in r165)
			)
	)

	(:action move-right-from-r167
		:precondition
			(and 
				(in r167)
			)
		:effect
			(and
				(not (search_again))
				(not (in r167))
				(in r168)
			)
	)


	(:action move-left-from-r167
		:precondition
			(and 
				(in r167)
			)
		:effect
			(and
				(not (search_again))
				(not (in r167))
				(in r166)
			)
	)

	(:action move-right-from-r168
		:precondition
			(and 
				(in r168)
			)
		:effect
			(and
				(not (search_again))
				(not (in r168))
				(in r169)
			)
	)


	(:action move-left-from-r168
		:precondition
			(and 
				(in r168)
			)
		:effect
			(and
				(not (search_again))
				(not (in r168))
				(in r167)
			)
	)

	(:action move-right-from-r169
		:precondition
			(and 
				(in r169)
			)
		:effect
			(and
				(not (search_again))
				(not (in r169))
				(in r170)
			)
	)


	(:action move-left-from-r169
		:precondition
			(and 
				(in r169)
			)
		:effect
			(and
				(not (search_again))
				(not (in r169))
				(in r168)
			)
	)

	(:action move-right-from-r170
		:precondition
			(and 
				(in r170)
			)
		:effect
			(and
				(not (search_again))
				(not (in r170))
				(in r171)
			)
	)


	(:action move-left-from-r170
		:precondition
			(and 
				(in r170)
			)
		:effect
			(and
				(not (search_again))
				(not (in r170))
				(in r169)
			)
	)

	(:action move-right-from-r171
		:precondition
			(and 
				(in r171)
			)
		:effect
			(and
				(not (search_again))
				(not (in r171))
				(in r172)
			)
	)


	(:action move-left-from-r171
		:precondition
			(and 
				(in r171)
			)
		:effect
			(and
				(not (search_again))
				(not (in r171))
				(in r170)
			)
	)

	(:action move-right-from-r172
		:precondition
			(and 
				(in r172)
			)
		:effect
			(and
				(not (search_again))
				(not (in r172))
				(in r173)
			)
	)


	(:action move-left-from-r172
		:precondition
			(and 
				(in r172)
			)
		:effect
			(and
				(not (search_again))
				(not (in r172))
				(in r171)
			)
	)

	(:action move-right-from-r173
		:precondition
			(and 
				(in r173)
			)
		:effect
			(and
				(not (search_again))
				(not (in r173))
				(in r174)
			)
	)


	(:action move-left-from-r173
		:precondition
			(and 
				(in r173)
			)
		:effect
			(and
				(not (search_again))
				(not (in r173))
				(in r172)
			)
	)

	(:action move-right-from-r174
		:precondition
			(and 
				(in r174)
			)
		:effect
			(and
				(not (search_again))
				(not (in r174))
				(in r175)
			)
	)


	(:action move-left-from-r174
		:precondition
			(and 
				(in r174)
			)
		:effect
			(and
				(not (search_again))
				(not (in r174))
				(in r173)
			)
	)

	(:action move-right-from-r175
		:precondition
			(and 
				(in r175)
			)
		:effect
			(and
				(not (search_again))
				(not (in r175))
				(in r176)
			)
	)


	(:action move-left-from-r175
		:precondition
			(and 
				(in r175)
			)
		:effect
			(and
				(not (search_again))
				(not (in r175))
				(in r174)
			)
	)

	(:action move-right-from-r176
		:precondition
			(and 
				(in r176)
			)
		:effect
			(and
				(not (search_again))
				(not (in r176))
				(in r177)
			)
	)


	(:action move-left-from-r176
		:precondition
			(and 
				(in r176)
			)
		:effect
			(and
				(not (search_again))
				(not (in r176))
				(in r175)
			)
	)

	(:action move-right-from-r177
		:precondition
			(and 
				(in r177)
			)
		:effect
			(and
				(not (search_again))
				(not (in r177))
				(in r178)
			)
	)


	(:action move-left-from-r177
		:precondition
			(and 
				(in r177)
			)
		:effect
			(and
				(not (search_again))
				(not (in r177))
				(in r176)
			)
	)

	(:action move-right-from-r178
		:precondition
			(and 
				(in r178)
			)
		:effect
			(and
				(not (search_again))
				(not (in r178))
				(in r179)
			)
	)


	(:action move-left-from-r178
		:precondition
			(and 
				(in r178)
			)
		:effect
			(and
				(not (search_again))
				(not (in r178))
				(in r177)
			)
	)

	(:action move-right-from-r179
		:precondition
			(and 
				(in r179)
			)
		:effect
			(and
				(not (search_again))
				(not (in r179))
				(in r180)
			)
	)


	(:action move-left-from-r179
		:precondition
			(and 
				(in r179)
			)
		:effect
			(and
				(not (search_again))
				(not (in r179))
				(in r178)
			)
	)

	(:action move-right-from-r180
		:precondition
			(and 
				(in r180)
			)
		:effect
			(and
				(not (search_again))
				(not (in r180))
				(in r181)
			)
	)


	(:action move-left-from-r180
		:precondition
			(and 
				(in r180)
			)
		:effect
			(and
				(not (search_again))
				(not (in r180))
				(in r179)
			)
	)

	(:action move-right-from-r181
		:precondition
			(and 
				(in r181)
			)
		:effect
			(and
				(not (search_again))
				(not (in r181))
				(in r182)
			)
	)


	(:action move-left-from-r181
		:precondition
			(and 
				(in r181)
			)
		:effect
			(and
				(not (search_again))
				(not (in r181))
				(in r180)
			)
	)

	(:action move-right-from-r182
		:precondition
			(and 
				(in r182)
			)
		:effect
			(and
				(not (search_again))
				(not (in r182))
				(in r183)
			)
	)


	(:action move-left-from-r182
		:precondition
			(and 
				(in r182)
			)
		:effect
			(and
				(not (search_again))
				(not (in r182))
				(in r181)
			)
	)

	(:action move-right-from-r183
		:precondition
			(and 
				(in r183)
			)
		:effect
			(and
				(not (search_again))
				(not (in r183))
				(in r184)
			)
	)


	(:action move-left-from-r183
		:precondition
			(and 
				(in r183)
			)
		:effect
			(and
				(not (search_again))
				(not (in r183))
				(in r182)
			)
	)

	(:action move-right-from-r184
		:precondition
			(and 
				(in r184)
			)
		:effect
			(and
				(not (search_again))
				(not (in r184))
				(in r185)
			)
	)


	(:action move-left-from-r184
		:precondition
			(and 
				(in r184)
			)
		:effect
			(and
				(not (search_again))
				(not (in r184))
				(in r183)
			)
	)

	(:action move-right-from-r185
		:precondition
			(and 
				(in r185)
			)
		:effect
			(and
				(not (search_again))
				(not (in r185))
				(in r186)
			)
	)


	(:action move-left-from-r185
		:precondition
			(and 
				(in r185)
			)
		:effect
			(and
				(not (search_again))
				(not (in r185))
				(in r184)
			)
	)

	(:action move-right-from-r186
		:precondition
			(and 
				(in r186)
			)
		:effect
			(and
				(not (search_again))
				(not (in r186))
				(in r187)
			)
	)


	(:action move-left-from-r186
		:precondition
			(and 
				(in r186)
			)
		:effect
			(and
				(not (search_again))
				(not (in r186))
				(in r185)
			)
	)

	(:action move-right-from-r187
		:precondition
			(and 
				(in r187)
			)
		:effect
			(and
				(not (search_again))
				(not (in r187))
				(in r188)
			)
	)


	(:action move-left-from-r187
		:precondition
			(and 
				(in r187)
			)
		:effect
			(and
				(not (search_again))
				(not (in r187))
				(in r186)
			)
	)

	(:action move-right-from-r188
		:precondition
			(and 
				(in r188)
			)
		:effect
			(and
				(not (search_again))
				(not (in r188))
				(in r189)
			)
	)


	(:action move-left-from-r188
		:precondition
			(and 
				(in r188)
			)
		:effect
			(and
				(not (search_again))
				(not (in r188))
				(in r187)
			)
	)

	(:action move-right-from-r189
		:precondition
			(and 
				(in r189)
			)
		:effect
			(and
				(not (search_again))
				(not (in r189))
				(in r190)
			)
	)


	(:action move-left-from-r189
		:precondition
			(and 
				(in r189)
			)
		:effect
			(and
				(not (search_again))
				(not (in r189))
				(in r188)
			)
	)

	(:action move-right-from-r190
		:precondition
			(and 
				(in r190)
			)
		:effect
			(and
				(not (search_again))
				(not (in r190))
				(in r191)
			)
	)


	(:action move-left-from-r190
		:precondition
			(and 
				(in r190)
			)
		:effect
			(and
				(not (search_again))
				(not (in r190))
				(in r189)
			)
	)

	(:action move-right-from-r191
		:precondition
			(and 
				(in r191)
			)
		:effect
			(and
				(not (search_again))
				(not (in r191))
				(in r192)
			)
	)


	(:action move-left-from-r191
		:precondition
			(and 
				(in r191)
			)
		:effect
			(and
				(not (search_again))
				(not (in r191))
				(in r190)
			)
	)

	(:action move-right-from-r192
		:precondition
			(and 
				(in r192)
			)
		:effect
			(and
				(not (search_again))
				(not (in r192))
				(in r193)
			)
	)


	(:action move-left-from-r192
		:precondition
			(and 
				(in r192)
			)
		:effect
			(and
				(not (search_again))
				(not (in r192))
				(in r191)
			)
	)

	(:action move-right-from-r193
		:precondition
			(and 
				(in r193)
			)
		:effect
			(and
				(not (search_again))
				(not (in r193))
				(in r194)
			)
	)


	(:action move-left-from-r193
		:precondition
			(and 
				(in r193)
			)
		:effect
			(and
				(not (search_again))
				(not (in r193))
				(in r192)
			)
	)

	(:action move-right-from-r194
		:precondition
			(and 
				(in r194)
			)
		:effect
			(and
				(not (search_again))
				(not (in r194))
				(in r195)
			)
	)


	(:action move-left-from-r194
		:precondition
			(and 
				(in r194)
			)
		:effect
			(and
				(not (search_again))
				(not (in r194))
				(in r193)
			)
	)

	(:action move-right-from-r195
		:precondition
			(and 
				(in r195)
			)
		:effect
			(and
				(not (search_again))
				(not (in r195))
				(in r196)
			)
	)


	(:action move-left-from-r195
		:precondition
			(and 
				(in r195)
			)
		:effect
			(and
				(not (search_again))
				(not (in r195))
				(in r194)
			)
	)

	(:action move-right-from-r196
		:precondition
			(and 
				(in r196)
			)
		:effect
			(and
				(not (search_again))
				(not (in r196))
				(in r197)
			)
	)


	(:action move-left-from-r196
		:precondition
			(and 
				(in r196)
			)
		:effect
			(and
				(not (search_again))
				(not (in r196))
				(in r195)
			)
	)

	(:action move-right-from-r197
		:precondition
			(and 
				(in r197)
			)
		:effect
			(and
				(not (search_again))
				(not (in r197))
				(in r198)
			)
	)


	(:action move-left-from-r197
		:precondition
			(and 
				(in r197)
			)
		:effect
			(and
				(not (search_again))
				(not (in r197))
				(in r196)
			)
	)

	(:action move-right-from-r198
		:precondition
			(and 
				(in r198)
			)
		:effect
			(and
				(not (search_again))
				(not (in r198))
				(in r199)
			)
	)


	(:action move-left-from-r198
		:precondition
			(and 
				(in r198)
			)
		:effect
			(and
				(not (search_again))
				(not (in r198))
				(in r197)
			)
	)

	(:action move-right-from-r199
		:precondition
			(and 
				(in r199)
			)
		:effect
			(and
				(not (search_again))
				(not (in r199))
				(in r200)
			)
	)


	(:action move-left-from-r199
		:precondition
			(and 
				(in r199)
			)
		:effect
			(and
				(not (search_again))
				(not (in r199))
				(in r198)
			)
	)

	(:action move-right-from-r200
		:precondition
			(and 
				(in r200)
			)
		:effect
			(and
				(not (search_again))
				(not (in r200))
				(in r201)
			)
	)


	(:action move-left-from-r200
		:precondition
			(and 
				(in r200)
			)
		:effect
			(and
				(not (search_again))
				(not (in r200))
				(in r199)
			)
	)

	(:action move-right-from-r201
		:precondition
			(and 
				(in r201)
			)
		:effect
			(and
				(not (search_again))
				(not (in r201))
				(in r202)
			)
	)


	(:action move-left-from-r201
		:precondition
			(and 
				(in r201)
			)
		:effect
			(and
				(not (search_again))
				(not (in r201))
				(in r200)
			)
	)

	(:action move-right-from-r202
		:precondition
			(and 
				(in r202)
			)
		:effect
			(and
				(not (search_again))
				(not (in r202))
				(in r203)
			)
	)


	(:action move-left-from-r202
		:precondition
			(and 
				(in r202)
			)
		:effect
			(and
				(not (search_again))
				(not (in r202))
				(in r201)
			)
	)

	(:action move-right-from-r203
		:precondition
			(and 
				(in r203)
			)
		:effect
			(and
				(not (search_again))
				(not (in r203))
				(in r204)
			)
	)


	(:action move-left-from-r203
		:precondition
			(and 
				(in r203)
			)
		:effect
			(and
				(not (search_again))
				(not (in r203))
				(in r202)
			)
	)

	(:action move-right-from-r204
		:precondition
			(and 
				(in r204)
			)
		:effect
			(and
				(not (search_again))
				(not (in r204))
				(in r205)
			)
	)


	(:action move-left-from-r204
		:precondition
			(and 
				(in r204)
			)
		:effect
			(and
				(not (search_again))
				(not (in r204))
				(in r203)
			)
	)

	(:action move-right-from-r205
		:precondition
			(and 
				(in r205)
			)
		:effect
			(and
				(not (search_again))
				(not (in r205))
				(in r206)
			)
	)


	(:action move-left-from-r205
		:precondition
			(and 
				(in r205)
			)
		:effect
			(and
				(not (search_again))
				(not (in r205))
				(in r204)
			)
	)

	(:action move-right-from-r206
		:precondition
			(and 
				(in r206)
			)
		:effect
			(and
				(not (search_again))
				(not (in r206))
				(in r207)
			)
	)


	(:action move-left-from-r206
		:precondition
			(and 
				(in r206)
			)
		:effect
			(and
				(not (search_again))
				(not (in r206))
				(in r205)
			)
	)

	(:action move-right-from-r207
		:precondition
			(and 
				(in r207)
			)
		:effect
			(and
				(not (search_again))
				(not (in r207))
				(in r208)
			)
	)


	(:action move-left-from-r207
		:precondition
			(and 
				(in r207)
			)
		:effect
			(and
				(not (search_again))
				(not (in r207))
				(in r206)
			)
	)

	(:action move-right-from-r208
		:precondition
			(and 
				(in r208)
			)
		:effect
			(and
				(not (search_again))
				(not (in r208))
				(in r209)
			)
	)


	(:action move-left-from-r208
		:precondition
			(and 
				(in r208)
			)
		:effect
			(and
				(not (search_again))
				(not (in r208))
				(in r207)
			)
	)

	(:action move-right-from-r209
		:precondition
			(and 
				(in r209)
			)
		:effect
			(and
				(not (search_again))
				(not (in r209))
				(in r210)
			)
	)


	(:action move-left-from-r209
		:precondition
			(and 
				(in r209)
			)
		:effect
			(and
				(not (search_again))
				(not (in r209))
				(in r208)
			)
	)

	(:action move-right-from-r210
		:precondition
			(and 
				(in r210)
			)
		:effect
			(and
				(not (search_again))
				(not (in r210))
				(in r211)
			)
	)


	(:action move-left-from-r210
		:precondition
			(and 
				(in r210)
			)
		:effect
			(and
				(not (search_again))
				(not (in r210))
				(in r209)
			)
	)

	(:action move-right-from-r211
		:precondition
			(and 
				(in r211)
			)
		:effect
			(and
				(not (search_again))
				(not (in r211))
				(in r212)
			)
	)


	(:action move-left-from-r211
		:precondition
			(and 
				(in r211)
			)
		:effect
			(and
				(not (search_again))
				(not (in r211))
				(in r210)
			)
	)

	(:action move-right-from-r212
		:precondition
			(and 
				(in r212)
			)
		:effect
			(and
				(not (search_again))
				(not (in r212))
				(in r213)
			)
	)


	(:action move-left-from-r212
		:precondition
			(and 
				(in r212)
			)
		:effect
			(and
				(not (search_again))
				(not (in r212))
				(in r211)
			)
	)

	(:action move-right-from-r213
		:precondition
			(and 
				(in r213)
			)
		:effect
			(and
				(not (search_again))
				(not (in r213))
				(in r214)
			)
	)


	(:action move-left-from-r213
		:precondition
			(and 
				(in r213)
			)
		:effect
			(and
				(not (search_again))
				(not (in r213))
				(in r212)
			)
	)

	(:action move-right-from-r214
		:precondition
			(and 
				(in r214)
			)
		:effect
			(and
				(not (search_again))
				(not (in r214))
				(in r215)
			)
	)


	(:action move-left-from-r214
		:precondition
			(and 
				(in r214)
			)
		:effect
			(and
				(not (search_again))
				(not (in r214))
				(in r213)
			)
	)

	(:action move-right-from-r215
		:precondition
			(and 
				(in r215)
			)
		:effect
			(and
				(not (search_again))
				(not (in r215))
				(in r216)
			)
	)


	(:action move-left-from-r215
		:precondition
			(and 
				(in r215)
			)
		:effect
			(and
				(not (search_again))
				(not (in r215))
				(in r214)
			)
	)

	(:action move-right-from-r216
		:precondition
			(and 
				(in r216)
			)
		:effect
			(and
				(not (search_again))
				(not (in r216))
				(in r217)
			)
	)


	(:action move-left-from-r216
		:precondition
			(and 
				(in r216)
			)
		:effect
			(and
				(not (search_again))
				(not (in r216))
				(in r215)
			)
	)

	(:action move-right-from-r217
		:precondition
			(and 
				(in r217)
			)
		:effect
			(and
				(not (search_again))
				(not (in r217))
				(in r218)
			)
	)


	(:action move-left-from-r217
		:precondition
			(and 
				(in r217)
			)
		:effect
			(and
				(not (search_again))
				(not (in r217))
				(in r216)
			)
	)

	(:action move-right-from-r218
		:precondition
			(and 
				(in r218)
			)
		:effect
			(and
				(not (search_again))
				(not (in r218))
				(in r219)
			)
	)


	(:action move-left-from-r218
		:precondition
			(and 
				(in r218)
			)
		:effect
			(and
				(not (search_again))
				(not (in r218))
				(in r217)
			)
	)

	(:action move-right-from-r219
		:precondition
			(and 
				(in r219)
			)
		:effect
			(and
				(not (search_again))
				(not (in r219))
				(in r220)
			)
	)


	(:action move-left-from-r219
		:precondition
			(and 
				(in r219)
			)
		:effect
			(and
				(not (search_again))
				(not (in r219))
				(in r218)
			)
	)

	(:action move-right-from-r220
		:precondition
			(and 
				(in r220)
			)
		:effect
			(and
				(not (search_again))
				(not (in r220))
				(in r221)
			)
	)


	(:action move-left-from-r220
		:precondition
			(and 
				(in r220)
			)
		:effect
			(and
				(not (search_again))
				(not (in r220))
				(in r219)
			)
	)

	(:action move-right-from-r221
		:precondition
			(and 
				(in r221)
			)
		:effect
			(and
				(not (search_again))
				(not (in r221))
				(in r222)
			)
	)


	(:action move-left-from-r221
		:precondition
			(and 
				(in r221)
			)
		:effect
			(and
				(not (search_again))
				(not (in r221))
				(in r220)
			)
	)

	(:action move-right-from-r222
		:precondition
			(and 
				(in r222)
			)
		:effect
			(and
				(not (search_again))
				(not (in r222))
				(in r223)
			)
	)


	(:action move-left-from-r222
		:precondition
			(and 
				(in r222)
			)
		:effect
			(and
				(not (search_again))
				(not (in r222))
				(in r221)
			)
	)

	(:action move-right-from-r223
		:precondition
			(and 
				(in r223)
			)
		:effect
			(and
				(not (search_again))
				(not (in r223))
				(in r224)
			)
	)


	(:action move-left-from-r223
		:precondition
			(and 
				(in r223)
			)
		:effect
			(and
				(not (search_again))
				(not (in r223))
				(in r222)
			)
	)

	(:action move-right-from-r224
		:precondition
			(and 
				(in r224)
			)
		:effect
			(and
				(not (search_again))
				(not (in r224))
				(in r225)
			)
	)


	(:action move-left-from-r224
		:precondition
			(and 
				(in r224)
			)
		:effect
			(and
				(not (search_again))
				(not (in r224))
				(in r223)
			)
	)

	(:action move-right-from-r225
		:precondition
			(and 
				(in r225)
			)
		:effect
			(and
				(not (search_again))
				(not (in r225))
				(in r226)
			)
	)


	(:action move-left-from-r225
		:precondition
			(and 
				(in r225)
			)
		:effect
			(and
				(not (search_again))
				(not (in r225))
				(in r224)
			)
	)

	(:action move-right-from-r226
		:precondition
			(and 
				(in r226)
			)
		:effect
			(and
				(not (search_again))
				(not (in r226))
				(in r227)
			)
	)


	(:action move-left-from-r226
		:precondition
			(and 
				(in r226)
			)
		:effect
			(and
				(not (search_again))
				(not (in r226))
				(in r225)
			)
	)

	(:action move-right-from-r227
		:precondition
			(and 
				(in r227)
			)
		:effect
			(and
				(not (search_again))
				(not (in r227))
				(in r228)
			)
	)


	(:action move-left-from-r227
		:precondition
			(and 
				(in r227)
			)
		:effect
			(and
				(not (search_again))
				(not (in r227))
				(in r226)
			)
	)

	(:action move-right-from-r228
		:precondition
			(and 
				(in r228)
			)
		:effect
			(and
				(not (search_again))
				(not (in r228))
				(in r229)
			)
	)


	(:action move-left-from-r228
		:precondition
			(and 
				(in r228)
			)
		:effect
			(and
				(not (search_again))
				(not (in r228))
				(in r227)
			)
	)

	(:action move-right-from-r229
		:precondition
			(and 
				(in r229)
			)
		:effect
			(and
				(not (search_again))
				(not (in r229))
				(in r230)
			)
	)


	(:action move-left-from-r229
		:precondition
			(and 
				(in r229)
			)
		:effect
			(and
				(not (search_again))
				(not (in r229))
				(in r228)
			)
	)

	(:action move-right-from-r230
		:precondition
			(and 
				(in r230)
			)
		:effect
			(and
				(not (search_again))
				(not (in r230))
				(in r231)
			)
	)


	(:action move-left-from-r230
		:precondition
			(and 
				(in r230)
			)
		:effect
			(and
				(not (search_again))
				(not (in r230))
				(in r229)
			)
	)

	(:action move-right-from-r231
		:precondition
			(and 
				(in r231)
			)
		:effect
			(and
				(not (search_again))
				(not (in r231))
				(in r232)
			)
	)


	(:action move-left-from-r231
		:precondition
			(and 
				(in r231)
			)
		:effect
			(and
				(not (search_again))
				(not (in r231))
				(in r230)
			)
	)

	(:action move-right-from-r232
		:precondition
			(and 
				(in r232)
			)
		:effect
			(and
				(not (search_again))
				(not (in r232))
				(in r233)
			)
	)


	(:action move-left-from-r232
		:precondition
			(and 
				(in r232)
			)
		:effect
			(and
				(not (search_again))
				(not (in r232))
				(in r231)
			)
	)

	(:action move-right-from-r233
		:precondition
			(and 
				(in r233)
			)
		:effect
			(and
				(not (search_again))
				(not (in r233))
				(in r234)
			)
	)


	(:action move-left-from-r233
		:precondition
			(and 
				(in r233)
			)
		:effect
			(and
				(not (search_again))
				(not (in r233))
				(in r232)
			)
	)

	(:action move-right-from-r234
		:precondition
			(and 
				(in r234)
			)
		:effect
			(and
				(not (search_again))
				(not (in r234))
				(in r235)
			)
	)


	(:action move-left-from-r234
		:precondition
			(and 
				(in r234)
			)
		:effect
			(and
				(not (search_again))
				(not (in r234))
				(in r233)
			)
	)

	(:action move-right-from-r235
		:precondition
			(and 
				(in r235)
			)
		:effect
			(and
				(not (search_again))
				(not (in r235))
				(in r236)
			)
	)


	(:action move-left-from-r235
		:precondition
			(and 
				(in r235)
			)
		:effect
			(and
				(not (search_again))
				(not (in r235))
				(in r234)
			)
	)

	(:action move-right-from-r236
		:precondition
			(and 
				(in r236)
			)
		:effect
			(and
				(not (search_again))
				(not (in r236))
				(in r237)
			)
	)


	(:action move-left-from-r236
		:precondition
			(and 
				(in r236)
			)
		:effect
			(and
				(not (search_again))
				(not (in r236))
				(in r235)
			)
	)

	(:action move-right-from-r237
		:precondition
			(and 
				(in r237)
			)
		:effect
			(and
				(not (search_again))
				(not (in r237))
				(in r238)
			)
	)


	(:action move-left-from-r237
		:precondition
			(and 
				(in r237)
			)
		:effect
			(and
				(not (search_again))
				(not (in r237))
				(in r236)
			)
	)

	(:action move-right-from-r238
		:precondition
			(and 
				(in r238)
			)
		:effect
			(and
				(not (search_again))
				(not (in r238))
				(in r239)
			)
	)


	(:action move-left-from-r238
		:precondition
			(and 
				(in r238)
			)
		:effect
			(and
				(not (search_again))
				(not (in r238))
				(in r237)
			)
	)

	(:action move-right-from-r239
		:precondition
			(and 
				(in r239)
			)
		:effect
			(and
				(not (search_again))
				(not (in r239))
				(in r240)
			)
	)


	(:action move-left-from-r239
		:precondition
			(and 
				(in r239)
			)
		:effect
			(and
				(not (search_again))
				(not (in r239))
				(in r238)
			)
	)

	(:action move-right-from-r240
		:precondition
			(and 
				(in r240)
			)
		:effect
			(and
				(not (search_again))
				(not (in r240))
				(in r241)
			)
	)


	(:action move-left-from-r240
		:precondition
			(and 
				(in r240)
			)
		:effect
			(and
				(not (search_again))
				(not (in r240))
				(in r239)
			)
	)

	(:action move-right-from-r241
		:precondition
			(and 
				(in r241)
			)
		:effect
			(and
				(not (search_again))
				(not (in r241))
				(in r242)
			)
	)


	(:action move-left-from-r241
		:precondition
			(and 
				(in r241)
			)
		:effect
			(and
				(not (search_again))
				(not (in r241))
				(in r240)
			)
	)

	(:action move-right-from-r242
		:precondition
			(and 
				(in r242)
			)
		:effect
			(and
				(not (search_again))
				(not (in r242))
				(in r243)
			)
	)


	(:action move-left-from-r242
		:precondition
			(and 
				(in r242)
			)
		:effect
			(and
				(not (search_again))
				(not (in r242))
				(in r241)
			)
	)

	(:action move-right-from-r243
		:precondition
			(and 
				(in r243)
			)
		:effect
			(and
				(not (search_again))
				(not (in r243))
				(in r244)
			)
	)


	(:action move-left-from-r243
		:precondition
			(and 
				(in r243)
			)
		:effect
			(and
				(not (search_again))
				(not (in r243))
				(in r242)
			)
	)

	(:action move-right-from-r244
		:precondition
			(and 
				(in r244)
			)
		:effect
			(and
				(not (search_again))
				(not (in r244))
				(in r245)
			)
	)


	(:action move-left-from-r244
		:precondition
			(and 
				(in r244)
			)
		:effect
			(and
				(not (search_again))
				(not (in r244))
				(in r243)
			)
	)

	(:action move-right-from-r245
		:precondition
			(and 
				(in r245)
			)
		:effect
			(and
				(not (search_again))
				(not (in r245))
				(in r246)
			)
	)


	(:action move-left-from-r245
		:precondition
			(and 
				(in r245)
			)
		:effect
			(and
				(not (search_again))
				(not (in r245))
				(in r244)
			)
	)

	(:action move-right-from-r246
		:precondition
			(and 
				(in r246)
			)
		:effect
			(and
				(not (search_again))
				(not (in r246))
				(in r247)
			)
	)


	(:action move-left-from-r246
		:precondition
			(and 
				(in r246)
			)
		:effect
			(and
				(not (search_again))
				(not (in r246))
				(in r245)
			)
	)

	(:action move-right-from-r247
		:precondition
			(and 
				(in r247)
			)
		:effect
			(and
				(not (search_again))
				(not (in r247))
				(in r248)
			)
	)


	(:action move-left-from-r247
		:precondition
			(and 
				(in r247)
			)
		:effect
			(and
				(not (search_again))
				(not (in r247))
				(in r246)
			)
	)

	(:action move-right-from-r248
		:precondition
			(and 
				(in r248)
			)
		:effect
			(and
				(not (search_again))
				(not (in r248))
				(in r249)
			)
	)


	(:action move-left-from-r248
		:precondition
			(and 
				(in r248)
			)
		:effect
			(and
				(not (search_again))
				(not (in r248))
				(in r247)
			)
	)

	(:action move-right-from-r249
		:precondition
			(and 
				(in r249)
			)
		:effect
			(and
				(not (search_again))
				(not (in r249))
				(in r250)
			)
	)


	(:action move-left-from-r249
		:precondition
			(and 
				(in r249)
			)
		:effect
			(and
				(not (search_again))
				(not (in r249))
				(in r248)
			)
	)

	(:action move-right-from-r250
		:precondition
			(and 
				(in r250)
			)
		:effect
			(and
				(not (search_again))
				(not (in r250))
				(in r251)
			)
	)


	(:action move-left-from-r250
		:precondition
			(and 
				(in r250)
			)
		:effect
			(and
				(not (search_again))
				(not (in r250))
				(in r249)
			)
	)

	(:action move-right-from-r251
		:precondition
			(and 
				(in r251)
			)
		:effect
			(and
				(not (search_again))
				(not (in r251))
				(in r252)
			)
	)


	(:action move-left-from-r251
		:precondition
			(and 
				(in r251)
			)
		:effect
			(and
				(not (search_again))
				(not (in r251))
				(in r250)
			)
	)

	(:action move-right-from-r252
		:precondition
			(and 
				(in r252)
			)
		:effect
			(and
				(not (search_again))
				(not (in r252))
				(in r253)
			)
	)


	(:action move-left-from-r252
		:precondition
			(and 
				(in r252)
			)
		:effect
			(and
				(not (search_again))
				(not (in r252))
				(in r251)
			)
	)

	(:action move-right-from-r253
		:precondition
			(and 
				(in r253)
			)
		:effect
			(and
				(not (search_again))
				(not (in r253))
				(in r254)
			)
	)


	(:action move-left-from-r253
		:precondition
			(and 
				(in r253)
			)
		:effect
			(and
				(not (search_again))
				(not (in r253))
				(in r252)
			)
	)

	(:action move-right-from-r254
		:precondition
			(and 
				(in r254)
			)
		:effect
			(and
				(not (search_again))
				(not (in r254))
				(in r255)
			)
	)


	(:action move-left-from-r254
		:precondition
			(and 
				(in r254)
			)
		:effect
			(and
				(not (search_again))
				(not (in r254))
				(in r253)
			)
	)

	(:action move-right-from-r255
		:precondition
			(and 
				(in r255)
			)
		:effect
			(and
				(not (search_again))
				(not (in r255))
				(in r256)
			)
	)


	(:action move-left-from-r255
		:precondition
			(and 
				(in r255)
			)
		:effect
			(and
				(not (search_again))
				(not (in r255))
				(in r254)
			)
	)

	(:action move-right-from-r256
		:precondition
			(and 
				(in r256)
			)
		:effect
			(and
				(not (search_again))
				(not (in r256))
				(in r257)
			)
	)


	(:action move-left-from-r256
		:precondition
			(and 
				(in r256)
			)
		:effect
			(and
				(not (search_again))
				(not (in r256))
				(in r255)
			)
	)

	(:action move-right-from-r257
		:precondition
			(and 
				(in r257)
			)
		:effect
			(and
				(not (search_again))
				(not (in r257))
				(in r258)
			)
	)


	(:action move-left-from-r257
		:precondition
			(and 
				(in r257)
			)
		:effect
			(and
				(not (search_again))
				(not (in r257))
				(in r256)
			)
	)

	(:action move-right-from-r258
		:precondition
			(and 
				(in r258)
			)
		:effect
			(and
				(not (search_again))
				(not (in r258))
				(in r259)
			)
	)


	(:action move-left-from-r258
		:precondition
			(and 
				(in r258)
			)
		:effect
			(and
				(not (search_again))
				(not (in r258))
				(in r257)
			)
	)

	(:action move-right-from-r259
		:precondition
			(and 
				(in r259)
			)
		:effect
			(and
				(not (search_again))
				(not (in r259))
				(in r260)
			)
	)


	(:action move-left-from-r259
		:precondition
			(and 
				(in r259)
			)
		:effect
			(and
				(not (search_again))
				(not (in r259))
				(in r258)
			)
	)

	(:action move-right-from-r260
		:precondition
			(and 
				(in r260)
			)
		:effect
			(and
				(not (search_again))
				(not (in r260))
				(in r261)
			)
	)


	(:action move-left-from-r260
		:precondition
			(and 
				(in r260)
			)
		:effect
			(and
				(not (search_again))
				(not (in r260))
				(in r259)
			)
	)

	(:action move-right-from-r261
		:precondition
			(and 
				(in r261)
			)
		:effect
			(and
				(not (search_again))
				(not (in r261))
				(in r262)
			)
	)


	(:action move-left-from-r261
		:precondition
			(and 
				(in r261)
			)
		:effect
			(and
				(not (search_again))
				(not (in r261))
				(in r260)
			)
	)

	(:action move-right-from-r262
		:precondition
			(and 
				(in r262)
			)
		:effect
			(and
				(not (search_again))
				(not (in r262))
				(in r263)
			)
	)


	(:action move-left-from-r262
		:precondition
			(and 
				(in r262)
			)
		:effect
			(and
				(not (search_again))
				(not (in r262))
				(in r261)
			)
	)

	(:action move-right-from-r263
		:precondition
			(and 
				(in r263)
			)
		:effect
			(and
				(not (search_again))
				(not (in r263))
				(in r264)
			)
	)


	(:action move-left-from-r263
		:precondition
			(and 
				(in r263)
			)
		:effect
			(and
				(not (search_again))
				(not (in r263))
				(in r262)
			)
	)

	(:action move-right-from-r264
		:precondition
			(and 
				(in r264)
			)
		:effect
			(and
				(not (search_again))
				(not (in r264))
				(in r265)
			)
	)


	(:action move-left-from-r264
		:precondition
			(and 
				(in r264)
			)
		:effect
			(and
				(not (search_again))
				(not (in r264))
				(in r263)
			)
	)

	(:action move-right-from-r265
		:precondition
			(and 
				(in r265)
			)
		:effect
			(and
				(not (search_again))
				(not (in r265))
				(in r266)
			)
	)


	(:action move-left-from-r265
		:precondition
			(and 
				(in r265)
			)
		:effect
			(and
				(not (search_again))
				(not (in r265))
				(in r264)
			)
	)

	(:action move-right-from-r266
		:precondition
			(and 
				(in r266)
			)
		:effect
			(and
				(not (search_again))
				(not (in r266))
				(in r267)
			)
	)


	(:action move-left-from-r266
		:precondition
			(and 
				(in r266)
			)
		:effect
			(and
				(not (search_again))
				(not (in r266))
				(in r265)
			)
	)

	(:action move-right-from-r267
		:precondition
			(and 
				(in r267)
			)
		:effect
			(and
				(not (search_again))
				(not (in r267))
				(in r268)
			)
	)


	(:action move-left-from-r267
		:precondition
			(and 
				(in r267)
			)
		:effect
			(and
				(not (search_again))
				(not (in r267))
				(in r266)
			)
	)

	(:action move-right-from-r268
		:precondition
			(and 
				(in r268)
			)
		:effect
			(and
				(not (search_again))
				(not (in r268))
				(in r269)
			)
	)


	(:action move-left-from-r268
		:precondition
			(and 
				(in r268)
			)
		:effect
			(and
				(not (search_again))
				(not (in r268))
				(in r267)
			)
	)

	(:action move-right-from-r269
		:precondition
			(and 
				(in r269)
			)
		:effect
			(and
				(not (search_again))
				(not (in r269))
				(in r270)
			)
	)


	(:action move-left-from-r269
		:precondition
			(and 
				(in r269)
			)
		:effect
			(and
				(not (search_again))
				(not (in r269))
				(in r268)
			)
	)

	(:action move-right-from-r270
		:precondition
			(and 
				(in r270)
			)
		:effect
			(and
				(not (search_again))
				(not (in r270))
				(in r271)
			)
	)


	(:action move-left-from-r270
		:precondition
			(and 
				(in r270)
			)
		:effect
			(and
				(not (search_again))
				(not (in r270))
				(in r269)
			)
	)

	(:action move-right-from-r271
		:precondition
			(and 
				(in r271)
			)
		:effect
			(and
				(not (search_again))
				(not (in r271))
				(in r272)
			)
	)


	(:action move-left-from-r271
		:precondition
			(and 
				(in r271)
			)
		:effect
			(and
				(not (search_again))
				(not (in r271))
				(in r270)
			)
	)

	(:action move-right-from-r272
		:precondition
			(and 
				(in r272)
			)
		:effect
			(and
				(not (search_again))
				(not (in r272))
				(in r273)
			)
	)


	(:action move-left-from-r272
		:precondition
			(and 
				(in r272)
			)
		:effect
			(and
				(not (search_again))
				(not (in r272))
				(in r271)
			)
	)

	(:action move-right-from-r273
		:precondition
			(and 
				(in r273)
			)
		:effect
			(and
				(not (search_again))
				(not (in r273))
				(in r274)
			)
	)


	(:action move-left-from-r273
		:precondition
			(and 
				(in r273)
			)
		:effect
			(and
				(not (search_again))
				(not (in r273))
				(in r272)
			)
	)

	(:action move-right-from-r274
		:precondition
			(and 
				(in r274)
			)
		:effect
			(and
				(not (search_again))
				(not (in r274))
				(in r275)
			)
	)


	(:action move-left-from-r274
		:precondition
			(and 
				(in r274)
			)
		:effect
			(and
				(not (search_again))
				(not (in r274))
				(in r273)
			)
	)

	(:action move-right-from-r275
		:precondition
			(and 
				(in r275)
			)
		:effect
			(and
				(not (search_again))
				(not (in r275))
				(in r276)
			)
	)


	(:action move-left-from-r275
		:precondition
			(and 
				(in r275)
			)
		:effect
			(and
				(not (search_again))
				(not (in r275))
				(in r274)
			)
	)

	(:action move-right-from-r276
		:precondition
			(and 
				(in r276)
			)
		:effect
			(and
				(not (search_again))
				(not (in r276))
				(in r277)
			)
	)


	(:action move-left-from-r276
		:precondition
			(and 
				(in r276)
			)
		:effect
			(and
				(not (search_again))
				(not (in r276))
				(in r275)
			)
	)

	(:action move-right-from-r277
		:precondition
			(and 
				(in r277)
			)
		:effect
			(and
				(not (search_again))
				(not (in r277))
				(in r278)
			)
	)


	(:action move-left-from-r277
		:precondition
			(and 
				(in r277)
			)
		:effect
			(and
				(not (search_again))
				(not (in r277))
				(in r276)
			)
	)

	(:action move-right-from-r278
		:precondition
			(and 
				(in r278)
			)
		:effect
			(and
				(not (search_again))
				(not (in r278))
				(in r279)
			)
	)


	(:action move-left-from-r278
		:precondition
			(and 
				(in r278)
			)
		:effect
			(and
				(not (search_again))
				(not (in r278))
				(in r277)
			)
	)

	(:action move-right-from-r279
		:precondition
			(and 
				(in r279)
			)
		:effect
			(and
				(not (search_again))
				(not (in r279))
				(in r280)
			)
	)


	(:action move-left-from-r279
		:precondition
			(and 
				(in r279)
			)
		:effect
			(and
				(not (search_again))
				(not (in r279))
				(in r278)
			)
	)

	(:action move-right-from-r280
		:precondition
			(and 
				(in r280)
			)
		:effect
			(and
				(not (search_again))
				(not (in r280))
				(in r281)
			)
	)


	(:action move-left-from-r280
		:precondition
			(and 
				(in r280)
			)
		:effect
			(and
				(not (search_again))
				(not (in r280))
				(in r279)
			)
	)

	(:action move-right-from-r281
		:precondition
			(and 
				(in r281)
			)
		:effect
			(and
				(not (search_again))
				(not (in r281))
				(in r282)
			)
	)


	(:action move-left-from-r281
		:precondition
			(and 
				(in r281)
			)
		:effect
			(and
				(not (search_again))
				(not (in r281))
				(in r280)
			)
	)

	(:action move-right-from-r282
		:precondition
			(and 
				(in r282)
			)
		:effect
			(and
				(not (search_again))
				(not (in r282))
				(in r283)
			)
	)


	(:action move-left-from-r282
		:precondition
			(and 
				(in r282)
			)
		:effect
			(and
				(not (search_again))
				(not (in r282))
				(in r281)
			)
	)

	(:action move-right-from-r283
		:precondition
			(and 
				(in r283)
			)
		:effect
			(and
				(not (search_again))
				(not (in r283))
				(in r284)
			)
	)


	(:action move-left-from-r283
		:precondition
			(and 
				(in r283)
			)
		:effect
			(and
				(not (search_again))
				(not (in r283))
				(in r282)
			)
	)

	(:action move-right-from-r284
		:precondition
			(and 
				(in r284)
			)
		:effect
			(and
				(not (search_again))
				(not (in r284))
				(in r285)
			)
	)


	(:action move-left-from-r284
		:precondition
			(and 
				(in r284)
			)
		:effect
			(and
				(not (search_again))
				(not (in r284))
				(in r283)
			)
	)

	(:action move-right-from-r285
		:precondition
			(and 
				(in r285)
			)
		:effect
			(and
				(not (search_again))
				(not (in r285))
				(in r286)
			)
	)


	(:action move-left-from-r285
		:precondition
			(and 
				(in r285)
			)
		:effect
			(and
				(not (search_again))
				(not (in r285))
				(in r284)
			)
	)

	(:action move-right-from-r286
		:precondition
			(and 
				(in r286)
			)
		:effect
			(and
				(not (search_again))
				(not (in r286))
				(in r287)
			)
	)


	(:action move-left-from-r286
		:precondition
			(and 
				(in r286)
			)
		:effect
			(and
				(not (search_again))
				(not (in r286))
				(in r285)
			)
	)

	(:action move-right-from-r287
		:precondition
			(and 
				(in r287)
			)
		:effect
			(and
				(not (search_again))
				(not (in r287))
				(in r288)
			)
	)


	(:action move-left-from-r287
		:precondition
			(and 
				(in r287)
			)
		:effect
			(and
				(not (search_again))
				(not (in r287))
				(in r286)
			)
	)

	(:action move-right-from-r288
		:precondition
			(and 
				(in r288)
			)
		:effect
			(and
				(not (search_again))
				(not (in r288))
				(in r289)
			)
	)


	(:action move-left-from-r288
		:precondition
			(and 
				(in r288)
			)
		:effect
			(and
				(not (search_again))
				(not (in r288))
				(in r287)
			)
	)

	(:action move-right-from-r289
		:precondition
			(and 
				(in r289)
			)
		:effect
			(and
				(not (search_again))
				(not (in r289))
				(in r290)
			)
	)


	(:action move-left-from-r289
		:precondition
			(and 
				(in r289)
			)
		:effect
			(and
				(not (search_again))
				(not (in r289))
				(in r288)
			)
	)

	(:action move-right-from-r290
		:precondition
			(and 
				(in r290)
			)
		:effect
			(and
				(not (search_again))
				(not (in r290))
				(in r291)
			)
	)


	(:action move-left-from-r290
		:precondition
			(and 
				(in r290)
			)
		:effect
			(and
				(not (search_again))
				(not (in r290))
				(in r289)
			)
	)

	(:action move-right-from-r291
		:precondition
			(and 
				(in r291)
			)
		:effect
			(and
				(not (search_again))
				(not (in r291))
				(in r292)
			)
	)


	(:action move-left-from-r291
		:precondition
			(and 
				(in r291)
			)
		:effect
			(and
				(not (search_again))
				(not (in r291))
				(in r290)
			)
	)

	(:action move-right-from-r292
		:precondition
			(and 
				(in r292)
			)
		:effect
			(and
				(not (search_again))
				(not (in r292))
				(in r293)
			)
	)


	(:action move-left-from-r292
		:precondition
			(and 
				(in r292)
			)
		:effect
			(and
				(not (search_again))
				(not (in r292))
				(in r291)
			)
	)

	(:action move-right-from-r293
		:precondition
			(and 
				(in r293)
			)
		:effect
			(and
				(not (search_again))
				(not (in r293))
				(in r294)
			)
	)


	(:action move-left-from-r293
		:precondition
			(and 
				(in r293)
			)
		:effect
			(and
				(not (search_again))
				(not (in r293))
				(in r292)
			)
	)

	(:action move-right-from-r294
		:precondition
			(and 
				(in r294)
			)
		:effect
			(and
				(not (search_again))
				(not (in r294))
				(in r295)
			)
	)


	(:action move-left-from-r294
		:precondition
			(and 
				(in r294)
			)
		:effect
			(and
				(not (search_again))
				(not (in r294))
				(in r293)
			)
	)

	(:action move-right-from-r295
		:precondition
			(and 
				(in r295)
			)
		:effect
			(and
				(not (search_again))
				(not (in r295))
				(in r296)
			)
	)


	(:action move-left-from-r295
		:precondition
			(and 
				(in r295)
			)
		:effect
			(and
				(not (search_again))
				(not (in r295))
				(in r294)
			)
	)

	(:action move-right-from-r296
		:precondition
			(and 
				(in r296)
			)
		:effect
			(and
				(not (search_again))
				(not (in r296))
				(in r297)
			)
	)


	(:action move-left-from-r296
		:precondition
			(and 
				(in r296)
			)
		:effect
			(and
				(not (search_again))
				(not (in r296))
				(in r295)
			)
	)

	(:action move-right-from-r297
		:precondition
			(and 
				(in r297)
			)
		:effect
			(and
				(not (search_again))
				(not (in r297))
				(in r298)
			)
	)


	(:action move-left-from-r297
		:precondition
			(and 
				(in r297)
			)
		:effect
			(and
				(not (search_again))
				(not (in r297))
				(in r296)
			)
	)

	(:action move-right-from-r298
		:precondition
			(and 
				(in r298)
			)
		:effect
			(and
				(not (search_again))
				(not (in r298))
				(in r299)
			)
	)


	(:action move-left-from-r298
		:precondition
			(and 
				(in r298)
			)
		:effect
			(and
				(not (search_again))
				(not (in r298))
				(in r297)
			)
	)

	(:action move-right-from-r299
		:precondition
			(and 
				(in r299)
				(not (searched_in r300))
			)
		:effect
			(and
				(not (search_again))
				(not (in r299))
				(in r300)
				(oneof (not (seen)) (seen))
				(searched_in r300)
			)
	)


	(:action move-left-from-r299
		:precondition
			(and 
				(in r299)
			)
		:effect
			(and
				(not (search_again))
				(not (in r299))
				(in r298)
			)
	)

	(:action move-right-from-r300
		:precondition
			(and 
				(in r300)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r300))
				(in r301)
			)
	)


	(:action move-left-from-r300
		:precondition
			(and 
				(in r300)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r300))
				(in r299)
			)
	)

	(:action move-right-from-r301
		:precondition
			(and 
				(in r301)
			)
		:effect
			(and
				(not (search_again))
				(not (in r301))
				(in r302)
			)
	)


	(:action move-left-from-r301
		:precondition
			(and 
				(in r301)
				(not (searched_in r300))
			)
		:effect
			(and
				(not (search_again))
				(not (in r301))
				(in r300)
				(oneof (not (seen)) (seen))
				(searched_in r300)
			)
	)

	(:action move-right-from-r302
		:precondition
			(and 
				(in r302)
			)
		:effect
			(and
				(not (search_again))
				(not (in r302))
				(in r303)
			)
	)


	(:action move-left-from-r302
		:precondition
			(and 
				(in r302)
			)
		:effect
			(and
				(not (search_again))
				(not (in r302))
				(in r301)
			)
	)

	(:action move-right-from-r303
		:precondition
			(and 
				(in r303)
			)
		:effect
			(and
				(not (search_again))
				(not (in r303))
				(in r304)
			)
	)


	(:action move-left-from-r303
		:precondition
			(and 
				(in r303)
			)
		:effect
			(and
				(not (search_again))
				(not (in r303))
				(in r302)
			)
	)

	(:action move-right-from-r304
		:precondition
			(and 
				(in r304)
			)
		:effect
			(and
				(not (search_again))
				(not (in r304))
				(in r305)
			)
	)


	(:action move-left-from-r304
		:precondition
			(and 
				(in r304)
			)
		:effect
			(and
				(not (search_again))
				(not (in r304))
				(in r303)
			)
	)

	(:action move-right-from-r305
		:precondition
			(and 
				(in r305)
			)
		:effect
			(and
				(not (search_again))
				(not (in r305))
				(in r306)
			)
	)


	(:action move-left-from-r305
		:precondition
			(and 
				(in r305)
			)
		:effect
			(and
				(not (search_again))
				(not (in r305))
				(in r304)
			)
	)

	(:action move-right-from-r306
		:precondition
			(and 
				(in r306)
			)
		:effect
			(and
				(not (search_again))
				(not (in r306))
				(in r307)
			)
	)


	(:action move-left-from-r306
		:precondition
			(and 
				(in r306)
			)
		:effect
			(and
				(not (search_again))
				(not (in r306))
				(in r305)
			)
	)

	(:action move-right-from-r307
		:precondition
			(and 
				(in r307)
			)
		:effect
			(and
				(not (search_again))
				(not (in r307))
				(in r308)
			)
	)


	(:action move-left-from-r307
		:precondition
			(and 
				(in r307)
			)
		:effect
			(and
				(not (search_again))
				(not (in r307))
				(in r306)
			)
	)

	(:action move-right-from-r308
		:precondition
			(and 
				(in r308)
			)
		:effect
			(and
				(not (search_again))
				(not (in r308))
				(in r309)
			)
	)


	(:action move-left-from-r308
		:precondition
			(and 
				(in r308)
			)
		:effect
			(and
				(not (search_again))
				(not (in r308))
				(in r307)
			)
	)

	(:action move-right-from-r309
		:precondition
			(and 
				(in r309)
			)
		:effect
			(and
				(not (search_again))
				(not (in r309))
				(in r310)
			)
	)


	(:action move-left-from-r309
		:precondition
			(and 
				(in r309)
			)
		:effect
			(and
				(not (search_again))
				(not (in r309))
				(in r308)
			)
	)

	(:action move-right-from-r310
		:precondition
			(and 
				(in r310)
			)
		:effect
			(and
				(not (search_again))
				(not (in r310))
				(in r311)
			)
	)


	(:action move-left-from-r310
		:precondition
			(and 
				(in r310)
			)
		:effect
			(and
				(not (search_again))
				(not (in r310))
				(in r309)
			)
	)

	(:action move-right-from-r311
		:precondition
			(and 
				(in r311)
			)
		:effect
			(and
				(not (search_again))
				(not (in r311))
				(in r312)
			)
	)


	(:action move-left-from-r311
		:precondition
			(and 
				(in r311)
			)
		:effect
			(and
				(not (search_again))
				(not (in r311))
				(in r310)
			)
	)

	(:action move-right-from-r312
		:precondition
			(and 
				(in r312)
			)
		:effect
			(and
				(not (search_again))
				(not (in r312))
				(in r313)
			)
	)


	(:action move-left-from-r312
		:precondition
			(and 
				(in r312)
			)
		:effect
			(and
				(not (search_again))
				(not (in r312))
				(in r311)
			)
	)

	(:action move-right-from-r313
		:precondition
			(and 
				(in r313)
			)
		:effect
			(and
				(not (search_again))
				(not (in r313))
				(in r314)
			)
	)


	(:action move-left-from-r313
		:precondition
			(and 
				(in r313)
			)
		:effect
			(and
				(not (search_again))
				(not (in r313))
				(in r312)
			)
	)

	(:action move-right-from-r314
		:precondition
			(and 
				(in r314)
			)
		:effect
			(and
				(not (search_again))
				(not (in r314))
				(in r315)
			)
	)


	(:action move-left-from-r314
		:precondition
			(and 
				(in r314)
			)
		:effect
			(and
				(not (search_again))
				(not (in r314))
				(in r313)
			)
	)

	(:action move-right-from-r315
		:precondition
			(and 
				(in r315)
			)
		:effect
			(and
				(not (search_again))
				(not (in r315))
				(in r316)
			)
	)


	(:action move-left-from-r315
		:precondition
			(and 
				(in r315)
			)
		:effect
			(and
				(not (search_again))
				(not (in r315))
				(in r314)
			)
	)

	(:action move-right-from-r316
		:precondition
			(and 
				(in r316)
			)
		:effect
			(and
				(not (search_again))
				(not (in r316))
				(in r317)
			)
	)


	(:action move-left-from-r316
		:precondition
			(and 
				(in r316)
			)
		:effect
			(and
				(not (search_again))
				(not (in r316))
				(in r315)
			)
	)

	(:action move-right-from-r317
		:precondition
			(and 
				(in r317)
			)
		:effect
			(and
				(not (search_again))
				(not (in r317))
				(in r318)
			)
	)


	(:action move-left-from-r317
		:precondition
			(and 
				(in r317)
			)
		:effect
			(and
				(not (search_again))
				(not (in r317))
				(in r316)
			)
	)

	(:action move-right-from-r318
		:precondition
			(and 
				(in r318)
			)
		:effect
			(and
				(not (search_again))
				(not (in r318))
				(in r319)
			)
	)


	(:action move-left-from-r318
		:precondition
			(and 
				(in r318)
			)
		:effect
			(and
				(not (search_again))
				(not (in r318))
				(in r317)
			)
	)

	(:action move-right-from-r319
		:precondition
			(and 
				(in r319)
			)
		:effect
			(and
				(not (search_again))
				(not (in r319))
				(in r320)
			)
	)


	(:action move-left-from-r319
		:precondition
			(and 
				(in r319)
			)
		:effect
			(and
				(not (search_again))
				(not (in r319))
				(in r318)
			)
	)

	(:action move-right-from-r320
		:precondition
			(and 
				(in r320)
			)
		:effect
			(and
				(not (search_again))
				(not (in r320))
				(in r321)
			)
	)


	(:action move-left-from-r320
		:precondition
			(and 
				(in r320)
			)
		:effect
			(and
				(not (search_again))
				(not (in r320))
				(in r319)
			)
	)

	(:action move-right-from-r321
		:precondition
			(and 
				(in r321)
			)
		:effect
			(and
				(not (search_again))
				(not (in r321))
				(in r322)
			)
	)


	(:action move-left-from-r321
		:precondition
			(and 
				(in r321)
			)
		:effect
			(and
				(not (search_again))
				(not (in r321))
				(in r320)
			)
	)

	(:action move-right-from-r322
		:precondition
			(and 
				(in r322)
			)
		:effect
			(and
				(not (search_again))
				(not (in r322))
				(in r323)
			)
	)


	(:action move-left-from-r322
		:precondition
			(and 
				(in r322)
			)
		:effect
			(and
				(not (search_again))
				(not (in r322))
				(in r321)
			)
	)

	(:action move-right-from-r323
		:precondition
			(and 
				(in r323)
			)
		:effect
			(and
				(not (search_again))
				(not (in r323))
				(in r324)
			)
	)


	(:action move-left-from-r323
		:precondition
			(and 
				(in r323)
			)
		:effect
			(and
				(not (search_again))
				(not (in r323))
				(in r322)
			)
	)

	(:action move-right-from-r324
		:precondition
			(and 
				(in r324)
			)
		:effect
			(and
				(not (search_again))
				(not (in r324))
				(in r325)
			)
	)


	(:action move-left-from-r324
		:precondition
			(and 
				(in r324)
			)
		:effect
			(and
				(not (search_again))
				(not (in r324))
				(in r323)
			)
	)

	(:action move-right-from-r325
		:precondition
			(and 
				(in r325)
			)
		:effect
			(and
				(not (search_again))
				(not (in r325))
				(in r326)
			)
	)


	(:action move-left-from-r325
		:precondition
			(and 
				(in r325)
			)
		:effect
			(and
				(not (search_again))
				(not (in r325))
				(in r324)
			)
	)

	(:action move-right-from-r326
		:precondition
			(and 
				(in r326)
			)
		:effect
			(and
				(not (search_again))
				(not (in r326))
				(in r327)
			)
	)


	(:action move-left-from-r326
		:precondition
			(and 
				(in r326)
			)
		:effect
			(and
				(not (search_again))
				(not (in r326))
				(in r325)
			)
	)

	(:action move-right-from-r327
		:precondition
			(and 
				(in r327)
			)
		:effect
			(and
				(not (search_again))
				(not (in r327))
				(in r328)
			)
	)


	(:action move-left-from-r327
		:precondition
			(and 
				(in r327)
			)
		:effect
			(and
				(not (search_again))
				(not (in r327))
				(in r326)
			)
	)

	(:action move-right-from-r328
		:precondition
			(and 
				(in r328)
			)
		:effect
			(and
				(not (search_again))
				(not (in r328))
				(in r329)
			)
	)


	(:action move-left-from-r328
		:precondition
			(and 
				(in r328)
			)
		:effect
			(and
				(not (search_again))
				(not (in r328))
				(in r327)
			)
	)

	(:action move-right-from-r329
		:precondition
			(and 
				(in r329)
			)
		:effect
			(and
				(not (search_again))
				(not (in r329))
				(in r330)
			)
	)


	(:action move-left-from-r329
		:precondition
			(and 
				(in r329)
			)
		:effect
			(and
				(not (search_again))
				(not (in r329))
				(in r328)
			)
	)

	(:action move-right-from-r330
		:precondition
			(and 
				(in r330)
			)
		:effect
			(and
				(not (search_again))
				(not (in r330))
				(in r331)
			)
	)


	(:action move-left-from-r330
		:precondition
			(and 
				(in r330)
			)
		:effect
			(and
				(not (search_again))
				(not (in r330))
				(in r329)
			)
	)

	(:action move-right-from-r331
		:precondition
			(and 
				(in r331)
			)
		:effect
			(and
				(not (search_again))
				(not (in r331))
				(in r332)
			)
	)


	(:action move-left-from-r331
		:precondition
			(and 
				(in r331)
			)
		:effect
			(and
				(not (search_again))
				(not (in r331))
				(in r330)
			)
	)

	(:action move-right-from-r332
		:precondition
			(and 
				(in r332)
			)
		:effect
			(and
				(not (search_again))
				(not (in r332))
				(in r333)
			)
	)


	(:action move-left-from-r332
		:precondition
			(and 
				(in r332)
			)
		:effect
			(and
				(not (search_again))
				(not (in r332))
				(in r331)
			)
	)

	(:action move-right-from-r333
		:precondition
			(and 
				(in r333)
			)
		:effect
			(and
				(not (search_again))
				(not (in r333))
				(in r334)
			)
	)


	(:action move-left-from-r333
		:precondition
			(and 
				(in r333)
			)
		:effect
			(and
				(not (search_again))
				(not (in r333))
				(in r332)
			)
	)

	(:action move-right-from-r334
		:precondition
			(and 
				(in r334)
			)
		:effect
			(and
				(not (search_again))
				(not (in r334))
				(in r335)
			)
	)


	(:action move-left-from-r334
		:precondition
			(and 
				(in r334)
			)
		:effect
			(and
				(not (search_again))
				(not (in r334))
				(in r333)
			)
	)

	(:action move-right-from-r335
		:precondition
			(and 
				(in r335)
			)
		:effect
			(and
				(not (search_again))
				(not (in r335))
				(in r336)
			)
	)


	(:action move-left-from-r335
		:precondition
			(and 
				(in r335)
			)
		:effect
			(and
				(not (search_again))
				(not (in r335))
				(in r334)
			)
	)

	(:action move-right-from-r336
		:precondition
			(and 
				(in r336)
			)
		:effect
			(and
				(not (search_again))
				(not (in r336))
				(in r337)
			)
	)


	(:action move-left-from-r336
		:precondition
			(and 
				(in r336)
			)
		:effect
			(and
				(not (search_again))
				(not (in r336))
				(in r335)
			)
	)

	(:action move-right-from-r337
		:precondition
			(and 
				(in r337)
			)
		:effect
			(and
				(not (search_again))
				(not (in r337))
				(in r338)
			)
	)


	(:action move-left-from-r337
		:precondition
			(and 
				(in r337)
			)
		:effect
			(and
				(not (search_again))
				(not (in r337))
				(in r336)
			)
	)

	(:action move-right-from-r338
		:precondition
			(and 
				(in r338)
			)
		:effect
			(and
				(not (search_again))
				(not (in r338))
				(in r339)
			)
	)


	(:action move-left-from-r338
		:precondition
			(and 
				(in r338)
			)
		:effect
			(and
				(not (search_again))
				(not (in r338))
				(in r337)
			)
	)

	(:action move-right-from-r339
		:precondition
			(and 
				(in r339)
			)
		:effect
			(and
				(not (search_again))
				(not (in r339))
				(in r340)
			)
	)


	(:action move-left-from-r339
		:precondition
			(and 
				(in r339)
			)
		:effect
			(and
				(not (search_again))
				(not (in r339))
				(in r338)
			)
	)

	(:action move-right-from-r340
		:precondition
			(and 
				(in r340)
			)
		:effect
			(and
				(not (search_again))
				(not (in r340))
				(in r341)
			)
	)


	(:action move-left-from-r340
		:precondition
			(and 
				(in r340)
			)
		:effect
			(and
				(not (search_again))
				(not (in r340))
				(in r339)
			)
	)

	(:action move-right-from-r341
		:precondition
			(and 
				(in r341)
			)
		:effect
			(and
				(not (search_again))
				(not (in r341))
				(in r342)
			)
	)


	(:action move-left-from-r341
		:precondition
			(and 
				(in r341)
			)
		:effect
			(and
				(not (search_again))
				(not (in r341))
				(in r340)
			)
	)

	(:action move-right-from-r342
		:precondition
			(and 
				(in r342)
			)
		:effect
			(and
				(not (search_again))
				(not (in r342))
				(in r343)
			)
	)


	(:action move-left-from-r342
		:precondition
			(and 
				(in r342)
			)
		:effect
			(and
				(not (search_again))
				(not (in r342))
				(in r341)
			)
	)

	(:action move-right-from-r343
		:precondition
			(and 
				(in r343)
			)
		:effect
			(and
				(not (search_again))
				(not (in r343))
				(in r344)
			)
	)


	(:action move-left-from-r343
		:precondition
			(and 
				(in r343)
			)
		:effect
			(and
				(not (search_again))
				(not (in r343))
				(in r342)
			)
	)

	(:action move-right-from-r344
		:precondition
			(and 
				(in r344)
			)
		:effect
			(and
				(not (search_again))
				(not (in r344))
				(in r345)
			)
	)


	(:action move-left-from-r344
		:precondition
			(and 
				(in r344)
			)
		:effect
			(and
				(not (search_again))
				(not (in r344))
				(in r343)
			)
	)

	(:action move-right-from-r345
		:precondition
			(and 
				(in r345)
			)
		:effect
			(and
				(not (search_again))
				(not (in r345))
				(in r346)
			)
	)


	(:action move-left-from-r345
		:precondition
			(and 
				(in r345)
			)
		:effect
			(and
				(not (search_again))
				(not (in r345))
				(in r344)
			)
	)

	(:action move-right-from-r346
		:precondition
			(and 
				(in r346)
			)
		:effect
			(and
				(not (search_again))
				(not (in r346))
				(in r347)
			)
	)


	(:action move-left-from-r346
		:precondition
			(and 
				(in r346)
			)
		:effect
			(and
				(not (search_again))
				(not (in r346))
				(in r345)
			)
	)

	(:action move-right-from-r347
		:precondition
			(and 
				(in r347)
			)
		:effect
			(and
				(not (search_again))
				(not (in r347))
				(in r348)
			)
	)


	(:action move-left-from-r347
		:precondition
			(and 
				(in r347)
			)
		:effect
			(and
				(not (search_again))
				(not (in r347))
				(in r346)
			)
	)

	(:action move-right-from-r348
		:precondition
			(and 
				(in r348)
			)
		:effect
			(and
				(not (search_again))
				(not (in r348))
				(in r349)
			)
	)


	(:action move-left-from-r348
		:precondition
			(and 
				(in r348)
			)
		:effect
			(and
				(not (search_again))
				(not (in r348))
				(in r347)
			)
	)

	(:action move-right-from-r349
		:precondition
			(and 
				(in r349)
			)
		:effect
			(and
				(not (search_again))
				(not (in r349))
				(in r350)
			)
	)


	(:action move-left-from-r349
		:precondition
			(and 
				(in r349)
			)
		:effect
			(and
				(not (search_again))
				(not (in r349))
				(in r348)
			)
	)

	(:action move-right-from-r350
		:precondition
			(and 
				(in r350)
			)
		:effect
			(and
				(not (search_again))
				(not (in r350))
				(in r351)
			)
	)


	(:action move-left-from-r350
		:precondition
			(and 
				(in r350)
			)
		:effect
			(and
				(not (search_again))
				(not (in r350))
				(in r349)
			)
	)

	(:action move-right-from-r351
		:precondition
			(and 
				(in r351)
			)
		:effect
			(and
				(not (search_again))
				(not (in r351))
				(in r352)
			)
	)


	(:action move-left-from-r351
		:precondition
			(and 
				(in r351)
			)
		:effect
			(and
				(not (search_again))
				(not (in r351))
				(in r350)
			)
	)

	(:action move-right-from-r352
		:precondition
			(and 
				(in r352)
			)
		:effect
			(and
				(not (search_again))
				(not (in r352))
				(in r353)
			)
	)


	(:action move-left-from-r352
		:precondition
			(and 
				(in r352)
			)
		:effect
			(and
				(not (search_again))
				(not (in r352))
				(in r351)
			)
	)

	(:action move-right-from-r353
		:precondition
			(and 
				(in r353)
			)
		:effect
			(and
				(not (search_again))
				(not (in r353))
				(in r354)
			)
	)


	(:action move-left-from-r353
		:precondition
			(and 
				(in r353)
			)
		:effect
			(and
				(not (search_again))
				(not (in r353))
				(in r352)
			)
	)

	(:action move-right-from-r354
		:precondition
			(and 
				(in r354)
			)
		:effect
			(and
				(not (search_again))
				(not (in r354))
				(in r355)
			)
	)


	(:action move-left-from-r354
		:precondition
			(and 
				(in r354)
			)
		:effect
			(and
				(not (search_again))
				(not (in r354))
				(in r353)
			)
	)

	(:action move-right-from-r355
		:precondition
			(and 
				(in r355)
			)
		:effect
			(and
				(not (search_again))
				(not (in r355))
				(in r356)
			)
	)


	(:action move-left-from-r355
		:precondition
			(and 
				(in r355)
			)
		:effect
			(and
				(not (search_again))
				(not (in r355))
				(in r354)
			)
	)

	(:action move-right-from-r356
		:precondition
			(and 
				(in r356)
			)
		:effect
			(and
				(not (search_again))
				(not (in r356))
				(in r357)
			)
	)


	(:action move-left-from-r356
		:precondition
			(and 
				(in r356)
			)
		:effect
			(and
				(not (search_again))
				(not (in r356))
				(in r355)
			)
	)

	(:action move-right-from-r357
		:precondition
			(and 
				(in r357)
			)
		:effect
			(and
				(not (search_again))
				(not (in r357))
				(in r358)
			)
	)


	(:action move-left-from-r357
		:precondition
			(and 
				(in r357)
			)
		:effect
			(and
				(not (search_again))
				(not (in r357))
				(in r356)
			)
	)

	(:action move-right-from-r358
		:precondition
			(and 
				(in r358)
			)
		:effect
			(and
				(not (search_again))
				(not (in r358))
				(in r359)
			)
	)


	(:action move-left-from-r358
		:precondition
			(and 
				(in r358)
			)
		:effect
			(and
				(not (search_again))
				(not (in r358))
				(in r357)
			)
	)

	(:action move-right-from-r359
		:precondition
			(and 
				(in r359)
			)
		:effect
			(and
				(not (search_again))
				(not (in r359))
				(in r360)
			)
	)


	(:action move-left-from-r359
		:precondition
			(and 
				(in r359)
			)
		:effect
			(and
				(not (search_again))
				(not (in r359))
				(in r358)
			)
	)

	(:action move-right-from-r360
		:precondition
			(and 
				(in r360)
			)
		:effect
			(and
				(not (search_again))
				(not (in r360))
				(in r361)
			)
	)


	(:action move-left-from-r360
		:precondition
			(and 
				(in r360)
			)
		:effect
			(and
				(not (search_again))
				(not (in r360))
				(in r359)
			)
	)

	(:action move-right-from-r361
		:precondition
			(and 
				(in r361)
			)
		:effect
			(and
				(not (search_again))
				(not (in r361))
				(in r362)
			)
	)


	(:action move-left-from-r361
		:precondition
			(and 
				(in r361)
			)
		:effect
			(and
				(not (search_again))
				(not (in r361))
				(in r360)
			)
	)

	(:action move-right-from-r362
		:precondition
			(and 
				(in r362)
			)
		:effect
			(and
				(not (search_again))
				(not (in r362))
				(in r363)
			)
	)


	(:action move-left-from-r362
		:precondition
			(and 
				(in r362)
			)
		:effect
			(and
				(not (search_again))
				(not (in r362))
				(in r361)
			)
	)

	(:action move-right-from-r363
		:precondition
			(and 
				(in r363)
			)
		:effect
			(and
				(not (search_again))
				(not (in r363))
				(in r364)
			)
	)


	(:action move-left-from-r363
		:precondition
			(and 
				(in r363)
			)
		:effect
			(and
				(not (search_again))
				(not (in r363))
				(in r362)
			)
	)

	(:action move-right-from-r364
		:precondition
			(and 
				(in r364)
			)
		:effect
			(and
				(not (search_again))
				(not (in r364))
				(in r365)
			)
	)


	(:action move-left-from-r364
		:precondition
			(and 
				(in r364)
			)
		:effect
			(and
				(not (search_again))
				(not (in r364))
				(in r363)
			)
	)

	(:action move-right-from-r365
		:precondition
			(and 
				(in r365)
			)
		:effect
			(and
				(not (search_again))
				(not (in r365))
				(in r366)
			)
	)


	(:action move-left-from-r365
		:precondition
			(and 
				(in r365)
			)
		:effect
			(and
				(not (search_again))
				(not (in r365))
				(in r364)
			)
	)

	(:action move-right-from-r366
		:precondition
			(and 
				(in r366)
			)
		:effect
			(and
				(not (search_again))
				(not (in r366))
				(in r367)
			)
	)


	(:action move-left-from-r366
		:precondition
			(and 
				(in r366)
			)
		:effect
			(and
				(not (search_again))
				(not (in r366))
				(in r365)
			)
	)

	(:action move-right-from-r367
		:precondition
			(and 
				(in r367)
			)
		:effect
			(and
				(not (search_again))
				(not (in r367))
				(in r368)
			)
	)


	(:action move-left-from-r367
		:precondition
			(and 
				(in r367)
			)
		:effect
			(and
				(not (search_again))
				(not (in r367))
				(in r366)
			)
	)

	(:action move-right-from-r368
		:precondition
			(and 
				(in r368)
			)
		:effect
			(and
				(not (search_again))
				(not (in r368))
				(in r369)
			)
	)


	(:action move-left-from-r368
		:precondition
			(and 
				(in r368)
			)
		:effect
			(and
				(not (search_again))
				(not (in r368))
				(in r367)
			)
	)

	(:action move-right-from-r369
		:precondition
			(and 
				(in r369)
			)
		:effect
			(and
				(not (search_again))
				(not (in r369))
				(in r370)
			)
	)


	(:action move-left-from-r369
		:precondition
			(and 
				(in r369)
			)
		:effect
			(and
				(not (search_again))
				(not (in r369))
				(in r368)
			)
	)

	(:action move-right-from-r370
		:precondition
			(and 
				(in r370)
			)
		:effect
			(and
				(not (search_again))
				(not (in r370))
				(in r371)
			)
	)


	(:action move-left-from-r370
		:precondition
			(and 
				(in r370)
			)
		:effect
			(and
				(not (search_again))
				(not (in r370))
				(in r369)
			)
	)

	(:action move-right-from-r371
		:precondition
			(and 
				(in r371)
			)
		:effect
			(and
				(not (search_again))
				(not (in r371))
				(in r372)
			)
	)


	(:action move-left-from-r371
		:precondition
			(and 
				(in r371)
			)
		:effect
			(and
				(not (search_again))
				(not (in r371))
				(in r370)
			)
	)

	(:action move-right-from-r372
		:precondition
			(and 
				(in r372)
			)
		:effect
			(and
				(not (search_again))
				(not (in r372))
				(in r373)
			)
	)


	(:action move-left-from-r372
		:precondition
			(and 
				(in r372)
			)
		:effect
			(and
				(not (search_again))
				(not (in r372))
				(in r371)
			)
	)

	(:action move-right-from-r373
		:precondition
			(and 
				(in r373)
			)
		:effect
			(and
				(not (search_again))
				(not (in r373))
				(in r374)
			)
	)


	(:action move-left-from-r373
		:precondition
			(and 
				(in r373)
			)
		:effect
			(and
				(not (search_again))
				(not (in r373))
				(in r372)
			)
	)

	(:action move-right-from-r374
		:precondition
			(and 
				(in r374)
			)
		:effect
			(and
				(not (search_again))
				(not (in r374))
				(in r375)
			)
	)


	(:action move-left-from-r374
		:precondition
			(and 
				(in r374)
			)
		:effect
			(and
				(not (search_again))
				(not (in r374))
				(in r373)
			)
	)

	(:action move-right-from-r375
		:precondition
			(and 
				(in r375)
			)
		:effect
			(and
				(not (search_again))
				(not (in r375))
				(in r376)
			)
	)


	(:action move-left-from-r375
		:precondition
			(and 
				(in r375)
			)
		:effect
			(and
				(not (search_again))
				(not (in r375))
				(in r374)
			)
	)

	(:action move-right-from-r376
		:precondition
			(and 
				(in r376)
			)
		:effect
			(and
				(not (search_again))
				(not (in r376))
				(in r377)
			)
	)


	(:action move-left-from-r376
		:precondition
			(and 
				(in r376)
			)
		:effect
			(and
				(not (search_again))
				(not (in r376))
				(in r375)
			)
	)

	(:action move-right-from-r377
		:precondition
			(and 
				(in r377)
			)
		:effect
			(and
				(not (search_again))
				(not (in r377))
				(in r378)
			)
	)


	(:action move-left-from-r377
		:precondition
			(and 
				(in r377)
			)
		:effect
			(and
				(not (search_again))
				(not (in r377))
				(in r376)
			)
	)

	(:action move-right-from-r378
		:precondition
			(and 
				(in r378)
			)
		:effect
			(and
				(not (search_again))
				(not (in r378))
				(in r379)
			)
	)


	(:action move-left-from-r378
		:precondition
			(and 
				(in r378)
			)
		:effect
			(and
				(not (search_again))
				(not (in r378))
				(in r377)
			)
	)

	(:action move-right-from-r379
		:precondition
			(and 
				(in r379)
			)
		:effect
			(and
				(not (search_again))
				(not (in r379))
				(in r380)
			)
	)


	(:action move-left-from-r379
		:precondition
			(and 
				(in r379)
			)
		:effect
			(and
				(not (search_again))
				(not (in r379))
				(in r378)
			)
	)

	(:action move-right-from-r380
		:precondition
			(and 
				(in r380)
			)
		:effect
			(and
				(not (search_again))
				(not (in r380))
				(in r381)
			)
	)


	(:action move-left-from-r380
		:precondition
			(and 
				(in r380)
			)
		:effect
			(and
				(not (search_again))
				(not (in r380))
				(in r379)
			)
	)

	(:action move-right-from-r381
		:precondition
			(and 
				(in r381)
			)
		:effect
			(and
				(not (search_again))
				(not (in r381))
				(in r382)
			)
	)


	(:action move-left-from-r381
		:precondition
			(and 
				(in r381)
			)
		:effect
			(and
				(not (search_again))
				(not (in r381))
				(in r380)
			)
	)

	(:action move-right-from-r382
		:precondition
			(and 
				(in r382)
			)
		:effect
			(and
				(not (search_again))
				(not (in r382))
				(in r383)
			)
	)


	(:action move-left-from-r382
		:precondition
			(and 
				(in r382)
			)
		:effect
			(and
				(not (search_again))
				(not (in r382))
				(in r381)
			)
	)

	(:action move-right-from-r383
		:precondition
			(and 
				(in r383)
			)
		:effect
			(and
				(not (search_again))
				(not (in r383))
				(in r384)
			)
	)


	(:action move-left-from-r383
		:precondition
			(and 
				(in r383)
			)
		:effect
			(and
				(not (search_again))
				(not (in r383))
				(in r382)
			)
	)

	(:action move-right-from-r384
		:precondition
			(and 
				(in r384)
			)
		:effect
			(and
				(not (search_again))
				(not (in r384))
				(in r385)
			)
	)


	(:action move-left-from-r384
		:precondition
			(and 
				(in r384)
			)
		:effect
			(and
				(not (search_again))
				(not (in r384))
				(in r383)
			)
	)

	(:action move-right-from-r385
		:precondition
			(and 
				(in r385)
			)
		:effect
			(and
				(not (search_again))
				(not (in r385))
				(in r386)
			)
	)


	(:action move-left-from-r385
		:precondition
			(and 
				(in r385)
			)
		:effect
			(and
				(not (search_again))
				(not (in r385))
				(in r384)
			)
	)

	(:action move-right-from-r386
		:precondition
			(and 
				(in r386)
			)
		:effect
			(and
				(not (search_again))
				(not (in r386))
				(in r387)
			)
	)


	(:action move-left-from-r386
		:precondition
			(and 
				(in r386)
			)
		:effect
			(and
				(not (search_again))
				(not (in r386))
				(in r385)
			)
	)

	(:action move-right-from-r387
		:precondition
			(and 
				(in r387)
			)
		:effect
			(and
				(not (search_again))
				(not (in r387))
				(in r388)
			)
	)


	(:action move-left-from-r387
		:precondition
			(and 
				(in r387)
			)
		:effect
			(and
				(not (search_again))
				(not (in r387))
				(in r386)
			)
	)

	(:action move-right-from-r388
		:precondition
			(and 
				(in r388)
			)
		:effect
			(and
				(not (search_again))
				(not (in r388))
				(in r389)
			)
	)


	(:action move-left-from-r388
		:precondition
			(and 
				(in r388)
			)
		:effect
			(and
				(not (search_again))
				(not (in r388))
				(in r387)
			)
	)

	(:action move-right-from-r389
		:precondition
			(and 
				(in r389)
			)
		:effect
			(and
				(not (search_again))
				(not (in r389))
				(in r390)
			)
	)


	(:action move-left-from-r389
		:precondition
			(and 
				(in r389)
			)
		:effect
			(and
				(not (search_again))
				(not (in r389))
				(in r388)
			)
	)

	(:action move-right-from-r390
		:precondition
			(and 
				(in r390)
			)
		:effect
			(and
				(not (search_again))
				(not (in r390))
				(in r391)
			)
	)


	(:action move-left-from-r390
		:precondition
			(and 
				(in r390)
			)
		:effect
			(and
				(not (search_again))
				(not (in r390))
				(in r389)
			)
	)

	(:action move-right-from-r391
		:precondition
			(and 
				(in r391)
			)
		:effect
			(and
				(not (search_again))
				(not (in r391))
				(in r392)
			)
	)


	(:action move-left-from-r391
		:precondition
			(and 
				(in r391)
			)
		:effect
			(and
				(not (search_again))
				(not (in r391))
				(in r390)
			)
	)

	(:action move-right-from-r392
		:precondition
			(and 
				(in r392)
			)
		:effect
			(and
				(not (search_again))
				(not (in r392))
				(in r393)
			)
	)


	(:action move-left-from-r392
		:precondition
			(and 
				(in r392)
			)
		:effect
			(and
				(not (search_again))
				(not (in r392))
				(in r391)
			)
	)

	(:action move-right-from-r393
		:precondition
			(and 
				(in r393)
			)
		:effect
			(and
				(not (search_again))
				(not (in r393))
				(in r394)
			)
	)


	(:action move-left-from-r393
		:precondition
			(and 
				(in r393)
			)
		:effect
			(and
				(not (search_again))
				(not (in r393))
				(in r392)
			)
	)

	(:action move-right-from-r394
		:precondition
			(and 
				(in r394)
			)
		:effect
			(and
				(not (search_again))
				(not (in r394))
				(in r395)
			)
	)


	(:action move-left-from-r394
		:precondition
			(and 
				(in r394)
			)
		:effect
			(and
				(not (search_again))
				(not (in r394))
				(in r393)
			)
	)

	(:action move-right-from-r395
		:precondition
			(and 
				(in r395)
			)
		:effect
			(and
				(not (search_again))
				(not (in r395))
				(in r396)
			)
	)


	(:action move-left-from-r395
		:precondition
			(and 
				(in r395)
			)
		:effect
			(and
				(not (search_again))
				(not (in r395))
				(in r394)
			)
	)

	(:action move-right-from-r396
		:precondition
			(and 
				(in r396)
			)
		:effect
			(and
				(not (search_again))
				(not (in r396))
				(in r397)
			)
	)


	(:action move-left-from-r396
		:precondition
			(and 
				(in r396)
			)
		:effect
			(and
				(not (search_again))
				(not (in r396))
				(in r395)
			)
	)

	(:action move-right-from-r397
		:precondition
			(and 
				(in r397)
			)
		:effect
			(and
				(not (search_again))
				(not (in r397))
				(in r398)
			)
	)


	(:action move-left-from-r397
		:precondition
			(and 
				(in r397)
			)
		:effect
			(and
				(not (search_again))
				(not (in r397))
				(in r396)
			)
	)

	(:action move-right-from-r398
		:precondition
			(and 
				(in r398)
			)
		:effect
			(and
				(not (search_again))
				(not (in r398))
				(in r399)
			)
	)


	(:action move-left-from-r398
		:precondition
			(and 
				(in r398)
			)
		:effect
			(and
				(not (search_again))
				(not (in r398))
				(in r397)
			)
	)

	(:action move-right-from-r399
		:precondition
			(and 
				(in r399)
			)
		:effect
			(and
				(not (search_again))
				(not (in r399))
				(in r400)
			)
	)


	(:action move-left-from-r399
		:precondition
			(and 
				(in r399)
			)
		:effect
			(and
				(not (search_again))
				(not (in r399))
				(in r398)
			)
	)

	(:action move-right-from-r400
		:precondition
			(and 
				(in r400)
			)
		:effect
			(and
				(not (search_again))
				(not (in r400))
				(in r401)
			)
	)


	(:action move-left-from-r400
		:precondition
			(and 
				(in r400)
			)
		:effect
			(and
				(not (search_again))
				(not (in r400))
				(in r399)
			)
	)

	(:action move-right-from-r401
		:precondition
			(and 
				(in r401)
			)
		:effect
			(and
				(not (search_again))
				(not (in r401))
				(in r402)
			)
	)


	(:action move-left-from-r401
		:precondition
			(and 
				(in r401)
			)
		:effect
			(and
				(not (search_again))
				(not (in r401))
				(in r400)
			)
	)

	(:action move-right-from-r402
		:precondition
			(and 
				(in r402)
			)
		:effect
			(and
				(not (search_again))
				(not (in r402))
				(in r403)
			)
	)


	(:action move-left-from-r402
		:precondition
			(and 
				(in r402)
			)
		:effect
			(and
				(not (search_again))
				(not (in r402))
				(in r401)
			)
	)

	(:action move-right-from-r403
		:precondition
			(and 
				(in r403)
			)
		:effect
			(and
				(not (search_again))
				(not (in r403))
				(in r404)
			)
	)


	(:action move-left-from-r403
		:precondition
			(and 
				(in r403)
			)
		:effect
			(and
				(not (search_again))
				(not (in r403))
				(in r402)
			)
	)

	(:action move-right-from-r404
		:precondition
			(and 
				(in r404)
			)
		:effect
			(and
				(not (search_again))
				(not (in r404))
				(in r405)
			)
	)


	(:action move-left-from-r404
		:precondition
			(and 
				(in r404)
			)
		:effect
			(and
				(not (search_again))
				(not (in r404))
				(in r403)
			)
	)

	(:action move-right-from-r405
		:precondition
			(and 
				(in r405)
			)
		:effect
			(and
				(not (search_again))
				(not (in r405))
				(in r406)
			)
	)


	(:action move-left-from-r405
		:precondition
			(and 
				(in r405)
			)
		:effect
			(and
				(not (search_again))
				(not (in r405))
				(in r404)
			)
	)

	(:action move-right-from-r406
		:precondition
			(and 
				(in r406)
			)
		:effect
			(and
				(not (search_again))
				(not (in r406))
				(in r407)
			)
	)


	(:action move-left-from-r406
		:precondition
			(and 
				(in r406)
			)
		:effect
			(and
				(not (search_again))
				(not (in r406))
				(in r405)
			)
	)

	(:action move-right-from-r407
		:precondition
			(and 
				(in r407)
			)
		:effect
			(and
				(not (search_again))
				(not (in r407))
				(in r408)
			)
	)


	(:action move-left-from-r407
		:precondition
			(and 
				(in r407)
			)
		:effect
			(and
				(not (search_again))
				(not (in r407))
				(in r406)
			)
	)

	(:action move-right-from-r408
		:precondition
			(and 
				(in r408)
			)
		:effect
			(and
				(not (search_again))
				(not (in r408))
				(in r409)
			)
	)


	(:action move-left-from-r408
		:precondition
			(and 
				(in r408)
			)
		:effect
			(and
				(not (search_again))
				(not (in r408))
				(in r407)
			)
	)

	(:action move-right-from-r409
		:precondition
			(and 
				(in r409)
			)
		:effect
			(and
				(not (search_again))
				(not (in r409))
				(in r410)
			)
	)


	(:action move-left-from-r409
		:precondition
			(and 
				(in r409)
			)
		:effect
			(and
				(not (search_again))
				(not (in r409))
				(in r408)
			)
	)

	(:action move-right-from-r410
		:precondition
			(and 
				(in r410)
			)
		:effect
			(and
				(not (search_again))
				(not (in r410))
				(in r411)
			)
	)


	(:action move-left-from-r410
		:precondition
			(and 
				(in r410)
			)
		:effect
			(and
				(not (search_again))
				(not (in r410))
				(in r409)
			)
	)

	(:action move-right-from-r411
		:precondition
			(and 
				(in r411)
			)
		:effect
			(and
				(not (search_again))
				(not (in r411))
				(in r412)
			)
	)


	(:action move-left-from-r411
		:precondition
			(and 
				(in r411)
			)
		:effect
			(and
				(not (search_again))
				(not (in r411))
				(in r410)
			)
	)

	(:action move-right-from-r412
		:precondition
			(and 
				(in r412)
			)
		:effect
			(and
				(not (search_again))
				(not (in r412))
				(in r413)
			)
	)


	(:action move-left-from-r412
		:precondition
			(and 
				(in r412)
			)
		:effect
			(and
				(not (search_again))
				(not (in r412))
				(in r411)
			)
	)

	(:action move-right-from-r413
		:precondition
			(and 
				(in r413)
			)
		:effect
			(and
				(not (search_again))
				(not (in r413))
				(in r414)
			)
	)


	(:action move-left-from-r413
		:precondition
			(and 
				(in r413)
			)
		:effect
			(and
				(not (search_again))
				(not (in r413))
				(in r412)
			)
	)

	(:action move-right-from-r414
		:precondition
			(and 
				(in r414)
			)
		:effect
			(and
				(not (search_again))
				(not (in r414))
				(in r415)
			)
	)


	(:action move-left-from-r414
		:precondition
			(and 
				(in r414)
			)
		:effect
			(and
				(not (search_again))
				(not (in r414))
				(in r413)
			)
	)

	(:action move-right-from-r415
		:precondition
			(and 
				(in r415)
			)
		:effect
			(and
				(not (search_again))
				(not (in r415))
				(in r416)
			)
	)


	(:action move-left-from-r415
		:precondition
			(and 
				(in r415)
			)
		:effect
			(and
				(not (search_again))
				(not (in r415))
				(in r414)
			)
	)

	(:action move-right-from-r416
		:precondition
			(and 
				(in r416)
			)
		:effect
			(and
				(not (search_again))
				(not (in r416))
				(in r417)
			)
	)


	(:action move-left-from-r416
		:precondition
			(and 
				(in r416)
			)
		:effect
			(and
				(not (search_again))
				(not (in r416))
				(in r415)
			)
	)

	(:action move-right-from-r417
		:precondition
			(and 
				(in r417)
			)
		:effect
			(and
				(not (search_again))
				(not (in r417))
				(in r418)
			)
	)


	(:action move-left-from-r417
		:precondition
			(and 
				(in r417)
			)
		:effect
			(and
				(not (search_again))
				(not (in r417))
				(in r416)
			)
	)

	(:action move-right-from-r418
		:precondition
			(and 
				(in r418)
			)
		:effect
			(and
				(not (search_again))
				(not (in r418))
				(in r419)
			)
	)


	(:action move-left-from-r418
		:precondition
			(and 
				(in r418)
			)
		:effect
			(and
				(not (search_again))
				(not (in r418))
				(in r417)
			)
	)

	(:action move-right-from-r419
		:precondition
			(and 
				(in r419)
			)
		:effect
			(and
				(not (search_again))
				(not (in r419))
				(in r420)
			)
	)


	(:action move-left-from-r419
		:precondition
			(and 
				(in r419)
			)
		:effect
			(and
				(not (search_again))
				(not (in r419))
				(in r418)
			)
	)

	(:action move-right-from-r420
		:precondition
			(and 
				(in r420)
			)
		:effect
			(and
				(not (search_again))
				(not (in r420))
				(in r421)
			)
	)


	(:action move-left-from-r420
		:precondition
			(and 
				(in r420)
			)
		:effect
			(and
				(not (search_again))
				(not (in r420))
				(in r419)
			)
	)

	(:action move-right-from-r421
		:precondition
			(and 
				(in r421)
			)
		:effect
			(and
				(not (search_again))
				(not (in r421))
				(in r422)
			)
	)


	(:action move-left-from-r421
		:precondition
			(and 
				(in r421)
			)
		:effect
			(and
				(not (search_again))
				(not (in r421))
				(in r420)
			)
	)

	(:action move-right-from-r422
		:precondition
			(and 
				(in r422)
			)
		:effect
			(and
				(not (search_again))
				(not (in r422))
				(in r423)
			)
	)


	(:action move-left-from-r422
		:precondition
			(and 
				(in r422)
			)
		:effect
			(and
				(not (search_again))
				(not (in r422))
				(in r421)
			)
	)

	(:action move-right-from-r423
		:precondition
			(and 
				(in r423)
			)
		:effect
			(and
				(not (search_again))
				(not (in r423))
				(in r424)
			)
	)


	(:action move-left-from-r423
		:precondition
			(and 
				(in r423)
			)
		:effect
			(and
				(not (search_again))
				(not (in r423))
				(in r422)
			)
	)

	(:action move-right-from-r424
		:precondition
			(and 
				(in r424)
			)
		:effect
			(and
				(not (search_again))
				(not (in r424))
				(in r425)
			)
	)


	(:action move-left-from-r424
		:precondition
			(and 
				(in r424)
			)
		:effect
			(and
				(not (search_again))
				(not (in r424))
				(in r423)
			)
	)

	(:action move-right-from-r425
		:precondition
			(and 
				(in r425)
			)
		:effect
			(and
				(not (search_again))
				(not (in r425))
				(in r426)
			)
	)


	(:action move-left-from-r425
		:precondition
			(and 
				(in r425)
			)
		:effect
			(and
				(not (search_again))
				(not (in r425))
				(in r424)
			)
	)

	(:action move-right-from-r426
		:precondition
			(and 
				(in r426)
			)
		:effect
			(and
				(not (search_again))
				(not (in r426))
				(in r427)
			)
	)


	(:action move-left-from-r426
		:precondition
			(and 
				(in r426)
			)
		:effect
			(and
				(not (search_again))
				(not (in r426))
				(in r425)
			)
	)

	(:action move-right-from-r427
		:precondition
			(and 
				(in r427)
			)
		:effect
			(and
				(not (search_again))
				(not (in r427))
				(in r428)
			)
	)


	(:action move-left-from-r427
		:precondition
			(and 
				(in r427)
			)
		:effect
			(and
				(not (search_again))
				(not (in r427))
				(in r426)
			)
	)

	(:action move-right-from-r428
		:precondition
			(and 
				(in r428)
			)
		:effect
			(and
				(not (search_again))
				(not (in r428))
				(in r429)
			)
	)


	(:action move-left-from-r428
		:precondition
			(and 
				(in r428)
			)
		:effect
			(and
				(not (search_again))
				(not (in r428))
				(in r427)
			)
	)

	(:action move-right-from-r429
		:precondition
			(and 
				(in r429)
			)
		:effect
			(and
				(not (search_again))
				(not (in r429))
				(in r430)
			)
	)


	(:action move-left-from-r429
		:precondition
			(and 
				(in r429)
			)
		:effect
			(and
				(not (search_again))
				(not (in r429))
				(in r428)
			)
	)

	(:action move-right-from-r430
		:precondition
			(and 
				(in r430)
			)
		:effect
			(and
				(not (search_again))
				(not (in r430))
				(in r431)
			)
	)


	(:action move-left-from-r430
		:precondition
			(and 
				(in r430)
			)
		:effect
			(and
				(not (search_again))
				(not (in r430))
				(in r429)
			)
	)

	(:action move-right-from-r431
		:precondition
			(and 
				(in r431)
			)
		:effect
			(and
				(not (search_again))
				(not (in r431))
				(in r432)
			)
	)


	(:action move-left-from-r431
		:precondition
			(and 
				(in r431)
			)
		:effect
			(and
				(not (search_again))
				(not (in r431))
				(in r430)
			)
	)

	(:action move-right-from-r432
		:precondition
			(and 
				(in r432)
			)
		:effect
			(and
				(not (search_again))
				(not (in r432))
				(in r433)
			)
	)


	(:action move-left-from-r432
		:precondition
			(and 
				(in r432)
			)
		:effect
			(and
				(not (search_again))
				(not (in r432))
				(in r431)
			)
	)

	(:action move-right-from-r433
		:precondition
			(and 
				(in r433)
			)
		:effect
			(and
				(not (search_again))
				(not (in r433))
				(in r434)
			)
	)


	(:action move-left-from-r433
		:precondition
			(and 
				(in r433)
			)
		:effect
			(and
				(not (search_again))
				(not (in r433))
				(in r432)
			)
	)

	(:action move-right-from-r434
		:precondition
			(and 
				(in r434)
			)
		:effect
			(and
				(not (search_again))
				(not (in r434))
				(in r435)
			)
	)


	(:action move-left-from-r434
		:precondition
			(and 
				(in r434)
			)
		:effect
			(and
				(not (search_again))
				(not (in r434))
				(in r433)
			)
	)

	(:action move-right-from-r435
		:precondition
			(and 
				(in r435)
			)
		:effect
			(and
				(not (search_again))
				(not (in r435))
				(in r436)
			)
	)


	(:action move-left-from-r435
		:precondition
			(and 
				(in r435)
			)
		:effect
			(and
				(not (search_again))
				(not (in r435))
				(in r434)
			)
	)

	(:action move-right-from-r436
		:precondition
			(and 
				(in r436)
			)
		:effect
			(and
				(not (search_again))
				(not (in r436))
				(in r437)
			)
	)


	(:action move-left-from-r436
		:precondition
			(and 
				(in r436)
			)
		:effect
			(and
				(not (search_again))
				(not (in r436))
				(in r435)
			)
	)

	(:action move-right-from-r437
		:precondition
			(and 
				(in r437)
			)
		:effect
			(and
				(not (search_again))
				(not (in r437))
				(in r438)
			)
	)


	(:action move-left-from-r437
		:precondition
			(and 
				(in r437)
			)
		:effect
			(and
				(not (search_again))
				(not (in r437))
				(in r436)
			)
	)

	(:action move-right-from-r438
		:precondition
			(and 
				(in r438)
			)
		:effect
			(and
				(not (search_again))
				(not (in r438))
				(in r439)
			)
	)


	(:action move-left-from-r438
		:precondition
			(and 
				(in r438)
			)
		:effect
			(and
				(not (search_again))
				(not (in r438))
				(in r437)
			)
	)

	(:action move-right-from-r439
		:precondition
			(and 
				(in r439)
			)
		:effect
			(and
				(not (search_again))
				(not (in r439))
				(in r440)
			)
	)


	(:action move-left-from-r439
		:precondition
			(and 
				(in r439)
			)
		:effect
			(and
				(not (search_again))
				(not (in r439))
				(in r438)
			)
	)

	(:action move-right-from-r440
		:precondition
			(and 
				(in r440)
			)
		:effect
			(and
				(not (search_again))
				(not (in r440))
				(in r441)
			)
	)


	(:action move-left-from-r440
		:precondition
			(and 
				(in r440)
			)
		:effect
			(and
				(not (search_again))
				(not (in r440))
				(in r439)
			)
	)

	(:action move-right-from-r441
		:precondition
			(and 
				(in r441)
			)
		:effect
			(and
				(not (search_again))
				(not (in r441))
				(in r442)
			)
	)


	(:action move-left-from-r441
		:precondition
			(and 
				(in r441)
			)
		:effect
			(and
				(not (search_again))
				(not (in r441))
				(in r440)
			)
	)

	(:action move-right-from-r442
		:precondition
			(and 
				(in r442)
			)
		:effect
			(and
				(not (search_again))
				(not (in r442))
				(in r443)
			)
	)


	(:action move-left-from-r442
		:precondition
			(and 
				(in r442)
			)
		:effect
			(and
				(not (search_again))
				(not (in r442))
				(in r441)
			)
	)

	(:action move-right-from-r443
		:precondition
			(and 
				(in r443)
			)
		:effect
			(and
				(not (search_again))
				(not (in r443))
				(in r444)
			)
	)


	(:action move-left-from-r443
		:precondition
			(and 
				(in r443)
			)
		:effect
			(and
				(not (search_again))
				(not (in r443))
				(in r442)
			)
	)

	(:action move-right-from-r444
		:precondition
			(and 
				(in r444)
			)
		:effect
			(and
				(not (search_again))
				(not (in r444))
				(in r445)
			)
	)


	(:action move-left-from-r444
		:precondition
			(and 
				(in r444)
			)
		:effect
			(and
				(not (search_again))
				(not (in r444))
				(in r443)
			)
	)

	(:action move-right-from-r445
		:precondition
			(and 
				(in r445)
			)
		:effect
			(and
				(not (search_again))
				(not (in r445))
				(in r446)
			)
	)


	(:action move-left-from-r445
		:precondition
			(and 
				(in r445)
			)
		:effect
			(and
				(not (search_again))
				(not (in r445))
				(in r444)
			)
	)

	(:action move-right-from-r446
		:precondition
			(and 
				(in r446)
			)
		:effect
			(and
				(not (search_again))
				(not (in r446))
				(in r447)
			)
	)


	(:action move-left-from-r446
		:precondition
			(and 
				(in r446)
			)
		:effect
			(and
				(not (search_again))
				(not (in r446))
				(in r445)
			)
	)

	(:action move-right-from-r447
		:precondition
			(and 
				(in r447)
			)
		:effect
			(and
				(not (search_again))
				(not (in r447))
				(in r448)
			)
	)


	(:action move-left-from-r447
		:precondition
			(and 
				(in r447)
			)
		:effect
			(and
				(not (search_again))
				(not (in r447))
				(in r446)
			)
	)

	(:action move-right-from-r448
		:precondition
			(and 
				(in r448)
			)
		:effect
			(and
				(not (search_again))
				(not (in r448))
				(in r449)
			)
	)


	(:action move-left-from-r448
		:precondition
			(and 
				(in r448)
			)
		:effect
			(and
				(not (search_again))
				(not (in r448))
				(in r447)
			)
	)

	(:action move-right-from-r449
		:precondition
			(and 
				(in r449)
			)
		:effect
			(and
				(not (search_again))
				(not (in r449))
				(in r450)
			)
	)


	(:action move-left-from-r449
		:precondition
			(and 
				(in r449)
			)
		:effect
			(and
				(not (search_again))
				(not (in r449))
				(in r448)
			)
	)

	(:action move-right-from-r450
		:precondition
			(and 
				(in r450)
			)
		:effect
			(and
				(not (search_again))
				(not (in r450))
				(in r451)
			)
	)


	(:action move-left-from-r450
		:precondition
			(and 
				(in r450)
			)
		:effect
			(and
				(not (search_again))
				(not (in r450))
				(in r449)
			)
	)

	(:action move-right-from-r451
		:precondition
			(and 
				(in r451)
			)
		:effect
			(and
				(not (search_again))
				(not (in r451))
				(in r452)
			)
	)


	(:action move-left-from-r451
		:precondition
			(and 
				(in r451)
			)
		:effect
			(and
				(not (search_again))
				(not (in r451))
				(in r450)
			)
	)

	(:action move-right-from-r452
		:precondition
			(and 
				(in r452)
			)
		:effect
			(and
				(not (search_again))
				(not (in r452))
				(in r453)
			)
	)


	(:action move-left-from-r452
		:precondition
			(and 
				(in r452)
			)
		:effect
			(and
				(not (search_again))
				(not (in r452))
				(in r451)
			)
	)

	(:action move-right-from-r453
		:precondition
			(and 
				(in r453)
			)
		:effect
			(and
				(not (search_again))
				(not (in r453))
				(in r454)
			)
	)


	(:action move-left-from-r453
		:precondition
			(and 
				(in r453)
			)
		:effect
			(and
				(not (search_again))
				(not (in r453))
				(in r452)
			)
	)

	(:action move-right-from-r454
		:precondition
			(and 
				(in r454)
			)
		:effect
			(and
				(not (search_again))
				(not (in r454))
				(in r455)
			)
	)


	(:action move-left-from-r454
		:precondition
			(and 
				(in r454)
			)
		:effect
			(and
				(not (search_again))
				(not (in r454))
				(in r453)
			)
	)

	(:action move-right-from-r455
		:precondition
			(and 
				(in r455)
			)
		:effect
			(and
				(not (search_again))
				(not (in r455))
				(in r456)
			)
	)


	(:action move-left-from-r455
		:precondition
			(and 
				(in r455)
			)
		:effect
			(and
				(not (search_again))
				(not (in r455))
				(in r454)
			)
	)

	(:action move-right-from-r456
		:precondition
			(and 
				(in r456)
			)
		:effect
			(and
				(not (search_again))
				(not (in r456))
				(in r457)
			)
	)


	(:action move-left-from-r456
		:precondition
			(and 
				(in r456)
			)
		:effect
			(and
				(not (search_again))
				(not (in r456))
				(in r455)
			)
	)

	(:action move-right-from-r457
		:precondition
			(and 
				(in r457)
			)
		:effect
			(and
				(not (search_again))
				(not (in r457))
				(in r458)
			)
	)


	(:action move-left-from-r457
		:precondition
			(and 
				(in r457)
			)
		:effect
			(and
				(not (search_again))
				(not (in r457))
				(in r456)
			)
	)

	(:action move-right-from-r458
		:precondition
			(and 
				(in r458)
			)
		:effect
			(and
				(not (search_again))
				(not (in r458))
				(in r459)
			)
	)


	(:action move-left-from-r458
		:precondition
			(and 
				(in r458)
			)
		:effect
			(and
				(not (search_again))
				(not (in r458))
				(in r457)
			)
	)

	(:action move-right-from-r459
		:precondition
			(and 
				(in r459)
			)
		:effect
			(and
				(not (search_again))
				(not (in r459))
				(in r460)
			)
	)


	(:action move-left-from-r459
		:precondition
			(and 
				(in r459)
			)
		:effect
			(and
				(not (search_again))
				(not (in r459))
				(in r458)
			)
	)

	(:action move-right-from-r460
		:precondition
			(and 
				(in r460)
			)
		:effect
			(and
				(not (search_again))
				(not (in r460))
				(in r461)
			)
	)


	(:action move-left-from-r460
		:precondition
			(and 
				(in r460)
			)
		:effect
			(and
				(not (search_again))
				(not (in r460))
				(in r459)
			)
	)

	(:action move-right-from-r461
		:precondition
			(and 
				(in r461)
			)
		:effect
			(and
				(not (search_again))
				(not (in r461))
				(in r462)
			)
	)


	(:action move-left-from-r461
		:precondition
			(and 
				(in r461)
			)
		:effect
			(and
				(not (search_again))
				(not (in r461))
				(in r460)
			)
	)

	(:action move-right-from-r462
		:precondition
			(and 
				(in r462)
			)
		:effect
			(and
				(not (search_again))
				(not (in r462))
				(in r463)
			)
	)


	(:action move-left-from-r462
		:precondition
			(and 
				(in r462)
			)
		:effect
			(and
				(not (search_again))
				(not (in r462))
				(in r461)
			)
	)

	(:action move-right-from-r463
		:precondition
			(and 
				(in r463)
			)
		:effect
			(and
				(not (search_again))
				(not (in r463))
				(in r464)
			)
	)


	(:action move-left-from-r463
		:precondition
			(and 
				(in r463)
			)
		:effect
			(and
				(not (search_again))
				(not (in r463))
				(in r462)
			)
	)

	(:action move-right-from-r464
		:precondition
			(and 
				(in r464)
			)
		:effect
			(and
				(not (search_again))
				(not (in r464))
				(in r465)
			)
	)


	(:action move-left-from-r464
		:precondition
			(and 
				(in r464)
			)
		:effect
			(and
				(not (search_again))
				(not (in r464))
				(in r463)
			)
	)

	(:action move-right-from-r465
		:precondition
			(and 
				(in r465)
			)
		:effect
			(and
				(not (search_again))
				(not (in r465))
				(in r466)
			)
	)


	(:action move-left-from-r465
		:precondition
			(and 
				(in r465)
			)
		:effect
			(and
				(not (search_again))
				(not (in r465))
				(in r464)
			)
	)

	(:action move-right-from-r466
		:precondition
			(and 
				(in r466)
			)
		:effect
			(and
				(not (search_again))
				(not (in r466))
				(in r467)
			)
	)


	(:action move-left-from-r466
		:precondition
			(and 
				(in r466)
			)
		:effect
			(and
				(not (search_again))
				(not (in r466))
				(in r465)
			)
	)

	(:action move-right-from-r467
		:precondition
			(and 
				(in r467)
			)
		:effect
			(and
				(not (search_again))
				(not (in r467))
				(in r468)
			)
	)


	(:action move-left-from-r467
		:precondition
			(and 
				(in r467)
			)
		:effect
			(and
				(not (search_again))
				(not (in r467))
				(in r466)
			)
	)

	(:action move-right-from-r468
		:precondition
			(and 
				(in r468)
			)
		:effect
			(and
				(not (search_again))
				(not (in r468))
				(in r469)
			)
	)


	(:action move-left-from-r468
		:precondition
			(and 
				(in r468)
			)
		:effect
			(and
				(not (search_again))
				(not (in r468))
				(in r467)
			)
	)

	(:action move-right-from-r469
		:precondition
			(and 
				(in r469)
			)
		:effect
			(and
				(not (search_again))
				(not (in r469))
				(in r470)
			)
	)


	(:action move-left-from-r469
		:precondition
			(and 
				(in r469)
			)
		:effect
			(and
				(not (search_again))
				(not (in r469))
				(in r468)
			)
	)

	(:action move-right-from-r470
		:precondition
			(and 
				(in r470)
			)
		:effect
			(and
				(not (search_again))
				(not (in r470))
				(in r471)
			)
	)


	(:action move-left-from-r470
		:precondition
			(and 
				(in r470)
			)
		:effect
			(and
				(not (search_again))
				(not (in r470))
				(in r469)
			)
	)

	(:action move-right-from-r471
		:precondition
			(and 
				(in r471)
			)
		:effect
			(and
				(not (search_again))
				(not (in r471))
				(in r472)
			)
	)


	(:action move-left-from-r471
		:precondition
			(and 
				(in r471)
			)
		:effect
			(and
				(not (search_again))
				(not (in r471))
				(in r470)
			)
	)

	(:action move-right-from-r472
		:precondition
			(and 
				(in r472)
			)
		:effect
			(and
				(not (search_again))
				(not (in r472))
				(in r473)
			)
	)


	(:action move-left-from-r472
		:precondition
			(and 
				(in r472)
			)
		:effect
			(and
				(not (search_again))
				(not (in r472))
				(in r471)
			)
	)

	(:action move-right-from-r473
		:precondition
			(and 
				(in r473)
			)
		:effect
			(and
				(not (search_again))
				(not (in r473))
				(in r474)
			)
	)


	(:action move-left-from-r473
		:precondition
			(and 
				(in r473)
			)
		:effect
			(and
				(not (search_again))
				(not (in r473))
				(in r472)
			)
	)

	(:action move-right-from-r474
		:precondition
			(and 
				(in r474)
			)
		:effect
			(and
				(not (search_again))
				(not (in r474))
				(in r475)
			)
	)


	(:action move-left-from-r474
		:precondition
			(and 
				(in r474)
			)
		:effect
			(and
				(not (search_again))
				(not (in r474))
				(in r473)
			)
	)

	(:action move-right-from-r475
		:precondition
			(and 
				(in r475)
			)
		:effect
			(and
				(not (search_again))
				(not (in r475))
				(in r476)
			)
	)


	(:action move-left-from-r475
		:precondition
			(and 
				(in r475)
			)
		:effect
			(and
				(not (search_again))
				(not (in r475))
				(in r474)
			)
	)

	(:action move-right-from-r476
		:precondition
			(and 
				(in r476)
			)
		:effect
			(and
				(not (search_again))
				(not (in r476))
				(in r477)
			)
	)


	(:action move-left-from-r476
		:precondition
			(and 
				(in r476)
			)
		:effect
			(and
				(not (search_again))
				(not (in r476))
				(in r475)
			)
	)

	(:action move-right-from-r477
		:precondition
			(and 
				(in r477)
			)
		:effect
			(and
				(not (search_again))
				(not (in r477))
				(in r478)
			)
	)


	(:action move-left-from-r477
		:precondition
			(and 
				(in r477)
			)
		:effect
			(and
				(not (search_again))
				(not (in r477))
				(in r476)
			)
	)

	(:action move-right-from-r478
		:precondition
			(and 
				(in r478)
			)
		:effect
			(and
				(not (search_again))
				(not (in r478))
				(in r479)
			)
	)


	(:action move-left-from-r478
		:precondition
			(and 
				(in r478)
			)
		:effect
			(and
				(not (search_again))
				(not (in r478))
				(in r477)
			)
	)

	(:action move-right-from-r479
		:precondition
			(and 
				(in r479)
			)
		:effect
			(and
				(not (search_again))
				(not (in r479))
				(in r480)
			)
	)


	(:action move-left-from-r479
		:precondition
			(and 
				(in r479)
			)
		:effect
			(and
				(not (search_again))
				(not (in r479))
				(in r478)
			)
	)

	(:action move-right-from-r480
		:precondition
			(and 
				(in r480)
			)
		:effect
			(and
				(not (search_again))
				(not (in r480))
				(in r481)
			)
	)


	(:action move-left-from-r480
		:precondition
			(and 
				(in r480)
			)
		:effect
			(and
				(not (search_again))
				(not (in r480))
				(in r479)
			)
	)

	(:action move-right-from-r481
		:precondition
			(and 
				(in r481)
			)
		:effect
			(and
				(not (search_again))
				(not (in r481))
				(in r482)
			)
	)


	(:action move-left-from-r481
		:precondition
			(and 
				(in r481)
			)
		:effect
			(and
				(not (search_again))
				(not (in r481))
				(in r480)
			)
	)

	(:action move-right-from-r482
		:precondition
			(and 
				(in r482)
			)
		:effect
			(and
				(not (search_again))
				(not (in r482))
				(in r483)
			)
	)


	(:action move-left-from-r482
		:precondition
			(and 
				(in r482)
			)
		:effect
			(and
				(not (search_again))
				(not (in r482))
				(in r481)
			)
	)

	(:action move-right-from-r483
		:precondition
			(and 
				(in r483)
			)
		:effect
			(and
				(not (search_again))
				(not (in r483))
				(in r484)
			)
	)


	(:action move-left-from-r483
		:precondition
			(and 
				(in r483)
			)
		:effect
			(and
				(not (search_again))
				(not (in r483))
				(in r482)
			)
	)

	(:action move-right-from-r484
		:precondition
			(and 
				(in r484)
			)
		:effect
			(and
				(not (search_again))
				(not (in r484))
				(in r485)
			)
	)


	(:action move-left-from-r484
		:precondition
			(and 
				(in r484)
			)
		:effect
			(and
				(not (search_again))
				(not (in r484))
				(in r483)
			)
	)

	(:action move-right-from-r485
		:precondition
			(and 
				(in r485)
			)
		:effect
			(and
				(not (search_again))
				(not (in r485))
				(in r486)
			)
	)


	(:action move-left-from-r485
		:precondition
			(and 
				(in r485)
			)
		:effect
			(and
				(not (search_again))
				(not (in r485))
				(in r484)
			)
	)

	(:action move-right-from-r486
		:precondition
			(and 
				(in r486)
			)
		:effect
			(and
				(not (search_again))
				(not (in r486))
				(in r487)
			)
	)


	(:action move-left-from-r486
		:precondition
			(and 
				(in r486)
			)
		:effect
			(and
				(not (search_again))
				(not (in r486))
				(in r485)
			)
	)

	(:action move-right-from-r487
		:precondition
			(and 
				(in r487)
			)
		:effect
			(and
				(not (search_again))
				(not (in r487))
				(in r488)
			)
	)


	(:action move-left-from-r487
		:precondition
			(and 
				(in r487)
			)
		:effect
			(and
				(not (search_again))
				(not (in r487))
				(in r486)
			)
	)

	(:action move-right-from-r488
		:precondition
			(and 
				(in r488)
			)
		:effect
			(and
				(not (search_again))
				(not (in r488))
				(in r489)
			)
	)


	(:action move-left-from-r488
		:precondition
			(and 
				(in r488)
			)
		:effect
			(and
				(not (search_again))
				(not (in r488))
				(in r487)
			)
	)

	(:action move-right-from-r489
		:precondition
			(and 
				(in r489)
			)
		:effect
			(and
				(not (search_again))
				(not (in r489))
				(in r490)
			)
	)


	(:action move-left-from-r489
		:precondition
			(and 
				(in r489)
			)
		:effect
			(and
				(not (search_again))
				(not (in r489))
				(in r488)
			)
	)

	(:action move-right-from-r490
		:precondition
			(and 
				(in r490)
			)
		:effect
			(and
				(not (search_again))
				(not (in r490))
				(in r491)
			)
	)


	(:action move-left-from-r490
		:precondition
			(and 
				(in r490)
			)
		:effect
			(and
				(not (search_again))
				(not (in r490))
				(in r489)
			)
	)

	(:action move-right-from-r491
		:precondition
			(and 
				(in r491)
			)
		:effect
			(and
				(not (search_again))
				(not (in r491))
				(in r492)
			)
	)


	(:action move-left-from-r491
		:precondition
			(and 
				(in r491)
			)
		:effect
			(and
				(not (search_again))
				(not (in r491))
				(in r490)
			)
	)

	(:action move-right-from-r492
		:precondition
			(and 
				(in r492)
			)
		:effect
			(and
				(not (search_again))
				(not (in r492))
				(in r493)
			)
	)


	(:action move-left-from-r492
		:precondition
			(and 
				(in r492)
			)
		:effect
			(and
				(not (search_again))
				(not (in r492))
				(in r491)
			)
	)

	(:action move-right-from-r493
		:precondition
			(and 
				(in r493)
			)
		:effect
			(and
				(not (search_again))
				(not (in r493))
				(in r494)
			)
	)


	(:action move-left-from-r493
		:precondition
			(and 
				(in r493)
			)
		:effect
			(and
				(not (search_again))
				(not (in r493))
				(in r492)
			)
	)

	(:action move-right-from-r494
		:precondition
			(and 
				(in r494)
			)
		:effect
			(and
				(not (search_again))
				(not (in r494))
				(in r495)
			)
	)


	(:action move-left-from-r494
		:precondition
			(and 
				(in r494)
			)
		:effect
			(and
				(not (search_again))
				(not (in r494))
				(in r493)
			)
	)

	(:action move-right-from-r495
		:precondition
			(and 
				(in r495)
			)
		:effect
			(and
				(not (search_again))
				(not (in r495))
				(in r496)
			)
	)


	(:action move-left-from-r495
		:precondition
			(and 
				(in r495)
			)
		:effect
			(and
				(not (search_again))
				(not (in r495))
				(in r494)
			)
	)

	(:action move-right-from-r496
		:precondition
			(and 
				(in r496)
			)
		:effect
			(and
				(not (search_again))
				(not (in r496))
				(in r497)
			)
	)


	(:action move-left-from-r496
		:precondition
			(and 
				(in r496)
			)
		:effect
			(and
				(not (search_again))
				(not (in r496))
				(in r495)
			)
	)

	(:action move-right-from-r497
		:precondition
			(and 
				(in r497)
			)
		:effect
			(and
				(not (search_again))
				(not (in r497))
				(in r498)
			)
	)


	(:action move-left-from-r497
		:precondition
			(and 
				(in r497)
			)
		:effect
			(and
				(not (search_again))
				(not (in r497))
				(in r496)
			)
	)

	(:action move-right-from-r498
		:precondition
			(and 
				(in r498)
			)
		:effect
			(and
				(not (search_again))
				(not (in r498))
				(in r499)
			)
	)


	(:action move-left-from-r498
		:precondition
			(and 
				(in r498)
			)
		:effect
			(and
				(not (search_again))
				(not (in r498))
				(in r497)
			)
	)

	(:action move-right-from-r499
		:precondition
			(and 
				(in r499)
			)
		:effect
			(and
				(not (search_again))
				(not (in r499))
				(in r500)
			)
	)


	(:action move-left-from-r499
		:precondition
			(and 
				(in r499)
			)
		:effect
			(and
				(not (search_again))
				(not (in r499))
				(in r498)
			)
	)

	(:action move-right-from-r500
		:precondition
			(and 
				(in r500)
			)
		:effect
			(and
				(not (search_again))
				(not (in r500))
				(in r501)
			)
	)


	(:action move-left-from-r500
		:precondition
			(and 
				(in r500)
			)
		:effect
			(and
				(not (search_again))
				(not (in r500))
				(in r499)
			)
	)

	(:action move-right-from-r501
		:precondition
			(and 
				(in r501)
			)
		:effect
			(and
				(not (search_again))
				(not (in r501))
				(in r502)
			)
	)


	(:action move-left-from-r501
		:precondition
			(and 
				(in r501)
			)
		:effect
			(and
				(not (search_again))
				(not (in r501))
				(in r500)
			)
	)

	(:action move-right-from-r502
		:precondition
			(and 
				(in r502)
			)
		:effect
			(and
				(not (search_again))
				(not (in r502))
				(in r503)
			)
	)


	(:action move-left-from-r502
		:precondition
			(and 
				(in r502)
			)
		:effect
			(and
				(not (search_again))
				(not (in r502))
				(in r501)
			)
	)

	(:action move-right-from-r503
		:precondition
			(and 
				(in r503)
			)
		:effect
			(and
				(not (search_again))
				(not (in r503))
				(in r504)
			)
	)


	(:action move-left-from-r503
		:precondition
			(and 
				(in r503)
			)
		:effect
			(and
				(not (search_again))
				(not (in r503))
				(in r502)
			)
	)

	(:action move-right-from-r504
		:precondition
			(and 
				(in r504)
			)
		:effect
			(and
				(not (search_again))
				(not (in r504))
				(in r505)
			)
	)


	(:action move-left-from-r504
		:precondition
			(and 
				(in r504)
			)
		:effect
			(and
				(not (search_again))
				(not (in r504))
				(in r503)
			)
	)

	(:action move-right-from-r505
		:precondition
			(and 
				(in r505)
			)
		:effect
			(and
				(not (search_again))
				(not (in r505))
				(in r506)
			)
	)


	(:action move-left-from-r505
		:precondition
			(and 
				(in r505)
			)
		:effect
			(and
				(not (search_again))
				(not (in r505))
				(in r504)
			)
	)

	(:action move-right-from-r506
		:precondition
			(and 
				(in r506)
			)
		:effect
			(and
				(not (search_again))
				(not (in r506))
				(in r507)
			)
	)


	(:action move-left-from-r506
		:precondition
			(and 
				(in r506)
			)
		:effect
			(and
				(not (search_again))
				(not (in r506))
				(in r505)
			)
	)

	(:action move-right-from-r507
		:precondition
			(and 
				(in r507)
			)
		:effect
			(and
				(not (search_again))
				(not (in r507))
				(in r508)
			)
	)


	(:action move-left-from-r507
		:precondition
			(and 
				(in r507)
			)
		:effect
			(and
				(not (search_again))
				(not (in r507))
				(in r506)
			)
	)

	(:action move-right-from-r508
		:precondition
			(and 
				(in r508)
			)
		:effect
			(and
				(not (search_again))
				(not (in r508))
				(in r509)
			)
	)


	(:action move-left-from-r508
		:precondition
			(and 
				(in r508)
			)
		:effect
			(and
				(not (search_again))
				(not (in r508))
				(in r507)
			)
	)

	(:action move-right-from-r509
		:precondition
			(and 
				(in r509)
			)
		:effect
			(and
				(not (search_again))
				(not (in r509))
				(in r510)
			)
	)


	(:action move-left-from-r509
		:precondition
			(and 
				(in r509)
			)
		:effect
			(and
				(not (search_again))
				(not (in r509))
				(in r508)
			)
	)

	(:action move-right-from-r510
		:precondition
			(and 
				(in r510)
			)
		:effect
			(and
				(not (search_again))
				(not (in r510))
				(in r511)
			)
	)


	(:action move-left-from-r510
		:precondition
			(and 
				(in r510)
			)
		:effect
			(and
				(not (search_again))
				(not (in r510))
				(in r509)
			)
	)

	(:action move-right-from-r511
		:precondition
			(and 
				(in r511)
			)
		:effect
			(and
				(not (search_again))
				(not (in r511))
				(in r512)
			)
	)


	(:action move-left-from-r511
		:precondition
			(and 
				(in r511)
			)
		:effect
			(and
				(not (search_again))
				(not (in r511))
				(in r510)
			)
	)

	(:action move-right-from-r512
		:precondition
			(and 
				(in r512)
			)
		:effect
			(and
				(not (search_again))
				(not (in r512))
				(in r513)
			)
	)


	(:action move-left-from-r512
		:precondition
			(and 
				(in r512)
			)
		:effect
			(and
				(not (search_again))
				(not (in r512))
				(in r511)
			)
	)

	(:action move-right-from-r513
		:precondition
			(and 
				(in r513)
			)
		:effect
			(and
				(not (search_again))
				(not (in r513))
				(in r514)
			)
	)


	(:action move-left-from-r513
		:precondition
			(and 
				(in r513)
			)
		:effect
			(and
				(not (search_again))
				(not (in r513))
				(in r512)
			)
	)

	(:action move-right-from-r514
		:precondition
			(and 
				(in r514)
			)
		:effect
			(and
				(not (search_again))
				(not (in r514))
				(in r515)
			)
	)


	(:action move-left-from-r514
		:precondition
			(and 
				(in r514)
			)
		:effect
			(and
				(not (search_again))
				(not (in r514))
				(in r513)
			)
	)

	(:action move-right-from-r515
		:precondition
			(and 
				(in r515)
			)
		:effect
			(and
				(not (search_again))
				(not (in r515))
				(in r516)
			)
	)


	(:action move-left-from-r515
		:precondition
			(and 
				(in r515)
			)
		:effect
			(and
				(not (search_again))
				(not (in r515))
				(in r514)
			)
	)

	(:action move-right-from-r516
		:precondition
			(and 
				(in r516)
			)
		:effect
			(and
				(not (search_again))
				(not (in r516))
				(in r517)
			)
	)


	(:action move-left-from-r516
		:precondition
			(and 
				(in r516)
			)
		:effect
			(and
				(not (search_again))
				(not (in r516))
				(in r515)
			)
	)

	(:action move-right-from-r517
		:precondition
			(and 
				(in r517)
			)
		:effect
			(and
				(not (search_again))
				(not (in r517))
				(in r518)
			)
	)


	(:action move-left-from-r517
		:precondition
			(and 
				(in r517)
			)
		:effect
			(and
				(not (search_again))
				(not (in r517))
				(in r516)
			)
	)

	(:action move-right-from-r518
		:precondition
			(and 
				(in r518)
			)
		:effect
			(and
				(not (search_again))
				(not (in r518))
				(in r519)
			)
	)


	(:action move-left-from-r518
		:precondition
			(and 
				(in r518)
			)
		:effect
			(and
				(not (search_again))
				(not (in r518))
				(in r517)
			)
	)

	(:action move-right-from-r519
		:precondition
			(and 
				(in r519)
			)
		:effect
			(and
				(not (search_again))
				(not (in r519))
				(in r520)
			)
	)


	(:action move-left-from-r519
		:precondition
			(and 
				(in r519)
			)
		:effect
			(and
				(not (search_again))
				(not (in r519))
				(in r518)
			)
	)

	(:action move-right-from-r520
		:precondition
			(and 
				(in r520)
			)
		:effect
			(and
				(not (search_again))
				(not (in r520))
				(in r521)
			)
	)


	(:action move-left-from-r520
		:precondition
			(and 
				(in r520)
			)
		:effect
			(and
				(not (search_again))
				(not (in r520))
				(in r519)
			)
	)

	(:action move-right-from-r521
		:precondition
			(and 
				(in r521)
			)
		:effect
			(and
				(not (search_again))
				(not (in r521))
				(in r522)
			)
	)


	(:action move-left-from-r521
		:precondition
			(and 
				(in r521)
			)
		:effect
			(and
				(not (search_again))
				(not (in r521))
				(in r520)
			)
	)

	(:action move-right-from-r522
		:precondition
			(and 
				(in r522)
			)
		:effect
			(and
				(not (search_again))
				(not (in r522))
				(in r523)
			)
	)


	(:action move-left-from-r522
		:precondition
			(and 
				(in r522)
			)
		:effect
			(and
				(not (search_again))
				(not (in r522))
				(in r521)
			)
	)

	(:action move-right-from-r523
		:precondition
			(and 
				(in r523)
			)
		:effect
			(and
				(not (search_again))
				(not (in r523))
				(in r524)
			)
	)


	(:action move-left-from-r523
		:precondition
			(and 
				(in r523)
			)
		:effect
			(and
				(not (search_again))
				(not (in r523))
				(in r522)
			)
	)

	(:action move-right-from-r524
		:precondition
			(and 
				(in r524)
			)
		:effect
			(and
				(not (search_again))
				(not (in r524))
				(in r525)
			)
	)


	(:action move-left-from-r524
		:precondition
			(and 
				(in r524)
			)
		:effect
			(and
				(not (search_again))
				(not (in r524))
				(in r523)
			)
	)

	(:action move-right-from-r525
		:precondition
			(and 
				(in r525)
			)
		:effect
			(and
				(not (search_again))
				(not (in r525))
				(in r526)
			)
	)


	(:action move-left-from-r525
		:precondition
			(and 
				(in r525)
			)
		:effect
			(and
				(not (search_again))
				(not (in r525))
				(in r524)
			)
	)

	(:action move-right-from-r526
		:precondition
			(and 
				(in r526)
			)
		:effect
			(and
				(not (search_again))
				(not (in r526))
				(in r527)
			)
	)


	(:action move-left-from-r526
		:precondition
			(and 
				(in r526)
			)
		:effect
			(and
				(not (search_again))
				(not (in r526))
				(in r525)
			)
	)

	(:action move-right-from-r527
		:precondition
			(and 
				(in r527)
			)
		:effect
			(and
				(not (search_again))
				(not (in r527))
				(in r528)
			)
	)


	(:action move-left-from-r527
		:precondition
			(and 
				(in r527)
			)
		:effect
			(and
				(not (search_again))
				(not (in r527))
				(in r526)
			)
	)

	(:action move-right-from-r528
		:precondition
			(and 
				(in r528)
			)
		:effect
			(and
				(not (search_again))
				(not (in r528))
				(in r529)
			)
	)


	(:action move-left-from-r528
		:precondition
			(and 
				(in r528)
			)
		:effect
			(and
				(not (search_again))
				(not (in r528))
				(in r527)
			)
	)

	(:action move-right-from-r529
		:precondition
			(and 
				(in r529)
			)
		:effect
			(and
				(not (search_again))
				(not (in r529))
				(in r530)
			)
	)


	(:action move-left-from-r529
		:precondition
			(and 
				(in r529)
			)
		:effect
			(and
				(not (search_again))
				(not (in r529))
				(in r528)
			)
	)

	(:action move-right-from-r530
		:precondition
			(and 
				(in r530)
			)
		:effect
			(and
				(not (search_again))
				(not (in r530))
				(in r531)
			)
	)


	(:action move-left-from-r530
		:precondition
			(and 
				(in r530)
			)
		:effect
			(and
				(not (search_again))
				(not (in r530))
				(in r529)
			)
	)

	(:action move-right-from-r531
		:precondition
			(and 
				(in r531)
			)
		:effect
			(and
				(not (search_again))
				(not (in r531))
				(in r532)
			)
	)


	(:action move-left-from-r531
		:precondition
			(and 
				(in r531)
			)
		:effect
			(and
				(not (search_again))
				(not (in r531))
				(in r530)
			)
	)

	(:action move-right-from-r532
		:precondition
			(and 
				(in r532)
			)
		:effect
			(and
				(not (search_again))
				(not (in r532))
				(in r533)
			)
	)


	(:action move-left-from-r532
		:precondition
			(and 
				(in r532)
			)
		:effect
			(and
				(not (search_again))
				(not (in r532))
				(in r531)
			)
	)

	(:action move-right-from-r533
		:precondition
			(and 
				(in r533)
			)
		:effect
			(and
				(not (search_again))
				(not (in r533))
				(in r534)
			)
	)


	(:action move-left-from-r533
		:precondition
			(and 
				(in r533)
			)
		:effect
			(and
				(not (search_again))
				(not (in r533))
				(in r532)
			)
	)

	(:action move-right-from-r534
		:precondition
			(and 
				(in r534)
			)
		:effect
			(and
				(not (search_again))
				(not (in r534))
				(in r535)
			)
	)


	(:action move-left-from-r534
		:precondition
			(and 
				(in r534)
			)
		:effect
			(and
				(not (search_again))
				(not (in r534))
				(in r533)
			)
	)

	(:action move-right-from-r535
		:precondition
			(and 
				(in r535)
			)
		:effect
			(and
				(not (search_again))
				(not (in r535))
				(in r536)
			)
	)


	(:action move-left-from-r535
		:precondition
			(and 
				(in r535)
			)
		:effect
			(and
				(not (search_again))
				(not (in r535))
				(in r534)
			)
	)

	(:action move-right-from-r536
		:precondition
			(and 
				(in r536)
			)
		:effect
			(and
				(not (search_again))
				(not (in r536))
				(in r537)
			)
	)


	(:action move-left-from-r536
		:precondition
			(and 
				(in r536)
			)
		:effect
			(and
				(not (search_again))
				(not (in r536))
				(in r535)
			)
	)

	(:action move-right-from-r537
		:precondition
			(and 
				(in r537)
			)
		:effect
			(and
				(not (search_again))
				(not (in r537))
				(in r538)
			)
	)


	(:action move-left-from-r537
		:precondition
			(and 
				(in r537)
			)
		:effect
			(and
				(not (search_again))
				(not (in r537))
				(in r536)
			)
	)

	(:action move-right-from-r538
		:precondition
			(and 
				(in r538)
			)
		:effect
			(and
				(not (search_again))
				(not (in r538))
				(in r539)
			)
	)


	(:action move-left-from-r538
		:precondition
			(and 
				(in r538)
			)
		:effect
			(and
				(not (search_again))
				(not (in r538))
				(in r537)
			)
	)

	(:action move-right-from-r539
		:precondition
			(and 
				(in r539)
			)
		:effect
			(and
				(not (search_again))
				(not (in r539))
				(in r540)
			)
	)


	(:action move-left-from-r539
		:precondition
			(and 
				(in r539)
			)
		:effect
			(and
				(not (search_again))
				(not (in r539))
				(in r538)
			)
	)

	(:action move-right-from-r540
		:precondition
			(and 
				(in r540)
			)
		:effect
			(and
				(not (search_again))
				(not (in r540))
				(in r541)
			)
	)


	(:action move-left-from-r540
		:precondition
			(and 
				(in r540)
			)
		:effect
			(and
				(not (search_again))
				(not (in r540))
				(in r539)
			)
	)

	(:action move-right-from-r541
		:precondition
			(and 
				(in r541)
			)
		:effect
			(and
				(not (search_again))
				(not (in r541))
				(in r542)
			)
	)


	(:action move-left-from-r541
		:precondition
			(and 
				(in r541)
			)
		:effect
			(and
				(not (search_again))
				(not (in r541))
				(in r540)
			)
	)

	(:action move-right-from-r542
		:precondition
			(and 
				(in r542)
			)
		:effect
			(and
				(not (search_again))
				(not (in r542))
				(in r543)
			)
	)


	(:action move-left-from-r542
		:precondition
			(and 
				(in r542)
			)
		:effect
			(and
				(not (search_again))
				(not (in r542))
				(in r541)
			)
	)

	(:action move-right-from-r543
		:precondition
			(and 
				(in r543)
			)
		:effect
			(and
				(not (search_again))
				(not (in r543))
				(in r544)
			)
	)


	(:action move-left-from-r543
		:precondition
			(and 
				(in r543)
			)
		:effect
			(and
				(not (search_again))
				(not (in r543))
				(in r542)
			)
	)

	(:action move-right-from-r544
		:precondition
			(and 
				(in r544)
			)
		:effect
			(and
				(not (search_again))
				(not (in r544))
				(in r545)
			)
	)


	(:action move-left-from-r544
		:precondition
			(and 
				(in r544)
			)
		:effect
			(and
				(not (search_again))
				(not (in r544))
				(in r543)
			)
	)

	(:action move-right-from-r545
		:precondition
			(and 
				(in r545)
			)
		:effect
			(and
				(not (search_again))
				(not (in r545))
				(in r546)
			)
	)


	(:action move-left-from-r545
		:precondition
			(and 
				(in r545)
			)
		:effect
			(and
				(not (search_again))
				(not (in r545))
				(in r544)
			)
	)

	(:action move-right-from-r546
		:precondition
			(and 
				(in r546)
			)
		:effect
			(and
				(not (search_again))
				(not (in r546))
				(in r547)
			)
	)


	(:action move-left-from-r546
		:precondition
			(and 
				(in r546)
			)
		:effect
			(and
				(not (search_again))
				(not (in r546))
				(in r545)
			)
	)

	(:action move-right-from-r547
		:precondition
			(and 
				(in r547)
			)
		:effect
			(and
				(not (search_again))
				(not (in r547))
				(in r548)
			)
	)


	(:action move-left-from-r547
		:precondition
			(and 
				(in r547)
			)
		:effect
			(and
				(not (search_again))
				(not (in r547))
				(in r546)
			)
	)

	(:action move-right-from-r548
		:precondition
			(and 
				(in r548)
			)
		:effect
			(and
				(not (search_again))
				(not (in r548))
				(in r549)
			)
	)


	(:action move-left-from-r548
		:precondition
			(and 
				(in r548)
			)
		:effect
			(and
				(not (search_again))
				(not (in r548))
				(in r547)
			)
	)

	(:action move-right-from-r549
		:precondition
			(and 
				(in r549)
			)
		:effect
			(and
				(not (search_again))
				(not (in r549))
				(in r550)
			)
	)


	(:action move-left-from-r549
		:precondition
			(and 
				(in r549)
			)
		:effect
			(and
				(not (search_again))
				(not (in r549))
				(in r548)
			)
	)

	(:action move-right-from-r550
		:precondition
			(and 
				(in r550)
			)
		:effect
			(and
				(not (search_again))
				(not (in r550))
				(in r551)
			)
	)


	(:action move-left-from-r550
		:precondition
			(and 
				(in r550)
			)
		:effect
			(and
				(not (search_again))
				(not (in r550))
				(in r549)
			)
	)

	(:action move-right-from-r551
		:precondition
			(and 
				(in r551)
			)
		:effect
			(and
				(not (search_again))
				(not (in r551))
				(in r552)
			)
	)


	(:action move-left-from-r551
		:precondition
			(and 
				(in r551)
			)
		:effect
			(and
				(not (search_again))
				(not (in r551))
				(in r550)
			)
	)

	(:action move-right-from-r552
		:precondition
			(and 
				(in r552)
			)
		:effect
			(and
				(not (search_again))
				(not (in r552))
				(in r553)
			)
	)


	(:action move-left-from-r552
		:precondition
			(and 
				(in r552)
			)
		:effect
			(and
				(not (search_again))
				(not (in r552))
				(in r551)
			)
	)

	(:action move-right-from-r553
		:precondition
			(and 
				(in r553)
			)
		:effect
			(and
				(not (search_again))
				(not (in r553))
				(in r554)
			)
	)


	(:action move-left-from-r553
		:precondition
			(and 
				(in r553)
			)
		:effect
			(and
				(not (search_again))
				(not (in r553))
				(in r552)
			)
	)

	(:action move-right-from-r554
		:precondition
			(and 
				(in r554)
			)
		:effect
			(and
				(not (search_again))
				(not (in r554))
				(in r555)
			)
	)


	(:action move-left-from-r554
		:precondition
			(and 
				(in r554)
			)
		:effect
			(and
				(not (search_again))
				(not (in r554))
				(in r553)
			)
	)

	(:action move-right-from-r555
		:precondition
			(and 
				(in r555)
			)
		:effect
			(and
				(not (search_again))
				(not (in r555))
				(in r556)
			)
	)


	(:action move-left-from-r555
		:precondition
			(and 
				(in r555)
			)
		:effect
			(and
				(not (search_again))
				(not (in r555))
				(in r554)
			)
	)

	(:action move-right-from-r556
		:precondition
			(and 
				(in r556)
			)
		:effect
			(and
				(not (search_again))
				(not (in r556))
				(in r557)
			)
	)


	(:action move-left-from-r556
		:precondition
			(and 
				(in r556)
			)
		:effect
			(and
				(not (search_again))
				(not (in r556))
				(in r555)
			)
	)

	(:action move-right-from-r557
		:precondition
			(and 
				(in r557)
			)
		:effect
			(and
				(not (search_again))
				(not (in r557))
				(in r558)
			)
	)


	(:action move-left-from-r557
		:precondition
			(and 
				(in r557)
			)
		:effect
			(and
				(not (search_again))
				(not (in r557))
				(in r556)
			)
	)

	(:action move-right-from-r558
		:precondition
			(and 
				(in r558)
			)
		:effect
			(and
				(not (search_again))
				(not (in r558))
				(in r559)
			)
	)


	(:action move-left-from-r558
		:precondition
			(and 
				(in r558)
			)
		:effect
			(and
				(not (search_again))
				(not (in r558))
				(in r557)
			)
	)

	(:action move-right-from-r559
		:precondition
			(and 
				(in r559)
			)
		:effect
			(and
				(not (search_again))
				(not (in r559))
				(in r560)
			)
	)


	(:action move-left-from-r559
		:precondition
			(and 
				(in r559)
			)
		:effect
			(and
				(not (search_again))
				(not (in r559))
				(in r558)
			)
	)

	(:action move-right-from-r560
		:precondition
			(and 
				(in r560)
			)
		:effect
			(and
				(not (search_again))
				(not (in r560))
				(in r561)
			)
	)


	(:action move-left-from-r560
		:precondition
			(and 
				(in r560)
			)
		:effect
			(and
				(not (search_again))
				(not (in r560))
				(in r559)
			)
	)

	(:action move-right-from-r561
		:precondition
			(and 
				(in r561)
			)
		:effect
			(and
				(not (search_again))
				(not (in r561))
				(in r562)
			)
	)


	(:action move-left-from-r561
		:precondition
			(and 
				(in r561)
			)
		:effect
			(and
				(not (search_again))
				(not (in r561))
				(in r560)
			)
	)

	(:action move-right-from-r562
		:precondition
			(and 
				(in r562)
			)
		:effect
			(and
				(not (search_again))
				(not (in r562))
				(in r563)
			)
	)


	(:action move-left-from-r562
		:precondition
			(and 
				(in r562)
			)
		:effect
			(and
				(not (search_again))
				(not (in r562))
				(in r561)
			)
	)

	(:action move-right-from-r563
		:precondition
			(and 
				(in r563)
			)
		:effect
			(and
				(not (search_again))
				(not (in r563))
				(in r564)
			)
	)


	(:action move-left-from-r563
		:precondition
			(and 
				(in r563)
			)
		:effect
			(and
				(not (search_again))
				(not (in r563))
				(in r562)
			)
	)

	(:action move-right-from-r564
		:precondition
			(and 
				(in r564)
			)
		:effect
			(and
				(not (search_again))
				(not (in r564))
				(in r565)
			)
	)


	(:action move-left-from-r564
		:precondition
			(and 
				(in r564)
			)
		:effect
			(and
				(not (search_again))
				(not (in r564))
				(in r563)
			)
	)

	(:action move-right-from-r565
		:precondition
			(and 
				(in r565)
			)
		:effect
			(and
				(not (search_again))
				(not (in r565))
				(in r566)
			)
	)


	(:action move-left-from-r565
		:precondition
			(and 
				(in r565)
			)
		:effect
			(and
				(not (search_again))
				(not (in r565))
				(in r564)
			)
	)

	(:action move-right-from-r566
		:precondition
			(and 
				(in r566)
			)
		:effect
			(and
				(not (search_again))
				(not (in r566))
				(in r567)
			)
	)


	(:action move-left-from-r566
		:precondition
			(and 
				(in r566)
			)
		:effect
			(and
				(not (search_again))
				(not (in r566))
				(in r565)
			)
	)

	(:action move-right-from-r567
		:precondition
			(and 
				(in r567)
			)
		:effect
			(and
				(not (search_again))
				(not (in r567))
				(in r568)
			)
	)


	(:action move-left-from-r567
		:precondition
			(and 
				(in r567)
			)
		:effect
			(and
				(not (search_again))
				(not (in r567))
				(in r566)
			)
	)

	(:action move-right-from-r568
		:precondition
			(and 
				(in r568)
			)
		:effect
			(and
				(not (search_again))
				(not (in r568))
				(in r569)
			)
	)


	(:action move-left-from-r568
		:precondition
			(and 
				(in r568)
			)
		:effect
			(and
				(not (search_again))
				(not (in r568))
				(in r567)
			)
	)

	(:action move-right-from-r569
		:precondition
			(and 
				(in r569)
			)
		:effect
			(and
				(not (search_again))
				(not (in r569))
				(in r570)
			)
	)


	(:action move-left-from-r569
		:precondition
			(and 
				(in r569)
			)
		:effect
			(and
				(not (search_again))
				(not (in r569))
				(in r568)
			)
	)

	(:action move-right-from-r570
		:precondition
			(and 
				(in r570)
			)
		:effect
			(and
				(not (search_again))
				(not (in r570))
				(in r571)
			)
	)


	(:action move-left-from-r570
		:precondition
			(and 
				(in r570)
			)
		:effect
			(and
				(not (search_again))
				(not (in r570))
				(in r569)
			)
	)

	(:action move-right-from-r571
		:precondition
			(and 
				(in r571)
			)
		:effect
			(and
				(not (search_again))
				(not (in r571))
				(in r572)
			)
	)


	(:action move-left-from-r571
		:precondition
			(and 
				(in r571)
			)
		:effect
			(and
				(not (search_again))
				(not (in r571))
				(in r570)
			)
	)

	(:action move-right-from-r572
		:precondition
			(and 
				(in r572)
			)
		:effect
			(and
				(not (search_again))
				(not (in r572))
				(in r573)
			)
	)


	(:action move-left-from-r572
		:precondition
			(and 
				(in r572)
			)
		:effect
			(and
				(not (search_again))
				(not (in r572))
				(in r571)
			)
	)

	(:action move-right-from-r573
		:precondition
			(and 
				(in r573)
			)
		:effect
			(and
				(not (search_again))
				(not (in r573))
				(in r574)
			)
	)


	(:action move-left-from-r573
		:precondition
			(and 
				(in r573)
			)
		:effect
			(and
				(not (search_again))
				(not (in r573))
				(in r572)
			)
	)

	(:action move-right-from-r574
		:precondition
			(and 
				(in r574)
			)
		:effect
			(and
				(not (search_again))
				(not (in r574))
				(in r575)
			)
	)


	(:action move-left-from-r574
		:precondition
			(and 
				(in r574)
			)
		:effect
			(and
				(not (search_again))
				(not (in r574))
				(in r573)
			)
	)

	(:action move-right-from-r575
		:precondition
			(and 
				(in r575)
			)
		:effect
			(and
				(not (search_again))
				(not (in r575))
				(in r576)
			)
	)


	(:action move-left-from-r575
		:precondition
			(and 
				(in r575)
			)
		:effect
			(and
				(not (search_again))
				(not (in r575))
				(in r574)
			)
	)

	(:action move-right-from-r576
		:precondition
			(and 
				(in r576)
			)
		:effect
			(and
				(not (search_again))
				(not (in r576))
				(in r577)
			)
	)


	(:action move-left-from-r576
		:precondition
			(and 
				(in r576)
			)
		:effect
			(and
				(not (search_again))
				(not (in r576))
				(in r575)
			)
	)

	(:action move-right-from-r577
		:precondition
			(and 
				(in r577)
			)
		:effect
			(and
				(not (search_again))
				(not (in r577))
				(in r578)
			)
	)


	(:action move-left-from-r577
		:precondition
			(and 
				(in r577)
			)
		:effect
			(and
				(not (search_again))
				(not (in r577))
				(in r576)
			)
	)

	(:action move-right-from-r578
		:precondition
			(and 
				(in r578)
			)
		:effect
			(and
				(not (search_again))
				(not (in r578))
				(in r579)
			)
	)


	(:action move-left-from-r578
		:precondition
			(and 
				(in r578)
			)
		:effect
			(and
				(not (search_again))
				(not (in r578))
				(in r577)
			)
	)

	(:action move-right-from-r579
		:precondition
			(and 
				(in r579)
			)
		:effect
			(and
				(not (search_again))
				(not (in r579))
				(in r580)
			)
	)


	(:action move-left-from-r579
		:precondition
			(and 
				(in r579)
			)
		:effect
			(and
				(not (search_again))
				(not (in r579))
				(in r578)
			)
	)

	(:action move-right-from-r580
		:precondition
			(and 
				(in r580)
			)
		:effect
			(and
				(not (search_again))
				(not (in r580))
				(in r581)
			)
	)


	(:action move-left-from-r580
		:precondition
			(and 
				(in r580)
			)
		:effect
			(and
				(not (search_again))
				(not (in r580))
				(in r579)
			)
	)

	(:action move-right-from-r581
		:precondition
			(and 
				(in r581)
			)
		:effect
			(and
				(not (search_again))
				(not (in r581))
				(in r582)
			)
	)


	(:action move-left-from-r581
		:precondition
			(and 
				(in r581)
			)
		:effect
			(and
				(not (search_again))
				(not (in r581))
				(in r580)
			)
	)

	(:action move-right-from-r582
		:precondition
			(and 
				(in r582)
			)
		:effect
			(and
				(not (search_again))
				(not (in r582))
				(in r583)
			)
	)


	(:action move-left-from-r582
		:precondition
			(and 
				(in r582)
			)
		:effect
			(and
				(not (search_again))
				(not (in r582))
				(in r581)
			)
	)

	(:action move-right-from-r583
		:precondition
			(and 
				(in r583)
			)
		:effect
			(and
				(not (search_again))
				(not (in r583))
				(in r584)
			)
	)


	(:action move-left-from-r583
		:precondition
			(and 
				(in r583)
			)
		:effect
			(and
				(not (search_again))
				(not (in r583))
				(in r582)
			)
	)

	(:action move-right-from-r584
		:precondition
			(and 
				(in r584)
			)
		:effect
			(and
				(not (search_again))
				(not (in r584))
				(in r585)
			)
	)


	(:action move-left-from-r584
		:precondition
			(and 
				(in r584)
			)
		:effect
			(and
				(not (search_again))
				(not (in r584))
				(in r583)
			)
	)

	(:action move-right-from-r585
		:precondition
			(and 
				(in r585)
			)
		:effect
			(and
				(not (search_again))
				(not (in r585))
				(in r586)
			)
	)


	(:action move-left-from-r585
		:precondition
			(and 
				(in r585)
			)
		:effect
			(and
				(not (search_again))
				(not (in r585))
				(in r584)
			)
	)

	(:action move-right-from-r586
		:precondition
			(and 
				(in r586)
			)
		:effect
			(and
				(not (search_again))
				(not (in r586))
				(in r587)
			)
	)


	(:action move-left-from-r586
		:precondition
			(and 
				(in r586)
			)
		:effect
			(and
				(not (search_again))
				(not (in r586))
				(in r585)
			)
	)

	(:action move-right-from-r587
		:precondition
			(and 
				(in r587)
			)
		:effect
			(and
				(not (search_again))
				(not (in r587))
				(in r588)
			)
	)


	(:action move-left-from-r587
		:precondition
			(and 
				(in r587)
			)
		:effect
			(and
				(not (search_again))
				(not (in r587))
				(in r586)
			)
	)

	(:action move-right-from-r588
		:precondition
			(and 
				(in r588)
			)
		:effect
			(and
				(not (search_again))
				(not (in r588))
				(in r589)
			)
	)


	(:action move-left-from-r588
		:precondition
			(and 
				(in r588)
			)
		:effect
			(and
				(not (search_again))
				(not (in r588))
				(in r587)
			)
	)

	(:action move-right-from-r589
		:precondition
			(and 
				(in r589)
			)
		:effect
			(and
				(not (search_again))
				(not (in r589))
				(in r590)
			)
	)


	(:action move-left-from-r589
		:precondition
			(and 
				(in r589)
			)
		:effect
			(and
				(not (search_again))
				(not (in r589))
				(in r588)
			)
	)

	(:action move-right-from-r590
		:precondition
			(and 
				(in r590)
			)
		:effect
			(and
				(not (search_again))
				(not (in r590))
				(in r591)
			)
	)


	(:action move-left-from-r590
		:precondition
			(and 
				(in r590)
			)
		:effect
			(and
				(not (search_again))
				(not (in r590))
				(in r589)
			)
	)

	(:action move-right-from-r591
		:precondition
			(and 
				(in r591)
			)
		:effect
			(and
				(not (search_again))
				(not (in r591))
				(in r592)
			)
	)


	(:action move-left-from-r591
		:precondition
			(and 
				(in r591)
			)
		:effect
			(and
				(not (search_again))
				(not (in r591))
				(in r590)
			)
	)

	(:action move-right-from-r592
		:precondition
			(and 
				(in r592)
			)
		:effect
			(and
				(not (search_again))
				(not (in r592))
				(in r593)
			)
	)


	(:action move-left-from-r592
		:precondition
			(and 
				(in r592)
			)
		:effect
			(and
				(not (search_again))
				(not (in r592))
				(in r591)
			)
	)

	(:action move-right-from-r593
		:precondition
			(and 
				(in r593)
			)
		:effect
			(and
				(not (search_again))
				(not (in r593))
				(in r594)
			)
	)


	(:action move-left-from-r593
		:precondition
			(and 
				(in r593)
			)
		:effect
			(and
				(not (search_again))
				(not (in r593))
				(in r592)
			)
	)

	(:action move-right-from-r594
		:precondition
			(and 
				(in r594)
			)
		:effect
			(and
				(not (search_again))
				(not (in r594))
				(in r595)
			)
	)


	(:action move-left-from-r594
		:precondition
			(and 
				(in r594)
			)
		:effect
			(and
				(not (search_again))
				(not (in r594))
				(in r593)
			)
	)

	(:action move-right-from-r595
		:precondition
			(and 
				(in r595)
			)
		:effect
			(and
				(not (search_again))
				(not (in r595))
				(in r596)
			)
	)


	(:action move-left-from-r595
		:precondition
			(and 
				(in r595)
			)
		:effect
			(and
				(not (search_again))
				(not (in r595))
				(in r594)
			)
	)

	(:action move-right-from-r596
		:precondition
			(and 
				(in r596)
			)
		:effect
			(and
				(not (search_again))
				(not (in r596))
				(in r597)
			)
	)


	(:action move-left-from-r596
		:precondition
			(and 
				(in r596)
			)
		:effect
			(and
				(not (search_again))
				(not (in r596))
				(in r595)
			)
	)

	(:action move-right-from-r597
		:precondition
			(and 
				(in r597)
			)
		:effect
			(and
				(not (search_again))
				(not (in r597))
				(in r598)
			)
	)


	(:action move-left-from-r597
		:precondition
			(and 
				(in r597)
			)
		:effect
			(and
				(not (search_again))
				(not (in r597))
				(in r596)
			)
	)

	(:action move-right-from-r598
		:precondition
			(and 
				(in r598)
			)
		:effect
			(and
				(not (search_again))
				(not (in r598))
				(in r599)
			)
	)


	(:action move-left-from-r598
		:precondition
			(and 
				(in r598)
			)
		:effect
			(and
				(not (search_again))
				(not (in r598))
				(in r597)
			)
	)

	(:action move-right-from-r599
		:precondition
			(and 
				(in r599)
				(not (searched_in r600))
			)
		:effect
			(and
				(not (search_again))
				(not (in r599))
				(in r600)
				(oneof (not (seen)) (seen))
				(searched_in r600)
			)
	)


	(:action move-left-from-r599
		:precondition
			(and 
				(in r599)
			)
		:effect
			(and
				(not (search_again))
				(not (in r599))
				(in r598)
			)
	)

	(:action move-right-from-r600
		:precondition
			(and 
				(in r600)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r600))
				(in r1)
			)
	)


	(:action move-left-from-r600
		:precondition
			(and 
				(in r600)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r600))
				(in r599)
			)
	)


	(:action stay
		:precondition
			(f_ok 
			)
		:effect
			(and
			)
	)


	(:action searching_again
		:precondition
			(and 
				(searched_in r300)
				(searched_in r600)
				(not (seen))
			)
		:effect
			(and
				(not (searched_in r300))
				(not (searched_in r600))
				(search_again)
			)
	)

)

