
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Waldo example for 2400 rooms
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Taken from
;; Kress-Gazit, H., Fainekos, G.E., Pappas, G.J.
;; Where's Waldo? Sensor-Based Temporal Logic Motion Planning.
;; ICRA 2007: 3116-3121
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Goal formula:
;;
;; AE(r1200 || seen) && AE(r2400 || seen)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; []<> (search_again | seen)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain waldo)
	(:requirements :strips :typing :equality)
	(:types 
		room
	)
	(:constants
		r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 r11 r12 r13 r14 r15 r16 r17 r18 r19 r20 r21 r22 r23 r24 r25 r26 r27 r28 r29 r30 r31 r32 r33 r34 r35 r36 r37 r38 r39 r40 r41 r42 r43 r44 r45 r46 r47 r48 r49 r50 r51 r52 r53 r54 r55 r56 r57 r58 r59 r60 r61 r62 r63 r64 r65 r66 r67 r68 r69 r70 r71 r72 r73 r74 r75 r76 r77 r78 r79 r80 r81 r82 r83 r84 r85 r86 r87 r88 r89 r90 r91 r92 r93 r94 r95 r96 r97 r98 r99 r100 r101 r102 r103 r104 r105 r106 r107 r108 r109 r110 r111 r112 r113 r114 r115 r116 r117 r118 r119 r120 r121 r122 r123 r124 r125 r126 r127 r128 r129 r130 r131 r132 r133 r134 r135 r136 r137 r138 r139 r140 r141 r142 r143 r144 r145 r146 r147 r148 r149 r150 r151 r152 r153 r154 r155 r156 r157 r158 r159 r160 r161 r162 r163 r164 r165 r166 r167 r168 r169 r170 r171 r172 r173 r174 r175 r176 r177 r178 r179 r180 r181 r182 r183 r184 r185 r186 r187 r188 r189 r190 r191 r192 r193 r194 r195 r196 r197 r198 r199 r200 r201 r202 r203 r204 r205 r206 r207 r208 r209 r210 r211 r212 r213 r214 r215 r216 r217 r218 r219 r220 r221 r222 r223 r224 r225 r226 r227 r228 r229 r230 r231 r232 r233 r234 r235 r236 r237 r238 r239 r240 r241 r242 r243 r244 r245 r246 r247 r248 r249 r250 r251 r252 r253 r254 r255 r256 r257 r258 r259 r260 r261 r262 r263 r264 r265 r266 r267 r268 r269 r270 r271 r272 r273 r274 r275 r276 r277 r278 r279 r280 r281 r282 r283 r284 r285 r286 r287 r288 r289 r290 r291 r292 r293 r294 r295 r296 r297 r298 r299 r300 r301 r302 r303 r304 r305 r306 r307 r308 r309 r310 r311 r312 r313 r314 r315 r316 r317 r318 r319 r320 r321 r322 r323 r324 r325 r326 r327 r328 r329 r330 r331 r332 r333 r334 r335 r336 r337 r338 r339 r340 r341 r342 r343 r344 r345 r346 r347 r348 r349 r350 r351 r352 r353 r354 r355 r356 r357 r358 r359 r360 r361 r362 r363 r364 r365 r366 r367 r368 r369 r370 r371 r372 r373 r374 r375 r376 r377 r378 r379 r380 r381 r382 r383 r384 r385 r386 r387 r388 r389 r390 r391 r392 r393 r394 r395 r396 r397 r398 r399 r400 r401 r402 r403 r404 r405 r406 r407 r408 r409 r410 r411 r412 r413 r414 r415 r416 r417 r418 r419 r420 r421 r422 r423 r424 r425 r426 r427 r428 r429 r430 r431 r432 r433 r434 r435 r436 r437 r438 r439 r440 r441 r442 r443 r444 r445 r446 r447 r448 r449 r450 r451 r452 r453 r454 r455 r456 r457 r458 r459 r460 r461 r462 r463 r464 r465 r466 r467 r468 r469 r470 r471 r472 r473 r474 r475 r476 r477 r478 r479 r480 r481 r482 r483 r484 r485 r486 r487 r488 r489 r490 r491 r492 r493 r494 r495 r496 r497 r498 r499 r500 r501 r502 r503 r504 r505 r506 r507 r508 r509 r510 r511 r512 r513 r514 r515 r516 r517 r518 r519 r520 r521 r522 r523 r524 r525 r526 r527 r528 r529 r530 r531 r532 r533 r534 r535 r536 r537 r538 r539 r540 r541 r542 r543 r544 r545 r546 r547 r548 r549 r550 r551 r552 r553 r554 r555 r556 r557 r558 r559 r560 r561 r562 r563 r564 r565 r566 r567 r568 r569 r570 r571 r572 r573 r574 r575 r576 r577 r578 r579 r580 r581 r582 r583 r584 r585 r586 r587 r588 r589 r590 r591 r592 r593 r594 r595 r596 r597 r598 r599 r600 r601 r602 r603 r604 r605 r606 r607 r608 r609 r610 r611 r612 r613 r614 r615 r616 r617 r618 r619 r620 r621 r622 r623 r624 r625 r626 r627 r628 r629 r630 r631 r632 r633 r634 r635 r636 r637 r638 r639 r640 r641 r642 r643 r644 r645 r646 r647 r648 r649 r650 r651 r652 r653 r654 r655 r656 r657 r658 r659 r660 r661 r662 r663 r664 r665 r666 r667 r668 r669 r670 r671 r672 r673 r674 r675 r676 r677 r678 r679 r680 r681 r682 r683 r684 r685 r686 r687 r688 r689 r690 r691 r692 r693 r694 r695 r696 r697 r698 r699 r700 r701 r702 r703 r704 r705 r706 r707 r708 r709 r710 r711 r712 r713 r714 r715 r716 r717 r718 r719 r720 r721 r722 r723 r724 r725 r726 r727 r728 r729 r730 r731 r732 r733 r734 r735 r736 r737 r738 r739 r740 r741 r742 r743 r744 r745 r746 r747 r748 r749 r750 r751 r752 r753 r754 r755 r756 r757 r758 r759 r760 r761 r762 r763 r764 r765 r766 r767 r768 r769 r770 r771 r772 r773 r774 r775 r776 r777 r778 r779 r780 r781 r782 r783 r784 r785 r786 r787 r788 r789 r790 r791 r792 r793 r794 r795 r796 r797 r798 r799 r800 r801 r802 r803 r804 r805 r806 r807 r808 r809 r810 r811 r812 r813 r814 r815 r816 r817 r818 r819 r820 r821 r822 r823 r824 r825 r826 r827 r828 r829 r830 r831 r832 r833 r834 r835 r836 r837 r838 r839 r840 r841 r842 r843 r844 r845 r846 r847 r848 r849 r850 r851 r852 r853 r854 r855 r856 r857 r858 r859 r860 r861 r862 r863 r864 r865 r866 r867 r868 r869 r870 r871 r872 r873 r874 r875 r876 r877 r878 r879 r880 r881 r882 r883 r884 r885 r886 r887 r888 r889 r890 r891 r892 r893 r894 r895 r896 r897 r898 r899 r900 r901 r902 r903 r904 r905 r906 r907 r908 r909 r910 r911 r912 r913 r914 r915 r916 r917 r918 r919 r920 r921 r922 r923 r924 r925 r926 r927 r928 r929 r930 r931 r932 r933 r934 r935 r936 r937 r938 r939 r940 r941 r942 r943 r944 r945 r946 r947 r948 r949 r950 r951 r952 r953 r954 r955 r956 r957 r958 r959 r960 r961 r962 r963 r964 r965 r966 r967 r968 r969 r970 r971 r972 r973 r974 r975 r976 r977 r978 r979 r980 r981 r982 r983 r984 r985 r986 r987 r988 r989 r990 r991 r992 r993 r994 r995 r996 r997 r998 r999 r1000 r1001 r1002 r1003 r1004 r1005 r1006 r1007 r1008 r1009 r1010 r1011 r1012 r1013 r1014 r1015 r1016 r1017 r1018 r1019 r1020 r1021 r1022 r1023 r1024 r1025 r1026 r1027 r1028 r1029 r1030 r1031 r1032 r1033 r1034 r1035 r1036 r1037 r1038 r1039 r1040 r1041 r1042 r1043 r1044 r1045 r1046 r1047 r1048 r1049 r1050 r1051 r1052 r1053 r1054 r1055 r1056 r1057 r1058 r1059 r1060 r1061 r1062 r1063 r1064 r1065 r1066 r1067 r1068 r1069 r1070 r1071 r1072 r1073 r1074 r1075 r1076 r1077 r1078 r1079 r1080 r1081 r1082 r1083 r1084 r1085 r1086 r1087 r1088 r1089 r1090 r1091 r1092 r1093 r1094 r1095 r1096 r1097 r1098 r1099 r1100 r1101 r1102 r1103 r1104 r1105 r1106 r1107 r1108 r1109 r1110 r1111 r1112 r1113 r1114 r1115 r1116 r1117 r1118 r1119 r1120 r1121 r1122 r1123 r1124 r1125 r1126 r1127 r1128 r1129 r1130 r1131 r1132 r1133 r1134 r1135 r1136 r1137 r1138 r1139 r1140 r1141 r1142 r1143 r1144 r1145 r1146 r1147 r1148 r1149 r1150 r1151 r1152 r1153 r1154 r1155 r1156 r1157 r1158 r1159 r1160 r1161 r1162 r1163 r1164 r1165 r1166 r1167 r1168 r1169 r1170 r1171 r1172 r1173 r1174 r1175 r1176 r1177 r1178 r1179 r1180 r1181 r1182 r1183 r1184 r1185 r1186 r1187 r1188 r1189 r1190 r1191 r1192 r1193 r1194 r1195 r1196 r1197 r1198 r1199 r1200 r1201 r1202 r1203 r1204 r1205 r1206 r1207 r1208 r1209 r1210 r1211 r1212 r1213 r1214 r1215 r1216 r1217 r1218 r1219 r1220 r1221 r1222 r1223 r1224 r1225 r1226 r1227 r1228 r1229 r1230 r1231 r1232 r1233 r1234 r1235 r1236 r1237 r1238 r1239 r1240 r1241 r1242 r1243 r1244 r1245 r1246 r1247 r1248 r1249 r1250 r1251 r1252 r1253 r1254 r1255 r1256 r1257 r1258 r1259 r1260 r1261 r1262 r1263 r1264 r1265 r1266 r1267 r1268 r1269 r1270 r1271 r1272 r1273 r1274 r1275 r1276 r1277 r1278 r1279 r1280 r1281 r1282 r1283 r1284 r1285 r1286 r1287 r1288 r1289 r1290 r1291 r1292 r1293 r1294 r1295 r1296 r1297 r1298 r1299 r1300 r1301 r1302 r1303 r1304 r1305 r1306 r1307 r1308 r1309 r1310 r1311 r1312 r1313 r1314 r1315 r1316 r1317 r1318 r1319 r1320 r1321 r1322 r1323 r1324 r1325 r1326 r1327 r1328 r1329 r1330 r1331 r1332 r1333 r1334 r1335 r1336 r1337 r1338 r1339 r1340 r1341 r1342 r1343 r1344 r1345 r1346 r1347 r1348 r1349 r1350 r1351 r1352 r1353 r1354 r1355 r1356 r1357 r1358 r1359 r1360 r1361 r1362 r1363 r1364 r1365 r1366 r1367 r1368 r1369 r1370 r1371 r1372 r1373 r1374 r1375 r1376 r1377 r1378 r1379 r1380 r1381 r1382 r1383 r1384 r1385 r1386 r1387 r1388 r1389 r1390 r1391 r1392 r1393 r1394 r1395 r1396 r1397 r1398 r1399 r1400 r1401 r1402 r1403 r1404 r1405 r1406 r1407 r1408 r1409 r1410 r1411 r1412 r1413 r1414 r1415 r1416 r1417 r1418 r1419 r1420 r1421 r1422 r1423 r1424 r1425 r1426 r1427 r1428 r1429 r1430 r1431 r1432 r1433 r1434 r1435 r1436 r1437 r1438 r1439 r1440 r1441 r1442 r1443 r1444 r1445 r1446 r1447 r1448 r1449 r1450 r1451 r1452 r1453 r1454 r1455 r1456 r1457 r1458 r1459 r1460 r1461 r1462 r1463 r1464 r1465 r1466 r1467 r1468 r1469 r1470 r1471 r1472 r1473 r1474 r1475 r1476 r1477 r1478 r1479 r1480 r1481 r1482 r1483 r1484 r1485 r1486 r1487 r1488 r1489 r1490 r1491 r1492 r1493 r1494 r1495 r1496 r1497 r1498 r1499 r1500 r1501 r1502 r1503 r1504 r1505 r1506 r1507 r1508 r1509 r1510 r1511 r1512 r1513 r1514 r1515 r1516 r1517 r1518 r1519 r1520 r1521 r1522 r1523 r1524 r1525 r1526 r1527 r1528 r1529 r1530 r1531 r1532 r1533 r1534 r1535 r1536 r1537 r1538 r1539 r1540 r1541 r1542 r1543 r1544 r1545 r1546 r1547 r1548 r1549 r1550 r1551 r1552 r1553 r1554 r1555 r1556 r1557 r1558 r1559 r1560 r1561 r1562 r1563 r1564 r1565 r1566 r1567 r1568 r1569 r1570 r1571 r1572 r1573 r1574 r1575 r1576 r1577 r1578 r1579 r1580 r1581 r1582 r1583 r1584 r1585 r1586 r1587 r1588 r1589 r1590 r1591 r1592 r1593 r1594 r1595 r1596 r1597 r1598 r1599 r1600 r1601 r1602 r1603 r1604 r1605 r1606 r1607 r1608 r1609 r1610 r1611 r1612 r1613 r1614 r1615 r1616 r1617 r1618 r1619 r1620 r1621 r1622 r1623 r1624 r1625 r1626 r1627 r1628 r1629 r1630 r1631 r1632 r1633 r1634 r1635 r1636 r1637 r1638 r1639 r1640 r1641 r1642 r1643 r1644 r1645 r1646 r1647 r1648 r1649 r1650 r1651 r1652 r1653 r1654 r1655 r1656 r1657 r1658 r1659 r1660 r1661 r1662 r1663 r1664 r1665 r1666 r1667 r1668 r1669 r1670 r1671 r1672 r1673 r1674 r1675 r1676 r1677 r1678 r1679 r1680 r1681 r1682 r1683 r1684 r1685 r1686 r1687 r1688 r1689 r1690 r1691 r1692 r1693 r1694 r1695 r1696 r1697 r1698 r1699 r1700 r1701 r1702 r1703 r1704 r1705 r1706 r1707 r1708 r1709 r1710 r1711 r1712 r1713 r1714 r1715 r1716 r1717 r1718 r1719 r1720 r1721 r1722 r1723 r1724 r1725 r1726 r1727 r1728 r1729 r1730 r1731 r1732 r1733 r1734 r1735 r1736 r1737 r1738 r1739 r1740 r1741 r1742 r1743 r1744 r1745 r1746 r1747 r1748 r1749 r1750 r1751 r1752 r1753 r1754 r1755 r1756 r1757 r1758 r1759 r1760 r1761 r1762 r1763 r1764 r1765 r1766 r1767 r1768 r1769 r1770 r1771 r1772 r1773 r1774 r1775 r1776 r1777 r1778 r1779 r1780 r1781 r1782 r1783 r1784 r1785 r1786 r1787 r1788 r1789 r1790 r1791 r1792 r1793 r1794 r1795 r1796 r1797 r1798 r1799 r1800 r1801 r1802 r1803 r1804 r1805 r1806 r1807 r1808 r1809 r1810 r1811 r1812 r1813 r1814 r1815 r1816 r1817 r1818 r1819 r1820 r1821 r1822 r1823 r1824 r1825 r1826 r1827 r1828 r1829 r1830 r1831 r1832 r1833 r1834 r1835 r1836 r1837 r1838 r1839 r1840 r1841 r1842 r1843 r1844 r1845 r1846 r1847 r1848 r1849 r1850 r1851 r1852 r1853 r1854 r1855 r1856 r1857 r1858 r1859 r1860 r1861 r1862 r1863 r1864 r1865 r1866 r1867 r1868 r1869 r1870 r1871 r1872 r1873 r1874 r1875 r1876 r1877 r1878 r1879 r1880 r1881 r1882 r1883 r1884 r1885 r1886 r1887 r1888 r1889 r1890 r1891 r1892 r1893 r1894 r1895 r1896 r1897 r1898 r1899 r1900 r1901 r1902 r1903 r1904 r1905 r1906 r1907 r1908 r1909 r1910 r1911 r1912 r1913 r1914 r1915 r1916 r1917 r1918 r1919 r1920 r1921 r1922 r1923 r1924 r1925 r1926 r1927 r1928 r1929 r1930 r1931 r1932 r1933 r1934 r1935 r1936 r1937 r1938 r1939 r1940 r1941 r1942 r1943 r1944 r1945 r1946 r1947 r1948 r1949 r1950 r1951 r1952 r1953 r1954 r1955 r1956 r1957 r1958 r1959 r1960 r1961 r1962 r1963 r1964 r1965 r1966 r1967 r1968 r1969 r1970 r1971 r1972 r1973 r1974 r1975 r1976 r1977 r1978 r1979 r1980 r1981 r1982 r1983 r1984 r1985 r1986 r1987 r1988 r1989 r1990 r1991 r1992 r1993 r1994 r1995 r1996 r1997 r1998 r1999 r2000 r2001 r2002 r2003 r2004 r2005 r2006 r2007 r2008 r2009 r2010 r2011 r2012 r2013 r2014 r2015 r2016 r2017 r2018 r2019 r2020 r2021 r2022 r2023 r2024 r2025 r2026 r2027 r2028 r2029 r2030 r2031 r2032 r2033 r2034 r2035 r2036 r2037 r2038 r2039 r2040 r2041 r2042 r2043 r2044 r2045 r2046 r2047 r2048 r2049 r2050 r2051 r2052 r2053 r2054 r2055 r2056 r2057 r2058 r2059 r2060 r2061 r2062 r2063 r2064 r2065 r2066 r2067 r2068 r2069 r2070 r2071 r2072 r2073 r2074 r2075 r2076 r2077 r2078 r2079 r2080 r2081 r2082 r2083 r2084 r2085 r2086 r2087 r2088 r2089 r2090 r2091 r2092 r2093 r2094 r2095 r2096 r2097 r2098 r2099 r2100 r2101 r2102 r2103 r2104 r2105 r2106 r2107 r2108 r2109 r2110 r2111 r2112 r2113 r2114 r2115 r2116 r2117 r2118 r2119 r2120 r2121 r2122 r2123 r2124 r2125 r2126 r2127 r2128 r2129 r2130 r2131 r2132 r2133 r2134 r2135 r2136 r2137 r2138 r2139 r2140 r2141 r2142 r2143 r2144 r2145 r2146 r2147 r2148 r2149 r2150 r2151 r2152 r2153 r2154 r2155 r2156 r2157 r2158 r2159 r2160 r2161 r2162 r2163 r2164 r2165 r2166 r2167 r2168 r2169 r2170 r2171 r2172 r2173 r2174 r2175 r2176 r2177 r2178 r2179 r2180 r2181 r2182 r2183 r2184 r2185 r2186 r2187 r2188 r2189 r2190 r2191 r2192 r2193 r2194 r2195 r2196 r2197 r2198 r2199 r2200 r2201 r2202 r2203 r2204 r2205 r2206 r2207 r2208 r2209 r2210 r2211 r2212 r2213 r2214 r2215 r2216 r2217 r2218 r2219 r2220 r2221 r2222 r2223 r2224 r2225 r2226 r2227 r2228 r2229 r2230 r2231 r2232 r2233 r2234 r2235 r2236 r2237 r2238 r2239 r2240 r2241 r2242 r2243 r2244 r2245 r2246 r2247 r2248 r2249 r2250 r2251 r2252 r2253 r2254 r2255 r2256 r2257 r2258 r2259 r2260 r2261 r2262 r2263 r2264 r2265 r2266 r2267 r2268 r2269 r2270 r2271 r2272 r2273 r2274 r2275 r2276 r2277 r2278 r2279 r2280 r2281 r2282 r2283 r2284 r2285 r2286 r2287 r2288 r2289 r2290 r2291 r2292 r2293 r2294 r2295 r2296 r2297 r2298 r2299 r2300 r2301 r2302 r2303 r2304 r2305 r2306 r2307 r2308 r2309 r2310 r2311 r2312 r2313 r2314 r2315 r2316 r2317 r2318 r2319 r2320 r2321 r2322 r2323 r2324 r2325 r2326 r2327 r2328 r2329 r2330 r2331 r2332 r2333 r2334 r2335 r2336 r2337 r2338 r2339 r2340 r2341 r2342 r2343 r2344 r2345 r2346 r2347 r2348 r2349 r2350 r2351 r2352 r2353 r2354 r2355 r2356 r2357 r2358 r2359 r2360 r2361 r2362 r2363 r2364 r2365 r2366 r2367 r2368 r2369 r2370 r2371 r2372 r2373 r2374 r2375 r2376 r2377 r2378 r2379 r2380 r2381 r2382 r2383 r2384 r2385 r2386 r2387 r2388 r2389 r2390 r2391 r2392 r2393 r2394 r2395 r2396 r2397 r2398 r2399 r2400 - room
	)
	(:predicates
		(seen) ;; found Waldo
		(search_again) ;; searching for Waldo
		(in ?r  - room)
		(searched_in ?r  - room)
	)

	(:action move-right-from-r1
		:precondition
			(and 
				(in r1)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1))
				(in r2)
			)
	)


	(:action move-left-from-r1
		:precondition
			(and 
				(in r1)
				(not (searched_in r2400))
			)
		:effect
			(and
				(not (search_again))
				(not (in r1))
				(in r2400)
				(oneof (not (seen)) (seen))
				(searched_in r2400)
			)
	)

	(:action move-right-from-r2
		:precondition
			(and 
				(in r2)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2))
				(in r3)
			)
	)


	(:action move-left-from-r2
		:precondition
			(and 
				(in r2)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2))
				(in r1)
			)
	)

	(:action move-right-from-r3
		:precondition
			(and 
				(in r3)
			)
		:effect
			(and
				(not (search_again))
				(not (in r3))
				(in r4)
			)
	)


	(:action move-left-from-r3
		:precondition
			(and 
				(in r3)
			)
		:effect
			(and
				(not (search_again))
				(not (in r3))
				(in r2)
			)
	)

	(:action move-right-from-r4
		:precondition
			(and 
				(in r4)
			)
		:effect
			(and
				(not (search_again))
				(not (in r4))
				(in r5)
			)
	)


	(:action move-left-from-r4
		:precondition
			(and 
				(in r4)
			)
		:effect
			(and
				(not (search_again))
				(not (in r4))
				(in r3)
			)
	)

	(:action move-right-from-r5
		:precondition
			(and 
				(in r5)
			)
		:effect
			(and
				(not (search_again))
				(not (in r5))
				(in r6)
			)
	)


	(:action move-left-from-r5
		:precondition
			(and 
				(in r5)
			)
		:effect
			(and
				(not (search_again))
				(not (in r5))
				(in r4)
			)
	)

	(:action move-right-from-r6
		:precondition
			(and 
				(in r6)
			)
		:effect
			(and
				(not (search_again))
				(not (in r6))
				(in r7)
			)
	)


	(:action move-left-from-r6
		:precondition
			(and 
				(in r6)
			)
		:effect
			(and
				(not (search_again))
				(not (in r6))
				(in r5)
			)
	)

	(:action move-right-from-r7
		:precondition
			(and 
				(in r7)
			)
		:effect
			(and
				(not (search_again))
				(not (in r7))
				(in r8)
			)
	)


	(:action move-left-from-r7
		:precondition
			(and 
				(in r7)
			)
		:effect
			(and
				(not (search_again))
				(not (in r7))
				(in r6)
			)
	)

	(:action move-right-from-r8
		:precondition
			(and 
				(in r8)
			)
		:effect
			(and
				(not (search_again))
				(not (in r8))
				(in r9)
			)
	)


	(:action move-left-from-r8
		:precondition
			(and 
				(in r8)
			)
		:effect
			(and
				(not (search_again))
				(not (in r8))
				(in r7)
			)
	)

	(:action move-right-from-r9
		:precondition
			(and 
				(in r9)
			)
		:effect
			(and
				(not (search_again))
				(not (in r9))
				(in r10)
			)
	)


	(:action move-left-from-r9
		:precondition
			(and 
				(in r9)
			)
		:effect
			(and
				(not (search_again))
				(not (in r9))
				(in r8)
			)
	)

	(:action move-right-from-r10
		:precondition
			(and 
				(in r10)
			)
		:effect
			(and
				(not (search_again))
				(not (in r10))
				(in r11)
			)
	)


	(:action move-left-from-r10
		:precondition
			(and 
				(in r10)
			)
		:effect
			(and
				(not (search_again))
				(not (in r10))
				(in r9)
			)
	)

	(:action move-right-from-r11
		:precondition
			(and 
				(in r11)
			)
		:effect
			(and
				(not (search_again))
				(not (in r11))
				(in r12)
			)
	)


	(:action move-left-from-r11
		:precondition
			(and 
				(in r11)
			)
		:effect
			(and
				(not (search_again))
				(not (in r11))
				(in r10)
			)
	)

	(:action move-right-from-r12
		:precondition
			(and 
				(in r12)
			)
		:effect
			(and
				(not (search_again))
				(not (in r12))
				(in r13)
			)
	)


	(:action move-left-from-r12
		:precondition
			(and 
				(in r12)
			)
		:effect
			(and
				(not (search_again))
				(not (in r12))
				(in r11)
			)
	)

	(:action move-right-from-r13
		:precondition
			(and 
				(in r13)
			)
		:effect
			(and
				(not (search_again))
				(not (in r13))
				(in r14)
			)
	)


	(:action move-left-from-r13
		:precondition
			(and 
				(in r13)
			)
		:effect
			(and
				(not (search_again))
				(not (in r13))
				(in r12)
			)
	)

	(:action move-right-from-r14
		:precondition
			(and 
				(in r14)
			)
		:effect
			(and
				(not (search_again))
				(not (in r14))
				(in r15)
			)
	)


	(:action move-left-from-r14
		:precondition
			(and 
				(in r14)
			)
		:effect
			(and
				(not (search_again))
				(not (in r14))
				(in r13)
			)
	)

	(:action move-right-from-r15
		:precondition
			(and 
				(in r15)
			)
		:effect
			(and
				(not (search_again))
				(not (in r15))
				(in r16)
			)
	)


	(:action move-left-from-r15
		:precondition
			(and 
				(in r15)
			)
		:effect
			(and
				(not (search_again))
				(not (in r15))
				(in r14)
			)
	)

	(:action move-right-from-r16
		:precondition
			(and 
				(in r16)
			)
		:effect
			(and
				(not (search_again))
				(not (in r16))
				(in r17)
			)
	)


	(:action move-left-from-r16
		:precondition
			(and 
				(in r16)
			)
		:effect
			(and
				(not (search_again))
				(not (in r16))
				(in r15)
			)
	)

	(:action move-right-from-r17
		:precondition
			(and 
				(in r17)
			)
		:effect
			(and
				(not (search_again))
				(not (in r17))
				(in r18)
			)
	)


	(:action move-left-from-r17
		:precondition
			(and 
				(in r17)
			)
		:effect
			(and
				(not (search_again))
				(not (in r17))
				(in r16)
			)
	)

	(:action move-right-from-r18
		:precondition
			(and 
				(in r18)
			)
		:effect
			(and
				(not (search_again))
				(not (in r18))
				(in r19)
			)
	)


	(:action move-left-from-r18
		:precondition
			(and 
				(in r18)
			)
		:effect
			(and
				(not (search_again))
				(not (in r18))
				(in r17)
			)
	)

	(:action move-right-from-r19
		:precondition
			(and 
				(in r19)
			)
		:effect
			(and
				(not (search_again))
				(not (in r19))
				(in r20)
			)
	)


	(:action move-left-from-r19
		:precondition
			(and 
				(in r19)
			)
		:effect
			(and
				(not (search_again))
				(not (in r19))
				(in r18)
			)
	)

	(:action move-right-from-r20
		:precondition
			(and 
				(in r20)
			)
		:effect
			(and
				(not (search_again))
				(not (in r20))
				(in r21)
			)
	)


	(:action move-left-from-r20
		:precondition
			(and 
				(in r20)
			)
		:effect
			(and
				(not (search_again))
				(not (in r20))
				(in r19)
			)
	)

	(:action move-right-from-r21
		:precondition
			(and 
				(in r21)
			)
		:effect
			(and
				(not (search_again))
				(not (in r21))
				(in r22)
			)
	)


	(:action move-left-from-r21
		:precondition
			(and 
				(in r21)
			)
		:effect
			(and
				(not (search_again))
				(not (in r21))
				(in r20)
			)
	)

	(:action move-right-from-r22
		:precondition
			(and 
				(in r22)
			)
		:effect
			(and
				(not (search_again))
				(not (in r22))
				(in r23)
			)
	)


	(:action move-left-from-r22
		:precondition
			(and 
				(in r22)
			)
		:effect
			(and
				(not (search_again))
				(not (in r22))
				(in r21)
			)
	)

	(:action move-right-from-r23
		:precondition
			(and 
				(in r23)
			)
		:effect
			(and
				(not (search_again))
				(not (in r23))
				(in r24)
			)
	)


	(:action move-left-from-r23
		:precondition
			(and 
				(in r23)
			)
		:effect
			(and
				(not (search_again))
				(not (in r23))
				(in r22)
			)
	)

	(:action move-right-from-r24
		:precondition
			(and 
				(in r24)
			)
		:effect
			(and
				(not (search_again))
				(not (in r24))
				(in r25)
			)
	)


	(:action move-left-from-r24
		:precondition
			(and 
				(in r24)
			)
		:effect
			(and
				(not (search_again))
				(not (in r24))
				(in r23)
			)
	)

	(:action move-right-from-r25
		:precondition
			(and 
				(in r25)
			)
		:effect
			(and
				(not (search_again))
				(not (in r25))
				(in r26)
			)
	)


	(:action move-left-from-r25
		:precondition
			(and 
				(in r25)
			)
		:effect
			(and
				(not (search_again))
				(not (in r25))
				(in r24)
			)
	)

	(:action move-right-from-r26
		:precondition
			(and 
				(in r26)
			)
		:effect
			(and
				(not (search_again))
				(not (in r26))
				(in r27)
			)
	)


	(:action move-left-from-r26
		:precondition
			(and 
				(in r26)
			)
		:effect
			(and
				(not (search_again))
				(not (in r26))
				(in r25)
			)
	)

	(:action move-right-from-r27
		:precondition
			(and 
				(in r27)
			)
		:effect
			(and
				(not (search_again))
				(not (in r27))
				(in r28)
			)
	)


	(:action move-left-from-r27
		:precondition
			(and 
				(in r27)
			)
		:effect
			(and
				(not (search_again))
				(not (in r27))
				(in r26)
			)
	)

	(:action move-right-from-r28
		:precondition
			(and 
				(in r28)
			)
		:effect
			(and
				(not (search_again))
				(not (in r28))
				(in r29)
			)
	)


	(:action move-left-from-r28
		:precondition
			(and 
				(in r28)
			)
		:effect
			(and
				(not (search_again))
				(not (in r28))
				(in r27)
			)
	)

	(:action move-right-from-r29
		:precondition
			(and 
				(in r29)
			)
		:effect
			(and
				(not (search_again))
				(not (in r29))
				(in r30)
			)
	)


	(:action move-left-from-r29
		:precondition
			(and 
				(in r29)
			)
		:effect
			(and
				(not (search_again))
				(not (in r29))
				(in r28)
			)
	)

	(:action move-right-from-r30
		:precondition
			(and 
				(in r30)
			)
		:effect
			(and
				(not (search_again))
				(not (in r30))
				(in r31)
			)
	)


	(:action move-left-from-r30
		:precondition
			(and 
				(in r30)
			)
		:effect
			(and
				(not (search_again))
				(not (in r30))
				(in r29)
			)
	)

	(:action move-right-from-r31
		:precondition
			(and 
				(in r31)
			)
		:effect
			(and
				(not (search_again))
				(not (in r31))
				(in r32)
			)
	)


	(:action move-left-from-r31
		:precondition
			(and 
				(in r31)
			)
		:effect
			(and
				(not (search_again))
				(not (in r31))
				(in r30)
			)
	)

	(:action move-right-from-r32
		:precondition
			(and 
				(in r32)
			)
		:effect
			(and
				(not (search_again))
				(not (in r32))
				(in r33)
			)
	)


	(:action move-left-from-r32
		:precondition
			(and 
				(in r32)
			)
		:effect
			(and
				(not (search_again))
				(not (in r32))
				(in r31)
			)
	)

	(:action move-right-from-r33
		:precondition
			(and 
				(in r33)
			)
		:effect
			(and
				(not (search_again))
				(not (in r33))
				(in r34)
			)
	)


	(:action move-left-from-r33
		:precondition
			(and 
				(in r33)
			)
		:effect
			(and
				(not (search_again))
				(not (in r33))
				(in r32)
			)
	)

	(:action move-right-from-r34
		:precondition
			(and 
				(in r34)
			)
		:effect
			(and
				(not (search_again))
				(not (in r34))
				(in r35)
			)
	)


	(:action move-left-from-r34
		:precondition
			(and 
				(in r34)
			)
		:effect
			(and
				(not (search_again))
				(not (in r34))
				(in r33)
			)
	)

	(:action move-right-from-r35
		:precondition
			(and 
				(in r35)
			)
		:effect
			(and
				(not (search_again))
				(not (in r35))
				(in r36)
			)
	)


	(:action move-left-from-r35
		:precondition
			(and 
				(in r35)
			)
		:effect
			(and
				(not (search_again))
				(not (in r35))
				(in r34)
			)
	)

	(:action move-right-from-r36
		:precondition
			(and 
				(in r36)
			)
		:effect
			(and
				(not (search_again))
				(not (in r36))
				(in r37)
			)
	)


	(:action move-left-from-r36
		:precondition
			(and 
				(in r36)
			)
		:effect
			(and
				(not (search_again))
				(not (in r36))
				(in r35)
			)
	)

	(:action move-right-from-r37
		:precondition
			(and 
				(in r37)
			)
		:effect
			(and
				(not (search_again))
				(not (in r37))
				(in r38)
			)
	)


	(:action move-left-from-r37
		:precondition
			(and 
				(in r37)
			)
		:effect
			(and
				(not (search_again))
				(not (in r37))
				(in r36)
			)
	)

	(:action move-right-from-r38
		:precondition
			(and 
				(in r38)
			)
		:effect
			(and
				(not (search_again))
				(not (in r38))
				(in r39)
			)
	)


	(:action move-left-from-r38
		:precondition
			(and 
				(in r38)
			)
		:effect
			(and
				(not (search_again))
				(not (in r38))
				(in r37)
			)
	)

	(:action move-right-from-r39
		:precondition
			(and 
				(in r39)
			)
		:effect
			(and
				(not (search_again))
				(not (in r39))
				(in r40)
			)
	)


	(:action move-left-from-r39
		:precondition
			(and 
				(in r39)
			)
		:effect
			(and
				(not (search_again))
				(not (in r39))
				(in r38)
			)
	)

	(:action move-right-from-r40
		:precondition
			(and 
				(in r40)
			)
		:effect
			(and
				(not (search_again))
				(not (in r40))
				(in r41)
			)
	)


	(:action move-left-from-r40
		:precondition
			(and 
				(in r40)
			)
		:effect
			(and
				(not (search_again))
				(not (in r40))
				(in r39)
			)
	)

	(:action move-right-from-r41
		:precondition
			(and 
				(in r41)
			)
		:effect
			(and
				(not (search_again))
				(not (in r41))
				(in r42)
			)
	)


	(:action move-left-from-r41
		:precondition
			(and 
				(in r41)
			)
		:effect
			(and
				(not (search_again))
				(not (in r41))
				(in r40)
			)
	)

	(:action move-right-from-r42
		:precondition
			(and 
				(in r42)
			)
		:effect
			(and
				(not (search_again))
				(not (in r42))
				(in r43)
			)
	)


	(:action move-left-from-r42
		:precondition
			(and 
				(in r42)
			)
		:effect
			(and
				(not (search_again))
				(not (in r42))
				(in r41)
			)
	)

	(:action move-right-from-r43
		:precondition
			(and 
				(in r43)
			)
		:effect
			(and
				(not (search_again))
				(not (in r43))
				(in r44)
			)
	)


	(:action move-left-from-r43
		:precondition
			(and 
				(in r43)
			)
		:effect
			(and
				(not (search_again))
				(not (in r43))
				(in r42)
			)
	)

	(:action move-right-from-r44
		:precondition
			(and 
				(in r44)
			)
		:effect
			(and
				(not (search_again))
				(not (in r44))
				(in r45)
			)
	)


	(:action move-left-from-r44
		:precondition
			(and 
				(in r44)
			)
		:effect
			(and
				(not (search_again))
				(not (in r44))
				(in r43)
			)
	)

	(:action move-right-from-r45
		:precondition
			(and 
				(in r45)
			)
		:effect
			(and
				(not (search_again))
				(not (in r45))
				(in r46)
			)
	)


	(:action move-left-from-r45
		:precondition
			(and 
				(in r45)
			)
		:effect
			(and
				(not (search_again))
				(not (in r45))
				(in r44)
			)
	)

	(:action move-right-from-r46
		:precondition
			(and 
				(in r46)
			)
		:effect
			(and
				(not (search_again))
				(not (in r46))
				(in r47)
			)
	)


	(:action move-left-from-r46
		:precondition
			(and 
				(in r46)
			)
		:effect
			(and
				(not (search_again))
				(not (in r46))
				(in r45)
			)
	)

	(:action move-right-from-r47
		:precondition
			(and 
				(in r47)
			)
		:effect
			(and
				(not (search_again))
				(not (in r47))
				(in r48)
			)
	)


	(:action move-left-from-r47
		:precondition
			(and 
				(in r47)
			)
		:effect
			(and
				(not (search_again))
				(not (in r47))
				(in r46)
			)
	)

	(:action move-right-from-r48
		:precondition
			(and 
				(in r48)
			)
		:effect
			(and
				(not (search_again))
				(not (in r48))
				(in r49)
			)
	)


	(:action move-left-from-r48
		:precondition
			(and 
				(in r48)
			)
		:effect
			(and
				(not (search_again))
				(not (in r48))
				(in r47)
			)
	)

	(:action move-right-from-r49
		:precondition
			(and 
				(in r49)
			)
		:effect
			(and
				(not (search_again))
				(not (in r49))
				(in r50)
			)
	)


	(:action move-left-from-r49
		:precondition
			(and 
				(in r49)
			)
		:effect
			(and
				(not (search_again))
				(not (in r49))
				(in r48)
			)
	)

	(:action move-right-from-r50
		:precondition
			(and 
				(in r50)
			)
		:effect
			(and
				(not (search_again))
				(not (in r50))
				(in r51)
			)
	)


	(:action move-left-from-r50
		:precondition
			(and 
				(in r50)
			)
		:effect
			(and
				(not (search_again))
				(not (in r50))
				(in r49)
			)
	)

	(:action move-right-from-r51
		:precondition
			(and 
				(in r51)
			)
		:effect
			(and
				(not (search_again))
				(not (in r51))
				(in r52)
			)
	)


	(:action move-left-from-r51
		:precondition
			(and 
				(in r51)
			)
		:effect
			(and
				(not (search_again))
				(not (in r51))
				(in r50)
			)
	)

	(:action move-right-from-r52
		:precondition
			(and 
				(in r52)
			)
		:effect
			(and
				(not (search_again))
				(not (in r52))
				(in r53)
			)
	)


	(:action move-left-from-r52
		:precondition
			(and 
				(in r52)
			)
		:effect
			(and
				(not (search_again))
				(not (in r52))
				(in r51)
			)
	)

	(:action move-right-from-r53
		:precondition
			(and 
				(in r53)
			)
		:effect
			(and
				(not (search_again))
				(not (in r53))
				(in r54)
			)
	)


	(:action move-left-from-r53
		:precondition
			(and 
				(in r53)
			)
		:effect
			(and
				(not (search_again))
				(not (in r53))
				(in r52)
			)
	)

	(:action move-right-from-r54
		:precondition
			(and 
				(in r54)
			)
		:effect
			(and
				(not (search_again))
				(not (in r54))
				(in r55)
			)
	)


	(:action move-left-from-r54
		:precondition
			(and 
				(in r54)
			)
		:effect
			(and
				(not (search_again))
				(not (in r54))
				(in r53)
			)
	)

	(:action move-right-from-r55
		:precondition
			(and 
				(in r55)
			)
		:effect
			(and
				(not (search_again))
				(not (in r55))
				(in r56)
			)
	)


	(:action move-left-from-r55
		:precondition
			(and 
				(in r55)
			)
		:effect
			(and
				(not (search_again))
				(not (in r55))
				(in r54)
			)
	)

	(:action move-right-from-r56
		:precondition
			(and 
				(in r56)
			)
		:effect
			(and
				(not (search_again))
				(not (in r56))
				(in r57)
			)
	)


	(:action move-left-from-r56
		:precondition
			(and 
				(in r56)
			)
		:effect
			(and
				(not (search_again))
				(not (in r56))
				(in r55)
			)
	)

	(:action move-right-from-r57
		:precondition
			(and 
				(in r57)
			)
		:effect
			(and
				(not (search_again))
				(not (in r57))
				(in r58)
			)
	)


	(:action move-left-from-r57
		:precondition
			(and 
				(in r57)
			)
		:effect
			(and
				(not (search_again))
				(not (in r57))
				(in r56)
			)
	)

	(:action move-right-from-r58
		:precondition
			(and 
				(in r58)
			)
		:effect
			(and
				(not (search_again))
				(not (in r58))
				(in r59)
			)
	)


	(:action move-left-from-r58
		:precondition
			(and 
				(in r58)
			)
		:effect
			(and
				(not (search_again))
				(not (in r58))
				(in r57)
			)
	)

	(:action move-right-from-r59
		:precondition
			(and 
				(in r59)
			)
		:effect
			(and
				(not (search_again))
				(not (in r59))
				(in r60)
			)
	)


	(:action move-left-from-r59
		:precondition
			(and 
				(in r59)
			)
		:effect
			(and
				(not (search_again))
				(not (in r59))
				(in r58)
			)
	)

	(:action move-right-from-r60
		:precondition
			(and 
				(in r60)
			)
		:effect
			(and
				(not (search_again))
				(not (in r60))
				(in r61)
			)
	)


	(:action move-left-from-r60
		:precondition
			(and 
				(in r60)
			)
		:effect
			(and
				(not (search_again))
				(not (in r60))
				(in r59)
			)
	)

	(:action move-right-from-r61
		:precondition
			(and 
				(in r61)
			)
		:effect
			(and
				(not (search_again))
				(not (in r61))
				(in r62)
			)
	)


	(:action move-left-from-r61
		:precondition
			(and 
				(in r61)
			)
		:effect
			(and
				(not (search_again))
				(not (in r61))
				(in r60)
			)
	)

	(:action move-right-from-r62
		:precondition
			(and 
				(in r62)
			)
		:effect
			(and
				(not (search_again))
				(not (in r62))
				(in r63)
			)
	)


	(:action move-left-from-r62
		:precondition
			(and 
				(in r62)
			)
		:effect
			(and
				(not (search_again))
				(not (in r62))
				(in r61)
			)
	)

	(:action move-right-from-r63
		:precondition
			(and 
				(in r63)
			)
		:effect
			(and
				(not (search_again))
				(not (in r63))
				(in r64)
			)
	)


	(:action move-left-from-r63
		:precondition
			(and 
				(in r63)
			)
		:effect
			(and
				(not (search_again))
				(not (in r63))
				(in r62)
			)
	)

	(:action move-right-from-r64
		:precondition
			(and 
				(in r64)
			)
		:effect
			(and
				(not (search_again))
				(not (in r64))
				(in r65)
			)
	)


	(:action move-left-from-r64
		:precondition
			(and 
				(in r64)
			)
		:effect
			(and
				(not (search_again))
				(not (in r64))
				(in r63)
			)
	)

	(:action move-right-from-r65
		:precondition
			(and 
				(in r65)
			)
		:effect
			(and
				(not (search_again))
				(not (in r65))
				(in r66)
			)
	)


	(:action move-left-from-r65
		:precondition
			(and 
				(in r65)
			)
		:effect
			(and
				(not (search_again))
				(not (in r65))
				(in r64)
			)
	)

	(:action move-right-from-r66
		:precondition
			(and 
				(in r66)
			)
		:effect
			(and
				(not (search_again))
				(not (in r66))
				(in r67)
			)
	)


	(:action move-left-from-r66
		:precondition
			(and 
				(in r66)
			)
		:effect
			(and
				(not (search_again))
				(not (in r66))
				(in r65)
			)
	)

	(:action move-right-from-r67
		:precondition
			(and 
				(in r67)
			)
		:effect
			(and
				(not (search_again))
				(not (in r67))
				(in r68)
			)
	)


	(:action move-left-from-r67
		:precondition
			(and 
				(in r67)
			)
		:effect
			(and
				(not (search_again))
				(not (in r67))
				(in r66)
			)
	)

	(:action move-right-from-r68
		:precondition
			(and 
				(in r68)
			)
		:effect
			(and
				(not (search_again))
				(not (in r68))
				(in r69)
			)
	)


	(:action move-left-from-r68
		:precondition
			(and 
				(in r68)
			)
		:effect
			(and
				(not (search_again))
				(not (in r68))
				(in r67)
			)
	)

	(:action move-right-from-r69
		:precondition
			(and 
				(in r69)
			)
		:effect
			(and
				(not (search_again))
				(not (in r69))
				(in r70)
			)
	)


	(:action move-left-from-r69
		:precondition
			(and 
				(in r69)
			)
		:effect
			(and
				(not (search_again))
				(not (in r69))
				(in r68)
			)
	)

	(:action move-right-from-r70
		:precondition
			(and 
				(in r70)
			)
		:effect
			(and
				(not (search_again))
				(not (in r70))
				(in r71)
			)
	)


	(:action move-left-from-r70
		:precondition
			(and 
				(in r70)
			)
		:effect
			(and
				(not (search_again))
				(not (in r70))
				(in r69)
			)
	)

	(:action move-right-from-r71
		:precondition
			(and 
				(in r71)
			)
		:effect
			(and
				(not (search_again))
				(not (in r71))
				(in r72)
			)
	)


	(:action move-left-from-r71
		:precondition
			(and 
				(in r71)
			)
		:effect
			(and
				(not (search_again))
				(not (in r71))
				(in r70)
			)
	)

	(:action move-right-from-r72
		:precondition
			(and 
				(in r72)
			)
		:effect
			(and
				(not (search_again))
				(not (in r72))
				(in r73)
			)
	)


	(:action move-left-from-r72
		:precondition
			(and 
				(in r72)
			)
		:effect
			(and
				(not (search_again))
				(not (in r72))
				(in r71)
			)
	)

	(:action move-right-from-r73
		:precondition
			(and 
				(in r73)
			)
		:effect
			(and
				(not (search_again))
				(not (in r73))
				(in r74)
			)
	)


	(:action move-left-from-r73
		:precondition
			(and 
				(in r73)
			)
		:effect
			(and
				(not (search_again))
				(not (in r73))
				(in r72)
			)
	)

	(:action move-right-from-r74
		:precondition
			(and 
				(in r74)
			)
		:effect
			(and
				(not (search_again))
				(not (in r74))
				(in r75)
			)
	)


	(:action move-left-from-r74
		:precondition
			(and 
				(in r74)
			)
		:effect
			(and
				(not (search_again))
				(not (in r74))
				(in r73)
			)
	)

	(:action move-right-from-r75
		:precondition
			(and 
				(in r75)
			)
		:effect
			(and
				(not (search_again))
				(not (in r75))
				(in r76)
			)
	)


	(:action move-left-from-r75
		:precondition
			(and 
				(in r75)
			)
		:effect
			(and
				(not (search_again))
				(not (in r75))
				(in r74)
			)
	)

	(:action move-right-from-r76
		:precondition
			(and 
				(in r76)
			)
		:effect
			(and
				(not (search_again))
				(not (in r76))
				(in r77)
			)
	)


	(:action move-left-from-r76
		:precondition
			(and 
				(in r76)
			)
		:effect
			(and
				(not (search_again))
				(not (in r76))
				(in r75)
			)
	)

	(:action move-right-from-r77
		:precondition
			(and 
				(in r77)
			)
		:effect
			(and
				(not (search_again))
				(not (in r77))
				(in r78)
			)
	)


	(:action move-left-from-r77
		:precondition
			(and 
				(in r77)
			)
		:effect
			(and
				(not (search_again))
				(not (in r77))
				(in r76)
			)
	)

	(:action move-right-from-r78
		:precondition
			(and 
				(in r78)
			)
		:effect
			(and
				(not (search_again))
				(not (in r78))
				(in r79)
			)
	)


	(:action move-left-from-r78
		:precondition
			(and 
				(in r78)
			)
		:effect
			(and
				(not (search_again))
				(not (in r78))
				(in r77)
			)
	)

	(:action move-right-from-r79
		:precondition
			(and 
				(in r79)
			)
		:effect
			(and
				(not (search_again))
				(not (in r79))
				(in r80)
			)
	)


	(:action move-left-from-r79
		:precondition
			(and 
				(in r79)
			)
		:effect
			(and
				(not (search_again))
				(not (in r79))
				(in r78)
			)
	)

	(:action move-right-from-r80
		:precondition
			(and 
				(in r80)
			)
		:effect
			(and
				(not (search_again))
				(not (in r80))
				(in r81)
			)
	)


	(:action move-left-from-r80
		:precondition
			(and 
				(in r80)
			)
		:effect
			(and
				(not (search_again))
				(not (in r80))
				(in r79)
			)
	)

	(:action move-right-from-r81
		:precondition
			(and 
				(in r81)
			)
		:effect
			(and
				(not (search_again))
				(not (in r81))
				(in r82)
			)
	)


	(:action move-left-from-r81
		:precondition
			(and 
				(in r81)
			)
		:effect
			(and
				(not (search_again))
				(not (in r81))
				(in r80)
			)
	)

	(:action move-right-from-r82
		:precondition
			(and 
				(in r82)
			)
		:effect
			(and
				(not (search_again))
				(not (in r82))
				(in r83)
			)
	)


	(:action move-left-from-r82
		:precondition
			(and 
				(in r82)
			)
		:effect
			(and
				(not (search_again))
				(not (in r82))
				(in r81)
			)
	)

	(:action move-right-from-r83
		:precondition
			(and 
				(in r83)
			)
		:effect
			(and
				(not (search_again))
				(not (in r83))
				(in r84)
			)
	)


	(:action move-left-from-r83
		:precondition
			(and 
				(in r83)
			)
		:effect
			(and
				(not (search_again))
				(not (in r83))
				(in r82)
			)
	)

	(:action move-right-from-r84
		:precondition
			(and 
				(in r84)
			)
		:effect
			(and
				(not (search_again))
				(not (in r84))
				(in r85)
			)
	)


	(:action move-left-from-r84
		:precondition
			(and 
				(in r84)
			)
		:effect
			(and
				(not (search_again))
				(not (in r84))
				(in r83)
			)
	)

	(:action move-right-from-r85
		:precondition
			(and 
				(in r85)
			)
		:effect
			(and
				(not (search_again))
				(not (in r85))
				(in r86)
			)
	)


	(:action move-left-from-r85
		:precondition
			(and 
				(in r85)
			)
		:effect
			(and
				(not (search_again))
				(not (in r85))
				(in r84)
			)
	)

	(:action move-right-from-r86
		:precondition
			(and 
				(in r86)
			)
		:effect
			(and
				(not (search_again))
				(not (in r86))
				(in r87)
			)
	)


	(:action move-left-from-r86
		:precondition
			(and 
				(in r86)
			)
		:effect
			(and
				(not (search_again))
				(not (in r86))
				(in r85)
			)
	)

	(:action move-right-from-r87
		:precondition
			(and 
				(in r87)
			)
		:effect
			(and
				(not (search_again))
				(not (in r87))
				(in r88)
			)
	)


	(:action move-left-from-r87
		:precondition
			(and 
				(in r87)
			)
		:effect
			(and
				(not (search_again))
				(not (in r87))
				(in r86)
			)
	)

	(:action move-right-from-r88
		:precondition
			(and 
				(in r88)
			)
		:effect
			(and
				(not (search_again))
				(not (in r88))
				(in r89)
			)
	)


	(:action move-left-from-r88
		:precondition
			(and 
				(in r88)
			)
		:effect
			(and
				(not (search_again))
				(not (in r88))
				(in r87)
			)
	)

	(:action move-right-from-r89
		:precondition
			(and 
				(in r89)
			)
		:effect
			(and
				(not (search_again))
				(not (in r89))
				(in r90)
			)
	)


	(:action move-left-from-r89
		:precondition
			(and 
				(in r89)
			)
		:effect
			(and
				(not (search_again))
				(not (in r89))
				(in r88)
			)
	)

	(:action move-right-from-r90
		:precondition
			(and 
				(in r90)
			)
		:effect
			(and
				(not (search_again))
				(not (in r90))
				(in r91)
			)
	)


	(:action move-left-from-r90
		:precondition
			(and 
				(in r90)
			)
		:effect
			(and
				(not (search_again))
				(not (in r90))
				(in r89)
			)
	)

	(:action move-right-from-r91
		:precondition
			(and 
				(in r91)
			)
		:effect
			(and
				(not (search_again))
				(not (in r91))
				(in r92)
			)
	)


	(:action move-left-from-r91
		:precondition
			(and 
				(in r91)
			)
		:effect
			(and
				(not (search_again))
				(not (in r91))
				(in r90)
			)
	)

	(:action move-right-from-r92
		:precondition
			(and 
				(in r92)
			)
		:effect
			(and
				(not (search_again))
				(not (in r92))
				(in r93)
			)
	)


	(:action move-left-from-r92
		:precondition
			(and 
				(in r92)
			)
		:effect
			(and
				(not (search_again))
				(not (in r92))
				(in r91)
			)
	)

	(:action move-right-from-r93
		:precondition
			(and 
				(in r93)
			)
		:effect
			(and
				(not (search_again))
				(not (in r93))
				(in r94)
			)
	)


	(:action move-left-from-r93
		:precondition
			(and 
				(in r93)
			)
		:effect
			(and
				(not (search_again))
				(not (in r93))
				(in r92)
			)
	)

	(:action move-right-from-r94
		:precondition
			(and 
				(in r94)
			)
		:effect
			(and
				(not (search_again))
				(not (in r94))
				(in r95)
			)
	)


	(:action move-left-from-r94
		:precondition
			(and 
				(in r94)
			)
		:effect
			(and
				(not (search_again))
				(not (in r94))
				(in r93)
			)
	)

	(:action move-right-from-r95
		:precondition
			(and 
				(in r95)
			)
		:effect
			(and
				(not (search_again))
				(not (in r95))
				(in r96)
			)
	)


	(:action move-left-from-r95
		:precondition
			(and 
				(in r95)
			)
		:effect
			(and
				(not (search_again))
				(not (in r95))
				(in r94)
			)
	)

	(:action move-right-from-r96
		:precondition
			(and 
				(in r96)
			)
		:effect
			(and
				(not (search_again))
				(not (in r96))
				(in r97)
			)
	)


	(:action move-left-from-r96
		:precondition
			(and 
				(in r96)
			)
		:effect
			(and
				(not (search_again))
				(not (in r96))
				(in r95)
			)
	)

	(:action move-right-from-r97
		:precondition
			(and 
				(in r97)
			)
		:effect
			(and
				(not (search_again))
				(not (in r97))
				(in r98)
			)
	)


	(:action move-left-from-r97
		:precondition
			(and 
				(in r97)
			)
		:effect
			(and
				(not (search_again))
				(not (in r97))
				(in r96)
			)
	)

	(:action move-right-from-r98
		:precondition
			(and 
				(in r98)
			)
		:effect
			(and
				(not (search_again))
				(not (in r98))
				(in r99)
			)
	)


	(:action move-left-from-r98
		:precondition
			(and 
				(in r98)
			)
		:effect
			(and
				(not (search_again))
				(not (in r98))
				(in r97)
			)
	)

	(:action move-right-from-r99
		:precondition
			(and 
				(in r99)
			)
		:effect
			(and
				(not (search_again))
				(not (in r99))
				(in r100)
			)
	)


	(:action move-left-from-r99
		:precondition
			(and 
				(in r99)
			)
		:effect
			(and
				(not (search_again))
				(not (in r99))
				(in r98)
			)
	)

	(:action move-right-from-r100
		:precondition
			(and 
				(in r100)
			)
		:effect
			(and
				(not (search_again))
				(not (in r100))
				(in r101)
			)
	)


	(:action move-left-from-r100
		:precondition
			(and 
				(in r100)
			)
		:effect
			(and
				(not (search_again))
				(not (in r100))
				(in r99)
			)
	)

	(:action move-right-from-r101
		:precondition
			(and 
				(in r101)
			)
		:effect
			(and
				(not (search_again))
				(not (in r101))
				(in r102)
			)
	)


	(:action move-left-from-r101
		:precondition
			(and 
				(in r101)
			)
		:effect
			(and
				(not (search_again))
				(not (in r101))
				(in r100)
			)
	)

	(:action move-right-from-r102
		:precondition
			(and 
				(in r102)
			)
		:effect
			(and
				(not (search_again))
				(not (in r102))
				(in r103)
			)
	)


	(:action move-left-from-r102
		:precondition
			(and 
				(in r102)
			)
		:effect
			(and
				(not (search_again))
				(not (in r102))
				(in r101)
			)
	)

	(:action move-right-from-r103
		:precondition
			(and 
				(in r103)
			)
		:effect
			(and
				(not (search_again))
				(not (in r103))
				(in r104)
			)
	)


	(:action move-left-from-r103
		:precondition
			(and 
				(in r103)
			)
		:effect
			(and
				(not (search_again))
				(not (in r103))
				(in r102)
			)
	)

	(:action move-right-from-r104
		:precondition
			(and 
				(in r104)
			)
		:effect
			(and
				(not (search_again))
				(not (in r104))
				(in r105)
			)
	)


	(:action move-left-from-r104
		:precondition
			(and 
				(in r104)
			)
		:effect
			(and
				(not (search_again))
				(not (in r104))
				(in r103)
			)
	)

	(:action move-right-from-r105
		:precondition
			(and 
				(in r105)
			)
		:effect
			(and
				(not (search_again))
				(not (in r105))
				(in r106)
			)
	)


	(:action move-left-from-r105
		:precondition
			(and 
				(in r105)
			)
		:effect
			(and
				(not (search_again))
				(not (in r105))
				(in r104)
			)
	)

	(:action move-right-from-r106
		:precondition
			(and 
				(in r106)
			)
		:effect
			(and
				(not (search_again))
				(not (in r106))
				(in r107)
			)
	)


	(:action move-left-from-r106
		:precondition
			(and 
				(in r106)
			)
		:effect
			(and
				(not (search_again))
				(not (in r106))
				(in r105)
			)
	)

	(:action move-right-from-r107
		:precondition
			(and 
				(in r107)
			)
		:effect
			(and
				(not (search_again))
				(not (in r107))
				(in r108)
			)
	)


	(:action move-left-from-r107
		:precondition
			(and 
				(in r107)
			)
		:effect
			(and
				(not (search_again))
				(not (in r107))
				(in r106)
			)
	)

	(:action move-right-from-r108
		:precondition
			(and 
				(in r108)
			)
		:effect
			(and
				(not (search_again))
				(not (in r108))
				(in r109)
			)
	)


	(:action move-left-from-r108
		:precondition
			(and 
				(in r108)
			)
		:effect
			(and
				(not (search_again))
				(not (in r108))
				(in r107)
			)
	)

	(:action move-right-from-r109
		:precondition
			(and 
				(in r109)
			)
		:effect
			(and
				(not (search_again))
				(not (in r109))
				(in r110)
			)
	)


	(:action move-left-from-r109
		:precondition
			(and 
				(in r109)
			)
		:effect
			(and
				(not (search_again))
				(not (in r109))
				(in r108)
			)
	)

	(:action move-right-from-r110
		:precondition
			(and 
				(in r110)
			)
		:effect
			(and
				(not (search_again))
				(not (in r110))
				(in r111)
			)
	)


	(:action move-left-from-r110
		:precondition
			(and 
				(in r110)
			)
		:effect
			(and
				(not (search_again))
				(not (in r110))
				(in r109)
			)
	)

	(:action move-right-from-r111
		:precondition
			(and 
				(in r111)
			)
		:effect
			(and
				(not (search_again))
				(not (in r111))
				(in r112)
			)
	)


	(:action move-left-from-r111
		:precondition
			(and 
				(in r111)
			)
		:effect
			(and
				(not (search_again))
				(not (in r111))
				(in r110)
			)
	)

	(:action move-right-from-r112
		:precondition
			(and 
				(in r112)
			)
		:effect
			(and
				(not (search_again))
				(not (in r112))
				(in r113)
			)
	)


	(:action move-left-from-r112
		:precondition
			(and 
				(in r112)
			)
		:effect
			(and
				(not (search_again))
				(not (in r112))
				(in r111)
			)
	)

	(:action move-right-from-r113
		:precondition
			(and 
				(in r113)
			)
		:effect
			(and
				(not (search_again))
				(not (in r113))
				(in r114)
			)
	)


	(:action move-left-from-r113
		:precondition
			(and 
				(in r113)
			)
		:effect
			(and
				(not (search_again))
				(not (in r113))
				(in r112)
			)
	)

	(:action move-right-from-r114
		:precondition
			(and 
				(in r114)
			)
		:effect
			(and
				(not (search_again))
				(not (in r114))
				(in r115)
			)
	)


	(:action move-left-from-r114
		:precondition
			(and 
				(in r114)
			)
		:effect
			(and
				(not (search_again))
				(not (in r114))
				(in r113)
			)
	)

	(:action move-right-from-r115
		:precondition
			(and 
				(in r115)
			)
		:effect
			(and
				(not (search_again))
				(not (in r115))
				(in r116)
			)
	)


	(:action move-left-from-r115
		:precondition
			(and 
				(in r115)
			)
		:effect
			(and
				(not (search_again))
				(not (in r115))
				(in r114)
			)
	)

	(:action move-right-from-r116
		:precondition
			(and 
				(in r116)
			)
		:effect
			(and
				(not (search_again))
				(not (in r116))
				(in r117)
			)
	)


	(:action move-left-from-r116
		:precondition
			(and 
				(in r116)
			)
		:effect
			(and
				(not (search_again))
				(not (in r116))
				(in r115)
			)
	)

	(:action move-right-from-r117
		:precondition
			(and 
				(in r117)
			)
		:effect
			(and
				(not (search_again))
				(not (in r117))
				(in r118)
			)
	)


	(:action move-left-from-r117
		:precondition
			(and 
				(in r117)
			)
		:effect
			(and
				(not (search_again))
				(not (in r117))
				(in r116)
			)
	)

	(:action move-right-from-r118
		:precondition
			(and 
				(in r118)
			)
		:effect
			(and
				(not (search_again))
				(not (in r118))
				(in r119)
			)
	)


	(:action move-left-from-r118
		:precondition
			(and 
				(in r118)
			)
		:effect
			(and
				(not (search_again))
				(not (in r118))
				(in r117)
			)
	)

	(:action move-right-from-r119
		:precondition
			(and 
				(in r119)
			)
		:effect
			(and
				(not (search_again))
				(not (in r119))
				(in r120)
			)
	)


	(:action move-left-from-r119
		:precondition
			(and 
				(in r119)
			)
		:effect
			(and
				(not (search_again))
				(not (in r119))
				(in r118)
			)
	)

	(:action move-right-from-r120
		:precondition
			(and 
				(in r120)
			)
		:effect
			(and
				(not (search_again))
				(not (in r120))
				(in r121)
			)
	)


	(:action move-left-from-r120
		:precondition
			(and 
				(in r120)
			)
		:effect
			(and
				(not (search_again))
				(not (in r120))
				(in r119)
			)
	)

	(:action move-right-from-r121
		:precondition
			(and 
				(in r121)
			)
		:effect
			(and
				(not (search_again))
				(not (in r121))
				(in r122)
			)
	)


	(:action move-left-from-r121
		:precondition
			(and 
				(in r121)
			)
		:effect
			(and
				(not (search_again))
				(not (in r121))
				(in r120)
			)
	)

	(:action move-right-from-r122
		:precondition
			(and 
				(in r122)
			)
		:effect
			(and
				(not (search_again))
				(not (in r122))
				(in r123)
			)
	)


	(:action move-left-from-r122
		:precondition
			(and 
				(in r122)
			)
		:effect
			(and
				(not (search_again))
				(not (in r122))
				(in r121)
			)
	)

	(:action move-right-from-r123
		:precondition
			(and 
				(in r123)
			)
		:effect
			(and
				(not (search_again))
				(not (in r123))
				(in r124)
			)
	)


	(:action move-left-from-r123
		:precondition
			(and 
				(in r123)
			)
		:effect
			(and
				(not (search_again))
				(not (in r123))
				(in r122)
			)
	)

	(:action move-right-from-r124
		:precondition
			(and 
				(in r124)
			)
		:effect
			(and
				(not (search_again))
				(not (in r124))
				(in r125)
			)
	)


	(:action move-left-from-r124
		:precondition
			(and 
				(in r124)
			)
		:effect
			(and
				(not (search_again))
				(not (in r124))
				(in r123)
			)
	)

	(:action move-right-from-r125
		:precondition
			(and 
				(in r125)
			)
		:effect
			(and
				(not (search_again))
				(not (in r125))
				(in r126)
			)
	)


	(:action move-left-from-r125
		:precondition
			(and 
				(in r125)
			)
		:effect
			(and
				(not (search_again))
				(not (in r125))
				(in r124)
			)
	)

	(:action move-right-from-r126
		:precondition
			(and 
				(in r126)
			)
		:effect
			(and
				(not (search_again))
				(not (in r126))
				(in r127)
			)
	)


	(:action move-left-from-r126
		:precondition
			(and 
				(in r126)
			)
		:effect
			(and
				(not (search_again))
				(not (in r126))
				(in r125)
			)
	)

	(:action move-right-from-r127
		:precondition
			(and 
				(in r127)
			)
		:effect
			(and
				(not (search_again))
				(not (in r127))
				(in r128)
			)
	)


	(:action move-left-from-r127
		:precondition
			(and 
				(in r127)
			)
		:effect
			(and
				(not (search_again))
				(not (in r127))
				(in r126)
			)
	)

	(:action move-right-from-r128
		:precondition
			(and 
				(in r128)
			)
		:effect
			(and
				(not (search_again))
				(not (in r128))
				(in r129)
			)
	)


	(:action move-left-from-r128
		:precondition
			(and 
				(in r128)
			)
		:effect
			(and
				(not (search_again))
				(not (in r128))
				(in r127)
			)
	)

	(:action move-right-from-r129
		:precondition
			(and 
				(in r129)
			)
		:effect
			(and
				(not (search_again))
				(not (in r129))
				(in r130)
			)
	)


	(:action move-left-from-r129
		:precondition
			(and 
				(in r129)
			)
		:effect
			(and
				(not (search_again))
				(not (in r129))
				(in r128)
			)
	)

	(:action move-right-from-r130
		:precondition
			(and 
				(in r130)
			)
		:effect
			(and
				(not (search_again))
				(not (in r130))
				(in r131)
			)
	)


	(:action move-left-from-r130
		:precondition
			(and 
				(in r130)
			)
		:effect
			(and
				(not (search_again))
				(not (in r130))
				(in r129)
			)
	)

	(:action move-right-from-r131
		:precondition
			(and 
				(in r131)
			)
		:effect
			(and
				(not (search_again))
				(not (in r131))
				(in r132)
			)
	)


	(:action move-left-from-r131
		:precondition
			(and 
				(in r131)
			)
		:effect
			(and
				(not (search_again))
				(not (in r131))
				(in r130)
			)
	)

	(:action move-right-from-r132
		:precondition
			(and 
				(in r132)
			)
		:effect
			(and
				(not (search_again))
				(not (in r132))
				(in r133)
			)
	)


	(:action move-left-from-r132
		:precondition
			(and 
				(in r132)
			)
		:effect
			(and
				(not (search_again))
				(not (in r132))
				(in r131)
			)
	)

	(:action move-right-from-r133
		:precondition
			(and 
				(in r133)
			)
		:effect
			(and
				(not (search_again))
				(not (in r133))
				(in r134)
			)
	)


	(:action move-left-from-r133
		:precondition
			(and 
				(in r133)
			)
		:effect
			(and
				(not (search_again))
				(not (in r133))
				(in r132)
			)
	)

	(:action move-right-from-r134
		:precondition
			(and 
				(in r134)
			)
		:effect
			(and
				(not (search_again))
				(not (in r134))
				(in r135)
			)
	)


	(:action move-left-from-r134
		:precondition
			(and 
				(in r134)
			)
		:effect
			(and
				(not (search_again))
				(not (in r134))
				(in r133)
			)
	)

	(:action move-right-from-r135
		:precondition
			(and 
				(in r135)
			)
		:effect
			(and
				(not (search_again))
				(not (in r135))
				(in r136)
			)
	)


	(:action move-left-from-r135
		:precondition
			(and 
				(in r135)
			)
		:effect
			(and
				(not (search_again))
				(not (in r135))
				(in r134)
			)
	)

	(:action move-right-from-r136
		:precondition
			(and 
				(in r136)
			)
		:effect
			(and
				(not (search_again))
				(not (in r136))
				(in r137)
			)
	)


	(:action move-left-from-r136
		:precondition
			(and 
				(in r136)
			)
		:effect
			(and
				(not (search_again))
				(not (in r136))
				(in r135)
			)
	)

	(:action move-right-from-r137
		:precondition
			(and 
				(in r137)
			)
		:effect
			(and
				(not (search_again))
				(not (in r137))
				(in r138)
			)
	)


	(:action move-left-from-r137
		:precondition
			(and 
				(in r137)
			)
		:effect
			(and
				(not (search_again))
				(not (in r137))
				(in r136)
			)
	)

	(:action move-right-from-r138
		:precondition
			(and 
				(in r138)
			)
		:effect
			(and
				(not (search_again))
				(not (in r138))
				(in r139)
			)
	)


	(:action move-left-from-r138
		:precondition
			(and 
				(in r138)
			)
		:effect
			(and
				(not (search_again))
				(not (in r138))
				(in r137)
			)
	)

	(:action move-right-from-r139
		:precondition
			(and 
				(in r139)
			)
		:effect
			(and
				(not (search_again))
				(not (in r139))
				(in r140)
			)
	)


	(:action move-left-from-r139
		:precondition
			(and 
				(in r139)
			)
		:effect
			(and
				(not (search_again))
				(not (in r139))
				(in r138)
			)
	)

	(:action move-right-from-r140
		:precondition
			(and 
				(in r140)
			)
		:effect
			(and
				(not (search_again))
				(not (in r140))
				(in r141)
			)
	)


	(:action move-left-from-r140
		:precondition
			(and 
				(in r140)
			)
		:effect
			(and
				(not (search_again))
				(not (in r140))
				(in r139)
			)
	)

	(:action move-right-from-r141
		:precondition
			(and 
				(in r141)
			)
		:effect
			(and
				(not (search_again))
				(not (in r141))
				(in r142)
			)
	)


	(:action move-left-from-r141
		:precondition
			(and 
				(in r141)
			)
		:effect
			(and
				(not (search_again))
				(not (in r141))
				(in r140)
			)
	)

	(:action move-right-from-r142
		:precondition
			(and 
				(in r142)
			)
		:effect
			(and
				(not (search_again))
				(not (in r142))
				(in r143)
			)
	)


	(:action move-left-from-r142
		:precondition
			(and 
				(in r142)
			)
		:effect
			(and
				(not (search_again))
				(not (in r142))
				(in r141)
			)
	)

	(:action move-right-from-r143
		:precondition
			(and 
				(in r143)
			)
		:effect
			(and
				(not (search_again))
				(not (in r143))
				(in r144)
			)
	)


	(:action move-left-from-r143
		:precondition
			(and 
				(in r143)
			)
		:effect
			(and
				(not (search_again))
				(not (in r143))
				(in r142)
			)
	)

	(:action move-right-from-r144
		:precondition
			(and 
				(in r144)
			)
		:effect
			(and
				(not (search_again))
				(not (in r144))
				(in r145)
			)
	)


	(:action move-left-from-r144
		:precondition
			(and 
				(in r144)
			)
		:effect
			(and
				(not (search_again))
				(not (in r144))
				(in r143)
			)
	)

	(:action move-right-from-r145
		:precondition
			(and 
				(in r145)
			)
		:effect
			(and
				(not (search_again))
				(not (in r145))
				(in r146)
			)
	)


	(:action move-left-from-r145
		:precondition
			(and 
				(in r145)
			)
		:effect
			(and
				(not (search_again))
				(not (in r145))
				(in r144)
			)
	)

	(:action move-right-from-r146
		:precondition
			(and 
				(in r146)
			)
		:effect
			(and
				(not (search_again))
				(not (in r146))
				(in r147)
			)
	)


	(:action move-left-from-r146
		:precondition
			(and 
				(in r146)
			)
		:effect
			(and
				(not (search_again))
				(not (in r146))
				(in r145)
			)
	)

	(:action move-right-from-r147
		:precondition
			(and 
				(in r147)
			)
		:effect
			(and
				(not (search_again))
				(not (in r147))
				(in r148)
			)
	)


	(:action move-left-from-r147
		:precondition
			(and 
				(in r147)
			)
		:effect
			(and
				(not (search_again))
				(not (in r147))
				(in r146)
			)
	)

	(:action move-right-from-r148
		:precondition
			(and 
				(in r148)
			)
		:effect
			(and
				(not (search_again))
				(not (in r148))
				(in r149)
			)
	)


	(:action move-left-from-r148
		:precondition
			(and 
				(in r148)
			)
		:effect
			(and
				(not (search_again))
				(not (in r148))
				(in r147)
			)
	)

	(:action move-right-from-r149
		:precondition
			(and 
				(in r149)
			)
		:effect
			(and
				(not (search_again))
				(not (in r149))
				(in r150)
			)
	)


	(:action move-left-from-r149
		:precondition
			(and 
				(in r149)
			)
		:effect
			(and
				(not (search_again))
				(not (in r149))
				(in r148)
			)
	)

	(:action move-right-from-r150
		:precondition
			(and 
				(in r150)
			)
		:effect
			(and
				(not (search_again))
				(not (in r150))
				(in r151)
			)
	)


	(:action move-left-from-r150
		:precondition
			(and 
				(in r150)
			)
		:effect
			(and
				(not (search_again))
				(not (in r150))
				(in r149)
			)
	)

	(:action move-right-from-r151
		:precondition
			(and 
				(in r151)
			)
		:effect
			(and
				(not (search_again))
				(not (in r151))
				(in r152)
			)
	)


	(:action move-left-from-r151
		:precondition
			(and 
				(in r151)
			)
		:effect
			(and
				(not (search_again))
				(not (in r151))
				(in r150)
			)
	)

	(:action move-right-from-r152
		:precondition
			(and 
				(in r152)
			)
		:effect
			(and
				(not (search_again))
				(not (in r152))
				(in r153)
			)
	)


	(:action move-left-from-r152
		:precondition
			(and 
				(in r152)
			)
		:effect
			(and
				(not (search_again))
				(not (in r152))
				(in r151)
			)
	)

	(:action move-right-from-r153
		:precondition
			(and 
				(in r153)
			)
		:effect
			(and
				(not (search_again))
				(not (in r153))
				(in r154)
			)
	)


	(:action move-left-from-r153
		:precondition
			(and 
				(in r153)
			)
		:effect
			(and
				(not (search_again))
				(not (in r153))
				(in r152)
			)
	)

	(:action move-right-from-r154
		:precondition
			(and 
				(in r154)
			)
		:effect
			(and
				(not (search_again))
				(not (in r154))
				(in r155)
			)
	)


	(:action move-left-from-r154
		:precondition
			(and 
				(in r154)
			)
		:effect
			(and
				(not (search_again))
				(not (in r154))
				(in r153)
			)
	)

	(:action move-right-from-r155
		:precondition
			(and 
				(in r155)
			)
		:effect
			(and
				(not (search_again))
				(not (in r155))
				(in r156)
			)
	)


	(:action move-left-from-r155
		:precondition
			(and 
				(in r155)
			)
		:effect
			(and
				(not (search_again))
				(not (in r155))
				(in r154)
			)
	)

	(:action move-right-from-r156
		:precondition
			(and 
				(in r156)
			)
		:effect
			(and
				(not (search_again))
				(not (in r156))
				(in r157)
			)
	)


	(:action move-left-from-r156
		:precondition
			(and 
				(in r156)
			)
		:effect
			(and
				(not (search_again))
				(not (in r156))
				(in r155)
			)
	)

	(:action move-right-from-r157
		:precondition
			(and 
				(in r157)
			)
		:effect
			(and
				(not (search_again))
				(not (in r157))
				(in r158)
			)
	)


	(:action move-left-from-r157
		:precondition
			(and 
				(in r157)
			)
		:effect
			(and
				(not (search_again))
				(not (in r157))
				(in r156)
			)
	)

	(:action move-right-from-r158
		:precondition
			(and 
				(in r158)
			)
		:effect
			(and
				(not (search_again))
				(not (in r158))
				(in r159)
			)
	)


	(:action move-left-from-r158
		:precondition
			(and 
				(in r158)
			)
		:effect
			(and
				(not (search_again))
				(not (in r158))
				(in r157)
			)
	)

	(:action move-right-from-r159
		:precondition
			(and 
				(in r159)
			)
		:effect
			(and
				(not (search_again))
				(not (in r159))
				(in r160)
			)
	)


	(:action move-left-from-r159
		:precondition
			(and 
				(in r159)
			)
		:effect
			(and
				(not (search_again))
				(not (in r159))
				(in r158)
			)
	)

	(:action move-right-from-r160
		:precondition
			(and 
				(in r160)
			)
		:effect
			(and
				(not (search_again))
				(not (in r160))
				(in r161)
			)
	)


	(:action move-left-from-r160
		:precondition
			(and 
				(in r160)
			)
		:effect
			(and
				(not (search_again))
				(not (in r160))
				(in r159)
			)
	)

	(:action move-right-from-r161
		:precondition
			(and 
				(in r161)
			)
		:effect
			(and
				(not (search_again))
				(not (in r161))
				(in r162)
			)
	)


	(:action move-left-from-r161
		:precondition
			(and 
				(in r161)
			)
		:effect
			(and
				(not (search_again))
				(not (in r161))
				(in r160)
			)
	)

	(:action move-right-from-r162
		:precondition
			(and 
				(in r162)
			)
		:effect
			(and
				(not (search_again))
				(not (in r162))
				(in r163)
			)
	)


	(:action move-left-from-r162
		:precondition
			(and 
				(in r162)
			)
		:effect
			(and
				(not (search_again))
				(not (in r162))
				(in r161)
			)
	)

	(:action move-right-from-r163
		:precondition
			(and 
				(in r163)
			)
		:effect
			(and
				(not (search_again))
				(not (in r163))
				(in r164)
			)
	)


	(:action move-left-from-r163
		:precondition
			(and 
				(in r163)
			)
		:effect
			(and
				(not (search_again))
				(not (in r163))
				(in r162)
			)
	)

	(:action move-right-from-r164
		:precondition
			(and 
				(in r164)
			)
		:effect
			(and
				(not (search_again))
				(not (in r164))
				(in r165)
			)
	)


	(:action move-left-from-r164
		:precondition
			(and 
				(in r164)
			)
		:effect
			(and
				(not (search_again))
				(not (in r164))
				(in r163)
			)
	)

	(:action move-right-from-r165
		:precondition
			(and 
				(in r165)
			)
		:effect
			(and
				(not (search_again))
				(not (in r165))
				(in r166)
			)
	)


	(:action move-left-from-r165
		:precondition
			(and 
				(in r165)
			)
		:effect
			(and
				(not (search_again))
				(not (in r165))
				(in r164)
			)
	)

	(:action move-right-from-r166
		:precondition
			(and 
				(in r166)
			)
		:effect
			(and
				(not (search_again))
				(not (in r166))
				(in r167)
			)
	)


	(:action move-left-from-r166
		:precondition
			(and 
				(in r166)
			)
		:effect
			(and
				(not (search_again))
				(not (in r166))
				(in r165)
			)
	)

	(:action move-right-from-r167
		:precondition
			(and 
				(in r167)
			)
		:effect
			(and
				(not (search_again))
				(not (in r167))
				(in r168)
			)
	)


	(:action move-left-from-r167
		:precondition
			(and 
				(in r167)
			)
		:effect
			(and
				(not (search_again))
				(not (in r167))
				(in r166)
			)
	)

	(:action move-right-from-r168
		:precondition
			(and 
				(in r168)
			)
		:effect
			(and
				(not (search_again))
				(not (in r168))
				(in r169)
			)
	)


	(:action move-left-from-r168
		:precondition
			(and 
				(in r168)
			)
		:effect
			(and
				(not (search_again))
				(not (in r168))
				(in r167)
			)
	)

	(:action move-right-from-r169
		:precondition
			(and 
				(in r169)
			)
		:effect
			(and
				(not (search_again))
				(not (in r169))
				(in r170)
			)
	)


	(:action move-left-from-r169
		:precondition
			(and 
				(in r169)
			)
		:effect
			(and
				(not (search_again))
				(not (in r169))
				(in r168)
			)
	)

	(:action move-right-from-r170
		:precondition
			(and 
				(in r170)
			)
		:effect
			(and
				(not (search_again))
				(not (in r170))
				(in r171)
			)
	)


	(:action move-left-from-r170
		:precondition
			(and 
				(in r170)
			)
		:effect
			(and
				(not (search_again))
				(not (in r170))
				(in r169)
			)
	)

	(:action move-right-from-r171
		:precondition
			(and 
				(in r171)
			)
		:effect
			(and
				(not (search_again))
				(not (in r171))
				(in r172)
			)
	)


	(:action move-left-from-r171
		:precondition
			(and 
				(in r171)
			)
		:effect
			(and
				(not (search_again))
				(not (in r171))
				(in r170)
			)
	)

	(:action move-right-from-r172
		:precondition
			(and 
				(in r172)
			)
		:effect
			(and
				(not (search_again))
				(not (in r172))
				(in r173)
			)
	)


	(:action move-left-from-r172
		:precondition
			(and 
				(in r172)
			)
		:effect
			(and
				(not (search_again))
				(not (in r172))
				(in r171)
			)
	)

	(:action move-right-from-r173
		:precondition
			(and 
				(in r173)
			)
		:effect
			(and
				(not (search_again))
				(not (in r173))
				(in r174)
			)
	)


	(:action move-left-from-r173
		:precondition
			(and 
				(in r173)
			)
		:effect
			(and
				(not (search_again))
				(not (in r173))
				(in r172)
			)
	)

	(:action move-right-from-r174
		:precondition
			(and 
				(in r174)
			)
		:effect
			(and
				(not (search_again))
				(not (in r174))
				(in r175)
			)
	)


	(:action move-left-from-r174
		:precondition
			(and 
				(in r174)
			)
		:effect
			(and
				(not (search_again))
				(not (in r174))
				(in r173)
			)
	)

	(:action move-right-from-r175
		:precondition
			(and 
				(in r175)
			)
		:effect
			(and
				(not (search_again))
				(not (in r175))
				(in r176)
			)
	)


	(:action move-left-from-r175
		:precondition
			(and 
				(in r175)
			)
		:effect
			(and
				(not (search_again))
				(not (in r175))
				(in r174)
			)
	)

	(:action move-right-from-r176
		:precondition
			(and 
				(in r176)
			)
		:effect
			(and
				(not (search_again))
				(not (in r176))
				(in r177)
			)
	)


	(:action move-left-from-r176
		:precondition
			(and 
				(in r176)
			)
		:effect
			(and
				(not (search_again))
				(not (in r176))
				(in r175)
			)
	)

	(:action move-right-from-r177
		:precondition
			(and 
				(in r177)
			)
		:effect
			(and
				(not (search_again))
				(not (in r177))
				(in r178)
			)
	)


	(:action move-left-from-r177
		:precondition
			(and 
				(in r177)
			)
		:effect
			(and
				(not (search_again))
				(not (in r177))
				(in r176)
			)
	)

	(:action move-right-from-r178
		:precondition
			(and 
				(in r178)
			)
		:effect
			(and
				(not (search_again))
				(not (in r178))
				(in r179)
			)
	)


	(:action move-left-from-r178
		:precondition
			(and 
				(in r178)
			)
		:effect
			(and
				(not (search_again))
				(not (in r178))
				(in r177)
			)
	)

	(:action move-right-from-r179
		:precondition
			(and 
				(in r179)
			)
		:effect
			(and
				(not (search_again))
				(not (in r179))
				(in r180)
			)
	)


	(:action move-left-from-r179
		:precondition
			(and 
				(in r179)
			)
		:effect
			(and
				(not (search_again))
				(not (in r179))
				(in r178)
			)
	)

	(:action move-right-from-r180
		:precondition
			(and 
				(in r180)
			)
		:effect
			(and
				(not (search_again))
				(not (in r180))
				(in r181)
			)
	)


	(:action move-left-from-r180
		:precondition
			(and 
				(in r180)
			)
		:effect
			(and
				(not (search_again))
				(not (in r180))
				(in r179)
			)
	)

	(:action move-right-from-r181
		:precondition
			(and 
				(in r181)
			)
		:effect
			(and
				(not (search_again))
				(not (in r181))
				(in r182)
			)
	)


	(:action move-left-from-r181
		:precondition
			(and 
				(in r181)
			)
		:effect
			(and
				(not (search_again))
				(not (in r181))
				(in r180)
			)
	)

	(:action move-right-from-r182
		:precondition
			(and 
				(in r182)
			)
		:effect
			(and
				(not (search_again))
				(not (in r182))
				(in r183)
			)
	)


	(:action move-left-from-r182
		:precondition
			(and 
				(in r182)
			)
		:effect
			(and
				(not (search_again))
				(not (in r182))
				(in r181)
			)
	)

	(:action move-right-from-r183
		:precondition
			(and 
				(in r183)
			)
		:effect
			(and
				(not (search_again))
				(not (in r183))
				(in r184)
			)
	)


	(:action move-left-from-r183
		:precondition
			(and 
				(in r183)
			)
		:effect
			(and
				(not (search_again))
				(not (in r183))
				(in r182)
			)
	)

	(:action move-right-from-r184
		:precondition
			(and 
				(in r184)
			)
		:effect
			(and
				(not (search_again))
				(not (in r184))
				(in r185)
			)
	)


	(:action move-left-from-r184
		:precondition
			(and 
				(in r184)
			)
		:effect
			(and
				(not (search_again))
				(not (in r184))
				(in r183)
			)
	)

	(:action move-right-from-r185
		:precondition
			(and 
				(in r185)
			)
		:effect
			(and
				(not (search_again))
				(not (in r185))
				(in r186)
			)
	)


	(:action move-left-from-r185
		:precondition
			(and 
				(in r185)
			)
		:effect
			(and
				(not (search_again))
				(not (in r185))
				(in r184)
			)
	)

	(:action move-right-from-r186
		:precondition
			(and 
				(in r186)
			)
		:effect
			(and
				(not (search_again))
				(not (in r186))
				(in r187)
			)
	)


	(:action move-left-from-r186
		:precondition
			(and 
				(in r186)
			)
		:effect
			(and
				(not (search_again))
				(not (in r186))
				(in r185)
			)
	)

	(:action move-right-from-r187
		:precondition
			(and 
				(in r187)
			)
		:effect
			(and
				(not (search_again))
				(not (in r187))
				(in r188)
			)
	)


	(:action move-left-from-r187
		:precondition
			(and 
				(in r187)
			)
		:effect
			(and
				(not (search_again))
				(not (in r187))
				(in r186)
			)
	)

	(:action move-right-from-r188
		:precondition
			(and 
				(in r188)
			)
		:effect
			(and
				(not (search_again))
				(not (in r188))
				(in r189)
			)
	)


	(:action move-left-from-r188
		:precondition
			(and 
				(in r188)
			)
		:effect
			(and
				(not (search_again))
				(not (in r188))
				(in r187)
			)
	)

	(:action move-right-from-r189
		:precondition
			(and 
				(in r189)
			)
		:effect
			(and
				(not (search_again))
				(not (in r189))
				(in r190)
			)
	)


	(:action move-left-from-r189
		:precondition
			(and 
				(in r189)
			)
		:effect
			(and
				(not (search_again))
				(not (in r189))
				(in r188)
			)
	)

	(:action move-right-from-r190
		:precondition
			(and 
				(in r190)
			)
		:effect
			(and
				(not (search_again))
				(not (in r190))
				(in r191)
			)
	)


	(:action move-left-from-r190
		:precondition
			(and 
				(in r190)
			)
		:effect
			(and
				(not (search_again))
				(not (in r190))
				(in r189)
			)
	)

	(:action move-right-from-r191
		:precondition
			(and 
				(in r191)
			)
		:effect
			(and
				(not (search_again))
				(not (in r191))
				(in r192)
			)
	)


	(:action move-left-from-r191
		:precondition
			(and 
				(in r191)
			)
		:effect
			(and
				(not (search_again))
				(not (in r191))
				(in r190)
			)
	)

	(:action move-right-from-r192
		:precondition
			(and 
				(in r192)
			)
		:effect
			(and
				(not (search_again))
				(not (in r192))
				(in r193)
			)
	)


	(:action move-left-from-r192
		:precondition
			(and 
				(in r192)
			)
		:effect
			(and
				(not (search_again))
				(not (in r192))
				(in r191)
			)
	)

	(:action move-right-from-r193
		:precondition
			(and 
				(in r193)
			)
		:effect
			(and
				(not (search_again))
				(not (in r193))
				(in r194)
			)
	)


	(:action move-left-from-r193
		:precondition
			(and 
				(in r193)
			)
		:effect
			(and
				(not (search_again))
				(not (in r193))
				(in r192)
			)
	)

	(:action move-right-from-r194
		:precondition
			(and 
				(in r194)
			)
		:effect
			(and
				(not (search_again))
				(not (in r194))
				(in r195)
			)
	)


	(:action move-left-from-r194
		:precondition
			(and 
				(in r194)
			)
		:effect
			(and
				(not (search_again))
				(not (in r194))
				(in r193)
			)
	)

	(:action move-right-from-r195
		:precondition
			(and 
				(in r195)
			)
		:effect
			(and
				(not (search_again))
				(not (in r195))
				(in r196)
			)
	)


	(:action move-left-from-r195
		:precondition
			(and 
				(in r195)
			)
		:effect
			(and
				(not (search_again))
				(not (in r195))
				(in r194)
			)
	)

	(:action move-right-from-r196
		:precondition
			(and 
				(in r196)
			)
		:effect
			(and
				(not (search_again))
				(not (in r196))
				(in r197)
			)
	)


	(:action move-left-from-r196
		:precondition
			(and 
				(in r196)
			)
		:effect
			(and
				(not (search_again))
				(not (in r196))
				(in r195)
			)
	)

	(:action move-right-from-r197
		:precondition
			(and 
				(in r197)
			)
		:effect
			(and
				(not (search_again))
				(not (in r197))
				(in r198)
			)
	)


	(:action move-left-from-r197
		:precondition
			(and 
				(in r197)
			)
		:effect
			(and
				(not (search_again))
				(not (in r197))
				(in r196)
			)
	)

	(:action move-right-from-r198
		:precondition
			(and 
				(in r198)
			)
		:effect
			(and
				(not (search_again))
				(not (in r198))
				(in r199)
			)
	)


	(:action move-left-from-r198
		:precondition
			(and 
				(in r198)
			)
		:effect
			(and
				(not (search_again))
				(not (in r198))
				(in r197)
			)
	)

	(:action move-right-from-r199
		:precondition
			(and 
				(in r199)
			)
		:effect
			(and
				(not (search_again))
				(not (in r199))
				(in r200)
			)
	)


	(:action move-left-from-r199
		:precondition
			(and 
				(in r199)
			)
		:effect
			(and
				(not (search_again))
				(not (in r199))
				(in r198)
			)
	)

	(:action move-right-from-r200
		:precondition
			(and 
				(in r200)
			)
		:effect
			(and
				(not (search_again))
				(not (in r200))
				(in r201)
			)
	)


	(:action move-left-from-r200
		:precondition
			(and 
				(in r200)
			)
		:effect
			(and
				(not (search_again))
				(not (in r200))
				(in r199)
			)
	)

	(:action move-right-from-r201
		:precondition
			(and 
				(in r201)
			)
		:effect
			(and
				(not (search_again))
				(not (in r201))
				(in r202)
			)
	)


	(:action move-left-from-r201
		:precondition
			(and 
				(in r201)
			)
		:effect
			(and
				(not (search_again))
				(not (in r201))
				(in r200)
			)
	)

	(:action move-right-from-r202
		:precondition
			(and 
				(in r202)
			)
		:effect
			(and
				(not (search_again))
				(not (in r202))
				(in r203)
			)
	)


	(:action move-left-from-r202
		:precondition
			(and 
				(in r202)
			)
		:effect
			(and
				(not (search_again))
				(not (in r202))
				(in r201)
			)
	)

	(:action move-right-from-r203
		:precondition
			(and 
				(in r203)
			)
		:effect
			(and
				(not (search_again))
				(not (in r203))
				(in r204)
			)
	)


	(:action move-left-from-r203
		:precondition
			(and 
				(in r203)
			)
		:effect
			(and
				(not (search_again))
				(not (in r203))
				(in r202)
			)
	)

	(:action move-right-from-r204
		:precondition
			(and 
				(in r204)
			)
		:effect
			(and
				(not (search_again))
				(not (in r204))
				(in r205)
			)
	)


	(:action move-left-from-r204
		:precondition
			(and 
				(in r204)
			)
		:effect
			(and
				(not (search_again))
				(not (in r204))
				(in r203)
			)
	)

	(:action move-right-from-r205
		:precondition
			(and 
				(in r205)
			)
		:effect
			(and
				(not (search_again))
				(not (in r205))
				(in r206)
			)
	)


	(:action move-left-from-r205
		:precondition
			(and 
				(in r205)
			)
		:effect
			(and
				(not (search_again))
				(not (in r205))
				(in r204)
			)
	)

	(:action move-right-from-r206
		:precondition
			(and 
				(in r206)
			)
		:effect
			(and
				(not (search_again))
				(not (in r206))
				(in r207)
			)
	)


	(:action move-left-from-r206
		:precondition
			(and 
				(in r206)
			)
		:effect
			(and
				(not (search_again))
				(not (in r206))
				(in r205)
			)
	)

	(:action move-right-from-r207
		:precondition
			(and 
				(in r207)
			)
		:effect
			(and
				(not (search_again))
				(not (in r207))
				(in r208)
			)
	)


	(:action move-left-from-r207
		:precondition
			(and 
				(in r207)
			)
		:effect
			(and
				(not (search_again))
				(not (in r207))
				(in r206)
			)
	)

	(:action move-right-from-r208
		:precondition
			(and 
				(in r208)
			)
		:effect
			(and
				(not (search_again))
				(not (in r208))
				(in r209)
			)
	)


	(:action move-left-from-r208
		:precondition
			(and 
				(in r208)
			)
		:effect
			(and
				(not (search_again))
				(not (in r208))
				(in r207)
			)
	)

	(:action move-right-from-r209
		:precondition
			(and 
				(in r209)
			)
		:effect
			(and
				(not (search_again))
				(not (in r209))
				(in r210)
			)
	)


	(:action move-left-from-r209
		:precondition
			(and 
				(in r209)
			)
		:effect
			(and
				(not (search_again))
				(not (in r209))
				(in r208)
			)
	)

	(:action move-right-from-r210
		:precondition
			(and 
				(in r210)
			)
		:effect
			(and
				(not (search_again))
				(not (in r210))
				(in r211)
			)
	)


	(:action move-left-from-r210
		:precondition
			(and 
				(in r210)
			)
		:effect
			(and
				(not (search_again))
				(not (in r210))
				(in r209)
			)
	)

	(:action move-right-from-r211
		:precondition
			(and 
				(in r211)
			)
		:effect
			(and
				(not (search_again))
				(not (in r211))
				(in r212)
			)
	)


	(:action move-left-from-r211
		:precondition
			(and 
				(in r211)
			)
		:effect
			(and
				(not (search_again))
				(not (in r211))
				(in r210)
			)
	)

	(:action move-right-from-r212
		:precondition
			(and 
				(in r212)
			)
		:effect
			(and
				(not (search_again))
				(not (in r212))
				(in r213)
			)
	)


	(:action move-left-from-r212
		:precondition
			(and 
				(in r212)
			)
		:effect
			(and
				(not (search_again))
				(not (in r212))
				(in r211)
			)
	)

	(:action move-right-from-r213
		:precondition
			(and 
				(in r213)
			)
		:effect
			(and
				(not (search_again))
				(not (in r213))
				(in r214)
			)
	)


	(:action move-left-from-r213
		:precondition
			(and 
				(in r213)
			)
		:effect
			(and
				(not (search_again))
				(not (in r213))
				(in r212)
			)
	)

	(:action move-right-from-r214
		:precondition
			(and 
				(in r214)
			)
		:effect
			(and
				(not (search_again))
				(not (in r214))
				(in r215)
			)
	)


	(:action move-left-from-r214
		:precondition
			(and 
				(in r214)
			)
		:effect
			(and
				(not (search_again))
				(not (in r214))
				(in r213)
			)
	)

	(:action move-right-from-r215
		:precondition
			(and 
				(in r215)
			)
		:effect
			(and
				(not (search_again))
				(not (in r215))
				(in r216)
			)
	)


	(:action move-left-from-r215
		:precondition
			(and 
				(in r215)
			)
		:effect
			(and
				(not (search_again))
				(not (in r215))
				(in r214)
			)
	)

	(:action move-right-from-r216
		:precondition
			(and 
				(in r216)
			)
		:effect
			(and
				(not (search_again))
				(not (in r216))
				(in r217)
			)
	)


	(:action move-left-from-r216
		:precondition
			(and 
				(in r216)
			)
		:effect
			(and
				(not (search_again))
				(not (in r216))
				(in r215)
			)
	)

	(:action move-right-from-r217
		:precondition
			(and 
				(in r217)
			)
		:effect
			(and
				(not (search_again))
				(not (in r217))
				(in r218)
			)
	)


	(:action move-left-from-r217
		:precondition
			(and 
				(in r217)
			)
		:effect
			(and
				(not (search_again))
				(not (in r217))
				(in r216)
			)
	)

	(:action move-right-from-r218
		:precondition
			(and 
				(in r218)
			)
		:effect
			(and
				(not (search_again))
				(not (in r218))
				(in r219)
			)
	)


	(:action move-left-from-r218
		:precondition
			(and 
				(in r218)
			)
		:effect
			(and
				(not (search_again))
				(not (in r218))
				(in r217)
			)
	)

	(:action move-right-from-r219
		:precondition
			(and 
				(in r219)
			)
		:effect
			(and
				(not (search_again))
				(not (in r219))
				(in r220)
			)
	)


	(:action move-left-from-r219
		:precondition
			(and 
				(in r219)
			)
		:effect
			(and
				(not (search_again))
				(not (in r219))
				(in r218)
			)
	)

	(:action move-right-from-r220
		:precondition
			(and 
				(in r220)
			)
		:effect
			(and
				(not (search_again))
				(not (in r220))
				(in r221)
			)
	)


	(:action move-left-from-r220
		:precondition
			(and 
				(in r220)
			)
		:effect
			(and
				(not (search_again))
				(not (in r220))
				(in r219)
			)
	)

	(:action move-right-from-r221
		:precondition
			(and 
				(in r221)
			)
		:effect
			(and
				(not (search_again))
				(not (in r221))
				(in r222)
			)
	)


	(:action move-left-from-r221
		:precondition
			(and 
				(in r221)
			)
		:effect
			(and
				(not (search_again))
				(not (in r221))
				(in r220)
			)
	)

	(:action move-right-from-r222
		:precondition
			(and 
				(in r222)
			)
		:effect
			(and
				(not (search_again))
				(not (in r222))
				(in r223)
			)
	)


	(:action move-left-from-r222
		:precondition
			(and 
				(in r222)
			)
		:effect
			(and
				(not (search_again))
				(not (in r222))
				(in r221)
			)
	)

	(:action move-right-from-r223
		:precondition
			(and 
				(in r223)
			)
		:effect
			(and
				(not (search_again))
				(not (in r223))
				(in r224)
			)
	)


	(:action move-left-from-r223
		:precondition
			(and 
				(in r223)
			)
		:effect
			(and
				(not (search_again))
				(not (in r223))
				(in r222)
			)
	)

	(:action move-right-from-r224
		:precondition
			(and 
				(in r224)
			)
		:effect
			(and
				(not (search_again))
				(not (in r224))
				(in r225)
			)
	)


	(:action move-left-from-r224
		:precondition
			(and 
				(in r224)
			)
		:effect
			(and
				(not (search_again))
				(not (in r224))
				(in r223)
			)
	)

	(:action move-right-from-r225
		:precondition
			(and 
				(in r225)
			)
		:effect
			(and
				(not (search_again))
				(not (in r225))
				(in r226)
			)
	)


	(:action move-left-from-r225
		:precondition
			(and 
				(in r225)
			)
		:effect
			(and
				(not (search_again))
				(not (in r225))
				(in r224)
			)
	)

	(:action move-right-from-r226
		:precondition
			(and 
				(in r226)
			)
		:effect
			(and
				(not (search_again))
				(not (in r226))
				(in r227)
			)
	)


	(:action move-left-from-r226
		:precondition
			(and 
				(in r226)
			)
		:effect
			(and
				(not (search_again))
				(not (in r226))
				(in r225)
			)
	)

	(:action move-right-from-r227
		:precondition
			(and 
				(in r227)
			)
		:effect
			(and
				(not (search_again))
				(not (in r227))
				(in r228)
			)
	)


	(:action move-left-from-r227
		:precondition
			(and 
				(in r227)
			)
		:effect
			(and
				(not (search_again))
				(not (in r227))
				(in r226)
			)
	)

	(:action move-right-from-r228
		:precondition
			(and 
				(in r228)
			)
		:effect
			(and
				(not (search_again))
				(not (in r228))
				(in r229)
			)
	)


	(:action move-left-from-r228
		:precondition
			(and 
				(in r228)
			)
		:effect
			(and
				(not (search_again))
				(not (in r228))
				(in r227)
			)
	)

	(:action move-right-from-r229
		:precondition
			(and 
				(in r229)
			)
		:effect
			(and
				(not (search_again))
				(not (in r229))
				(in r230)
			)
	)


	(:action move-left-from-r229
		:precondition
			(and 
				(in r229)
			)
		:effect
			(and
				(not (search_again))
				(not (in r229))
				(in r228)
			)
	)

	(:action move-right-from-r230
		:precondition
			(and 
				(in r230)
			)
		:effect
			(and
				(not (search_again))
				(not (in r230))
				(in r231)
			)
	)


	(:action move-left-from-r230
		:precondition
			(and 
				(in r230)
			)
		:effect
			(and
				(not (search_again))
				(not (in r230))
				(in r229)
			)
	)

	(:action move-right-from-r231
		:precondition
			(and 
				(in r231)
			)
		:effect
			(and
				(not (search_again))
				(not (in r231))
				(in r232)
			)
	)


	(:action move-left-from-r231
		:precondition
			(and 
				(in r231)
			)
		:effect
			(and
				(not (search_again))
				(not (in r231))
				(in r230)
			)
	)

	(:action move-right-from-r232
		:precondition
			(and 
				(in r232)
			)
		:effect
			(and
				(not (search_again))
				(not (in r232))
				(in r233)
			)
	)


	(:action move-left-from-r232
		:precondition
			(and 
				(in r232)
			)
		:effect
			(and
				(not (search_again))
				(not (in r232))
				(in r231)
			)
	)

	(:action move-right-from-r233
		:precondition
			(and 
				(in r233)
			)
		:effect
			(and
				(not (search_again))
				(not (in r233))
				(in r234)
			)
	)


	(:action move-left-from-r233
		:precondition
			(and 
				(in r233)
			)
		:effect
			(and
				(not (search_again))
				(not (in r233))
				(in r232)
			)
	)

	(:action move-right-from-r234
		:precondition
			(and 
				(in r234)
			)
		:effect
			(and
				(not (search_again))
				(not (in r234))
				(in r235)
			)
	)


	(:action move-left-from-r234
		:precondition
			(and 
				(in r234)
			)
		:effect
			(and
				(not (search_again))
				(not (in r234))
				(in r233)
			)
	)

	(:action move-right-from-r235
		:precondition
			(and 
				(in r235)
			)
		:effect
			(and
				(not (search_again))
				(not (in r235))
				(in r236)
			)
	)


	(:action move-left-from-r235
		:precondition
			(and 
				(in r235)
			)
		:effect
			(and
				(not (search_again))
				(not (in r235))
				(in r234)
			)
	)

	(:action move-right-from-r236
		:precondition
			(and 
				(in r236)
			)
		:effect
			(and
				(not (search_again))
				(not (in r236))
				(in r237)
			)
	)


	(:action move-left-from-r236
		:precondition
			(and 
				(in r236)
			)
		:effect
			(and
				(not (search_again))
				(not (in r236))
				(in r235)
			)
	)

	(:action move-right-from-r237
		:precondition
			(and 
				(in r237)
			)
		:effect
			(and
				(not (search_again))
				(not (in r237))
				(in r238)
			)
	)


	(:action move-left-from-r237
		:precondition
			(and 
				(in r237)
			)
		:effect
			(and
				(not (search_again))
				(not (in r237))
				(in r236)
			)
	)

	(:action move-right-from-r238
		:precondition
			(and 
				(in r238)
			)
		:effect
			(and
				(not (search_again))
				(not (in r238))
				(in r239)
			)
	)


	(:action move-left-from-r238
		:precondition
			(and 
				(in r238)
			)
		:effect
			(and
				(not (search_again))
				(not (in r238))
				(in r237)
			)
	)

	(:action move-right-from-r239
		:precondition
			(and 
				(in r239)
			)
		:effect
			(and
				(not (search_again))
				(not (in r239))
				(in r240)
			)
	)


	(:action move-left-from-r239
		:precondition
			(and 
				(in r239)
			)
		:effect
			(and
				(not (search_again))
				(not (in r239))
				(in r238)
			)
	)

	(:action move-right-from-r240
		:precondition
			(and 
				(in r240)
			)
		:effect
			(and
				(not (search_again))
				(not (in r240))
				(in r241)
			)
	)


	(:action move-left-from-r240
		:precondition
			(and 
				(in r240)
			)
		:effect
			(and
				(not (search_again))
				(not (in r240))
				(in r239)
			)
	)

	(:action move-right-from-r241
		:precondition
			(and 
				(in r241)
			)
		:effect
			(and
				(not (search_again))
				(not (in r241))
				(in r242)
			)
	)


	(:action move-left-from-r241
		:precondition
			(and 
				(in r241)
			)
		:effect
			(and
				(not (search_again))
				(not (in r241))
				(in r240)
			)
	)

	(:action move-right-from-r242
		:precondition
			(and 
				(in r242)
			)
		:effect
			(and
				(not (search_again))
				(not (in r242))
				(in r243)
			)
	)


	(:action move-left-from-r242
		:precondition
			(and 
				(in r242)
			)
		:effect
			(and
				(not (search_again))
				(not (in r242))
				(in r241)
			)
	)

	(:action move-right-from-r243
		:precondition
			(and 
				(in r243)
			)
		:effect
			(and
				(not (search_again))
				(not (in r243))
				(in r244)
			)
	)


	(:action move-left-from-r243
		:precondition
			(and 
				(in r243)
			)
		:effect
			(and
				(not (search_again))
				(not (in r243))
				(in r242)
			)
	)

	(:action move-right-from-r244
		:precondition
			(and 
				(in r244)
			)
		:effect
			(and
				(not (search_again))
				(not (in r244))
				(in r245)
			)
	)


	(:action move-left-from-r244
		:precondition
			(and 
				(in r244)
			)
		:effect
			(and
				(not (search_again))
				(not (in r244))
				(in r243)
			)
	)

	(:action move-right-from-r245
		:precondition
			(and 
				(in r245)
			)
		:effect
			(and
				(not (search_again))
				(not (in r245))
				(in r246)
			)
	)


	(:action move-left-from-r245
		:precondition
			(and 
				(in r245)
			)
		:effect
			(and
				(not (search_again))
				(not (in r245))
				(in r244)
			)
	)

	(:action move-right-from-r246
		:precondition
			(and 
				(in r246)
			)
		:effect
			(and
				(not (search_again))
				(not (in r246))
				(in r247)
			)
	)


	(:action move-left-from-r246
		:precondition
			(and 
				(in r246)
			)
		:effect
			(and
				(not (search_again))
				(not (in r246))
				(in r245)
			)
	)

	(:action move-right-from-r247
		:precondition
			(and 
				(in r247)
			)
		:effect
			(and
				(not (search_again))
				(not (in r247))
				(in r248)
			)
	)


	(:action move-left-from-r247
		:precondition
			(and 
				(in r247)
			)
		:effect
			(and
				(not (search_again))
				(not (in r247))
				(in r246)
			)
	)

	(:action move-right-from-r248
		:precondition
			(and 
				(in r248)
			)
		:effect
			(and
				(not (search_again))
				(not (in r248))
				(in r249)
			)
	)


	(:action move-left-from-r248
		:precondition
			(and 
				(in r248)
			)
		:effect
			(and
				(not (search_again))
				(not (in r248))
				(in r247)
			)
	)

	(:action move-right-from-r249
		:precondition
			(and 
				(in r249)
			)
		:effect
			(and
				(not (search_again))
				(not (in r249))
				(in r250)
			)
	)


	(:action move-left-from-r249
		:precondition
			(and 
				(in r249)
			)
		:effect
			(and
				(not (search_again))
				(not (in r249))
				(in r248)
			)
	)

	(:action move-right-from-r250
		:precondition
			(and 
				(in r250)
			)
		:effect
			(and
				(not (search_again))
				(not (in r250))
				(in r251)
			)
	)


	(:action move-left-from-r250
		:precondition
			(and 
				(in r250)
			)
		:effect
			(and
				(not (search_again))
				(not (in r250))
				(in r249)
			)
	)

	(:action move-right-from-r251
		:precondition
			(and 
				(in r251)
			)
		:effect
			(and
				(not (search_again))
				(not (in r251))
				(in r252)
			)
	)


	(:action move-left-from-r251
		:precondition
			(and 
				(in r251)
			)
		:effect
			(and
				(not (search_again))
				(not (in r251))
				(in r250)
			)
	)

	(:action move-right-from-r252
		:precondition
			(and 
				(in r252)
			)
		:effect
			(and
				(not (search_again))
				(not (in r252))
				(in r253)
			)
	)


	(:action move-left-from-r252
		:precondition
			(and 
				(in r252)
			)
		:effect
			(and
				(not (search_again))
				(not (in r252))
				(in r251)
			)
	)

	(:action move-right-from-r253
		:precondition
			(and 
				(in r253)
			)
		:effect
			(and
				(not (search_again))
				(not (in r253))
				(in r254)
			)
	)


	(:action move-left-from-r253
		:precondition
			(and 
				(in r253)
			)
		:effect
			(and
				(not (search_again))
				(not (in r253))
				(in r252)
			)
	)

	(:action move-right-from-r254
		:precondition
			(and 
				(in r254)
			)
		:effect
			(and
				(not (search_again))
				(not (in r254))
				(in r255)
			)
	)


	(:action move-left-from-r254
		:precondition
			(and 
				(in r254)
			)
		:effect
			(and
				(not (search_again))
				(not (in r254))
				(in r253)
			)
	)

	(:action move-right-from-r255
		:precondition
			(and 
				(in r255)
			)
		:effect
			(and
				(not (search_again))
				(not (in r255))
				(in r256)
			)
	)


	(:action move-left-from-r255
		:precondition
			(and 
				(in r255)
			)
		:effect
			(and
				(not (search_again))
				(not (in r255))
				(in r254)
			)
	)

	(:action move-right-from-r256
		:precondition
			(and 
				(in r256)
			)
		:effect
			(and
				(not (search_again))
				(not (in r256))
				(in r257)
			)
	)


	(:action move-left-from-r256
		:precondition
			(and 
				(in r256)
			)
		:effect
			(and
				(not (search_again))
				(not (in r256))
				(in r255)
			)
	)

	(:action move-right-from-r257
		:precondition
			(and 
				(in r257)
			)
		:effect
			(and
				(not (search_again))
				(not (in r257))
				(in r258)
			)
	)


	(:action move-left-from-r257
		:precondition
			(and 
				(in r257)
			)
		:effect
			(and
				(not (search_again))
				(not (in r257))
				(in r256)
			)
	)

	(:action move-right-from-r258
		:precondition
			(and 
				(in r258)
			)
		:effect
			(and
				(not (search_again))
				(not (in r258))
				(in r259)
			)
	)


	(:action move-left-from-r258
		:precondition
			(and 
				(in r258)
			)
		:effect
			(and
				(not (search_again))
				(not (in r258))
				(in r257)
			)
	)

	(:action move-right-from-r259
		:precondition
			(and 
				(in r259)
			)
		:effect
			(and
				(not (search_again))
				(not (in r259))
				(in r260)
			)
	)


	(:action move-left-from-r259
		:precondition
			(and 
				(in r259)
			)
		:effect
			(and
				(not (search_again))
				(not (in r259))
				(in r258)
			)
	)

	(:action move-right-from-r260
		:precondition
			(and 
				(in r260)
			)
		:effect
			(and
				(not (search_again))
				(not (in r260))
				(in r261)
			)
	)


	(:action move-left-from-r260
		:precondition
			(and 
				(in r260)
			)
		:effect
			(and
				(not (search_again))
				(not (in r260))
				(in r259)
			)
	)

	(:action move-right-from-r261
		:precondition
			(and 
				(in r261)
			)
		:effect
			(and
				(not (search_again))
				(not (in r261))
				(in r262)
			)
	)


	(:action move-left-from-r261
		:precondition
			(and 
				(in r261)
			)
		:effect
			(and
				(not (search_again))
				(not (in r261))
				(in r260)
			)
	)

	(:action move-right-from-r262
		:precondition
			(and 
				(in r262)
			)
		:effect
			(and
				(not (search_again))
				(not (in r262))
				(in r263)
			)
	)


	(:action move-left-from-r262
		:precondition
			(and 
				(in r262)
			)
		:effect
			(and
				(not (search_again))
				(not (in r262))
				(in r261)
			)
	)

	(:action move-right-from-r263
		:precondition
			(and 
				(in r263)
			)
		:effect
			(and
				(not (search_again))
				(not (in r263))
				(in r264)
			)
	)


	(:action move-left-from-r263
		:precondition
			(and 
				(in r263)
			)
		:effect
			(and
				(not (search_again))
				(not (in r263))
				(in r262)
			)
	)

	(:action move-right-from-r264
		:precondition
			(and 
				(in r264)
			)
		:effect
			(and
				(not (search_again))
				(not (in r264))
				(in r265)
			)
	)


	(:action move-left-from-r264
		:precondition
			(and 
				(in r264)
			)
		:effect
			(and
				(not (search_again))
				(not (in r264))
				(in r263)
			)
	)

	(:action move-right-from-r265
		:precondition
			(and 
				(in r265)
			)
		:effect
			(and
				(not (search_again))
				(not (in r265))
				(in r266)
			)
	)


	(:action move-left-from-r265
		:precondition
			(and 
				(in r265)
			)
		:effect
			(and
				(not (search_again))
				(not (in r265))
				(in r264)
			)
	)

	(:action move-right-from-r266
		:precondition
			(and 
				(in r266)
			)
		:effect
			(and
				(not (search_again))
				(not (in r266))
				(in r267)
			)
	)


	(:action move-left-from-r266
		:precondition
			(and 
				(in r266)
			)
		:effect
			(and
				(not (search_again))
				(not (in r266))
				(in r265)
			)
	)

	(:action move-right-from-r267
		:precondition
			(and 
				(in r267)
			)
		:effect
			(and
				(not (search_again))
				(not (in r267))
				(in r268)
			)
	)


	(:action move-left-from-r267
		:precondition
			(and 
				(in r267)
			)
		:effect
			(and
				(not (search_again))
				(not (in r267))
				(in r266)
			)
	)

	(:action move-right-from-r268
		:precondition
			(and 
				(in r268)
			)
		:effect
			(and
				(not (search_again))
				(not (in r268))
				(in r269)
			)
	)


	(:action move-left-from-r268
		:precondition
			(and 
				(in r268)
			)
		:effect
			(and
				(not (search_again))
				(not (in r268))
				(in r267)
			)
	)

	(:action move-right-from-r269
		:precondition
			(and 
				(in r269)
			)
		:effect
			(and
				(not (search_again))
				(not (in r269))
				(in r270)
			)
	)


	(:action move-left-from-r269
		:precondition
			(and 
				(in r269)
			)
		:effect
			(and
				(not (search_again))
				(not (in r269))
				(in r268)
			)
	)

	(:action move-right-from-r270
		:precondition
			(and 
				(in r270)
			)
		:effect
			(and
				(not (search_again))
				(not (in r270))
				(in r271)
			)
	)


	(:action move-left-from-r270
		:precondition
			(and 
				(in r270)
			)
		:effect
			(and
				(not (search_again))
				(not (in r270))
				(in r269)
			)
	)

	(:action move-right-from-r271
		:precondition
			(and 
				(in r271)
			)
		:effect
			(and
				(not (search_again))
				(not (in r271))
				(in r272)
			)
	)


	(:action move-left-from-r271
		:precondition
			(and 
				(in r271)
			)
		:effect
			(and
				(not (search_again))
				(not (in r271))
				(in r270)
			)
	)

	(:action move-right-from-r272
		:precondition
			(and 
				(in r272)
			)
		:effect
			(and
				(not (search_again))
				(not (in r272))
				(in r273)
			)
	)


	(:action move-left-from-r272
		:precondition
			(and 
				(in r272)
			)
		:effect
			(and
				(not (search_again))
				(not (in r272))
				(in r271)
			)
	)

	(:action move-right-from-r273
		:precondition
			(and 
				(in r273)
			)
		:effect
			(and
				(not (search_again))
				(not (in r273))
				(in r274)
			)
	)


	(:action move-left-from-r273
		:precondition
			(and 
				(in r273)
			)
		:effect
			(and
				(not (search_again))
				(not (in r273))
				(in r272)
			)
	)

	(:action move-right-from-r274
		:precondition
			(and 
				(in r274)
			)
		:effect
			(and
				(not (search_again))
				(not (in r274))
				(in r275)
			)
	)


	(:action move-left-from-r274
		:precondition
			(and 
				(in r274)
			)
		:effect
			(and
				(not (search_again))
				(not (in r274))
				(in r273)
			)
	)

	(:action move-right-from-r275
		:precondition
			(and 
				(in r275)
			)
		:effect
			(and
				(not (search_again))
				(not (in r275))
				(in r276)
			)
	)


	(:action move-left-from-r275
		:precondition
			(and 
				(in r275)
			)
		:effect
			(and
				(not (search_again))
				(not (in r275))
				(in r274)
			)
	)

	(:action move-right-from-r276
		:precondition
			(and 
				(in r276)
			)
		:effect
			(and
				(not (search_again))
				(not (in r276))
				(in r277)
			)
	)


	(:action move-left-from-r276
		:precondition
			(and 
				(in r276)
			)
		:effect
			(and
				(not (search_again))
				(not (in r276))
				(in r275)
			)
	)

	(:action move-right-from-r277
		:precondition
			(and 
				(in r277)
			)
		:effect
			(and
				(not (search_again))
				(not (in r277))
				(in r278)
			)
	)


	(:action move-left-from-r277
		:precondition
			(and 
				(in r277)
			)
		:effect
			(and
				(not (search_again))
				(not (in r277))
				(in r276)
			)
	)

	(:action move-right-from-r278
		:precondition
			(and 
				(in r278)
			)
		:effect
			(and
				(not (search_again))
				(not (in r278))
				(in r279)
			)
	)


	(:action move-left-from-r278
		:precondition
			(and 
				(in r278)
			)
		:effect
			(and
				(not (search_again))
				(not (in r278))
				(in r277)
			)
	)

	(:action move-right-from-r279
		:precondition
			(and 
				(in r279)
			)
		:effect
			(and
				(not (search_again))
				(not (in r279))
				(in r280)
			)
	)


	(:action move-left-from-r279
		:precondition
			(and 
				(in r279)
			)
		:effect
			(and
				(not (search_again))
				(not (in r279))
				(in r278)
			)
	)

	(:action move-right-from-r280
		:precondition
			(and 
				(in r280)
			)
		:effect
			(and
				(not (search_again))
				(not (in r280))
				(in r281)
			)
	)


	(:action move-left-from-r280
		:precondition
			(and 
				(in r280)
			)
		:effect
			(and
				(not (search_again))
				(not (in r280))
				(in r279)
			)
	)

	(:action move-right-from-r281
		:precondition
			(and 
				(in r281)
			)
		:effect
			(and
				(not (search_again))
				(not (in r281))
				(in r282)
			)
	)


	(:action move-left-from-r281
		:precondition
			(and 
				(in r281)
			)
		:effect
			(and
				(not (search_again))
				(not (in r281))
				(in r280)
			)
	)

	(:action move-right-from-r282
		:precondition
			(and 
				(in r282)
			)
		:effect
			(and
				(not (search_again))
				(not (in r282))
				(in r283)
			)
	)


	(:action move-left-from-r282
		:precondition
			(and 
				(in r282)
			)
		:effect
			(and
				(not (search_again))
				(not (in r282))
				(in r281)
			)
	)

	(:action move-right-from-r283
		:precondition
			(and 
				(in r283)
			)
		:effect
			(and
				(not (search_again))
				(not (in r283))
				(in r284)
			)
	)


	(:action move-left-from-r283
		:precondition
			(and 
				(in r283)
			)
		:effect
			(and
				(not (search_again))
				(not (in r283))
				(in r282)
			)
	)

	(:action move-right-from-r284
		:precondition
			(and 
				(in r284)
			)
		:effect
			(and
				(not (search_again))
				(not (in r284))
				(in r285)
			)
	)


	(:action move-left-from-r284
		:precondition
			(and 
				(in r284)
			)
		:effect
			(and
				(not (search_again))
				(not (in r284))
				(in r283)
			)
	)

	(:action move-right-from-r285
		:precondition
			(and 
				(in r285)
			)
		:effect
			(and
				(not (search_again))
				(not (in r285))
				(in r286)
			)
	)


	(:action move-left-from-r285
		:precondition
			(and 
				(in r285)
			)
		:effect
			(and
				(not (search_again))
				(not (in r285))
				(in r284)
			)
	)

	(:action move-right-from-r286
		:precondition
			(and 
				(in r286)
			)
		:effect
			(and
				(not (search_again))
				(not (in r286))
				(in r287)
			)
	)


	(:action move-left-from-r286
		:precondition
			(and 
				(in r286)
			)
		:effect
			(and
				(not (search_again))
				(not (in r286))
				(in r285)
			)
	)

	(:action move-right-from-r287
		:precondition
			(and 
				(in r287)
			)
		:effect
			(and
				(not (search_again))
				(not (in r287))
				(in r288)
			)
	)


	(:action move-left-from-r287
		:precondition
			(and 
				(in r287)
			)
		:effect
			(and
				(not (search_again))
				(not (in r287))
				(in r286)
			)
	)

	(:action move-right-from-r288
		:precondition
			(and 
				(in r288)
			)
		:effect
			(and
				(not (search_again))
				(not (in r288))
				(in r289)
			)
	)


	(:action move-left-from-r288
		:precondition
			(and 
				(in r288)
			)
		:effect
			(and
				(not (search_again))
				(not (in r288))
				(in r287)
			)
	)

	(:action move-right-from-r289
		:precondition
			(and 
				(in r289)
			)
		:effect
			(and
				(not (search_again))
				(not (in r289))
				(in r290)
			)
	)


	(:action move-left-from-r289
		:precondition
			(and 
				(in r289)
			)
		:effect
			(and
				(not (search_again))
				(not (in r289))
				(in r288)
			)
	)

	(:action move-right-from-r290
		:precondition
			(and 
				(in r290)
			)
		:effect
			(and
				(not (search_again))
				(not (in r290))
				(in r291)
			)
	)


	(:action move-left-from-r290
		:precondition
			(and 
				(in r290)
			)
		:effect
			(and
				(not (search_again))
				(not (in r290))
				(in r289)
			)
	)

	(:action move-right-from-r291
		:precondition
			(and 
				(in r291)
			)
		:effect
			(and
				(not (search_again))
				(not (in r291))
				(in r292)
			)
	)


	(:action move-left-from-r291
		:precondition
			(and 
				(in r291)
			)
		:effect
			(and
				(not (search_again))
				(not (in r291))
				(in r290)
			)
	)

	(:action move-right-from-r292
		:precondition
			(and 
				(in r292)
			)
		:effect
			(and
				(not (search_again))
				(not (in r292))
				(in r293)
			)
	)


	(:action move-left-from-r292
		:precondition
			(and 
				(in r292)
			)
		:effect
			(and
				(not (search_again))
				(not (in r292))
				(in r291)
			)
	)

	(:action move-right-from-r293
		:precondition
			(and 
				(in r293)
			)
		:effect
			(and
				(not (search_again))
				(not (in r293))
				(in r294)
			)
	)


	(:action move-left-from-r293
		:precondition
			(and 
				(in r293)
			)
		:effect
			(and
				(not (search_again))
				(not (in r293))
				(in r292)
			)
	)

	(:action move-right-from-r294
		:precondition
			(and 
				(in r294)
			)
		:effect
			(and
				(not (search_again))
				(not (in r294))
				(in r295)
			)
	)


	(:action move-left-from-r294
		:precondition
			(and 
				(in r294)
			)
		:effect
			(and
				(not (search_again))
				(not (in r294))
				(in r293)
			)
	)

	(:action move-right-from-r295
		:precondition
			(and 
				(in r295)
			)
		:effect
			(and
				(not (search_again))
				(not (in r295))
				(in r296)
			)
	)


	(:action move-left-from-r295
		:precondition
			(and 
				(in r295)
			)
		:effect
			(and
				(not (search_again))
				(not (in r295))
				(in r294)
			)
	)

	(:action move-right-from-r296
		:precondition
			(and 
				(in r296)
			)
		:effect
			(and
				(not (search_again))
				(not (in r296))
				(in r297)
			)
	)


	(:action move-left-from-r296
		:precondition
			(and 
				(in r296)
			)
		:effect
			(and
				(not (search_again))
				(not (in r296))
				(in r295)
			)
	)

	(:action move-right-from-r297
		:precondition
			(and 
				(in r297)
			)
		:effect
			(and
				(not (search_again))
				(not (in r297))
				(in r298)
			)
	)


	(:action move-left-from-r297
		:precondition
			(and 
				(in r297)
			)
		:effect
			(and
				(not (search_again))
				(not (in r297))
				(in r296)
			)
	)

	(:action move-right-from-r298
		:precondition
			(and 
				(in r298)
			)
		:effect
			(and
				(not (search_again))
				(not (in r298))
				(in r299)
			)
	)


	(:action move-left-from-r298
		:precondition
			(and 
				(in r298)
			)
		:effect
			(and
				(not (search_again))
				(not (in r298))
				(in r297)
			)
	)

	(:action move-right-from-r299
		:precondition
			(and 
				(in r299)
			)
		:effect
			(and
				(not (search_again))
				(not (in r299))
				(in r300)
			)
	)


	(:action move-left-from-r299
		:precondition
			(and 
				(in r299)
			)
		:effect
			(and
				(not (search_again))
				(not (in r299))
				(in r298)
			)
	)

	(:action move-right-from-r300
		:precondition
			(and 
				(in r300)
			)
		:effect
			(and
				(not (search_again))
				(not (in r300))
				(in r301)
			)
	)


	(:action move-left-from-r300
		:precondition
			(and 
				(in r300)
			)
		:effect
			(and
				(not (search_again))
				(not (in r300))
				(in r299)
			)
	)

	(:action move-right-from-r301
		:precondition
			(and 
				(in r301)
			)
		:effect
			(and
				(not (search_again))
				(not (in r301))
				(in r302)
			)
	)


	(:action move-left-from-r301
		:precondition
			(and 
				(in r301)
			)
		:effect
			(and
				(not (search_again))
				(not (in r301))
				(in r300)
			)
	)

	(:action move-right-from-r302
		:precondition
			(and 
				(in r302)
			)
		:effect
			(and
				(not (search_again))
				(not (in r302))
				(in r303)
			)
	)


	(:action move-left-from-r302
		:precondition
			(and 
				(in r302)
			)
		:effect
			(and
				(not (search_again))
				(not (in r302))
				(in r301)
			)
	)

	(:action move-right-from-r303
		:precondition
			(and 
				(in r303)
			)
		:effect
			(and
				(not (search_again))
				(not (in r303))
				(in r304)
			)
	)


	(:action move-left-from-r303
		:precondition
			(and 
				(in r303)
			)
		:effect
			(and
				(not (search_again))
				(not (in r303))
				(in r302)
			)
	)

	(:action move-right-from-r304
		:precondition
			(and 
				(in r304)
			)
		:effect
			(and
				(not (search_again))
				(not (in r304))
				(in r305)
			)
	)


	(:action move-left-from-r304
		:precondition
			(and 
				(in r304)
			)
		:effect
			(and
				(not (search_again))
				(not (in r304))
				(in r303)
			)
	)

	(:action move-right-from-r305
		:precondition
			(and 
				(in r305)
			)
		:effect
			(and
				(not (search_again))
				(not (in r305))
				(in r306)
			)
	)


	(:action move-left-from-r305
		:precondition
			(and 
				(in r305)
			)
		:effect
			(and
				(not (search_again))
				(not (in r305))
				(in r304)
			)
	)

	(:action move-right-from-r306
		:precondition
			(and 
				(in r306)
			)
		:effect
			(and
				(not (search_again))
				(not (in r306))
				(in r307)
			)
	)


	(:action move-left-from-r306
		:precondition
			(and 
				(in r306)
			)
		:effect
			(and
				(not (search_again))
				(not (in r306))
				(in r305)
			)
	)

	(:action move-right-from-r307
		:precondition
			(and 
				(in r307)
			)
		:effect
			(and
				(not (search_again))
				(not (in r307))
				(in r308)
			)
	)


	(:action move-left-from-r307
		:precondition
			(and 
				(in r307)
			)
		:effect
			(and
				(not (search_again))
				(not (in r307))
				(in r306)
			)
	)

	(:action move-right-from-r308
		:precondition
			(and 
				(in r308)
			)
		:effect
			(and
				(not (search_again))
				(not (in r308))
				(in r309)
			)
	)


	(:action move-left-from-r308
		:precondition
			(and 
				(in r308)
			)
		:effect
			(and
				(not (search_again))
				(not (in r308))
				(in r307)
			)
	)

	(:action move-right-from-r309
		:precondition
			(and 
				(in r309)
			)
		:effect
			(and
				(not (search_again))
				(not (in r309))
				(in r310)
			)
	)


	(:action move-left-from-r309
		:precondition
			(and 
				(in r309)
			)
		:effect
			(and
				(not (search_again))
				(not (in r309))
				(in r308)
			)
	)

	(:action move-right-from-r310
		:precondition
			(and 
				(in r310)
			)
		:effect
			(and
				(not (search_again))
				(not (in r310))
				(in r311)
			)
	)


	(:action move-left-from-r310
		:precondition
			(and 
				(in r310)
			)
		:effect
			(and
				(not (search_again))
				(not (in r310))
				(in r309)
			)
	)

	(:action move-right-from-r311
		:precondition
			(and 
				(in r311)
			)
		:effect
			(and
				(not (search_again))
				(not (in r311))
				(in r312)
			)
	)


	(:action move-left-from-r311
		:precondition
			(and 
				(in r311)
			)
		:effect
			(and
				(not (search_again))
				(not (in r311))
				(in r310)
			)
	)

	(:action move-right-from-r312
		:precondition
			(and 
				(in r312)
			)
		:effect
			(and
				(not (search_again))
				(not (in r312))
				(in r313)
			)
	)


	(:action move-left-from-r312
		:precondition
			(and 
				(in r312)
			)
		:effect
			(and
				(not (search_again))
				(not (in r312))
				(in r311)
			)
	)

	(:action move-right-from-r313
		:precondition
			(and 
				(in r313)
			)
		:effect
			(and
				(not (search_again))
				(not (in r313))
				(in r314)
			)
	)


	(:action move-left-from-r313
		:precondition
			(and 
				(in r313)
			)
		:effect
			(and
				(not (search_again))
				(not (in r313))
				(in r312)
			)
	)

	(:action move-right-from-r314
		:precondition
			(and 
				(in r314)
			)
		:effect
			(and
				(not (search_again))
				(not (in r314))
				(in r315)
			)
	)


	(:action move-left-from-r314
		:precondition
			(and 
				(in r314)
			)
		:effect
			(and
				(not (search_again))
				(not (in r314))
				(in r313)
			)
	)

	(:action move-right-from-r315
		:precondition
			(and 
				(in r315)
			)
		:effect
			(and
				(not (search_again))
				(not (in r315))
				(in r316)
			)
	)


	(:action move-left-from-r315
		:precondition
			(and 
				(in r315)
			)
		:effect
			(and
				(not (search_again))
				(not (in r315))
				(in r314)
			)
	)

	(:action move-right-from-r316
		:precondition
			(and 
				(in r316)
			)
		:effect
			(and
				(not (search_again))
				(not (in r316))
				(in r317)
			)
	)


	(:action move-left-from-r316
		:precondition
			(and 
				(in r316)
			)
		:effect
			(and
				(not (search_again))
				(not (in r316))
				(in r315)
			)
	)

	(:action move-right-from-r317
		:precondition
			(and 
				(in r317)
			)
		:effect
			(and
				(not (search_again))
				(not (in r317))
				(in r318)
			)
	)


	(:action move-left-from-r317
		:precondition
			(and 
				(in r317)
			)
		:effect
			(and
				(not (search_again))
				(not (in r317))
				(in r316)
			)
	)

	(:action move-right-from-r318
		:precondition
			(and 
				(in r318)
			)
		:effect
			(and
				(not (search_again))
				(not (in r318))
				(in r319)
			)
	)


	(:action move-left-from-r318
		:precondition
			(and 
				(in r318)
			)
		:effect
			(and
				(not (search_again))
				(not (in r318))
				(in r317)
			)
	)

	(:action move-right-from-r319
		:precondition
			(and 
				(in r319)
			)
		:effect
			(and
				(not (search_again))
				(not (in r319))
				(in r320)
			)
	)


	(:action move-left-from-r319
		:precondition
			(and 
				(in r319)
			)
		:effect
			(and
				(not (search_again))
				(not (in r319))
				(in r318)
			)
	)

	(:action move-right-from-r320
		:precondition
			(and 
				(in r320)
			)
		:effect
			(and
				(not (search_again))
				(not (in r320))
				(in r321)
			)
	)


	(:action move-left-from-r320
		:precondition
			(and 
				(in r320)
			)
		:effect
			(and
				(not (search_again))
				(not (in r320))
				(in r319)
			)
	)

	(:action move-right-from-r321
		:precondition
			(and 
				(in r321)
			)
		:effect
			(and
				(not (search_again))
				(not (in r321))
				(in r322)
			)
	)


	(:action move-left-from-r321
		:precondition
			(and 
				(in r321)
			)
		:effect
			(and
				(not (search_again))
				(not (in r321))
				(in r320)
			)
	)

	(:action move-right-from-r322
		:precondition
			(and 
				(in r322)
			)
		:effect
			(and
				(not (search_again))
				(not (in r322))
				(in r323)
			)
	)


	(:action move-left-from-r322
		:precondition
			(and 
				(in r322)
			)
		:effect
			(and
				(not (search_again))
				(not (in r322))
				(in r321)
			)
	)

	(:action move-right-from-r323
		:precondition
			(and 
				(in r323)
			)
		:effect
			(and
				(not (search_again))
				(not (in r323))
				(in r324)
			)
	)


	(:action move-left-from-r323
		:precondition
			(and 
				(in r323)
			)
		:effect
			(and
				(not (search_again))
				(not (in r323))
				(in r322)
			)
	)

	(:action move-right-from-r324
		:precondition
			(and 
				(in r324)
			)
		:effect
			(and
				(not (search_again))
				(not (in r324))
				(in r325)
			)
	)


	(:action move-left-from-r324
		:precondition
			(and 
				(in r324)
			)
		:effect
			(and
				(not (search_again))
				(not (in r324))
				(in r323)
			)
	)

	(:action move-right-from-r325
		:precondition
			(and 
				(in r325)
			)
		:effect
			(and
				(not (search_again))
				(not (in r325))
				(in r326)
			)
	)


	(:action move-left-from-r325
		:precondition
			(and 
				(in r325)
			)
		:effect
			(and
				(not (search_again))
				(not (in r325))
				(in r324)
			)
	)

	(:action move-right-from-r326
		:precondition
			(and 
				(in r326)
			)
		:effect
			(and
				(not (search_again))
				(not (in r326))
				(in r327)
			)
	)


	(:action move-left-from-r326
		:precondition
			(and 
				(in r326)
			)
		:effect
			(and
				(not (search_again))
				(not (in r326))
				(in r325)
			)
	)

	(:action move-right-from-r327
		:precondition
			(and 
				(in r327)
			)
		:effect
			(and
				(not (search_again))
				(not (in r327))
				(in r328)
			)
	)


	(:action move-left-from-r327
		:precondition
			(and 
				(in r327)
			)
		:effect
			(and
				(not (search_again))
				(not (in r327))
				(in r326)
			)
	)

	(:action move-right-from-r328
		:precondition
			(and 
				(in r328)
			)
		:effect
			(and
				(not (search_again))
				(not (in r328))
				(in r329)
			)
	)


	(:action move-left-from-r328
		:precondition
			(and 
				(in r328)
			)
		:effect
			(and
				(not (search_again))
				(not (in r328))
				(in r327)
			)
	)

	(:action move-right-from-r329
		:precondition
			(and 
				(in r329)
			)
		:effect
			(and
				(not (search_again))
				(not (in r329))
				(in r330)
			)
	)


	(:action move-left-from-r329
		:precondition
			(and 
				(in r329)
			)
		:effect
			(and
				(not (search_again))
				(not (in r329))
				(in r328)
			)
	)

	(:action move-right-from-r330
		:precondition
			(and 
				(in r330)
			)
		:effect
			(and
				(not (search_again))
				(not (in r330))
				(in r331)
			)
	)


	(:action move-left-from-r330
		:precondition
			(and 
				(in r330)
			)
		:effect
			(and
				(not (search_again))
				(not (in r330))
				(in r329)
			)
	)

	(:action move-right-from-r331
		:precondition
			(and 
				(in r331)
			)
		:effect
			(and
				(not (search_again))
				(not (in r331))
				(in r332)
			)
	)


	(:action move-left-from-r331
		:precondition
			(and 
				(in r331)
			)
		:effect
			(and
				(not (search_again))
				(not (in r331))
				(in r330)
			)
	)

	(:action move-right-from-r332
		:precondition
			(and 
				(in r332)
			)
		:effect
			(and
				(not (search_again))
				(not (in r332))
				(in r333)
			)
	)


	(:action move-left-from-r332
		:precondition
			(and 
				(in r332)
			)
		:effect
			(and
				(not (search_again))
				(not (in r332))
				(in r331)
			)
	)

	(:action move-right-from-r333
		:precondition
			(and 
				(in r333)
			)
		:effect
			(and
				(not (search_again))
				(not (in r333))
				(in r334)
			)
	)


	(:action move-left-from-r333
		:precondition
			(and 
				(in r333)
			)
		:effect
			(and
				(not (search_again))
				(not (in r333))
				(in r332)
			)
	)

	(:action move-right-from-r334
		:precondition
			(and 
				(in r334)
			)
		:effect
			(and
				(not (search_again))
				(not (in r334))
				(in r335)
			)
	)


	(:action move-left-from-r334
		:precondition
			(and 
				(in r334)
			)
		:effect
			(and
				(not (search_again))
				(not (in r334))
				(in r333)
			)
	)

	(:action move-right-from-r335
		:precondition
			(and 
				(in r335)
			)
		:effect
			(and
				(not (search_again))
				(not (in r335))
				(in r336)
			)
	)


	(:action move-left-from-r335
		:precondition
			(and 
				(in r335)
			)
		:effect
			(and
				(not (search_again))
				(not (in r335))
				(in r334)
			)
	)

	(:action move-right-from-r336
		:precondition
			(and 
				(in r336)
			)
		:effect
			(and
				(not (search_again))
				(not (in r336))
				(in r337)
			)
	)


	(:action move-left-from-r336
		:precondition
			(and 
				(in r336)
			)
		:effect
			(and
				(not (search_again))
				(not (in r336))
				(in r335)
			)
	)

	(:action move-right-from-r337
		:precondition
			(and 
				(in r337)
			)
		:effect
			(and
				(not (search_again))
				(not (in r337))
				(in r338)
			)
	)


	(:action move-left-from-r337
		:precondition
			(and 
				(in r337)
			)
		:effect
			(and
				(not (search_again))
				(not (in r337))
				(in r336)
			)
	)

	(:action move-right-from-r338
		:precondition
			(and 
				(in r338)
			)
		:effect
			(and
				(not (search_again))
				(not (in r338))
				(in r339)
			)
	)


	(:action move-left-from-r338
		:precondition
			(and 
				(in r338)
			)
		:effect
			(and
				(not (search_again))
				(not (in r338))
				(in r337)
			)
	)

	(:action move-right-from-r339
		:precondition
			(and 
				(in r339)
			)
		:effect
			(and
				(not (search_again))
				(not (in r339))
				(in r340)
			)
	)


	(:action move-left-from-r339
		:precondition
			(and 
				(in r339)
			)
		:effect
			(and
				(not (search_again))
				(not (in r339))
				(in r338)
			)
	)

	(:action move-right-from-r340
		:precondition
			(and 
				(in r340)
			)
		:effect
			(and
				(not (search_again))
				(not (in r340))
				(in r341)
			)
	)


	(:action move-left-from-r340
		:precondition
			(and 
				(in r340)
			)
		:effect
			(and
				(not (search_again))
				(not (in r340))
				(in r339)
			)
	)

	(:action move-right-from-r341
		:precondition
			(and 
				(in r341)
			)
		:effect
			(and
				(not (search_again))
				(not (in r341))
				(in r342)
			)
	)


	(:action move-left-from-r341
		:precondition
			(and 
				(in r341)
			)
		:effect
			(and
				(not (search_again))
				(not (in r341))
				(in r340)
			)
	)

	(:action move-right-from-r342
		:precondition
			(and 
				(in r342)
			)
		:effect
			(and
				(not (search_again))
				(not (in r342))
				(in r343)
			)
	)


	(:action move-left-from-r342
		:precondition
			(and 
				(in r342)
			)
		:effect
			(and
				(not (search_again))
				(not (in r342))
				(in r341)
			)
	)

	(:action move-right-from-r343
		:precondition
			(and 
				(in r343)
			)
		:effect
			(and
				(not (search_again))
				(not (in r343))
				(in r344)
			)
	)


	(:action move-left-from-r343
		:precondition
			(and 
				(in r343)
			)
		:effect
			(and
				(not (search_again))
				(not (in r343))
				(in r342)
			)
	)

	(:action move-right-from-r344
		:precondition
			(and 
				(in r344)
			)
		:effect
			(and
				(not (search_again))
				(not (in r344))
				(in r345)
			)
	)


	(:action move-left-from-r344
		:precondition
			(and 
				(in r344)
			)
		:effect
			(and
				(not (search_again))
				(not (in r344))
				(in r343)
			)
	)

	(:action move-right-from-r345
		:precondition
			(and 
				(in r345)
			)
		:effect
			(and
				(not (search_again))
				(not (in r345))
				(in r346)
			)
	)


	(:action move-left-from-r345
		:precondition
			(and 
				(in r345)
			)
		:effect
			(and
				(not (search_again))
				(not (in r345))
				(in r344)
			)
	)

	(:action move-right-from-r346
		:precondition
			(and 
				(in r346)
			)
		:effect
			(and
				(not (search_again))
				(not (in r346))
				(in r347)
			)
	)


	(:action move-left-from-r346
		:precondition
			(and 
				(in r346)
			)
		:effect
			(and
				(not (search_again))
				(not (in r346))
				(in r345)
			)
	)

	(:action move-right-from-r347
		:precondition
			(and 
				(in r347)
			)
		:effect
			(and
				(not (search_again))
				(not (in r347))
				(in r348)
			)
	)


	(:action move-left-from-r347
		:precondition
			(and 
				(in r347)
			)
		:effect
			(and
				(not (search_again))
				(not (in r347))
				(in r346)
			)
	)

	(:action move-right-from-r348
		:precondition
			(and 
				(in r348)
			)
		:effect
			(and
				(not (search_again))
				(not (in r348))
				(in r349)
			)
	)


	(:action move-left-from-r348
		:precondition
			(and 
				(in r348)
			)
		:effect
			(and
				(not (search_again))
				(not (in r348))
				(in r347)
			)
	)

	(:action move-right-from-r349
		:precondition
			(and 
				(in r349)
			)
		:effect
			(and
				(not (search_again))
				(not (in r349))
				(in r350)
			)
	)


	(:action move-left-from-r349
		:precondition
			(and 
				(in r349)
			)
		:effect
			(and
				(not (search_again))
				(not (in r349))
				(in r348)
			)
	)

	(:action move-right-from-r350
		:precondition
			(and 
				(in r350)
			)
		:effect
			(and
				(not (search_again))
				(not (in r350))
				(in r351)
			)
	)


	(:action move-left-from-r350
		:precondition
			(and 
				(in r350)
			)
		:effect
			(and
				(not (search_again))
				(not (in r350))
				(in r349)
			)
	)

	(:action move-right-from-r351
		:precondition
			(and 
				(in r351)
			)
		:effect
			(and
				(not (search_again))
				(not (in r351))
				(in r352)
			)
	)


	(:action move-left-from-r351
		:precondition
			(and 
				(in r351)
			)
		:effect
			(and
				(not (search_again))
				(not (in r351))
				(in r350)
			)
	)

	(:action move-right-from-r352
		:precondition
			(and 
				(in r352)
			)
		:effect
			(and
				(not (search_again))
				(not (in r352))
				(in r353)
			)
	)


	(:action move-left-from-r352
		:precondition
			(and 
				(in r352)
			)
		:effect
			(and
				(not (search_again))
				(not (in r352))
				(in r351)
			)
	)

	(:action move-right-from-r353
		:precondition
			(and 
				(in r353)
			)
		:effect
			(and
				(not (search_again))
				(not (in r353))
				(in r354)
			)
	)


	(:action move-left-from-r353
		:precondition
			(and 
				(in r353)
			)
		:effect
			(and
				(not (search_again))
				(not (in r353))
				(in r352)
			)
	)

	(:action move-right-from-r354
		:precondition
			(and 
				(in r354)
			)
		:effect
			(and
				(not (search_again))
				(not (in r354))
				(in r355)
			)
	)


	(:action move-left-from-r354
		:precondition
			(and 
				(in r354)
			)
		:effect
			(and
				(not (search_again))
				(not (in r354))
				(in r353)
			)
	)

	(:action move-right-from-r355
		:precondition
			(and 
				(in r355)
			)
		:effect
			(and
				(not (search_again))
				(not (in r355))
				(in r356)
			)
	)


	(:action move-left-from-r355
		:precondition
			(and 
				(in r355)
			)
		:effect
			(and
				(not (search_again))
				(not (in r355))
				(in r354)
			)
	)

	(:action move-right-from-r356
		:precondition
			(and 
				(in r356)
			)
		:effect
			(and
				(not (search_again))
				(not (in r356))
				(in r357)
			)
	)


	(:action move-left-from-r356
		:precondition
			(and 
				(in r356)
			)
		:effect
			(and
				(not (search_again))
				(not (in r356))
				(in r355)
			)
	)

	(:action move-right-from-r357
		:precondition
			(and 
				(in r357)
			)
		:effect
			(and
				(not (search_again))
				(not (in r357))
				(in r358)
			)
	)


	(:action move-left-from-r357
		:precondition
			(and 
				(in r357)
			)
		:effect
			(and
				(not (search_again))
				(not (in r357))
				(in r356)
			)
	)

	(:action move-right-from-r358
		:precondition
			(and 
				(in r358)
			)
		:effect
			(and
				(not (search_again))
				(not (in r358))
				(in r359)
			)
	)


	(:action move-left-from-r358
		:precondition
			(and 
				(in r358)
			)
		:effect
			(and
				(not (search_again))
				(not (in r358))
				(in r357)
			)
	)

	(:action move-right-from-r359
		:precondition
			(and 
				(in r359)
			)
		:effect
			(and
				(not (search_again))
				(not (in r359))
				(in r360)
			)
	)


	(:action move-left-from-r359
		:precondition
			(and 
				(in r359)
			)
		:effect
			(and
				(not (search_again))
				(not (in r359))
				(in r358)
			)
	)

	(:action move-right-from-r360
		:precondition
			(and 
				(in r360)
			)
		:effect
			(and
				(not (search_again))
				(not (in r360))
				(in r361)
			)
	)


	(:action move-left-from-r360
		:precondition
			(and 
				(in r360)
			)
		:effect
			(and
				(not (search_again))
				(not (in r360))
				(in r359)
			)
	)

	(:action move-right-from-r361
		:precondition
			(and 
				(in r361)
			)
		:effect
			(and
				(not (search_again))
				(not (in r361))
				(in r362)
			)
	)


	(:action move-left-from-r361
		:precondition
			(and 
				(in r361)
			)
		:effect
			(and
				(not (search_again))
				(not (in r361))
				(in r360)
			)
	)

	(:action move-right-from-r362
		:precondition
			(and 
				(in r362)
			)
		:effect
			(and
				(not (search_again))
				(not (in r362))
				(in r363)
			)
	)


	(:action move-left-from-r362
		:precondition
			(and 
				(in r362)
			)
		:effect
			(and
				(not (search_again))
				(not (in r362))
				(in r361)
			)
	)

	(:action move-right-from-r363
		:precondition
			(and 
				(in r363)
			)
		:effect
			(and
				(not (search_again))
				(not (in r363))
				(in r364)
			)
	)


	(:action move-left-from-r363
		:precondition
			(and 
				(in r363)
			)
		:effect
			(and
				(not (search_again))
				(not (in r363))
				(in r362)
			)
	)

	(:action move-right-from-r364
		:precondition
			(and 
				(in r364)
			)
		:effect
			(and
				(not (search_again))
				(not (in r364))
				(in r365)
			)
	)


	(:action move-left-from-r364
		:precondition
			(and 
				(in r364)
			)
		:effect
			(and
				(not (search_again))
				(not (in r364))
				(in r363)
			)
	)

	(:action move-right-from-r365
		:precondition
			(and 
				(in r365)
			)
		:effect
			(and
				(not (search_again))
				(not (in r365))
				(in r366)
			)
	)


	(:action move-left-from-r365
		:precondition
			(and 
				(in r365)
			)
		:effect
			(and
				(not (search_again))
				(not (in r365))
				(in r364)
			)
	)

	(:action move-right-from-r366
		:precondition
			(and 
				(in r366)
			)
		:effect
			(and
				(not (search_again))
				(not (in r366))
				(in r367)
			)
	)


	(:action move-left-from-r366
		:precondition
			(and 
				(in r366)
			)
		:effect
			(and
				(not (search_again))
				(not (in r366))
				(in r365)
			)
	)

	(:action move-right-from-r367
		:precondition
			(and 
				(in r367)
			)
		:effect
			(and
				(not (search_again))
				(not (in r367))
				(in r368)
			)
	)


	(:action move-left-from-r367
		:precondition
			(and 
				(in r367)
			)
		:effect
			(and
				(not (search_again))
				(not (in r367))
				(in r366)
			)
	)

	(:action move-right-from-r368
		:precondition
			(and 
				(in r368)
			)
		:effect
			(and
				(not (search_again))
				(not (in r368))
				(in r369)
			)
	)


	(:action move-left-from-r368
		:precondition
			(and 
				(in r368)
			)
		:effect
			(and
				(not (search_again))
				(not (in r368))
				(in r367)
			)
	)

	(:action move-right-from-r369
		:precondition
			(and 
				(in r369)
			)
		:effect
			(and
				(not (search_again))
				(not (in r369))
				(in r370)
			)
	)


	(:action move-left-from-r369
		:precondition
			(and 
				(in r369)
			)
		:effect
			(and
				(not (search_again))
				(not (in r369))
				(in r368)
			)
	)

	(:action move-right-from-r370
		:precondition
			(and 
				(in r370)
			)
		:effect
			(and
				(not (search_again))
				(not (in r370))
				(in r371)
			)
	)


	(:action move-left-from-r370
		:precondition
			(and 
				(in r370)
			)
		:effect
			(and
				(not (search_again))
				(not (in r370))
				(in r369)
			)
	)

	(:action move-right-from-r371
		:precondition
			(and 
				(in r371)
			)
		:effect
			(and
				(not (search_again))
				(not (in r371))
				(in r372)
			)
	)


	(:action move-left-from-r371
		:precondition
			(and 
				(in r371)
			)
		:effect
			(and
				(not (search_again))
				(not (in r371))
				(in r370)
			)
	)

	(:action move-right-from-r372
		:precondition
			(and 
				(in r372)
			)
		:effect
			(and
				(not (search_again))
				(not (in r372))
				(in r373)
			)
	)


	(:action move-left-from-r372
		:precondition
			(and 
				(in r372)
			)
		:effect
			(and
				(not (search_again))
				(not (in r372))
				(in r371)
			)
	)

	(:action move-right-from-r373
		:precondition
			(and 
				(in r373)
			)
		:effect
			(and
				(not (search_again))
				(not (in r373))
				(in r374)
			)
	)


	(:action move-left-from-r373
		:precondition
			(and 
				(in r373)
			)
		:effect
			(and
				(not (search_again))
				(not (in r373))
				(in r372)
			)
	)

	(:action move-right-from-r374
		:precondition
			(and 
				(in r374)
			)
		:effect
			(and
				(not (search_again))
				(not (in r374))
				(in r375)
			)
	)


	(:action move-left-from-r374
		:precondition
			(and 
				(in r374)
			)
		:effect
			(and
				(not (search_again))
				(not (in r374))
				(in r373)
			)
	)

	(:action move-right-from-r375
		:precondition
			(and 
				(in r375)
			)
		:effect
			(and
				(not (search_again))
				(not (in r375))
				(in r376)
			)
	)


	(:action move-left-from-r375
		:precondition
			(and 
				(in r375)
			)
		:effect
			(and
				(not (search_again))
				(not (in r375))
				(in r374)
			)
	)

	(:action move-right-from-r376
		:precondition
			(and 
				(in r376)
			)
		:effect
			(and
				(not (search_again))
				(not (in r376))
				(in r377)
			)
	)


	(:action move-left-from-r376
		:precondition
			(and 
				(in r376)
			)
		:effect
			(and
				(not (search_again))
				(not (in r376))
				(in r375)
			)
	)

	(:action move-right-from-r377
		:precondition
			(and 
				(in r377)
			)
		:effect
			(and
				(not (search_again))
				(not (in r377))
				(in r378)
			)
	)


	(:action move-left-from-r377
		:precondition
			(and 
				(in r377)
			)
		:effect
			(and
				(not (search_again))
				(not (in r377))
				(in r376)
			)
	)

	(:action move-right-from-r378
		:precondition
			(and 
				(in r378)
			)
		:effect
			(and
				(not (search_again))
				(not (in r378))
				(in r379)
			)
	)


	(:action move-left-from-r378
		:precondition
			(and 
				(in r378)
			)
		:effect
			(and
				(not (search_again))
				(not (in r378))
				(in r377)
			)
	)

	(:action move-right-from-r379
		:precondition
			(and 
				(in r379)
			)
		:effect
			(and
				(not (search_again))
				(not (in r379))
				(in r380)
			)
	)


	(:action move-left-from-r379
		:precondition
			(and 
				(in r379)
			)
		:effect
			(and
				(not (search_again))
				(not (in r379))
				(in r378)
			)
	)

	(:action move-right-from-r380
		:precondition
			(and 
				(in r380)
			)
		:effect
			(and
				(not (search_again))
				(not (in r380))
				(in r381)
			)
	)


	(:action move-left-from-r380
		:precondition
			(and 
				(in r380)
			)
		:effect
			(and
				(not (search_again))
				(not (in r380))
				(in r379)
			)
	)

	(:action move-right-from-r381
		:precondition
			(and 
				(in r381)
			)
		:effect
			(and
				(not (search_again))
				(not (in r381))
				(in r382)
			)
	)


	(:action move-left-from-r381
		:precondition
			(and 
				(in r381)
			)
		:effect
			(and
				(not (search_again))
				(not (in r381))
				(in r380)
			)
	)

	(:action move-right-from-r382
		:precondition
			(and 
				(in r382)
			)
		:effect
			(and
				(not (search_again))
				(not (in r382))
				(in r383)
			)
	)


	(:action move-left-from-r382
		:precondition
			(and 
				(in r382)
			)
		:effect
			(and
				(not (search_again))
				(not (in r382))
				(in r381)
			)
	)

	(:action move-right-from-r383
		:precondition
			(and 
				(in r383)
			)
		:effect
			(and
				(not (search_again))
				(not (in r383))
				(in r384)
			)
	)


	(:action move-left-from-r383
		:precondition
			(and 
				(in r383)
			)
		:effect
			(and
				(not (search_again))
				(not (in r383))
				(in r382)
			)
	)

	(:action move-right-from-r384
		:precondition
			(and 
				(in r384)
			)
		:effect
			(and
				(not (search_again))
				(not (in r384))
				(in r385)
			)
	)


	(:action move-left-from-r384
		:precondition
			(and 
				(in r384)
			)
		:effect
			(and
				(not (search_again))
				(not (in r384))
				(in r383)
			)
	)

	(:action move-right-from-r385
		:precondition
			(and 
				(in r385)
			)
		:effect
			(and
				(not (search_again))
				(not (in r385))
				(in r386)
			)
	)


	(:action move-left-from-r385
		:precondition
			(and 
				(in r385)
			)
		:effect
			(and
				(not (search_again))
				(not (in r385))
				(in r384)
			)
	)

	(:action move-right-from-r386
		:precondition
			(and 
				(in r386)
			)
		:effect
			(and
				(not (search_again))
				(not (in r386))
				(in r387)
			)
	)


	(:action move-left-from-r386
		:precondition
			(and 
				(in r386)
			)
		:effect
			(and
				(not (search_again))
				(not (in r386))
				(in r385)
			)
	)

	(:action move-right-from-r387
		:precondition
			(and 
				(in r387)
			)
		:effect
			(and
				(not (search_again))
				(not (in r387))
				(in r388)
			)
	)


	(:action move-left-from-r387
		:precondition
			(and 
				(in r387)
			)
		:effect
			(and
				(not (search_again))
				(not (in r387))
				(in r386)
			)
	)

	(:action move-right-from-r388
		:precondition
			(and 
				(in r388)
			)
		:effect
			(and
				(not (search_again))
				(not (in r388))
				(in r389)
			)
	)


	(:action move-left-from-r388
		:precondition
			(and 
				(in r388)
			)
		:effect
			(and
				(not (search_again))
				(not (in r388))
				(in r387)
			)
	)

	(:action move-right-from-r389
		:precondition
			(and 
				(in r389)
			)
		:effect
			(and
				(not (search_again))
				(not (in r389))
				(in r390)
			)
	)


	(:action move-left-from-r389
		:precondition
			(and 
				(in r389)
			)
		:effect
			(and
				(not (search_again))
				(not (in r389))
				(in r388)
			)
	)

	(:action move-right-from-r390
		:precondition
			(and 
				(in r390)
			)
		:effect
			(and
				(not (search_again))
				(not (in r390))
				(in r391)
			)
	)


	(:action move-left-from-r390
		:precondition
			(and 
				(in r390)
			)
		:effect
			(and
				(not (search_again))
				(not (in r390))
				(in r389)
			)
	)

	(:action move-right-from-r391
		:precondition
			(and 
				(in r391)
			)
		:effect
			(and
				(not (search_again))
				(not (in r391))
				(in r392)
			)
	)


	(:action move-left-from-r391
		:precondition
			(and 
				(in r391)
			)
		:effect
			(and
				(not (search_again))
				(not (in r391))
				(in r390)
			)
	)

	(:action move-right-from-r392
		:precondition
			(and 
				(in r392)
			)
		:effect
			(and
				(not (search_again))
				(not (in r392))
				(in r393)
			)
	)


	(:action move-left-from-r392
		:precondition
			(and 
				(in r392)
			)
		:effect
			(and
				(not (search_again))
				(not (in r392))
				(in r391)
			)
	)

	(:action move-right-from-r393
		:precondition
			(and 
				(in r393)
			)
		:effect
			(and
				(not (search_again))
				(not (in r393))
				(in r394)
			)
	)


	(:action move-left-from-r393
		:precondition
			(and 
				(in r393)
			)
		:effect
			(and
				(not (search_again))
				(not (in r393))
				(in r392)
			)
	)

	(:action move-right-from-r394
		:precondition
			(and 
				(in r394)
			)
		:effect
			(and
				(not (search_again))
				(not (in r394))
				(in r395)
			)
	)


	(:action move-left-from-r394
		:precondition
			(and 
				(in r394)
			)
		:effect
			(and
				(not (search_again))
				(not (in r394))
				(in r393)
			)
	)

	(:action move-right-from-r395
		:precondition
			(and 
				(in r395)
			)
		:effect
			(and
				(not (search_again))
				(not (in r395))
				(in r396)
			)
	)


	(:action move-left-from-r395
		:precondition
			(and 
				(in r395)
			)
		:effect
			(and
				(not (search_again))
				(not (in r395))
				(in r394)
			)
	)

	(:action move-right-from-r396
		:precondition
			(and 
				(in r396)
			)
		:effect
			(and
				(not (search_again))
				(not (in r396))
				(in r397)
			)
	)


	(:action move-left-from-r396
		:precondition
			(and 
				(in r396)
			)
		:effect
			(and
				(not (search_again))
				(not (in r396))
				(in r395)
			)
	)

	(:action move-right-from-r397
		:precondition
			(and 
				(in r397)
			)
		:effect
			(and
				(not (search_again))
				(not (in r397))
				(in r398)
			)
	)


	(:action move-left-from-r397
		:precondition
			(and 
				(in r397)
			)
		:effect
			(and
				(not (search_again))
				(not (in r397))
				(in r396)
			)
	)

	(:action move-right-from-r398
		:precondition
			(and 
				(in r398)
			)
		:effect
			(and
				(not (search_again))
				(not (in r398))
				(in r399)
			)
	)


	(:action move-left-from-r398
		:precondition
			(and 
				(in r398)
			)
		:effect
			(and
				(not (search_again))
				(not (in r398))
				(in r397)
			)
	)

	(:action move-right-from-r399
		:precondition
			(and 
				(in r399)
			)
		:effect
			(and
				(not (search_again))
				(not (in r399))
				(in r400)
			)
	)


	(:action move-left-from-r399
		:precondition
			(and 
				(in r399)
			)
		:effect
			(and
				(not (search_again))
				(not (in r399))
				(in r398)
			)
	)

	(:action move-right-from-r400
		:precondition
			(and 
				(in r400)
			)
		:effect
			(and
				(not (search_again))
				(not (in r400))
				(in r401)
			)
	)


	(:action move-left-from-r400
		:precondition
			(and 
				(in r400)
			)
		:effect
			(and
				(not (search_again))
				(not (in r400))
				(in r399)
			)
	)

	(:action move-right-from-r401
		:precondition
			(and 
				(in r401)
			)
		:effect
			(and
				(not (search_again))
				(not (in r401))
				(in r402)
			)
	)


	(:action move-left-from-r401
		:precondition
			(and 
				(in r401)
			)
		:effect
			(and
				(not (search_again))
				(not (in r401))
				(in r400)
			)
	)

	(:action move-right-from-r402
		:precondition
			(and 
				(in r402)
			)
		:effect
			(and
				(not (search_again))
				(not (in r402))
				(in r403)
			)
	)


	(:action move-left-from-r402
		:precondition
			(and 
				(in r402)
			)
		:effect
			(and
				(not (search_again))
				(not (in r402))
				(in r401)
			)
	)

	(:action move-right-from-r403
		:precondition
			(and 
				(in r403)
			)
		:effect
			(and
				(not (search_again))
				(not (in r403))
				(in r404)
			)
	)


	(:action move-left-from-r403
		:precondition
			(and 
				(in r403)
			)
		:effect
			(and
				(not (search_again))
				(not (in r403))
				(in r402)
			)
	)

	(:action move-right-from-r404
		:precondition
			(and 
				(in r404)
			)
		:effect
			(and
				(not (search_again))
				(not (in r404))
				(in r405)
			)
	)


	(:action move-left-from-r404
		:precondition
			(and 
				(in r404)
			)
		:effect
			(and
				(not (search_again))
				(not (in r404))
				(in r403)
			)
	)

	(:action move-right-from-r405
		:precondition
			(and 
				(in r405)
			)
		:effect
			(and
				(not (search_again))
				(not (in r405))
				(in r406)
			)
	)


	(:action move-left-from-r405
		:precondition
			(and 
				(in r405)
			)
		:effect
			(and
				(not (search_again))
				(not (in r405))
				(in r404)
			)
	)

	(:action move-right-from-r406
		:precondition
			(and 
				(in r406)
			)
		:effect
			(and
				(not (search_again))
				(not (in r406))
				(in r407)
			)
	)


	(:action move-left-from-r406
		:precondition
			(and 
				(in r406)
			)
		:effect
			(and
				(not (search_again))
				(not (in r406))
				(in r405)
			)
	)

	(:action move-right-from-r407
		:precondition
			(and 
				(in r407)
			)
		:effect
			(and
				(not (search_again))
				(not (in r407))
				(in r408)
			)
	)


	(:action move-left-from-r407
		:precondition
			(and 
				(in r407)
			)
		:effect
			(and
				(not (search_again))
				(not (in r407))
				(in r406)
			)
	)

	(:action move-right-from-r408
		:precondition
			(and 
				(in r408)
			)
		:effect
			(and
				(not (search_again))
				(not (in r408))
				(in r409)
			)
	)


	(:action move-left-from-r408
		:precondition
			(and 
				(in r408)
			)
		:effect
			(and
				(not (search_again))
				(not (in r408))
				(in r407)
			)
	)

	(:action move-right-from-r409
		:precondition
			(and 
				(in r409)
			)
		:effect
			(and
				(not (search_again))
				(not (in r409))
				(in r410)
			)
	)


	(:action move-left-from-r409
		:precondition
			(and 
				(in r409)
			)
		:effect
			(and
				(not (search_again))
				(not (in r409))
				(in r408)
			)
	)

	(:action move-right-from-r410
		:precondition
			(and 
				(in r410)
			)
		:effect
			(and
				(not (search_again))
				(not (in r410))
				(in r411)
			)
	)


	(:action move-left-from-r410
		:precondition
			(and 
				(in r410)
			)
		:effect
			(and
				(not (search_again))
				(not (in r410))
				(in r409)
			)
	)

	(:action move-right-from-r411
		:precondition
			(and 
				(in r411)
			)
		:effect
			(and
				(not (search_again))
				(not (in r411))
				(in r412)
			)
	)


	(:action move-left-from-r411
		:precondition
			(and 
				(in r411)
			)
		:effect
			(and
				(not (search_again))
				(not (in r411))
				(in r410)
			)
	)

	(:action move-right-from-r412
		:precondition
			(and 
				(in r412)
			)
		:effect
			(and
				(not (search_again))
				(not (in r412))
				(in r413)
			)
	)


	(:action move-left-from-r412
		:precondition
			(and 
				(in r412)
			)
		:effect
			(and
				(not (search_again))
				(not (in r412))
				(in r411)
			)
	)

	(:action move-right-from-r413
		:precondition
			(and 
				(in r413)
			)
		:effect
			(and
				(not (search_again))
				(not (in r413))
				(in r414)
			)
	)


	(:action move-left-from-r413
		:precondition
			(and 
				(in r413)
			)
		:effect
			(and
				(not (search_again))
				(not (in r413))
				(in r412)
			)
	)

	(:action move-right-from-r414
		:precondition
			(and 
				(in r414)
			)
		:effect
			(and
				(not (search_again))
				(not (in r414))
				(in r415)
			)
	)


	(:action move-left-from-r414
		:precondition
			(and 
				(in r414)
			)
		:effect
			(and
				(not (search_again))
				(not (in r414))
				(in r413)
			)
	)

	(:action move-right-from-r415
		:precondition
			(and 
				(in r415)
			)
		:effect
			(and
				(not (search_again))
				(not (in r415))
				(in r416)
			)
	)


	(:action move-left-from-r415
		:precondition
			(and 
				(in r415)
			)
		:effect
			(and
				(not (search_again))
				(not (in r415))
				(in r414)
			)
	)

	(:action move-right-from-r416
		:precondition
			(and 
				(in r416)
			)
		:effect
			(and
				(not (search_again))
				(not (in r416))
				(in r417)
			)
	)


	(:action move-left-from-r416
		:precondition
			(and 
				(in r416)
			)
		:effect
			(and
				(not (search_again))
				(not (in r416))
				(in r415)
			)
	)

	(:action move-right-from-r417
		:precondition
			(and 
				(in r417)
			)
		:effect
			(and
				(not (search_again))
				(not (in r417))
				(in r418)
			)
	)


	(:action move-left-from-r417
		:precondition
			(and 
				(in r417)
			)
		:effect
			(and
				(not (search_again))
				(not (in r417))
				(in r416)
			)
	)

	(:action move-right-from-r418
		:precondition
			(and 
				(in r418)
			)
		:effect
			(and
				(not (search_again))
				(not (in r418))
				(in r419)
			)
	)


	(:action move-left-from-r418
		:precondition
			(and 
				(in r418)
			)
		:effect
			(and
				(not (search_again))
				(not (in r418))
				(in r417)
			)
	)

	(:action move-right-from-r419
		:precondition
			(and 
				(in r419)
			)
		:effect
			(and
				(not (search_again))
				(not (in r419))
				(in r420)
			)
	)


	(:action move-left-from-r419
		:precondition
			(and 
				(in r419)
			)
		:effect
			(and
				(not (search_again))
				(not (in r419))
				(in r418)
			)
	)

	(:action move-right-from-r420
		:precondition
			(and 
				(in r420)
			)
		:effect
			(and
				(not (search_again))
				(not (in r420))
				(in r421)
			)
	)


	(:action move-left-from-r420
		:precondition
			(and 
				(in r420)
			)
		:effect
			(and
				(not (search_again))
				(not (in r420))
				(in r419)
			)
	)

	(:action move-right-from-r421
		:precondition
			(and 
				(in r421)
			)
		:effect
			(and
				(not (search_again))
				(not (in r421))
				(in r422)
			)
	)


	(:action move-left-from-r421
		:precondition
			(and 
				(in r421)
			)
		:effect
			(and
				(not (search_again))
				(not (in r421))
				(in r420)
			)
	)

	(:action move-right-from-r422
		:precondition
			(and 
				(in r422)
			)
		:effect
			(and
				(not (search_again))
				(not (in r422))
				(in r423)
			)
	)


	(:action move-left-from-r422
		:precondition
			(and 
				(in r422)
			)
		:effect
			(and
				(not (search_again))
				(not (in r422))
				(in r421)
			)
	)

	(:action move-right-from-r423
		:precondition
			(and 
				(in r423)
			)
		:effect
			(and
				(not (search_again))
				(not (in r423))
				(in r424)
			)
	)


	(:action move-left-from-r423
		:precondition
			(and 
				(in r423)
			)
		:effect
			(and
				(not (search_again))
				(not (in r423))
				(in r422)
			)
	)

	(:action move-right-from-r424
		:precondition
			(and 
				(in r424)
			)
		:effect
			(and
				(not (search_again))
				(not (in r424))
				(in r425)
			)
	)


	(:action move-left-from-r424
		:precondition
			(and 
				(in r424)
			)
		:effect
			(and
				(not (search_again))
				(not (in r424))
				(in r423)
			)
	)

	(:action move-right-from-r425
		:precondition
			(and 
				(in r425)
			)
		:effect
			(and
				(not (search_again))
				(not (in r425))
				(in r426)
			)
	)


	(:action move-left-from-r425
		:precondition
			(and 
				(in r425)
			)
		:effect
			(and
				(not (search_again))
				(not (in r425))
				(in r424)
			)
	)

	(:action move-right-from-r426
		:precondition
			(and 
				(in r426)
			)
		:effect
			(and
				(not (search_again))
				(not (in r426))
				(in r427)
			)
	)


	(:action move-left-from-r426
		:precondition
			(and 
				(in r426)
			)
		:effect
			(and
				(not (search_again))
				(not (in r426))
				(in r425)
			)
	)

	(:action move-right-from-r427
		:precondition
			(and 
				(in r427)
			)
		:effect
			(and
				(not (search_again))
				(not (in r427))
				(in r428)
			)
	)


	(:action move-left-from-r427
		:precondition
			(and 
				(in r427)
			)
		:effect
			(and
				(not (search_again))
				(not (in r427))
				(in r426)
			)
	)

	(:action move-right-from-r428
		:precondition
			(and 
				(in r428)
			)
		:effect
			(and
				(not (search_again))
				(not (in r428))
				(in r429)
			)
	)


	(:action move-left-from-r428
		:precondition
			(and 
				(in r428)
			)
		:effect
			(and
				(not (search_again))
				(not (in r428))
				(in r427)
			)
	)

	(:action move-right-from-r429
		:precondition
			(and 
				(in r429)
			)
		:effect
			(and
				(not (search_again))
				(not (in r429))
				(in r430)
			)
	)


	(:action move-left-from-r429
		:precondition
			(and 
				(in r429)
			)
		:effect
			(and
				(not (search_again))
				(not (in r429))
				(in r428)
			)
	)

	(:action move-right-from-r430
		:precondition
			(and 
				(in r430)
			)
		:effect
			(and
				(not (search_again))
				(not (in r430))
				(in r431)
			)
	)


	(:action move-left-from-r430
		:precondition
			(and 
				(in r430)
			)
		:effect
			(and
				(not (search_again))
				(not (in r430))
				(in r429)
			)
	)

	(:action move-right-from-r431
		:precondition
			(and 
				(in r431)
			)
		:effect
			(and
				(not (search_again))
				(not (in r431))
				(in r432)
			)
	)


	(:action move-left-from-r431
		:precondition
			(and 
				(in r431)
			)
		:effect
			(and
				(not (search_again))
				(not (in r431))
				(in r430)
			)
	)

	(:action move-right-from-r432
		:precondition
			(and 
				(in r432)
			)
		:effect
			(and
				(not (search_again))
				(not (in r432))
				(in r433)
			)
	)


	(:action move-left-from-r432
		:precondition
			(and 
				(in r432)
			)
		:effect
			(and
				(not (search_again))
				(not (in r432))
				(in r431)
			)
	)

	(:action move-right-from-r433
		:precondition
			(and 
				(in r433)
			)
		:effect
			(and
				(not (search_again))
				(not (in r433))
				(in r434)
			)
	)


	(:action move-left-from-r433
		:precondition
			(and 
				(in r433)
			)
		:effect
			(and
				(not (search_again))
				(not (in r433))
				(in r432)
			)
	)

	(:action move-right-from-r434
		:precondition
			(and 
				(in r434)
			)
		:effect
			(and
				(not (search_again))
				(not (in r434))
				(in r435)
			)
	)


	(:action move-left-from-r434
		:precondition
			(and 
				(in r434)
			)
		:effect
			(and
				(not (search_again))
				(not (in r434))
				(in r433)
			)
	)

	(:action move-right-from-r435
		:precondition
			(and 
				(in r435)
			)
		:effect
			(and
				(not (search_again))
				(not (in r435))
				(in r436)
			)
	)


	(:action move-left-from-r435
		:precondition
			(and 
				(in r435)
			)
		:effect
			(and
				(not (search_again))
				(not (in r435))
				(in r434)
			)
	)

	(:action move-right-from-r436
		:precondition
			(and 
				(in r436)
			)
		:effect
			(and
				(not (search_again))
				(not (in r436))
				(in r437)
			)
	)


	(:action move-left-from-r436
		:precondition
			(and 
				(in r436)
			)
		:effect
			(and
				(not (search_again))
				(not (in r436))
				(in r435)
			)
	)

	(:action move-right-from-r437
		:precondition
			(and 
				(in r437)
			)
		:effect
			(and
				(not (search_again))
				(not (in r437))
				(in r438)
			)
	)


	(:action move-left-from-r437
		:precondition
			(and 
				(in r437)
			)
		:effect
			(and
				(not (search_again))
				(not (in r437))
				(in r436)
			)
	)

	(:action move-right-from-r438
		:precondition
			(and 
				(in r438)
			)
		:effect
			(and
				(not (search_again))
				(not (in r438))
				(in r439)
			)
	)


	(:action move-left-from-r438
		:precondition
			(and 
				(in r438)
			)
		:effect
			(and
				(not (search_again))
				(not (in r438))
				(in r437)
			)
	)

	(:action move-right-from-r439
		:precondition
			(and 
				(in r439)
			)
		:effect
			(and
				(not (search_again))
				(not (in r439))
				(in r440)
			)
	)


	(:action move-left-from-r439
		:precondition
			(and 
				(in r439)
			)
		:effect
			(and
				(not (search_again))
				(not (in r439))
				(in r438)
			)
	)

	(:action move-right-from-r440
		:precondition
			(and 
				(in r440)
			)
		:effect
			(and
				(not (search_again))
				(not (in r440))
				(in r441)
			)
	)


	(:action move-left-from-r440
		:precondition
			(and 
				(in r440)
			)
		:effect
			(and
				(not (search_again))
				(not (in r440))
				(in r439)
			)
	)

	(:action move-right-from-r441
		:precondition
			(and 
				(in r441)
			)
		:effect
			(and
				(not (search_again))
				(not (in r441))
				(in r442)
			)
	)


	(:action move-left-from-r441
		:precondition
			(and 
				(in r441)
			)
		:effect
			(and
				(not (search_again))
				(not (in r441))
				(in r440)
			)
	)

	(:action move-right-from-r442
		:precondition
			(and 
				(in r442)
			)
		:effect
			(and
				(not (search_again))
				(not (in r442))
				(in r443)
			)
	)


	(:action move-left-from-r442
		:precondition
			(and 
				(in r442)
			)
		:effect
			(and
				(not (search_again))
				(not (in r442))
				(in r441)
			)
	)

	(:action move-right-from-r443
		:precondition
			(and 
				(in r443)
			)
		:effect
			(and
				(not (search_again))
				(not (in r443))
				(in r444)
			)
	)


	(:action move-left-from-r443
		:precondition
			(and 
				(in r443)
			)
		:effect
			(and
				(not (search_again))
				(not (in r443))
				(in r442)
			)
	)

	(:action move-right-from-r444
		:precondition
			(and 
				(in r444)
			)
		:effect
			(and
				(not (search_again))
				(not (in r444))
				(in r445)
			)
	)


	(:action move-left-from-r444
		:precondition
			(and 
				(in r444)
			)
		:effect
			(and
				(not (search_again))
				(not (in r444))
				(in r443)
			)
	)

	(:action move-right-from-r445
		:precondition
			(and 
				(in r445)
			)
		:effect
			(and
				(not (search_again))
				(not (in r445))
				(in r446)
			)
	)


	(:action move-left-from-r445
		:precondition
			(and 
				(in r445)
			)
		:effect
			(and
				(not (search_again))
				(not (in r445))
				(in r444)
			)
	)

	(:action move-right-from-r446
		:precondition
			(and 
				(in r446)
			)
		:effect
			(and
				(not (search_again))
				(not (in r446))
				(in r447)
			)
	)


	(:action move-left-from-r446
		:precondition
			(and 
				(in r446)
			)
		:effect
			(and
				(not (search_again))
				(not (in r446))
				(in r445)
			)
	)

	(:action move-right-from-r447
		:precondition
			(and 
				(in r447)
			)
		:effect
			(and
				(not (search_again))
				(not (in r447))
				(in r448)
			)
	)


	(:action move-left-from-r447
		:precondition
			(and 
				(in r447)
			)
		:effect
			(and
				(not (search_again))
				(not (in r447))
				(in r446)
			)
	)

	(:action move-right-from-r448
		:precondition
			(and 
				(in r448)
			)
		:effect
			(and
				(not (search_again))
				(not (in r448))
				(in r449)
			)
	)


	(:action move-left-from-r448
		:precondition
			(and 
				(in r448)
			)
		:effect
			(and
				(not (search_again))
				(not (in r448))
				(in r447)
			)
	)

	(:action move-right-from-r449
		:precondition
			(and 
				(in r449)
			)
		:effect
			(and
				(not (search_again))
				(not (in r449))
				(in r450)
			)
	)


	(:action move-left-from-r449
		:precondition
			(and 
				(in r449)
			)
		:effect
			(and
				(not (search_again))
				(not (in r449))
				(in r448)
			)
	)

	(:action move-right-from-r450
		:precondition
			(and 
				(in r450)
			)
		:effect
			(and
				(not (search_again))
				(not (in r450))
				(in r451)
			)
	)


	(:action move-left-from-r450
		:precondition
			(and 
				(in r450)
			)
		:effect
			(and
				(not (search_again))
				(not (in r450))
				(in r449)
			)
	)

	(:action move-right-from-r451
		:precondition
			(and 
				(in r451)
			)
		:effect
			(and
				(not (search_again))
				(not (in r451))
				(in r452)
			)
	)


	(:action move-left-from-r451
		:precondition
			(and 
				(in r451)
			)
		:effect
			(and
				(not (search_again))
				(not (in r451))
				(in r450)
			)
	)

	(:action move-right-from-r452
		:precondition
			(and 
				(in r452)
			)
		:effect
			(and
				(not (search_again))
				(not (in r452))
				(in r453)
			)
	)


	(:action move-left-from-r452
		:precondition
			(and 
				(in r452)
			)
		:effect
			(and
				(not (search_again))
				(not (in r452))
				(in r451)
			)
	)

	(:action move-right-from-r453
		:precondition
			(and 
				(in r453)
			)
		:effect
			(and
				(not (search_again))
				(not (in r453))
				(in r454)
			)
	)


	(:action move-left-from-r453
		:precondition
			(and 
				(in r453)
			)
		:effect
			(and
				(not (search_again))
				(not (in r453))
				(in r452)
			)
	)

	(:action move-right-from-r454
		:precondition
			(and 
				(in r454)
			)
		:effect
			(and
				(not (search_again))
				(not (in r454))
				(in r455)
			)
	)


	(:action move-left-from-r454
		:precondition
			(and 
				(in r454)
			)
		:effect
			(and
				(not (search_again))
				(not (in r454))
				(in r453)
			)
	)

	(:action move-right-from-r455
		:precondition
			(and 
				(in r455)
			)
		:effect
			(and
				(not (search_again))
				(not (in r455))
				(in r456)
			)
	)


	(:action move-left-from-r455
		:precondition
			(and 
				(in r455)
			)
		:effect
			(and
				(not (search_again))
				(not (in r455))
				(in r454)
			)
	)

	(:action move-right-from-r456
		:precondition
			(and 
				(in r456)
			)
		:effect
			(and
				(not (search_again))
				(not (in r456))
				(in r457)
			)
	)


	(:action move-left-from-r456
		:precondition
			(and 
				(in r456)
			)
		:effect
			(and
				(not (search_again))
				(not (in r456))
				(in r455)
			)
	)

	(:action move-right-from-r457
		:precondition
			(and 
				(in r457)
			)
		:effect
			(and
				(not (search_again))
				(not (in r457))
				(in r458)
			)
	)


	(:action move-left-from-r457
		:precondition
			(and 
				(in r457)
			)
		:effect
			(and
				(not (search_again))
				(not (in r457))
				(in r456)
			)
	)

	(:action move-right-from-r458
		:precondition
			(and 
				(in r458)
			)
		:effect
			(and
				(not (search_again))
				(not (in r458))
				(in r459)
			)
	)


	(:action move-left-from-r458
		:precondition
			(and 
				(in r458)
			)
		:effect
			(and
				(not (search_again))
				(not (in r458))
				(in r457)
			)
	)

	(:action move-right-from-r459
		:precondition
			(and 
				(in r459)
			)
		:effect
			(and
				(not (search_again))
				(not (in r459))
				(in r460)
			)
	)


	(:action move-left-from-r459
		:precondition
			(and 
				(in r459)
			)
		:effect
			(and
				(not (search_again))
				(not (in r459))
				(in r458)
			)
	)

	(:action move-right-from-r460
		:precondition
			(and 
				(in r460)
			)
		:effect
			(and
				(not (search_again))
				(not (in r460))
				(in r461)
			)
	)


	(:action move-left-from-r460
		:precondition
			(and 
				(in r460)
			)
		:effect
			(and
				(not (search_again))
				(not (in r460))
				(in r459)
			)
	)

	(:action move-right-from-r461
		:precondition
			(and 
				(in r461)
			)
		:effect
			(and
				(not (search_again))
				(not (in r461))
				(in r462)
			)
	)


	(:action move-left-from-r461
		:precondition
			(and 
				(in r461)
			)
		:effect
			(and
				(not (search_again))
				(not (in r461))
				(in r460)
			)
	)

	(:action move-right-from-r462
		:precondition
			(and 
				(in r462)
			)
		:effect
			(and
				(not (search_again))
				(not (in r462))
				(in r463)
			)
	)


	(:action move-left-from-r462
		:precondition
			(and 
				(in r462)
			)
		:effect
			(and
				(not (search_again))
				(not (in r462))
				(in r461)
			)
	)

	(:action move-right-from-r463
		:precondition
			(and 
				(in r463)
			)
		:effect
			(and
				(not (search_again))
				(not (in r463))
				(in r464)
			)
	)


	(:action move-left-from-r463
		:precondition
			(and 
				(in r463)
			)
		:effect
			(and
				(not (search_again))
				(not (in r463))
				(in r462)
			)
	)

	(:action move-right-from-r464
		:precondition
			(and 
				(in r464)
			)
		:effect
			(and
				(not (search_again))
				(not (in r464))
				(in r465)
			)
	)


	(:action move-left-from-r464
		:precondition
			(and 
				(in r464)
			)
		:effect
			(and
				(not (search_again))
				(not (in r464))
				(in r463)
			)
	)

	(:action move-right-from-r465
		:precondition
			(and 
				(in r465)
			)
		:effect
			(and
				(not (search_again))
				(not (in r465))
				(in r466)
			)
	)


	(:action move-left-from-r465
		:precondition
			(and 
				(in r465)
			)
		:effect
			(and
				(not (search_again))
				(not (in r465))
				(in r464)
			)
	)

	(:action move-right-from-r466
		:precondition
			(and 
				(in r466)
			)
		:effect
			(and
				(not (search_again))
				(not (in r466))
				(in r467)
			)
	)


	(:action move-left-from-r466
		:precondition
			(and 
				(in r466)
			)
		:effect
			(and
				(not (search_again))
				(not (in r466))
				(in r465)
			)
	)

	(:action move-right-from-r467
		:precondition
			(and 
				(in r467)
			)
		:effect
			(and
				(not (search_again))
				(not (in r467))
				(in r468)
			)
	)


	(:action move-left-from-r467
		:precondition
			(and 
				(in r467)
			)
		:effect
			(and
				(not (search_again))
				(not (in r467))
				(in r466)
			)
	)

	(:action move-right-from-r468
		:precondition
			(and 
				(in r468)
			)
		:effect
			(and
				(not (search_again))
				(not (in r468))
				(in r469)
			)
	)


	(:action move-left-from-r468
		:precondition
			(and 
				(in r468)
			)
		:effect
			(and
				(not (search_again))
				(not (in r468))
				(in r467)
			)
	)

	(:action move-right-from-r469
		:precondition
			(and 
				(in r469)
			)
		:effect
			(and
				(not (search_again))
				(not (in r469))
				(in r470)
			)
	)


	(:action move-left-from-r469
		:precondition
			(and 
				(in r469)
			)
		:effect
			(and
				(not (search_again))
				(not (in r469))
				(in r468)
			)
	)

	(:action move-right-from-r470
		:precondition
			(and 
				(in r470)
			)
		:effect
			(and
				(not (search_again))
				(not (in r470))
				(in r471)
			)
	)


	(:action move-left-from-r470
		:precondition
			(and 
				(in r470)
			)
		:effect
			(and
				(not (search_again))
				(not (in r470))
				(in r469)
			)
	)

	(:action move-right-from-r471
		:precondition
			(and 
				(in r471)
			)
		:effect
			(and
				(not (search_again))
				(not (in r471))
				(in r472)
			)
	)


	(:action move-left-from-r471
		:precondition
			(and 
				(in r471)
			)
		:effect
			(and
				(not (search_again))
				(not (in r471))
				(in r470)
			)
	)

	(:action move-right-from-r472
		:precondition
			(and 
				(in r472)
			)
		:effect
			(and
				(not (search_again))
				(not (in r472))
				(in r473)
			)
	)


	(:action move-left-from-r472
		:precondition
			(and 
				(in r472)
			)
		:effect
			(and
				(not (search_again))
				(not (in r472))
				(in r471)
			)
	)

	(:action move-right-from-r473
		:precondition
			(and 
				(in r473)
			)
		:effect
			(and
				(not (search_again))
				(not (in r473))
				(in r474)
			)
	)


	(:action move-left-from-r473
		:precondition
			(and 
				(in r473)
			)
		:effect
			(and
				(not (search_again))
				(not (in r473))
				(in r472)
			)
	)

	(:action move-right-from-r474
		:precondition
			(and 
				(in r474)
			)
		:effect
			(and
				(not (search_again))
				(not (in r474))
				(in r475)
			)
	)


	(:action move-left-from-r474
		:precondition
			(and 
				(in r474)
			)
		:effect
			(and
				(not (search_again))
				(not (in r474))
				(in r473)
			)
	)

	(:action move-right-from-r475
		:precondition
			(and 
				(in r475)
			)
		:effect
			(and
				(not (search_again))
				(not (in r475))
				(in r476)
			)
	)


	(:action move-left-from-r475
		:precondition
			(and 
				(in r475)
			)
		:effect
			(and
				(not (search_again))
				(not (in r475))
				(in r474)
			)
	)

	(:action move-right-from-r476
		:precondition
			(and 
				(in r476)
			)
		:effect
			(and
				(not (search_again))
				(not (in r476))
				(in r477)
			)
	)


	(:action move-left-from-r476
		:precondition
			(and 
				(in r476)
			)
		:effect
			(and
				(not (search_again))
				(not (in r476))
				(in r475)
			)
	)

	(:action move-right-from-r477
		:precondition
			(and 
				(in r477)
			)
		:effect
			(and
				(not (search_again))
				(not (in r477))
				(in r478)
			)
	)


	(:action move-left-from-r477
		:precondition
			(and 
				(in r477)
			)
		:effect
			(and
				(not (search_again))
				(not (in r477))
				(in r476)
			)
	)

	(:action move-right-from-r478
		:precondition
			(and 
				(in r478)
			)
		:effect
			(and
				(not (search_again))
				(not (in r478))
				(in r479)
			)
	)


	(:action move-left-from-r478
		:precondition
			(and 
				(in r478)
			)
		:effect
			(and
				(not (search_again))
				(not (in r478))
				(in r477)
			)
	)

	(:action move-right-from-r479
		:precondition
			(and 
				(in r479)
			)
		:effect
			(and
				(not (search_again))
				(not (in r479))
				(in r480)
			)
	)


	(:action move-left-from-r479
		:precondition
			(and 
				(in r479)
			)
		:effect
			(and
				(not (search_again))
				(not (in r479))
				(in r478)
			)
	)

	(:action move-right-from-r480
		:precondition
			(and 
				(in r480)
			)
		:effect
			(and
				(not (search_again))
				(not (in r480))
				(in r481)
			)
	)


	(:action move-left-from-r480
		:precondition
			(and 
				(in r480)
			)
		:effect
			(and
				(not (search_again))
				(not (in r480))
				(in r479)
			)
	)

	(:action move-right-from-r481
		:precondition
			(and 
				(in r481)
			)
		:effect
			(and
				(not (search_again))
				(not (in r481))
				(in r482)
			)
	)


	(:action move-left-from-r481
		:precondition
			(and 
				(in r481)
			)
		:effect
			(and
				(not (search_again))
				(not (in r481))
				(in r480)
			)
	)

	(:action move-right-from-r482
		:precondition
			(and 
				(in r482)
			)
		:effect
			(and
				(not (search_again))
				(not (in r482))
				(in r483)
			)
	)


	(:action move-left-from-r482
		:precondition
			(and 
				(in r482)
			)
		:effect
			(and
				(not (search_again))
				(not (in r482))
				(in r481)
			)
	)

	(:action move-right-from-r483
		:precondition
			(and 
				(in r483)
			)
		:effect
			(and
				(not (search_again))
				(not (in r483))
				(in r484)
			)
	)


	(:action move-left-from-r483
		:precondition
			(and 
				(in r483)
			)
		:effect
			(and
				(not (search_again))
				(not (in r483))
				(in r482)
			)
	)

	(:action move-right-from-r484
		:precondition
			(and 
				(in r484)
			)
		:effect
			(and
				(not (search_again))
				(not (in r484))
				(in r485)
			)
	)


	(:action move-left-from-r484
		:precondition
			(and 
				(in r484)
			)
		:effect
			(and
				(not (search_again))
				(not (in r484))
				(in r483)
			)
	)

	(:action move-right-from-r485
		:precondition
			(and 
				(in r485)
			)
		:effect
			(and
				(not (search_again))
				(not (in r485))
				(in r486)
			)
	)


	(:action move-left-from-r485
		:precondition
			(and 
				(in r485)
			)
		:effect
			(and
				(not (search_again))
				(not (in r485))
				(in r484)
			)
	)

	(:action move-right-from-r486
		:precondition
			(and 
				(in r486)
			)
		:effect
			(and
				(not (search_again))
				(not (in r486))
				(in r487)
			)
	)


	(:action move-left-from-r486
		:precondition
			(and 
				(in r486)
			)
		:effect
			(and
				(not (search_again))
				(not (in r486))
				(in r485)
			)
	)

	(:action move-right-from-r487
		:precondition
			(and 
				(in r487)
			)
		:effect
			(and
				(not (search_again))
				(not (in r487))
				(in r488)
			)
	)


	(:action move-left-from-r487
		:precondition
			(and 
				(in r487)
			)
		:effect
			(and
				(not (search_again))
				(not (in r487))
				(in r486)
			)
	)

	(:action move-right-from-r488
		:precondition
			(and 
				(in r488)
			)
		:effect
			(and
				(not (search_again))
				(not (in r488))
				(in r489)
			)
	)


	(:action move-left-from-r488
		:precondition
			(and 
				(in r488)
			)
		:effect
			(and
				(not (search_again))
				(not (in r488))
				(in r487)
			)
	)

	(:action move-right-from-r489
		:precondition
			(and 
				(in r489)
			)
		:effect
			(and
				(not (search_again))
				(not (in r489))
				(in r490)
			)
	)


	(:action move-left-from-r489
		:precondition
			(and 
				(in r489)
			)
		:effect
			(and
				(not (search_again))
				(not (in r489))
				(in r488)
			)
	)

	(:action move-right-from-r490
		:precondition
			(and 
				(in r490)
			)
		:effect
			(and
				(not (search_again))
				(not (in r490))
				(in r491)
			)
	)


	(:action move-left-from-r490
		:precondition
			(and 
				(in r490)
			)
		:effect
			(and
				(not (search_again))
				(not (in r490))
				(in r489)
			)
	)

	(:action move-right-from-r491
		:precondition
			(and 
				(in r491)
			)
		:effect
			(and
				(not (search_again))
				(not (in r491))
				(in r492)
			)
	)


	(:action move-left-from-r491
		:precondition
			(and 
				(in r491)
			)
		:effect
			(and
				(not (search_again))
				(not (in r491))
				(in r490)
			)
	)

	(:action move-right-from-r492
		:precondition
			(and 
				(in r492)
			)
		:effect
			(and
				(not (search_again))
				(not (in r492))
				(in r493)
			)
	)


	(:action move-left-from-r492
		:precondition
			(and 
				(in r492)
			)
		:effect
			(and
				(not (search_again))
				(not (in r492))
				(in r491)
			)
	)

	(:action move-right-from-r493
		:precondition
			(and 
				(in r493)
			)
		:effect
			(and
				(not (search_again))
				(not (in r493))
				(in r494)
			)
	)


	(:action move-left-from-r493
		:precondition
			(and 
				(in r493)
			)
		:effect
			(and
				(not (search_again))
				(not (in r493))
				(in r492)
			)
	)

	(:action move-right-from-r494
		:precondition
			(and 
				(in r494)
			)
		:effect
			(and
				(not (search_again))
				(not (in r494))
				(in r495)
			)
	)


	(:action move-left-from-r494
		:precondition
			(and 
				(in r494)
			)
		:effect
			(and
				(not (search_again))
				(not (in r494))
				(in r493)
			)
	)

	(:action move-right-from-r495
		:precondition
			(and 
				(in r495)
			)
		:effect
			(and
				(not (search_again))
				(not (in r495))
				(in r496)
			)
	)


	(:action move-left-from-r495
		:precondition
			(and 
				(in r495)
			)
		:effect
			(and
				(not (search_again))
				(not (in r495))
				(in r494)
			)
	)

	(:action move-right-from-r496
		:precondition
			(and 
				(in r496)
			)
		:effect
			(and
				(not (search_again))
				(not (in r496))
				(in r497)
			)
	)


	(:action move-left-from-r496
		:precondition
			(and 
				(in r496)
			)
		:effect
			(and
				(not (search_again))
				(not (in r496))
				(in r495)
			)
	)

	(:action move-right-from-r497
		:precondition
			(and 
				(in r497)
			)
		:effect
			(and
				(not (search_again))
				(not (in r497))
				(in r498)
			)
	)


	(:action move-left-from-r497
		:precondition
			(and 
				(in r497)
			)
		:effect
			(and
				(not (search_again))
				(not (in r497))
				(in r496)
			)
	)

	(:action move-right-from-r498
		:precondition
			(and 
				(in r498)
			)
		:effect
			(and
				(not (search_again))
				(not (in r498))
				(in r499)
			)
	)


	(:action move-left-from-r498
		:precondition
			(and 
				(in r498)
			)
		:effect
			(and
				(not (search_again))
				(not (in r498))
				(in r497)
			)
	)

	(:action move-right-from-r499
		:precondition
			(and 
				(in r499)
			)
		:effect
			(and
				(not (search_again))
				(not (in r499))
				(in r500)
			)
	)


	(:action move-left-from-r499
		:precondition
			(and 
				(in r499)
			)
		:effect
			(and
				(not (search_again))
				(not (in r499))
				(in r498)
			)
	)

	(:action move-right-from-r500
		:precondition
			(and 
				(in r500)
			)
		:effect
			(and
				(not (search_again))
				(not (in r500))
				(in r501)
			)
	)


	(:action move-left-from-r500
		:precondition
			(and 
				(in r500)
			)
		:effect
			(and
				(not (search_again))
				(not (in r500))
				(in r499)
			)
	)

	(:action move-right-from-r501
		:precondition
			(and 
				(in r501)
			)
		:effect
			(and
				(not (search_again))
				(not (in r501))
				(in r502)
			)
	)


	(:action move-left-from-r501
		:precondition
			(and 
				(in r501)
			)
		:effect
			(and
				(not (search_again))
				(not (in r501))
				(in r500)
			)
	)

	(:action move-right-from-r502
		:precondition
			(and 
				(in r502)
			)
		:effect
			(and
				(not (search_again))
				(not (in r502))
				(in r503)
			)
	)


	(:action move-left-from-r502
		:precondition
			(and 
				(in r502)
			)
		:effect
			(and
				(not (search_again))
				(not (in r502))
				(in r501)
			)
	)

	(:action move-right-from-r503
		:precondition
			(and 
				(in r503)
			)
		:effect
			(and
				(not (search_again))
				(not (in r503))
				(in r504)
			)
	)


	(:action move-left-from-r503
		:precondition
			(and 
				(in r503)
			)
		:effect
			(and
				(not (search_again))
				(not (in r503))
				(in r502)
			)
	)

	(:action move-right-from-r504
		:precondition
			(and 
				(in r504)
			)
		:effect
			(and
				(not (search_again))
				(not (in r504))
				(in r505)
			)
	)


	(:action move-left-from-r504
		:precondition
			(and 
				(in r504)
			)
		:effect
			(and
				(not (search_again))
				(not (in r504))
				(in r503)
			)
	)

	(:action move-right-from-r505
		:precondition
			(and 
				(in r505)
			)
		:effect
			(and
				(not (search_again))
				(not (in r505))
				(in r506)
			)
	)


	(:action move-left-from-r505
		:precondition
			(and 
				(in r505)
			)
		:effect
			(and
				(not (search_again))
				(not (in r505))
				(in r504)
			)
	)

	(:action move-right-from-r506
		:precondition
			(and 
				(in r506)
			)
		:effect
			(and
				(not (search_again))
				(not (in r506))
				(in r507)
			)
	)


	(:action move-left-from-r506
		:precondition
			(and 
				(in r506)
			)
		:effect
			(and
				(not (search_again))
				(not (in r506))
				(in r505)
			)
	)

	(:action move-right-from-r507
		:precondition
			(and 
				(in r507)
			)
		:effect
			(and
				(not (search_again))
				(not (in r507))
				(in r508)
			)
	)


	(:action move-left-from-r507
		:precondition
			(and 
				(in r507)
			)
		:effect
			(and
				(not (search_again))
				(not (in r507))
				(in r506)
			)
	)

	(:action move-right-from-r508
		:precondition
			(and 
				(in r508)
			)
		:effect
			(and
				(not (search_again))
				(not (in r508))
				(in r509)
			)
	)


	(:action move-left-from-r508
		:precondition
			(and 
				(in r508)
			)
		:effect
			(and
				(not (search_again))
				(not (in r508))
				(in r507)
			)
	)

	(:action move-right-from-r509
		:precondition
			(and 
				(in r509)
			)
		:effect
			(and
				(not (search_again))
				(not (in r509))
				(in r510)
			)
	)


	(:action move-left-from-r509
		:precondition
			(and 
				(in r509)
			)
		:effect
			(and
				(not (search_again))
				(not (in r509))
				(in r508)
			)
	)

	(:action move-right-from-r510
		:precondition
			(and 
				(in r510)
			)
		:effect
			(and
				(not (search_again))
				(not (in r510))
				(in r511)
			)
	)


	(:action move-left-from-r510
		:precondition
			(and 
				(in r510)
			)
		:effect
			(and
				(not (search_again))
				(not (in r510))
				(in r509)
			)
	)

	(:action move-right-from-r511
		:precondition
			(and 
				(in r511)
			)
		:effect
			(and
				(not (search_again))
				(not (in r511))
				(in r512)
			)
	)


	(:action move-left-from-r511
		:precondition
			(and 
				(in r511)
			)
		:effect
			(and
				(not (search_again))
				(not (in r511))
				(in r510)
			)
	)

	(:action move-right-from-r512
		:precondition
			(and 
				(in r512)
			)
		:effect
			(and
				(not (search_again))
				(not (in r512))
				(in r513)
			)
	)


	(:action move-left-from-r512
		:precondition
			(and 
				(in r512)
			)
		:effect
			(and
				(not (search_again))
				(not (in r512))
				(in r511)
			)
	)

	(:action move-right-from-r513
		:precondition
			(and 
				(in r513)
			)
		:effect
			(and
				(not (search_again))
				(not (in r513))
				(in r514)
			)
	)


	(:action move-left-from-r513
		:precondition
			(and 
				(in r513)
			)
		:effect
			(and
				(not (search_again))
				(not (in r513))
				(in r512)
			)
	)

	(:action move-right-from-r514
		:precondition
			(and 
				(in r514)
			)
		:effect
			(and
				(not (search_again))
				(not (in r514))
				(in r515)
			)
	)


	(:action move-left-from-r514
		:precondition
			(and 
				(in r514)
			)
		:effect
			(and
				(not (search_again))
				(not (in r514))
				(in r513)
			)
	)

	(:action move-right-from-r515
		:precondition
			(and 
				(in r515)
			)
		:effect
			(and
				(not (search_again))
				(not (in r515))
				(in r516)
			)
	)


	(:action move-left-from-r515
		:precondition
			(and 
				(in r515)
			)
		:effect
			(and
				(not (search_again))
				(not (in r515))
				(in r514)
			)
	)

	(:action move-right-from-r516
		:precondition
			(and 
				(in r516)
			)
		:effect
			(and
				(not (search_again))
				(not (in r516))
				(in r517)
			)
	)


	(:action move-left-from-r516
		:precondition
			(and 
				(in r516)
			)
		:effect
			(and
				(not (search_again))
				(not (in r516))
				(in r515)
			)
	)

	(:action move-right-from-r517
		:precondition
			(and 
				(in r517)
			)
		:effect
			(and
				(not (search_again))
				(not (in r517))
				(in r518)
			)
	)


	(:action move-left-from-r517
		:precondition
			(and 
				(in r517)
			)
		:effect
			(and
				(not (search_again))
				(not (in r517))
				(in r516)
			)
	)

	(:action move-right-from-r518
		:precondition
			(and 
				(in r518)
			)
		:effect
			(and
				(not (search_again))
				(not (in r518))
				(in r519)
			)
	)


	(:action move-left-from-r518
		:precondition
			(and 
				(in r518)
			)
		:effect
			(and
				(not (search_again))
				(not (in r518))
				(in r517)
			)
	)

	(:action move-right-from-r519
		:precondition
			(and 
				(in r519)
			)
		:effect
			(and
				(not (search_again))
				(not (in r519))
				(in r520)
			)
	)


	(:action move-left-from-r519
		:precondition
			(and 
				(in r519)
			)
		:effect
			(and
				(not (search_again))
				(not (in r519))
				(in r518)
			)
	)

	(:action move-right-from-r520
		:precondition
			(and 
				(in r520)
			)
		:effect
			(and
				(not (search_again))
				(not (in r520))
				(in r521)
			)
	)


	(:action move-left-from-r520
		:precondition
			(and 
				(in r520)
			)
		:effect
			(and
				(not (search_again))
				(not (in r520))
				(in r519)
			)
	)

	(:action move-right-from-r521
		:precondition
			(and 
				(in r521)
			)
		:effect
			(and
				(not (search_again))
				(not (in r521))
				(in r522)
			)
	)


	(:action move-left-from-r521
		:precondition
			(and 
				(in r521)
			)
		:effect
			(and
				(not (search_again))
				(not (in r521))
				(in r520)
			)
	)

	(:action move-right-from-r522
		:precondition
			(and 
				(in r522)
			)
		:effect
			(and
				(not (search_again))
				(not (in r522))
				(in r523)
			)
	)


	(:action move-left-from-r522
		:precondition
			(and 
				(in r522)
			)
		:effect
			(and
				(not (search_again))
				(not (in r522))
				(in r521)
			)
	)

	(:action move-right-from-r523
		:precondition
			(and 
				(in r523)
			)
		:effect
			(and
				(not (search_again))
				(not (in r523))
				(in r524)
			)
	)


	(:action move-left-from-r523
		:precondition
			(and 
				(in r523)
			)
		:effect
			(and
				(not (search_again))
				(not (in r523))
				(in r522)
			)
	)

	(:action move-right-from-r524
		:precondition
			(and 
				(in r524)
			)
		:effect
			(and
				(not (search_again))
				(not (in r524))
				(in r525)
			)
	)


	(:action move-left-from-r524
		:precondition
			(and 
				(in r524)
			)
		:effect
			(and
				(not (search_again))
				(not (in r524))
				(in r523)
			)
	)

	(:action move-right-from-r525
		:precondition
			(and 
				(in r525)
			)
		:effect
			(and
				(not (search_again))
				(not (in r525))
				(in r526)
			)
	)


	(:action move-left-from-r525
		:precondition
			(and 
				(in r525)
			)
		:effect
			(and
				(not (search_again))
				(not (in r525))
				(in r524)
			)
	)

	(:action move-right-from-r526
		:precondition
			(and 
				(in r526)
			)
		:effect
			(and
				(not (search_again))
				(not (in r526))
				(in r527)
			)
	)


	(:action move-left-from-r526
		:precondition
			(and 
				(in r526)
			)
		:effect
			(and
				(not (search_again))
				(not (in r526))
				(in r525)
			)
	)

	(:action move-right-from-r527
		:precondition
			(and 
				(in r527)
			)
		:effect
			(and
				(not (search_again))
				(not (in r527))
				(in r528)
			)
	)


	(:action move-left-from-r527
		:precondition
			(and 
				(in r527)
			)
		:effect
			(and
				(not (search_again))
				(not (in r527))
				(in r526)
			)
	)

	(:action move-right-from-r528
		:precondition
			(and 
				(in r528)
			)
		:effect
			(and
				(not (search_again))
				(not (in r528))
				(in r529)
			)
	)


	(:action move-left-from-r528
		:precondition
			(and 
				(in r528)
			)
		:effect
			(and
				(not (search_again))
				(not (in r528))
				(in r527)
			)
	)

	(:action move-right-from-r529
		:precondition
			(and 
				(in r529)
			)
		:effect
			(and
				(not (search_again))
				(not (in r529))
				(in r530)
			)
	)


	(:action move-left-from-r529
		:precondition
			(and 
				(in r529)
			)
		:effect
			(and
				(not (search_again))
				(not (in r529))
				(in r528)
			)
	)

	(:action move-right-from-r530
		:precondition
			(and 
				(in r530)
			)
		:effect
			(and
				(not (search_again))
				(not (in r530))
				(in r531)
			)
	)


	(:action move-left-from-r530
		:precondition
			(and 
				(in r530)
			)
		:effect
			(and
				(not (search_again))
				(not (in r530))
				(in r529)
			)
	)

	(:action move-right-from-r531
		:precondition
			(and 
				(in r531)
			)
		:effect
			(and
				(not (search_again))
				(not (in r531))
				(in r532)
			)
	)


	(:action move-left-from-r531
		:precondition
			(and 
				(in r531)
			)
		:effect
			(and
				(not (search_again))
				(not (in r531))
				(in r530)
			)
	)

	(:action move-right-from-r532
		:precondition
			(and 
				(in r532)
			)
		:effect
			(and
				(not (search_again))
				(not (in r532))
				(in r533)
			)
	)


	(:action move-left-from-r532
		:precondition
			(and 
				(in r532)
			)
		:effect
			(and
				(not (search_again))
				(not (in r532))
				(in r531)
			)
	)

	(:action move-right-from-r533
		:precondition
			(and 
				(in r533)
			)
		:effect
			(and
				(not (search_again))
				(not (in r533))
				(in r534)
			)
	)


	(:action move-left-from-r533
		:precondition
			(and 
				(in r533)
			)
		:effect
			(and
				(not (search_again))
				(not (in r533))
				(in r532)
			)
	)

	(:action move-right-from-r534
		:precondition
			(and 
				(in r534)
			)
		:effect
			(and
				(not (search_again))
				(not (in r534))
				(in r535)
			)
	)


	(:action move-left-from-r534
		:precondition
			(and 
				(in r534)
			)
		:effect
			(and
				(not (search_again))
				(not (in r534))
				(in r533)
			)
	)

	(:action move-right-from-r535
		:precondition
			(and 
				(in r535)
			)
		:effect
			(and
				(not (search_again))
				(not (in r535))
				(in r536)
			)
	)


	(:action move-left-from-r535
		:precondition
			(and 
				(in r535)
			)
		:effect
			(and
				(not (search_again))
				(not (in r535))
				(in r534)
			)
	)

	(:action move-right-from-r536
		:precondition
			(and 
				(in r536)
			)
		:effect
			(and
				(not (search_again))
				(not (in r536))
				(in r537)
			)
	)


	(:action move-left-from-r536
		:precondition
			(and 
				(in r536)
			)
		:effect
			(and
				(not (search_again))
				(not (in r536))
				(in r535)
			)
	)

	(:action move-right-from-r537
		:precondition
			(and 
				(in r537)
			)
		:effect
			(and
				(not (search_again))
				(not (in r537))
				(in r538)
			)
	)


	(:action move-left-from-r537
		:precondition
			(and 
				(in r537)
			)
		:effect
			(and
				(not (search_again))
				(not (in r537))
				(in r536)
			)
	)

	(:action move-right-from-r538
		:precondition
			(and 
				(in r538)
			)
		:effect
			(and
				(not (search_again))
				(not (in r538))
				(in r539)
			)
	)


	(:action move-left-from-r538
		:precondition
			(and 
				(in r538)
			)
		:effect
			(and
				(not (search_again))
				(not (in r538))
				(in r537)
			)
	)

	(:action move-right-from-r539
		:precondition
			(and 
				(in r539)
			)
		:effect
			(and
				(not (search_again))
				(not (in r539))
				(in r540)
			)
	)


	(:action move-left-from-r539
		:precondition
			(and 
				(in r539)
			)
		:effect
			(and
				(not (search_again))
				(not (in r539))
				(in r538)
			)
	)

	(:action move-right-from-r540
		:precondition
			(and 
				(in r540)
			)
		:effect
			(and
				(not (search_again))
				(not (in r540))
				(in r541)
			)
	)


	(:action move-left-from-r540
		:precondition
			(and 
				(in r540)
			)
		:effect
			(and
				(not (search_again))
				(not (in r540))
				(in r539)
			)
	)

	(:action move-right-from-r541
		:precondition
			(and 
				(in r541)
			)
		:effect
			(and
				(not (search_again))
				(not (in r541))
				(in r542)
			)
	)


	(:action move-left-from-r541
		:precondition
			(and 
				(in r541)
			)
		:effect
			(and
				(not (search_again))
				(not (in r541))
				(in r540)
			)
	)

	(:action move-right-from-r542
		:precondition
			(and 
				(in r542)
			)
		:effect
			(and
				(not (search_again))
				(not (in r542))
				(in r543)
			)
	)


	(:action move-left-from-r542
		:precondition
			(and 
				(in r542)
			)
		:effect
			(and
				(not (search_again))
				(not (in r542))
				(in r541)
			)
	)

	(:action move-right-from-r543
		:precondition
			(and 
				(in r543)
			)
		:effect
			(and
				(not (search_again))
				(not (in r543))
				(in r544)
			)
	)


	(:action move-left-from-r543
		:precondition
			(and 
				(in r543)
			)
		:effect
			(and
				(not (search_again))
				(not (in r543))
				(in r542)
			)
	)

	(:action move-right-from-r544
		:precondition
			(and 
				(in r544)
			)
		:effect
			(and
				(not (search_again))
				(not (in r544))
				(in r545)
			)
	)


	(:action move-left-from-r544
		:precondition
			(and 
				(in r544)
			)
		:effect
			(and
				(not (search_again))
				(not (in r544))
				(in r543)
			)
	)

	(:action move-right-from-r545
		:precondition
			(and 
				(in r545)
			)
		:effect
			(and
				(not (search_again))
				(not (in r545))
				(in r546)
			)
	)


	(:action move-left-from-r545
		:precondition
			(and 
				(in r545)
			)
		:effect
			(and
				(not (search_again))
				(not (in r545))
				(in r544)
			)
	)

	(:action move-right-from-r546
		:precondition
			(and 
				(in r546)
			)
		:effect
			(and
				(not (search_again))
				(not (in r546))
				(in r547)
			)
	)


	(:action move-left-from-r546
		:precondition
			(and 
				(in r546)
			)
		:effect
			(and
				(not (search_again))
				(not (in r546))
				(in r545)
			)
	)

	(:action move-right-from-r547
		:precondition
			(and 
				(in r547)
			)
		:effect
			(and
				(not (search_again))
				(not (in r547))
				(in r548)
			)
	)


	(:action move-left-from-r547
		:precondition
			(and 
				(in r547)
			)
		:effect
			(and
				(not (search_again))
				(not (in r547))
				(in r546)
			)
	)

	(:action move-right-from-r548
		:precondition
			(and 
				(in r548)
			)
		:effect
			(and
				(not (search_again))
				(not (in r548))
				(in r549)
			)
	)


	(:action move-left-from-r548
		:precondition
			(and 
				(in r548)
			)
		:effect
			(and
				(not (search_again))
				(not (in r548))
				(in r547)
			)
	)

	(:action move-right-from-r549
		:precondition
			(and 
				(in r549)
			)
		:effect
			(and
				(not (search_again))
				(not (in r549))
				(in r550)
			)
	)


	(:action move-left-from-r549
		:precondition
			(and 
				(in r549)
			)
		:effect
			(and
				(not (search_again))
				(not (in r549))
				(in r548)
			)
	)

	(:action move-right-from-r550
		:precondition
			(and 
				(in r550)
			)
		:effect
			(and
				(not (search_again))
				(not (in r550))
				(in r551)
			)
	)


	(:action move-left-from-r550
		:precondition
			(and 
				(in r550)
			)
		:effect
			(and
				(not (search_again))
				(not (in r550))
				(in r549)
			)
	)

	(:action move-right-from-r551
		:precondition
			(and 
				(in r551)
			)
		:effect
			(and
				(not (search_again))
				(not (in r551))
				(in r552)
			)
	)


	(:action move-left-from-r551
		:precondition
			(and 
				(in r551)
			)
		:effect
			(and
				(not (search_again))
				(not (in r551))
				(in r550)
			)
	)

	(:action move-right-from-r552
		:precondition
			(and 
				(in r552)
			)
		:effect
			(and
				(not (search_again))
				(not (in r552))
				(in r553)
			)
	)


	(:action move-left-from-r552
		:precondition
			(and 
				(in r552)
			)
		:effect
			(and
				(not (search_again))
				(not (in r552))
				(in r551)
			)
	)

	(:action move-right-from-r553
		:precondition
			(and 
				(in r553)
			)
		:effect
			(and
				(not (search_again))
				(not (in r553))
				(in r554)
			)
	)


	(:action move-left-from-r553
		:precondition
			(and 
				(in r553)
			)
		:effect
			(and
				(not (search_again))
				(not (in r553))
				(in r552)
			)
	)

	(:action move-right-from-r554
		:precondition
			(and 
				(in r554)
			)
		:effect
			(and
				(not (search_again))
				(not (in r554))
				(in r555)
			)
	)


	(:action move-left-from-r554
		:precondition
			(and 
				(in r554)
			)
		:effect
			(and
				(not (search_again))
				(not (in r554))
				(in r553)
			)
	)

	(:action move-right-from-r555
		:precondition
			(and 
				(in r555)
			)
		:effect
			(and
				(not (search_again))
				(not (in r555))
				(in r556)
			)
	)


	(:action move-left-from-r555
		:precondition
			(and 
				(in r555)
			)
		:effect
			(and
				(not (search_again))
				(not (in r555))
				(in r554)
			)
	)

	(:action move-right-from-r556
		:precondition
			(and 
				(in r556)
			)
		:effect
			(and
				(not (search_again))
				(not (in r556))
				(in r557)
			)
	)


	(:action move-left-from-r556
		:precondition
			(and 
				(in r556)
			)
		:effect
			(and
				(not (search_again))
				(not (in r556))
				(in r555)
			)
	)

	(:action move-right-from-r557
		:precondition
			(and 
				(in r557)
			)
		:effect
			(and
				(not (search_again))
				(not (in r557))
				(in r558)
			)
	)


	(:action move-left-from-r557
		:precondition
			(and 
				(in r557)
			)
		:effect
			(and
				(not (search_again))
				(not (in r557))
				(in r556)
			)
	)

	(:action move-right-from-r558
		:precondition
			(and 
				(in r558)
			)
		:effect
			(and
				(not (search_again))
				(not (in r558))
				(in r559)
			)
	)


	(:action move-left-from-r558
		:precondition
			(and 
				(in r558)
			)
		:effect
			(and
				(not (search_again))
				(not (in r558))
				(in r557)
			)
	)

	(:action move-right-from-r559
		:precondition
			(and 
				(in r559)
			)
		:effect
			(and
				(not (search_again))
				(not (in r559))
				(in r560)
			)
	)


	(:action move-left-from-r559
		:precondition
			(and 
				(in r559)
			)
		:effect
			(and
				(not (search_again))
				(not (in r559))
				(in r558)
			)
	)

	(:action move-right-from-r560
		:precondition
			(and 
				(in r560)
			)
		:effect
			(and
				(not (search_again))
				(not (in r560))
				(in r561)
			)
	)


	(:action move-left-from-r560
		:precondition
			(and 
				(in r560)
			)
		:effect
			(and
				(not (search_again))
				(not (in r560))
				(in r559)
			)
	)

	(:action move-right-from-r561
		:precondition
			(and 
				(in r561)
			)
		:effect
			(and
				(not (search_again))
				(not (in r561))
				(in r562)
			)
	)


	(:action move-left-from-r561
		:precondition
			(and 
				(in r561)
			)
		:effect
			(and
				(not (search_again))
				(not (in r561))
				(in r560)
			)
	)

	(:action move-right-from-r562
		:precondition
			(and 
				(in r562)
			)
		:effect
			(and
				(not (search_again))
				(not (in r562))
				(in r563)
			)
	)


	(:action move-left-from-r562
		:precondition
			(and 
				(in r562)
			)
		:effect
			(and
				(not (search_again))
				(not (in r562))
				(in r561)
			)
	)

	(:action move-right-from-r563
		:precondition
			(and 
				(in r563)
			)
		:effect
			(and
				(not (search_again))
				(not (in r563))
				(in r564)
			)
	)


	(:action move-left-from-r563
		:precondition
			(and 
				(in r563)
			)
		:effect
			(and
				(not (search_again))
				(not (in r563))
				(in r562)
			)
	)

	(:action move-right-from-r564
		:precondition
			(and 
				(in r564)
			)
		:effect
			(and
				(not (search_again))
				(not (in r564))
				(in r565)
			)
	)


	(:action move-left-from-r564
		:precondition
			(and 
				(in r564)
			)
		:effect
			(and
				(not (search_again))
				(not (in r564))
				(in r563)
			)
	)

	(:action move-right-from-r565
		:precondition
			(and 
				(in r565)
			)
		:effect
			(and
				(not (search_again))
				(not (in r565))
				(in r566)
			)
	)


	(:action move-left-from-r565
		:precondition
			(and 
				(in r565)
			)
		:effect
			(and
				(not (search_again))
				(not (in r565))
				(in r564)
			)
	)

	(:action move-right-from-r566
		:precondition
			(and 
				(in r566)
			)
		:effect
			(and
				(not (search_again))
				(not (in r566))
				(in r567)
			)
	)


	(:action move-left-from-r566
		:precondition
			(and 
				(in r566)
			)
		:effect
			(and
				(not (search_again))
				(not (in r566))
				(in r565)
			)
	)

	(:action move-right-from-r567
		:precondition
			(and 
				(in r567)
			)
		:effect
			(and
				(not (search_again))
				(not (in r567))
				(in r568)
			)
	)


	(:action move-left-from-r567
		:precondition
			(and 
				(in r567)
			)
		:effect
			(and
				(not (search_again))
				(not (in r567))
				(in r566)
			)
	)

	(:action move-right-from-r568
		:precondition
			(and 
				(in r568)
			)
		:effect
			(and
				(not (search_again))
				(not (in r568))
				(in r569)
			)
	)


	(:action move-left-from-r568
		:precondition
			(and 
				(in r568)
			)
		:effect
			(and
				(not (search_again))
				(not (in r568))
				(in r567)
			)
	)

	(:action move-right-from-r569
		:precondition
			(and 
				(in r569)
			)
		:effect
			(and
				(not (search_again))
				(not (in r569))
				(in r570)
			)
	)


	(:action move-left-from-r569
		:precondition
			(and 
				(in r569)
			)
		:effect
			(and
				(not (search_again))
				(not (in r569))
				(in r568)
			)
	)

	(:action move-right-from-r570
		:precondition
			(and 
				(in r570)
			)
		:effect
			(and
				(not (search_again))
				(not (in r570))
				(in r571)
			)
	)


	(:action move-left-from-r570
		:precondition
			(and 
				(in r570)
			)
		:effect
			(and
				(not (search_again))
				(not (in r570))
				(in r569)
			)
	)

	(:action move-right-from-r571
		:precondition
			(and 
				(in r571)
			)
		:effect
			(and
				(not (search_again))
				(not (in r571))
				(in r572)
			)
	)


	(:action move-left-from-r571
		:precondition
			(and 
				(in r571)
			)
		:effect
			(and
				(not (search_again))
				(not (in r571))
				(in r570)
			)
	)

	(:action move-right-from-r572
		:precondition
			(and 
				(in r572)
			)
		:effect
			(and
				(not (search_again))
				(not (in r572))
				(in r573)
			)
	)


	(:action move-left-from-r572
		:precondition
			(and 
				(in r572)
			)
		:effect
			(and
				(not (search_again))
				(not (in r572))
				(in r571)
			)
	)

	(:action move-right-from-r573
		:precondition
			(and 
				(in r573)
			)
		:effect
			(and
				(not (search_again))
				(not (in r573))
				(in r574)
			)
	)


	(:action move-left-from-r573
		:precondition
			(and 
				(in r573)
			)
		:effect
			(and
				(not (search_again))
				(not (in r573))
				(in r572)
			)
	)

	(:action move-right-from-r574
		:precondition
			(and 
				(in r574)
			)
		:effect
			(and
				(not (search_again))
				(not (in r574))
				(in r575)
			)
	)


	(:action move-left-from-r574
		:precondition
			(and 
				(in r574)
			)
		:effect
			(and
				(not (search_again))
				(not (in r574))
				(in r573)
			)
	)

	(:action move-right-from-r575
		:precondition
			(and 
				(in r575)
			)
		:effect
			(and
				(not (search_again))
				(not (in r575))
				(in r576)
			)
	)


	(:action move-left-from-r575
		:precondition
			(and 
				(in r575)
			)
		:effect
			(and
				(not (search_again))
				(not (in r575))
				(in r574)
			)
	)

	(:action move-right-from-r576
		:precondition
			(and 
				(in r576)
			)
		:effect
			(and
				(not (search_again))
				(not (in r576))
				(in r577)
			)
	)


	(:action move-left-from-r576
		:precondition
			(and 
				(in r576)
			)
		:effect
			(and
				(not (search_again))
				(not (in r576))
				(in r575)
			)
	)

	(:action move-right-from-r577
		:precondition
			(and 
				(in r577)
			)
		:effect
			(and
				(not (search_again))
				(not (in r577))
				(in r578)
			)
	)


	(:action move-left-from-r577
		:precondition
			(and 
				(in r577)
			)
		:effect
			(and
				(not (search_again))
				(not (in r577))
				(in r576)
			)
	)

	(:action move-right-from-r578
		:precondition
			(and 
				(in r578)
			)
		:effect
			(and
				(not (search_again))
				(not (in r578))
				(in r579)
			)
	)


	(:action move-left-from-r578
		:precondition
			(and 
				(in r578)
			)
		:effect
			(and
				(not (search_again))
				(not (in r578))
				(in r577)
			)
	)

	(:action move-right-from-r579
		:precondition
			(and 
				(in r579)
			)
		:effect
			(and
				(not (search_again))
				(not (in r579))
				(in r580)
			)
	)


	(:action move-left-from-r579
		:precondition
			(and 
				(in r579)
			)
		:effect
			(and
				(not (search_again))
				(not (in r579))
				(in r578)
			)
	)

	(:action move-right-from-r580
		:precondition
			(and 
				(in r580)
			)
		:effect
			(and
				(not (search_again))
				(not (in r580))
				(in r581)
			)
	)


	(:action move-left-from-r580
		:precondition
			(and 
				(in r580)
			)
		:effect
			(and
				(not (search_again))
				(not (in r580))
				(in r579)
			)
	)

	(:action move-right-from-r581
		:precondition
			(and 
				(in r581)
			)
		:effect
			(and
				(not (search_again))
				(not (in r581))
				(in r582)
			)
	)


	(:action move-left-from-r581
		:precondition
			(and 
				(in r581)
			)
		:effect
			(and
				(not (search_again))
				(not (in r581))
				(in r580)
			)
	)

	(:action move-right-from-r582
		:precondition
			(and 
				(in r582)
			)
		:effect
			(and
				(not (search_again))
				(not (in r582))
				(in r583)
			)
	)


	(:action move-left-from-r582
		:precondition
			(and 
				(in r582)
			)
		:effect
			(and
				(not (search_again))
				(not (in r582))
				(in r581)
			)
	)

	(:action move-right-from-r583
		:precondition
			(and 
				(in r583)
			)
		:effect
			(and
				(not (search_again))
				(not (in r583))
				(in r584)
			)
	)


	(:action move-left-from-r583
		:precondition
			(and 
				(in r583)
			)
		:effect
			(and
				(not (search_again))
				(not (in r583))
				(in r582)
			)
	)

	(:action move-right-from-r584
		:precondition
			(and 
				(in r584)
			)
		:effect
			(and
				(not (search_again))
				(not (in r584))
				(in r585)
			)
	)


	(:action move-left-from-r584
		:precondition
			(and 
				(in r584)
			)
		:effect
			(and
				(not (search_again))
				(not (in r584))
				(in r583)
			)
	)

	(:action move-right-from-r585
		:precondition
			(and 
				(in r585)
			)
		:effect
			(and
				(not (search_again))
				(not (in r585))
				(in r586)
			)
	)


	(:action move-left-from-r585
		:precondition
			(and 
				(in r585)
			)
		:effect
			(and
				(not (search_again))
				(not (in r585))
				(in r584)
			)
	)

	(:action move-right-from-r586
		:precondition
			(and 
				(in r586)
			)
		:effect
			(and
				(not (search_again))
				(not (in r586))
				(in r587)
			)
	)


	(:action move-left-from-r586
		:precondition
			(and 
				(in r586)
			)
		:effect
			(and
				(not (search_again))
				(not (in r586))
				(in r585)
			)
	)

	(:action move-right-from-r587
		:precondition
			(and 
				(in r587)
			)
		:effect
			(and
				(not (search_again))
				(not (in r587))
				(in r588)
			)
	)


	(:action move-left-from-r587
		:precondition
			(and 
				(in r587)
			)
		:effect
			(and
				(not (search_again))
				(not (in r587))
				(in r586)
			)
	)

	(:action move-right-from-r588
		:precondition
			(and 
				(in r588)
			)
		:effect
			(and
				(not (search_again))
				(not (in r588))
				(in r589)
			)
	)


	(:action move-left-from-r588
		:precondition
			(and 
				(in r588)
			)
		:effect
			(and
				(not (search_again))
				(not (in r588))
				(in r587)
			)
	)

	(:action move-right-from-r589
		:precondition
			(and 
				(in r589)
			)
		:effect
			(and
				(not (search_again))
				(not (in r589))
				(in r590)
			)
	)


	(:action move-left-from-r589
		:precondition
			(and 
				(in r589)
			)
		:effect
			(and
				(not (search_again))
				(not (in r589))
				(in r588)
			)
	)

	(:action move-right-from-r590
		:precondition
			(and 
				(in r590)
			)
		:effect
			(and
				(not (search_again))
				(not (in r590))
				(in r591)
			)
	)


	(:action move-left-from-r590
		:precondition
			(and 
				(in r590)
			)
		:effect
			(and
				(not (search_again))
				(not (in r590))
				(in r589)
			)
	)

	(:action move-right-from-r591
		:precondition
			(and 
				(in r591)
			)
		:effect
			(and
				(not (search_again))
				(not (in r591))
				(in r592)
			)
	)


	(:action move-left-from-r591
		:precondition
			(and 
				(in r591)
			)
		:effect
			(and
				(not (search_again))
				(not (in r591))
				(in r590)
			)
	)

	(:action move-right-from-r592
		:precondition
			(and 
				(in r592)
			)
		:effect
			(and
				(not (search_again))
				(not (in r592))
				(in r593)
			)
	)


	(:action move-left-from-r592
		:precondition
			(and 
				(in r592)
			)
		:effect
			(and
				(not (search_again))
				(not (in r592))
				(in r591)
			)
	)

	(:action move-right-from-r593
		:precondition
			(and 
				(in r593)
			)
		:effect
			(and
				(not (search_again))
				(not (in r593))
				(in r594)
			)
	)


	(:action move-left-from-r593
		:precondition
			(and 
				(in r593)
			)
		:effect
			(and
				(not (search_again))
				(not (in r593))
				(in r592)
			)
	)

	(:action move-right-from-r594
		:precondition
			(and 
				(in r594)
			)
		:effect
			(and
				(not (search_again))
				(not (in r594))
				(in r595)
			)
	)


	(:action move-left-from-r594
		:precondition
			(and 
				(in r594)
			)
		:effect
			(and
				(not (search_again))
				(not (in r594))
				(in r593)
			)
	)

	(:action move-right-from-r595
		:precondition
			(and 
				(in r595)
			)
		:effect
			(and
				(not (search_again))
				(not (in r595))
				(in r596)
			)
	)


	(:action move-left-from-r595
		:precondition
			(and 
				(in r595)
			)
		:effect
			(and
				(not (search_again))
				(not (in r595))
				(in r594)
			)
	)

	(:action move-right-from-r596
		:precondition
			(and 
				(in r596)
			)
		:effect
			(and
				(not (search_again))
				(not (in r596))
				(in r597)
			)
	)


	(:action move-left-from-r596
		:precondition
			(and 
				(in r596)
			)
		:effect
			(and
				(not (search_again))
				(not (in r596))
				(in r595)
			)
	)

	(:action move-right-from-r597
		:precondition
			(and 
				(in r597)
			)
		:effect
			(and
				(not (search_again))
				(not (in r597))
				(in r598)
			)
	)


	(:action move-left-from-r597
		:precondition
			(and 
				(in r597)
			)
		:effect
			(and
				(not (search_again))
				(not (in r597))
				(in r596)
			)
	)

	(:action move-right-from-r598
		:precondition
			(and 
				(in r598)
			)
		:effect
			(and
				(not (search_again))
				(not (in r598))
				(in r599)
			)
	)


	(:action move-left-from-r598
		:precondition
			(and 
				(in r598)
			)
		:effect
			(and
				(not (search_again))
				(not (in r598))
				(in r597)
			)
	)

	(:action move-right-from-r599
		:precondition
			(and 
				(in r599)
			)
		:effect
			(and
				(not (search_again))
				(not (in r599))
				(in r600)
			)
	)


	(:action move-left-from-r599
		:precondition
			(and 
				(in r599)
			)
		:effect
			(and
				(not (search_again))
				(not (in r599))
				(in r598)
			)
	)

	(:action move-right-from-r600
		:precondition
			(and 
				(in r600)
			)
		:effect
			(and
				(not (search_again))
				(not (in r600))
				(in r601)
			)
	)


	(:action move-left-from-r600
		:precondition
			(and 
				(in r600)
			)
		:effect
			(and
				(not (search_again))
				(not (in r600))
				(in r599)
			)
	)

	(:action move-right-from-r601
		:precondition
			(and 
				(in r601)
			)
		:effect
			(and
				(not (search_again))
				(not (in r601))
				(in r602)
			)
	)


	(:action move-left-from-r601
		:precondition
			(and 
				(in r601)
			)
		:effect
			(and
				(not (search_again))
				(not (in r601))
				(in r600)
			)
	)

	(:action move-right-from-r602
		:precondition
			(and 
				(in r602)
			)
		:effect
			(and
				(not (search_again))
				(not (in r602))
				(in r603)
			)
	)


	(:action move-left-from-r602
		:precondition
			(and 
				(in r602)
			)
		:effect
			(and
				(not (search_again))
				(not (in r602))
				(in r601)
			)
	)

	(:action move-right-from-r603
		:precondition
			(and 
				(in r603)
			)
		:effect
			(and
				(not (search_again))
				(not (in r603))
				(in r604)
			)
	)


	(:action move-left-from-r603
		:precondition
			(and 
				(in r603)
			)
		:effect
			(and
				(not (search_again))
				(not (in r603))
				(in r602)
			)
	)

	(:action move-right-from-r604
		:precondition
			(and 
				(in r604)
			)
		:effect
			(and
				(not (search_again))
				(not (in r604))
				(in r605)
			)
	)


	(:action move-left-from-r604
		:precondition
			(and 
				(in r604)
			)
		:effect
			(and
				(not (search_again))
				(not (in r604))
				(in r603)
			)
	)

	(:action move-right-from-r605
		:precondition
			(and 
				(in r605)
			)
		:effect
			(and
				(not (search_again))
				(not (in r605))
				(in r606)
			)
	)


	(:action move-left-from-r605
		:precondition
			(and 
				(in r605)
			)
		:effect
			(and
				(not (search_again))
				(not (in r605))
				(in r604)
			)
	)

	(:action move-right-from-r606
		:precondition
			(and 
				(in r606)
			)
		:effect
			(and
				(not (search_again))
				(not (in r606))
				(in r607)
			)
	)


	(:action move-left-from-r606
		:precondition
			(and 
				(in r606)
			)
		:effect
			(and
				(not (search_again))
				(not (in r606))
				(in r605)
			)
	)

	(:action move-right-from-r607
		:precondition
			(and 
				(in r607)
			)
		:effect
			(and
				(not (search_again))
				(not (in r607))
				(in r608)
			)
	)


	(:action move-left-from-r607
		:precondition
			(and 
				(in r607)
			)
		:effect
			(and
				(not (search_again))
				(not (in r607))
				(in r606)
			)
	)

	(:action move-right-from-r608
		:precondition
			(and 
				(in r608)
			)
		:effect
			(and
				(not (search_again))
				(not (in r608))
				(in r609)
			)
	)


	(:action move-left-from-r608
		:precondition
			(and 
				(in r608)
			)
		:effect
			(and
				(not (search_again))
				(not (in r608))
				(in r607)
			)
	)

	(:action move-right-from-r609
		:precondition
			(and 
				(in r609)
			)
		:effect
			(and
				(not (search_again))
				(not (in r609))
				(in r610)
			)
	)


	(:action move-left-from-r609
		:precondition
			(and 
				(in r609)
			)
		:effect
			(and
				(not (search_again))
				(not (in r609))
				(in r608)
			)
	)

	(:action move-right-from-r610
		:precondition
			(and 
				(in r610)
			)
		:effect
			(and
				(not (search_again))
				(not (in r610))
				(in r611)
			)
	)


	(:action move-left-from-r610
		:precondition
			(and 
				(in r610)
			)
		:effect
			(and
				(not (search_again))
				(not (in r610))
				(in r609)
			)
	)

	(:action move-right-from-r611
		:precondition
			(and 
				(in r611)
			)
		:effect
			(and
				(not (search_again))
				(not (in r611))
				(in r612)
			)
	)


	(:action move-left-from-r611
		:precondition
			(and 
				(in r611)
			)
		:effect
			(and
				(not (search_again))
				(not (in r611))
				(in r610)
			)
	)

	(:action move-right-from-r612
		:precondition
			(and 
				(in r612)
			)
		:effect
			(and
				(not (search_again))
				(not (in r612))
				(in r613)
			)
	)


	(:action move-left-from-r612
		:precondition
			(and 
				(in r612)
			)
		:effect
			(and
				(not (search_again))
				(not (in r612))
				(in r611)
			)
	)

	(:action move-right-from-r613
		:precondition
			(and 
				(in r613)
			)
		:effect
			(and
				(not (search_again))
				(not (in r613))
				(in r614)
			)
	)


	(:action move-left-from-r613
		:precondition
			(and 
				(in r613)
			)
		:effect
			(and
				(not (search_again))
				(not (in r613))
				(in r612)
			)
	)

	(:action move-right-from-r614
		:precondition
			(and 
				(in r614)
			)
		:effect
			(and
				(not (search_again))
				(not (in r614))
				(in r615)
			)
	)


	(:action move-left-from-r614
		:precondition
			(and 
				(in r614)
			)
		:effect
			(and
				(not (search_again))
				(not (in r614))
				(in r613)
			)
	)

	(:action move-right-from-r615
		:precondition
			(and 
				(in r615)
			)
		:effect
			(and
				(not (search_again))
				(not (in r615))
				(in r616)
			)
	)


	(:action move-left-from-r615
		:precondition
			(and 
				(in r615)
			)
		:effect
			(and
				(not (search_again))
				(not (in r615))
				(in r614)
			)
	)

	(:action move-right-from-r616
		:precondition
			(and 
				(in r616)
			)
		:effect
			(and
				(not (search_again))
				(not (in r616))
				(in r617)
			)
	)


	(:action move-left-from-r616
		:precondition
			(and 
				(in r616)
			)
		:effect
			(and
				(not (search_again))
				(not (in r616))
				(in r615)
			)
	)

	(:action move-right-from-r617
		:precondition
			(and 
				(in r617)
			)
		:effect
			(and
				(not (search_again))
				(not (in r617))
				(in r618)
			)
	)


	(:action move-left-from-r617
		:precondition
			(and 
				(in r617)
			)
		:effect
			(and
				(not (search_again))
				(not (in r617))
				(in r616)
			)
	)

	(:action move-right-from-r618
		:precondition
			(and 
				(in r618)
			)
		:effect
			(and
				(not (search_again))
				(not (in r618))
				(in r619)
			)
	)


	(:action move-left-from-r618
		:precondition
			(and 
				(in r618)
			)
		:effect
			(and
				(not (search_again))
				(not (in r618))
				(in r617)
			)
	)

	(:action move-right-from-r619
		:precondition
			(and 
				(in r619)
			)
		:effect
			(and
				(not (search_again))
				(not (in r619))
				(in r620)
			)
	)


	(:action move-left-from-r619
		:precondition
			(and 
				(in r619)
			)
		:effect
			(and
				(not (search_again))
				(not (in r619))
				(in r618)
			)
	)

	(:action move-right-from-r620
		:precondition
			(and 
				(in r620)
			)
		:effect
			(and
				(not (search_again))
				(not (in r620))
				(in r621)
			)
	)


	(:action move-left-from-r620
		:precondition
			(and 
				(in r620)
			)
		:effect
			(and
				(not (search_again))
				(not (in r620))
				(in r619)
			)
	)

	(:action move-right-from-r621
		:precondition
			(and 
				(in r621)
			)
		:effect
			(and
				(not (search_again))
				(not (in r621))
				(in r622)
			)
	)


	(:action move-left-from-r621
		:precondition
			(and 
				(in r621)
			)
		:effect
			(and
				(not (search_again))
				(not (in r621))
				(in r620)
			)
	)

	(:action move-right-from-r622
		:precondition
			(and 
				(in r622)
			)
		:effect
			(and
				(not (search_again))
				(not (in r622))
				(in r623)
			)
	)


	(:action move-left-from-r622
		:precondition
			(and 
				(in r622)
			)
		:effect
			(and
				(not (search_again))
				(not (in r622))
				(in r621)
			)
	)

	(:action move-right-from-r623
		:precondition
			(and 
				(in r623)
			)
		:effect
			(and
				(not (search_again))
				(not (in r623))
				(in r624)
			)
	)


	(:action move-left-from-r623
		:precondition
			(and 
				(in r623)
			)
		:effect
			(and
				(not (search_again))
				(not (in r623))
				(in r622)
			)
	)

	(:action move-right-from-r624
		:precondition
			(and 
				(in r624)
			)
		:effect
			(and
				(not (search_again))
				(not (in r624))
				(in r625)
			)
	)


	(:action move-left-from-r624
		:precondition
			(and 
				(in r624)
			)
		:effect
			(and
				(not (search_again))
				(not (in r624))
				(in r623)
			)
	)

	(:action move-right-from-r625
		:precondition
			(and 
				(in r625)
			)
		:effect
			(and
				(not (search_again))
				(not (in r625))
				(in r626)
			)
	)


	(:action move-left-from-r625
		:precondition
			(and 
				(in r625)
			)
		:effect
			(and
				(not (search_again))
				(not (in r625))
				(in r624)
			)
	)

	(:action move-right-from-r626
		:precondition
			(and 
				(in r626)
			)
		:effect
			(and
				(not (search_again))
				(not (in r626))
				(in r627)
			)
	)


	(:action move-left-from-r626
		:precondition
			(and 
				(in r626)
			)
		:effect
			(and
				(not (search_again))
				(not (in r626))
				(in r625)
			)
	)

	(:action move-right-from-r627
		:precondition
			(and 
				(in r627)
			)
		:effect
			(and
				(not (search_again))
				(not (in r627))
				(in r628)
			)
	)


	(:action move-left-from-r627
		:precondition
			(and 
				(in r627)
			)
		:effect
			(and
				(not (search_again))
				(not (in r627))
				(in r626)
			)
	)

	(:action move-right-from-r628
		:precondition
			(and 
				(in r628)
			)
		:effect
			(and
				(not (search_again))
				(not (in r628))
				(in r629)
			)
	)


	(:action move-left-from-r628
		:precondition
			(and 
				(in r628)
			)
		:effect
			(and
				(not (search_again))
				(not (in r628))
				(in r627)
			)
	)

	(:action move-right-from-r629
		:precondition
			(and 
				(in r629)
			)
		:effect
			(and
				(not (search_again))
				(not (in r629))
				(in r630)
			)
	)


	(:action move-left-from-r629
		:precondition
			(and 
				(in r629)
			)
		:effect
			(and
				(not (search_again))
				(not (in r629))
				(in r628)
			)
	)

	(:action move-right-from-r630
		:precondition
			(and 
				(in r630)
			)
		:effect
			(and
				(not (search_again))
				(not (in r630))
				(in r631)
			)
	)


	(:action move-left-from-r630
		:precondition
			(and 
				(in r630)
			)
		:effect
			(and
				(not (search_again))
				(not (in r630))
				(in r629)
			)
	)

	(:action move-right-from-r631
		:precondition
			(and 
				(in r631)
			)
		:effect
			(and
				(not (search_again))
				(not (in r631))
				(in r632)
			)
	)


	(:action move-left-from-r631
		:precondition
			(and 
				(in r631)
			)
		:effect
			(and
				(not (search_again))
				(not (in r631))
				(in r630)
			)
	)

	(:action move-right-from-r632
		:precondition
			(and 
				(in r632)
			)
		:effect
			(and
				(not (search_again))
				(not (in r632))
				(in r633)
			)
	)


	(:action move-left-from-r632
		:precondition
			(and 
				(in r632)
			)
		:effect
			(and
				(not (search_again))
				(not (in r632))
				(in r631)
			)
	)

	(:action move-right-from-r633
		:precondition
			(and 
				(in r633)
			)
		:effect
			(and
				(not (search_again))
				(not (in r633))
				(in r634)
			)
	)


	(:action move-left-from-r633
		:precondition
			(and 
				(in r633)
			)
		:effect
			(and
				(not (search_again))
				(not (in r633))
				(in r632)
			)
	)

	(:action move-right-from-r634
		:precondition
			(and 
				(in r634)
			)
		:effect
			(and
				(not (search_again))
				(not (in r634))
				(in r635)
			)
	)


	(:action move-left-from-r634
		:precondition
			(and 
				(in r634)
			)
		:effect
			(and
				(not (search_again))
				(not (in r634))
				(in r633)
			)
	)

	(:action move-right-from-r635
		:precondition
			(and 
				(in r635)
			)
		:effect
			(and
				(not (search_again))
				(not (in r635))
				(in r636)
			)
	)


	(:action move-left-from-r635
		:precondition
			(and 
				(in r635)
			)
		:effect
			(and
				(not (search_again))
				(not (in r635))
				(in r634)
			)
	)

	(:action move-right-from-r636
		:precondition
			(and 
				(in r636)
			)
		:effect
			(and
				(not (search_again))
				(not (in r636))
				(in r637)
			)
	)


	(:action move-left-from-r636
		:precondition
			(and 
				(in r636)
			)
		:effect
			(and
				(not (search_again))
				(not (in r636))
				(in r635)
			)
	)

	(:action move-right-from-r637
		:precondition
			(and 
				(in r637)
			)
		:effect
			(and
				(not (search_again))
				(not (in r637))
				(in r638)
			)
	)


	(:action move-left-from-r637
		:precondition
			(and 
				(in r637)
			)
		:effect
			(and
				(not (search_again))
				(not (in r637))
				(in r636)
			)
	)

	(:action move-right-from-r638
		:precondition
			(and 
				(in r638)
			)
		:effect
			(and
				(not (search_again))
				(not (in r638))
				(in r639)
			)
	)


	(:action move-left-from-r638
		:precondition
			(and 
				(in r638)
			)
		:effect
			(and
				(not (search_again))
				(not (in r638))
				(in r637)
			)
	)

	(:action move-right-from-r639
		:precondition
			(and 
				(in r639)
			)
		:effect
			(and
				(not (search_again))
				(not (in r639))
				(in r640)
			)
	)


	(:action move-left-from-r639
		:precondition
			(and 
				(in r639)
			)
		:effect
			(and
				(not (search_again))
				(not (in r639))
				(in r638)
			)
	)

	(:action move-right-from-r640
		:precondition
			(and 
				(in r640)
			)
		:effect
			(and
				(not (search_again))
				(not (in r640))
				(in r641)
			)
	)


	(:action move-left-from-r640
		:precondition
			(and 
				(in r640)
			)
		:effect
			(and
				(not (search_again))
				(not (in r640))
				(in r639)
			)
	)

	(:action move-right-from-r641
		:precondition
			(and 
				(in r641)
			)
		:effect
			(and
				(not (search_again))
				(not (in r641))
				(in r642)
			)
	)


	(:action move-left-from-r641
		:precondition
			(and 
				(in r641)
			)
		:effect
			(and
				(not (search_again))
				(not (in r641))
				(in r640)
			)
	)

	(:action move-right-from-r642
		:precondition
			(and 
				(in r642)
			)
		:effect
			(and
				(not (search_again))
				(not (in r642))
				(in r643)
			)
	)


	(:action move-left-from-r642
		:precondition
			(and 
				(in r642)
			)
		:effect
			(and
				(not (search_again))
				(not (in r642))
				(in r641)
			)
	)

	(:action move-right-from-r643
		:precondition
			(and 
				(in r643)
			)
		:effect
			(and
				(not (search_again))
				(not (in r643))
				(in r644)
			)
	)


	(:action move-left-from-r643
		:precondition
			(and 
				(in r643)
			)
		:effect
			(and
				(not (search_again))
				(not (in r643))
				(in r642)
			)
	)

	(:action move-right-from-r644
		:precondition
			(and 
				(in r644)
			)
		:effect
			(and
				(not (search_again))
				(not (in r644))
				(in r645)
			)
	)


	(:action move-left-from-r644
		:precondition
			(and 
				(in r644)
			)
		:effect
			(and
				(not (search_again))
				(not (in r644))
				(in r643)
			)
	)

	(:action move-right-from-r645
		:precondition
			(and 
				(in r645)
			)
		:effect
			(and
				(not (search_again))
				(not (in r645))
				(in r646)
			)
	)


	(:action move-left-from-r645
		:precondition
			(and 
				(in r645)
			)
		:effect
			(and
				(not (search_again))
				(not (in r645))
				(in r644)
			)
	)

	(:action move-right-from-r646
		:precondition
			(and 
				(in r646)
			)
		:effect
			(and
				(not (search_again))
				(not (in r646))
				(in r647)
			)
	)


	(:action move-left-from-r646
		:precondition
			(and 
				(in r646)
			)
		:effect
			(and
				(not (search_again))
				(not (in r646))
				(in r645)
			)
	)

	(:action move-right-from-r647
		:precondition
			(and 
				(in r647)
			)
		:effect
			(and
				(not (search_again))
				(not (in r647))
				(in r648)
			)
	)


	(:action move-left-from-r647
		:precondition
			(and 
				(in r647)
			)
		:effect
			(and
				(not (search_again))
				(not (in r647))
				(in r646)
			)
	)

	(:action move-right-from-r648
		:precondition
			(and 
				(in r648)
			)
		:effect
			(and
				(not (search_again))
				(not (in r648))
				(in r649)
			)
	)


	(:action move-left-from-r648
		:precondition
			(and 
				(in r648)
			)
		:effect
			(and
				(not (search_again))
				(not (in r648))
				(in r647)
			)
	)

	(:action move-right-from-r649
		:precondition
			(and 
				(in r649)
			)
		:effect
			(and
				(not (search_again))
				(not (in r649))
				(in r650)
			)
	)


	(:action move-left-from-r649
		:precondition
			(and 
				(in r649)
			)
		:effect
			(and
				(not (search_again))
				(not (in r649))
				(in r648)
			)
	)

	(:action move-right-from-r650
		:precondition
			(and 
				(in r650)
			)
		:effect
			(and
				(not (search_again))
				(not (in r650))
				(in r651)
			)
	)


	(:action move-left-from-r650
		:precondition
			(and 
				(in r650)
			)
		:effect
			(and
				(not (search_again))
				(not (in r650))
				(in r649)
			)
	)

	(:action move-right-from-r651
		:precondition
			(and 
				(in r651)
			)
		:effect
			(and
				(not (search_again))
				(not (in r651))
				(in r652)
			)
	)


	(:action move-left-from-r651
		:precondition
			(and 
				(in r651)
			)
		:effect
			(and
				(not (search_again))
				(not (in r651))
				(in r650)
			)
	)

	(:action move-right-from-r652
		:precondition
			(and 
				(in r652)
			)
		:effect
			(and
				(not (search_again))
				(not (in r652))
				(in r653)
			)
	)


	(:action move-left-from-r652
		:precondition
			(and 
				(in r652)
			)
		:effect
			(and
				(not (search_again))
				(not (in r652))
				(in r651)
			)
	)

	(:action move-right-from-r653
		:precondition
			(and 
				(in r653)
			)
		:effect
			(and
				(not (search_again))
				(not (in r653))
				(in r654)
			)
	)


	(:action move-left-from-r653
		:precondition
			(and 
				(in r653)
			)
		:effect
			(and
				(not (search_again))
				(not (in r653))
				(in r652)
			)
	)

	(:action move-right-from-r654
		:precondition
			(and 
				(in r654)
			)
		:effect
			(and
				(not (search_again))
				(not (in r654))
				(in r655)
			)
	)


	(:action move-left-from-r654
		:precondition
			(and 
				(in r654)
			)
		:effect
			(and
				(not (search_again))
				(not (in r654))
				(in r653)
			)
	)

	(:action move-right-from-r655
		:precondition
			(and 
				(in r655)
			)
		:effect
			(and
				(not (search_again))
				(not (in r655))
				(in r656)
			)
	)


	(:action move-left-from-r655
		:precondition
			(and 
				(in r655)
			)
		:effect
			(and
				(not (search_again))
				(not (in r655))
				(in r654)
			)
	)

	(:action move-right-from-r656
		:precondition
			(and 
				(in r656)
			)
		:effect
			(and
				(not (search_again))
				(not (in r656))
				(in r657)
			)
	)


	(:action move-left-from-r656
		:precondition
			(and 
				(in r656)
			)
		:effect
			(and
				(not (search_again))
				(not (in r656))
				(in r655)
			)
	)

	(:action move-right-from-r657
		:precondition
			(and 
				(in r657)
			)
		:effect
			(and
				(not (search_again))
				(not (in r657))
				(in r658)
			)
	)


	(:action move-left-from-r657
		:precondition
			(and 
				(in r657)
			)
		:effect
			(and
				(not (search_again))
				(not (in r657))
				(in r656)
			)
	)

	(:action move-right-from-r658
		:precondition
			(and 
				(in r658)
			)
		:effect
			(and
				(not (search_again))
				(not (in r658))
				(in r659)
			)
	)


	(:action move-left-from-r658
		:precondition
			(and 
				(in r658)
			)
		:effect
			(and
				(not (search_again))
				(not (in r658))
				(in r657)
			)
	)

	(:action move-right-from-r659
		:precondition
			(and 
				(in r659)
			)
		:effect
			(and
				(not (search_again))
				(not (in r659))
				(in r660)
			)
	)


	(:action move-left-from-r659
		:precondition
			(and 
				(in r659)
			)
		:effect
			(and
				(not (search_again))
				(not (in r659))
				(in r658)
			)
	)

	(:action move-right-from-r660
		:precondition
			(and 
				(in r660)
			)
		:effect
			(and
				(not (search_again))
				(not (in r660))
				(in r661)
			)
	)


	(:action move-left-from-r660
		:precondition
			(and 
				(in r660)
			)
		:effect
			(and
				(not (search_again))
				(not (in r660))
				(in r659)
			)
	)

	(:action move-right-from-r661
		:precondition
			(and 
				(in r661)
			)
		:effect
			(and
				(not (search_again))
				(not (in r661))
				(in r662)
			)
	)


	(:action move-left-from-r661
		:precondition
			(and 
				(in r661)
			)
		:effect
			(and
				(not (search_again))
				(not (in r661))
				(in r660)
			)
	)

	(:action move-right-from-r662
		:precondition
			(and 
				(in r662)
			)
		:effect
			(and
				(not (search_again))
				(not (in r662))
				(in r663)
			)
	)


	(:action move-left-from-r662
		:precondition
			(and 
				(in r662)
			)
		:effect
			(and
				(not (search_again))
				(not (in r662))
				(in r661)
			)
	)

	(:action move-right-from-r663
		:precondition
			(and 
				(in r663)
			)
		:effect
			(and
				(not (search_again))
				(not (in r663))
				(in r664)
			)
	)


	(:action move-left-from-r663
		:precondition
			(and 
				(in r663)
			)
		:effect
			(and
				(not (search_again))
				(not (in r663))
				(in r662)
			)
	)

	(:action move-right-from-r664
		:precondition
			(and 
				(in r664)
			)
		:effect
			(and
				(not (search_again))
				(not (in r664))
				(in r665)
			)
	)


	(:action move-left-from-r664
		:precondition
			(and 
				(in r664)
			)
		:effect
			(and
				(not (search_again))
				(not (in r664))
				(in r663)
			)
	)

	(:action move-right-from-r665
		:precondition
			(and 
				(in r665)
			)
		:effect
			(and
				(not (search_again))
				(not (in r665))
				(in r666)
			)
	)


	(:action move-left-from-r665
		:precondition
			(and 
				(in r665)
			)
		:effect
			(and
				(not (search_again))
				(not (in r665))
				(in r664)
			)
	)

	(:action move-right-from-r666
		:precondition
			(and 
				(in r666)
			)
		:effect
			(and
				(not (search_again))
				(not (in r666))
				(in r667)
			)
	)


	(:action move-left-from-r666
		:precondition
			(and 
				(in r666)
			)
		:effect
			(and
				(not (search_again))
				(not (in r666))
				(in r665)
			)
	)

	(:action move-right-from-r667
		:precondition
			(and 
				(in r667)
			)
		:effect
			(and
				(not (search_again))
				(not (in r667))
				(in r668)
			)
	)


	(:action move-left-from-r667
		:precondition
			(and 
				(in r667)
			)
		:effect
			(and
				(not (search_again))
				(not (in r667))
				(in r666)
			)
	)

	(:action move-right-from-r668
		:precondition
			(and 
				(in r668)
			)
		:effect
			(and
				(not (search_again))
				(not (in r668))
				(in r669)
			)
	)


	(:action move-left-from-r668
		:precondition
			(and 
				(in r668)
			)
		:effect
			(and
				(not (search_again))
				(not (in r668))
				(in r667)
			)
	)

	(:action move-right-from-r669
		:precondition
			(and 
				(in r669)
			)
		:effect
			(and
				(not (search_again))
				(not (in r669))
				(in r670)
			)
	)


	(:action move-left-from-r669
		:precondition
			(and 
				(in r669)
			)
		:effect
			(and
				(not (search_again))
				(not (in r669))
				(in r668)
			)
	)

	(:action move-right-from-r670
		:precondition
			(and 
				(in r670)
			)
		:effect
			(and
				(not (search_again))
				(not (in r670))
				(in r671)
			)
	)


	(:action move-left-from-r670
		:precondition
			(and 
				(in r670)
			)
		:effect
			(and
				(not (search_again))
				(not (in r670))
				(in r669)
			)
	)

	(:action move-right-from-r671
		:precondition
			(and 
				(in r671)
			)
		:effect
			(and
				(not (search_again))
				(not (in r671))
				(in r672)
			)
	)


	(:action move-left-from-r671
		:precondition
			(and 
				(in r671)
			)
		:effect
			(and
				(not (search_again))
				(not (in r671))
				(in r670)
			)
	)

	(:action move-right-from-r672
		:precondition
			(and 
				(in r672)
			)
		:effect
			(and
				(not (search_again))
				(not (in r672))
				(in r673)
			)
	)


	(:action move-left-from-r672
		:precondition
			(and 
				(in r672)
			)
		:effect
			(and
				(not (search_again))
				(not (in r672))
				(in r671)
			)
	)

	(:action move-right-from-r673
		:precondition
			(and 
				(in r673)
			)
		:effect
			(and
				(not (search_again))
				(not (in r673))
				(in r674)
			)
	)


	(:action move-left-from-r673
		:precondition
			(and 
				(in r673)
			)
		:effect
			(and
				(not (search_again))
				(not (in r673))
				(in r672)
			)
	)

	(:action move-right-from-r674
		:precondition
			(and 
				(in r674)
			)
		:effect
			(and
				(not (search_again))
				(not (in r674))
				(in r675)
			)
	)


	(:action move-left-from-r674
		:precondition
			(and 
				(in r674)
			)
		:effect
			(and
				(not (search_again))
				(not (in r674))
				(in r673)
			)
	)

	(:action move-right-from-r675
		:precondition
			(and 
				(in r675)
			)
		:effect
			(and
				(not (search_again))
				(not (in r675))
				(in r676)
			)
	)


	(:action move-left-from-r675
		:precondition
			(and 
				(in r675)
			)
		:effect
			(and
				(not (search_again))
				(not (in r675))
				(in r674)
			)
	)

	(:action move-right-from-r676
		:precondition
			(and 
				(in r676)
			)
		:effect
			(and
				(not (search_again))
				(not (in r676))
				(in r677)
			)
	)


	(:action move-left-from-r676
		:precondition
			(and 
				(in r676)
			)
		:effect
			(and
				(not (search_again))
				(not (in r676))
				(in r675)
			)
	)

	(:action move-right-from-r677
		:precondition
			(and 
				(in r677)
			)
		:effect
			(and
				(not (search_again))
				(not (in r677))
				(in r678)
			)
	)


	(:action move-left-from-r677
		:precondition
			(and 
				(in r677)
			)
		:effect
			(and
				(not (search_again))
				(not (in r677))
				(in r676)
			)
	)

	(:action move-right-from-r678
		:precondition
			(and 
				(in r678)
			)
		:effect
			(and
				(not (search_again))
				(not (in r678))
				(in r679)
			)
	)


	(:action move-left-from-r678
		:precondition
			(and 
				(in r678)
			)
		:effect
			(and
				(not (search_again))
				(not (in r678))
				(in r677)
			)
	)

	(:action move-right-from-r679
		:precondition
			(and 
				(in r679)
			)
		:effect
			(and
				(not (search_again))
				(not (in r679))
				(in r680)
			)
	)


	(:action move-left-from-r679
		:precondition
			(and 
				(in r679)
			)
		:effect
			(and
				(not (search_again))
				(not (in r679))
				(in r678)
			)
	)

	(:action move-right-from-r680
		:precondition
			(and 
				(in r680)
			)
		:effect
			(and
				(not (search_again))
				(not (in r680))
				(in r681)
			)
	)


	(:action move-left-from-r680
		:precondition
			(and 
				(in r680)
			)
		:effect
			(and
				(not (search_again))
				(not (in r680))
				(in r679)
			)
	)

	(:action move-right-from-r681
		:precondition
			(and 
				(in r681)
			)
		:effect
			(and
				(not (search_again))
				(not (in r681))
				(in r682)
			)
	)


	(:action move-left-from-r681
		:precondition
			(and 
				(in r681)
			)
		:effect
			(and
				(not (search_again))
				(not (in r681))
				(in r680)
			)
	)

	(:action move-right-from-r682
		:precondition
			(and 
				(in r682)
			)
		:effect
			(and
				(not (search_again))
				(not (in r682))
				(in r683)
			)
	)


	(:action move-left-from-r682
		:precondition
			(and 
				(in r682)
			)
		:effect
			(and
				(not (search_again))
				(not (in r682))
				(in r681)
			)
	)

	(:action move-right-from-r683
		:precondition
			(and 
				(in r683)
			)
		:effect
			(and
				(not (search_again))
				(not (in r683))
				(in r684)
			)
	)


	(:action move-left-from-r683
		:precondition
			(and 
				(in r683)
			)
		:effect
			(and
				(not (search_again))
				(not (in r683))
				(in r682)
			)
	)

	(:action move-right-from-r684
		:precondition
			(and 
				(in r684)
			)
		:effect
			(and
				(not (search_again))
				(not (in r684))
				(in r685)
			)
	)


	(:action move-left-from-r684
		:precondition
			(and 
				(in r684)
			)
		:effect
			(and
				(not (search_again))
				(not (in r684))
				(in r683)
			)
	)

	(:action move-right-from-r685
		:precondition
			(and 
				(in r685)
			)
		:effect
			(and
				(not (search_again))
				(not (in r685))
				(in r686)
			)
	)


	(:action move-left-from-r685
		:precondition
			(and 
				(in r685)
			)
		:effect
			(and
				(not (search_again))
				(not (in r685))
				(in r684)
			)
	)

	(:action move-right-from-r686
		:precondition
			(and 
				(in r686)
			)
		:effect
			(and
				(not (search_again))
				(not (in r686))
				(in r687)
			)
	)


	(:action move-left-from-r686
		:precondition
			(and 
				(in r686)
			)
		:effect
			(and
				(not (search_again))
				(not (in r686))
				(in r685)
			)
	)

	(:action move-right-from-r687
		:precondition
			(and 
				(in r687)
			)
		:effect
			(and
				(not (search_again))
				(not (in r687))
				(in r688)
			)
	)


	(:action move-left-from-r687
		:precondition
			(and 
				(in r687)
			)
		:effect
			(and
				(not (search_again))
				(not (in r687))
				(in r686)
			)
	)

	(:action move-right-from-r688
		:precondition
			(and 
				(in r688)
			)
		:effect
			(and
				(not (search_again))
				(not (in r688))
				(in r689)
			)
	)


	(:action move-left-from-r688
		:precondition
			(and 
				(in r688)
			)
		:effect
			(and
				(not (search_again))
				(not (in r688))
				(in r687)
			)
	)

	(:action move-right-from-r689
		:precondition
			(and 
				(in r689)
			)
		:effect
			(and
				(not (search_again))
				(not (in r689))
				(in r690)
			)
	)


	(:action move-left-from-r689
		:precondition
			(and 
				(in r689)
			)
		:effect
			(and
				(not (search_again))
				(not (in r689))
				(in r688)
			)
	)

	(:action move-right-from-r690
		:precondition
			(and 
				(in r690)
			)
		:effect
			(and
				(not (search_again))
				(not (in r690))
				(in r691)
			)
	)


	(:action move-left-from-r690
		:precondition
			(and 
				(in r690)
			)
		:effect
			(and
				(not (search_again))
				(not (in r690))
				(in r689)
			)
	)

	(:action move-right-from-r691
		:precondition
			(and 
				(in r691)
			)
		:effect
			(and
				(not (search_again))
				(not (in r691))
				(in r692)
			)
	)


	(:action move-left-from-r691
		:precondition
			(and 
				(in r691)
			)
		:effect
			(and
				(not (search_again))
				(not (in r691))
				(in r690)
			)
	)

	(:action move-right-from-r692
		:precondition
			(and 
				(in r692)
			)
		:effect
			(and
				(not (search_again))
				(not (in r692))
				(in r693)
			)
	)


	(:action move-left-from-r692
		:precondition
			(and 
				(in r692)
			)
		:effect
			(and
				(not (search_again))
				(not (in r692))
				(in r691)
			)
	)

	(:action move-right-from-r693
		:precondition
			(and 
				(in r693)
			)
		:effect
			(and
				(not (search_again))
				(not (in r693))
				(in r694)
			)
	)


	(:action move-left-from-r693
		:precondition
			(and 
				(in r693)
			)
		:effect
			(and
				(not (search_again))
				(not (in r693))
				(in r692)
			)
	)

	(:action move-right-from-r694
		:precondition
			(and 
				(in r694)
			)
		:effect
			(and
				(not (search_again))
				(not (in r694))
				(in r695)
			)
	)


	(:action move-left-from-r694
		:precondition
			(and 
				(in r694)
			)
		:effect
			(and
				(not (search_again))
				(not (in r694))
				(in r693)
			)
	)

	(:action move-right-from-r695
		:precondition
			(and 
				(in r695)
			)
		:effect
			(and
				(not (search_again))
				(not (in r695))
				(in r696)
			)
	)


	(:action move-left-from-r695
		:precondition
			(and 
				(in r695)
			)
		:effect
			(and
				(not (search_again))
				(not (in r695))
				(in r694)
			)
	)

	(:action move-right-from-r696
		:precondition
			(and 
				(in r696)
			)
		:effect
			(and
				(not (search_again))
				(not (in r696))
				(in r697)
			)
	)


	(:action move-left-from-r696
		:precondition
			(and 
				(in r696)
			)
		:effect
			(and
				(not (search_again))
				(not (in r696))
				(in r695)
			)
	)

	(:action move-right-from-r697
		:precondition
			(and 
				(in r697)
			)
		:effect
			(and
				(not (search_again))
				(not (in r697))
				(in r698)
			)
	)


	(:action move-left-from-r697
		:precondition
			(and 
				(in r697)
			)
		:effect
			(and
				(not (search_again))
				(not (in r697))
				(in r696)
			)
	)

	(:action move-right-from-r698
		:precondition
			(and 
				(in r698)
			)
		:effect
			(and
				(not (search_again))
				(not (in r698))
				(in r699)
			)
	)


	(:action move-left-from-r698
		:precondition
			(and 
				(in r698)
			)
		:effect
			(and
				(not (search_again))
				(not (in r698))
				(in r697)
			)
	)

	(:action move-right-from-r699
		:precondition
			(and 
				(in r699)
			)
		:effect
			(and
				(not (search_again))
				(not (in r699))
				(in r700)
			)
	)


	(:action move-left-from-r699
		:precondition
			(and 
				(in r699)
			)
		:effect
			(and
				(not (search_again))
				(not (in r699))
				(in r698)
			)
	)

	(:action move-right-from-r700
		:precondition
			(and 
				(in r700)
			)
		:effect
			(and
				(not (search_again))
				(not (in r700))
				(in r701)
			)
	)


	(:action move-left-from-r700
		:precondition
			(and 
				(in r700)
			)
		:effect
			(and
				(not (search_again))
				(not (in r700))
				(in r699)
			)
	)

	(:action move-right-from-r701
		:precondition
			(and 
				(in r701)
			)
		:effect
			(and
				(not (search_again))
				(not (in r701))
				(in r702)
			)
	)


	(:action move-left-from-r701
		:precondition
			(and 
				(in r701)
			)
		:effect
			(and
				(not (search_again))
				(not (in r701))
				(in r700)
			)
	)

	(:action move-right-from-r702
		:precondition
			(and 
				(in r702)
			)
		:effect
			(and
				(not (search_again))
				(not (in r702))
				(in r703)
			)
	)


	(:action move-left-from-r702
		:precondition
			(and 
				(in r702)
			)
		:effect
			(and
				(not (search_again))
				(not (in r702))
				(in r701)
			)
	)

	(:action move-right-from-r703
		:precondition
			(and 
				(in r703)
			)
		:effect
			(and
				(not (search_again))
				(not (in r703))
				(in r704)
			)
	)


	(:action move-left-from-r703
		:precondition
			(and 
				(in r703)
			)
		:effect
			(and
				(not (search_again))
				(not (in r703))
				(in r702)
			)
	)

	(:action move-right-from-r704
		:precondition
			(and 
				(in r704)
			)
		:effect
			(and
				(not (search_again))
				(not (in r704))
				(in r705)
			)
	)


	(:action move-left-from-r704
		:precondition
			(and 
				(in r704)
			)
		:effect
			(and
				(not (search_again))
				(not (in r704))
				(in r703)
			)
	)

	(:action move-right-from-r705
		:precondition
			(and 
				(in r705)
			)
		:effect
			(and
				(not (search_again))
				(not (in r705))
				(in r706)
			)
	)


	(:action move-left-from-r705
		:precondition
			(and 
				(in r705)
			)
		:effect
			(and
				(not (search_again))
				(not (in r705))
				(in r704)
			)
	)

	(:action move-right-from-r706
		:precondition
			(and 
				(in r706)
			)
		:effect
			(and
				(not (search_again))
				(not (in r706))
				(in r707)
			)
	)


	(:action move-left-from-r706
		:precondition
			(and 
				(in r706)
			)
		:effect
			(and
				(not (search_again))
				(not (in r706))
				(in r705)
			)
	)

	(:action move-right-from-r707
		:precondition
			(and 
				(in r707)
			)
		:effect
			(and
				(not (search_again))
				(not (in r707))
				(in r708)
			)
	)


	(:action move-left-from-r707
		:precondition
			(and 
				(in r707)
			)
		:effect
			(and
				(not (search_again))
				(not (in r707))
				(in r706)
			)
	)

	(:action move-right-from-r708
		:precondition
			(and 
				(in r708)
			)
		:effect
			(and
				(not (search_again))
				(not (in r708))
				(in r709)
			)
	)


	(:action move-left-from-r708
		:precondition
			(and 
				(in r708)
			)
		:effect
			(and
				(not (search_again))
				(not (in r708))
				(in r707)
			)
	)

	(:action move-right-from-r709
		:precondition
			(and 
				(in r709)
			)
		:effect
			(and
				(not (search_again))
				(not (in r709))
				(in r710)
			)
	)


	(:action move-left-from-r709
		:precondition
			(and 
				(in r709)
			)
		:effect
			(and
				(not (search_again))
				(not (in r709))
				(in r708)
			)
	)

	(:action move-right-from-r710
		:precondition
			(and 
				(in r710)
			)
		:effect
			(and
				(not (search_again))
				(not (in r710))
				(in r711)
			)
	)


	(:action move-left-from-r710
		:precondition
			(and 
				(in r710)
			)
		:effect
			(and
				(not (search_again))
				(not (in r710))
				(in r709)
			)
	)

	(:action move-right-from-r711
		:precondition
			(and 
				(in r711)
			)
		:effect
			(and
				(not (search_again))
				(not (in r711))
				(in r712)
			)
	)


	(:action move-left-from-r711
		:precondition
			(and 
				(in r711)
			)
		:effect
			(and
				(not (search_again))
				(not (in r711))
				(in r710)
			)
	)

	(:action move-right-from-r712
		:precondition
			(and 
				(in r712)
			)
		:effect
			(and
				(not (search_again))
				(not (in r712))
				(in r713)
			)
	)


	(:action move-left-from-r712
		:precondition
			(and 
				(in r712)
			)
		:effect
			(and
				(not (search_again))
				(not (in r712))
				(in r711)
			)
	)

	(:action move-right-from-r713
		:precondition
			(and 
				(in r713)
			)
		:effect
			(and
				(not (search_again))
				(not (in r713))
				(in r714)
			)
	)


	(:action move-left-from-r713
		:precondition
			(and 
				(in r713)
			)
		:effect
			(and
				(not (search_again))
				(not (in r713))
				(in r712)
			)
	)

	(:action move-right-from-r714
		:precondition
			(and 
				(in r714)
			)
		:effect
			(and
				(not (search_again))
				(not (in r714))
				(in r715)
			)
	)


	(:action move-left-from-r714
		:precondition
			(and 
				(in r714)
			)
		:effect
			(and
				(not (search_again))
				(not (in r714))
				(in r713)
			)
	)

	(:action move-right-from-r715
		:precondition
			(and 
				(in r715)
			)
		:effect
			(and
				(not (search_again))
				(not (in r715))
				(in r716)
			)
	)


	(:action move-left-from-r715
		:precondition
			(and 
				(in r715)
			)
		:effect
			(and
				(not (search_again))
				(not (in r715))
				(in r714)
			)
	)

	(:action move-right-from-r716
		:precondition
			(and 
				(in r716)
			)
		:effect
			(and
				(not (search_again))
				(not (in r716))
				(in r717)
			)
	)


	(:action move-left-from-r716
		:precondition
			(and 
				(in r716)
			)
		:effect
			(and
				(not (search_again))
				(not (in r716))
				(in r715)
			)
	)

	(:action move-right-from-r717
		:precondition
			(and 
				(in r717)
			)
		:effect
			(and
				(not (search_again))
				(not (in r717))
				(in r718)
			)
	)


	(:action move-left-from-r717
		:precondition
			(and 
				(in r717)
			)
		:effect
			(and
				(not (search_again))
				(not (in r717))
				(in r716)
			)
	)

	(:action move-right-from-r718
		:precondition
			(and 
				(in r718)
			)
		:effect
			(and
				(not (search_again))
				(not (in r718))
				(in r719)
			)
	)


	(:action move-left-from-r718
		:precondition
			(and 
				(in r718)
			)
		:effect
			(and
				(not (search_again))
				(not (in r718))
				(in r717)
			)
	)

	(:action move-right-from-r719
		:precondition
			(and 
				(in r719)
			)
		:effect
			(and
				(not (search_again))
				(not (in r719))
				(in r720)
			)
	)


	(:action move-left-from-r719
		:precondition
			(and 
				(in r719)
			)
		:effect
			(and
				(not (search_again))
				(not (in r719))
				(in r718)
			)
	)

	(:action move-right-from-r720
		:precondition
			(and 
				(in r720)
			)
		:effect
			(and
				(not (search_again))
				(not (in r720))
				(in r721)
			)
	)


	(:action move-left-from-r720
		:precondition
			(and 
				(in r720)
			)
		:effect
			(and
				(not (search_again))
				(not (in r720))
				(in r719)
			)
	)

	(:action move-right-from-r721
		:precondition
			(and 
				(in r721)
			)
		:effect
			(and
				(not (search_again))
				(not (in r721))
				(in r722)
			)
	)


	(:action move-left-from-r721
		:precondition
			(and 
				(in r721)
			)
		:effect
			(and
				(not (search_again))
				(not (in r721))
				(in r720)
			)
	)

	(:action move-right-from-r722
		:precondition
			(and 
				(in r722)
			)
		:effect
			(and
				(not (search_again))
				(not (in r722))
				(in r723)
			)
	)


	(:action move-left-from-r722
		:precondition
			(and 
				(in r722)
			)
		:effect
			(and
				(not (search_again))
				(not (in r722))
				(in r721)
			)
	)

	(:action move-right-from-r723
		:precondition
			(and 
				(in r723)
			)
		:effect
			(and
				(not (search_again))
				(not (in r723))
				(in r724)
			)
	)


	(:action move-left-from-r723
		:precondition
			(and 
				(in r723)
			)
		:effect
			(and
				(not (search_again))
				(not (in r723))
				(in r722)
			)
	)

	(:action move-right-from-r724
		:precondition
			(and 
				(in r724)
			)
		:effect
			(and
				(not (search_again))
				(not (in r724))
				(in r725)
			)
	)


	(:action move-left-from-r724
		:precondition
			(and 
				(in r724)
			)
		:effect
			(and
				(not (search_again))
				(not (in r724))
				(in r723)
			)
	)

	(:action move-right-from-r725
		:precondition
			(and 
				(in r725)
			)
		:effect
			(and
				(not (search_again))
				(not (in r725))
				(in r726)
			)
	)


	(:action move-left-from-r725
		:precondition
			(and 
				(in r725)
			)
		:effect
			(and
				(not (search_again))
				(not (in r725))
				(in r724)
			)
	)

	(:action move-right-from-r726
		:precondition
			(and 
				(in r726)
			)
		:effect
			(and
				(not (search_again))
				(not (in r726))
				(in r727)
			)
	)


	(:action move-left-from-r726
		:precondition
			(and 
				(in r726)
			)
		:effect
			(and
				(not (search_again))
				(not (in r726))
				(in r725)
			)
	)

	(:action move-right-from-r727
		:precondition
			(and 
				(in r727)
			)
		:effect
			(and
				(not (search_again))
				(not (in r727))
				(in r728)
			)
	)


	(:action move-left-from-r727
		:precondition
			(and 
				(in r727)
			)
		:effect
			(and
				(not (search_again))
				(not (in r727))
				(in r726)
			)
	)

	(:action move-right-from-r728
		:precondition
			(and 
				(in r728)
			)
		:effect
			(and
				(not (search_again))
				(not (in r728))
				(in r729)
			)
	)


	(:action move-left-from-r728
		:precondition
			(and 
				(in r728)
			)
		:effect
			(and
				(not (search_again))
				(not (in r728))
				(in r727)
			)
	)

	(:action move-right-from-r729
		:precondition
			(and 
				(in r729)
			)
		:effect
			(and
				(not (search_again))
				(not (in r729))
				(in r730)
			)
	)


	(:action move-left-from-r729
		:precondition
			(and 
				(in r729)
			)
		:effect
			(and
				(not (search_again))
				(not (in r729))
				(in r728)
			)
	)

	(:action move-right-from-r730
		:precondition
			(and 
				(in r730)
			)
		:effect
			(and
				(not (search_again))
				(not (in r730))
				(in r731)
			)
	)


	(:action move-left-from-r730
		:precondition
			(and 
				(in r730)
			)
		:effect
			(and
				(not (search_again))
				(not (in r730))
				(in r729)
			)
	)

	(:action move-right-from-r731
		:precondition
			(and 
				(in r731)
			)
		:effect
			(and
				(not (search_again))
				(not (in r731))
				(in r732)
			)
	)


	(:action move-left-from-r731
		:precondition
			(and 
				(in r731)
			)
		:effect
			(and
				(not (search_again))
				(not (in r731))
				(in r730)
			)
	)

	(:action move-right-from-r732
		:precondition
			(and 
				(in r732)
			)
		:effect
			(and
				(not (search_again))
				(not (in r732))
				(in r733)
			)
	)


	(:action move-left-from-r732
		:precondition
			(and 
				(in r732)
			)
		:effect
			(and
				(not (search_again))
				(not (in r732))
				(in r731)
			)
	)

	(:action move-right-from-r733
		:precondition
			(and 
				(in r733)
			)
		:effect
			(and
				(not (search_again))
				(not (in r733))
				(in r734)
			)
	)


	(:action move-left-from-r733
		:precondition
			(and 
				(in r733)
			)
		:effect
			(and
				(not (search_again))
				(not (in r733))
				(in r732)
			)
	)

	(:action move-right-from-r734
		:precondition
			(and 
				(in r734)
			)
		:effect
			(and
				(not (search_again))
				(not (in r734))
				(in r735)
			)
	)


	(:action move-left-from-r734
		:precondition
			(and 
				(in r734)
			)
		:effect
			(and
				(not (search_again))
				(not (in r734))
				(in r733)
			)
	)

	(:action move-right-from-r735
		:precondition
			(and 
				(in r735)
			)
		:effect
			(and
				(not (search_again))
				(not (in r735))
				(in r736)
			)
	)


	(:action move-left-from-r735
		:precondition
			(and 
				(in r735)
			)
		:effect
			(and
				(not (search_again))
				(not (in r735))
				(in r734)
			)
	)

	(:action move-right-from-r736
		:precondition
			(and 
				(in r736)
			)
		:effect
			(and
				(not (search_again))
				(not (in r736))
				(in r737)
			)
	)


	(:action move-left-from-r736
		:precondition
			(and 
				(in r736)
			)
		:effect
			(and
				(not (search_again))
				(not (in r736))
				(in r735)
			)
	)

	(:action move-right-from-r737
		:precondition
			(and 
				(in r737)
			)
		:effect
			(and
				(not (search_again))
				(not (in r737))
				(in r738)
			)
	)


	(:action move-left-from-r737
		:precondition
			(and 
				(in r737)
			)
		:effect
			(and
				(not (search_again))
				(not (in r737))
				(in r736)
			)
	)

	(:action move-right-from-r738
		:precondition
			(and 
				(in r738)
			)
		:effect
			(and
				(not (search_again))
				(not (in r738))
				(in r739)
			)
	)


	(:action move-left-from-r738
		:precondition
			(and 
				(in r738)
			)
		:effect
			(and
				(not (search_again))
				(not (in r738))
				(in r737)
			)
	)

	(:action move-right-from-r739
		:precondition
			(and 
				(in r739)
			)
		:effect
			(and
				(not (search_again))
				(not (in r739))
				(in r740)
			)
	)


	(:action move-left-from-r739
		:precondition
			(and 
				(in r739)
			)
		:effect
			(and
				(not (search_again))
				(not (in r739))
				(in r738)
			)
	)

	(:action move-right-from-r740
		:precondition
			(and 
				(in r740)
			)
		:effect
			(and
				(not (search_again))
				(not (in r740))
				(in r741)
			)
	)


	(:action move-left-from-r740
		:precondition
			(and 
				(in r740)
			)
		:effect
			(and
				(not (search_again))
				(not (in r740))
				(in r739)
			)
	)

	(:action move-right-from-r741
		:precondition
			(and 
				(in r741)
			)
		:effect
			(and
				(not (search_again))
				(not (in r741))
				(in r742)
			)
	)


	(:action move-left-from-r741
		:precondition
			(and 
				(in r741)
			)
		:effect
			(and
				(not (search_again))
				(not (in r741))
				(in r740)
			)
	)

	(:action move-right-from-r742
		:precondition
			(and 
				(in r742)
			)
		:effect
			(and
				(not (search_again))
				(not (in r742))
				(in r743)
			)
	)


	(:action move-left-from-r742
		:precondition
			(and 
				(in r742)
			)
		:effect
			(and
				(not (search_again))
				(not (in r742))
				(in r741)
			)
	)

	(:action move-right-from-r743
		:precondition
			(and 
				(in r743)
			)
		:effect
			(and
				(not (search_again))
				(not (in r743))
				(in r744)
			)
	)


	(:action move-left-from-r743
		:precondition
			(and 
				(in r743)
			)
		:effect
			(and
				(not (search_again))
				(not (in r743))
				(in r742)
			)
	)

	(:action move-right-from-r744
		:precondition
			(and 
				(in r744)
			)
		:effect
			(and
				(not (search_again))
				(not (in r744))
				(in r745)
			)
	)


	(:action move-left-from-r744
		:precondition
			(and 
				(in r744)
			)
		:effect
			(and
				(not (search_again))
				(not (in r744))
				(in r743)
			)
	)

	(:action move-right-from-r745
		:precondition
			(and 
				(in r745)
			)
		:effect
			(and
				(not (search_again))
				(not (in r745))
				(in r746)
			)
	)


	(:action move-left-from-r745
		:precondition
			(and 
				(in r745)
			)
		:effect
			(and
				(not (search_again))
				(not (in r745))
				(in r744)
			)
	)

	(:action move-right-from-r746
		:precondition
			(and 
				(in r746)
			)
		:effect
			(and
				(not (search_again))
				(not (in r746))
				(in r747)
			)
	)


	(:action move-left-from-r746
		:precondition
			(and 
				(in r746)
			)
		:effect
			(and
				(not (search_again))
				(not (in r746))
				(in r745)
			)
	)

	(:action move-right-from-r747
		:precondition
			(and 
				(in r747)
			)
		:effect
			(and
				(not (search_again))
				(not (in r747))
				(in r748)
			)
	)


	(:action move-left-from-r747
		:precondition
			(and 
				(in r747)
			)
		:effect
			(and
				(not (search_again))
				(not (in r747))
				(in r746)
			)
	)

	(:action move-right-from-r748
		:precondition
			(and 
				(in r748)
			)
		:effect
			(and
				(not (search_again))
				(not (in r748))
				(in r749)
			)
	)


	(:action move-left-from-r748
		:precondition
			(and 
				(in r748)
			)
		:effect
			(and
				(not (search_again))
				(not (in r748))
				(in r747)
			)
	)

	(:action move-right-from-r749
		:precondition
			(and 
				(in r749)
			)
		:effect
			(and
				(not (search_again))
				(not (in r749))
				(in r750)
			)
	)


	(:action move-left-from-r749
		:precondition
			(and 
				(in r749)
			)
		:effect
			(and
				(not (search_again))
				(not (in r749))
				(in r748)
			)
	)

	(:action move-right-from-r750
		:precondition
			(and 
				(in r750)
			)
		:effect
			(and
				(not (search_again))
				(not (in r750))
				(in r751)
			)
	)


	(:action move-left-from-r750
		:precondition
			(and 
				(in r750)
			)
		:effect
			(and
				(not (search_again))
				(not (in r750))
				(in r749)
			)
	)

	(:action move-right-from-r751
		:precondition
			(and 
				(in r751)
			)
		:effect
			(and
				(not (search_again))
				(not (in r751))
				(in r752)
			)
	)


	(:action move-left-from-r751
		:precondition
			(and 
				(in r751)
			)
		:effect
			(and
				(not (search_again))
				(not (in r751))
				(in r750)
			)
	)

	(:action move-right-from-r752
		:precondition
			(and 
				(in r752)
			)
		:effect
			(and
				(not (search_again))
				(not (in r752))
				(in r753)
			)
	)


	(:action move-left-from-r752
		:precondition
			(and 
				(in r752)
			)
		:effect
			(and
				(not (search_again))
				(not (in r752))
				(in r751)
			)
	)

	(:action move-right-from-r753
		:precondition
			(and 
				(in r753)
			)
		:effect
			(and
				(not (search_again))
				(not (in r753))
				(in r754)
			)
	)


	(:action move-left-from-r753
		:precondition
			(and 
				(in r753)
			)
		:effect
			(and
				(not (search_again))
				(not (in r753))
				(in r752)
			)
	)

	(:action move-right-from-r754
		:precondition
			(and 
				(in r754)
			)
		:effect
			(and
				(not (search_again))
				(not (in r754))
				(in r755)
			)
	)


	(:action move-left-from-r754
		:precondition
			(and 
				(in r754)
			)
		:effect
			(and
				(not (search_again))
				(not (in r754))
				(in r753)
			)
	)

	(:action move-right-from-r755
		:precondition
			(and 
				(in r755)
			)
		:effect
			(and
				(not (search_again))
				(not (in r755))
				(in r756)
			)
	)


	(:action move-left-from-r755
		:precondition
			(and 
				(in r755)
			)
		:effect
			(and
				(not (search_again))
				(not (in r755))
				(in r754)
			)
	)

	(:action move-right-from-r756
		:precondition
			(and 
				(in r756)
			)
		:effect
			(and
				(not (search_again))
				(not (in r756))
				(in r757)
			)
	)


	(:action move-left-from-r756
		:precondition
			(and 
				(in r756)
			)
		:effect
			(and
				(not (search_again))
				(not (in r756))
				(in r755)
			)
	)

	(:action move-right-from-r757
		:precondition
			(and 
				(in r757)
			)
		:effect
			(and
				(not (search_again))
				(not (in r757))
				(in r758)
			)
	)


	(:action move-left-from-r757
		:precondition
			(and 
				(in r757)
			)
		:effect
			(and
				(not (search_again))
				(not (in r757))
				(in r756)
			)
	)

	(:action move-right-from-r758
		:precondition
			(and 
				(in r758)
			)
		:effect
			(and
				(not (search_again))
				(not (in r758))
				(in r759)
			)
	)


	(:action move-left-from-r758
		:precondition
			(and 
				(in r758)
			)
		:effect
			(and
				(not (search_again))
				(not (in r758))
				(in r757)
			)
	)

	(:action move-right-from-r759
		:precondition
			(and 
				(in r759)
			)
		:effect
			(and
				(not (search_again))
				(not (in r759))
				(in r760)
			)
	)


	(:action move-left-from-r759
		:precondition
			(and 
				(in r759)
			)
		:effect
			(and
				(not (search_again))
				(not (in r759))
				(in r758)
			)
	)

	(:action move-right-from-r760
		:precondition
			(and 
				(in r760)
			)
		:effect
			(and
				(not (search_again))
				(not (in r760))
				(in r761)
			)
	)


	(:action move-left-from-r760
		:precondition
			(and 
				(in r760)
			)
		:effect
			(and
				(not (search_again))
				(not (in r760))
				(in r759)
			)
	)

	(:action move-right-from-r761
		:precondition
			(and 
				(in r761)
			)
		:effect
			(and
				(not (search_again))
				(not (in r761))
				(in r762)
			)
	)


	(:action move-left-from-r761
		:precondition
			(and 
				(in r761)
			)
		:effect
			(and
				(not (search_again))
				(not (in r761))
				(in r760)
			)
	)

	(:action move-right-from-r762
		:precondition
			(and 
				(in r762)
			)
		:effect
			(and
				(not (search_again))
				(not (in r762))
				(in r763)
			)
	)


	(:action move-left-from-r762
		:precondition
			(and 
				(in r762)
			)
		:effect
			(and
				(not (search_again))
				(not (in r762))
				(in r761)
			)
	)

	(:action move-right-from-r763
		:precondition
			(and 
				(in r763)
			)
		:effect
			(and
				(not (search_again))
				(not (in r763))
				(in r764)
			)
	)


	(:action move-left-from-r763
		:precondition
			(and 
				(in r763)
			)
		:effect
			(and
				(not (search_again))
				(not (in r763))
				(in r762)
			)
	)

	(:action move-right-from-r764
		:precondition
			(and 
				(in r764)
			)
		:effect
			(and
				(not (search_again))
				(not (in r764))
				(in r765)
			)
	)


	(:action move-left-from-r764
		:precondition
			(and 
				(in r764)
			)
		:effect
			(and
				(not (search_again))
				(not (in r764))
				(in r763)
			)
	)

	(:action move-right-from-r765
		:precondition
			(and 
				(in r765)
			)
		:effect
			(and
				(not (search_again))
				(not (in r765))
				(in r766)
			)
	)


	(:action move-left-from-r765
		:precondition
			(and 
				(in r765)
			)
		:effect
			(and
				(not (search_again))
				(not (in r765))
				(in r764)
			)
	)

	(:action move-right-from-r766
		:precondition
			(and 
				(in r766)
			)
		:effect
			(and
				(not (search_again))
				(not (in r766))
				(in r767)
			)
	)


	(:action move-left-from-r766
		:precondition
			(and 
				(in r766)
			)
		:effect
			(and
				(not (search_again))
				(not (in r766))
				(in r765)
			)
	)

	(:action move-right-from-r767
		:precondition
			(and 
				(in r767)
			)
		:effect
			(and
				(not (search_again))
				(not (in r767))
				(in r768)
			)
	)


	(:action move-left-from-r767
		:precondition
			(and 
				(in r767)
			)
		:effect
			(and
				(not (search_again))
				(not (in r767))
				(in r766)
			)
	)

	(:action move-right-from-r768
		:precondition
			(and 
				(in r768)
			)
		:effect
			(and
				(not (search_again))
				(not (in r768))
				(in r769)
			)
	)


	(:action move-left-from-r768
		:precondition
			(and 
				(in r768)
			)
		:effect
			(and
				(not (search_again))
				(not (in r768))
				(in r767)
			)
	)

	(:action move-right-from-r769
		:precondition
			(and 
				(in r769)
			)
		:effect
			(and
				(not (search_again))
				(not (in r769))
				(in r770)
			)
	)


	(:action move-left-from-r769
		:precondition
			(and 
				(in r769)
			)
		:effect
			(and
				(not (search_again))
				(not (in r769))
				(in r768)
			)
	)

	(:action move-right-from-r770
		:precondition
			(and 
				(in r770)
			)
		:effect
			(and
				(not (search_again))
				(not (in r770))
				(in r771)
			)
	)


	(:action move-left-from-r770
		:precondition
			(and 
				(in r770)
			)
		:effect
			(and
				(not (search_again))
				(not (in r770))
				(in r769)
			)
	)

	(:action move-right-from-r771
		:precondition
			(and 
				(in r771)
			)
		:effect
			(and
				(not (search_again))
				(not (in r771))
				(in r772)
			)
	)


	(:action move-left-from-r771
		:precondition
			(and 
				(in r771)
			)
		:effect
			(and
				(not (search_again))
				(not (in r771))
				(in r770)
			)
	)

	(:action move-right-from-r772
		:precondition
			(and 
				(in r772)
			)
		:effect
			(and
				(not (search_again))
				(not (in r772))
				(in r773)
			)
	)


	(:action move-left-from-r772
		:precondition
			(and 
				(in r772)
			)
		:effect
			(and
				(not (search_again))
				(not (in r772))
				(in r771)
			)
	)

	(:action move-right-from-r773
		:precondition
			(and 
				(in r773)
			)
		:effect
			(and
				(not (search_again))
				(not (in r773))
				(in r774)
			)
	)


	(:action move-left-from-r773
		:precondition
			(and 
				(in r773)
			)
		:effect
			(and
				(not (search_again))
				(not (in r773))
				(in r772)
			)
	)

	(:action move-right-from-r774
		:precondition
			(and 
				(in r774)
			)
		:effect
			(and
				(not (search_again))
				(not (in r774))
				(in r775)
			)
	)


	(:action move-left-from-r774
		:precondition
			(and 
				(in r774)
			)
		:effect
			(and
				(not (search_again))
				(not (in r774))
				(in r773)
			)
	)

	(:action move-right-from-r775
		:precondition
			(and 
				(in r775)
			)
		:effect
			(and
				(not (search_again))
				(not (in r775))
				(in r776)
			)
	)


	(:action move-left-from-r775
		:precondition
			(and 
				(in r775)
			)
		:effect
			(and
				(not (search_again))
				(not (in r775))
				(in r774)
			)
	)

	(:action move-right-from-r776
		:precondition
			(and 
				(in r776)
			)
		:effect
			(and
				(not (search_again))
				(not (in r776))
				(in r777)
			)
	)


	(:action move-left-from-r776
		:precondition
			(and 
				(in r776)
			)
		:effect
			(and
				(not (search_again))
				(not (in r776))
				(in r775)
			)
	)

	(:action move-right-from-r777
		:precondition
			(and 
				(in r777)
			)
		:effect
			(and
				(not (search_again))
				(not (in r777))
				(in r778)
			)
	)


	(:action move-left-from-r777
		:precondition
			(and 
				(in r777)
			)
		:effect
			(and
				(not (search_again))
				(not (in r777))
				(in r776)
			)
	)

	(:action move-right-from-r778
		:precondition
			(and 
				(in r778)
			)
		:effect
			(and
				(not (search_again))
				(not (in r778))
				(in r779)
			)
	)


	(:action move-left-from-r778
		:precondition
			(and 
				(in r778)
			)
		:effect
			(and
				(not (search_again))
				(not (in r778))
				(in r777)
			)
	)

	(:action move-right-from-r779
		:precondition
			(and 
				(in r779)
			)
		:effect
			(and
				(not (search_again))
				(not (in r779))
				(in r780)
			)
	)


	(:action move-left-from-r779
		:precondition
			(and 
				(in r779)
			)
		:effect
			(and
				(not (search_again))
				(not (in r779))
				(in r778)
			)
	)

	(:action move-right-from-r780
		:precondition
			(and 
				(in r780)
			)
		:effect
			(and
				(not (search_again))
				(not (in r780))
				(in r781)
			)
	)


	(:action move-left-from-r780
		:precondition
			(and 
				(in r780)
			)
		:effect
			(and
				(not (search_again))
				(not (in r780))
				(in r779)
			)
	)

	(:action move-right-from-r781
		:precondition
			(and 
				(in r781)
			)
		:effect
			(and
				(not (search_again))
				(not (in r781))
				(in r782)
			)
	)


	(:action move-left-from-r781
		:precondition
			(and 
				(in r781)
			)
		:effect
			(and
				(not (search_again))
				(not (in r781))
				(in r780)
			)
	)

	(:action move-right-from-r782
		:precondition
			(and 
				(in r782)
			)
		:effect
			(and
				(not (search_again))
				(not (in r782))
				(in r783)
			)
	)


	(:action move-left-from-r782
		:precondition
			(and 
				(in r782)
			)
		:effect
			(and
				(not (search_again))
				(not (in r782))
				(in r781)
			)
	)

	(:action move-right-from-r783
		:precondition
			(and 
				(in r783)
			)
		:effect
			(and
				(not (search_again))
				(not (in r783))
				(in r784)
			)
	)


	(:action move-left-from-r783
		:precondition
			(and 
				(in r783)
			)
		:effect
			(and
				(not (search_again))
				(not (in r783))
				(in r782)
			)
	)

	(:action move-right-from-r784
		:precondition
			(and 
				(in r784)
			)
		:effect
			(and
				(not (search_again))
				(not (in r784))
				(in r785)
			)
	)


	(:action move-left-from-r784
		:precondition
			(and 
				(in r784)
			)
		:effect
			(and
				(not (search_again))
				(not (in r784))
				(in r783)
			)
	)

	(:action move-right-from-r785
		:precondition
			(and 
				(in r785)
			)
		:effect
			(and
				(not (search_again))
				(not (in r785))
				(in r786)
			)
	)


	(:action move-left-from-r785
		:precondition
			(and 
				(in r785)
			)
		:effect
			(and
				(not (search_again))
				(not (in r785))
				(in r784)
			)
	)

	(:action move-right-from-r786
		:precondition
			(and 
				(in r786)
			)
		:effect
			(and
				(not (search_again))
				(not (in r786))
				(in r787)
			)
	)


	(:action move-left-from-r786
		:precondition
			(and 
				(in r786)
			)
		:effect
			(and
				(not (search_again))
				(not (in r786))
				(in r785)
			)
	)

	(:action move-right-from-r787
		:precondition
			(and 
				(in r787)
			)
		:effect
			(and
				(not (search_again))
				(not (in r787))
				(in r788)
			)
	)


	(:action move-left-from-r787
		:precondition
			(and 
				(in r787)
			)
		:effect
			(and
				(not (search_again))
				(not (in r787))
				(in r786)
			)
	)

	(:action move-right-from-r788
		:precondition
			(and 
				(in r788)
			)
		:effect
			(and
				(not (search_again))
				(not (in r788))
				(in r789)
			)
	)


	(:action move-left-from-r788
		:precondition
			(and 
				(in r788)
			)
		:effect
			(and
				(not (search_again))
				(not (in r788))
				(in r787)
			)
	)

	(:action move-right-from-r789
		:precondition
			(and 
				(in r789)
			)
		:effect
			(and
				(not (search_again))
				(not (in r789))
				(in r790)
			)
	)


	(:action move-left-from-r789
		:precondition
			(and 
				(in r789)
			)
		:effect
			(and
				(not (search_again))
				(not (in r789))
				(in r788)
			)
	)

	(:action move-right-from-r790
		:precondition
			(and 
				(in r790)
			)
		:effect
			(and
				(not (search_again))
				(not (in r790))
				(in r791)
			)
	)


	(:action move-left-from-r790
		:precondition
			(and 
				(in r790)
			)
		:effect
			(and
				(not (search_again))
				(not (in r790))
				(in r789)
			)
	)

	(:action move-right-from-r791
		:precondition
			(and 
				(in r791)
			)
		:effect
			(and
				(not (search_again))
				(not (in r791))
				(in r792)
			)
	)


	(:action move-left-from-r791
		:precondition
			(and 
				(in r791)
			)
		:effect
			(and
				(not (search_again))
				(not (in r791))
				(in r790)
			)
	)

	(:action move-right-from-r792
		:precondition
			(and 
				(in r792)
			)
		:effect
			(and
				(not (search_again))
				(not (in r792))
				(in r793)
			)
	)


	(:action move-left-from-r792
		:precondition
			(and 
				(in r792)
			)
		:effect
			(and
				(not (search_again))
				(not (in r792))
				(in r791)
			)
	)

	(:action move-right-from-r793
		:precondition
			(and 
				(in r793)
			)
		:effect
			(and
				(not (search_again))
				(not (in r793))
				(in r794)
			)
	)


	(:action move-left-from-r793
		:precondition
			(and 
				(in r793)
			)
		:effect
			(and
				(not (search_again))
				(not (in r793))
				(in r792)
			)
	)

	(:action move-right-from-r794
		:precondition
			(and 
				(in r794)
			)
		:effect
			(and
				(not (search_again))
				(not (in r794))
				(in r795)
			)
	)


	(:action move-left-from-r794
		:precondition
			(and 
				(in r794)
			)
		:effect
			(and
				(not (search_again))
				(not (in r794))
				(in r793)
			)
	)

	(:action move-right-from-r795
		:precondition
			(and 
				(in r795)
			)
		:effect
			(and
				(not (search_again))
				(not (in r795))
				(in r796)
			)
	)


	(:action move-left-from-r795
		:precondition
			(and 
				(in r795)
			)
		:effect
			(and
				(not (search_again))
				(not (in r795))
				(in r794)
			)
	)

	(:action move-right-from-r796
		:precondition
			(and 
				(in r796)
			)
		:effect
			(and
				(not (search_again))
				(not (in r796))
				(in r797)
			)
	)


	(:action move-left-from-r796
		:precondition
			(and 
				(in r796)
			)
		:effect
			(and
				(not (search_again))
				(not (in r796))
				(in r795)
			)
	)

	(:action move-right-from-r797
		:precondition
			(and 
				(in r797)
			)
		:effect
			(and
				(not (search_again))
				(not (in r797))
				(in r798)
			)
	)


	(:action move-left-from-r797
		:precondition
			(and 
				(in r797)
			)
		:effect
			(and
				(not (search_again))
				(not (in r797))
				(in r796)
			)
	)

	(:action move-right-from-r798
		:precondition
			(and 
				(in r798)
			)
		:effect
			(and
				(not (search_again))
				(not (in r798))
				(in r799)
			)
	)


	(:action move-left-from-r798
		:precondition
			(and 
				(in r798)
			)
		:effect
			(and
				(not (search_again))
				(not (in r798))
				(in r797)
			)
	)

	(:action move-right-from-r799
		:precondition
			(and 
				(in r799)
			)
		:effect
			(and
				(not (search_again))
				(not (in r799))
				(in r800)
			)
	)


	(:action move-left-from-r799
		:precondition
			(and 
				(in r799)
			)
		:effect
			(and
				(not (search_again))
				(not (in r799))
				(in r798)
			)
	)

	(:action move-right-from-r800
		:precondition
			(and 
				(in r800)
			)
		:effect
			(and
				(not (search_again))
				(not (in r800))
				(in r801)
			)
	)


	(:action move-left-from-r800
		:precondition
			(and 
				(in r800)
			)
		:effect
			(and
				(not (search_again))
				(not (in r800))
				(in r799)
			)
	)

	(:action move-right-from-r801
		:precondition
			(and 
				(in r801)
			)
		:effect
			(and
				(not (search_again))
				(not (in r801))
				(in r802)
			)
	)


	(:action move-left-from-r801
		:precondition
			(and 
				(in r801)
			)
		:effect
			(and
				(not (search_again))
				(not (in r801))
				(in r800)
			)
	)

	(:action move-right-from-r802
		:precondition
			(and 
				(in r802)
			)
		:effect
			(and
				(not (search_again))
				(not (in r802))
				(in r803)
			)
	)


	(:action move-left-from-r802
		:precondition
			(and 
				(in r802)
			)
		:effect
			(and
				(not (search_again))
				(not (in r802))
				(in r801)
			)
	)

	(:action move-right-from-r803
		:precondition
			(and 
				(in r803)
			)
		:effect
			(and
				(not (search_again))
				(not (in r803))
				(in r804)
			)
	)


	(:action move-left-from-r803
		:precondition
			(and 
				(in r803)
			)
		:effect
			(and
				(not (search_again))
				(not (in r803))
				(in r802)
			)
	)

	(:action move-right-from-r804
		:precondition
			(and 
				(in r804)
			)
		:effect
			(and
				(not (search_again))
				(not (in r804))
				(in r805)
			)
	)


	(:action move-left-from-r804
		:precondition
			(and 
				(in r804)
			)
		:effect
			(and
				(not (search_again))
				(not (in r804))
				(in r803)
			)
	)

	(:action move-right-from-r805
		:precondition
			(and 
				(in r805)
			)
		:effect
			(and
				(not (search_again))
				(not (in r805))
				(in r806)
			)
	)


	(:action move-left-from-r805
		:precondition
			(and 
				(in r805)
			)
		:effect
			(and
				(not (search_again))
				(not (in r805))
				(in r804)
			)
	)

	(:action move-right-from-r806
		:precondition
			(and 
				(in r806)
			)
		:effect
			(and
				(not (search_again))
				(not (in r806))
				(in r807)
			)
	)


	(:action move-left-from-r806
		:precondition
			(and 
				(in r806)
			)
		:effect
			(and
				(not (search_again))
				(not (in r806))
				(in r805)
			)
	)

	(:action move-right-from-r807
		:precondition
			(and 
				(in r807)
			)
		:effect
			(and
				(not (search_again))
				(not (in r807))
				(in r808)
			)
	)


	(:action move-left-from-r807
		:precondition
			(and 
				(in r807)
			)
		:effect
			(and
				(not (search_again))
				(not (in r807))
				(in r806)
			)
	)

	(:action move-right-from-r808
		:precondition
			(and 
				(in r808)
			)
		:effect
			(and
				(not (search_again))
				(not (in r808))
				(in r809)
			)
	)


	(:action move-left-from-r808
		:precondition
			(and 
				(in r808)
			)
		:effect
			(and
				(not (search_again))
				(not (in r808))
				(in r807)
			)
	)

	(:action move-right-from-r809
		:precondition
			(and 
				(in r809)
			)
		:effect
			(and
				(not (search_again))
				(not (in r809))
				(in r810)
			)
	)


	(:action move-left-from-r809
		:precondition
			(and 
				(in r809)
			)
		:effect
			(and
				(not (search_again))
				(not (in r809))
				(in r808)
			)
	)

	(:action move-right-from-r810
		:precondition
			(and 
				(in r810)
			)
		:effect
			(and
				(not (search_again))
				(not (in r810))
				(in r811)
			)
	)


	(:action move-left-from-r810
		:precondition
			(and 
				(in r810)
			)
		:effect
			(and
				(not (search_again))
				(not (in r810))
				(in r809)
			)
	)

	(:action move-right-from-r811
		:precondition
			(and 
				(in r811)
			)
		:effect
			(and
				(not (search_again))
				(not (in r811))
				(in r812)
			)
	)


	(:action move-left-from-r811
		:precondition
			(and 
				(in r811)
			)
		:effect
			(and
				(not (search_again))
				(not (in r811))
				(in r810)
			)
	)

	(:action move-right-from-r812
		:precondition
			(and 
				(in r812)
			)
		:effect
			(and
				(not (search_again))
				(not (in r812))
				(in r813)
			)
	)


	(:action move-left-from-r812
		:precondition
			(and 
				(in r812)
			)
		:effect
			(and
				(not (search_again))
				(not (in r812))
				(in r811)
			)
	)

	(:action move-right-from-r813
		:precondition
			(and 
				(in r813)
			)
		:effect
			(and
				(not (search_again))
				(not (in r813))
				(in r814)
			)
	)


	(:action move-left-from-r813
		:precondition
			(and 
				(in r813)
			)
		:effect
			(and
				(not (search_again))
				(not (in r813))
				(in r812)
			)
	)

	(:action move-right-from-r814
		:precondition
			(and 
				(in r814)
			)
		:effect
			(and
				(not (search_again))
				(not (in r814))
				(in r815)
			)
	)


	(:action move-left-from-r814
		:precondition
			(and 
				(in r814)
			)
		:effect
			(and
				(not (search_again))
				(not (in r814))
				(in r813)
			)
	)

	(:action move-right-from-r815
		:precondition
			(and 
				(in r815)
			)
		:effect
			(and
				(not (search_again))
				(not (in r815))
				(in r816)
			)
	)


	(:action move-left-from-r815
		:precondition
			(and 
				(in r815)
			)
		:effect
			(and
				(not (search_again))
				(not (in r815))
				(in r814)
			)
	)

	(:action move-right-from-r816
		:precondition
			(and 
				(in r816)
			)
		:effect
			(and
				(not (search_again))
				(not (in r816))
				(in r817)
			)
	)


	(:action move-left-from-r816
		:precondition
			(and 
				(in r816)
			)
		:effect
			(and
				(not (search_again))
				(not (in r816))
				(in r815)
			)
	)

	(:action move-right-from-r817
		:precondition
			(and 
				(in r817)
			)
		:effect
			(and
				(not (search_again))
				(not (in r817))
				(in r818)
			)
	)


	(:action move-left-from-r817
		:precondition
			(and 
				(in r817)
			)
		:effect
			(and
				(not (search_again))
				(not (in r817))
				(in r816)
			)
	)

	(:action move-right-from-r818
		:precondition
			(and 
				(in r818)
			)
		:effect
			(and
				(not (search_again))
				(not (in r818))
				(in r819)
			)
	)


	(:action move-left-from-r818
		:precondition
			(and 
				(in r818)
			)
		:effect
			(and
				(not (search_again))
				(not (in r818))
				(in r817)
			)
	)

	(:action move-right-from-r819
		:precondition
			(and 
				(in r819)
			)
		:effect
			(and
				(not (search_again))
				(not (in r819))
				(in r820)
			)
	)


	(:action move-left-from-r819
		:precondition
			(and 
				(in r819)
			)
		:effect
			(and
				(not (search_again))
				(not (in r819))
				(in r818)
			)
	)

	(:action move-right-from-r820
		:precondition
			(and 
				(in r820)
			)
		:effect
			(and
				(not (search_again))
				(not (in r820))
				(in r821)
			)
	)


	(:action move-left-from-r820
		:precondition
			(and 
				(in r820)
			)
		:effect
			(and
				(not (search_again))
				(not (in r820))
				(in r819)
			)
	)

	(:action move-right-from-r821
		:precondition
			(and 
				(in r821)
			)
		:effect
			(and
				(not (search_again))
				(not (in r821))
				(in r822)
			)
	)


	(:action move-left-from-r821
		:precondition
			(and 
				(in r821)
			)
		:effect
			(and
				(not (search_again))
				(not (in r821))
				(in r820)
			)
	)

	(:action move-right-from-r822
		:precondition
			(and 
				(in r822)
			)
		:effect
			(and
				(not (search_again))
				(not (in r822))
				(in r823)
			)
	)


	(:action move-left-from-r822
		:precondition
			(and 
				(in r822)
			)
		:effect
			(and
				(not (search_again))
				(not (in r822))
				(in r821)
			)
	)

	(:action move-right-from-r823
		:precondition
			(and 
				(in r823)
			)
		:effect
			(and
				(not (search_again))
				(not (in r823))
				(in r824)
			)
	)


	(:action move-left-from-r823
		:precondition
			(and 
				(in r823)
			)
		:effect
			(and
				(not (search_again))
				(not (in r823))
				(in r822)
			)
	)

	(:action move-right-from-r824
		:precondition
			(and 
				(in r824)
			)
		:effect
			(and
				(not (search_again))
				(not (in r824))
				(in r825)
			)
	)


	(:action move-left-from-r824
		:precondition
			(and 
				(in r824)
			)
		:effect
			(and
				(not (search_again))
				(not (in r824))
				(in r823)
			)
	)

	(:action move-right-from-r825
		:precondition
			(and 
				(in r825)
			)
		:effect
			(and
				(not (search_again))
				(not (in r825))
				(in r826)
			)
	)


	(:action move-left-from-r825
		:precondition
			(and 
				(in r825)
			)
		:effect
			(and
				(not (search_again))
				(not (in r825))
				(in r824)
			)
	)

	(:action move-right-from-r826
		:precondition
			(and 
				(in r826)
			)
		:effect
			(and
				(not (search_again))
				(not (in r826))
				(in r827)
			)
	)


	(:action move-left-from-r826
		:precondition
			(and 
				(in r826)
			)
		:effect
			(and
				(not (search_again))
				(not (in r826))
				(in r825)
			)
	)

	(:action move-right-from-r827
		:precondition
			(and 
				(in r827)
			)
		:effect
			(and
				(not (search_again))
				(not (in r827))
				(in r828)
			)
	)


	(:action move-left-from-r827
		:precondition
			(and 
				(in r827)
			)
		:effect
			(and
				(not (search_again))
				(not (in r827))
				(in r826)
			)
	)

	(:action move-right-from-r828
		:precondition
			(and 
				(in r828)
			)
		:effect
			(and
				(not (search_again))
				(not (in r828))
				(in r829)
			)
	)


	(:action move-left-from-r828
		:precondition
			(and 
				(in r828)
			)
		:effect
			(and
				(not (search_again))
				(not (in r828))
				(in r827)
			)
	)

	(:action move-right-from-r829
		:precondition
			(and 
				(in r829)
			)
		:effect
			(and
				(not (search_again))
				(not (in r829))
				(in r830)
			)
	)


	(:action move-left-from-r829
		:precondition
			(and 
				(in r829)
			)
		:effect
			(and
				(not (search_again))
				(not (in r829))
				(in r828)
			)
	)

	(:action move-right-from-r830
		:precondition
			(and 
				(in r830)
			)
		:effect
			(and
				(not (search_again))
				(not (in r830))
				(in r831)
			)
	)


	(:action move-left-from-r830
		:precondition
			(and 
				(in r830)
			)
		:effect
			(and
				(not (search_again))
				(not (in r830))
				(in r829)
			)
	)

	(:action move-right-from-r831
		:precondition
			(and 
				(in r831)
			)
		:effect
			(and
				(not (search_again))
				(not (in r831))
				(in r832)
			)
	)


	(:action move-left-from-r831
		:precondition
			(and 
				(in r831)
			)
		:effect
			(and
				(not (search_again))
				(not (in r831))
				(in r830)
			)
	)

	(:action move-right-from-r832
		:precondition
			(and 
				(in r832)
			)
		:effect
			(and
				(not (search_again))
				(not (in r832))
				(in r833)
			)
	)


	(:action move-left-from-r832
		:precondition
			(and 
				(in r832)
			)
		:effect
			(and
				(not (search_again))
				(not (in r832))
				(in r831)
			)
	)

	(:action move-right-from-r833
		:precondition
			(and 
				(in r833)
			)
		:effect
			(and
				(not (search_again))
				(not (in r833))
				(in r834)
			)
	)


	(:action move-left-from-r833
		:precondition
			(and 
				(in r833)
			)
		:effect
			(and
				(not (search_again))
				(not (in r833))
				(in r832)
			)
	)

	(:action move-right-from-r834
		:precondition
			(and 
				(in r834)
			)
		:effect
			(and
				(not (search_again))
				(not (in r834))
				(in r835)
			)
	)


	(:action move-left-from-r834
		:precondition
			(and 
				(in r834)
			)
		:effect
			(and
				(not (search_again))
				(not (in r834))
				(in r833)
			)
	)

	(:action move-right-from-r835
		:precondition
			(and 
				(in r835)
			)
		:effect
			(and
				(not (search_again))
				(not (in r835))
				(in r836)
			)
	)


	(:action move-left-from-r835
		:precondition
			(and 
				(in r835)
			)
		:effect
			(and
				(not (search_again))
				(not (in r835))
				(in r834)
			)
	)

	(:action move-right-from-r836
		:precondition
			(and 
				(in r836)
			)
		:effect
			(and
				(not (search_again))
				(not (in r836))
				(in r837)
			)
	)


	(:action move-left-from-r836
		:precondition
			(and 
				(in r836)
			)
		:effect
			(and
				(not (search_again))
				(not (in r836))
				(in r835)
			)
	)

	(:action move-right-from-r837
		:precondition
			(and 
				(in r837)
			)
		:effect
			(and
				(not (search_again))
				(not (in r837))
				(in r838)
			)
	)


	(:action move-left-from-r837
		:precondition
			(and 
				(in r837)
			)
		:effect
			(and
				(not (search_again))
				(not (in r837))
				(in r836)
			)
	)

	(:action move-right-from-r838
		:precondition
			(and 
				(in r838)
			)
		:effect
			(and
				(not (search_again))
				(not (in r838))
				(in r839)
			)
	)


	(:action move-left-from-r838
		:precondition
			(and 
				(in r838)
			)
		:effect
			(and
				(not (search_again))
				(not (in r838))
				(in r837)
			)
	)

	(:action move-right-from-r839
		:precondition
			(and 
				(in r839)
			)
		:effect
			(and
				(not (search_again))
				(not (in r839))
				(in r840)
			)
	)


	(:action move-left-from-r839
		:precondition
			(and 
				(in r839)
			)
		:effect
			(and
				(not (search_again))
				(not (in r839))
				(in r838)
			)
	)

	(:action move-right-from-r840
		:precondition
			(and 
				(in r840)
			)
		:effect
			(and
				(not (search_again))
				(not (in r840))
				(in r841)
			)
	)


	(:action move-left-from-r840
		:precondition
			(and 
				(in r840)
			)
		:effect
			(and
				(not (search_again))
				(not (in r840))
				(in r839)
			)
	)

	(:action move-right-from-r841
		:precondition
			(and 
				(in r841)
			)
		:effect
			(and
				(not (search_again))
				(not (in r841))
				(in r842)
			)
	)


	(:action move-left-from-r841
		:precondition
			(and 
				(in r841)
			)
		:effect
			(and
				(not (search_again))
				(not (in r841))
				(in r840)
			)
	)

	(:action move-right-from-r842
		:precondition
			(and 
				(in r842)
			)
		:effect
			(and
				(not (search_again))
				(not (in r842))
				(in r843)
			)
	)


	(:action move-left-from-r842
		:precondition
			(and 
				(in r842)
			)
		:effect
			(and
				(not (search_again))
				(not (in r842))
				(in r841)
			)
	)

	(:action move-right-from-r843
		:precondition
			(and 
				(in r843)
			)
		:effect
			(and
				(not (search_again))
				(not (in r843))
				(in r844)
			)
	)


	(:action move-left-from-r843
		:precondition
			(and 
				(in r843)
			)
		:effect
			(and
				(not (search_again))
				(not (in r843))
				(in r842)
			)
	)

	(:action move-right-from-r844
		:precondition
			(and 
				(in r844)
			)
		:effect
			(and
				(not (search_again))
				(not (in r844))
				(in r845)
			)
	)


	(:action move-left-from-r844
		:precondition
			(and 
				(in r844)
			)
		:effect
			(and
				(not (search_again))
				(not (in r844))
				(in r843)
			)
	)

	(:action move-right-from-r845
		:precondition
			(and 
				(in r845)
			)
		:effect
			(and
				(not (search_again))
				(not (in r845))
				(in r846)
			)
	)


	(:action move-left-from-r845
		:precondition
			(and 
				(in r845)
			)
		:effect
			(and
				(not (search_again))
				(not (in r845))
				(in r844)
			)
	)

	(:action move-right-from-r846
		:precondition
			(and 
				(in r846)
			)
		:effect
			(and
				(not (search_again))
				(not (in r846))
				(in r847)
			)
	)


	(:action move-left-from-r846
		:precondition
			(and 
				(in r846)
			)
		:effect
			(and
				(not (search_again))
				(not (in r846))
				(in r845)
			)
	)

	(:action move-right-from-r847
		:precondition
			(and 
				(in r847)
			)
		:effect
			(and
				(not (search_again))
				(not (in r847))
				(in r848)
			)
	)


	(:action move-left-from-r847
		:precondition
			(and 
				(in r847)
			)
		:effect
			(and
				(not (search_again))
				(not (in r847))
				(in r846)
			)
	)

	(:action move-right-from-r848
		:precondition
			(and 
				(in r848)
			)
		:effect
			(and
				(not (search_again))
				(not (in r848))
				(in r849)
			)
	)


	(:action move-left-from-r848
		:precondition
			(and 
				(in r848)
			)
		:effect
			(and
				(not (search_again))
				(not (in r848))
				(in r847)
			)
	)

	(:action move-right-from-r849
		:precondition
			(and 
				(in r849)
			)
		:effect
			(and
				(not (search_again))
				(not (in r849))
				(in r850)
			)
	)


	(:action move-left-from-r849
		:precondition
			(and 
				(in r849)
			)
		:effect
			(and
				(not (search_again))
				(not (in r849))
				(in r848)
			)
	)

	(:action move-right-from-r850
		:precondition
			(and 
				(in r850)
			)
		:effect
			(and
				(not (search_again))
				(not (in r850))
				(in r851)
			)
	)


	(:action move-left-from-r850
		:precondition
			(and 
				(in r850)
			)
		:effect
			(and
				(not (search_again))
				(not (in r850))
				(in r849)
			)
	)

	(:action move-right-from-r851
		:precondition
			(and 
				(in r851)
			)
		:effect
			(and
				(not (search_again))
				(not (in r851))
				(in r852)
			)
	)


	(:action move-left-from-r851
		:precondition
			(and 
				(in r851)
			)
		:effect
			(and
				(not (search_again))
				(not (in r851))
				(in r850)
			)
	)

	(:action move-right-from-r852
		:precondition
			(and 
				(in r852)
			)
		:effect
			(and
				(not (search_again))
				(not (in r852))
				(in r853)
			)
	)


	(:action move-left-from-r852
		:precondition
			(and 
				(in r852)
			)
		:effect
			(and
				(not (search_again))
				(not (in r852))
				(in r851)
			)
	)

	(:action move-right-from-r853
		:precondition
			(and 
				(in r853)
			)
		:effect
			(and
				(not (search_again))
				(not (in r853))
				(in r854)
			)
	)


	(:action move-left-from-r853
		:precondition
			(and 
				(in r853)
			)
		:effect
			(and
				(not (search_again))
				(not (in r853))
				(in r852)
			)
	)

	(:action move-right-from-r854
		:precondition
			(and 
				(in r854)
			)
		:effect
			(and
				(not (search_again))
				(not (in r854))
				(in r855)
			)
	)


	(:action move-left-from-r854
		:precondition
			(and 
				(in r854)
			)
		:effect
			(and
				(not (search_again))
				(not (in r854))
				(in r853)
			)
	)

	(:action move-right-from-r855
		:precondition
			(and 
				(in r855)
			)
		:effect
			(and
				(not (search_again))
				(not (in r855))
				(in r856)
			)
	)


	(:action move-left-from-r855
		:precondition
			(and 
				(in r855)
			)
		:effect
			(and
				(not (search_again))
				(not (in r855))
				(in r854)
			)
	)

	(:action move-right-from-r856
		:precondition
			(and 
				(in r856)
			)
		:effect
			(and
				(not (search_again))
				(not (in r856))
				(in r857)
			)
	)


	(:action move-left-from-r856
		:precondition
			(and 
				(in r856)
			)
		:effect
			(and
				(not (search_again))
				(not (in r856))
				(in r855)
			)
	)

	(:action move-right-from-r857
		:precondition
			(and 
				(in r857)
			)
		:effect
			(and
				(not (search_again))
				(not (in r857))
				(in r858)
			)
	)


	(:action move-left-from-r857
		:precondition
			(and 
				(in r857)
			)
		:effect
			(and
				(not (search_again))
				(not (in r857))
				(in r856)
			)
	)

	(:action move-right-from-r858
		:precondition
			(and 
				(in r858)
			)
		:effect
			(and
				(not (search_again))
				(not (in r858))
				(in r859)
			)
	)


	(:action move-left-from-r858
		:precondition
			(and 
				(in r858)
			)
		:effect
			(and
				(not (search_again))
				(not (in r858))
				(in r857)
			)
	)

	(:action move-right-from-r859
		:precondition
			(and 
				(in r859)
			)
		:effect
			(and
				(not (search_again))
				(not (in r859))
				(in r860)
			)
	)


	(:action move-left-from-r859
		:precondition
			(and 
				(in r859)
			)
		:effect
			(and
				(not (search_again))
				(not (in r859))
				(in r858)
			)
	)

	(:action move-right-from-r860
		:precondition
			(and 
				(in r860)
			)
		:effect
			(and
				(not (search_again))
				(not (in r860))
				(in r861)
			)
	)


	(:action move-left-from-r860
		:precondition
			(and 
				(in r860)
			)
		:effect
			(and
				(not (search_again))
				(not (in r860))
				(in r859)
			)
	)

	(:action move-right-from-r861
		:precondition
			(and 
				(in r861)
			)
		:effect
			(and
				(not (search_again))
				(not (in r861))
				(in r862)
			)
	)


	(:action move-left-from-r861
		:precondition
			(and 
				(in r861)
			)
		:effect
			(and
				(not (search_again))
				(not (in r861))
				(in r860)
			)
	)

	(:action move-right-from-r862
		:precondition
			(and 
				(in r862)
			)
		:effect
			(and
				(not (search_again))
				(not (in r862))
				(in r863)
			)
	)


	(:action move-left-from-r862
		:precondition
			(and 
				(in r862)
			)
		:effect
			(and
				(not (search_again))
				(not (in r862))
				(in r861)
			)
	)

	(:action move-right-from-r863
		:precondition
			(and 
				(in r863)
			)
		:effect
			(and
				(not (search_again))
				(not (in r863))
				(in r864)
			)
	)


	(:action move-left-from-r863
		:precondition
			(and 
				(in r863)
			)
		:effect
			(and
				(not (search_again))
				(not (in r863))
				(in r862)
			)
	)

	(:action move-right-from-r864
		:precondition
			(and 
				(in r864)
			)
		:effect
			(and
				(not (search_again))
				(not (in r864))
				(in r865)
			)
	)


	(:action move-left-from-r864
		:precondition
			(and 
				(in r864)
			)
		:effect
			(and
				(not (search_again))
				(not (in r864))
				(in r863)
			)
	)

	(:action move-right-from-r865
		:precondition
			(and 
				(in r865)
			)
		:effect
			(and
				(not (search_again))
				(not (in r865))
				(in r866)
			)
	)


	(:action move-left-from-r865
		:precondition
			(and 
				(in r865)
			)
		:effect
			(and
				(not (search_again))
				(not (in r865))
				(in r864)
			)
	)

	(:action move-right-from-r866
		:precondition
			(and 
				(in r866)
			)
		:effect
			(and
				(not (search_again))
				(not (in r866))
				(in r867)
			)
	)


	(:action move-left-from-r866
		:precondition
			(and 
				(in r866)
			)
		:effect
			(and
				(not (search_again))
				(not (in r866))
				(in r865)
			)
	)

	(:action move-right-from-r867
		:precondition
			(and 
				(in r867)
			)
		:effect
			(and
				(not (search_again))
				(not (in r867))
				(in r868)
			)
	)


	(:action move-left-from-r867
		:precondition
			(and 
				(in r867)
			)
		:effect
			(and
				(not (search_again))
				(not (in r867))
				(in r866)
			)
	)

	(:action move-right-from-r868
		:precondition
			(and 
				(in r868)
			)
		:effect
			(and
				(not (search_again))
				(not (in r868))
				(in r869)
			)
	)


	(:action move-left-from-r868
		:precondition
			(and 
				(in r868)
			)
		:effect
			(and
				(not (search_again))
				(not (in r868))
				(in r867)
			)
	)

	(:action move-right-from-r869
		:precondition
			(and 
				(in r869)
			)
		:effect
			(and
				(not (search_again))
				(not (in r869))
				(in r870)
			)
	)


	(:action move-left-from-r869
		:precondition
			(and 
				(in r869)
			)
		:effect
			(and
				(not (search_again))
				(not (in r869))
				(in r868)
			)
	)

	(:action move-right-from-r870
		:precondition
			(and 
				(in r870)
			)
		:effect
			(and
				(not (search_again))
				(not (in r870))
				(in r871)
			)
	)


	(:action move-left-from-r870
		:precondition
			(and 
				(in r870)
			)
		:effect
			(and
				(not (search_again))
				(not (in r870))
				(in r869)
			)
	)

	(:action move-right-from-r871
		:precondition
			(and 
				(in r871)
			)
		:effect
			(and
				(not (search_again))
				(not (in r871))
				(in r872)
			)
	)


	(:action move-left-from-r871
		:precondition
			(and 
				(in r871)
			)
		:effect
			(and
				(not (search_again))
				(not (in r871))
				(in r870)
			)
	)

	(:action move-right-from-r872
		:precondition
			(and 
				(in r872)
			)
		:effect
			(and
				(not (search_again))
				(not (in r872))
				(in r873)
			)
	)


	(:action move-left-from-r872
		:precondition
			(and 
				(in r872)
			)
		:effect
			(and
				(not (search_again))
				(not (in r872))
				(in r871)
			)
	)

	(:action move-right-from-r873
		:precondition
			(and 
				(in r873)
			)
		:effect
			(and
				(not (search_again))
				(not (in r873))
				(in r874)
			)
	)


	(:action move-left-from-r873
		:precondition
			(and 
				(in r873)
			)
		:effect
			(and
				(not (search_again))
				(not (in r873))
				(in r872)
			)
	)

	(:action move-right-from-r874
		:precondition
			(and 
				(in r874)
			)
		:effect
			(and
				(not (search_again))
				(not (in r874))
				(in r875)
			)
	)


	(:action move-left-from-r874
		:precondition
			(and 
				(in r874)
			)
		:effect
			(and
				(not (search_again))
				(not (in r874))
				(in r873)
			)
	)

	(:action move-right-from-r875
		:precondition
			(and 
				(in r875)
			)
		:effect
			(and
				(not (search_again))
				(not (in r875))
				(in r876)
			)
	)


	(:action move-left-from-r875
		:precondition
			(and 
				(in r875)
			)
		:effect
			(and
				(not (search_again))
				(not (in r875))
				(in r874)
			)
	)

	(:action move-right-from-r876
		:precondition
			(and 
				(in r876)
			)
		:effect
			(and
				(not (search_again))
				(not (in r876))
				(in r877)
			)
	)


	(:action move-left-from-r876
		:precondition
			(and 
				(in r876)
			)
		:effect
			(and
				(not (search_again))
				(not (in r876))
				(in r875)
			)
	)

	(:action move-right-from-r877
		:precondition
			(and 
				(in r877)
			)
		:effect
			(and
				(not (search_again))
				(not (in r877))
				(in r878)
			)
	)


	(:action move-left-from-r877
		:precondition
			(and 
				(in r877)
			)
		:effect
			(and
				(not (search_again))
				(not (in r877))
				(in r876)
			)
	)

	(:action move-right-from-r878
		:precondition
			(and 
				(in r878)
			)
		:effect
			(and
				(not (search_again))
				(not (in r878))
				(in r879)
			)
	)


	(:action move-left-from-r878
		:precondition
			(and 
				(in r878)
			)
		:effect
			(and
				(not (search_again))
				(not (in r878))
				(in r877)
			)
	)

	(:action move-right-from-r879
		:precondition
			(and 
				(in r879)
			)
		:effect
			(and
				(not (search_again))
				(not (in r879))
				(in r880)
			)
	)


	(:action move-left-from-r879
		:precondition
			(and 
				(in r879)
			)
		:effect
			(and
				(not (search_again))
				(not (in r879))
				(in r878)
			)
	)

	(:action move-right-from-r880
		:precondition
			(and 
				(in r880)
			)
		:effect
			(and
				(not (search_again))
				(not (in r880))
				(in r881)
			)
	)


	(:action move-left-from-r880
		:precondition
			(and 
				(in r880)
			)
		:effect
			(and
				(not (search_again))
				(not (in r880))
				(in r879)
			)
	)

	(:action move-right-from-r881
		:precondition
			(and 
				(in r881)
			)
		:effect
			(and
				(not (search_again))
				(not (in r881))
				(in r882)
			)
	)


	(:action move-left-from-r881
		:precondition
			(and 
				(in r881)
			)
		:effect
			(and
				(not (search_again))
				(not (in r881))
				(in r880)
			)
	)

	(:action move-right-from-r882
		:precondition
			(and 
				(in r882)
			)
		:effect
			(and
				(not (search_again))
				(not (in r882))
				(in r883)
			)
	)


	(:action move-left-from-r882
		:precondition
			(and 
				(in r882)
			)
		:effect
			(and
				(not (search_again))
				(not (in r882))
				(in r881)
			)
	)

	(:action move-right-from-r883
		:precondition
			(and 
				(in r883)
			)
		:effect
			(and
				(not (search_again))
				(not (in r883))
				(in r884)
			)
	)


	(:action move-left-from-r883
		:precondition
			(and 
				(in r883)
			)
		:effect
			(and
				(not (search_again))
				(not (in r883))
				(in r882)
			)
	)

	(:action move-right-from-r884
		:precondition
			(and 
				(in r884)
			)
		:effect
			(and
				(not (search_again))
				(not (in r884))
				(in r885)
			)
	)


	(:action move-left-from-r884
		:precondition
			(and 
				(in r884)
			)
		:effect
			(and
				(not (search_again))
				(not (in r884))
				(in r883)
			)
	)

	(:action move-right-from-r885
		:precondition
			(and 
				(in r885)
			)
		:effect
			(and
				(not (search_again))
				(not (in r885))
				(in r886)
			)
	)


	(:action move-left-from-r885
		:precondition
			(and 
				(in r885)
			)
		:effect
			(and
				(not (search_again))
				(not (in r885))
				(in r884)
			)
	)

	(:action move-right-from-r886
		:precondition
			(and 
				(in r886)
			)
		:effect
			(and
				(not (search_again))
				(not (in r886))
				(in r887)
			)
	)


	(:action move-left-from-r886
		:precondition
			(and 
				(in r886)
			)
		:effect
			(and
				(not (search_again))
				(not (in r886))
				(in r885)
			)
	)

	(:action move-right-from-r887
		:precondition
			(and 
				(in r887)
			)
		:effect
			(and
				(not (search_again))
				(not (in r887))
				(in r888)
			)
	)


	(:action move-left-from-r887
		:precondition
			(and 
				(in r887)
			)
		:effect
			(and
				(not (search_again))
				(not (in r887))
				(in r886)
			)
	)

	(:action move-right-from-r888
		:precondition
			(and 
				(in r888)
			)
		:effect
			(and
				(not (search_again))
				(not (in r888))
				(in r889)
			)
	)


	(:action move-left-from-r888
		:precondition
			(and 
				(in r888)
			)
		:effect
			(and
				(not (search_again))
				(not (in r888))
				(in r887)
			)
	)

	(:action move-right-from-r889
		:precondition
			(and 
				(in r889)
			)
		:effect
			(and
				(not (search_again))
				(not (in r889))
				(in r890)
			)
	)


	(:action move-left-from-r889
		:precondition
			(and 
				(in r889)
			)
		:effect
			(and
				(not (search_again))
				(not (in r889))
				(in r888)
			)
	)

	(:action move-right-from-r890
		:precondition
			(and 
				(in r890)
			)
		:effect
			(and
				(not (search_again))
				(not (in r890))
				(in r891)
			)
	)


	(:action move-left-from-r890
		:precondition
			(and 
				(in r890)
			)
		:effect
			(and
				(not (search_again))
				(not (in r890))
				(in r889)
			)
	)

	(:action move-right-from-r891
		:precondition
			(and 
				(in r891)
			)
		:effect
			(and
				(not (search_again))
				(not (in r891))
				(in r892)
			)
	)


	(:action move-left-from-r891
		:precondition
			(and 
				(in r891)
			)
		:effect
			(and
				(not (search_again))
				(not (in r891))
				(in r890)
			)
	)

	(:action move-right-from-r892
		:precondition
			(and 
				(in r892)
			)
		:effect
			(and
				(not (search_again))
				(not (in r892))
				(in r893)
			)
	)


	(:action move-left-from-r892
		:precondition
			(and 
				(in r892)
			)
		:effect
			(and
				(not (search_again))
				(not (in r892))
				(in r891)
			)
	)

	(:action move-right-from-r893
		:precondition
			(and 
				(in r893)
			)
		:effect
			(and
				(not (search_again))
				(not (in r893))
				(in r894)
			)
	)


	(:action move-left-from-r893
		:precondition
			(and 
				(in r893)
			)
		:effect
			(and
				(not (search_again))
				(not (in r893))
				(in r892)
			)
	)

	(:action move-right-from-r894
		:precondition
			(and 
				(in r894)
			)
		:effect
			(and
				(not (search_again))
				(not (in r894))
				(in r895)
			)
	)


	(:action move-left-from-r894
		:precondition
			(and 
				(in r894)
			)
		:effect
			(and
				(not (search_again))
				(not (in r894))
				(in r893)
			)
	)

	(:action move-right-from-r895
		:precondition
			(and 
				(in r895)
			)
		:effect
			(and
				(not (search_again))
				(not (in r895))
				(in r896)
			)
	)


	(:action move-left-from-r895
		:precondition
			(and 
				(in r895)
			)
		:effect
			(and
				(not (search_again))
				(not (in r895))
				(in r894)
			)
	)

	(:action move-right-from-r896
		:precondition
			(and 
				(in r896)
			)
		:effect
			(and
				(not (search_again))
				(not (in r896))
				(in r897)
			)
	)


	(:action move-left-from-r896
		:precondition
			(and 
				(in r896)
			)
		:effect
			(and
				(not (search_again))
				(not (in r896))
				(in r895)
			)
	)

	(:action move-right-from-r897
		:precondition
			(and 
				(in r897)
			)
		:effect
			(and
				(not (search_again))
				(not (in r897))
				(in r898)
			)
	)


	(:action move-left-from-r897
		:precondition
			(and 
				(in r897)
			)
		:effect
			(and
				(not (search_again))
				(not (in r897))
				(in r896)
			)
	)

	(:action move-right-from-r898
		:precondition
			(and 
				(in r898)
			)
		:effect
			(and
				(not (search_again))
				(not (in r898))
				(in r899)
			)
	)


	(:action move-left-from-r898
		:precondition
			(and 
				(in r898)
			)
		:effect
			(and
				(not (search_again))
				(not (in r898))
				(in r897)
			)
	)

	(:action move-right-from-r899
		:precondition
			(and 
				(in r899)
			)
		:effect
			(and
				(not (search_again))
				(not (in r899))
				(in r900)
			)
	)


	(:action move-left-from-r899
		:precondition
			(and 
				(in r899)
			)
		:effect
			(and
				(not (search_again))
				(not (in r899))
				(in r898)
			)
	)

	(:action move-right-from-r900
		:precondition
			(and 
				(in r900)
			)
		:effect
			(and
				(not (search_again))
				(not (in r900))
				(in r901)
			)
	)


	(:action move-left-from-r900
		:precondition
			(and 
				(in r900)
			)
		:effect
			(and
				(not (search_again))
				(not (in r900))
				(in r899)
			)
	)

	(:action move-right-from-r901
		:precondition
			(and 
				(in r901)
			)
		:effect
			(and
				(not (search_again))
				(not (in r901))
				(in r902)
			)
	)


	(:action move-left-from-r901
		:precondition
			(and 
				(in r901)
			)
		:effect
			(and
				(not (search_again))
				(not (in r901))
				(in r900)
			)
	)

	(:action move-right-from-r902
		:precondition
			(and 
				(in r902)
			)
		:effect
			(and
				(not (search_again))
				(not (in r902))
				(in r903)
			)
	)


	(:action move-left-from-r902
		:precondition
			(and 
				(in r902)
			)
		:effect
			(and
				(not (search_again))
				(not (in r902))
				(in r901)
			)
	)

	(:action move-right-from-r903
		:precondition
			(and 
				(in r903)
			)
		:effect
			(and
				(not (search_again))
				(not (in r903))
				(in r904)
			)
	)


	(:action move-left-from-r903
		:precondition
			(and 
				(in r903)
			)
		:effect
			(and
				(not (search_again))
				(not (in r903))
				(in r902)
			)
	)

	(:action move-right-from-r904
		:precondition
			(and 
				(in r904)
			)
		:effect
			(and
				(not (search_again))
				(not (in r904))
				(in r905)
			)
	)


	(:action move-left-from-r904
		:precondition
			(and 
				(in r904)
			)
		:effect
			(and
				(not (search_again))
				(not (in r904))
				(in r903)
			)
	)

	(:action move-right-from-r905
		:precondition
			(and 
				(in r905)
			)
		:effect
			(and
				(not (search_again))
				(not (in r905))
				(in r906)
			)
	)


	(:action move-left-from-r905
		:precondition
			(and 
				(in r905)
			)
		:effect
			(and
				(not (search_again))
				(not (in r905))
				(in r904)
			)
	)

	(:action move-right-from-r906
		:precondition
			(and 
				(in r906)
			)
		:effect
			(and
				(not (search_again))
				(not (in r906))
				(in r907)
			)
	)


	(:action move-left-from-r906
		:precondition
			(and 
				(in r906)
			)
		:effect
			(and
				(not (search_again))
				(not (in r906))
				(in r905)
			)
	)

	(:action move-right-from-r907
		:precondition
			(and 
				(in r907)
			)
		:effect
			(and
				(not (search_again))
				(not (in r907))
				(in r908)
			)
	)


	(:action move-left-from-r907
		:precondition
			(and 
				(in r907)
			)
		:effect
			(and
				(not (search_again))
				(not (in r907))
				(in r906)
			)
	)

	(:action move-right-from-r908
		:precondition
			(and 
				(in r908)
			)
		:effect
			(and
				(not (search_again))
				(not (in r908))
				(in r909)
			)
	)


	(:action move-left-from-r908
		:precondition
			(and 
				(in r908)
			)
		:effect
			(and
				(not (search_again))
				(not (in r908))
				(in r907)
			)
	)

	(:action move-right-from-r909
		:precondition
			(and 
				(in r909)
			)
		:effect
			(and
				(not (search_again))
				(not (in r909))
				(in r910)
			)
	)


	(:action move-left-from-r909
		:precondition
			(and 
				(in r909)
			)
		:effect
			(and
				(not (search_again))
				(not (in r909))
				(in r908)
			)
	)

	(:action move-right-from-r910
		:precondition
			(and 
				(in r910)
			)
		:effect
			(and
				(not (search_again))
				(not (in r910))
				(in r911)
			)
	)


	(:action move-left-from-r910
		:precondition
			(and 
				(in r910)
			)
		:effect
			(and
				(not (search_again))
				(not (in r910))
				(in r909)
			)
	)

	(:action move-right-from-r911
		:precondition
			(and 
				(in r911)
			)
		:effect
			(and
				(not (search_again))
				(not (in r911))
				(in r912)
			)
	)


	(:action move-left-from-r911
		:precondition
			(and 
				(in r911)
			)
		:effect
			(and
				(not (search_again))
				(not (in r911))
				(in r910)
			)
	)

	(:action move-right-from-r912
		:precondition
			(and 
				(in r912)
			)
		:effect
			(and
				(not (search_again))
				(not (in r912))
				(in r913)
			)
	)


	(:action move-left-from-r912
		:precondition
			(and 
				(in r912)
			)
		:effect
			(and
				(not (search_again))
				(not (in r912))
				(in r911)
			)
	)

	(:action move-right-from-r913
		:precondition
			(and 
				(in r913)
			)
		:effect
			(and
				(not (search_again))
				(not (in r913))
				(in r914)
			)
	)


	(:action move-left-from-r913
		:precondition
			(and 
				(in r913)
			)
		:effect
			(and
				(not (search_again))
				(not (in r913))
				(in r912)
			)
	)

	(:action move-right-from-r914
		:precondition
			(and 
				(in r914)
			)
		:effect
			(and
				(not (search_again))
				(not (in r914))
				(in r915)
			)
	)


	(:action move-left-from-r914
		:precondition
			(and 
				(in r914)
			)
		:effect
			(and
				(not (search_again))
				(not (in r914))
				(in r913)
			)
	)

	(:action move-right-from-r915
		:precondition
			(and 
				(in r915)
			)
		:effect
			(and
				(not (search_again))
				(not (in r915))
				(in r916)
			)
	)


	(:action move-left-from-r915
		:precondition
			(and 
				(in r915)
			)
		:effect
			(and
				(not (search_again))
				(not (in r915))
				(in r914)
			)
	)

	(:action move-right-from-r916
		:precondition
			(and 
				(in r916)
			)
		:effect
			(and
				(not (search_again))
				(not (in r916))
				(in r917)
			)
	)


	(:action move-left-from-r916
		:precondition
			(and 
				(in r916)
			)
		:effect
			(and
				(not (search_again))
				(not (in r916))
				(in r915)
			)
	)

	(:action move-right-from-r917
		:precondition
			(and 
				(in r917)
			)
		:effect
			(and
				(not (search_again))
				(not (in r917))
				(in r918)
			)
	)


	(:action move-left-from-r917
		:precondition
			(and 
				(in r917)
			)
		:effect
			(and
				(not (search_again))
				(not (in r917))
				(in r916)
			)
	)

	(:action move-right-from-r918
		:precondition
			(and 
				(in r918)
			)
		:effect
			(and
				(not (search_again))
				(not (in r918))
				(in r919)
			)
	)


	(:action move-left-from-r918
		:precondition
			(and 
				(in r918)
			)
		:effect
			(and
				(not (search_again))
				(not (in r918))
				(in r917)
			)
	)

	(:action move-right-from-r919
		:precondition
			(and 
				(in r919)
			)
		:effect
			(and
				(not (search_again))
				(not (in r919))
				(in r920)
			)
	)


	(:action move-left-from-r919
		:precondition
			(and 
				(in r919)
			)
		:effect
			(and
				(not (search_again))
				(not (in r919))
				(in r918)
			)
	)

	(:action move-right-from-r920
		:precondition
			(and 
				(in r920)
			)
		:effect
			(and
				(not (search_again))
				(not (in r920))
				(in r921)
			)
	)


	(:action move-left-from-r920
		:precondition
			(and 
				(in r920)
			)
		:effect
			(and
				(not (search_again))
				(not (in r920))
				(in r919)
			)
	)

	(:action move-right-from-r921
		:precondition
			(and 
				(in r921)
			)
		:effect
			(and
				(not (search_again))
				(not (in r921))
				(in r922)
			)
	)


	(:action move-left-from-r921
		:precondition
			(and 
				(in r921)
			)
		:effect
			(and
				(not (search_again))
				(not (in r921))
				(in r920)
			)
	)

	(:action move-right-from-r922
		:precondition
			(and 
				(in r922)
			)
		:effect
			(and
				(not (search_again))
				(not (in r922))
				(in r923)
			)
	)


	(:action move-left-from-r922
		:precondition
			(and 
				(in r922)
			)
		:effect
			(and
				(not (search_again))
				(not (in r922))
				(in r921)
			)
	)

	(:action move-right-from-r923
		:precondition
			(and 
				(in r923)
			)
		:effect
			(and
				(not (search_again))
				(not (in r923))
				(in r924)
			)
	)


	(:action move-left-from-r923
		:precondition
			(and 
				(in r923)
			)
		:effect
			(and
				(not (search_again))
				(not (in r923))
				(in r922)
			)
	)

	(:action move-right-from-r924
		:precondition
			(and 
				(in r924)
			)
		:effect
			(and
				(not (search_again))
				(not (in r924))
				(in r925)
			)
	)


	(:action move-left-from-r924
		:precondition
			(and 
				(in r924)
			)
		:effect
			(and
				(not (search_again))
				(not (in r924))
				(in r923)
			)
	)

	(:action move-right-from-r925
		:precondition
			(and 
				(in r925)
			)
		:effect
			(and
				(not (search_again))
				(not (in r925))
				(in r926)
			)
	)


	(:action move-left-from-r925
		:precondition
			(and 
				(in r925)
			)
		:effect
			(and
				(not (search_again))
				(not (in r925))
				(in r924)
			)
	)

	(:action move-right-from-r926
		:precondition
			(and 
				(in r926)
			)
		:effect
			(and
				(not (search_again))
				(not (in r926))
				(in r927)
			)
	)


	(:action move-left-from-r926
		:precondition
			(and 
				(in r926)
			)
		:effect
			(and
				(not (search_again))
				(not (in r926))
				(in r925)
			)
	)

	(:action move-right-from-r927
		:precondition
			(and 
				(in r927)
			)
		:effect
			(and
				(not (search_again))
				(not (in r927))
				(in r928)
			)
	)


	(:action move-left-from-r927
		:precondition
			(and 
				(in r927)
			)
		:effect
			(and
				(not (search_again))
				(not (in r927))
				(in r926)
			)
	)

	(:action move-right-from-r928
		:precondition
			(and 
				(in r928)
			)
		:effect
			(and
				(not (search_again))
				(not (in r928))
				(in r929)
			)
	)


	(:action move-left-from-r928
		:precondition
			(and 
				(in r928)
			)
		:effect
			(and
				(not (search_again))
				(not (in r928))
				(in r927)
			)
	)

	(:action move-right-from-r929
		:precondition
			(and 
				(in r929)
			)
		:effect
			(and
				(not (search_again))
				(not (in r929))
				(in r930)
			)
	)


	(:action move-left-from-r929
		:precondition
			(and 
				(in r929)
			)
		:effect
			(and
				(not (search_again))
				(not (in r929))
				(in r928)
			)
	)

	(:action move-right-from-r930
		:precondition
			(and 
				(in r930)
			)
		:effect
			(and
				(not (search_again))
				(not (in r930))
				(in r931)
			)
	)


	(:action move-left-from-r930
		:precondition
			(and 
				(in r930)
			)
		:effect
			(and
				(not (search_again))
				(not (in r930))
				(in r929)
			)
	)

	(:action move-right-from-r931
		:precondition
			(and 
				(in r931)
			)
		:effect
			(and
				(not (search_again))
				(not (in r931))
				(in r932)
			)
	)


	(:action move-left-from-r931
		:precondition
			(and 
				(in r931)
			)
		:effect
			(and
				(not (search_again))
				(not (in r931))
				(in r930)
			)
	)

	(:action move-right-from-r932
		:precondition
			(and 
				(in r932)
			)
		:effect
			(and
				(not (search_again))
				(not (in r932))
				(in r933)
			)
	)


	(:action move-left-from-r932
		:precondition
			(and 
				(in r932)
			)
		:effect
			(and
				(not (search_again))
				(not (in r932))
				(in r931)
			)
	)

	(:action move-right-from-r933
		:precondition
			(and 
				(in r933)
			)
		:effect
			(and
				(not (search_again))
				(not (in r933))
				(in r934)
			)
	)


	(:action move-left-from-r933
		:precondition
			(and 
				(in r933)
			)
		:effect
			(and
				(not (search_again))
				(not (in r933))
				(in r932)
			)
	)

	(:action move-right-from-r934
		:precondition
			(and 
				(in r934)
			)
		:effect
			(and
				(not (search_again))
				(not (in r934))
				(in r935)
			)
	)


	(:action move-left-from-r934
		:precondition
			(and 
				(in r934)
			)
		:effect
			(and
				(not (search_again))
				(not (in r934))
				(in r933)
			)
	)

	(:action move-right-from-r935
		:precondition
			(and 
				(in r935)
			)
		:effect
			(and
				(not (search_again))
				(not (in r935))
				(in r936)
			)
	)


	(:action move-left-from-r935
		:precondition
			(and 
				(in r935)
			)
		:effect
			(and
				(not (search_again))
				(not (in r935))
				(in r934)
			)
	)

	(:action move-right-from-r936
		:precondition
			(and 
				(in r936)
			)
		:effect
			(and
				(not (search_again))
				(not (in r936))
				(in r937)
			)
	)


	(:action move-left-from-r936
		:precondition
			(and 
				(in r936)
			)
		:effect
			(and
				(not (search_again))
				(not (in r936))
				(in r935)
			)
	)

	(:action move-right-from-r937
		:precondition
			(and 
				(in r937)
			)
		:effect
			(and
				(not (search_again))
				(not (in r937))
				(in r938)
			)
	)


	(:action move-left-from-r937
		:precondition
			(and 
				(in r937)
			)
		:effect
			(and
				(not (search_again))
				(not (in r937))
				(in r936)
			)
	)

	(:action move-right-from-r938
		:precondition
			(and 
				(in r938)
			)
		:effect
			(and
				(not (search_again))
				(not (in r938))
				(in r939)
			)
	)


	(:action move-left-from-r938
		:precondition
			(and 
				(in r938)
			)
		:effect
			(and
				(not (search_again))
				(not (in r938))
				(in r937)
			)
	)

	(:action move-right-from-r939
		:precondition
			(and 
				(in r939)
			)
		:effect
			(and
				(not (search_again))
				(not (in r939))
				(in r940)
			)
	)


	(:action move-left-from-r939
		:precondition
			(and 
				(in r939)
			)
		:effect
			(and
				(not (search_again))
				(not (in r939))
				(in r938)
			)
	)

	(:action move-right-from-r940
		:precondition
			(and 
				(in r940)
			)
		:effect
			(and
				(not (search_again))
				(not (in r940))
				(in r941)
			)
	)


	(:action move-left-from-r940
		:precondition
			(and 
				(in r940)
			)
		:effect
			(and
				(not (search_again))
				(not (in r940))
				(in r939)
			)
	)

	(:action move-right-from-r941
		:precondition
			(and 
				(in r941)
			)
		:effect
			(and
				(not (search_again))
				(not (in r941))
				(in r942)
			)
	)


	(:action move-left-from-r941
		:precondition
			(and 
				(in r941)
			)
		:effect
			(and
				(not (search_again))
				(not (in r941))
				(in r940)
			)
	)

	(:action move-right-from-r942
		:precondition
			(and 
				(in r942)
			)
		:effect
			(and
				(not (search_again))
				(not (in r942))
				(in r943)
			)
	)


	(:action move-left-from-r942
		:precondition
			(and 
				(in r942)
			)
		:effect
			(and
				(not (search_again))
				(not (in r942))
				(in r941)
			)
	)

	(:action move-right-from-r943
		:precondition
			(and 
				(in r943)
			)
		:effect
			(and
				(not (search_again))
				(not (in r943))
				(in r944)
			)
	)


	(:action move-left-from-r943
		:precondition
			(and 
				(in r943)
			)
		:effect
			(and
				(not (search_again))
				(not (in r943))
				(in r942)
			)
	)

	(:action move-right-from-r944
		:precondition
			(and 
				(in r944)
			)
		:effect
			(and
				(not (search_again))
				(not (in r944))
				(in r945)
			)
	)


	(:action move-left-from-r944
		:precondition
			(and 
				(in r944)
			)
		:effect
			(and
				(not (search_again))
				(not (in r944))
				(in r943)
			)
	)

	(:action move-right-from-r945
		:precondition
			(and 
				(in r945)
			)
		:effect
			(and
				(not (search_again))
				(not (in r945))
				(in r946)
			)
	)


	(:action move-left-from-r945
		:precondition
			(and 
				(in r945)
			)
		:effect
			(and
				(not (search_again))
				(not (in r945))
				(in r944)
			)
	)

	(:action move-right-from-r946
		:precondition
			(and 
				(in r946)
			)
		:effect
			(and
				(not (search_again))
				(not (in r946))
				(in r947)
			)
	)


	(:action move-left-from-r946
		:precondition
			(and 
				(in r946)
			)
		:effect
			(and
				(not (search_again))
				(not (in r946))
				(in r945)
			)
	)

	(:action move-right-from-r947
		:precondition
			(and 
				(in r947)
			)
		:effect
			(and
				(not (search_again))
				(not (in r947))
				(in r948)
			)
	)


	(:action move-left-from-r947
		:precondition
			(and 
				(in r947)
			)
		:effect
			(and
				(not (search_again))
				(not (in r947))
				(in r946)
			)
	)

	(:action move-right-from-r948
		:precondition
			(and 
				(in r948)
			)
		:effect
			(and
				(not (search_again))
				(not (in r948))
				(in r949)
			)
	)


	(:action move-left-from-r948
		:precondition
			(and 
				(in r948)
			)
		:effect
			(and
				(not (search_again))
				(not (in r948))
				(in r947)
			)
	)

	(:action move-right-from-r949
		:precondition
			(and 
				(in r949)
			)
		:effect
			(and
				(not (search_again))
				(not (in r949))
				(in r950)
			)
	)


	(:action move-left-from-r949
		:precondition
			(and 
				(in r949)
			)
		:effect
			(and
				(not (search_again))
				(not (in r949))
				(in r948)
			)
	)

	(:action move-right-from-r950
		:precondition
			(and 
				(in r950)
			)
		:effect
			(and
				(not (search_again))
				(not (in r950))
				(in r951)
			)
	)


	(:action move-left-from-r950
		:precondition
			(and 
				(in r950)
			)
		:effect
			(and
				(not (search_again))
				(not (in r950))
				(in r949)
			)
	)

	(:action move-right-from-r951
		:precondition
			(and 
				(in r951)
			)
		:effect
			(and
				(not (search_again))
				(not (in r951))
				(in r952)
			)
	)


	(:action move-left-from-r951
		:precondition
			(and 
				(in r951)
			)
		:effect
			(and
				(not (search_again))
				(not (in r951))
				(in r950)
			)
	)

	(:action move-right-from-r952
		:precondition
			(and 
				(in r952)
			)
		:effect
			(and
				(not (search_again))
				(not (in r952))
				(in r953)
			)
	)


	(:action move-left-from-r952
		:precondition
			(and 
				(in r952)
			)
		:effect
			(and
				(not (search_again))
				(not (in r952))
				(in r951)
			)
	)

	(:action move-right-from-r953
		:precondition
			(and 
				(in r953)
			)
		:effect
			(and
				(not (search_again))
				(not (in r953))
				(in r954)
			)
	)


	(:action move-left-from-r953
		:precondition
			(and 
				(in r953)
			)
		:effect
			(and
				(not (search_again))
				(not (in r953))
				(in r952)
			)
	)

	(:action move-right-from-r954
		:precondition
			(and 
				(in r954)
			)
		:effect
			(and
				(not (search_again))
				(not (in r954))
				(in r955)
			)
	)


	(:action move-left-from-r954
		:precondition
			(and 
				(in r954)
			)
		:effect
			(and
				(not (search_again))
				(not (in r954))
				(in r953)
			)
	)

	(:action move-right-from-r955
		:precondition
			(and 
				(in r955)
			)
		:effect
			(and
				(not (search_again))
				(not (in r955))
				(in r956)
			)
	)


	(:action move-left-from-r955
		:precondition
			(and 
				(in r955)
			)
		:effect
			(and
				(not (search_again))
				(not (in r955))
				(in r954)
			)
	)

	(:action move-right-from-r956
		:precondition
			(and 
				(in r956)
			)
		:effect
			(and
				(not (search_again))
				(not (in r956))
				(in r957)
			)
	)


	(:action move-left-from-r956
		:precondition
			(and 
				(in r956)
			)
		:effect
			(and
				(not (search_again))
				(not (in r956))
				(in r955)
			)
	)

	(:action move-right-from-r957
		:precondition
			(and 
				(in r957)
			)
		:effect
			(and
				(not (search_again))
				(not (in r957))
				(in r958)
			)
	)


	(:action move-left-from-r957
		:precondition
			(and 
				(in r957)
			)
		:effect
			(and
				(not (search_again))
				(not (in r957))
				(in r956)
			)
	)

	(:action move-right-from-r958
		:precondition
			(and 
				(in r958)
			)
		:effect
			(and
				(not (search_again))
				(not (in r958))
				(in r959)
			)
	)


	(:action move-left-from-r958
		:precondition
			(and 
				(in r958)
			)
		:effect
			(and
				(not (search_again))
				(not (in r958))
				(in r957)
			)
	)

	(:action move-right-from-r959
		:precondition
			(and 
				(in r959)
			)
		:effect
			(and
				(not (search_again))
				(not (in r959))
				(in r960)
			)
	)


	(:action move-left-from-r959
		:precondition
			(and 
				(in r959)
			)
		:effect
			(and
				(not (search_again))
				(not (in r959))
				(in r958)
			)
	)

	(:action move-right-from-r960
		:precondition
			(and 
				(in r960)
			)
		:effect
			(and
				(not (search_again))
				(not (in r960))
				(in r961)
			)
	)


	(:action move-left-from-r960
		:precondition
			(and 
				(in r960)
			)
		:effect
			(and
				(not (search_again))
				(not (in r960))
				(in r959)
			)
	)

	(:action move-right-from-r961
		:precondition
			(and 
				(in r961)
			)
		:effect
			(and
				(not (search_again))
				(not (in r961))
				(in r962)
			)
	)


	(:action move-left-from-r961
		:precondition
			(and 
				(in r961)
			)
		:effect
			(and
				(not (search_again))
				(not (in r961))
				(in r960)
			)
	)

	(:action move-right-from-r962
		:precondition
			(and 
				(in r962)
			)
		:effect
			(and
				(not (search_again))
				(not (in r962))
				(in r963)
			)
	)


	(:action move-left-from-r962
		:precondition
			(and 
				(in r962)
			)
		:effect
			(and
				(not (search_again))
				(not (in r962))
				(in r961)
			)
	)

	(:action move-right-from-r963
		:precondition
			(and 
				(in r963)
			)
		:effect
			(and
				(not (search_again))
				(not (in r963))
				(in r964)
			)
	)


	(:action move-left-from-r963
		:precondition
			(and 
				(in r963)
			)
		:effect
			(and
				(not (search_again))
				(not (in r963))
				(in r962)
			)
	)

	(:action move-right-from-r964
		:precondition
			(and 
				(in r964)
			)
		:effect
			(and
				(not (search_again))
				(not (in r964))
				(in r965)
			)
	)


	(:action move-left-from-r964
		:precondition
			(and 
				(in r964)
			)
		:effect
			(and
				(not (search_again))
				(not (in r964))
				(in r963)
			)
	)

	(:action move-right-from-r965
		:precondition
			(and 
				(in r965)
			)
		:effect
			(and
				(not (search_again))
				(not (in r965))
				(in r966)
			)
	)


	(:action move-left-from-r965
		:precondition
			(and 
				(in r965)
			)
		:effect
			(and
				(not (search_again))
				(not (in r965))
				(in r964)
			)
	)

	(:action move-right-from-r966
		:precondition
			(and 
				(in r966)
			)
		:effect
			(and
				(not (search_again))
				(not (in r966))
				(in r967)
			)
	)


	(:action move-left-from-r966
		:precondition
			(and 
				(in r966)
			)
		:effect
			(and
				(not (search_again))
				(not (in r966))
				(in r965)
			)
	)

	(:action move-right-from-r967
		:precondition
			(and 
				(in r967)
			)
		:effect
			(and
				(not (search_again))
				(not (in r967))
				(in r968)
			)
	)


	(:action move-left-from-r967
		:precondition
			(and 
				(in r967)
			)
		:effect
			(and
				(not (search_again))
				(not (in r967))
				(in r966)
			)
	)

	(:action move-right-from-r968
		:precondition
			(and 
				(in r968)
			)
		:effect
			(and
				(not (search_again))
				(not (in r968))
				(in r969)
			)
	)


	(:action move-left-from-r968
		:precondition
			(and 
				(in r968)
			)
		:effect
			(and
				(not (search_again))
				(not (in r968))
				(in r967)
			)
	)

	(:action move-right-from-r969
		:precondition
			(and 
				(in r969)
			)
		:effect
			(and
				(not (search_again))
				(not (in r969))
				(in r970)
			)
	)


	(:action move-left-from-r969
		:precondition
			(and 
				(in r969)
			)
		:effect
			(and
				(not (search_again))
				(not (in r969))
				(in r968)
			)
	)

	(:action move-right-from-r970
		:precondition
			(and 
				(in r970)
			)
		:effect
			(and
				(not (search_again))
				(not (in r970))
				(in r971)
			)
	)


	(:action move-left-from-r970
		:precondition
			(and 
				(in r970)
			)
		:effect
			(and
				(not (search_again))
				(not (in r970))
				(in r969)
			)
	)

	(:action move-right-from-r971
		:precondition
			(and 
				(in r971)
			)
		:effect
			(and
				(not (search_again))
				(not (in r971))
				(in r972)
			)
	)


	(:action move-left-from-r971
		:precondition
			(and 
				(in r971)
			)
		:effect
			(and
				(not (search_again))
				(not (in r971))
				(in r970)
			)
	)

	(:action move-right-from-r972
		:precondition
			(and 
				(in r972)
			)
		:effect
			(and
				(not (search_again))
				(not (in r972))
				(in r973)
			)
	)


	(:action move-left-from-r972
		:precondition
			(and 
				(in r972)
			)
		:effect
			(and
				(not (search_again))
				(not (in r972))
				(in r971)
			)
	)

	(:action move-right-from-r973
		:precondition
			(and 
				(in r973)
			)
		:effect
			(and
				(not (search_again))
				(not (in r973))
				(in r974)
			)
	)


	(:action move-left-from-r973
		:precondition
			(and 
				(in r973)
			)
		:effect
			(and
				(not (search_again))
				(not (in r973))
				(in r972)
			)
	)

	(:action move-right-from-r974
		:precondition
			(and 
				(in r974)
			)
		:effect
			(and
				(not (search_again))
				(not (in r974))
				(in r975)
			)
	)


	(:action move-left-from-r974
		:precondition
			(and 
				(in r974)
			)
		:effect
			(and
				(not (search_again))
				(not (in r974))
				(in r973)
			)
	)

	(:action move-right-from-r975
		:precondition
			(and 
				(in r975)
			)
		:effect
			(and
				(not (search_again))
				(not (in r975))
				(in r976)
			)
	)


	(:action move-left-from-r975
		:precondition
			(and 
				(in r975)
			)
		:effect
			(and
				(not (search_again))
				(not (in r975))
				(in r974)
			)
	)

	(:action move-right-from-r976
		:precondition
			(and 
				(in r976)
			)
		:effect
			(and
				(not (search_again))
				(not (in r976))
				(in r977)
			)
	)


	(:action move-left-from-r976
		:precondition
			(and 
				(in r976)
			)
		:effect
			(and
				(not (search_again))
				(not (in r976))
				(in r975)
			)
	)

	(:action move-right-from-r977
		:precondition
			(and 
				(in r977)
			)
		:effect
			(and
				(not (search_again))
				(not (in r977))
				(in r978)
			)
	)


	(:action move-left-from-r977
		:precondition
			(and 
				(in r977)
			)
		:effect
			(and
				(not (search_again))
				(not (in r977))
				(in r976)
			)
	)

	(:action move-right-from-r978
		:precondition
			(and 
				(in r978)
			)
		:effect
			(and
				(not (search_again))
				(not (in r978))
				(in r979)
			)
	)


	(:action move-left-from-r978
		:precondition
			(and 
				(in r978)
			)
		:effect
			(and
				(not (search_again))
				(not (in r978))
				(in r977)
			)
	)

	(:action move-right-from-r979
		:precondition
			(and 
				(in r979)
			)
		:effect
			(and
				(not (search_again))
				(not (in r979))
				(in r980)
			)
	)


	(:action move-left-from-r979
		:precondition
			(and 
				(in r979)
			)
		:effect
			(and
				(not (search_again))
				(not (in r979))
				(in r978)
			)
	)

	(:action move-right-from-r980
		:precondition
			(and 
				(in r980)
			)
		:effect
			(and
				(not (search_again))
				(not (in r980))
				(in r981)
			)
	)


	(:action move-left-from-r980
		:precondition
			(and 
				(in r980)
			)
		:effect
			(and
				(not (search_again))
				(not (in r980))
				(in r979)
			)
	)

	(:action move-right-from-r981
		:precondition
			(and 
				(in r981)
			)
		:effect
			(and
				(not (search_again))
				(not (in r981))
				(in r982)
			)
	)


	(:action move-left-from-r981
		:precondition
			(and 
				(in r981)
			)
		:effect
			(and
				(not (search_again))
				(not (in r981))
				(in r980)
			)
	)

	(:action move-right-from-r982
		:precondition
			(and 
				(in r982)
			)
		:effect
			(and
				(not (search_again))
				(not (in r982))
				(in r983)
			)
	)


	(:action move-left-from-r982
		:precondition
			(and 
				(in r982)
			)
		:effect
			(and
				(not (search_again))
				(not (in r982))
				(in r981)
			)
	)

	(:action move-right-from-r983
		:precondition
			(and 
				(in r983)
			)
		:effect
			(and
				(not (search_again))
				(not (in r983))
				(in r984)
			)
	)


	(:action move-left-from-r983
		:precondition
			(and 
				(in r983)
			)
		:effect
			(and
				(not (search_again))
				(not (in r983))
				(in r982)
			)
	)

	(:action move-right-from-r984
		:precondition
			(and 
				(in r984)
			)
		:effect
			(and
				(not (search_again))
				(not (in r984))
				(in r985)
			)
	)


	(:action move-left-from-r984
		:precondition
			(and 
				(in r984)
			)
		:effect
			(and
				(not (search_again))
				(not (in r984))
				(in r983)
			)
	)

	(:action move-right-from-r985
		:precondition
			(and 
				(in r985)
			)
		:effect
			(and
				(not (search_again))
				(not (in r985))
				(in r986)
			)
	)


	(:action move-left-from-r985
		:precondition
			(and 
				(in r985)
			)
		:effect
			(and
				(not (search_again))
				(not (in r985))
				(in r984)
			)
	)

	(:action move-right-from-r986
		:precondition
			(and 
				(in r986)
			)
		:effect
			(and
				(not (search_again))
				(not (in r986))
				(in r987)
			)
	)


	(:action move-left-from-r986
		:precondition
			(and 
				(in r986)
			)
		:effect
			(and
				(not (search_again))
				(not (in r986))
				(in r985)
			)
	)

	(:action move-right-from-r987
		:precondition
			(and 
				(in r987)
			)
		:effect
			(and
				(not (search_again))
				(not (in r987))
				(in r988)
			)
	)


	(:action move-left-from-r987
		:precondition
			(and 
				(in r987)
			)
		:effect
			(and
				(not (search_again))
				(not (in r987))
				(in r986)
			)
	)

	(:action move-right-from-r988
		:precondition
			(and 
				(in r988)
			)
		:effect
			(and
				(not (search_again))
				(not (in r988))
				(in r989)
			)
	)


	(:action move-left-from-r988
		:precondition
			(and 
				(in r988)
			)
		:effect
			(and
				(not (search_again))
				(not (in r988))
				(in r987)
			)
	)

	(:action move-right-from-r989
		:precondition
			(and 
				(in r989)
			)
		:effect
			(and
				(not (search_again))
				(not (in r989))
				(in r990)
			)
	)


	(:action move-left-from-r989
		:precondition
			(and 
				(in r989)
			)
		:effect
			(and
				(not (search_again))
				(not (in r989))
				(in r988)
			)
	)

	(:action move-right-from-r990
		:precondition
			(and 
				(in r990)
			)
		:effect
			(and
				(not (search_again))
				(not (in r990))
				(in r991)
			)
	)


	(:action move-left-from-r990
		:precondition
			(and 
				(in r990)
			)
		:effect
			(and
				(not (search_again))
				(not (in r990))
				(in r989)
			)
	)

	(:action move-right-from-r991
		:precondition
			(and 
				(in r991)
			)
		:effect
			(and
				(not (search_again))
				(not (in r991))
				(in r992)
			)
	)


	(:action move-left-from-r991
		:precondition
			(and 
				(in r991)
			)
		:effect
			(and
				(not (search_again))
				(not (in r991))
				(in r990)
			)
	)

	(:action move-right-from-r992
		:precondition
			(and 
				(in r992)
			)
		:effect
			(and
				(not (search_again))
				(not (in r992))
				(in r993)
			)
	)


	(:action move-left-from-r992
		:precondition
			(and 
				(in r992)
			)
		:effect
			(and
				(not (search_again))
				(not (in r992))
				(in r991)
			)
	)

	(:action move-right-from-r993
		:precondition
			(and 
				(in r993)
			)
		:effect
			(and
				(not (search_again))
				(not (in r993))
				(in r994)
			)
	)


	(:action move-left-from-r993
		:precondition
			(and 
				(in r993)
			)
		:effect
			(and
				(not (search_again))
				(not (in r993))
				(in r992)
			)
	)

	(:action move-right-from-r994
		:precondition
			(and 
				(in r994)
			)
		:effect
			(and
				(not (search_again))
				(not (in r994))
				(in r995)
			)
	)


	(:action move-left-from-r994
		:precondition
			(and 
				(in r994)
			)
		:effect
			(and
				(not (search_again))
				(not (in r994))
				(in r993)
			)
	)

	(:action move-right-from-r995
		:precondition
			(and 
				(in r995)
			)
		:effect
			(and
				(not (search_again))
				(not (in r995))
				(in r996)
			)
	)


	(:action move-left-from-r995
		:precondition
			(and 
				(in r995)
			)
		:effect
			(and
				(not (search_again))
				(not (in r995))
				(in r994)
			)
	)

	(:action move-right-from-r996
		:precondition
			(and 
				(in r996)
			)
		:effect
			(and
				(not (search_again))
				(not (in r996))
				(in r997)
			)
	)


	(:action move-left-from-r996
		:precondition
			(and 
				(in r996)
			)
		:effect
			(and
				(not (search_again))
				(not (in r996))
				(in r995)
			)
	)

	(:action move-right-from-r997
		:precondition
			(and 
				(in r997)
			)
		:effect
			(and
				(not (search_again))
				(not (in r997))
				(in r998)
			)
	)


	(:action move-left-from-r997
		:precondition
			(and 
				(in r997)
			)
		:effect
			(and
				(not (search_again))
				(not (in r997))
				(in r996)
			)
	)

	(:action move-right-from-r998
		:precondition
			(and 
				(in r998)
			)
		:effect
			(and
				(not (search_again))
				(not (in r998))
				(in r999)
			)
	)


	(:action move-left-from-r998
		:precondition
			(and 
				(in r998)
			)
		:effect
			(and
				(not (search_again))
				(not (in r998))
				(in r997)
			)
	)

	(:action move-right-from-r999
		:precondition
			(and 
				(in r999)
			)
		:effect
			(and
				(not (search_again))
				(not (in r999))
				(in r1000)
			)
	)


	(:action move-left-from-r999
		:precondition
			(and 
				(in r999)
			)
		:effect
			(and
				(not (search_again))
				(not (in r999))
				(in r998)
			)
	)

	(:action move-right-from-r1000
		:precondition
			(and 
				(in r1000)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1000))
				(in r1001)
			)
	)


	(:action move-left-from-r1000
		:precondition
			(and 
				(in r1000)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1000))
				(in r999)
			)
	)

	(:action move-right-from-r1001
		:precondition
			(and 
				(in r1001)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1001))
				(in r1002)
			)
	)


	(:action move-left-from-r1001
		:precondition
			(and 
				(in r1001)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1001))
				(in r1000)
			)
	)

	(:action move-right-from-r1002
		:precondition
			(and 
				(in r1002)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1002))
				(in r1003)
			)
	)


	(:action move-left-from-r1002
		:precondition
			(and 
				(in r1002)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1002))
				(in r1001)
			)
	)

	(:action move-right-from-r1003
		:precondition
			(and 
				(in r1003)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1003))
				(in r1004)
			)
	)


	(:action move-left-from-r1003
		:precondition
			(and 
				(in r1003)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1003))
				(in r1002)
			)
	)

	(:action move-right-from-r1004
		:precondition
			(and 
				(in r1004)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1004))
				(in r1005)
			)
	)


	(:action move-left-from-r1004
		:precondition
			(and 
				(in r1004)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1004))
				(in r1003)
			)
	)

	(:action move-right-from-r1005
		:precondition
			(and 
				(in r1005)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1005))
				(in r1006)
			)
	)


	(:action move-left-from-r1005
		:precondition
			(and 
				(in r1005)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1005))
				(in r1004)
			)
	)

	(:action move-right-from-r1006
		:precondition
			(and 
				(in r1006)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1006))
				(in r1007)
			)
	)


	(:action move-left-from-r1006
		:precondition
			(and 
				(in r1006)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1006))
				(in r1005)
			)
	)

	(:action move-right-from-r1007
		:precondition
			(and 
				(in r1007)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1007))
				(in r1008)
			)
	)


	(:action move-left-from-r1007
		:precondition
			(and 
				(in r1007)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1007))
				(in r1006)
			)
	)

	(:action move-right-from-r1008
		:precondition
			(and 
				(in r1008)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1008))
				(in r1009)
			)
	)


	(:action move-left-from-r1008
		:precondition
			(and 
				(in r1008)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1008))
				(in r1007)
			)
	)

	(:action move-right-from-r1009
		:precondition
			(and 
				(in r1009)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1009))
				(in r1010)
			)
	)


	(:action move-left-from-r1009
		:precondition
			(and 
				(in r1009)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1009))
				(in r1008)
			)
	)

	(:action move-right-from-r1010
		:precondition
			(and 
				(in r1010)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1010))
				(in r1011)
			)
	)


	(:action move-left-from-r1010
		:precondition
			(and 
				(in r1010)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1010))
				(in r1009)
			)
	)

	(:action move-right-from-r1011
		:precondition
			(and 
				(in r1011)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1011))
				(in r1012)
			)
	)


	(:action move-left-from-r1011
		:precondition
			(and 
				(in r1011)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1011))
				(in r1010)
			)
	)

	(:action move-right-from-r1012
		:precondition
			(and 
				(in r1012)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1012))
				(in r1013)
			)
	)


	(:action move-left-from-r1012
		:precondition
			(and 
				(in r1012)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1012))
				(in r1011)
			)
	)

	(:action move-right-from-r1013
		:precondition
			(and 
				(in r1013)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1013))
				(in r1014)
			)
	)


	(:action move-left-from-r1013
		:precondition
			(and 
				(in r1013)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1013))
				(in r1012)
			)
	)

	(:action move-right-from-r1014
		:precondition
			(and 
				(in r1014)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1014))
				(in r1015)
			)
	)


	(:action move-left-from-r1014
		:precondition
			(and 
				(in r1014)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1014))
				(in r1013)
			)
	)

	(:action move-right-from-r1015
		:precondition
			(and 
				(in r1015)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1015))
				(in r1016)
			)
	)


	(:action move-left-from-r1015
		:precondition
			(and 
				(in r1015)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1015))
				(in r1014)
			)
	)

	(:action move-right-from-r1016
		:precondition
			(and 
				(in r1016)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1016))
				(in r1017)
			)
	)


	(:action move-left-from-r1016
		:precondition
			(and 
				(in r1016)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1016))
				(in r1015)
			)
	)

	(:action move-right-from-r1017
		:precondition
			(and 
				(in r1017)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1017))
				(in r1018)
			)
	)


	(:action move-left-from-r1017
		:precondition
			(and 
				(in r1017)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1017))
				(in r1016)
			)
	)

	(:action move-right-from-r1018
		:precondition
			(and 
				(in r1018)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1018))
				(in r1019)
			)
	)


	(:action move-left-from-r1018
		:precondition
			(and 
				(in r1018)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1018))
				(in r1017)
			)
	)

	(:action move-right-from-r1019
		:precondition
			(and 
				(in r1019)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1019))
				(in r1020)
			)
	)


	(:action move-left-from-r1019
		:precondition
			(and 
				(in r1019)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1019))
				(in r1018)
			)
	)

	(:action move-right-from-r1020
		:precondition
			(and 
				(in r1020)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1020))
				(in r1021)
			)
	)


	(:action move-left-from-r1020
		:precondition
			(and 
				(in r1020)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1020))
				(in r1019)
			)
	)

	(:action move-right-from-r1021
		:precondition
			(and 
				(in r1021)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1021))
				(in r1022)
			)
	)


	(:action move-left-from-r1021
		:precondition
			(and 
				(in r1021)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1021))
				(in r1020)
			)
	)

	(:action move-right-from-r1022
		:precondition
			(and 
				(in r1022)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1022))
				(in r1023)
			)
	)


	(:action move-left-from-r1022
		:precondition
			(and 
				(in r1022)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1022))
				(in r1021)
			)
	)

	(:action move-right-from-r1023
		:precondition
			(and 
				(in r1023)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1023))
				(in r1024)
			)
	)


	(:action move-left-from-r1023
		:precondition
			(and 
				(in r1023)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1023))
				(in r1022)
			)
	)

	(:action move-right-from-r1024
		:precondition
			(and 
				(in r1024)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1024))
				(in r1025)
			)
	)


	(:action move-left-from-r1024
		:precondition
			(and 
				(in r1024)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1024))
				(in r1023)
			)
	)

	(:action move-right-from-r1025
		:precondition
			(and 
				(in r1025)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1025))
				(in r1026)
			)
	)


	(:action move-left-from-r1025
		:precondition
			(and 
				(in r1025)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1025))
				(in r1024)
			)
	)

	(:action move-right-from-r1026
		:precondition
			(and 
				(in r1026)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1026))
				(in r1027)
			)
	)


	(:action move-left-from-r1026
		:precondition
			(and 
				(in r1026)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1026))
				(in r1025)
			)
	)

	(:action move-right-from-r1027
		:precondition
			(and 
				(in r1027)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1027))
				(in r1028)
			)
	)


	(:action move-left-from-r1027
		:precondition
			(and 
				(in r1027)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1027))
				(in r1026)
			)
	)

	(:action move-right-from-r1028
		:precondition
			(and 
				(in r1028)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1028))
				(in r1029)
			)
	)


	(:action move-left-from-r1028
		:precondition
			(and 
				(in r1028)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1028))
				(in r1027)
			)
	)

	(:action move-right-from-r1029
		:precondition
			(and 
				(in r1029)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1029))
				(in r1030)
			)
	)


	(:action move-left-from-r1029
		:precondition
			(and 
				(in r1029)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1029))
				(in r1028)
			)
	)

	(:action move-right-from-r1030
		:precondition
			(and 
				(in r1030)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1030))
				(in r1031)
			)
	)


	(:action move-left-from-r1030
		:precondition
			(and 
				(in r1030)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1030))
				(in r1029)
			)
	)

	(:action move-right-from-r1031
		:precondition
			(and 
				(in r1031)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1031))
				(in r1032)
			)
	)


	(:action move-left-from-r1031
		:precondition
			(and 
				(in r1031)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1031))
				(in r1030)
			)
	)

	(:action move-right-from-r1032
		:precondition
			(and 
				(in r1032)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1032))
				(in r1033)
			)
	)


	(:action move-left-from-r1032
		:precondition
			(and 
				(in r1032)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1032))
				(in r1031)
			)
	)

	(:action move-right-from-r1033
		:precondition
			(and 
				(in r1033)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1033))
				(in r1034)
			)
	)


	(:action move-left-from-r1033
		:precondition
			(and 
				(in r1033)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1033))
				(in r1032)
			)
	)

	(:action move-right-from-r1034
		:precondition
			(and 
				(in r1034)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1034))
				(in r1035)
			)
	)


	(:action move-left-from-r1034
		:precondition
			(and 
				(in r1034)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1034))
				(in r1033)
			)
	)

	(:action move-right-from-r1035
		:precondition
			(and 
				(in r1035)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1035))
				(in r1036)
			)
	)


	(:action move-left-from-r1035
		:precondition
			(and 
				(in r1035)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1035))
				(in r1034)
			)
	)

	(:action move-right-from-r1036
		:precondition
			(and 
				(in r1036)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1036))
				(in r1037)
			)
	)


	(:action move-left-from-r1036
		:precondition
			(and 
				(in r1036)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1036))
				(in r1035)
			)
	)

	(:action move-right-from-r1037
		:precondition
			(and 
				(in r1037)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1037))
				(in r1038)
			)
	)


	(:action move-left-from-r1037
		:precondition
			(and 
				(in r1037)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1037))
				(in r1036)
			)
	)

	(:action move-right-from-r1038
		:precondition
			(and 
				(in r1038)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1038))
				(in r1039)
			)
	)


	(:action move-left-from-r1038
		:precondition
			(and 
				(in r1038)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1038))
				(in r1037)
			)
	)

	(:action move-right-from-r1039
		:precondition
			(and 
				(in r1039)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1039))
				(in r1040)
			)
	)


	(:action move-left-from-r1039
		:precondition
			(and 
				(in r1039)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1039))
				(in r1038)
			)
	)

	(:action move-right-from-r1040
		:precondition
			(and 
				(in r1040)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1040))
				(in r1041)
			)
	)


	(:action move-left-from-r1040
		:precondition
			(and 
				(in r1040)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1040))
				(in r1039)
			)
	)

	(:action move-right-from-r1041
		:precondition
			(and 
				(in r1041)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1041))
				(in r1042)
			)
	)


	(:action move-left-from-r1041
		:precondition
			(and 
				(in r1041)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1041))
				(in r1040)
			)
	)

	(:action move-right-from-r1042
		:precondition
			(and 
				(in r1042)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1042))
				(in r1043)
			)
	)


	(:action move-left-from-r1042
		:precondition
			(and 
				(in r1042)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1042))
				(in r1041)
			)
	)

	(:action move-right-from-r1043
		:precondition
			(and 
				(in r1043)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1043))
				(in r1044)
			)
	)


	(:action move-left-from-r1043
		:precondition
			(and 
				(in r1043)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1043))
				(in r1042)
			)
	)

	(:action move-right-from-r1044
		:precondition
			(and 
				(in r1044)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1044))
				(in r1045)
			)
	)


	(:action move-left-from-r1044
		:precondition
			(and 
				(in r1044)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1044))
				(in r1043)
			)
	)

	(:action move-right-from-r1045
		:precondition
			(and 
				(in r1045)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1045))
				(in r1046)
			)
	)


	(:action move-left-from-r1045
		:precondition
			(and 
				(in r1045)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1045))
				(in r1044)
			)
	)

	(:action move-right-from-r1046
		:precondition
			(and 
				(in r1046)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1046))
				(in r1047)
			)
	)


	(:action move-left-from-r1046
		:precondition
			(and 
				(in r1046)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1046))
				(in r1045)
			)
	)

	(:action move-right-from-r1047
		:precondition
			(and 
				(in r1047)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1047))
				(in r1048)
			)
	)


	(:action move-left-from-r1047
		:precondition
			(and 
				(in r1047)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1047))
				(in r1046)
			)
	)

	(:action move-right-from-r1048
		:precondition
			(and 
				(in r1048)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1048))
				(in r1049)
			)
	)


	(:action move-left-from-r1048
		:precondition
			(and 
				(in r1048)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1048))
				(in r1047)
			)
	)

	(:action move-right-from-r1049
		:precondition
			(and 
				(in r1049)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1049))
				(in r1050)
			)
	)


	(:action move-left-from-r1049
		:precondition
			(and 
				(in r1049)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1049))
				(in r1048)
			)
	)

	(:action move-right-from-r1050
		:precondition
			(and 
				(in r1050)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1050))
				(in r1051)
			)
	)


	(:action move-left-from-r1050
		:precondition
			(and 
				(in r1050)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1050))
				(in r1049)
			)
	)

	(:action move-right-from-r1051
		:precondition
			(and 
				(in r1051)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1051))
				(in r1052)
			)
	)


	(:action move-left-from-r1051
		:precondition
			(and 
				(in r1051)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1051))
				(in r1050)
			)
	)

	(:action move-right-from-r1052
		:precondition
			(and 
				(in r1052)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1052))
				(in r1053)
			)
	)


	(:action move-left-from-r1052
		:precondition
			(and 
				(in r1052)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1052))
				(in r1051)
			)
	)

	(:action move-right-from-r1053
		:precondition
			(and 
				(in r1053)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1053))
				(in r1054)
			)
	)


	(:action move-left-from-r1053
		:precondition
			(and 
				(in r1053)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1053))
				(in r1052)
			)
	)

	(:action move-right-from-r1054
		:precondition
			(and 
				(in r1054)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1054))
				(in r1055)
			)
	)


	(:action move-left-from-r1054
		:precondition
			(and 
				(in r1054)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1054))
				(in r1053)
			)
	)

	(:action move-right-from-r1055
		:precondition
			(and 
				(in r1055)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1055))
				(in r1056)
			)
	)


	(:action move-left-from-r1055
		:precondition
			(and 
				(in r1055)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1055))
				(in r1054)
			)
	)

	(:action move-right-from-r1056
		:precondition
			(and 
				(in r1056)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1056))
				(in r1057)
			)
	)


	(:action move-left-from-r1056
		:precondition
			(and 
				(in r1056)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1056))
				(in r1055)
			)
	)

	(:action move-right-from-r1057
		:precondition
			(and 
				(in r1057)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1057))
				(in r1058)
			)
	)


	(:action move-left-from-r1057
		:precondition
			(and 
				(in r1057)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1057))
				(in r1056)
			)
	)

	(:action move-right-from-r1058
		:precondition
			(and 
				(in r1058)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1058))
				(in r1059)
			)
	)


	(:action move-left-from-r1058
		:precondition
			(and 
				(in r1058)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1058))
				(in r1057)
			)
	)

	(:action move-right-from-r1059
		:precondition
			(and 
				(in r1059)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1059))
				(in r1060)
			)
	)


	(:action move-left-from-r1059
		:precondition
			(and 
				(in r1059)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1059))
				(in r1058)
			)
	)

	(:action move-right-from-r1060
		:precondition
			(and 
				(in r1060)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1060))
				(in r1061)
			)
	)


	(:action move-left-from-r1060
		:precondition
			(and 
				(in r1060)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1060))
				(in r1059)
			)
	)

	(:action move-right-from-r1061
		:precondition
			(and 
				(in r1061)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1061))
				(in r1062)
			)
	)


	(:action move-left-from-r1061
		:precondition
			(and 
				(in r1061)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1061))
				(in r1060)
			)
	)

	(:action move-right-from-r1062
		:precondition
			(and 
				(in r1062)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1062))
				(in r1063)
			)
	)


	(:action move-left-from-r1062
		:precondition
			(and 
				(in r1062)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1062))
				(in r1061)
			)
	)

	(:action move-right-from-r1063
		:precondition
			(and 
				(in r1063)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1063))
				(in r1064)
			)
	)


	(:action move-left-from-r1063
		:precondition
			(and 
				(in r1063)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1063))
				(in r1062)
			)
	)

	(:action move-right-from-r1064
		:precondition
			(and 
				(in r1064)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1064))
				(in r1065)
			)
	)


	(:action move-left-from-r1064
		:precondition
			(and 
				(in r1064)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1064))
				(in r1063)
			)
	)

	(:action move-right-from-r1065
		:precondition
			(and 
				(in r1065)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1065))
				(in r1066)
			)
	)


	(:action move-left-from-r1065
		:precondition
			(and 
				(in r1065)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1065))
				(in r1064)
			)
	)

	(:action move-right-from-r1066
		:precondition
			(and 
				(in r1066)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1066))
				(in r1067)
			)
	)


	(:action move-left-from-r1066
		:precondition
			(and 
				(in r1066)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1066))
				(in r1065)
			)
	)

	(:action move-right-from-r1067
		:precondition
			(and 
				(in r1067)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1067))
				(in r1068)
			)
	)


	(:action move-left-from-r1067
		:precondition
			(and 
				(in r1067)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1067))
				(in r1066)
			)
	)

	(:action move-right-from-r1068
		:precondition
			(and 
				(in r1068)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1068))
				(in r1069)
			)
	)


	(:action move-left-from-r1068
		:precondition
			(and 
				(in r1068)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1068))
				(in r1067)
			)
	)

	(:action move-right-from-r1069
		:precondition
			(and 
				(in r1069)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1069))
				(in r1070)
			)
	)


	(:action move-left-from-r1069
		:precondition
			(and 
				(in r1069)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1069))
				(in r1068)
			)
	)

	(:action move-right-from-r1070
		:precondition
			(and 
				(in r1070)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1070))
				(in r1071)
			)
	)


	(:action move-left-from-r1070
		:precondition
			(and 
				(in r1070)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1070))
				(in r1069)
			)
	)

	(:action move-right-from-r1071
		:precondition
			(and 
				(in r1071)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1071))
				(in r1072)
			)
	)


	(:action move-left-from-r1071
		:precondition
			(and 
				(in r1071)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1071))
				(in r1070)
			)
	)

	(:action move-right-from-r1072
		:precondition
			(and 
				(in r1072)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1072))
				(in r1073)
			)
	)


	(:action move-left-from-r1072
		:precondition
			(and 
				(in r1072)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1072))
				(in r1071)
			)
	)

	(:action move-right-from-r1073
		:precondition
			(and 
				(in r1073)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1073))
				(in r1074)
			)
	)


	(:action move-left-from-r1073
		:precondition
			(and 
				(in r1073)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1073))
				(in r1072)
			)
	)

	(:action move-right-from-r1074
		:precondition
			(and 
				(in r1074)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1074))
				(in r1075)
			)
	)


	(:action move-left-from-r1074
		:precondition
			(and 
				(in r1074)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1074))
				(in r1073)
			)
	)

	(:action move-right-from-r1075
		:precondition
			(and 
				(in r1075)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1075))
				(in r1076)
			)
	)


	(:action move-left-from-r1075
		:precondition
			(and 
				(in r1075)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1075))
				(in r1074)
			)
	)

	(:action move-right-from-r1076
		:precondition
			(and 
				(in r1076)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1076))
				(in r1077)
			)
	)


	(:action move-left-from-r1076
		:precondition
			(and 
				(in r1076)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1076))
				(in r1075)
			)
	)

	(:action move-right-from-r1077
		:precondition
			(and 
				(in r1077)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1077))
				(in r1078)
			)
	)


	(:action move-left-from-r1077
		:precondition
			(and 
				(in r1077)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1077))
				(in r1076)
			)
	)

	(:action move-right-from-r1078
		:precondition
			(and 
				(in r1078)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1078))
				(in r1079)
			)
	)


	(:action move-left-from-r1078
		:precondition
			(and 
				(in r1078)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1078))
				(in r1077)
			)
	)

	(:action move-right-from-r1079
		:precondition
			(and 
				(in r1079)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1079))
				(in r1080)
			)
	)


	(:action move-left-from-r1079
		:precondition
			(and 
				(in r1079)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1079))
				(in r1078)
			)
	)

	(:action move-right-from-r1080
		:precondition
			(and 
				(in r1080)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1080))
				(in r1081)
			)
	)


	(:action move-left-from-r1080
		:precondition
			(and 
				(in r1080)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1080))
				(in r1079)
			)
	)

	(:action move-right-from-r1081
		:precondition
			(and 
				(in r1081)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1081))
				(in r1082)
			)
	)


	(:action move-left-from-r1081
		:precondition
			(and 
				(in r1081)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1081))
				(in r1080)
			)
	)

	(:action move-right-from-r1082
		:precondition
			(and 
				(in r1082)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1082))
				(in r1083)
			)
	)


	(:action move-left-from-r1082
		:precondition
			(and 
				(in r1082)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1082))
				(in r1081)
			)
	)

	(:action move-right-from-r1083
		:precondition
			(and 
				(in r1083)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1083))
				(in r1084)
			)
	)


	(:action move-left-from-r1083
		:precondition
			(and 
				(in r1083)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1083))
				(in r1082)
			)
	)

	(:action move-right-from-r1084
		:precondition
			(and 
				(in r1084)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1084))
				(in r1085)
			)
	)


	(:action move-left-from-r1084
		:precondition
			(and 
				(in r1084)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1084))
				(in r1083)
			)
	)

	(:action move-right-from-r1085
		:precondition
			(and 
				(in r1085)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1085))
				(in r1086)
			)
	)


	(:action move-left-from-r1085
		:precondition
			(and 
				(in r1085)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1085))
				(in r1084)
			)
	)

	(:action move-right-from-r1086
		:precondition
			(and 
				(in r1086)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1086))
				(in r1087)
			)
	)


	(:action move-left-from-r1086
		:precondition
			(and 
				(in r1086)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1086))
				(in r1085)
			)
	)

	(:action move-right-from-r1087
		:precondition
			(and 
				(in r1087)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1087))
				(in r1088)
			)
	)


	(:action move-left-from-r1087
		:precondition
			(and 
				(in r1087)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1087))
				(in r1086)
			)
	)

	(:action move-right-from-r1088
		:precondition
			(and 
				(in r1088)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1088))
				(in r1089)
			)
	)


	(:action move-left-from-r1088
		:precondition
			(and 
				(in r1088)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1088))
				(in r1087)
			)
	)

	(:action move-right-from-r1089
		:precondition
			(and 
				(in r1089)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1089))
				(in r1090)
			)
	)


	(:action move-left-from-r1089
		:precondition
			(and 
				(in r1089)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1089))
				(in r1088)
			)
	)

	(:action move-right-from-r1090
		:precondition
			(and 
				(in r1090)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1090))
				(in r1091)
			)
	)


	(:action move-left-from-r1090
		:precondition
			(and 
				(in r1090)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1090))
				(in r1089)
			)
	)

	(:action move-right-from-r1091
		:precondition
			(and 
				(in r1091)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1091))
				(in r1092)
			)
	)


	(:action move-left-from-r1091
		:precondition
			(and 
				(in r1091)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1091))
				(in r1090)
			)
	)

	(:action move-right-from-r1092
		:precondition
			(and 
				(in r1092)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1092))
				(in r1093)
			)
	)


	(:action move-left-from-r1092
		:precondition
			(and 
				(in r1092)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1092))
				(in r1091)
			)
	)

	(:action move-right-from-r1093
		:precondition
			(and 
				(in r1093)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1093))
				(in r1094)
			)
	)


	(:action move-left-from-r1093
		:precondition
			(and 
				(in r1093)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1093))
				(in r1092)
			)
	)

	(:action move-right-from-r1094
		:precondition
			(and 
				(in r1094)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1094))
				(in r1095)
			)
	)


	(:action move-left-from-r1094
		:precondition
			(and 
				(in r1094)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1094))
				(in r1093)
			)
	)

	(:action move-right-from-r1095
		:precondition
			(and 
				(in r1095)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1095))
				(in r1096)
			)
	)


	(:action move-left-from-r1095
		:precondition
			(and 
				(in r1095)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1095))
				(in r1094)
			)
	)

	(:action move-right-from-r1096
		:precondition
			(and 
				(in r1096)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1096))
				(in r1097)
			)
	)


	(:action move-left-from-r1096
		:precondition
			(and 
				(in r1096)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1096))
				(in r1095)
			)
	)

	(:action move-right-from-r1097
		:precondition
			(and 
				(in r1097)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1097))
				(in r1098)
			)
	)


	(:action move-left-from-r1097
		:precondition
			(and 
				(in r1097)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1097))
				(in r1096)
			)
	)

	(:action move-right-from-r1098
		:precondition
			(and 
				(in r1098)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1098))
				(in r1099)
			)
	)


	(:action move-left-from-r1098
		:precondition
			(and 
				(in r1098)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1098))
				(in r1097)
			)
	)

	(:action move-right-from-r1099
		:precondition
			(and 
				(in r1099)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1099))
				(in r1100)
			)
	)


	(:action move-left-from-r1099
		:precondition
			(and 
				(in r1099)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1099))
				(in r1098)
			)
	)

	(:action move-right-from-r1100
		:precondition
			(and 
				(in r1100)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1100))
				(in r1101)
			)
	)


	(:action move-left-from-r1100
		:precondition
			(and 
				(in r1100)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1100))
				(in r1099)
			)
	)

	(:action move-right-from-r1101
		:precondition
			(and 
				(in r1101)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1101))
				(in r1102)
			)
	)


	(:action move-left-from-r1101
		:precondition
			(and 
				(in r1101)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1101))
				(in r1100)
			)
	)

	(:action move-right-from-r1102
		:precondition
			(and 
				(in r1102)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1102))
				(in r1103)
			)
	)


	(:action move-left-from-r1102
		:precondition
			(and 
				(in r1102)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1102))
				(in r1101)
			)
	)

	(:action move-right-from-r1103
		:precondition
			(and 
				(in r1103)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1103))
				(in r1104)
			)
	)


	(:action move-left-from-r1103
		:precondition
			(and 
				(in r1103)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1103))
				(in r1102)
			)
	)

	(:action move-right-from-r1104
		:precondition
			(and 
				(in r1104)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1104))
				(in r1105)
			)
	)


	(:action move-left-from-r1104
		:precondition
			(and 
				(in r1104)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1104))
				(in r1103)
			)
	)

	(:action move-right-from-r1105
		:precondition
			(and 
				(in r1105)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1105))
				(in r1106)
			)
	)


	(:action move-left-from-r1105
		:precondition
			(and 
				(in r1105)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1105))
				(in r1104)
			)
	)

	(:action move-right-from-r1106
		:precondition
			(and 
				(in r1106)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1106))
				(in r1107)
			)
	)


	(:action move-left-from-r1106
		:precondition
			(and 
				(in r1106)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1106))
				(in r1105)
			)
	)

	(:action move-right-from-r1107
		:precondition
			(and 
				(in r1107)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1107))
				(in r1108)
			)
	)


	(:action move-left-from-r1107
		:precondition
			(and 
				(in r1107)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1107))
				(in r1106)
			)
	)

	(:action move-right-from-r1108
		:precondition
			(and 
				(in r1108)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1108))
				(in r1109)
			)
	)


	(:action move-left-from-r1108
		:precondition
			(and 
				(in r1108)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1108))
				(in r1107)
			)
	)

	(:action move-right-from-r1109
		:precondition
			(and 
				(in r1109)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1109))
				(in r1110)
			)
	)


	(:action move-left-from-r1109
		:precondition
			(and 
				(in r1109)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1109))
				(in r1108)
			)
	)

	(:action move-right-from-r1110
		:precondition
			(and 
				(in r1110)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1110))
				(in r1111)
			)
	)


	(:action move-left-from-r1110
		:precondition
			(and 
				(in r1110)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1110))
				(in r1109)
			)
	)

	(:action move-right-from-r1111
		:precondition
			(and 
				(in r1111)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1111))
				(in r1112)
			)
	)


	(:action move-left-from-r1111
		:precondition
			(and 
				(in r1111)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1111))
				(in r1110)
			)
	)

	(:action move-right-from-r1112
		:precondition
			(and 
				(in r1112)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1112))
				(in r1113)
			)
	)


	(:action move-left-from-r1112
		:precondition
			(and 
				(in r1112)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1112))
				(in r1111)
			)
	)

	(:action move-right-from-r1113
		:precondition
			(and 
				(in r1113)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1113))
				(in r1114)
			)
	)


	(:action move-left-from-r1113
		:precondition
			(and 
				(in r1113)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1113))
				(in r1112)
			)
	)

	(:action move-right-from-r1114
		:precondition
			(and 
				(in r1114)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1114))
				(in r1115)
			)
	)


	(:action move-left-from-r1114
		:precondition
			(and 
				(in r1114)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1114))
				(in r1113)
			)
	)

	(:action move-right-from-r1115
		:precondition
			(and 
				(in r1115)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1115))
				(in r1116)
			)
	)


	(:action move-left-from-r1115
		:precondition
			(and 
				(in r1115)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1115))
				(in r1114)
			)
	)

	(:action move-right-from-r1116
		:precondition
			(and 
				(in r1116)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1116))
				(in r1117)
			)
	)


	(:action move-left-from-r1116
		:precondition
			(and 
				(in r1116)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1116))
				(in r1115)
			)
	)

	(:action move-right-from-r1117
		:precondition
			(and 
				(in r1117)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1117))
				(in r1118)
			)
	)


	(:action move-left-from-r1117
		:precondition
			(and 
				(in r1117)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1117))
				(in r1116)
			)
	)

	(:action move-right-from-r1118
		:precondition
			(and 
				(in r1118)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1118))
				(in r1119)
			)
	)


	(:action move-left-from-r1118
		:precondition
			(and 
				(in r1118)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1118))
				(in r1117)
			)
	)

	(:action move-right-from-r1119
		:precondition
			(and 
				(in r1119)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1119))
				(in r1120)
			)
	)


	(:action move-left-from-r1119
		:precondition
			(and 
				(in r1119)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1119))
				(in r1118)
			)
	)

	(:action move-right-from-r1120
		:precondition
			(and 
				(in r1120)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1120))
				(in r1121)
			)
	)


	(:action move-left-from-r1120
		:precondition
			(and 
				(in r1120)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1120))
				(in r1119)
			)
	)

	(:action move-right-from-r1121
		:precondition
			(and 
				(in r1121)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1121))
				(in r1122)
			)
	)


	(:action move-left-from-r1121
		:precondition
			(and 
				(in r1121)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1121))
				(in r1120)
			)
	)

	(:action move-right-from-r1122
		:precondition
			(and 
				(in r1122)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1122))
				(in r1123)
			)
	)


	(:action move-left-from-r1122
		:precondition
			(and 
				(in r1122)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1122))
				(in r1121)
			)
	)

	(:action move-right-from-r1123
		:precondition
			(and 
				(in r1123)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1123))
				(in r1124)
			)
	)


	(:action move-left-from-r1123
		:precondition
			(and 
				(in r1123)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1123))
				(in r1122)
			)
	)

	(:action move-right-from-r1124
		:precondition
			(and 
				(in r1124)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1124))
				(in r1125)
			)
	)


	(:action move-left-from-r1124
		:precondition
			(and 
				(in r1124)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1124))
				(in r1123)
			)
	)

	(:action move-right-from-r1125
		:precondition
			(and 
				(in r1125)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1125))
				(in r1126)
			)
	)


	(:action move-left-from-r1125
		:precondition
			(and 
				(in r1125)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1125))
				(in r1124)
			)
	)

	(:action move-right-from-r1126
		:precondition
			(and 
				(in r1126)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1126))
				(in r1127)
			)
	)


	(:action move-left-from-r1126
		:precondition
			(and 
				(in r1126)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1126))
				(in r1125)
			)
	)

	(:action move-right-from-r1127
		:precondition
			(and 
				(in r1127)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1127))
				(in r1128)
			)
	)


	(:action move-left-from-r1127
		:precondition
			(and 
				(in r1127)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1127))
				(in r1126)
			)
	)

	(:action move-right-from-r1128
		:precondition
			(and 
				(in r1128)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1128))
				(in r1129)
			)
	)


	(:action move-left-from-r1128
		:precondition
			(and 
				(in r1128)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1128))
				(in r1127)
			)
	)

	(:action move-right-from-r1129
		:precondition
			(and 
				(in r1129)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1129))
				(in r1130)
			)
	)


	(:action move-left-from-r1129
		:precondition
			(and 
				(in r1129)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1129))
				(in r1128)
			)
	)

	(:action move-right-from-r1130
		:precondition
			(and 
				(in r1130)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1130))
				(in r1131)
			)
	)


	(:action move-left-from-r1130
		:precondition
			(and 
				(in r1130)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1130))
				(in r1129)
			)
	)

	(:action move-right-from-r1131
		:precondition
			(and 
				(in r1131)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1131))
				(in r1132)
			)
	)


	(:action move-left-from-r1131
		:precondition
			(and 
				(in r1131)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1131))
				(in r1130)
			)
	)

	(:action move-right-from-r1132
		:precondition
			(and 
				(in r1132)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1132))
				(in r1133)
			)
	)


	(:action move-left-from-r1132
		:precondition
			(and 
				(in r1132)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1132))
				(in r1131)
			)
	)

	(:action move-right-from-r1133
		:precondition
			(and 
				(in r1133)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1133))
				(in r1134)
			)
	)


	(:action move-left-from-r1133
		:precondition
			(and 
				(in r1133)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1133))
				(in r1132)
			)
	)

	(:action move-right-from-r1134
		:precondition
			(and 
				(in r1134)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1134))
				(in r1135)
			)
	)


	(:action move-left-from-r1134
		:precondition
			(and 
				(in r1134)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1134))
				(in r1133)
			)
	)

	(:action move-right-from-r1135
		:precondition
			(and 
				(in r1135)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1135))
				(in r1136)
			)
	)


	(:action move-left-from-r1135
		:precondition
			(and 
				(in r1135)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1135))
				(in r1134)
			)
	)

	(:action move-right-from-r1136
		:precondition
			(and 
				(in r1136)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1136))
				(in r1137)
			)
	)


	(:action move-left-from-r1136
		:precondition
			(and 
				(in r1136)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1136))
				(in r1135)
			)
	)

	(:action move-right-from-r1137
		:precondition
			(and 
				(in r1137)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1137))
				(in r1138)
			)
	)


	(:action move-left-from-r1137
		:precondition
			(and 
				(in r1137)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1137))
				(in r1136)
			)
	)

	(:action move-right-from-r1138
		:precondition
			(and 
				(in r1138)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1138))
				(in r1139)
			)
	)


	(:action move-left-from-r1138
		:precondition
			(and 
				(in r1138)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1138))
				(in r1137)
			)
	)

	(:action move-right-from-r1139
		:precondition
			(and 
				(in r1139)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1139))
				(in r1140)
			)
	)


	(:action move-left-from-r1139
		:precondition
			(and 
				(in r1139)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1139))
				(in r1138)
			)
	)

	(:action move-right-from-r1140
		:precondition
			(and 
				(in r1140)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1140))
				(in r1141)
			)
	)


	(:action move-left-from-r1140
		:precondition
			(and 
				(in r1140)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1140))
				(in r1139)
			)
	)

	(:action move-right-from-r1141
		:precondition
			(and 
				(in r1141)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1141))
				(in r1142)
			)
	)


	(:action move-left-from-r1141
		:precondition
			(and 
				(in r1141)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1141))
				(in r1140)
			)
	)

	(:action move-right-from-r1142
		:precondition
			(and 
				(in r1142)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1142))
				(in r1143)
			)
	)


	(:action move-left-from-r1142
		:precondition
			(and 
				(in r1142)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1142))
				(in r1141)
			)
	)

	(:action move-right-from-r1143
		:precondition
			(and 
				(in r1143)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1143))
				(in r1144)
			)
	)


	(:action move-left-from-r1143
		:precondition
			(and 
				(in r1143)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1143))
				(in r1142)
			)
	)

	(:action move-right-from-r1144
		:precondition
			(and 
				(in r1144)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1144))
				(in r1145)
			)
	)


	(:action move-left-from-r1144
		:precondition
			(and 
				(in r1144)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1144))
				(in r1143)
			)
	)

	(:action move-right-from-r1145
		:precondition
			(and 
				(in r1145)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1145))
				(in r1146)
			)
	)


	(:action move-left-from-r1145
		:precondition
			(and 
				(in r1145)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1145))
				(in r1144)
			)
	)

	(:action move-right-from-r1146
		:precondition
			(and 
				(in r1146)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1146))
				(in r1147)
			)
	)


	(:action move-left-from-r1146
		:precondition
			(and 
				(in r1146)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1146))
				(in r1145)
			)
	)

	(:action move-right-from-r1147
		:precondition
			(and 
				(in r1147)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1147))
				(in r1148)
			)
	)


	(:action move-left-from-r1147
		:precondition
			(and 
				(in r1147)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1147))
				(in r1146)
			)
	)

	(:action move-right-from-r1148
		:precondition
			(and 
				(in r1148)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1148))
				(in r1149)
			)
	)


	(:action move-left-from-r1148
		:precondition
			(and 
				(in r1148)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1148))
				(in r1147)
			)
	)

	(:action move-right-from-r1149
		:precondition
			(and 
				(in r1149)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1149))
				(in r1150)
			)
	)


	(:action move-left-from-r1149
		:precondition
			(and 
				(in r1149)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1149))
				(in r1148)
			)
	)

	(:action move-right-from-r1150
		:precondition
			(and 
				(in r1150)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1150))
				(in r1151)
			)
	)


	(:action move-left-from-r1150
		:precondition
			(and 
				(in r1150)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1150))
				(in r1149)
			)
	)

	(:action move-right-from-r1151
		:precondition
			(and 
				(in r1151)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1151))
				(in r1152)
			)
	)


	(:action move-left-from-r1151
		:precondition
			(and 
				(in r1151)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1151))
				(in r1150)
			)
	)

	(:action move-right-from-r1152
		:precondition
			(and 
				(in r1152)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1152))
				(in r1153)
			)
	)


	(:action move-left-from-r1152
		:precondition
			(and 
				(in r1152)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1152))
				(in r1151)
			)
	)

	(:action move-right-from-r1153
		:precondition
			(and 
				(in r1153)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1153))
				(in r1154)
			)
	)


	(:action move-left-from-r1153
		:precondition
			(and 
				(in r1153)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1153))
				(in r1152)
			)
	)

	(:action move-right-from-r1154
		:precondition
			(and 
				(in r1154)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1154))
				(in r1155)
			)
	)


	(:action move-left-from-r1154
		:precondition
			(and 
				(in r1154)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1154))
				(in r1153)
			)
	)

	(:action move-right-from-r1155
		:precondition
			(and 
				(in r1155)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1155))
				(in r1156)
			)
	)


	(:action move-left-from-r1155
		:precondition
			(and 
				(in r1155)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1155))
				(in r1154)
			)
	)

	(:action move-right-from-r1156
		:precondition
			(and 
				(in r1156)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1156))
				(in r1157)
			)
	)


	(:action move-left-from-r1156
		:precondition
			(and 
				(in r1156)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1156))
				(in r1155)
			)
	)

	(:action move-right-from-r1157
		:precondition
			(and 
				(in r1157)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1157))
				(in r1158)
			)
	)


	(:action move-left-from-r1157
		:precondition
			(and 
				(in r1157)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1157))
				(in r1156)
			)
	)

	(:action move-right-from-r1158
		:precondition
			(and 
				(in r1158)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1158))
				(in r1159)
			)
	)


	(:action move-left-from-r1158
		:precondition
			(and 
				(in r1158)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1158))
				(in r1157)
			)
	)

	(:action move-right-from-r1159
		:precondition
			(and 
				(in r1159)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1159))
				(in r1160)
			)
	)


	(:action move-left-from-r1159
		:precondition
			(and 
				(in r1159)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1159))
				(in r1158)
			)
	)

	(:action move-right-from-r1160
		:precondition
			(and 
				(in r1160)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1160))
				(in r1161)
			)
	)


	(:action move-left-from-r1160
		:precondition
			(and 
				(in r1160)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1160))
				(in r1159)
			)
	)

	(:action move-right-from-r1161
		:precondition
			(and 
				(in r1161)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1161))
				(in r1162)
			)
	)


	(:action move-left-from-r1161
		:precondition
			(and 
				(in r1161)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1161))
				(in r1160)
			)
	)

	(:action move-right-from-r1162
		:precondition
			(and 
				(in r1162)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1162))
				(in r1163)
			)
	)


	(:action move-left-from-r1162
		:precondition
			(and 
				(in r1162)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1162))
				(in r1161)
			)
	)

	(:action move-right-from-r1163
		:precondition
			(and 
				(in r1163)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1163))
				(in r1164)
			)
	)


	(:action move-left-from-r1163
		:precondition
			(and 
				(in r1163)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1163))
				(in r1162)
			)
	)

	(:action move-right-from-r1164
		:precondition
			(and 
				(in r1164)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1164))
				(in r1165)
			)
	)


	(:action move-left-from-r1164
		:precondition
			(and 
				(in r1164)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1164))
				(in r1163)
			)
	)

	(:action move-right-from-r1165
		:precondition
			(and 
				(in r1165)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1165))
				(in r1166)
			)
	)


	(:action move-left-from-r1165
		:precondition
			(and 
				(in r1165)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1165))
				(in r1164)
			)
	)

	(:action move-right-from-r1166
		:precondition
			(and 
				(in r1166)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1166))
				(in r1167)
			)
	)


	(:action move-left-from-r1166
		:precondition
			(and 
				(in r1166)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1166))
				(in r1165)
			)
	)

	(:action move-right-from-r1167
		:precondition
			(and 
				(in r1167)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1167))
				(in r1168)
			)
	)


	(:action move-left-from-r1167
		:precondition
			(and 
				(in r1167)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1167))
				(in r1166)
			)
	)

	(:action move-right-from-r1168
		:precondition
			(and 
				(in r1168)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1168))
				(in r1169)
			)
	)


	(:action move-left-from-r1168
		:precondition
			(and 
				(in r1168)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1168))
				(in r1167)
			)
	)

	(:action move-right-from-r1169
		:precondition
			(and 
				(in r1169)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1169))
				(in r1170)
			)
	)


	(:action move-left-from-r1169
		:precondition
			(and 
				(in r1169)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1169))
				(in r1168)
			)
	)

	(:action move-right-from-r1170
		:precondition
			(and 
				(in r1170)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1170))
				(in r1171)
			)
	)


	(:action move-left-from-r1170
		:precondition
			(and 
				(in r1170)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1170))
				(in r1169)
			)
	)

	(:action move-right-from-r1171
		:precondition
			(and 
				(in r1171)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1171))
				(in r1172)
			)
	)


	(:action move-left-from-r1171
		:precondition
			(and 
				(in r1171)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1171))
				(in r1170)
			)
	)

	(:action move-right-from-r1172
		:precondition
			(and 
				(in r1172)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1172))
				(in r1173)
			)
	)


	(:action move-left-from-r1172
		:precondition
			(and 
				(in r1172)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1172))
				(in r1171)
			)
	)

	(:action move-right-from-r1173
		:precondition
			(and 
				(in r1173)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1173))
				(in r1174)
			)
	)


	(:action move-left-from-r1173
		:precondition
			(and 
				(in r1173)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1173))
				(in r1172)
			)
	)

	(:action move-right-from-r1174
		:precondition
			(and 
				(in r1174)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1174))
				(in r1175)
			)
	)


	(:action move-left-from-r1174
		:precondition
			(and 
				(in r1174)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1174))
				(in r1173)
			)
	)

	(:action move-right-from-r1175
		:precondition
			(and 
				(in r1175)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1175))
				(in r1176)
			)
	)


	(:action move-left-from-r1175
		:precondition
			(and 
				(in r1175)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1175))
				(in r1174)
			)
	)

	(:action move-right-from-r1176
		:precondition
			(and 
				(in r1176)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1176))
				(in r1177)
			)
	)


	(:action move-left-from-r1176
		:precondition
			(and 
				(in r1176)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1176))
				(in r1175)
			)
	)

	(:action move-right-from-r1177
		:precondition
			(and 
				(in r1177)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1177))
				(in r1178)
			)
	)


	(:action move-left-from-r1177
		:precondition
			(and 
				(in r1177)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1177))
				(in r1176)
			)
	)

	(:action move-right-from-r1178
		:precondition
			(and 
				(in r1178)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1178))
				(in r1179)
			)
	)


	(:action move-left-from-r1178
		:precondition
			(and 
				(in r1178)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1178))
				(in r1177)
			)
	)

	(:action move-right-from-r1179
		:precondition
			(and 
				(in r1179)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1179))
				(in r1180)
			)
	)


	(:action move-left-from-r1179
		:precondition
			(and 
				(in r1179)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1179))
				(in r1178)
			)
	)

	(:action move-right-from-r1180
		:precondition
			(and 
				(in r1180)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1180))
				(in r1181)
			)
	)


	(:action move-left-from-r1180
		:precondition
			(and 
				(in r1180)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1180))
				(in r1179)
			)
	)

	(:action move-right-from-r1181
		:precondition
			(and 
				(in r1181)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1181))
				(in r1182)
			)
	)


	(:action move-left-from-r1181
		:precondition
			(and 
				(in r1181)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1181))
				(in r1180)
			)
	)

	(:action move-right-from-r1182
		:precondition
			(and 
				(in r1182)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1182))
				(in r1183)
			)
	)


	(:action move-left-from-r1182
		:precondition
			(and 
				(in r1182)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1182))
				(in r1181)
			)
	)

	(:action move-right-from-r1183
		:precondition
			(and 
				(in r1183)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1183))
				(in r1184)
			)
	)


	(:action move-left-from-r1183
		:precondition
			(and 
				(in r1183)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1183))
				(in r1182)
			)
	)

	(:action move-right-from-r1184
		:precondition
			(and 
				(in r1184)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1184))
				(in r1185)
			)
	)


	(:action move-left-from-r1184
		:precondition
			(and 
				(in r1184)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1184))
				(in r1183)
			)
	)

	(:action move-right-from-r1185
		:precondition
			(and 
				(in r1185)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1185))
				(in r1186)
			)
	)


	(:action move-left-from-r1185
		:precondition
			(and 
				(in r1185)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1185))
				(in r1184)
			)
	)

	(:action move-right-from-r1186
		:precondition
			(and 
				(in r1186)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1186))
				(in r1187)
			)
	)


	(:action move-left-from-r1186
		:precondition
			(and 
				(in r1186)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1186))
				(in r1185)
			)
	)

	(:action move-right-from-r1187
		:precondition
			(and 
				(in r1187)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1187))
				(in r1188)
			)
	)


	(:action move-left-from-r1187
		:precondition
			(and 
				(in r1187)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1187))
				(in r1186)
			)
	)

	(:action move-right-from-r1188
		:precondition
			(and 
				(in r1188)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1188))
				(in r1189)
			)
	)


	(:action move-left-from-r1188
		:precondition
			(and 
				(in r1188)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1188))
				(in r1187)
			)
	)

	(:action move-right-from-r1189
		:precondition
			(and 
				(in r1189)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1189))
				(in r1190)
			)
	)


	(:action move-left-from-r1189
		:precondition
			(and 
				(in r1189)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1189))
				(in r1188)
			)
	)

	(:action move-right-from-r1190
		:precondition
			(and 
				(in r1190)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1190))
				(in r1191)
			)
	)


	(:action move-left-from-r1190
		:precondition
			(and 
				(in r1190)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1190))
				(in r1189)
			)
	)

	(:action move-right-from-r1191
		:precondition
			(and 
				(in r1191)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1191))
				(in r1192)
			)
	)


	(:action move-left-from-r1191
		:precondition
			(and 
				(in r1191)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1191))
				(in r1190)
			)
	)

	(:action move-right-from-r1192
		:precondition
			(and 
				(in r1192)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1192))
				(in r1193)
			)
	)


	(:action move-left-from-r1192
		:precondition
			(and 
				(in r1192)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1192))
				(in r1191)
			)
	)

	(:action move-right-from-r1193
		:precondition
			(and 
				(in r1193)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1193))
				(in r1194)
			)
	)


	(:action move-left-from-r1193
		:precondition
			(and 
				(in r1193)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1193))
				(in r1192)
			)
	)

	(:action move-right-from-r1194
		:precondition
			(and 
				(in r1194)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1194))
				(in r1195)
			)
	)


	(:action move-left-from-r1194
		:precondition
			(and 
				(in r1194)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1194))
				(in r1193)
			)
	)

	(:action move-right-from-r1195
		:precondition
			(and 
				(in r1195)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1195))
				(in r1196)
			)
	)


	(:action move-left-from-r1195
		:precondition
			(and 
				(in r1195)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1195))
				(in r1194)
			)
	)

	(:action move-right-from-r1196
		:precondition
			(and 
				(in r1196)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1196))
				(in r1197)
			)
	)


	(:action move-left-from-r1196
		:precondition
			(and 
				(in r1196)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1196))
				(in r1195)
			)
	)

	(:action move-right-from-r1197
		:precondition
			(and 
				(in r1197)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1197))
				(in r1198)
			)
	)


	(:action move-left-from-r1197
		:precondition
			(and 
				(in r1197)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1197))
				(in r1196)
			)
	)

	(:action move-right-from-r1198
		:precondition
			(and 
				(in r1198)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1198))
				(in r1199)
			)
	)


	(:action move-left-from-r1198
		:precondition
			(and 
				(in r1198)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1198))
				(in r1197)
			)
	)

	(:action move-right-from-r1199
		:precondition
			(and 
				(in r1199)
				(not (searched_in r1200))
			)
		:effect
			(and
				(not (search_again))
				(not (in r1199))
				(in r1200)
				(oneof (not (seen)) (seen))
				(searched_in r1200)
			)
	)


	(:action move-left-from-r1199
		:precondition
			(and 
				(in r1199)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1199))
				(in r1198)
			)
	)

	(:action move-right-from-r1200
		:precondition
			(and 
				(in r1200)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r1200))
				(in r1201)
			)
	)


	(:action move-left-from-r1200
		:precondition
			(and 
				(in r1200)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r1200))
				(in r1199)
			)
	)

	(:action move-right-from-r1201
		:precondition
			(and 
				(in r1201)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1201))
				(in r1202)
			)
	)


	(:action move-left-from-r1201
		:precondition
			(and 
				(in r1201)
				(not (searched_in r1200))
			)
		:effect
			(and
				(not (search_again))
				(not (in r1201))
				(in r1200)
				(oneof (not (seen)) (seen))
				(searched_in r1200)
			)
	)

	(:action move-right-from-r1202
		:precondition
			(and 
				(in r1202)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1202))
				(in r1203)
			)
	)


	(:action move-left-from-r1202
		:precondition
			(and 
				(in r1202)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1202))
				(in r1201)
			)
	)

	(:action move-right-from-r1203
		:precondition
			(and 
				(in r1203)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1203))
				(in r1204)
			)
	)


	(:action move-left-from-r1203
		:precondition
			(and 
				(in r1203)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1203))
				(in r1202)
			)
	)

	(:action move-right-from-r1204
		:precondition
			(and 
				(in r1204)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1204))
				(in r1205)
			)
	)


	(:action move-left-from-r1204
		:precondition
			(and 
				(in r1204)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1204))
				(in r1203)
			)
	)

	(:action move-right-from-r1205
		:precondition
			(and 
				(in r1205)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1205))
				(in r1206)
			)
	)


	(:action move-left-from-r1205
		:precondition
			(and 
				(in r1205)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1205))
				(in r1204)
			)
	)

	(:action move-right-from-r1206
		:precondition
			(and 
				(in r1206)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1206))
				(in r1207)
			)
	)


	(:action move-left-from-r1206
		:precondition
			(and 
				(in r1206)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1206))
				(in r1205)
			)
	)

	(:action move-right-from-r1207
		:precondition
			(and 
				(in r1207)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1207))
				(in r1208)
			)
	)


	(:action move-left-from-r1207
		:precondition
			(and 
				(in r1207)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1207))
				(in r1206)
			)
	)

	(:action move-right-from-r1208
		:precondition
			(and 
				(in r1208)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1208))
				(in r1209)
			)
	)


	(:action move-left-from-r1208
		:precondition
			(and 
				(in r1208)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1208))
				(in r1207)
			)
	)

	(:action move-right-from-r1209
		:precondition
			(and 
				(in r1209)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1209))
				(in r1210)
			)
	)


	(:action move-left-from-r1209
		:precondition
			(and 
				(in r1209)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1209))
				(in r1208)
			)
	)

	(:action move-right-from-r1210
		:precondition
			(and 
				(in r1210)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1210))
				(in r1211)
			)
	)


	(:action move-left-from-r1210
		:precondition
			(and 
				(in r1210)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1210))
				(in r1209)
			)
	)

	(:action move-right-from-r1211
		:precondition
			(and 
				(in r1211)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1211))
				(in r1212)
			)
	)


	(:action move-left-from-r1211
		:precondition
			(and 
				(in r1211)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1211))
				(in r1210)
			)
	)

	(:action move-right-from-r1212
		:precondition
			(and 
				(in r1212)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1212))
				(in r1213)
			)
	)


	(:action move-left-from-r1212
		:precondition
			(and 
				(in r1212)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1212))
				(in r1211)
			)
	)

	(:action move-right-from-r1213
		:precondition
			(and 
				(in r1213)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1213))
				(in r1214)
			)
	)


	(:action move-left-from-r1213
		:precondition
			(and 
				(in r1213)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1213))
				(in r1212)
			)
	)

	(:action move-right-from-r1214
		:precondition
			(and 
				(in r1214)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1214))
				(in r1215)
			)
	)


	(:action move-left-from-r1214
		:precondition
			(and 
				(in r1214)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1214))
				(in r1213)
			)
	)

	(:action move-right-from-r1215
		:precondition
			(and 
				(in r1215)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1215))
				(in r1216)
			)
	)


	(:action move-left-from-r1215
		:precondition
			(and 
				(in r1215)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1215))
				(in r1214)
			)
	)

	(:action move-right-from-r1216
		:precondition
			(and 
				(in r1216)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1216))
				(in r1217)
			)
	)


	(:action move-left-from-r1216
		:precondition
			(and 
				(in r1216)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1216))
				(in r1215)
			)
	)

	(:action move-right-from-r1217
		:precondition
			(and 
				(in r1217)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1217))
				(in r1218)
			)
	)


	(:action move-left-from-r1217
		:precondition
			(and 
				(in r1217)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1217))
				(in r1216)
			)
	)

	(:action move-right-from-r1218
		:precondition
			(and 
				(in r1218)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1218))
				(in r1219)
			)
	)


	(:action move-left-from-r1218
		:precondition
			(and 
				(in r1218)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1218))
				(in r1217)
			)
	)

	(:action move-right-from-r1219
		:precondition
			(and 
				(in r1219)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1219))
				(in r1220)
			)
	)


	(:action move-left-from-r1219
		:precondition
			(and 
				(in r1219)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1219))
				(in r1218)
			)
	)

	(:action move-right-from-r1220
		:precondition
			(and 
				(in r1220)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1220))
				(in r1221)
			)
	)


	(:action move-left-from-r1220
		:precondition
			(and 
				(in r1220)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1220))
				(in r1219)
			)
	)

	(:action move-right-from-r1221
		:precondition
			(and 
				(in r1221)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1221))
				(in r1222)
			)
	)


	(:action move-left-from-r1221
		:precondition
			(and 
				(in r1221)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1221))
				(in r1220)
			)
	)

	(:action move-right-from-r1222
		:precondition
			(and 
				(in r1222)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1222))
				(in r1223)
			)
	)


	(:action move-left-from-r1222
		:precondition
			(and 
				(in r1222)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1222))
				(in r1221)
			)
	)

	(:action move-right-from-r1223
		:precondition
			(and 
				(in r1223)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1223))
				(in r1224)
			)
	)


	(:action move-left-from-r1223
		:precondition
			(and 
				(in r1223)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1223))
				(in r1222)
			)
	)

	(:action move-right-from-r1224
		:precondition
			(and 
				(in r1224)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1224))
				(in r1225)
			)
	)


	(:action move-left-from-r1224
		:precondition
			(and 
				(in r1224)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1224))
				(in r1223)
			)
	)

	(:action move-right-from-r1225
		:precondition
			(and 
				(in r1225)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1225))
				(in r1226)
			)
	)


	(:action move-left-from-r1225
		:precondition
			(and 
				(in r1225)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1225))
				(in r1224)
			)
	)

	(:action move-right-from-r1226
		:precondition
			(and 
				(in r1226)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1226))
				(in r1227)
			)
	)


	(:action move-left-from-r1226
		:precondition
			(and 
				(in r1226)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1226))
				(in r1225)
			)
	)

	(:action move-right-from-r1227
		:precondition
			(and 
				(in r1227)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1227))
				(in r1228)
			)
	)


	(:action move-left-from-r1227
		:precondition
			(and 
				(in r1227)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1227))
				(in r1226)
			)
	)

	(:action move-right-from-r1228
		:precondition
			(and 
				(in r1228)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1228))
				(in r1229)
			)
	)


	(:action move-left-from-r1228
		:precondition
			(and 
				(in r1228)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1228))
				(in r1227)
			)
	)

	(:action move-right-from-r1229
		:precondition
			(and 
				(in r1229)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1229))
				(in r1230)
			)
	)


	(:action move-left-from-r1229
		:precondition
			(and 
				(in r1229)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1229))
				(in r1228)
			)
	)

	(:action move-right-from-r1230
		:precondition
			(and 
				(in r1230)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1230))
				(in r1231)
			)
	)


	(:action move-left-from-r1230
		:precondition
			(and 
				(in r1230)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1230))
				(in r1229)
			)
	)

	(:action move-right-from-r1231
		:precondition
			(and 
				(in r1231)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1231))
				(in r1232)
			)
	)


	(:action move-left-from-r1231
		:precondition
			(and 
				(in r1231)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1231))
				(in r1230)
			)
	)

	(:action move-right-from-r1232
		:precondition
			(and 
				(in r1232)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1232))
				(in r1233)
			)
	)


	(:action move-left-from-r1232
		:precondition
			(and 
				(in r1232)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1232))
				(in r1231)
			)
	)

	(:action move-right-from-r1233
		:precondition
			(and 
				(in r1233)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1233))
				(in r1234)
			)
	)


	(:action move-left-from-r1233
		:precondition
			(and 
				(in r1233)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1233))
				(in r1232)
			)
	)

	(:action move-right-from-r1234
		:precondition
			(and 
				(in r1234)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1234))
				(in r1235)
			)
	)


	(:action move-left-from-r1234
		:precondition
			(and 
				(in r1234)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1234))
				(in r1233)
			)
	)

	(:action move-right-from-r1235
		:precondition
			(and 
				(in r1235)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1235))
				(in r1236)
			)
	)


	(:action move-left-from-r1235
		:precondition
			(and 
				(in r1235)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1235))
				(in r1234)
			)
	)

	(:action move-right-from-r1236
		:precondition
			(and 
				(in r1236)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1236))
				(in r1237)
			)
	)


	(:action move-left-from-r1236
		:precondition
			(and 
				(in r1236)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1236))
				(in r1235)
			)
	)

	(:action move-right-from-r1237
		:precondition
			(and 
				(in r1237)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1237))
				(in r1238)
			)
	)


	(:action move-left-from-r1237
		:precondition
			(and 
				(in r1237)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1237))
				(in r1236)
			)
	)

	(:action move-right-from-r1238
		:precondition
			(and 
				(in r1238)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1238))
				(in r1239)
			)
	)


	(:action move-left-from-r1238
		:precondition
			(and 
				(in r1238)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1238))
				(in r1237)
			)
	)

	(:action move-right-from-r1239
		:precondition
			(and 
				(in r1239)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1239))
				(in r1240)
			)
	)


	(:action move-left-from-r1239
		:precondition
			(and 
				(in r1239)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1239))
				(in r1238)
			)
	)

	(:action move-right-from-r1240
		:precondition
			(and 
				(in r1240)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1240))
				(in r1241)
			)
	)


	(:action move-left-from-r1240
		:precondition
			(and 
				(in r1240)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1240))
				(in r1239)
			)
	)

	(:action move-right-from-r1241
		:precondition
			(and 
				(in r1241)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1241))
				(in r1242)
			)
	)


	(:action move-left-from-r1241
		:precondition
			(and 
				(in r1241)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1241))
				(in r1240)
			)
	)

	(:action move-right-from-r1242
		:precondition
			(and 
				(in r1242)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1242))
				(in r1243)
			)
	)


	(:action move-left-from-r1242
		:precondition
			(and 
				(in r1242)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1242))
				(in r1241)
			)
	)

	(:action move-right-from-r1243
		:precondition
			(and 
				(in r1243)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1243))
				(in r1244)
			)
	)


	(:action move-left-from-r1243
		:precondition
			(and 
				(in r1243)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1243))
				(in r1242)
			)
	)

	(:action move-right-from-r1244
		:precondition
			(and 
				(in r1244)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1244))
				(in r1245)
			)
	)


	(:action move-left-from-r1244
		:precondition
			(and 
				(in r1244)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1244))
				(in r1243)
			)
	)

	(:action move-right-from-r1245
		:precondition
			(and 
				(in r1245)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1245))
				(in r1246)
			)
	)


	(:action move-left-from-r1245
		:precondition
			(and 
				(in r1245)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1245))
				(in r1244)
			)
	)

	(:action move-right-from-r1246
		:precondition
			(and 
				(in r1246)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1246))
				(in r1247)
			)
	)


	(:action move-left-from-r1246
		:precondition
			(and 
				(in r1246)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1246))
				(in r1245)
			)
	)

	(:action move-right-from-r1247
		:precondition
			(and 
				(in r1247)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1247))
				(in r1248)
			)
	)


	(:action move-left-from-r1247
		:precondition
			(and 
				(in r1247)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1247))
				(in r1246)
			)
	)

	(:action move-right-from-r1248
		:precondition
			(and 
				(in r1248)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1248))
				(in r1249)
			)
	)


	(:action move-left-from-r1248
		:precondition
			(and 
				(in r1248)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1248))
				(in r1247)
			)
	)

	(:action move-right-from-r1249
		:precondition
			(and 
				(in r1249)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1249))
				(in r1250)
			)
	)


	(:action move-left-from-r1249
		:precondition
			(and 
				(in r1249)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1249))
				(in r1248)
			)
	)

	(:action move-right-from-r1250
		:precondition
			(and 
				(in r1250)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1250))
				(in r1251)
			)
	)


	(:action move-left-from-r1250
		:precondition
			(and 
				(in r1250)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1250))
				(in r1249)
			)
	)

	(:action move-right-from-r1251
		:precondition
			(and 
				(in r1251)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1251))
				(in r1252)
			)
	)


	(:action move-left-from-r1251
		:precondition
			(and 
				(in r1251)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1251))
				(in r1250)
			)
	)

	(:action move-right-from-r1252
		:precondition
			(and 
				(in r1252)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1252))
				(in r1253)
			)
	)


	(:action move-left-from-r1252
		:precondition
			(and 
				(in r1252)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1252))
				(in r1251)
			)
	)

	(:action move-right-from-r1253
		:precondition
			(and 
				(in r1253)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1253))
				(in r1254)
			)
	)


	(:action move-left-from-r1253
		:precondition
			(and 
				(in r1253)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1253))
				(in r1252)
			)
	)

	(:action move-right-from-r1254
		:precondition
			(and 
				(in r1254)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1254))
				(in r1255)
			)
	)


	(:action move-left-from-r1254
		:precondition
			(and 
				(in r1254)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1254))
				(in r1253)
			)
	)

	(:action move-right-from-r1255
		:precondition
			(and 
				(in r1255)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1255))
				(in r1256)
			)
	)


	(:action move-left-from-r1255
		:precondition
			(and 
				(in r1255)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1255))
				(in r1254)
			)
	)

	(:action move-right-from-r1256
		:precondition
			(and 
				(in r1256)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1256))
				(in r1257)
			)
	)


	(:action move-left-from-r1256
		:precondition
			(and 
				(in r1256)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1256))
				(in r1255)
			)
	)

	(:action move-right-from-r1257
		:precondition
			(and 
				(in r1257)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1257))
				(in r1258)
			)
	)


	(:action move-left-from-r1257
		:precondition
			(and 
				(in r1257)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1257))
				(in r1256)
			)
	)

	(:action move-right-from-r1258
		:precondition
			(and 
				(in r1258)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1258))
				(in r1259)
			)
	)


	(:action move-left-from-r1258
		:precondition
			(and 
				(in r1258)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1258))
				(in r1257)
			)
	)

	(:action move-right-from-r1259
		:precondition
			(and 
				(in r1259)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1259))
				(in r1260)
			)
	)


	(:action move-left-from-r1259
		:precondition
			(and 
				(in r1259)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1259))
				(in r1258)
			)
	)

	(:action move-right-from-r1260
		:precondition
			(and 
				(in r1260)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1260))
				(in r1261)
			)
	)


	(:action move-left-from-r1260
		:precondition
			(and 
				(in r1260)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1260))
				(in r1259)
			)
	)

	(:action move-right-from-r1261
		:precondition
			(and 
				(in r1261)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1261))
				(in r1262)
			)
	)


	(:action move-left-from-r1261
		:precondition
			(and 
				(in r1261)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1261))
				(in r1260)
			)
	)

	(:action move-right-from-r1262
		:precondition
			(and 
				(in r1262)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1262))
				(in r1263)
			)
	)


	(:action move-left-from-r1262
		:precondition
			(and 
				(in r1262)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1262))
				(in r1261)
			)
	)

	(:action move-right-from-r1263
		:precondition
			(and 
				(in r1263)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1263))
				(in r1264)
			)
	)


	(:action move-left-from-r1263
		:precondition
			(and 
				(in r1263)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1263))
				(in r1262)
			)
	)

	(:action move-right-from-r1264
		:precondition
			(and 
				(in r1264)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1264))
				(in r1265)
			)
	)


	(:action move-left-from-r1264
		:precondition
			(and 
				(in r1264)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1264))
				(in r1263)
			)
	)

	(:action move-right-from-r1265
		:precondition
			(and 
				(in r1265)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1265))
				(in r1266)
			)
	)


	(:action move-left-from-r1265
		:precondition
			(and 
				(in r1265)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1265))
				(in r1264)
			)
	)

	(:action move-right-from-r1266
		:precondition
			(and 
				(in r1266)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1266))
				(in r1267)
			)
	)


	(:action move-left-from-r1266
		:precondition
			(and 
				(in r1266)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1266))
				(in r1265)
			)
	)

	(:action move-right-from-r1267
		:precondition
			(and 
				(in r1267)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1267))
				(in r1268)
			)
	)


	(:action move-left-from-r1267
		:precondition
			(and 
				(in r1267)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1267))
				(in r1266)
			)
	)

	(:action move-right-from-r1268
		:precondition
			(and 
				(in r1268)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1268))
				(in r1269)
			)
	)


	(:action move-left-from-r1268
		:precondition
			(and 
				(in r1268)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1268))
				(in r1267)
			)
	)

	(:action move-right-from-r1269
		:precondition
			(and 
				(in r1269)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1269))
				(in r1270)
			)
	)


	(:action move-left-from-r1269
		:precondition
			(and 
				(in r1269)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1269))
				(in r1268)
			)
	)

	(:action move-right-from-r1270
		:precondition
			(and 
				(in r1270)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1270))
				(in r1271)
			)
	)


	(:action move-left-from-r1270
		:precondition
			(and 
				(in r1270)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1270))
				(in r1269)
			)
	)

	(:action move-right-from-r1271
		:precondition
			(and 
				(in r1271)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1271))
				(in r1272)
			)
	)


	(:action move-left-from-r1271
		:precondition
			(and 
				(in r1271)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1271))
				(in r1270)
			)
	)

	(:action move-right-from-r1272
		:precondition
			(and 
				(in r1272)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1272))
				(in r1273)
			)
	)


	(:action move-left-from-r1272
		:precondition
			(and 
				(in r1272)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1272))
				(in r1271)
			)
	)

	(:action move-right-from-r1273
		:precondition
			(and 
				(in r1273)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1273))
				(in r1274)
			)
	)


	(:action move-left-from-r1273
		:precondition
			(and 
				(in r1273)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1273))
				(in r1272)
			)
	)

	(:action move-right-from-r1274
		:precondition
			(and 
				(in r1274)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1274))
				(in r1275)
			)
	)


	(:action move-left-from-r1274
		:precondition
			(and 
				(in r1274)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1274))
				(in r1273)
			)
	)

	(:action move-right-from-r1275
		:precondition
			(and 
				(in r1275)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1275))
				(in r1276)
			)
	)


	(:action move-left-from-r1275
		:precondition
			(and 
				(in r1275)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1275))
				(in r1274)
			)
	)

	(:action move-right-from-r1276
		:precondition
			(and 
				(in r1276)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1276))
				(in r1277)
			)
	)


	(:action move-left-from-r1276
		:precondition
			(and 
				(in r1276)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1276))
				(in r1275)
			)
	)

	(:action move-right-from-r1277
		:precondition
			(and 
				(in r1277)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1277))
				(in r1278)
			)
	)


	(:action move-left-from-r1277
		:precondition
			(and 
				(in r1277)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1277))
				(in r1276)
			)
	)

	(:action move-right-from-r1278
		:precondition
			(and 
				(in r1278)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1278))
				(in r1279)
			)
	)


	(:action move-left-from-r1278
		:precondition
			(and 
				(in r1278)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1278))
				(in r1277)
			)
	)

	(:action move-right-from-r1279
		:precondition
			(and 
				(in r1279)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1279))
				(in r1280)
			)
	)


	(:action move-left-from-r1279
		:precondition
			(and 
				(in r1279)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1279))
				(in r1278)
			)
	)

	(:action move-right-from-r1280
		:precondition
			(and 
				(in r1280)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1280))
				(in r1281)
			)
	)


	(:action move-left-from-r1280
		:precondition
			(and 
				(in r1280)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1280))
				(in r1279)
			)
	)

	(:action move-right-from-r1281
		:precondition
			(and 
				(in r1281)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1281))
				(in r1282)
			)
	)


	(:action move-left-from-r1281
		:precondition
			(and 
				(in r1281)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1281))
				(in r1280)
			)
	)

	(:action move-right-from-r1282
		:precondition
			(and 
				(in r1282)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1282))
				(in r1283)
			)
	)


	(:action move-left-from-r1282
		:precondition
			(and 
				(in r1282)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1282))
				(in r1281)
			)
	)

	(:action move-right-from-r1283
		:precondition
			(and 
				(in r1283)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1283))
				(in r1284)
			)
	)


	(:action move-left-from-r1283
		:precondition
			(and 
				(in r1283)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1283))
				(in r1282)
			)
	)

	(:action move-right-from-r1284
		:precondition
			(and 
				(in r1284)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1284))
				(in r1285)
			)
	)


	(:action move-left-from-r1284
		:precondition
			(and 
				(in r1284)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1284))
				(in r1283)
			)
	)

	(:action move-right-from-r1285
		:precondition
			(and 
				(in r1285)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1285))
				(in r1286)
			)
	)


	(:action move-left-from-r1285
		:precondition
			(and 
				(in r1285)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1285))
				(in r1284)
			)
	)

	(:action move-right-from-r1286
		:precondition
			(and 
				(in r1286)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1286))
				(in r1287)
			)
	)


	(:action move-left-from-r1286
		:precondition
			(and 
				(in r1286)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1286))
				(in r1285)
			)
	)

	(:action move-right-from-r1287
		:precondition
			(and 
				(in r1287)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1287))
				(in r1288)
			)
	)


	(:action move-left-from-r1287
		:precondition
			(and 
				(in r1287)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1287))
				(in r1286)
			)
	)

	(:action move-right-from-r1288
		:precondition
			(and 
				(in r1288)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1288))
				(in r1289)
			)
	)


	(:action move-left-from-r1288
		:precondition
			(and 
				(in r1288)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1288))
				(in r1287)
			)
	)

	(:action move-right-from-r1289
		:precondition
			(and 
				(in r1289)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1289))
				(in r1290)
			)
	)


	(:action move-left-from-r1289
		:precondition
			(and 
				(in r1289)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1289))
				(in r1288)
			)
	)

	(:action move-right-from-r1290
		:precondition
			(and 
				(in r1290)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1290))
				(in r1291)
			)
	)


	(:action move-left-from-r1290
		:precondition
			(and 
				(in r1290)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1290))
				(in r1289)
			)
	)

	(:action move-right-from-r1291
		:precondition
			(and 
				(in r1291)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1291))
				(in r1292)
			)
	)


	(:action move-left-from-r1291
		:precondition
			(and 
				(in r1291)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1291))
				(in r1290)
			)
	)

	(:action move-right-from-r1292
		:precondition
			(and 
				(in r1292)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1292))
				(in r1293)
			)
	)


	(:action move-left-from-r1292
		:precondition
			(and 
				(in r1292)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1292))
				(in r1291)
			)
	)

	(:action move-right-from-r1293
		:precondition
			(and 
				(in r1293)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1293))
				(in r1294)
			)
	)


	(:action move-left-from-r1293
		:precondition
			(and 
				(in r1293)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1293))
				(in r1292)
			)
	)

	(:action move-right-from-r1294
		:precondition
			(and 
				(in r1294)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1294))
				(in r1295)
			)
	)


	(:action move-left-from-r1294
		:precondition
			(and 
				(in r1294)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1294))
				(in r1293)
			)
	)

	(:action move-right-from-r1295
		:precondition
			(and 
				(in r1295)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1295))
				(in r1296)
			)
	)


	(:action move-left-from-r1295
		:precondition
			(and 
				(in r1295)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1295))
				(in r1294)
			)
	)

	(:action move-right-from-r1296
		:precondition
			(and 
				(in r1296)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1296))
				(in r1297)
			)
	)


	(:action move-left-from-r1296
		:precondition
			(and 
				(in r1296)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1296))
				(in r1295)
			)
	)

	(:action move-right-from-r1297
		:precondition
			(and 
				(in r1297)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1297))
				(in r1298)
			)
	)


	(:action move-left-from-r1297
		:precondition
			(and 
				(in r1297)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1297))
				(in r1296)
			)
	)

	(:action move-right-from-r1298
		:precondition
			(and 
				(in r1298)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1298))
				(in r1299)
			)
	)


	(:action move-left-from-r1298
		:precondition
			(and 
				(in r1298)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1298))
				(in r1297)
			)
	)

	(:action move-right-from-r1299
		:precondition
			(and 
				(in r1299)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1299))
				(in r1300)
			)
	)


	(:action move-left-from-r1299
		:precondition
			(and 
				(in r1299)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1299))
				(in r1298)
			)
	)

	(:action move-right-from-r1300
		:precondition
			(and 
				(in r1300)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1300))
				(in r1301)
			)
	)


	(:action move-left-from-r1300
		:precondition
			(and 
				(in r1300)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1300))
				(in r1299)
			)
	)

	(:action move-right-from-r1301
		:precondition
			(and 
				(in r1301)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1301))
				(in r1302)
			)
	)


	(:action move-left-from-r1301
		:precondition
			(and 
				(in r1301)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1301))
				(in r1300)
			)
	)

	(:action move-right-from-r1302
		:precondition
			(and 
				(in r1302)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1302))
				(in r1303)
			)
	)


	(:action move-left-from-r1302
		:precondition
			(and 
				(in r1302)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1302))
				(in r1301)
			)
	)

	(:action move-right-from-r1303
		:precondition
			(and 
				(in r1303)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1303))
				(in r1304)
			)
	)


	(:action move-left-from-r1303
		:precondition
			(and 
				(in r1303)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1303))
				(in r1302)
			)
	)

	(:action move-right-from-r1304
		:precondition
			(and 
				(in r1304)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1304))
				(in r1305)
			)
	)


	(:action move-left-from-r1304
		:precondition
			(and 
				(in r1304)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1304))
				(in r1303)
			)
	)

	(:action move-right-from-r1305
		:precondition
			(and 
				(in r1305)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1305))
				(in r1306)
			)
	)


	(:action move-left-from-r1305
		:precondition
			(and 
				(in r1305)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1305))
				(in r1304)
			)
	)

	(:action move-right-from-r1306
		:precondition
			(and 
				(in r1306)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1306))
				(in r1307)
			)
	)


	(:action move-left-from-r1306
		:precondition
			(and 
				(in r1306)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1306))
				(in r1305)
			)
	)

	(:action move-right-from-r1307
		:precondition
			(and 
				(in r1307)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1307))
				(in r1308)
			)
	)


	(:action move-left-from-r1307
		:precondition
			(and 
				(in r1307)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1307))
				(in r1306)
			)
	)

	(:action move-right-from-r1308
		:precondition
			(and 
				(in r1308)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1308))
				(in r1309)
			)
	)


	(:action move-left-from-r1308
		:precondition
			(and 
				(in r1308)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1308))
				(in r1307)
			)
	)

	(:action move-right-from-r1309
		:precondition
			(and 
				(in r1309)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1309))
				(in r1310)
			)
	)


	(:action move-left-from-r1309
		:precondition
			(and 
				(in r1309)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1309))
				(in r1308)
			)
	)

	(:action move-right-from-r1310
		:precondition
			(and 
				(in r1310)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1310))
				(in r1311)
			)
	)


	(:action move-left-from-r1310
		:precondition
			(and 
				(in r1310)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1310))
				(in r1309)
			)
	)

	(:action move-right-from-r1311
		:precondition
			(and 
				(in r1311)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1311))
				(in r1312)
			)
	)


	(:action move-left-from-r1311
		:precondition
			(and 
				(in r1311)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1311))
				(in r1310)
			)
	)

	(:action move-right-from-r1312
		:precondition
			(and 
				(in r1312)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1312))
				(in r1313)
			)
	)


	(:action move-left-from-r1312
		:precondition
			(and 
				(in r1312)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1312))
				(in r1311)
			)
	)

	(:action move-right-from-r1313
		:precondition
			(and 
				(in r1313)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1313))
				(in r1314)
			)
	)


	(:action move-left-from-r1313
		:precondition
			(and 
				(in r1313)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1313))
				(in r1312)
			)
	)

	(:action move-right-from-r1314
		:precondition
			(and 
				(in r1314)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1314))
				(in r1315)
			)
	)


	(:action move-left-from-r1314
		:precondition
			(and 
				(in r1314)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1314))
				(in r1313)
			)
	)

	(:action move-right-from-r1315
		:precondition
			(and 
				(in r1315)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1315))
				(in r1316)
			)
	)


	(:action move-left-from-r1315
		:precondition
			(and 
				(in r1315)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1315))
				(in r1314)
			)
	)

	(:action move-right-from-r1316
		:precondition
			(and 
				(in r1316)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1316))
				(in r1317)
			)
	)


	(:action move-left-from-r1316
		:precondition
			(and 
				(in r1316)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1316))
				(in r1315)
			)
	)

	(:action move-right-from-r1317
		:precondition
			(and 
				(in r1317)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1317))
				(in r1318)
			)
	)


	(:action move-left-from-r1317
		:precondition
			(and 
				(in r1317)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1317))
				(in r1316)
			)
	)

	(:action move-right-from-r1318
		:precondition
			(and 
				(in r1318)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1318))
				(in r1319)
			)
	)


	(:action move-left-from-r1318
		:precondition
			(and 
				(in r1318)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1318))
				(in r1317)
			)
	)

	(:action move-right-from-r1319
		:precondition
			(and 
				(in r1319)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1319))
				(in r1320)
			)
	)


	(:action move-left-from-r1319
		:precondition
			(and 
				(in r1319)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1319))
				(in r1318)
			)
	)

	(:action move-right-from-r1320
		:precondition
			(and 
				(in r1320)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1320))
				(in r1321)
			)
	)


	(:action move-left-from-r1320
		:precondition
			(and 
				(in r1320)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1320))
				(in r1319)
			)
	)

	(:action move-right-from-r1321
		:precondition
			(and 
				(in r1321)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1321))
				(in r1322)
			)
	)


	(:action move-left-from-r1321
		:precondition
			(and 
				(in r1321)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1321))
				(in r1320)
			)
	)

	(:action move-right-from-r1322
		:precondition
			(and 
				(in r1322)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1322))
				(in r1323)
			)
	)


	(:action move-left-from-r1322
		:precondition
			(and 
				(in r1322)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1322))
				(in r1321)
			)
	)

	(:action move-right-from-r1323
		:precondition
			(and 
				(in r1323)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1323))
				(in r1324)
			)
	)


	(:action move-left-from-r1323
		:precondition
			(and 
				(in r1323)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1323))
				(in r1322)
			)
	)

	(:action move-right-from-r1324
		:precondition
			(and 
				(in r1324)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1324))
				(in r1325)
			)
	)


	(:action move-left-from-r1324
		:precondition
			(and 
				(in r1324)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1324))
				(in r1323)
			)
	)

	(:action move-right-from-r1325
		:precondition
			(and 
				(in r1325)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1325))
				(in r1326)
			)
	)


	(:action move-left-from-r1325
		:precondition
			(and 
				(in r1325)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1325))
				(in r1324)
			)
	)

	(:action move-right-from-r1326
		:precondition
			(and 
				(in r1326)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1326))
				(in r1327)
			)
	)


	(:action move-left-from-r1326
		:precondition
			(and 
				(in r1326)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1326))
				(in r1325)
			)
	)

	(:action move-right-from-r1327
		:precondition
			(and 
				(in r1327)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1327))
				(in r1328)
			)
	)


	(:action move-left-from-r1327
		:precondition
			(and 
				(in r1327)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1327))
				(in r1326)
			)
	)

	(:action move-right-from-r1328
		:precondition
			(and 
				(in r1328)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1328))
				(in r1329)
			)
	)


	(:action move-left-from-r1328
		:precondition
			(and 
				(in r1328)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1328))
				(in r1327)
			)
	)

	(:action move-right-from-r1329
		:precondition
			(and 
				(in r1329)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1329))
				(in r1330)
			)
	)


	(:action move-left-from-r1329
		:precondition
			(and 
				(in r1329)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1329))
				(in r1328)
			)
	)

	(:action move-right-from-r1330
		:precondition
			(and 
				(in r1330)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1330))
				(in r1331)
			)
	)


	(:action move-left-from-r1330
		:precondition
			(and 
				(in r1330)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1330))
				(in r1329)
			)
	)

	(:action move-right-from-r1331
		:precondition
			(and 
				(in r1331)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1331))
				(in r1332)
			)
	)


	(:action move-left-from-r1331
		:precondition
			(and 
				(in r1331)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1331))
				(in r1330)
			)
	)

	(:action move-right-from-r1332
		:precondition
			(and 
				(in r1332)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1332))
				(in r1333)
			)
	)


	(:action move-left-from-r1332
		:precondition
			(and 
				(in r1332)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1332))
				(in r1331)
			)
	)

	(:action move-right-from-r1333
		:precondition
			(and 
				(in r1333)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1333))
				(in r1334)
			)
	)


	(:action move-left-from-r1333
		:precondition
			(and 
				(in r1333)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1333))
				(in r1332)
			)
	)

	(:action move-right-from-r1334
		:precondition
			(and 
				(in r1334)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1334))
				(in r1335)
			)
	)


	(:action move-left-from-r1334
		:precondition
			(and 
				(in r1334)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1334))
				(in r1333)
			)
	)

	(:action move-right-from-r1335
		:precondition
			(and 
				(in r1335)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1335))
				(in r1336)
			)
	)


	(:action move-left-from-r1335
		:precondition
			(and 
				(in r1335)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1335))
				(in r1334)
			)
	)

	(:action move-right-from-r1336
		:precondition
			(and 
				(in r1336)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1336))
				(in r1337)
			)
	)


	(:action move-left-from-r1336
		:precondition
			(and 
				(in r1336)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1336))
				(in r1335)
			)
	)

	(:action move-right-from-r1337
		:precondition
			(and 
				(in r1337)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1337))
				(in r1338)
			)
	)


	(:action move-left-from-r1337
		:precondition
			(and 
				(in r1337)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1337))
				(in r1336)
			)
	)

	(:action move-right-from-r1338
		:precondition
			(and 
				(in r1338)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1338))
				(in r1339)
			)
	)


	(:action move-left-from-r1338
		:precondition
			(and 
				(in r1338)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1338))
				(in r1337)
			)
	)

	(:action move-right-from-r1339
		:precondition
			(and 
				(in r1339)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1339))
				(in r1340)
			)
	)


	(:action move-left-from-r1339
		:precondition
			(and 
				(in r1339)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1339))
				(in r1338)
			)
	)

	(:action move-right-from-r1340
		:precondition
			(and 
				(in r1340)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1340))
				(in r1341)
			)
	)


	(:action move-left-from-r1340
		:precondition
			(and 
				(in r1340)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1340))
				(in r1339)
			)
	)

	(:action move-right-from-r1341
		:precondition
			(and 
				(in r1341)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1341))
				(in r1342)
			)
	)


	(:action move-left-from-r1341
		:precondition
			(and 
				(in r1341)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1341))
				(in r1340)
			)
	)

	(:action move-right-from-r1342
		:precondition
			(and 
				(in r1342)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1342))
				(in r1343)
			)
	)


	(:action move-left-from-r1342
		:precondition
			(and 
				(in r1342)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1342))
				(in r1341)
			)
	)

	(:action move-right-from-r1343
		:precondition
			(and 
				(in r1343)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1343))
				(in r1344)
			)
	)


	(:action move-left-from-r1343
		:precondition
			(and 
				(in r1343)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1343))
				(in r1342)
			)
	)

	(:action move-right-from-r1344
		:precondition
			(and 
				(in r1344)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1344))
				(in r1345)
			)
	)


	(:action move-left-from-r1344
		:precondition
			(and 
				(in r1344)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1344))
				(in r1343)
			)
	)

	(:action move-right-from-r1345
		:precondition
			(and 
				(in r1345)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1345))
				(in r1346)
			)
	)


	(:action move-left-from-r1345
		:precondition
			(and 
				(in r1345)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1345))
				(in r1344)
			)
	)

	(:action move-right-from-r1346
		:precondition
			(and 
				(in r1346)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1346))
				(in r1347)
			)
	)


	(:action move-left-from-r1346
		:precondition
			(and 
				(in r1346)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1346))
				(in r1345)
			)
	)

	(:action move-right-from-r1347
		:precondition
			(and 
				(in r1347)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1347))
				(in r1348)
			)
	)


	(:action move-left-from-r1347
		:precondition
			(and 
				(in r1347)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1347))
				(in r1346)
			)
	)

	(:action move-right-from-r1348
		:precondition
			(and 
				(in r1348)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1348))
				(in r1349)
			)
	)


	(:action move-left-from-r1348
		:precondition
			(and 
				(in r1348)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1348))
				(in r1347)
			)
	)

	(:action move-right-from-r1349
		:precondition
			(and 
				(in r1349)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1349))
				(in r1350)
			)
	)


	(:action move-left-from-r1349
		:precondition
			(and 
				(in r1349)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1349))
				(in r1348)
			)
	)

	(:action move-right-from-r1350
		:precondition
			(and 
				(in r1350)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1350))
				(in r1351)
			)
	)


	(:action move-left-from-r1350
		:precondition
			(and 
				(in r1350)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1350))
				(in r1349)
			)
	)

	(:action move-right-from-r1351
		:precondition
			(and 
				(in r1351)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1351))
				(in r1352)
			)
	)


	(:action move-left-from-r1351
		:precondition
			(and 
				(in r1351)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1351))
				(in r1350)
			)
	)

	(:action move-right-from-r1352
		:precondition
			(and 
				(in r1352)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1352))
				(in r1353)
			)
	)


	(:action move-left-from-r1352
		:precondition
			(and 
				(in r1352)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1352))
				(in r1351)
			)
	)

	(:action move-right-from-r1353
		:precondition
			(and 
				(in r1353)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1353))
				(in r1354)
			)
	)


	(:action move-left-from-r1353
		:precondition
			(and 
				(in r1353)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1353))
				(in r1352)
			)
	)

	(:action move-right-from-r1354
		:precondition
			(and 
				(in r1354)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1354))
				(in r1355)
			)
	)


	(:action move-left-from-r1354
		:precondition
			(and 
				(in r1354)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1354))
				(in r1353)
			)
	)

	(:action move-right-from-r1355
		:precondition
			(and 
				(in r1355)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1355))
				(in r1356)
			)
	)


	(:action move-left-from-r1355
		:precondition
			(and 
				(in r1355)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1355))
				(in r1354)
			)
	)

	(:action move-right-from-r1356
		:precondition
			(and 
				(in r1356)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1356))
				(in r1357)
			)
	)


	(:action move-left-from-r1356
		:precondition
			(and 
				(in r1356)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1356))
				(in r1355)
			)
	)

	(:action move-right-from-r1357
		:precondition
			(and 
				(in r1357)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1357))
				(in r1358)
			)
	)


	(:action move-left-from-r1357
		:precondition
			(and 
				(in r1357)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1357))
				(in r1356)
			)
	)

	(:action move-right-from-r1358
		:precondition
			(and 
				(in r1358)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1358))
				(in r1359)
			)
	)


	(:action move-left-from-r1358
		:precondition
			(and 
				(in r1358)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1358))
				(in r1357)
			)
	)

	(:action move-right-from-r1359
		:precondition
			(and 
				(in r1359)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1359))
				(in r1360)
			)
	)


	(:action move-left-from-r1359
		:precondition
			(and 
				(in r1359)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1359))
				(in r1358)
			)
	)

	(:action move-right-from-r1360
		:precondition
			(and 
				(in r1360)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1360))
				(in r1361)
			)
	)


	(:action move-left-from-r1360
		:precondition
			(and 
				(in r1360)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1360))
				(in r1359)
			)
	)

	(:action move-right-from-r1361
		:precondition
			(and 
				(in r1361)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1361))
				(in r1362)
			)
	)


	(:action move-left-from-r1361
		:precondition
			(and 
				(in r1361)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1361))
				(in r1360)
			)
	)

	(:action move-right-from-r1362
		:precondition
			(and 
				(in r1362)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1362))
				(in r1363)
			)
	)


	(:action move-left-from-r1362
		:precondition
			(and 
				(in r1362)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1362))
				(in r1361)
			)
	)

	(:action move-right-from-r1363
		:precondition
			(and 
				(in r1363)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1363))
				(in r1364)
			)
	)


	(:action move-left-from-r1363
		:precondition
			(and 
				(in r1363)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1363))
				(in r1362)
			)
	)

	(:action move-right-from-r1364
		:precondition
			(and 
				(in r1364)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1364))
				(in r1365)
			)
	)


	(:action move-left-from-r1364
		:precondition
			(and 
				(in r1364)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1364))
				(in r1363)
			)
	)

	(:action move-right-from-r1365
		:precondition
			(and 
				(in r1365)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1365))
				(in r1366)
			)
	)


	(:action move-left-from-r1365
		:precondition
			(and 
				(in r1365)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1365))
				(in r1364)
			)
	)

	(:action move-right-from-r1366
		:precondition
			(and 
				(in r1366)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1366))
				(in r1367)
			)
	)


	(:action move-left-from-r1366
		:precondition
			(and 
				(in r1366)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1366))
				(in r1365)
			)
	)

	(:action move-right-from-r1367
		:precondition
			(and 
				(in r1367)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1367))
				(in r1368)
			)
	)


	(:action move-left-from-r1367
		:precondition
			(and 
				(in r1367)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1367))
				(in r1366)
			)
	)

	(:action move-right-from-r1368
		:precondition
			(and 
				(in r1368)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1368))
				(in r1369)
			)
	)


	(:action move-left-from-r1368
		:precondition
			(and 
				(in r1368)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1368))
				(in r1367)
			)
	)

	(:action move-right-from-r1369
		:precondition
			(and 
				(in r1369)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1369))
				(in r1370)
			)
	)


	(:action move-left-from-r1369
		:precondition
			(and 
				(in r1369)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1369))
				(in r1368)
			)
	)

	(:action move-right-from-r1370
		:precondition
			(and 
				(in r1370)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1370))
				(in r1371)
			)
	)


	(:action move-left-from-r1370
		:precondition
			(and 
				(in r1370)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1370))
				(in r1369)
			)
	)

	(:action move-right-from-r1371
		:precondition
			(and 
				(in r1371)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1371))
				(in r1372)
			)
	)


	(:action move-left-from-r1371
		:precondition
			(and 
				(in r1371)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1371))
				(in r1370)
			)
	)

	(:action move-right-from-r1372
		:precondition
			(and 
				(in r1372)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1372))
				(in r1373)
			)
	)


	(:action move-left-from-r1372
		:precondition
			(and 
				(in r1372)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1372))
				(in r1371)
			)
	)

	(:action move-right-from-r1373
		:precondition
			(and 
				(in r1373)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1373))
				(in r1374)
			)
	)


	(:action move-left-from-r1373
		:precondition
			(and 
				(in r1373)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1373))
				(in r1372)
			)
	)

	(:action move-right-from-r1374
		:precondition
			(and 
				(in r1374)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1374))
				(in r1375)
			)
	)


	(:action move-left-from-r1374
		:precondition
			(and 
				(in r1374)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1374))
				(in r1373)
			)
	)

	(:action move-right-from-r1375
		:precondition
			(and 
				(in r1375)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1375))
				(in r1376)
			)
	)


	(:action move-left-from-r1375
		:precondition
			(and 
				(in r1375)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1375))
				(in r1374)
			)
	)

	(:action move-right-from-r1376
		:precondition
			(and 
				(in r1376)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1376))
				(in r1377)
			)
	)


	(:action move-left-from-r1376
		:precondition
			(and 
				(in r1376)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1376))
				(in r1375)
			)
	)

	(:action move-right-from-r1377
		:precondition
			(and 
				(in r1377)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1377))
				(in r1378)
			)
	)


	(:action move-left-from-r1377
		:precondition
			(and 
				(in r1377)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1377))
				(in r1376)
			)
	)

	(:action move-right-from-r1378
		:precondition
			(and 
				(in r1378)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1378))
				(in r1379)
			)
	)


	(:action move-left-from-r1378
		:precondition
			(and 
				(in r1378)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1378))
				(in r1377)
			)
	)

	(:action move-right-from-r1379
		:precondition
			(and 
				(in r1379)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1379))
				(in r1380)
			)
	)


	(:action move-left-from-r1379
		:precondition
			(and 
				(in r1379)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1379))
				(in r1378)
			)
	)

	(:action move-right-from-r1380
		:precondition
			(and 
				(in r1380)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1380))
				(in r1381)
			)
	)


	(:action move-left-from-r1380
		:precondition
			(and 
				(in r1380)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1380))
				(in r1379)
			)
	)

	(:action move-right-from-r1381
		:precondition
			(and 
				(in r1381)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1381))
				(in r1382)
			)
	)


	(:action move-left-from-r1381
		:precondition
			(and 
				(in r1381)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1381))
				(in r1380)
			)
	)

	(:action move-right-from-r1382
		:precondition
			(and 
				(in r1382)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1382))
				(in r1383)
			)
	)


	(:action move-left-from-r1382
		:precondition
			(and 
				(in r1382)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1382))
				(in r1381)
			)
	)

	(:action move-right-from-r1383
		:precondition
			(and 
				(in r1383)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1383))
				(in r1384)
			)
	)


	(:action move-left-from-r1383
		:precondition
			(and 
				(in r1383)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1383))
				(in r1382)
			)
	)

	(:action move-right-from-r1384
		:precondition
			(and 
				(in r1384)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1384))
				(in r1385)
			)
	)


	(:action move-left-from-r1384
		:precondition
			(and 
				(in r1384)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1384))
				(in r1383)
			)
	)

	(:action move-right-from-r1385
		:precondition
			(and 
				(in r1385)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1385))
				(in r1386)
			)
	)


	(:action move-left-from-r1385
		:precondition
			(and 
				(in r1385)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1385))
				(in r1384)
			)
	)

	(:action move-right-from-r1386
		:precondition
			(and 
				(in r1386)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1386))
				(in r1387)
			)
	)


	(:action move-left-from-r1386
		:precondition
			(and 
				(in r1386)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1386))
				(in r1385)
			)
	)

	(:action move-right-from-r1387
		:precondition
			(and 
				(in r1387)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1387))
				(in r1388)
			)
	)


	(:action move-left-from-r1387
		:precondition
			(and 
				(in r1387)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1387))
				(in r1386)
			)
	)

	(:action move-right-from-r1388
		:precondition
			(and 
				(in r1388)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1388))
				(in r1389)
			)
	)


	(:action move-left-from-r1388
		:precondition
			(and 
				(in r1388)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1388))
				(in r1387)
			)
	)

	(:action move-right-from-r1389
		:precondition
			(and 
				(in r1389)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1389))
				(in r1390)
			)
	)


	(:action move-left-from-r1389
		:precondition
			(and 
				(in r1389)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1389))
				(in r1388)
			)
	)

	(:action move-right-from-r1390
		:precondition
			(and 
				(in r1390)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1390))
				(in r1391)
			)
	)


	(:action move-left-from-r1390
		:precondition
			(and 
				(in r1390)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1390))
				(in r1389)
			)
	)

	(:action move-right-from-r1391
		:precondition
			(and 
				(in r1391)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1391))
				(in r1392)
			)
	)


	(:action move-left-from-r1391
		:precondition
			(and 
				(in r1391)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1391))
				(in r1390)
			)
	)

	(:action move-right-from-r1392
		:precondition
			(and 
				(in r1392)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1392))
				(in r1393)
			)
	)


	(:action move-left-from-r1392
		:precondition
			(and 
				(in r1392)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1392))
				(in r1391)
			)
	)

	(:action move-right-from-r1393
		:precondition
			(and 
				(in r1393)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1393))
				(in r1394)
			)
	)


	(:action move-left-from-r1393
		:precondition
			(and 
				(in r1393)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1393))
				(in r1392)
			)
	)

	(:action move-right-from-r1394
		:precondition
			(and 
				(in r1394)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1394))
				(in r1395)
			)
	)


	(:action move-left-from-r1394
		:precondition
			(and 
				(in r1394)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1394))
				(in r1393)
			)
	)

	(:action move-right-from-r1395
		:precondition
			(and 
				(in r1395)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1395))
				(in r1396)
			)
	)


	(:action move-left-from-r1395
		:precondition
			(and 
				(in r1395)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1395))
				(in r1394)
			)
	)

	(:action move-right-from-r1396
		:precondition
			(and 
				(in r1396)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1396))
				(in r1397)
			)
	)


	(:action move-left-from-r1396
		:precondition
			(and 
				(in r1396)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1396))
				(in r1395)
			)
	)

	(:action move-right-from-r1397
		:precondition
			(and 
				(in r1397)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1397))
				(in r1398)
			)
	)


	(:action move-left-from-r1397
		:precondition
			(and 
				(in r1397)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1397))
				(in r1396)
			)
	)

	(:action move-right-from-r1398
		:precondition
			(and 
				(in r1398)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1398))
				(in r1399)
			)
	)


	(:action move-left-from-r1398
		:precondition
			(and 
				(in r1398)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1398))
				(in r1397)
			)
	)

	(:action move-right-from-r1399
		:precondition
			(and 
				(in r1399)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1399))
				(in r1400)
			)
	)


	(:action move-left-from-r1399
		:precondition
			(and 
				(in r1399)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1399))
				(in r1398)
			)
	)

	(:action move-right-from-r1400
		:precondition
			(and 
				(in r1400)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1400))
				(in r1401)
			)
	)


	(:action move-left-from-r1400
		:precondition
			(and 
				(in r1400)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1400))
				(in r1399)
			)
	)

	(:action move-right-from-r1401
		:precondition
			(and 
				(in r1401)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1401))
				(in r1402)
			)
	)


	(:action move-left-from-r1401
		:precondition
			(and 
				(in r1401)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1401))
				(in r1400)
			)
	)

	(:action move-right-from-r1402
		:precondition
			(and 
				(in r1402)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1402))
				(in r1403)
			)
	)


	(:action move-left-from-r1402
		:precondition
			(and 
				(in r1402)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1402))
				(in r1401)
			)
	)

	(:action move-right-from-r1403
		:precondition
			(and 
				(in r1403)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1403))
				(in r1404)
			)
	)


	(:action move-left-from-r1403
		:precondition
			(and 
				(in r1403)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1403))
				(in r1402)
			)
	)

	(:action move-right-from-r1404
		:precondition
			(and 
				(in r1404)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1404))
				(in r1405)
			)
	)


	(:action move-left-from-r1404
		:precondition
			(and 
				(in r1404)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1404))
				(in r1403)
			)
	)

	(:action move-right-from-r1405
		:precondition
			(and 
				(in r1405)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1405))
				(in r1406)
			)
	)


	(:action move-left-from-r1405
		:precondition
			(and 
				(in r1405)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1405))
				(in r1404)
			)
	)

	(:action move-right-from-r1406
		:precondition
			(and 
				(in r1406)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1406))
				(in r1407)
			)
	)


	(:action move-left-from-r1406
		:precondition
			(and 
				(in r1406)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1406))
				(in r1405)
			)
	)

	(:action move-right-from-r1407
		:precondition
			(and 
				(in r1407)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1407))
				(in r1408)
			)
	)


	(:action move-left-from-r1407
		:precondition
			(and 
				(in r1407)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1407))
				(in r1406)
			)
	)

	(:action move-right-from-r1408
		:precondition
			(and 
				(in r1408)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1408))
				(in r1409)
			)
	)


	(:action move-left-from-r1408
		:precondition
			(and 
				(in r1408)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1408))
				(in r1407)
			)
	)

	(:action move-right-from-r1409
		:precondition
			(and 
				(in r1409)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1409))
				(in r1410)
			)
	)


	(:action move-left-from-r1409
		:precondition
			(and 
				(in r1409)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1409))
				(in r1408)
			)
	)

	(:action move-right-from-r1410
		:precondition
			(and 
				(in r1410)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1410))
				(in r1411)
			)
	)


	(:action move-left-from-r1410
		:precondition
			(and 
				(in r1410)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1410))
				(in r1409)
			)
	)

	(:action move-right-from-r1411
		:precondition
			(and 
				(in r1411)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1411))
				(in r1412)
			)
	)


	(:action move-left-from-r1411
		:precondition
			(and 
				(in r1411)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1411))
				(in r1410)
			)
	)

	(:action move-right-from-r1412
		:precondition
			(and 
				(in r1412)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1412))
				(in r1413)
			)
	)


	(:action move-left-from-r1412
		:precondition
			(and 
				(in r1412)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1412))
				(in r1411)
			)
	)

	(:action move-right-from-r1413
		:precondition
			(and 
				(in r1413)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1413))
				(in r1414)
			)
	)


	(:action move-left-from-r1413
		:precondition
			(and 
				(in r1413)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1413))
				(in r1412)
			)
	)

	(:action move-right-from-r1414
		:precondition
			(and 
				(in r1414)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1414))
				(in r1415)
			)
	)


	(:action move-left-from-r1414
		:precondition
			(and 
				(in r1414)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1414))
				(in r1413)
			)
	)

	(:action move-right-from-r1415
		:precondition
			(and 
				(in r1415)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1415))
				(in r1416)
			)
	)


	(:action move-left-from-r1415
		:precondition
			(and 
				(in r1415)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1415))
				(in r1414)
			)
	)

	(:action move-right-from-r1416
		:precondition
			(and 
				(in r1416)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1416))
				(in r1417)
			)
	)


	(:action move-left-from-r1416
		:precondition
			(and 
				(in r1416)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1416))
				(in r1415)
			)
	)

	(:action move-right-from-r1417
		:precondition
			(and 
				(in r1417)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1417))
				(in r1418)
			)
	)


	(:action move-left-from-r1417
		:precondition
			(and 
				(in r1417)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1417))
				(in r1416)
			)
	)

	(:action move-right-from-r1418
		:precondition
			(and 
				(in r1418)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1418))
				(in r1419)
			)
	)


	(:action move-left-from-r1418
		:precondition
			(and 
				(in r1418)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1418))
				(in r1417)
			)
	)

	(:action move-right-from-r1419
		:precondition
			(and 
				(in r1419)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1419))
				(in r1420)
			)
	)


	(:action move-left-from-r1419
		:precondition
			(and 
				(in r1419)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1419))
				(in r1418)
			)
	)

	(:action move-right-from-r1420
		:precondition
			(and 
				(in r1420)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1420))
				(in r1421)
			)
	)


	(:action move-left-from-r1420
		:precondition
			(and 
				(in r1420)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1420))
				(in r1419)
			)
	)

	(:action move-right-from-r1421
		:precondition
			(and 
				(in r1421)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1421))
				(in r1422)
			)
	)


	(:action move-left-from-r1421
		:precondition
			(and 
				(in r1421)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1421))
				(in r1420)
			)
	)

	(:action move-right-from-r1422
		:precondition
			(and 
				(in r1422)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1422))
				(in r1423)
			)
	)


	(:action move-left-from-r1422
		:precondition
			(and 
				(in r1422)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1422))
				(in r1421)
			)
	)

	(:action move-right-from-r1423
		:precondition
			(and 
				(in r1423)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1423))
				(in r1424)
			)
	)


	(:action move-left-from-r1423
		:precondition
			(and 
				(in r1423)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1423))
				(in r1422)
			)
	)

	(:action move-right-from-r1424
		:precondition
			(and 
				(in r1424)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1424))
				(in r1425)
			)
	)


	(:action move-left-from-r1424
		:precondition
			(and 
				(in r1424)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1424))
				(in r1423)
			)
	)

	(:action move-right-from-r1425
		:precondition
			(and 
				(in r1425)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1425))
				(in r1426)
			)
	)


	(:action move-left-from-r1425
		:precondition
			(and 
				(in r1425)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1425))
				(in r1424)
			)
	)

	(:action move-right-from-r1426
		:precondition
			(and 
				(in r1426)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1426))
				(in r1427)
			)
	)


	(:action move-left-from-r1426
		:precondition
			(and 
				(in r1426)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1426))
				(in r1425)
			)
	)

	(:action move-right-from-r1427
		:precondition
			(and 
				(in r1427)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1427))
				(in r1428)
			)
	)


	(:action move-left-from-r1427
		:precondition
			(and 
				(in r1427)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1427))
				(in r1426)
			)
	)

	(:action move-right-from-r1428
		:precondition
			(and 
				(in r1428)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1428))
				(in r1429)
			)
	)


	(:action move-left-from-r1428
		:precondition
			(and 
				(in r1428)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1428))
				(in r1427)
			)
	)

	(:action move-right-from-r1429
		:precondition
			(and 
				(in r1429)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1429))
				(in r1430)
			)
	)


	(:action move-left-from-r1429
		:precondition
			(and 
				(in r1429)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1429))
				(in r1428)
			)
	)

	(:action move-right-from-r1430
		:precondition
			(and 
				(in r1430)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1430))
				(in r1431)
			)
	)


	(:action move-left-from-r1430
		:precondition
			(and 
				(in r1430)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1430))
				(in r1429)
			)
	)

	(:action move-right-from-r1431
		:precondition
			(and 
				(in r1431)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1431))
				(in r1432)
			)
	)


	(:action move-left-from-r1431
		:precondition
			(and 
				(in r1431)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1431))
				(in r1430)
			)
	)

	(:action move-right-from-r1432
		:precondition
			(and 
				(in r1432)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1432))
				(in r1433)
			)
	)


	(:action move-left-from-r1432
		:precondition
			(and 
				(in r1432)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1432))
				(in r1431)
			)
	)

	(:action move-right-from-r1433
		:precondition
			(and 
				(in r1433)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1433))
				(in r1434)
			)
	)


	(:action move-left-from-r1433
		:precondition
			(and 
				(in r1433)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1433))
				(in r1432)
			)
	)

	(:action move-right-from-r1434
		:precondition
			(and 
				(in r1434)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1434))
				(in r1435)
			)
	)


	(:action move-left-from-r1434
		:precondition
			(and 
				(in r1434)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1434))
				(in r1433)
			)
	)

	(:action move-right-from-r1435
		:precondition
			(and 
				(in r1435)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1435))
				(in r1436)
			)
	)


	(:action move-left-from-r1435
		:precondition
			(and 
				(in r1435)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1435))
				(in r1434)
			)
	)

	(:action move-right-from-r1436
		:precondition
			(and 
				(in r1436)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1436))
				(in r1437)
			)
	)


	(:action move-left-from-r1436
		:precondition
			(and 
				(in r1436)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1436))
				(in r1435)
			)
	)

	(:action move-right-from-r1437
		:precondition
			(and 
				(in r1437)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1437))
				(in r1438)
			)
	)


	(:action move-left-from-r1437
		:precondition
			(and 
				(in r1437)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1437))
				(in r1436)
			)
	)

	(:action move-right-from-r1438
		:precondition
			(and 
				(in r1438)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1438))
				(in r1439)
			)
	)


	(:action move-left-from-r1438
		:precondition
			(and 
				(in r1438)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1438))
				(in r1437)
			)
	)

	(:action move-right-from-r1439
		:precondition
			(and 
				(in r1439)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1439))
				(in r1440)
			)
	)


	(:action move-left-from-r1439
		:precondition
			(and 
				(in r1439)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1439))
				(in r1438)
			)
	)

	(:action move-right-from-r1440
		:precondition
			(and 
				(in r1440)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1440))
				(in r1441)
			)
	)


	(:action move-left-from-r1440
		:precondition
			(and 
				(in r1440)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1440))
				(in r1439)
			)
	)

	(:action move-right-from-r1441
		:precondition
			(and 
				(in r1441)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1441))
				(in r1442)
			)
	)


	(:action move-left-from-r1441
		:precondition
			(and 
				(in r1441)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1441))
				(in r1440)
			)
	)

	(:action move-right-from-r1442
		:precondition
			(and 
				(in r1442)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1442))
				(in r1443)
			)
	)


	(:action move-left-from-r1442
		:precondition
			(and 
				(in r1442)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1442))
				(in r1441)
			)
	)

	(:action move-right-from-r1443
		:precondition
			(and 
				(in r1443)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1443))
				(in r1444)
			)
	)


	(:action move-left-from-r1443
		:precondition
			(and 
				(in r1443)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1443))
				(in r1442)
			)
	)

	(:action move-right-from-r1444
		:precondition
			(and 
				(in r1444)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1444))
				(in r1445)
			)
	)


	(:action move-left-from-r1444
		:precondition
			(and 
				(in r1444)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1444))
				(in r1443)
			)
	)

	(:action move-right-from-r1445
		:precondition
			(and 
				(in r1445)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1445))
				(in r1446)
			)
	)


	(:action move-left-from-r1445
		:precondition
			(and 
				(in r1445)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1445))
				(in r1444)
			)
	)

	(:action move-right-from-r1446
		:precondition
			(and 
				(in r1446)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1446))
				(in r1447)
			)
	)


	(:action move-left-from-r1446
		:precondition
			(and 
				(in r1446)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1446))
				(in r1445)
			)
	)

	(:action move-right-from-r1447
		:precondition
			(and 
				(in r1447)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1447))
				(in r1448)
			)
	)


	(:action move-left-from-r1447
		:precondition
			(and 
				(in r1447)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1447))
				(in r1446)
			)
	)

	(:action move-right-from-r1448
		:precondition
			(and 
				(in r1448)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1448))
				(in r1449)
			)
	)


	(:action move-left-from-r1448
		:precondition
			(and 
				(in r1448)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1448))
				(in r1447)
			)
	)

	(:action move-right-from-r1449
		:precondition
			(and 
				(in r1449)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1449))
				(in r1450)
			)
	)


	(:action move-left-from-r1449
		:precondition
			(and 
				(in r1449)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1449))
				(in r1448)
			)
	)

	(:action move-right-from-r1450
		:precondition
			(and 
				(in r1450)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1450))
				(in r1451)
			)
	)


	(:action move-left-from-r1450
		:precondition
			(and 
				(in r1450)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1450))
				(in r1449)
			)
	)

	(:action move-right-from-r1451
		:precondition
			(and 
				(in r1451)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1451))
				(in r1452)
			)
	)


	(:action move-left-from-r1451
		:precondition
			(and 
				(in r1451)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1451))
				(in r1450)
			)
	)

	(:action move-right-from-r1452
		:precondition
			(and 
				(in r1452)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1452))
				(in r1453)
			)
	)


	(:action move-left-from-r1452
		:precondition
			(and 
				(in r1452)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1452))
				(in r1451)
			)
	)

	(:action move-right-from-r1453
		:precondition
			(and 
				(in r1453)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1453))
				(in r1454)
			)
	)


	(:action move-left-from-r1453
		:precondition
			(and 
				(in r1453)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1453))
				(in r1452)
			)
	)

	(:action move-right-from-r1454
		:precondition
			(and 
				(in r1454)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1454))
				(in r1455)
			)
	)


	(:action move-left-from-r1454
		:precondition
			(and 
				(in r1454)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1454))
				(in r1453)
			)
	)

	(:action move-right-from-r1455
		:precondition
			(and 
				(in r1455)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1455))
				(in r1456)
			)
	)


	(:action move-left-from-r1455
		:precondition
			(and 
				(in r1455)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1455))
				(in r1454)
			)
	)

	(:action move-right-from-r1456
		:precondition
			(and 
				(in r1456)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1456))
				(in r1457)
			)
	)


	(:action move-left-from-r1456
		:precondition
			(and 
				(in r1456)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1456))
				(in r1455)
			)
	)

	(:action move-right-from-r1457
		:precondition
			(and 
				(in r1457)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1457))
				(in r1458)
			)
	)


	(:action move-left-from-r1457
		:precondition
			(and 
				(in r1457)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1457))
				(in r1456)
			)
	)

	(:action move-right-from-r1458
		:precondition
			(and 
				(in r1458)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1458))
				(in r1459)
			)
	)


	(:action move-left-from-r1458
		:precondition
			(and 
				(in r1458)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1458))
				(in r1457)
			)
	)

	(:action move-right-from-r1459
		:precondition
			(and 
				(in r1459)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1459))
				(in r1460)
			)
	)


	(:action move-left-from-r1459
		:precondition
			(and 
				(in r1459)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1459))
				(in r1458)
			)
	)

	(:action move-right-from-r1460
		:precondition
			(and 
				(in r1460)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1460))
				(in r1461)
			)
	)


	(:action move-left-from-r1460
		:precondition
			(and 
				(in r1460)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1460))
				(in r1459)
			)
	)

	(:action move-right-from-r1461
		:precondition
			(and 
				(in r1461)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1461))
				(in r1462)
			)
	)


	(:action move-left-from-r1461
		:precondition
			(and 
				(in r1461)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1461))
				(in r1460)
			)
	)

	(:action move-right-from-r1462
		:precondition
			(and 
				(in r1462)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1462))
				(in r1463)
			)
	)


	(:action move-left-from-r1462
		:precondition
			(and 
				(in r1462)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1462))
				(in r1461)
			)
	)

	(:action move-right-from-r1463
		:precondition
			(and 
				(in r1463)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1463))
				(in r1464)
			)
	)


	(:action move-left-from-r1463
		:precondition
			(and 
				(in r1463)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1463))
				(in r1462)
			)
	)

	(:action move-right-from-r1464
		:precondition
			(and 
				(in r1464)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1464))
				(in r1465)
			)
	)


	(:action move-left-from-r1464
		:precondition
			(and 
				(in r1464)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1464))
				(in r1463)
			)
	)

	(:action move-right-from-r1465
		:precondition
			(and 
				(in r1465)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1465))
				(in r1466)
			)
	)


	(:action move-left-from-r1465
		:precondition
			(and 
				(in r1465)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1465))
				(in r1464)
			)
	)

	(:action move-right-from-r1466
		:precondition
			(and 
				(in r1466)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1466))
				(in r1467)
			)
	)


	(:action move-left-from-r1466
		:precondition
			(and 
				(in r1466)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1466))
				(in r1465)
			)
	)

	(:action move-right-from-r1467
		:precondition
			(and 
				(in r1467)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1467))
				(in r1468)
			)
	)


	(:action move-left-from-r1467
		:precondition
			(and 
				(in r1467)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1467))
				(in r1466)
			)
	)

	(:action move-right-from-r1468
		:precondition
			(and 
				(in r1468)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1468))
				(in r1469)
			)
	)


	(:action move-left-from-r1468
		:precondition
			(and 
				(in r1468)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1468))
				(in r1467)
			)
	)

	(:action move-right-from-r1469
		:precondition
			(and 
				(in r1469)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1469))
				(in r1470)
			)
	)


	(:action move-left-from-r1469
		:precondition
			(and 
				(in r1469)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1469))
				(in r1468)
			)
	)

	(:action move-right-from-r1470
		:precondition
			(and 
				(in r1470)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1470))
				(in r1471)
			)
	)


	(:action move-left-from-r1470
		:precondition
			(and 
				(in r1470)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1470))
				(in r1469)
			)
	)

	(:action move-right-from-r1471
		:precondition
			(and 
				(in r1471)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1471))
				(in r1472)
			)
	)


	(:action move-left-from-r1471
		:precondition
			(and 
				(in r1471)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1471))
				(in r1470)
			)
	)

	(:action move-right-from-r1472
		:precondition
			(and 
				(in r1472)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1472))
				(in r1473)
			)
	)


	(:action move-left-from-r1472
		:precondition
			(and 
				(in r1472)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1472))
				(in r1471)
			)
	)

	(:action move-right-from-r1473
		:precondition
			(and 
				(in r1473)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1473))
				(in r1474)
			)
	)


	(:action move-left-from-r1473
		:precondition
			(and 
				(in r1473)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1473))
				(in r1472)
			)
	)

	(:action move-right-from-r1474
		:precondition
			(and 
				(in r1474)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1474))
				(in r1475)
			)
	)


	(:action move-left-from-r1474
		:precondition
			(and 
				(in r1474)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1474))
				(in r1473)
			)
	)

	(:action move-right-from-r1475
		:precondition
			(and 
				(in r1475)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1475))
				(in r1476)
			)
	)


	(:action move-left-from-r1475
		:precondition
			(and 
				(in r1475)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1475))
				(in r1474)
			)
	)

	(:action move-right-from-r1476
		:precondition
			(and 
				(in r1476)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1476))
				(in r1477)
			)
	)


	(:action move-left-from-r1476
		:precondition
			(and 
				(in r1476)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1476))
				(in r1475)
			)
	)

	(:action move-right-from-r1477
		:precondition
			(and 
				(in r1477)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1477))
				(in r1478)
			)
	)


	(:action move-left-from-r1477
		:precondition
			(and 
				(in r1477)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1477))
				(in r1476)
			)
	)

	(:action move-right-from-r1478
		:precondition
			(and 
				(in r1478)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1478))
				(in r1479)
			)
	)


	(:action move-left-from-r1478
		:precondition
			(and 
				(in r1478)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1478))
				(in r1477)
			)
	)

	(:action move-right-from-r1479
		:precondition
			(and 
				(in r1479)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1479))
				(in r1480)
			)
	)


	(:action move-left-from-r1479
		:precondition
			(and 
				(in r1479)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1479))
				(in r1478)
			)
	)

	(:action move-right-from-r1480
		:precondition
			(and 
				(in r1480)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1480))
				(in r1481)
			)
	)


	(:action move-left-from-r1480
		:precondition
			(and 
				(in r1480)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1480))
				(in r1479)
			)
	)

	(:action move-right-from-r1481
		:precondition
			(and 
				(in r1481)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1481))
				(in r1482)
			)
	)


	(:action move-left-from-r1481
		:precondition
			(and 
				(in r1481)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1481))
				(in r1480)
			)
	)

	(:action move-right-from-r1482
		:precondition
			(and 
				(in r1482)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1482))
				(in r1483)
			)
	)


	(:action move-left-from-r1482
		:precondition
			(and 
				(in r1482)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1482))
				(in r1481)
			)
	)

	(:action move-right-from-r1483
		:precondition
			(and 
				(in r1483)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1483))
				(in r1484)
			)
	)


	(:action move-left-from-r1483
		:precondition
			(and 
				(in r1483)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1483))
				(in r1482)
			)
	)

	(:action move-right-from-r1484
		:precondition
			(and 
				(in r1484)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1484))
				(in r1485)
			)
	)


	(:action move-left-from-r1484
		:precondition
			(and 
				(in r1484)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1484))
				(in r1483)
			)
	)

	(:action move-right-from-r1485
		:precondition
			(and 
				(in r1485)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1485))
				(in r1486)
			)
	)


	(:action move-left-from-r1485
		:precondition
			(and 
				(in r1485)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1485))
				(in r1484)
			)
	)

	(:action move-right-from-r1486
		:precondition
			(and 
				(in r1486)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1486))
				(in r1487)
			)
	)


	(:action move-left-from-r1486
		:precondition
			(and 
				(in r1486)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1486))
				(in r1485)
			)
	)

	(:action move-right-from-r1487
		:precondition
			(and 
				(in r1487)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1487))
				(in r1488)
			)
	)


	(:action move-left-from-r1487
		:precondition
			(and 
				(in r1487)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1487))
				(in r1486)
			)
	)

	(:action move-right-from-r1488
		:precondition
			(and 
				(in r1488)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1488))
				(in r1489)
			)
	)


	(:action move-left-from-r1488
		:precondition
			(and 
				(in r1488)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1488))
				(in r1487)
			)
	)

	(:action move-right-from-r1489
		:precondition
			(and 
				(in r1489)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1489))
				(in r1490)
			)
	)


	(:action move-left-from-r1489
		:precondition
			(and 
				(in r1489)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1489))
				(in r1488)
			)
	)

	(:action move-right-from-r1490
		:precondition
			(and 
				(in r1490)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1490))
				(in r1491)
			)
	)


	(:action move-left-from-r1490
		:precondition
			(and 
				(in r1490)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1490))
				(in r1489)
			)
	)

	(:action move-right-from-r1491
		:precondition
			(and 
				(in r1491)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1491))
				(in r1492)
			)
	)


	(:action move-left-from-r1491
		:precondition
			(and 
				(in r1491)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1491))
				(in r1490)
			)
	)

	(:action move-right-from-r1492
		:precondition
			(and 
				(in r1492)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1492))
				(in r1493)
			)
	)


	(:action move-left-from-r1492
		:precondition
			(and 
				(in r1492)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1492))
				(in r1491)
			)
	)

	(:action move-right-from-r1493
		:precondition
			(and 
				(in r1493)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1493))
				(in r1494)
			)
	)


	(:action move-left-from-r1493
		:precondition
			(and 
				(in r1493)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1493))
				(in r1492)
			)
	)

	(:action move-right-from-r1494
		:precondition
			(and 
				(in r1494)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1494))
				(in r1495)
			)
	)


	(:action move-left-from-r1494
		:precondition
			(and 
				(in r1494)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1494))
				(in r1493)
			)
	)

	(:action move-right-from-r1495
		:precondition
			(and 
				(in r1495)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1495))
				(in r1496)
			)
	)


	(:action move-left-from-r1495
		:precondition
			(and 
				(in r1495)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1495))
				(in r1494)
			)
	)

	(:action move-right-from-r1496
		:precondition
			(and 
				(in r1496)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1496))
				(in r1497)
			)
	)


	(:action move-left-from-r1496
		:precondition
			(and 
				(in r1496)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1496))
				(in r1495)
			)
	)

	(:action move-right-from-r1497
		:precondition
			(and 
				(in r1497)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1497))
				(in r1498)
			)
	)


	(:action move-left-from-r1497
		:precondition
			(and 
				(in r1497)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1497))
				(in r1496)
			)
	)

	(:action move-right-from-r1498
		:precondition
			(and 
				(in r1498)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1498))
				(in r1499)
			)
	)


	(:action move-left-from-r1498
		:precondition
			(and 
				(in r1498)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1498))
				(in r1497)
			)
	)

	(:action move-right-from-r1499
		:precondition
			(and 
				(in r1499)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1499))
				(in r1500)
			)
	)


	(:action move-left-from-r1499
		:precondition
			(and 
				(in r1499)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1499))
				(in r1498)
			)
	)

	(:action move-right-from-r1500
		:precondition
			(and 
				(in r1500)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1500))
				(in r1501)
			)
	)


	(:action move-left-from-r1500
		:precondition
			(and 
				(in r1500)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1500))
				(in r1499)
			)
	)

	(:action move-right-from-r1501
		:precondition
			(and 
				(in r1501)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1501))
				(in r1502)
			)
	)


	(:action move-left-from-r1501
		:precondition
			(and 
				(in r1501)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1501))
				(in r1500)
			)
	)

	(:action move-right-from-r1502
		:precondition
			(and 
				(in r1502)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1502))
				(in r1503)
			)
	)


	(:action move-left-from-r1502
		:precondition
			(and 
				(in r1502)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1502))
				(in r1501)
			)
	)

	(:action move-right-from-r1503
		:precondition
			(and 
				(in r1503)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1503))
				(in r1504)
			)
	)


	(:action move-left-from-r1503
		:precondition
			(and 
				(in r1503)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1503))
				(in r1502)
			)
	)

	(:action move-right-from-r1504
		:precondition
			(and 
				(in r1504)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1504))
				(in r1505)
			)
	)


	(:action move-left-from-r1504
		:precondition
			(and 
				(in r1504)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1504))
				(in r1503)
			)
	)

	(:action move-right-from-r1505
		:precondition
			(and 
				(in r1505)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1505))
				(in r1506)
			)
	)


	(:action move-left-from-r1505
		:precondition
			(and 
				(in r1505)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1505))
				(in r1504)
			)
	)

	(:action move-right-from-r1506
		:precondition
			(and 
				(in r1506)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1506))
				(in r1507)
			)
	)


	(:action move-left-from-r1506
		:precondition
			(and 
				(in r1506)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1506))
				(in r1505)
			)
	)

	(:action move-right-from-r1507
		:precondition
			(and 
				(in r1507)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1507))
				(in r1508)
			)
	)


	(:action move-left-from-r1507
		:precondition
			(and 
				(in r1507)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1507))
				(in r1506)
			)
	)

	(:action move-right-from-r1508
		:precondition
			(and 
				(in r1508)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1508))
				(in r1509)
			)
	)


	(:action move-left-from-r1508
		:precondition
			(and 
				(in r1508)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1508))
				(in r1507)
			)
	)

	(:action move-right-from-r1509
		:precondition
			(and 
				(in r1509)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1509))
				(in r1510)
			)
	)


	(:action move-left-from-r1509
		:precondition
			(and 
				(in r1509)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1509))
				(in r1508)
			)
	)

	(:action move-right-from-r1510
		:precondition
			(and 
				(in r1510)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1510))
				(in r1511)
			)
	)


	(:action move-left-from-r1510
		:precondition
			(and 
				(in r1510)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1510))
				(in r1509)
			)
	)

	(:action move-right-from-r1511
		:precondition
			(and 
				(in r1511)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1511))
				(in r1512)
			)
	)


	(:action move-left-from-r1511
		:precondition
			(and 
				(in r1511)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1511))
				(in r1510)
			)
	)

	(:action move-right-from-r1512
		:precondition
			(and 
				(in r1512)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1512))
				(in r1513)
			)
	)


	(:action move-left-from-r1512
		:precondition
			(and 
				(in r1512)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1512))
				(in r1511)
			)
	)

	(:action move-right-from-r1513
		:precondition
			(and 
				(in r1513)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1513))
				(in r1514)
			)
	)


	(:action move-left-from-r1513
		:precondition
			(and 
				(in r1513)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1513))
				(in r1512)
			)
	)

	(:action move-right-from-r1514
		:precondition
			(and 
				(in r1514)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1514))
				(in r1515)
			)
	)


	(:action move-left-from-r1514
		:precondition
			(and 
				(in r1514)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1514))
				(in r1513)
			)
	)

	(:action move-right-from-r1515
		:precondition
			(and 
				(in r1515)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1515))
				(in r1516)
			)
	)


	(:action move-left-from-r1515
		:precondition
			(and 
				(in r1515)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1515))
				(in r1514)
			)
	)

	(:action move-right-from-r1516
		:precondition
			(and 
				(in r1516)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1516))
				(in r1517)
			)
	)


	(:action move-left-from-r1516
		:precondition
			(and 
				(in r1516)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1516))
				(in r1515)
			)
	)

	(:action move-right-from-r1517
		:precondition
			(and 
				(in r1517)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1517))
				(in r1518)
			)
	)


	(:action move-left-from-r1517
		:precondition
			(and 
				(in r1517)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1517))
				(in r1516)
			)
	)

	(:action move-right-from-r1518
		:precondition
			(and 
				(in r1518)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1518))
				(in r1519)
			)
	)


	(:action move-left-from-r1518
		:precondition
			(and 
				(in r1518)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1518))
				(in r1517)
			)
	)

	(:action move-right-from-r1519
		:precondition
			(and 
				(in r1519)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1519))
				(in r1520)
			)
	)


	(:action move-left-from-r1519
		:precondition
			(and 
				(in r1519)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1519))
				(in r1518)
			)
	)

	(:action move-right-from-r1520
		:precondition
			(and 
				(in r1520)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1520))
				(in r1521)
			)
	)


	(:action move-left-from-r1520
		:precondition
			(and 
				(in r1520)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1520))
				(in r1519)
			)
	)

	(:action move-right-from-r1521
		:precondition
			(and 
				(in r1521)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1521))
				(in r1522)
			)
	)


	(:action move-left-from-r1521
		:precondition
			(and 
				(in r1521)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1521))
				(in r1520)
			)
	)

	(:action move-right-from-r1522
		:precondition
			(and 
				(in r1522)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1522))
				(in r1523)
			)
	)


	(:action move-left-from-r1522
		:precondition
			(and 
				(in r1522)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1522))
				(in r1521)
			)
	)

	(:action move-right-from-r1523
		:precondition
			(and 
				(in r1523)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1523))
				(in r1524)
			)
	)


	(:action move-left-from-r1523
		:precondition
			(and 
				(in r1523)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1523))
				(in r1522)
			)
	)

	(:action move-right-from-r1524
		:precondition
			(and 
				(in r1524)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1524))
				(in r1525)
			)
	)


	(:action move-left-from-r1524
		:precondition
			(and 
				(in r1524)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1524))
				(in r1523)
			)
	)

	(:action move-right-from-r1525
		:precondition
			(and 
				(in r1525)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1525))
				(in r1526)
			)
	)


	(:action move-left-from-r1525
		:precondition
			(and 
				(in r1525)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1525))
				(in r1524)
			)
	)

	(:action move-right-from-r1526
		:precondition
			(and 
				(in r1526)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1526))
				(in r1527)
			)
	)


	(:action move-left-from-r1526
		:precondition
			(and 
				(in r1526)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1526))
				(in r1525)
			)
	)

	(:action move-right-from-r1527
		:precondition
			(and 
				(in r1527)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1527))
				(in r1528)
			)
	)


	(:action move-left-from-r1527
		:precondition
			(and 
				(in r1527)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1527))
				(in r1526)
			)
	)

	(:action move-right-from-r1528
		:precondition
			(and 
				(in r1528)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1528))
				(in r1529)
			)
	)


	(:action move-left-from-r1528
		:precondition
			(and 
				(in r1528)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1528))
				(in r1527)
			)
	)

	(:action move-right-from-r1529
		:precondition
			(and 
				(in r1529)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1529))
				(in r1530)
			)
	)


	(:action move-left-from-r1529
		:precondition
			(and 
				(in r1529)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1529))
				(in r1528)
			)
	)

	(:action move-right-from-r1530
		:precondition
			(and 
				(in r1530)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1530))
				(in r1531)
			)
	)


	(:action move-left-from-r1530
		:precondition
			(and 
				(in r1530)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1530))
				(in r1529)
			)
	)

	(:action move-right-from-r1531
		:precondition
			(and 
				(in r1531)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1531))
				(in r1532)
			)
	)


	(:action move-left-from-r1531
		:precondition
			(and 
				(in r1531)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1531))
				(in r1530)
			)
	)

	(:action move-right-from-r1532
		:precondition
			(and 
				(in r1532)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1532))
				(in r1533)
			)
	)


	(:action move-left-from-r1532
		:precondition
			(and 
				(in r1532)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1532))
				(in r1531)
			)
	)

	(:action move-right-from-r1533
		:precondition
			(and 
				(in r1533)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1533))
				(in r1534)
			)
	)


	(:action move-left-from-r1533
		:precondition
			(and 
				(in r1533)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1533))
				(in r1532)
			)
	)

	(:action move-right-from-r1534
		:precondition
			(and 
				(in r1534)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1534))
				(in r1535)
			)
	)


	(:action move-left-from-r1534
		:precondition
			(and 
				(in r1534)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1534))
				(in r1533)
			)
	)

	(:action move-right-from-r1535
		:precondition
			(and 
				(in r1535)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1535))
				(in r1536)
			)
	)


	(:action move-left-from-r1535
		:precondition
			(and 
				(in r1535)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1535))
				(in r1534)
			)
	)

	(:action move-right-from-r1536
		:precondition
			(and 
				(in r1536)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1536))
				(in r1537)
			)
	)


	(:action move-left-from-r1536
		:precondition
			(and 
				(in r1536)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1536))
				(in r1535)
			)
	)

	(:action move-right-from-r1537
		:precondition
			(and 
				(in r1537)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1537))
				(in r1538)
			)
	)


	(:action move-left-from-r1537
		:precondition
			(and 
				(in r1537)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1537))
				(in r1536)
			)
	)

	(:action move-right-from-r1538
		:precondition
			(and 
				(in r1538)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1538))
				(in r1539)
			)
	)


	(:action move-left-from-r1538
		:precondition
			(and 
				(in r1538)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1538))
				(in r1537)
			)
	)

	(:action move-right-from-r1539
		:precondition
			(and 
				(in r1539)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1539))
				(in r1540)
			)
	)


	(:action move-left-from-r1539
		:precondition
			(and 
				(in r1539)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1539))
				(in r1538)
			)
	)

	(:action move-right-from-r1540
		:precondition
			(and 
				(in r1540)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1540))
				(in r1541)
			)
	)


	(:action move-left-from-r1540
		:precondition
			(and 
				(in r1540)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1540))
				(in r1539)
			)
	)

	(:action move-right-from-r1541
		:precondition
			(and 
				(in r1541)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1541))
				(in r1542)
			)
	)


	(:action move-left-from-r1541
		:precondition
			(and 
				(in r1541)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1541))
				(in r1540)
			)
	)

	(:action move-right-from-r1542
		:precondition
			(and 
				(in r1542)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1542))
				(in r1543)
			)
	)


	(:action move-left-from-r1542
		:precondition
			(and 
				(in r1542)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1542))
				(in r1541)
			)
	)

	(:action move-right-from-r1543
		:precondition
			(and 
				(in r1543)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1543))
				(in r1544)
			)
	)


	(:action move-left-from-r1543
		:precondition
			(and 
				(in r1543)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1543))
				(in r1542)
			)
	)

	(:action move-right-from-r1544
		:precondition
			(and 
				(in r1544)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1544))
				(in r1545)
			)
	)


	(:action move-left-from-r1544
		:precondition
			(and 
				(in r1544)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1544))
				(in r1543)
			)
	)

	(:action move-right-from-r1545
		:precondition
			(and 
				(in r1545)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1545))
				(in r1546)
			)
	)


	(:action move-left-from-r1545
		:precondition
			(and 
				(in r1545)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1545))
				(in r1544)
			)
	)

	(:action move-right-from-r1546
		:precondition
			(and 
				(in r1546)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1546))
				(in r1547)
			)
	)


	(:action move-left-from-r1546
		:precondition
			(and 
				(in r1546)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1546))
				(in r1545)
			)
	)

	(:action move-right-from-r1547
		:precondition
			(and 
				(in r1547)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1547))
				(in r1548)
			)
	)


	(:action move-left-from-r1547
		:precondition
			(and 
				(in r1547)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1547))
				(in r1546)
			)
	)

	(:action move-right-from-r1548
		:precondition
			(and 
				(in r1548)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1548))
				(in r1549)
			)
	)


	(:action move-left-from-r1548
		:precondition
			(and 
				(in r1548)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1548))
				(in r1547)
			)
	)

	(:action move-right-from-r1549
		:precondition
			(and 
				(in r1549)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1549))
				(in r1550)
			)
	)


	(:action move-left-from-r1549
		:precondition
			(and 
				(in r1549)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1549))
				(in r1548)
			)
	)

	(:action move-right-from-r1550
		:precondition
			(and 
				(in r1550)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1550))
				(in r1551)
			)
	)


	(:action move-left-from-r1550
		:precondition
			(and 
				(in r1550)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1550))
				(in r1549)
			)
	)

	(:action move-right-from-r1551
		:precondition
			(and 
				(in r1551)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1551))
				(in r1552)
			)
	)


	(:action move-left-from-r1551
		:precondition
			(and 
				(in r1551)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1551))
				(in r1550)
			)
	)

	(:action move-right-from-r1552
		:precondition
			(and 
				(in r1552)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1552))
				(in r1553)
			)
	)


	(:action move-left-from-r1552
		:precondition
			(and 
				(in r1552)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1552))
				(in r1551)
			)
	)

	(:action move-right-from-r1553
		:precondition
			(and 
				(in r1553)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1553))
				(in r1554)
			)
	)


	(:action move-left-from-r1553
		:precondition
			(and 
				(in r1553)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1553))
				(in r1552)
			)
	)

	(:action move-right-from-r1554
		:precondition
			(and 
				(in r1554)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1554))
				(in r1555)
			)
	)


	(:action move-left-from-r1554
		:precondition
			(and 
				(in r1554)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1554))
				(in r1553)
			)
	)

	(:action move-right-from-r1555
		:precondition
			(and 
				(in r1555)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1555))
				(in r1556)
			)
	)


	(:action move-left-from-r1555
		:precondition
			(and 
				(in r1555)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1555))
				(in r1554)
			)
	)

	(:action move-right-from-r1556
		:precondition
			(and 
				(in r1556)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1556))
				(in r1557)
			)
	)


	(:action move-left-from-r1556
		:precondition
			(and 
				(in r1556)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1556))
				(in r1555)
			)
	)

	(:action move-right-from-r1557
		:precondition
			(and 
				(in r1557)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1557))
				(in r1558)
			)
	)


	(:action move-left-from-r1557
		:precondition
			(and 
				(in r1557)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1557))
				(in r1556)
			)
	)

	(:action move-right-from-r1558
		:precondition
			(and 
				(in r1558)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1558))
				(in r1559)
			)
	)


	(:action move-left-from-r1558
		:precondition
			(and 
				(in r1558)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1558))
				(in r1557)
			)
	)

	(:action move-right-from-r1559
		:precondition
			(and 
				(in r1559)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1559))
				(in r1560)
			)
	)


	(:action move-left-from-r1559
		:precondition
			(and 
				(in r1559)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1559))
				(in r1558)
			)
	)

	(:action move-right-from-r1560
		:precondition
			(and 
				(in r1560)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1560))
				(in r1561)
			)
	)


	(:action move-left-from-r1560
		:precondition
			(and 
				(in r1560)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1560))
				(in r1559)
			)
	)

	(:action move-right-from-r1561
		:precondition
			(and 
				(in r1561)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1561))
				(in r1562)
			)
	)


	(:action move-left-from-r1561
		:precondition
			(and 
				(in r1561)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1561))
				(in r1560)
			)
	)

	(:action move-right-from-r1562
		:precondition
			(and 
				(in r1562)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1562))
				(in r1563)
			)
	)


	(:action move-left-from-r1562
		:precondition
			(and 
				(in r1562)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1562))
				(in r1561)
			)
	)

	(:action move-right-from-r1563
		:precondition
			(and 
				(in r1563)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1563))
				(in r1564)
			)
	)


	(:action move-left-from-r1563
		:precondition
			(and 
				(in r1563)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1563))
				(in r1562)
			)
	)

	(:action move-right-from-r1564
		:precondition
			(and 
				(in r1564)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1564))
				(in r1565)
			)
	)


	(:action move-left-from-r1564
		:precondition
			(and 
				(in r1564)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1564))
				(in r1563)
			)
	)

	(:action move-right-from-r1565
		:precondition
			(and 
				(in r1565)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1565))
				(in r1566)
			)
	)


	(:action move-left-from-r1565
		:precondition
			(and 
				(in r1565)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1565))
				(in r1564)
			)
	)

	(:action move-right-from-r1566
		:precondition
			(and 
				(in r1566)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1566))
				(in r1567)
			)
	)


	(:action move-left-from-r1566
		:precondition
			(and 
				(in r1566)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1566))
				(in r1565)
			)
	)

	(:action move-right-from-r1567
		:precondition
			(and 
				(in r1567)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1567))
				(in r1568)
			)
	)


	(:action move-left-from-r1567
		:precondition
			(and 
				(in r1567)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1567))
				(in r1566)
			)
	)

	(:action move-right-from-r1568
		:precondition
			(and 
				(in r1568)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1568))
				(in r1569)
			)
	)


	(:action move-left-from-r1568
		:precondition
			(and 
				(in r1568)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1568))
				(in r1567)
			)
	)

	(:action move-right-from-r1569
		:precondition
			(and 
				(in r1569)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1569))
				(in r1570)
			)
	)


	(:action move-left-from-r1569
		:precondition
			(and 
				(in r1569)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1569))
				(in r1568)
			)
	)

	(:action move-right-from-r1570
		:precondition
			(and 
				(in r1570)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1570))
				(in r1571)
			)
	)


	(:action move-left-from-r1570
		:precondition
			(and 
				(in r1570)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1570))
				(in r1569)
			)
	)

	(:action move-right-from-r1571
		:precondition
			(and 
				(in r1571)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1571))
				(in r1572)
			)
	)


	(:action move-left-from-r1571
		:precondition
			(and 
				(in r1571)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1571))
				(in r1570)
			)
	)

	(:action move-right-from-r1572
		:precondition
			(and 
				(in r1572)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1572))
				(in r1573)
			)
	)


	(:action move-left-from-r1572
		:precondition
			(and 
				(in r1572)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1572))
				(in r1571)
			)
	)

	(:action move-right-from-r1573
		:precondition
			(and 
				(in r1573)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1573))
				(in r1574)
			)
	)


	(:action move-left-from-r1573
		:precondition
			(and 
				(in r1573)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1573))
				(in r1572)
			)
	)

	(:action move-right-from-r1574
		:precondition
			(and 
				(in r1574)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1574))
				(in r1575)
			)
	)


	(:action move-left-from-r1574
		:precondition
			(and 
				(in r1574)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1574))
				(in r1573)
			)
	)

	(:action move-right-from-r1575
		:precondition
			(and 
				(in r1575)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1575))
				(in r1576)
			)
	)


	(:action move-left-from-r1575
		:precondition
			(and 
				(in r1575)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1575))
				(in r1574)
			)
	)

	(:action move-right-from-r1576
		:precondition
			(and 
				(in r1576)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1576))
				(in r1577)
			)
	)


	(:action move-left-from-r1576
		:precondition
			(and 
				(in r1576)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1576))
				(in r1575)
			)
	)

	(:action move-right-from-r1577
		:precondition
			(and 
				(in r1577)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1577))
				(in r1578)
			)
	)


	(:action move-left-from-r1577
		:precondition
			(and 
				(in r1577)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1577))
				(in r1576)
			)
	)

	(:action move-right-from-r1578
		:precondition
			(and 
				(in r1578)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1578))
				(in r1579)
			)
	)


	(:action move-left-from-r1578
		:precondition
			(and 
				(in r1578)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1578))
				(in r1577)
			)
	)

	(:action move-right-from-r1579
		:precondition
			(and 
				(in r1579)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1579))
				(in r1580)
			)
	)


	(:action move-left-from-r1579
		:precondition
			(and 
				(in r1579)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1579))
				(in r1578)
			)
	)

	(:action move-right-from-r1580
		:precondition
			(and 
				(in r1580)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1580))
				(in r1581)
			)
	)


	(:action move-left-from-r1580
		:precondition
			(and 
				(in r1580)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1580))
				(in r1579)
			)
	)

	(:action move-right-from-r1581
		:precondition
			(and 
				(in r1581)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1581))
				(in r1582)
			)
	)


	(:action move-left-from-r1581
		:precondition
			(and 
				(in r1581)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1581))
				(in r1580)
			)
	)

	(:action move-right-from-r1582
		:precondition
			(and 
				(in r1582)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1582))
				(in r1583)
			)
	)


	(:action move-left-from-r1582
		:precondition
			(and 
				(in r1582)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1582))
				(in r1581)
			)
	)

	(:action move-right-from-r1583
		:precondition
			(and 
				(in r1583)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1583))
				(in r1584)
			)
	)


	(:action move-left-from-r1583
		:precondition
			(and 
				(in r1583)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1583))
				(in r1582)
			)
	)

	(:action move-right-from-r1584
		:precondition
			(and 
				(in r1584)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1584))
				(in r1585)
			)
	)


	(:action move-left-from-r1584
		:precondition
			(and 
				(in r1584)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1584))
				(in r1583)
			)
	)

	(:action move-right-from-r1585
		:precondition
			(and 
				(in r1585)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1585))
				(in r1586)
			)
	)


	(:action move-left-from-r1585
		:precondition
			(and 
				(in r1585)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1585))
				(in r1584)
			)
	)

	(:action move-right-from-r1586
		:precondition
			(and 
				(in r1586)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1586))
				(in r1587)
			)
	)


	(:action move-left-from-r1586
		:precondition
			(and 
				(in r1586)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1586))
				(in r1585)
			)
	)

	(:action move-right-from-r1587
		:precondition
			(and 
				(in r1587)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1587))
				(in r1588)
			)
	)


	(:action move-left-from-r1587
		:precondition
			(and 
				(in r1587)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1587))
				(in r1586)
			)
	)

	(:action move-right-from-r1588
		:precondition
			(and 
				(in r1588)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1588))
				(in r1589)
			)
	)


	(:action move-left-from-r1588
		:precondition
			(and 
				(in r1588)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1588))
				(in r1587)
			)
	)

	(:action move-right-from-r1589
		:precondition
			(and 
				(in r1589)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1589))
				(in r1590)
			)
	)


	(:action move-left-from-r1589
		:precondition
			(and 
				(in r1589)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1589))
				(in r1588)
			)
	)

	(:action move-right-from-r1590
		:precondition
			(and 
				(in r1590)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1590))
				(in r1591)
			)
	)


	(:action move-left-from-r1590
		:precondition
			(and 
				(in r1590)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1590))
				(in r1589)
			)
	)

	(:action move-right-from-r1591
		:precondition
			(and 
				(in r1591)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1591))
				(in r1592)
			)
	)


	(:action move-left-from-r1591
		:precondition
			(and 
				(in r1591)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1591))
				(in r1590)
			)
	)

	(:action move-right-from-r1592
		:precondition
			(and 
				(in r1592)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1592))
				(in r1593)
			)
	)


	(:action move-left-from-r1592
		:precondition
			(and 
				(in r1592)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1592))
				(in r1591)
			)
	)

	(:action move-right-from-r1593
		:precondition
			(and 
				(in r1593)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1593))
				(in r1594)
			)
	)


	(:action move-left-from-r1593
		:precondition
			(and 
				(in r1593)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1593))
				(in r1592)
			)
	)

	(:action move-right-from-r1594
		:precondition
			(and 
				(in r1594)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1594))
				(in r1595)
			)
	)


	(:action move-left-from-r1594
		:precondition
			(and 
				(in r1594)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1594))
				(in r1593)
			)
	)

	(:action move-right-from-r1595
		:precondition
			(and 
				(in r1595)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1595))
				(in r1596)
			)
	)


	(:action move-left-from-r1595
		:precondition
			(and 
				(in r1595)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1595))
				(in r1594)
			)
	)

	(:action move-right-from-r1596
		:precondition
			(and 
				(in r1596)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1596))
				(in r1597)
			)
	)


	(:action move-left-from-r1596
		:precondition
			(and 
				(in r1596)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1596))
				(in r1595)
			)
	)

	(:action move-right-from-r1597
		:precondition
			(and 
				(in r1597)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1597))
				(in r1598)
			)
	)


	(:action move-left-from-r1597
		:precondition
			(and 
				(in r1597)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1597))
				(in r1596)
			)
	)

	(:action move-right-from-r1598
		:precondition
			(and 
				(in r1598)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1598))
				(in r1599)
			)
	)


	(:action move-left-from-r1598
		:precondition
			(and 
				(in r1598)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1598))
				(in r1597)
			)
	)

	(:action move-right-from-r1599
		:precondition
			(and 
				(in r1599)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1599))
				(in r1600)
			)
	)


	(:action move-left-from-r1599
		:precondition
			(and 
				(in r1599)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1599))
				(in r1598)
			)
	)

	(:action move-right-from-r1600
		:precondition
			(and 
				(in r1600)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1600))
				(in r1601)
			)
	)


	(:action move-left-from-r1600
		:precondition
			(and 
				(in r1600)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1600))
				(in r1599)
			)
	)

	(:action move-right-from-r1601
		:precondition
			(and 
				(in r1601)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1601))
				(in r1602)
			)
	)


	(:action move-left-from-r1601
		:precondition
			(and 
				(in r1601)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1601))
				(in r1600)
			)
	)

	(:action move-right-from-r1602
		:precondition
			(and 
				(in r1602)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1602))
				(in r1603)
			)
	)


	(:action move-left-from-r1602
		:precondition
			(and 
				(in r1602)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1602))
				(in r1601)
			)
	)

	(:action move-right-from-r1603
		:precondition
			(and 
				(in r1603)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1603))
				(in r1604)
			)
	)


	(:action move-left-from-r1603
		:precondition
			(and 
				(in r1603)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1603))
				(in r1602)
			)
	)

	(:action move-right-from-r1604
		:precondition
			(and 
				(in r1604)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1604))
				(in r1605)
			)
	)


	(:action move-left-from-r1604
		:precondition
			(and 
				(in r1604)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1604))
				(in r1603)
			)
	)

	(:action move-right-from-r1605
		:precondition
			(and 
				(in r1605)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1605))
				(in r1606)
			)
	)


	(:action move-left-from-r1605
		:precondition
			(and 
				(in r1605)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1605))
				(in r1604)
			)
	)

	(:action move-right-from-r1606
		:precondition
			(and 
				(in r1606)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1606))
				(in r1607)
			)
	)


	(:action move-left-from-r1606
		:precondition
			(and 
				(in r1606)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1606))
				(in r1605)
			)
	)

	(:action move-right-from-r1607
		:precondition
			(and 
				(in r1607)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1607))
				(in r1608)
			)
	)


	(:action move-left-from-r1607
		:precondition
			(and 
				(in r1607)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1607))
				(in r1606)
			)
	)

	(:action move-right-from-r1608
		:precondition
			(and 
				(in r1608)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1608))
				(in r1609)
			)
	)


	(:action move-left-from-r1608
		:precondition
			(and 
				(in r1608)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1608))
				(in r1607)
			)
	)

	(:action move-right-from-r1609
		:precondition
			(and 
				(in r1609)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1609))
				(in r1610)
			)
	)


	(:action move-left-from-r1609
		:precondition
			(and 
				(in r1609)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1609))
				(in r1608)
			)
	)

	(:action move-right-from-r1610
		:precondition
			(and 
				(in r1610)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1610))
				(in r1611)
			)
	)


	(:action move-left-from-r1610
		:precondition
			(and 
				(in r1610)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1610))
				(in r1609)
			)
	)

	(:action move-right-from-r1611
		:precondition
			(and 
				(in r1611)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1611))
				(in r1612)
			)
	)


	(:action move-left-from-r1611
		:precondition
			(and 
				(in r1611)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1611))
				(in r1610)
			)
	)

	(:action move-right-from-r1612
		:precondition
			(and 
				(in r1612)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1612))
				(in r1613)
			)
	)


	(:action move-left-from-r1612
		:precondition
			(and 
				(in r1612)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1612))
				(in r1611)
			)
	)

	(:action move-right-from-r1613
		:precondition
			(and 
				(in r1613)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1613))
				(in r1614)
			)
	)


	(:action move-left-from-r1613
		:precondition
			(and 
				(in r1613)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1613))
				(in r1612)
			)
	)

	(:action move-right-from-r1614
		:precondition
			(and 
				(in r1614)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1614))
				(in r1615)
			)
	)


	(:action move-left-from-r1614
		:precondition
			(and 
				(in r1614)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1614))
				(in r1613)
			)
	)

	(:action move-right-from-r1615
		:precondition
			(and 
				(in r1615)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1615))
				(in r1616)
			)
	)


	(:action move-left-from-r1615
		:precondition
			(and 
				(in r1615)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1615))
				(in r1614)
			)
	)

	(:action move-right-from-r1616
		:precondition
			(and 
				(in r1616)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1616))
				(in r1617)
			)
	)


	(:action move-left-from-r1616
		:precondition
			(and 
				(in r1616)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1616))
				(in r1615)
			)
	)

	(:action move-right-from-r1617
		:precondition
			(and 
				(in r1617)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1617))
				(in r1618)
			)
	)


	(:action move-left-from-r1617
		:precondition
			(and 
				(in r1617)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1617))
				(in r1616)
			)
	)

	(:action move-right-from-r1618
		:precondition
			(and 
				(in r1618)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1618))
				(in r1619)
			)
	)


	(:action move-left-from-r1618
		:precondition
			(and 
				(in r1618)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1618))
				(in r1617)
			)
	)

	(:action move-right-from-r1619
		:precondition
			(and 
				(in r1619)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1619))
				(in r1620)
			)
	)


	(:action move-left-from-r1619
		:precondition
			(and 
				(in r1619)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1619))
				(in r1618)
			)
	)

	(:action move-right-from-r1620
		:precondition
			(and 
				(in r1620)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1620))
				(in r1621)
			)
	)


	(:action move-left-from-r1620
		:precondition
			(and 
				(in r1620)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1620))
				(in r1619)
			)
	)

	(:action move-right-from-r1621
		:precondition
			(and 
				(in r1621)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1621))
				(in r1622)
			)
	)


	(:action move-left-from-r1621
		:precondition
			(and 
				(in r1621)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1621))
				(in r1620)
			)
	)

	(:action move-right-from-r1622
		:precondition
			(and 
				(in r1622)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1622))
				(in r1623)
			)
	)


	(:action move-left-from-r1622
		:precondition
			(and 
				(in r1622)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1622))
				(in r1621)
			)
	)

	(:action move-right-from-r1623
		:precondition
			(and 
				(in r1623)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1623))
				(in r1624)
			)
	)


	(:action move-left-from-r1623
		:precondition
			(and 
				(in r1623)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1623))
				(in r1622)
			)
	)

	(:action move-right-from-r1624
		:precondition
			(and 
				(in r1624)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1624))
				(in r1625)
			)
	)


	(:action move-left-from-r1624
		:precondition
			(and 
				(in r1624)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1624))
				(in r1623)
			)
	)

	(:action move-right-from-r1625
		:precondition
			(and 
				(in r1625)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1625))
				(in r1626)
			)
	)


	(:action move-left-from-r1625
		:precondition
			(and 
				(in r1625)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1625))
				(in r1624)
			)
	)

	(:action move-right-from-r1626
		:precondition
			(and 
				(in r1626)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1626))
				(in r1627)
			)
	)


	(:action move-left-from-r1626
		:precondition
			(and 
				(in r1626)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1626))
				(in r1625)
			)
	)

	(:action move-right-from-r1627
		:precondition
			(and 
				(in r1627)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1627))
				(in r1628)
			)
	)


	(:action move-left-from-r1627
		:precondition
			(and 
				(in r1627)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1627))
				(in r1626)
			)
	)

	(:action move-right-from-r1628
		:precondition
			(and 
				(in r1628)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1628))
				(in r1629)
			)
	)


	(:action move-left-from-r1628
		:precondition
			(and 
				(in r1628)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1628))
				(in r1627)
			)
	)

	(:action move-right-from-r1629
		:precondition
			(and 
				(in r1629)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1629))
				(in r1630)
			)
	)


	(:action move-left-from-r1629
		:precondition
			(and 
				(in r1629)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1629))
				(in r1628)
			)
	)

	(:action move-right-from-r1630
		:precondition
			(and 
				(in r1630)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1630))
				(in r1631)
			)
	)


	(:action move-left-from-r1630
		:precondition
			(and 
				(in r1630)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1630))
				(in r1629)
			)
	)

	(:action move-right-from-r1631
		:precondition
			(and 
				(in r1631)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1631))
				(in r1632)
			)
	)


	(:action move-left-from-r1631
		:precondition
			(and 
				(in r1631)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1631))
				(in r1630)
			)
	)

	(:action move-right-from-r1632
		:precondition
			(and 
				(in r1632)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1632))
				(in r1633)
			)
	)


	(:action move-left-from-r1632
		:precondition
			(and 
				(in r1632)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1632))
				(in r1631)
			)
	)

	(:action move-right-from-r1633
		:precondition
			(and 
				(in r1633)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1633))
				(in r1634)
			)
	)


	(:action move-left-from-r1633
		:precondition
			(and 
				(in r1633)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1633))
				(in r1632)
			)
	)

	(:action move-right-from-r1634
		:precondition
			(and 
				(in r1634)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1634))
				(in r1635)
			)
	)


	(:action move-left-from-r1634
		:precondition
			(and 
				(in r1634)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1634))
				(in r1633)
			)
	)

	(:action move-right-from-r1635
		:precondition
			(and 
				(in r1635)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1635))
				(in r1636)
			)
	)


	(:action move-left-from-r1635
		:precondition
			(and 
				(in r1635)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1635))
				(in r1634)
			)
	)

	(:action move-right-from-r1636
		:precondition
			(and 
				(in r1636)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1636))
				(in r1637)
			)
	)


	(:action move-left-from-r1636
		:precondition
			(and 
				(in r1636)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1636))
				(in r1635)
			)
	)

	(:action move-right-from-r1637
		:precondition
			(and 
				(in r1637)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1637))
				(in r1638)
			)
	)


	(:action move-left-from-r1637
		:precondition
			(and 
				(in r1637)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1637))
				(in r1636)
			)
	)

	(:action move-right-from-r1638
		:precondition
			(and 
				(in r1638)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1638))
				(in r1639)
			)
	)


	(:action move-left-from-r1638
		:precondition
			(and 
				(in r1638)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1638))
				(in r1637)
			)
	)

	(:action move-right-from-r1639
		:precondition
			(and 
				(in r1639)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1639))
				(in r1640)
			)
	)


	(:action move-left-from-r1639
		:precondition
			(and 
				(in r1639)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1639))
				(in r1638)
			)
	)

	(:action move-right-from-r1640
		:precondition
			(and 
				(in r1640)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1640))
				(in r1641)
			)
	)


	(:action move-left-from-r1640
		:precondition
			(and 
				(in r1640)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1640))
				(in r1639)
			)
	)

	(:action move-right-from-r1641
		:precondition
			(and 
				(in r1641)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1641))
				(in r1642)
			)
	)


	(:action move-left-from-r1641
		:precondition
			(and 
				(in r1641)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1641))
				(in r1640)
			)
	)

	(:action move-right-from-r1642
		:precondition
			(and 
				(in r1642)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1642))
				(in r1643)
			)
	)


	(:action move-left-from-r1642
		:precondition
			(and 
				(in r1642)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1642))
				(in r1641)
			)
	)

	(:action move-right-from-r1643
		:precondition
			(and 
				(in r1643)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1643))
				(in r1644)
			)
	)


	(:action move-left-from-r1643
		:precondition
			(and 
				(in r1643)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1643))
				(in r1642)
			)
	)

	(:action move-right-from-r1644
		:precondition
			(and 
				(in r1644)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1644))
				(in r1645)
			)
	)


	(:action move-left-from-r1644
		:precondition
			(and 
				(in r1644)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1644))
				(in r1643)
			)
	)

	(:action move-right-from-r1645
		:precondition
			(and 
				(in r1645)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1645))
				(in r1646)
			)
	)


	(:action move-left-from-r1645
		:precondition
			(and 
				(in r1645)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1645))
				(in r1644)
			)
	)

	(:action move-right-from-r1646
		:precondition
			(and 
				(in r1646)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1646))
				(in r1647)
			)
	)


	(:action move-left-from-r1646
		:precondition
			(and 
				(in r1646)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1646))
				(in r1645)
			)
	)

	(:action move-right-from-r1647
		:precondition
			(and 
				(in r1647)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1647))
				(in r1648)
			)
	)


	(:action move-left-from-r1647
		:precondition
			(and 
				(in r1647)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1647))
				(in r1646)
			)
	)

	(:action move-right-from-r1648
		:precondition
			(and 
				(in r1648)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1648))
				(in r1649)
			)
	)


	(:action move-left-from-r1648
		:precondition
			(and 
				(in r1648)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1648))
				(in r1647)
			)
	)

	(:action move-right-from-r1649
		:precondition
			(and 
				(in r1649)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1649))
				(in r1650)
			)
	)


	(:action move-left-from-r1649
		:precondition
			(and 
				(in r1649)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1649))
				(in r1648)
			)
	)

	(:action move-right-from-r1650
		:precondition
			(and 
				(in r1650)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1650))
				(in r1651)
			)
	)


	(:action move-left-from-r1650
		:precondition
			(and 
				(in r1650)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1650))
				(in r1649)
			)
	)

	(:action move-right-from-r1651
		:precondition
			(and 
				(in r1651)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1651))
				(in r1652)
			)
	)


	(:action move-left-from-r1651
		:precondition
			(and 
				(in r1651)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1651))
				(in r1650)
			)
	)

	(:action move-right-from-r1652
		:precondition
			(and 
				(in r1652)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1652))
				(in r1653)
			)
	)


	(:action move-left-from-r1652
		:precondition
			(and 
				(in r1652)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1652))
				(in r1651)
			)
	)

	(:action move-right-from-r1653
		:precondition
			(and 
				(in r1653)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1653))
				(in r1654)
			)
	)


	(:action move-left-from-r1653
		:precondition
			(and 
				(in r1653)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1653))
				(in r1652)
			)
	)

	(:action move-right-from-r1654
		:precondition
			(and 
				(in r1654)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1654))
				(in r1655)
			)
	)


	(:action move-left-from-r1654
		:precondition
			(and 
				(in r1654)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1654))
				(in r1653)
			)
	)

	(:action move-right-from-r1655
		:precondition
			(and 
				(in r1655)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1655))
				(in r1656)
			)
	)


	(:action move-left-from-r1655
		:precondition
			(and 
				(in r1655)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1655))
				(in r1654)
			)
	)

	(:action move-right-from-r1656
		:precondition
			(and 
				(in r1656)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1656))
				(in r1657)
			)
	)


	(:action move-left-from-r1656
		:precondition
			(and 
				(in r1656)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1656))
				(in r1655)
			)
	)

	(:action move-right-from-r1657
		:precondition
			(and 
				(in r1657)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1657))
				(in r1658)
			)
	)


	(:action move-left-from-r1657
		:precondition
			(and 
				(in r1657)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1657))
				(in r1656)
			)
	)

	(:action move-right-from-r1658
		:precondition
			(and 
				(in r1658)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1658))
				(in r1659)
			)
	)


	(:action move-left-from-r1658
		:precondition
			(and 
				(in r1658)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1658))
				(in r1657)
			)
	)

	(:action move-right-from-r1659
		:precondition
			(and 
				(in r1659)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1659))
				(in r1660)
			)
	)


	(:action move-left-from-r1659
		:precondition
			(and 
				(in r1659)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1659))
				(in r1658)
			)
	)

	(:action move-right-from-r1660
		:precondition
			(and 
				(in r1660)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1660))
				(in r1661)
			)
	)


	(:action move-left-from-r1660
		:precondition
			(and 
				(in r1660)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1660))
				(in r1659)
			)
	)

	(:action move-right-from-r1661
		:precondition
			(and 
				(in r1661)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1661))
				(in r1662)
			)
	)


	(:action move-left-from-r1661
		:precondition
			(and 
				(in r1661)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1661))
				(in r1660)
			)
	)

	(:action move-right-from-r1662
		:precondition
			(and 
				(in r1662)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1662))
				(in r1663)
			)
	)


	(:action move-left-from-r1662
		:precondition
			(and 
				(in r1662)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1662))
				(in r1661)
			)
	)

	(:action move-right-from-r1663
		:precondition
			(and 
				(in r1663)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1663))
				(in r1664)
			)
	)


	(:action move-left-from-r1663
		:precondition
			(and 
				(in r1663)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1663))
				(in r1662)
			)
	)

	(:action move-right-from-r1664
		:precondition
			(and 
				(in r1664)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1664))
				(in r1665)
			)
	)


	(:action move-left-from-r1664
		:precondition
			(and 
				(in r1664)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1664))
				(in r1663)
			)
	)

	(:action move-right-from-r1665
		:precondition
			(and 
				(in r1665)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1665))
				(in r1666)
			)
	)


	(:action move-left-from-r1665
		:precondition
			(and 
				(in r1665)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1665))
				(in r1664)
			)
	)

	(:action move-right-from-r1666
		:precondition
			(and 
				(in r1666)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1666))
				(in r1667)
			)
	)


	(:action move-left-from-r1666
		:precondition
			(and 
				(in r1666)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1666))
				(in r1665)
			)
	)

	(:action move-right-from-r1667
		:precondition
			(and 
				(in r1667)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1667))
				(in r1668)
			)
	)


	(:action move-left-from-r1667
		:precondition
			(and 
				(in r1667)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1667))
				(in r1666)
			)
	)

	(:action move-right-from-r1668
		:precondition
			(and 
				(in r1668)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1668))
				(in r1669)
			)
	)


	(:action move-left-from-r1668
		:precondition
			(and 
				(in r1668)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1668))
				(in r1667)
			)
	)

	(:action move-right-from-r1669
		:precondition
			(and 
				(in r1669)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1669))
				(in r1670)
			)
	)


	(:action move-left-from-r1669
		:precondition
			(and 
				(in r1669)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1669))
				(in r1668)
			)
	)

	(:action move-right-from-r1670
		:precondition
			(and 
				(in r1670)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1670))
				(in r1671)
			)
	)


	(:action move-left-from-r1670
		:precondition
			(and 
				(in r1670)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1670))
				(in r1669)
			)
	)

	(:action move-right-from-r1671
		:precondition
			(and 
				(in r1671)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1671))
				(in r1672)
			)
	)


	(:action move-left-from-r1671
		:precondition
			(and 
				(in r1671)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1671))
				(in r1670)
			)
	)

	(:action move-right-from-r1672
		:precondition
			(and 
				(in r1672)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1672))
				(in r1673)
			)
	)


	(:action move-left-from-r1672
		:precondition
			(and 
				(in r1672)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1672))
				(in r1671)
			)
	)

	(:action move-right-from-r1673
		:precondition
			(and 
				(in r1673)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1673))
				(in r1674)
			)
	)


	(:action move-left-from-r1673
		:precondition
			(and 
				(in r1673)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1673))
				(in r1672)
			)
	)

	(:action move-right-from-r1674
		:precondition
			(and 
				(in r1674)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1674))
				(in r1675)
			)
	)


	(:action move-left-from-r1674
		:precondition
			(and 
				(in r1674)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1674))
				(in r1673)
			)
	)

	(:action move-right-from-r1675
		:precondition
			(and 
				(in r1675)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1675))
				(in r1676)
			)
	)


	(:action move-left-from-r1675
		:precondition
			(and 
				(in r1675)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1675))
				(in r1674)
			)
	)

	(:action move-right-from-r1676
		:precondition
			(and 
				(in r1676)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1676))
				(in r1677)
			)
	)


	(:action move-left-from-r1676
		:precondition
			(and 
				(in r1676)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1676))
				(in r1675)
			)
	)

	(:action move-right-from-r1677
		:precondition
			(and 
				(in r1677)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1677))
				(in r1678)
			)
	)


	(:action move-left-from-r1677
		:precondition
			(and 
				(in r1677)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1677))
				(in r1676)
			)
	)

	(:action move-right-from-r1678
		:precondition
			(and 
				(in r1678)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1678))
				(in r1679)
			)
	)


	(:action move-left-from-r1678
		:precondition
			(and 
				(in r1678)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1678))
				(in r1677)
			)
	)

	(:action move-right-from-r1679
		:precondition
			(and 
				(in r1679)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1679))
				(in r1680)
			)
	)


	(:action move-left-from-r1679
		:precondition
			(and 
				(in r1679)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1679))
				(in r1678)
			)
	)

	(:action move-right-from-r1680
		:precondition
			(and 
				(in r1680)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1680))
				(in r1681)
			)
	)


	(:action move-left-from-r1680
		:precondition
			(and 
				(in r1680)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1680))
				(in r1679)
			)
	)

	(:action move-right-from-r1681
		:precondition
			(and 
				(in r1681)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1681))
				(in r1682)
			)
	)


	(:action move-left-from-r1681
		:precondition
			(and 
				(in r1681)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1681))
				(in r1680)
			)
	)

	(:action move-right-from-r1682
		:precondition
			(and 
				(in r1682)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1682))
				(in r1683)
			)
	)


	(:action move-left-from-r1682
		:precondition
			(and 
				(in r1682)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1682))
				(in r1681)
			)
	)

	(:action move-right-from-r1683
		:precondition
			(and 
				(in r1683)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1683))
				(in r1684)
			)
	)


	(:action move-left-from-r1683
		:precondition
			(and 
				(in r1683)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1683))
				(in r1682)
			)
	)

	(:action move-right-from-r1684
		:precondition
			(and 
				(in r1684)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1684))
				(in r1685)
			)
	)


	(:action move-left-from-r1684
		:precondition
			(and 
				(in r1684)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1684))
				(in r1683)
			)
	)

	(:action move-right-from-r1685
		:precondition
			(and 
				(in r1685)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1685))
				(in r1686)
			)
	)


	(:action move-left-from-r1685
		:precondition
			(and 
				(in r1685)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1685))
				(in r1684)
			)
	)

	(:action move-right-from-r1686
		:precondition
			(and 
				(in r1686)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1686))
				(in r1687)
			)
	)


	(:action move-left-from-r1686
		:precondition
			(and 
				(in r1686)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1686))
				(in r1685)
			)
	)

	(:action move-right-from-r1687
		:precondition
			(and 
				(in r1687)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1687))
				(in r1688)
			)
	)


	(:action move-left-from-r1687
		:precondition
			(and 
				(in r1687)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1687))
				(in r1686)
			)
	)

	(:action move-right-from-r1688
		:precondition
			(and 
				(in r1688)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1688))
				(in r1689)
			)
	)


	(:action move-left-from-r1688
		:precondition
			(and 
				(in r1688)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1688))
				(in r1687)
			)
	)

	(:action move-right-from-r1689
		:precondition
			(and 
				(in r1689)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1689))
				(in r1690)
			)
	)


	(:action move-left-from-r1689
		:precondition
			(and 
				(in r1689)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1689))
				(in r1688)
			)
	)

	(:action move-right-from-r1690
		:precondition
			(and 
				(in r1690)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1690))
				(in r1691)
			)
	)


	(:action move-left-from-r1690
		:precondition
			(and 
				(in r1690)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1690))
				(in r1689)
			)
	)

	(:action move-right-from-r1691
		:precondition
			(and 
				(in r1691)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1691))
				(in r1692)
			)
	)


	(:action move-left-from-r1691
		:precondition
			(and 
				(in r1691)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1691))
				(in r1690)
			)
	)

	(:action move-right-from-r1692
		:precondition
			(and 
				(in r1692)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1692))
				(in r1693)
			)
	)


	(:action move-left-from-r1692
		:precondition
			(and 
				(in r1692)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1692))
				(in r1691)
			)
	)

	(:action move-right-from-r1693
		:precondition
			(and 
				(in r1693)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1693))
				(in r1694)
			)
	)


	(:action move-left-from-r1693
		:precondition
			(and 
				(in r1693)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1693))
				(in r1692)
			)
	)

	(:action move-right-from-r1694
		:precondition
			(and 
				(in r1694)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1694))
				(in r1695)
			)
	)


	(:action move-left-from-r1694
		:precondition
			(and 
				(in r1694)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1694))
				(in r1693)
			)
	)

	(:action move-right-from-r1695
		:precondition
			(and 
				(in r1695)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1695))
				(in r1696)
			)
	)


	(:action move-left-from-r1695
		:precondition
			(and 
				(in r1695)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1695))
				(in r1694)
			)
	)

	(:action move-right-from-r1696
		:precondition
			(and 
				(in r1696)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1696))
				(in r1697)
			)
	)


	(:action move-left-from-r1696
		:precondition
			(and 
				(in r1696)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1696))
				(in r1695)
			)
	)

	(:action move-right-from-r1697
		:precondition
			(and 
				(in r1697)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1697))
				(in r1698)
			)
	)


	(:action move-left-from-r1697
		:precondition
			(and 
				(in r1697)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1697))
				(in r1696)
			)
	)

	(:action move-right-from-r1698
		:precondition
			(and 
				(in r1698)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1698))
				(in r1699)
			)
	)


	(:action move-left-from-r1698
		:precondition
			(and 
				(in r1698)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1698))
				(in r1697)
			)
	)

	(:action move-right-from-r1699
		:precondition
			(and 
				(in r1699)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1699))
				(in r1700)
			)
	)


	(:action move-left-from-r1699
		:precondition
			(and 
				(in r1699)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1699))
				(in r1698)
			)
	)

	(:action move-right-from-r1700
		:precondition
			(and 
				(in r1700)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1700))
				(in r1701)
			)
	)


	(:action move-left-from-r1700
		:precondition
			(and 
				(in r1700)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1700))
				(in r1699)
			)
	)

	(:action move-right-from-r1701
		:precondition
			(and 
				(in r1701)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1701))
				(in r1702)
			)
	)


	(:action move-left-from-r1701
		:precondition
			(and 
				(in r1701)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1701))
				(in r1700)
			)
	)

	(:action move-right-from-r1702
		:precondition
			(and 
				(in r1702)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1702))
				(in r1703)
			)
	)


	(:action move-left-from-r1702
		:precondition
			(and 
				(in r1702)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1702))
				(in r1701)
			)
	)

	(:action move-right-from-r1703
		:precondition
			(and 
				(in r1703)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1703))
				(in r1704)
			)
	)


	(:action move-left-from-r1703
		:precondition
			(and 
				(in r1703)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1703))
				(in r1702)
			)
	)

	(:action move-right-from-r1704
		:precondition
			(and 
				(in r1704)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1704))
				(in r1705)
			)
	)


	(:action move-left-from-r1704
		:precondition
			(and 
				(in r1704)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1704))
				(in r1703)
			)
	)

	(:action move-right-from-r1705
		:precondition
			(and 
				(in r1705)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1705))
				(in r1706)
			)
	)


	(:action move-left-from-r1705
		:precondition
			(and 
				(in r1705)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1705))
				(in r1704)
			)
	)

	(:action move-right-from-r1706
		:precondition
			(and 
				(in r1706)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1706))
				(in r1707)
			)
	)


	(:action move-left-from-r1706
		:precondition
			(and 
				(in r1706)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1706))
				(in r1705)
			)
	)

	(:action move-right-from-r1707
		:precondition
			(and 
				(in r1707)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1707))
				(in r1708)
			)
	)


	(:action move-left-from-r1707
		:precondition
			(and 
				(in r1707)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1707))
				(in r1706)
			)
	)

	(:action move-right-from-r1708
		:precondition
			(and 
				(in r1708)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1708))
				(in r1709)
			)
	)


	(:action move-left-from-r1708
		:precondition
			(and 
				(in r1708)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1708))
				(in r1707)
			)
	)

	(:action move-right-from-r1709
		:precondition
			(and 
				(in r1709)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1709))
				(in r1710)
			)
	)


	(:action move-left-from-r1709
		:precondition
			(and 
				(in r1709)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1709))
				(in r1708)
			)
	)

	(:action move-right-from-r1710
		:precondition
			(and 
				(in r1710)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1710))
				(in r1711)
			)
	)


	(:action move-left-from-r1710
		:precondition
			(and 
				(in r1710)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1710))
				(in r1709)
			)
	)

	(:action move-right-from-r1711
		:precondition
			(and 
				(in r1711)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1711))
				(in r1712)
			)
	)


	(:action move-left-from-r1711
		:precondition
			(and 
				(in r1711)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1711))
				(in r1710)
			)
	)

	(:action move-right-from-r1712
		:precondition
			(and 
				(in r1712)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1712))
				(in r1713)
			)
	)


	(:action move-left-from-r1712
		:precondition
			(and 
				(in r1712)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1712))
				(in r1711)
			)
	)

	(:action move-right-from-r1713
		:precondition
			(and 
				(in r1713)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1713))
				(in r1714)
			)
	)


	(:action move-left-from-r1713
		:precondition
			(and 
				(in r1713)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1713))
				(in r1712)
			)
	)

	(:action move-right-from-r1714
		:precondition
			(and 
				(in r1714)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1714))
				(in r1715)
			)
	)


	(:action move-left-from-r1714
		:precondition
			(and 
				(in r1714)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1714))
				(in r1713)
			)
	)

	(:action move-right-from-r1715
		:precondition
			(and 
				(in r1715)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1715))
				(in r1716)
			)
	)


	(:action move-left-from-r1715
		:precondition
			(and 
				(in r1715)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1715))
				(in r1714)
			)
	)

	(:action move-right-from-r1716
		:precondition
			(and 
				(in r1716)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1716))
				(in r1717)
			)
	)


	(:action move-left-from-r1716
		:precondition
			(and 
				(in r1716)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1716))
				(in r1715)
			)
	)

	(:action move-right-from-r1717
		:precondition
			(and 
				(in r1717)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1717))
				(in r1718)
			)
	)


	(:action move-left-from-r1717
		:precondition
			(and 
				(in r1717)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1717))
				(in r1716)
			)
	)

	(:action move-right-from-r1718
		:precondition
			(and 
				(in r1718)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1718))
				(in r1719)
			)
	)


	(:action move-left-from-r1718
		:precondition
			(and 
				(in r1718)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1718))
				(in r1717)
			)
	)

	(:action move-right-from-r1719
		:precondition
			(and 
				(in r1719)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1719))
				(in r1720)
			)
	)


	(:action move-left-from-r1719
		:precondition
			(and 
				(in r1719)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1719))
				(in r1718)
			)
	)

	(:action move-right-from-r1720
		:precondition
			(and 
				(in r1720)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1720))
				(in r1721)
			)
	)


	(:action move-left-from-r1720
		:precondition
			(and 
				(in r1720)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1720))
				(in r1719)
			)
	)

	(:action move-right-from-r1721
		:precondition
			(and 
				(in r1721)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1721))
				(in r1722)
			)
	)


	(:action move-left-from-r1721
		:precondition
			(and 
				(in r1721)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1721))
				(in r1720)
			)
	)

	(:action move-right-from-r1722
		:precondition
			(and 
				(in r1722)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1722))
				(in r1723)
			)
	)


	(:action move-left-from-r1722
		:precondition
			(and 
				(in r1722)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1722))
				(in r1721)
			)
	)

	(:action move-right-from-r1723
		:precondition
			(and 
				(in r1723)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1723))
				(in r1724)
			)
	)


	(:action move-left-from-r1723
		:precondition
			(and 
				(in r1723)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1723))
				(in r1722)
			)
	)

	(:action move-right-from-r1724
		:precondition
			(and 
				(in r1724)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1724))
				(in r1725)
			)
	)


	(:action move-left-from-r1724
		:precondition
			(and 
				(in r1724)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1724))
				(in r1723)
			)
	)

	(:action move-right-from-r1725
		:precondition
			(and 
				(in r1725)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1725))
				(in r1726)
			)
	)


	(:action move-left-from-r1725
		:precondition
			(and 
				(in r1725)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1725))
				(in r1724)
			)
	)

	(:action move-right-from-r1726
		:precondition
			(and 
				(in r1726)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1726))
				(in r1727)
			)
	)


	(:action move-left-from-r1726
		:precondition
			(and 
				(in r1726)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1726))
				(in r1725)
			)
	)

	(:action move-right-from-r1727
		:precondition
			(and 
				(in r1727)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1727))
				(in r1728)
			)
	)


	(:action move-left-from-r1727
		:precondition
			(and 
				(in r1727)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1727))
				(in r1726)
			)
	)

	(:action move-right-from-r1728
		:precondition
			(and 
				(in r1728)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1728))
				(in r1729)
			)
	)


	(:action move-left-from-r1728
		:precondition
			(and 
				(in r1728)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1728))
				(in r1727)
			)
	)

	(:action move-right-from-r1729
		:precondition
			(and 
				(in r1729)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1729))
				(in r1730)
			)
	)


	(:action move-left-from-r1729
		:precondition
			(and 
				(in r1729)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1729))
				(in r1728)
			)
	)

	(:action move-right-from-r1730
		:precondition
			(and 
				(in r1730)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1730))
				(in r1731)
			)
	)


	(:action move-left-from-r1730
		:precondition
			(and 
				(in r1730)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1730))
				(in r1729)
			)
	)

	(:action move-right-from-r1731
		:precondition
			(and 
				(in r1731)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1731))
				(in r1732)
			)
	)


	(:action move-left-from-r1731
		:precondition
			(and 
				(in r1731)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1731))
				(in r1730)
			)
	)

	(:action move-right-from-r1732
		:precondition
			(and 
				(in r1732)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1732))
				(in r1733)
			)
	)


	(:action move-left-from-r1732
		:precondition
			(and 
				(in r1732)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1732))
				(in r1731)
			)
	)

	(:action move-right-from-r1733
		:precondition
			(and 
				(in r1733)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1733))
				(in r1734)
			)
	)


	(:action move-left-from-r1733
		:precondition
			(and 
				(in r1733)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1733))
				(in r1732)
			)
	)

	(:action move-right-from-r1734
		:precondition
			(and 
				(in r1734)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1734))
				(in r1735)
			)
	)


	(:action move-left-from-r1734
		:precondition
			(and 
				(in r1734)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1734))
				(in r1733)
			)
	)

	(:action move-right-from-r1735
		:precondition
			(and 
				(in r1735)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1735))
				(in r1736)
			)
	)


	(:action move-left-from-r1735
		:precondition
			(and 
				(in r1735)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1735))
				(in r1734)
			)
	)

	(:action move-right-from-r1736
		:precondition
			(and 
				(in r1736)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1736))
				(in r1737)
			)
	)


	(:action move-left-from-r1736
		:precondition
			(and 
				(in r1736)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1736))
				(in r1735)
			)
	)

	(:action move-right-from-r1737
		:precondition
			(and 
				(in r1737)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1737))
				(in r1738)
			)
	)


	(:action move-left-from-r1737
		:precondition
			(and 
				(in r1737)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1737))
				(in r1736)
			)
	)

	(:action move-right-from-r1738
		:precondition
			(and 
				(in r1738)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1738))
				(in r1739)
			)
	)


	(:action move-left-from-r1738
		:precondition
			(and 
				(in r1738)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1738))
				(in r1737)
			)
	)

	(:action move-right-from-r1739
		:precondition
			(and 
				(in r1739)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1739))
				(in r1740)
			)
	)


	(:action move-left-from-r1739
		:precondition
			(and 
				(in r1739)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1739))
				(in r1738)
			)
	)

	(:action move-right-from-r1740
		:precondition
			(and 
				(in r1740)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1740))
				(in r1741)
			)
	)


	(:action move-left-from-r1740
		:precondition
			(and 
				(in r1740)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1740))
				(in r1739)
			)
	)

	(:action move-right-from-r1741
		:precondition
			(and 
				(in r1741)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1741))
				(in r1742)
			)
	)


	(:action move-left-from-r1741
		:precondition
			(and 
				(in r1741)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1741))
				(in r1740)
			)
	)

	(:action move-right-from-r1742
		:precondition
			(and 
				(in r1742)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1742))
				(in r1743)
			)
	)


	(:action move-left-from-r1742
		:precondition
			(and 
				(in r1742)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1742))
				(in r1741)
			)
	)

	(:action move-right-from-r1743
		:precondition
			(and 
				(in r1743)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1743))
				(in r1744)
			)
	)


	(:action move-left-from-r1743
		:precondition
			(and 
				(in r1743)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1743))
				(in r1742)
			)
	)

	(:action move-right-from-r1744
		:precondition
			(and 
				(in r1744)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1744))
				(in r1745)
			)
	)


	(:action move-left-from-r1744
		:precondition
			(and 
				(in r1744)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1744))
				(in r1743)
			)
	)

	(:action move-right-from-r1745
		:precondition
			(and 
				(in r1745)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1745))
				(in r1746)
			)
	)


	(:action move-left-from-r1745
		:precondition
			(and 
				(in r1745)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1745))
				(in r1744)
			)
	)

	(:action move-right-from-r1746
		:precondition
			(and 
				(in r1746)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1746))
				(in r1747)
			)
	)


	(:action move-left-from-r1746
		:precondition
			(and 
				(in r1746)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1746))
				(in r1745)
			)
	)

	(:action move-right-from-r1747
		:precondition
			(and 
				(in r1747)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1747))
				(in r1748)
			)
	)


	(:action move-left-from-r1747
		:precondition
			(and 
				(in r1747)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1747))
				(in r1746)
			)
	)

	(:action move-right-from-r1748
		:precondition
			(and 
				(in r1748)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1748))
				(in r1749)
			)
	)


	(:action move-left-from-r1748
		:precondition
			(and 
				(in r1748)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1748))
				(in r1747)
			)
	)

	(:action move-right-from-r1749
		:precondition
			(and 
				(in r1749)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1749))
				(in r1750)
			)
	)


	(:action move-left-from-r1749
		:precondition
			(and 
				(in r1749)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1749))
				(in r1748)
			)
	)

	(:action move-right-from-r1750
		:precondition
			(and 
				(in r1750)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1750))
				(in r1751)
			)
	)


	(:action move-left-from-r1750
		:precondition
			(and 
				(in r1750)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1750))
				(in r1749)
			)
	)

	(:action move-right-from-r1751
		:precondition
			(and 
				(in r1751)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1751))
				(in r1752)
			)
	)


	(:action move-left-from-r1751
		:precondition
			(and 
				(in r1751)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1751))
				(in r1750)
			)
	)

	(:action move-right-from-r1752
		:precondition
			(and 
				(in r1752)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1752))
				(in r1753)
			)
	)


	(:action move-left-from-r1752
		:precondition
			(and 
				(in r1752)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1752))
				(in r1751)
			)
	)

	(:action move-right-from-r1753
		:precondition
			(and 
				(in r1753)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1753))
				(in r1754)
			)
	)


	(:action move-left-from-r1753
		:precondition
			(and 
				(in r1753)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1753))
				(in r1752)
			)
	)

	(:action move-right-from-r1754
		:precondition
			(and 
				(in r1754)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1754))
				(in r1755)
			)
	)


	(:action move-left-from-r1754
		:precondition
			(and 
				(in r1754)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1754))
				(in r1753)
			)
	)

	(:action move-right-from-r1755
		:precondition
			(and 
				(in r1755)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1755))
				(in r1756)
			)
	)


	(:action move-left-from-r1755
		:precondition
			(and 
				(in r1755)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1755))
				(in r1754)
			)
	)

	(:action move-right-from-r1756
		:precondition
			(and 
				(in r1756)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1756))
				(in r1757)
			)
	)


	(:action move-left-from-r1756
		:precondition
			(and 
				(in r1756)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1756))
				(in r1755)
			)
	)

	(:action move-right-from-r1757
		:precondition
			(and 
				(in r1757)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1757))
				(in r1758)
			)
	)


	(:action move-left-from-r1757
		:precondition
			(and 
				(in r1757)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1757))
				(in r1756)
			)
	)

	(:action move-right-from-r1758
		:precondition
			(and 
				(in r1758)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1758))
				(in r1759)
			)
	)


	(:action move-left-from-r1758
		:precondition
			(and 
				(in r1758)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1758))
				(in r1757)
			)
	)

	(:action move-right-from-r1759
		:precondition
			(and 
				(in r1759)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1759))
				(in r1760)
			)
	)


	(:action move-left-from-r1759
		:precondition
			(and 
				(in r1759)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1759))
				(in r1758)
			)
	)

	(:action move-right-from-r1760
		:precondition
			(and 
				(in r1760)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1760))
				(in r1761)
			)
	)


	(:action move-left-from-r1760
		:precondition
			(and 
				(in r1760)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1760))
				(in r1759)
			)
	)

	(:action move-right-from-r1761
		:precondition
			(and 
				(in r1761)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1761))
				(in r1762)
			)
	)


	(:action move-left-from-r1761
		:precondition
			(and 
				(in r1761)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1761))
				(in r1760)
			)
	)

	(:action move-right-from-r1762
		:precondition
			(and 
				(in r1762)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1762))
				(in r1763)
			)
	)


	(:action move-left-from-r1762
		:precondition
			(and 
				(in r1762)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1762))
				(in r1761)
			)
	)

	(:action move-right-from-r1763
		:precondition
			(and 
				(in r1763)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1763))
				(in r1764)
			)
	)


	(:action move-left-from-r1763
		:precondition
			(and 
				(in r1763)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1763))
				(in r1762)
			)
	)

	(:action move-right-from-r1764
		:precondition
			(and 
				(in r1764)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1764))
				(in r1765)
			)
	)


	(:action move-left-from-r1764
		:precondition
			(and 
				(in r1764)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1764))
				(in r1763)
			)
	)

	(:action move-right-from-r1765
		:precondition
			(and 
				(in r1765)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1765))
				(in r1766)
			)
	)


	(:action move-left-from-r1765
		:precondition
			(and 
				(in r1765)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1765))
				(in r1764)
			)
	)

	(:action move-right-from-r1766
		:precondition
			(and 
				(in r1766)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1766))
				(in r1767)
			)
	)


	(:action move-left-from-r1766
		:precondition
			(and 
				(in r1766)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1766))
				(in r1765)
			)
	)

	(:action move-right-from-r1767
		:precondition
			(and 
				(in r1767)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1767))
				(in r1768)
			)
	)


	(:action move-left-from-r1767
		:precondition
			(and 
				(in r1767)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1767))
				(in r1766)
			)
	)

	(:action move-right-from-r1768
		:precondition
			(and 
				(in r1768)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1768))
				(in r1769)
			)
	)


	(:action move-left-from-r1768
		:precondition
			(and 
				(in r1768)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1768))
				(in r1767)
			)
	)

	(:action move-right-from-r1769
		:precondition
			(and 
				(in r1769)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1769))
				(in r1770)
			)
	)


	(:action move-left-from-r1769
		:precondition
			(and 
				(in r1769)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1769))
				(in r1768)
			)
	)

	(:action move-right-from-r1770
		:precondition
			(and 
				(in r1770)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1770))
				(in r1771)
			)
	)


	(:action move-left-from-r1770
		:precondition
			(and 
				(in r1770)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1770))
				(in r1769)
			)
	)

	(:action move-right-from-r1771
		:precondition
			(and 
				(in r1771)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1771))
				(in r1772)
			)
	)


	(:action move-left-from-r1771
		:precondition
			(and 
				(in r1771)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1771))
				(in r1770)
			)
	)

	(:action move-right-from-r1772
		:precondition
			(and 
				(in r1772)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1772))
				(in r1773)
			)
	)


	(:action move-left-from-r1772
		:precondition
			(and 
				(in r1772)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1772))
				(in r1771)
			)
	)

	(:action move-right-from-r1773
		:precondition
			(and 
				(in r1773)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1773))
				(in r1774)
			)
	)


	(:action move-left-from-r1773
		:precondition
			(and 
				(in r1773)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1773))
				(in r1772)
			)
	)

	(:action move-right-from-r1774
		:precondition
			(and 
				(in r1774)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1774))
				(in r1775)
			)
	)


	(:action move-left-from-r1774
		:precondition
			(and 
				(in r1774)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1774))
				(in r1773)
			)
	)

	(:action move-right-from-r1775
		:precondition
			(and 
				(in r1775)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1775))
				(in r1776)
			)
	)


	(:action move-left-from-r1775
		:precondition
			(and 
				(in r1775)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1775))
				(in r1774)
			)
	)

	(:action move-right-from-r1776
		:precondition
			(and 
				(in r1776)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1776))
				(in r1777)
			)
	)


	(:action move-left-from-r1776
		:precondition
			(and 
				(in r1776)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1776))
				(in r1775)
			)
	)

	(:action move-right-from-r1777
		:precondition
			(and 
				(in r1777)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1777))
				(in r1778)
			)
	)


	(:action move-left-from-r1777
		:precondition
			(and 
				(in r1777)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1777))
				(in r1776)
			)
	)

	(:action move-right-from-r1778
		:precondition
			(and 
				(in r1778)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1778))
				(in r1779)
			)
	)


	(:action move-left-from-r1778
		:precondition
			(and 
				(in r1778)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1778))
				(in r1777)
			)
	)

	(:action move-right-from-r1779
		:precondition
			(and 
				(in r1779)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1779))
				(in r1780)
			)
	)


	(:action move-left-from-r1779
		:precondition
			(and 
				(in r1779)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1779))
				(in r1778)
			)
	)

	(:action move-right-from-r1780
		:precondition
			(and 
				(in r1780)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1780))
				(in r1781)
			)
	)


	(:action move-left-from-r1780
		:precondition
			(and 
				(in r1780)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1780))
				(in r1779)
			)
	)

	(:action move-right-from-r1781
		:precondition
			(and 
				(in r1781)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1781))
				(in r1782)
			)
	)


	(:action move-left-from-r1781
		:precondition
			(and 
				(in r1781)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1781))
				(in r1780)
			)
	)

	(:action move-right-from-r1782
		:precondition
			(and 
				(in r1782)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1782))
				(in r1783)
			)
	)


	(:action move-left-from-r1782
		:precondition
			(and 
				(in r1782)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1782))
				(in r1781)
			)
	)

	(:action move-right-from-r1783
		:precondition
			(and 
				(in r1783)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1783))
				(in r1784)
			)
	)


	(:action move-left-from-r1783
		:precondition
			(and 
				(in r1783)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1783))
				(in r1782)
			)
	)

	(:action move-right-from-r1784
		:precondition
			(and 
				(in r1784)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1784))
				(in r1785)
			)
	)


	(:action move-left-from-r1784
		:precondition
			(and 
				(in r1784)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1784))
				(in r1783)
			)
	)

	(:action move-right-from-r1785
		:precondition
			(and 
				(in r1785)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1785))
				(in r1786)
			)
	)


	(:action move-left-from-r1785
		:precondition
			(and 
				(in r1785)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1785))
				(in r1784)
			)
	)

	(:action move-right-from-r1786
		:precondition
			(and 
				(in r1786)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1786))
				(in r1787)
			)
	)


	(:action move-left-from-r1786
		:precondition
			(and 
				(in r1786)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1786))
				(in r1785)
			)
	)

	(:action move-right-from-r1787
		:precondition
			(and 
				(in r1787)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1787))
				(in r1788)
			)
	)


	(:action move-left-from-r1787
		:precondition
			(and 
				(in r1787)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1787))
				(in r1786)
			)
	)

	(:action move-right-from-r1788
		:precondition
			(and 
				(in r1788)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1788))
				(in r1789)
			)
	)


	(:action move-left-from-r1788
		:precondition
			(and 
				(in r1788)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1788))
				(in r1787)
			)
	)

	(:action move-right-from-r1789
		:precondition
			(and 
				(in r1789)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1789))
				(in r1790)
			)
	)


	(:action move-left-from-r1789
		:precondition
			(and 
				(in r1789)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1789))
				(in r1788)
			)
	)

	(:action move-right-from-r1790
		:precondition
			(and 
				(in r1790)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1790))
				(in r1791)
			)
	)


	(:action move-left-from-r1790
		:precondition
			(and 
				(in r1790)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1790))
				(in r1789)
			)
	)

	(:action move-right-from-r1791
		:precondition
			(and 
				(in r1791)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1791))
				(in r1792)
			)
	)


	(:action move-left-from-r1791
		:precondition
			(and 
				(in r1791)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1791))
				(in r1790)
			)
	)

	(:action move-right-from-r1792
		:precondition
			(and 
				(in r1792)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1792))
				(in r1793)
			)
	)


	(:action move-left-from-r1792
		:precondition
			(and 
				(in r1792)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1792))
				(in r1791)
			)
	)

	(:action move-right-from-r1793
		:precondition
			(and 
				(in r1793)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1793))
				(in r1794)
			)
	)


	(:action move-left-from-r1793
		:precondition
			(and 
				(in r1793)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1793))
				(in r1792)
			)
	)

	(:action move-right-from-r1794
		:precondition
			(and 
				(in r1794)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1794))
				(in r1795)
			)
	)


	(:action move-left-from-r1794
		:precondition
			(and 
				(in r1794)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1794))
				(in r1793)
			)
	)

	(:action move-right-from-r1795
		:precondition
			(and 
				(in r1795)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1795))
				(in r1796)
			)
	)


	(:action move-left-from-r1795
		:precondition
			(and 
				(in r1795)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1795))
				(in r1794)
			)
	)

	(:action move-right-from-r1796
		:precondition
			(and 
				(in r1796)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1796))
				(in r1797)
			)
	)


	(:action move-left-from-r1796
		:precondition
			(and 
				(in r1796)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1796))
				(in r1795)
			)
	)

	(:action move-right-from-r1797
		:precondition
			(and 
				(in r1797)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1797))
				(in r1798)
			)
	)


	(:action move-left-from-r1797
		:precondition
			(and 
				(in r1797)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1797))
				(in r1796)
			)
	)

	(:action move-right-from-r1798
		:precondition
			(and 
				(in r1798)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1798))
				(in r1799)
			)
	)


	(:action move-left-from-r1798
		:precondition
			(and 
				(in r1798)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1798))
				(in r1797)
			)
	)

	(:action move-right-from-r1799
		:precondition
			(and 
				(in r1799)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1799))
				(in r1800)
			)
	)


	(:action move-left-from-r1799
		:precondition
			(and 
				(in r1799)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1799))
				(in r1798)
			)
	)

	(:action move-right-from-r1800
		:precondition
			(and 
				(in r1800)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1800))
				(in r1801)
			)
	)


	(:action move-left-from-r1800
		:precondition
			(and 
				(in r1800)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1800))
				(in r1799)
			)
	)

	(:action move-right-from-r1801
		:precondition
			(and 
				(in r1801)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1801))
				(in r1802)
			)
	)


	(:action move-left-from-r1801
		:precondition
			(and 
				(in r1801)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1801))
				(in r1800)
			)
	)

	(:action move-right-from-r1802
		:precondition
			(and 
				(in r1802)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1802))
				(in r1803)
			)
	)


	(:action move-left-from-r1802
		:precondition
			(and 
				(in r1802)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1802))
				(in r1801)
			)
	)

	(:action move-right-from-r1803
		:precondition
			(and 
				(in r1803)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1803))
				(in r1804)
			)
	)


	(:action move-left-from-r1803
		:precondition
			(and 
				(in r1803)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1803))
				(in r1802)
			)
	)

	(:action move-right-from-r1804
		:precondition
			(and 
				(in r1804)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1804))
				(in r1805)
			)
	)


	(:action move-left-from-r1804
		:precondition
			(and 
				(in r1804)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1804))
				(in r1803)
			)
	)

	(:action move-right-from-r1805
		:precondition
			(and 
				(in r1805)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1805))
				(in r1806)
			)
	)


	(:action move-left-from-r1805
		:precondition
			(and 
				(in r1805)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1805))
				(in r1804)
			)
	)

	(:action move-right-from-r1806
		:precondition
			(and 
				(in r1806)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1806))
				(in r1807)
			)
	)


	(:action move-left-from-r1806
		:precondition
			(and 
				(in r1806)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1806))
				(in r1805)
			)
	)

	(:action move-right-from-r1807
		:precondition
			(and 
				(in r1807)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1807))
				(in r1808)
			)
	)


	(:action move-left-from-r1807
		:precondition
			(and 
				(in r1807)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1807))
				(in r1806)
			)
	)

	(:action move-right-from-r1808
		:precondition
			(and 
				(in r1808)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1808))
				(in r1809)
			)
	)


	(:action move-left-from-r1808
		:precondition
			(and 
				(in r1808)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1808))
				(in r1807)
			)
	)

	(:action move-right-from-r1809
		:precondition
			(and 
				(in r1809)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1809))
				(in r1810)
			)
	)


	(:action move-left-from-r1809
		:precondition
			(and 
				(in r1809)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1809))
				(in r1808)
			)
	)

	(:action move-right-from-r1810
		:precondition
			(and 
				(in r1810)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1810))
				(in r1811)
			)
	)


	(:action move-left-from-r1810
		:precondition
			(and 
				(in r1810)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1810))
				(in r1809)
			)
	)

	(:action move-right-from-r1811
		:precondition
			(and 
				(in r1811)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1811))
				(in r1812)
			)
	)


	(:action move-left-from-r1811
		:precondition
			(and 
				(in r1811)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1811))
				(in r1810)
			)
	)

	(:action move-right-from-r1812
		:precondition
			(and 
				(in r1812)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1812))
				(in r1813)
			)
	)


	(:action move-left-from-r1812
		:precondition
			(and 
				(in r1812)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1812))
				(in r1811)
			)
	)

	(:action move-right-from-r1813
		:precondition
			(and 
				(in r1813)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1813))
				(in r1814)
			)
	)


	(:action move-left-from-r1813
		:precondition
			(and 
				(in r1813)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1813))
				(in r1812)
			)
	)

	(:action move-right-from-r1814
		:precondition
			(and 
				(in r1814)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1814))
				(in r1815)
			)
	)


	(:action move-left-from-r1814
		:precondition
			(and 
				(in r1814)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1814))
				(in r1813)
			)
	)

	(:action move-right-from-r1815
		:precondition
			(and 
				(in r1815)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1815))
				(in r1816)
			)
	)


	(:action move-left-from-r1815
		:precondition
			(and 
				(in r1815)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1815))
				(in r1814)
			)
	)

	(:action move-right-from-r1816
		:precondition
			(and 
				(in r1816)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1816))
				(in r1817)
			)
	)


	(:action move-left-from-r1816
		:precondition
			(and 
				(in r1816)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1816))
				(in r1815)
			)
	)

	(:action move-right-from-r1817
		:precondition
			(and 
				(in r1817)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1817))
				(in r1818)
			)
	)


	(:action move-left-from-r1817
		:precondition
			(and 
				(in r1817)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1817))
				(in r1816)
			)
	)

	(:action move-right-from-r1818
		:precondition
			(and 
				(in r1818)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1818))
				(in r1819)
			)
	)


	(:action move-left-from-r1818
		:precondition
			(and 
				(in r1818)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1818))
				(in r1817)
			)
	)

	(:action move-right-from-r1819
		:precondition
			(and 
				(in r1819)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1819))
				(in r1820)
			)
	)


	(:action move-left-from-r1819
		:precondition
			(and 
				(in r1819)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1819))
				(in r1818)
			)
	)

	(:action move-right-from-r1820
		:precondition
			(and 
				(in r1820)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1820))
				(in r1821)
			)
	)


	(:action move-left-from-r1820
		:precondition
			(and 
				(in r1820)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1820))
				(in r1819)
			)
	)

	(:action move-right-from-r1821
		:precondition
			(and 
				(in r1821)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1821))
				(in r1822)
			)
	)


	(:action move-left-from-r1821
		:precondition
			(and 
				(in r1821)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1821))
				(in r1820)
			)
	)

	(:action move-right-from-r1822
		:precondition
			(and 
				(in r1822)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1822))
				(in r1823)
			)
	)


	(:action move-left-from-r1822
		:precondition
			(and 
				(in r1822)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1822))
				(in r1821)
			)
	)

	(:action move-right-from-r1823
		:precondition
			(and 
				(in r1823)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1823))
				(in r1824)
			)
	)


	(:action move-left-from-r1823
		:precondition
			(and 
				(in r1823)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1823))
				(in r1822)
			)
	)

	(:action move-right-from-r1824
		:precondition
			(and 
				(in r1824)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1824))
				(in r1825)
			)
	)


	(:action move-left-from-r1824
		:precondition
			(and 
				(in r1824)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1824))
				(in r1823)
			)
	)

	(:action move-right-from-r1825
		:precondition
			(and 
				(in r1825)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1825))
				(in r1826)
			)
	)


	(:action move-left-from-r1825
		:precondition
			(and 
				(in r1825)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1825))
				(in r1824)
			)
	)

	(:action move-right-from-r1826
		:precondition
			(and 
				(in r1826)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1826))
				(in r1827)
			)
	)


	(:action move-left-from-r1826
		:precondition
			(and 
				(in r1826)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1826))
				(in r1825)
			)
	)

	(:action move-right-from-r1827
		:precondition
			(and 
				(in r1827)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1827))
				(in r1828)
			)
	)


	(:action move-left-from-r1827
		:precondition
			(and 
				(in r1827)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1827))
				(in r1826)
			)
	)

	(:action move-right-from-r1828
		:precondition
			(and 
				(in r1828)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1828))
				(in r1829)
			)
	)


	(:action move-left-from-r1828
		:precondition
			(and 
				(in r1828)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1828))
				(in r1827)
			)
	)

	(:action move-right-from-r1829
		:precondition
			(and 
				(in r1829)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1829))
				(in r1830)
			)
	)


	(:action move-left-from-r1829
		:precondition
			(and 
				(in r1829)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1829))
				(in r1828)
			)
	)

	(:action move-right-from-r1830
		:precondition
			(and 
				(in r1830)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1830))
				(in r1831)
			)
	)


	(:action move-left-from-r1830
		:precondition
			(and 
				(in r1830)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1830))
				(in r1829)
			)
	)

	(:action move-right-from-r1831
		:precondition
			(and 
				(in r1831)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1831))
				(in r1832)
			)
	)


	(:action move-left-from-r1831
		:precondition
			(and 
				(in r1831)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1831))
				(in r1830)
			)
	)

	(:action move-right-from-r1832
		:precondition
			(and 
				(in r1832)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1832))
				(in r1833)
			)
	)


	(:action move-left-from-r1832
		:precondition
			(and 
				(in r1832)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1832))
				(in r1831)
			)
	)

	(:action move-right-from-r1833
		:precondition
			(and 
				(in r1833)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1833))
				(in r1834)
			)
	)


	(:action move-left-from-r1833
		:precondition
			(and 
				(in r1833)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1833))
				(in r1832)
			)
	)

	(:action move-right-from-r1834
		:precondition
			(and 
				(in r1834)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1834))
				(in r1835)
			)
	)


	(:action move-left-from-r1834
		:precondition
			(and 
				(in r1834)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1834))
				(in r1833)
			)
	)

	(:action move-right-from-r1835
		:precondition
			(and 
				(in r1835)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1835))
				(in r1836)
			)
	)


	(:action move-left-from-r1835
		:precondition
			(and 
				(in r1835)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1835))
				(in r1834)
			)
	)

	(:action move-right-from-r1836
		:precondition
			(and 
				(in r1836)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1836))
				(in r1837)
			)
	)


	(:action move-left-from-r1836
		:precondition
			(and 
				(in r1836)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1836))
				(in r1835)
			)
	)

	(:action move-right-from-r1837
		:precondition
			(and 
				(in r1837)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1837))
				(in r1838)
			)
	)


	(:action move-left-from-r1837
		:precondition
			(and 
				(in r1837)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1837))
				(in r1836)
			)
	)

	(:action move-right-from-r1838
		:precondition
			(and 
				(in r1838)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1838))
				(in r1839)
			)
	)


	(:action move-left-from-r1838
		:precondition
			(and 
				(in r1838)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1838))
				(in r1837)
			)
	)

	(:action move-right-from-r1839
		:precondition
			(and 
				(in r1839)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1839))
				(in r1840)
			)
	)


	(:action move-left-from-r1839
		:precondition
			(and 
				(in r1839)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1839))
				(in r1838)
			)
	)

	(:action move-right-from-r1840
		:precondition
			(and 
				(in r1840)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1840))
				(in r1841)
			)
	)


	(:action move-left-from-r1840
		:precondition
			(and 
				(in r1840)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1840))
				(in r1839)
			)
	)

	(:action move-right-from-r1841
		:precondition
			(and 
				(in r1841)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1841))
				(in r1842)
			)
	)


	(:action move-left-from-r1841
		:precondition
			(and 
				(in r1841)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1841))
				(in r1840)
			)
	)

	(:action move-right-from-r1842
		:precondition
			(and 
				(in r1842)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1842))
				(in r1843)
			)
	)


	(:action move-left-from-r1842
		:precondition
			(and 
				(in r1842)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1842))
				(in r1841)
			)
	)

	(:action move-right-from-r1843
		:precondition
			(and 
				(in r1843)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1843))
				(in r1844)
			)
	)


	(:action move-left-from-r1843
		:precondition
			(and 
				(in r1843)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1843))
				(in r1842)
			)
	)

	(:action move-right-from-r1844
		:precondition
			(and 
				(in r1844)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1844))
				(in r1845)
			)
	)


	(:action move-left-from-r1844
		:precondition
			(and 
				(in r1844)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1844))
				(in r1843)
			)
	)

	(:action move-right-from-r1845
		:precondition
			(and 
				(in r1845)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1845))
				(in r1846)
			)
	)


	(:action move-left-from-r1845
		:precondition
			(and 
				(in r1845)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1845))
				(in r1844)
			)
	)

	(:action move-right-from-r1846
		:precondition
			(and 
				(in r1846)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1846))
				(in r1847)
			)
	)


	(:action move-left-from-r1846
		:precondition
			(and 
				(in r1846)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1846))
				(in r1845)
			)
	)

	(:action move-right-from-r1847
		:precondition
			(and 
				(in r1847)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1847))
				(in r1848)
			)
	)


	(:action move-left-from-r1847
		:precondition
			(and 
				(in r1847)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1847))
				(in r1846)
			)
	)

	(:action move-right-from-r1848
		:precondition
			(and 
				(in r1848)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1848))
				(in r1849)
			)
	)


	(:action move-left-from-r1848
		:precondition
			(and 
				(in r1848)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1848))
				(in r1847)
			)
	)

	(:action move-right-from-r1849
		:precondition
			(and 
				(in r1849)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1849))
				(in r1850)
			)
	)


	(:action move-left-from-r1849
		:precondition
			(and 
				(in r1849)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1849))
				(in r1848)
			)
	)

	(:action move-right-from-r1850
		:precondition
			(and 
				(in r1850)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1850))
				(in r1851)
			)
	)


	(:action move-left-from-r1850
		:precondition
			(and 
				(in r1850)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1850))
				(in r1849)
			)
	)

	(:action move-right-from-r1851
		:precondition
			(and 
				(in r1851)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1851))
				(in r1852)
			)
	)


	(:action move-left-from-r1851
		:precondition
			(and 
				(in r1851)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1851))
				(in r1850)
			)
	)

	(:action move-right-from-r1852
		:precondition
			(and 
				(in r1852)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1852))
				(in r1853)
			)
	)


	(:action move-left-from-r1852
		:precondition
			(and 
				(in r1852)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1852))
				(in r1851)
			)
	)

	(:action move-right-from-r1853
		:precondition
			(and 
				(in r1853)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1853))
				(in r1854)
			)
	)


	(:action move-left-from-r1853
		:precondition
			(and 
				(in r1853)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1853))
				(in r1852)
			)
	)

	(:action move-right-from-r1854
		:precondition
			(and 
				(in r1854)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1854))
				(in r1855)
			)
	)


	(:action move-left-from-r1854
		:precondition
			(and 
				(in r1854)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1854))
				(in r1853)
			)
	)

	(:action move-right-from-r1855
		:precondition
			(and 
				(in r1855)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1855))
				(in r1856)
			)
	)


	(:action move-left-from-r1855
		:precondition
			(and 
				(in r1855)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1855))
				(in r1854)
			)
	)

	(:action move-right-from-r1856
		:precondition
			(and 
				(in r1856)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1856))
				(in r1857)
			)
	)


	(:action move-left-from-r1856
		:precondition
			(and 
				(in r1856)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1856))
				(in r1855)
			)
	)

	(:action move-right-from-r1857
		:precondition
			(and 
				(in r1857)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1857))
				(in r1858)
			)
	)


	(:action move-left-from-r1857
		:precondition
			(and 
				(in r1857)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1857))
				(in r1856)
			)
	)

	(:action move-right-from-r1858
		:precondition
			(and 
				(in r1858)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1858))
				(in r1859)
			)
	)


	(:action move-left-from-r1858
		:precondition
			(and 
				(in r1858)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1858))
				(in r1857)
			)
	)

	(:action move-right-from-r1859
		:precondition
			(and 
				(in r1859)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1859))
				(in r1860)
			)
	)


	(:action move-left-from-r1859
		:precondition
			(and 
				(in r1859)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1859))
				(in r1858)
			)
	)

	(:action move-right-from-r1860
		:precondition
			(and 
				(in r1860)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1860))
				(in r1861)
			)
	)


	(:action move-left-from-r1860
		:precondition
			(and 
				(in r1860)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1860))
				(in r1859)
			)
	)

	(:action move-right-from-r1861
		:precondition
			(and 
				(in r1861)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1861))
				(in r1862)
			)
	)


	(:action move-left-from-r1861
		:precondition
			(and 
				(in r1861)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1861))
				(in r1860)
			)
	)

	(:action move-right-from-r1862
		:precondition
			(and 
				(in r1862)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1862))
				(in r1863)
			)
	)


	(:action move-left-from-r1862
		:precondition
			(and 
				(in r1862)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1862))
				(in r1861)
			)
	)

	(:action move-right-from-r1863
		:precondition
			(and 
				(in r1863)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1863))
				(in r1864)
			)
	)


	(:action move-left-from-r1863
		:precondition
			(and 
				(in r1863)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1863))
				(in r1862)
			)
	)

	(:action move-right-from-r1864
		:precondition
			(and 
				(in r1864)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1864))
				(in r1865)
			)
	)


	(:action move-left-from-r1864
		:precondition
			(and 
				(in r1864)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1864))
				(in r1863)
			)
	)

	(:action move-right-from-r1865
		:precondition
			(and 
				(in r1865)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1865))
				(in r1866)
			)
	)


	(:action move-left-from-r1865
		:precondition
			(and 
				(in r1865)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1865))
				(in r1864)
			)
	)

	(:action move-right-from-r1866
		:precondition
			(and 
				(in r1866)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1866))
				(in r1867)
			)
	)


	(:action move-left-from-r1866
		:precondition
			(and 
				(in r1866)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1866))
				(in r1865)
			)
	)

	(:action move-right-from-r1867
		:precondition
			(and 
				(in r1867)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1867))
				(in r1868)
			)
	)


	(:action move-left-from-r1867
		:precondition
			(and 
				(in r1867)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1867))
				(in r1866)
			)
	)

	(:action move-right-from-r1868
		:precondition
			(and 
				(in r1868)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1868))
				(in r1869)
			)
	)


	(:action move-left-from-r1868
		:precondition
			(and 
				(in r1868)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1868))
				(in r1867)
			)
	)

	(:action move-right-from-r1869
		:precondition
			(and 
				(in r1869)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1869))
				(in r1870)
			)
	)


	(:action move-left-from-r1869
		:precondition
			(and 
				(in r1869)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1869))
				(in r1868)
			)
	)

	(:action move-right-from-r1870
		:precondition
			(and 
				(in r1870)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1870))
				(in r1871)
			)
	)


	(:action move-left-from-r1870
		:precondition
			(and 
				(in r1870)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1870))
				(in r1869)
			)
	)

	(:action move-right-from-r1871
		:precondition
			(and 
				(in r1871)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1871))
				(in r1872)
			)
	)


	(:action move-left-from-r1871
		:precondition
			(and 
				(in r1871)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1871))
				(in r1870)
			)
	)

	(:action move-right-from-r1872
		:precondition
			(and 
				(in r1872)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1872))
				(in r1873)
			)
	)


	(:action move-left-from-r1872
		:precondition
			(and 
				(in r1872)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1872))
				(in r1871)
			)
	)

	(:action move-right-from-r1873
		:precondition
			(and 
				(in r1873)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1873))
				(in r1874)
			)
	)


	(:action move-left-from-r1873
		:precondition
			(and 
				(in r1873)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1873))
				(in r1872)
			)
	)

	(:action move-right-from-r1874
		:precondition
			(and 
				(in r1874)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1874))
				(in r1875)
			)
	)


	(:action move-left-from-r1874
		:precondition
			(and 
				(in r1874)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1874))
				(in r1873)
			)
	)

	(:action move-right-from-r1875
		:precondition
			(and 
				(in r1875)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1875))
				(in r1876)
			)
	)


	(:action move-left-from-r1875
		:precondition
			(and 
				(in r1875)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1875))
				(in r1874)
			)
	)

	(:action move-right-from-r1876
		:precondition
			(and 
				(in r1876)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1876))
				(in r1877)
			)
	)


	(:action move-left-from-r1876
		:precondition
			(and 
				(in r1876)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1876))
				(in r1875)
			)
	)

	(:action move-right-from-r1877
		:precondition
			(and 
				(in r1877)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1877))
				(in r1878)
			)
	)


	(:action move-left-from-r1877
		:precondition
			(and 
				(in r1877)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1877))
				(in r1876)
			)
	)

	(:action move-right-from-r1878
		:precondition
			(and 
				(in r1878)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1878))
				(in r1879)
			)
	)


	(:action move-left-from-r1878
		:precondition
			(and 
				(in r1878)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1878))
				(in r1877)
			)
	)

	(:action move-right-from-r1879
		:precondition
			(and 
				(in r1879)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1879))
				(in r1880)
			)
	)


	(:action move-left-from-r1879
		:precondition
			(and 
				(in r1879)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1879))
				(in r1878)
			)
	)

	(:action move-right-from-r1880
		:precondition
			(and 
				(in r1880)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1880))
				(in r1881)
			)
	)


	(:action move-left-from-r1880
		:precondition
			(and 
				(in r1880)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1880))
				(in r1879)
			)
	)

	(:action move-right-from-r1881
		:precondition
			(and 
				(in r1881)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1881))
				(in r1882)
			)
	)


	(:action move-left-from-r1881
		:precondition
			(and 
				(in r1881)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1881))
				(in r1880)
			)
	)

	(:action move-right-from-r1882
		:precondition
			(and 
				(in r1882)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1882))
				(in r1883)
			)
	)


	(:action move-left-from-r1882
		:precondition
			(and 
				(in r1882)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1882))
				(in r1881)
			)
	)

	(:action move-right-from-r1883
		:precondition
			(and 
				(in r1883)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1883))
				(in r1884)
			)
	)


	(:action move-left-from-r1883
		:precondition
			(and 
				(in r1883)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1883))
				(in r1882)
			)
	)

	(:action move-right-from-r1884
		:precondition
			(and 
				(in r1884)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1884))
				(in r1885)
			)
	)


	(:action move-left-from-r1884
		:precondition
			(and 
				(in r1884)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1884))
				(in r1883)
			)
	)

	(:action move-right-from-r1885
		:precondition
			(and 
				(in r1885)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1885))
				(in r1886)
			)
	)


	(:action move-left-from-r1885
		:precondition
			(and 
				(in r1885)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1885))
				(in r1884)
			)
	)

	(:action move-right-from-r1886
		:precondition
			(and 
				(in r1886)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1886))
				(in r1887)
			)
	)


	(:action move-left-from-r1886
		:precondition
			(and 
				(in r1886)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1886))
				(in r1885)
			)
	)

	(:action move-right-from-r1887
		:precondition
			(and 
				(in r1887)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1887))
				(in r1888)
			)
	)


	(:action move-left-from-r1887
		:precondition
			(and 
				(in r1887)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1887))
				(in r1886)
			)
	)

	(:action move-right-from-r1888
		:precondition
			(and 
				(in r1888)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1888))
				(in r1889)
			)
	)


	(:action move-left-from-r1888
		:precondition
			(and 
				(in r1888)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1888))
				(in r1887)
			)
	)

	(:action move-right-from-r1889
		:precondition
			(and 
				(in r1889)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1889))
				(in r1890)
			)
	)


	(:action move-left-from-r1889
		:precondition
			(and 
				(in r1889)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1889))
				(in r1888)
			)
	)

	(:action move-right-from-r1890
		:precondition
			(and 
				(in r1890)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1890))
				(in r1891)
			)
	)


	(:action move-left-from-r1890
		:precondition
			(and 
				(in r1890)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1890))
				(in r1889)
			)
	)

	(:action move-right-from-r1891
		:precondition
			(and 
				(in r1891)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1891))
				(in r1892)
			)
	)


	(:action move-left-from-r1891
		:precondition
			(and 
				(in r1891)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1891))
				(in r1890)
			)
	)

	(:action move-right-from-r1892
		:precondition
			(and 
				(in r1892)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1892))
				(in r1893)
			)
	)


	(:action move-left-from-r1892
		:precondition
			(and 
				(in r1892)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1892))
				(in r1891)
			)
	)

	(:action move-right-from-r1893
		:precondition
			(and 
				(in r1893)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1893))
				(in r1894)
			)
	)


	(:action move-left-from-r1893
		:precondition
			(and 
				(in r1893)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1893))
				(in r1892)
			)
	)

	(:action move-right-from-r1894
		:precondition
			(and 
				(in r1894)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1894))
				(in r1895)
			)
	)


	(:action move-left-from-r1894
		:precondition
			(and 
				(in r1894)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1894))
				(in r1893)
			)
	)

	(:action move-right-from-r1895
		:precondition
			(and 
				(in r1895)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1895))
				(in r1896)
			)
	)


	(:action move-left-from-r1895
		:precondition
			(and 
				(in r1895)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1895))
				(in r1894)
			)
	)

	(:action move-right-from-r1896
		:precondition
			(and 
				(in r1896)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1896))
				(in r1897)
			)
	)


	(:action move-left-from-r1896
		:precondition
			(and 
				(in r1896)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1896))
				(in r1895)
			)
	)

	(:action move-right-from-r1897
		:precondition
			(and 
				(in r1897)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1897))
				(in r1898)
			)
	)


	(:action move-left-from-r1897
		:precondition
			(and 
				(in r1897)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1897))
				(in r1896)
			)
	)

	(:action move-right-from-r1898
		:precondition
			(and 
				(in r1898)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1898))
				(in r1899)
			)
	)


	(:action move-left-from-r1898
		:precondition
			(and 
				(in r1898)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1898))
				(in r1897)
			)
	)

	(:action move-right-from-r1899
		:precondition
			(and 
				(in r1899)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1899))
				(in r1900)
			)
	)


	(:action move-left-from-r1899
		:precondition
			(and 
				(in r1899)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1899))
				(in r1898)
			)
	)

	(:action move-right-from-r1900
		:precondition
			(and 
				(in r1900)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1900))
				(in r1901)
			)
	)


	(:action move-left-from-r1900
		:precondition
			(and 
				(in r1900)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1900))
				(in r1899)
			)
	)

	(:action move-right-from-r1901
		:precondition
			(and 
				(in r1901)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1901))
				(in r1902)
			)
	)


	(:action move-left-from-r1901
		:precondition
			(and 
				(in r1901)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1901))
				(in r1900)
			)
	)

	(:action move-right-from-r1902
		:precondition
			(and 
				(in r1902)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1902))
				(in r1903)
			)
	)


	(:action move-left-from-r1902
		:precondition
			(and 
				(in r1902)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1902))
				(in r1901)
			)
	)

	(:action move-right-from-r1903
		:precondition
			(and 
				(in r1903)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1903))
				(in r1904)
			)
	)


	(:action move-left-from-r1903
		:precondition
			(and 
				(in r1903)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1903))
				(in r1902)
			)
	)

	(:action move-right-from-r1904
		:precondition
			(and 
				(in r1904)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1904))
				(in r1905)
			)
	)


	(:action move-left-from-r1904
		:precondition
			(and 
				(in r1904)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1904))
				(in r1903)
			)
	)

	(:action move-right-from-r1905
		:precondition
			(and 
				(in r1905)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1905))
				(in r1906)
			)
	)


	(:action move-left-from-r1905
		:precondition
			(and 
				(in r1905)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1905))
				(in r1904)
			)
	)

	(:action move-right-from-r1906
		:precondition
			(and 
				(in r1906)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1906))
				(in r1907)
			)
	)


	(:action move-left-from-r1906
		:precondition
			(and 
				(in r1906)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1906))
				(in r1905)
			)
	)

	(:action move-right-from-r1907
		:precondition
			(and 
				(in r1907)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1907))
				(in r1908)
			)
	)


	(:action move-left-from-r1907
		:precondition
			(and 
				(in r1907)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1907))
				(in r1906)
			)
	)

	(:action move-right-from-r1908
		:precondition
			(and 
				(in r1908)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1908))
				(in r1909)
			)
	)


	(:action move-left-from-r1908
		:precondition
			(and 
				(in r1908)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1908))
				(in r1907)
			)
	)

	(:action move-right-from-r1909
		:precondition
			(and 
				(in r1909)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1909))
				(in r1910)
			)
	)


	(:action move-left-from-r1909
		:precondition
			(and 
				(in r1909)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1909))
				(in r1908)
			)
	)

	(:action move-right-from-r1910
		:precondition
			(and 
				(in r1910)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1910))
				(in r1911)
			)
	)


	(:action move-left-from-r1910
		:precondition
			(and 
				(in r1910)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1910))
				(in r1909)
			)
	)

	(:action move-right-from-r1911
		:precondition
			(and 
				(in r1911)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1911))
				(in r1912)
			)
	)


	(:action move-left-from-r1911
		:precondition
			(and 
				(in r1911)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1911))
				(in r1910)
			)
	)

	(:action move-right-from-r1912
		:precondition
			(and 
				(in r1912)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1912))
				(in r1913)
			)
	)


	(:action move-left-from-r1912
		:precondition
			(and 
				(in r1912)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1912))
				(in r1911)
			)
	)

	(:action move-right-from-r1913
		:precondition
			(and 
				(in r1913)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1913))
				(in r1914)
			)
	)


	(:action move-left-from-r1913
		:precondition
			(and 
				(in r1913)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1913))
				(in r1912)
			)
	)

	(:action move-right-from-r1914
		:precondition
			(and 
				(in r1914)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1914))
				(in r1915)
			)
	)


	(:action move-left-from-r1914
		:precondition
			(and 
				(in r1914)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1914))
				(in r1913)
			)
	)

	(:action move-right-from-r1915
		:precondition
			(and 
				(in r1915)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1915))
				(in r1916)
			)
	)


	(:action move-left-from-r1915
		:precondition
			(and 
				(in r1915)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1915))
				(in r1914)
			)
	)

	(:action move-right-from-r1916
		:precondition
			(and 
				(in r1916)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1916))
				(in r1917)
			)
	)


	(:action move-left-from-r1916
		:precondition
			(and 
				(in r1916)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1916))
				(in r1915)
			)
	)

	(:action move-right-from-r1917
		:precondition
			(and 
				(in r1917)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1917))
				(in r1918)
			)
	)


	(:action move-left-from-r1917
		:precondition
			(and 
				(in r1917)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1917))
				(in r1916)
			)
	)

	(:action move-right-from-r1918
		:precondition
			(and 
				(in r1918)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1918))
				(in r1919)
			)
	)


	(:action move-left-from-r1918
		:precondition
			(and 
				(in r1918)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1918))
				(in r1917)
			)
	)

	(:action move-right-from-r1919
		:precondition
			(and 
				(in r1919)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1919))
				(in r1920)
			)
	)


	(:action move-left-from-r1919
		:precondition
			(and 
				(in r1919)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1919))
				(in r1918)
			)
	)

	(:action move-right-from-r1920
		:precondition
			(and 
				(in r1920)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1920))
				(in r1921)
			)
	)


	(:action move-left-from-r1920
		:precondition
			(and 
				(in r1920)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1920))
				(in r1919)
			)
	)

	(:action move-right-from-r1921
		:precondition
			(and 
				(in r1921)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1921))
				(in r1922)
			)
	)


	(:action move-left-from-r1921
		:precondition
			(and 
				(in r1921)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1921))
				(in r1920)
			)
	)

	(:action move-right-from-r1922
		:precondition
			(and 
				(in r1922)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1922))
				(in r1923)
			)
	)


	(:action move-left-from-r1922
		:precondition
			(and 
				(in r1922)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1922))
				(in r1921)
			)
	)

	(:action move-right-from-r1923
		:precondition
			(and 
				(in r1923)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1923))
				(in r1924)
			)
	)


	(:action move-left-from-r1923
		:precondition
			(and 
				(in r1923)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1923))
				(in r1922)
			)
	)

	(:action move-right-from-r1924
		:precondition
			(and 
				(in r1924)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1924))
				(in r1925)
			)
	)


	(:action move-left-from-r1924
		:precondition
			(and 
				(in r1924)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1924))
				(in r1923)
			)
	)

	(:action move-right-from-r1925
		:precondition
			(and 
				(in r1925)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1925))
				(in r1926)
			)
	)


	(:action move-left-from-r1925
		:precondition
			(and 
				(in r1925)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1925))
				(in r1924)
			)
	)

	(:action move-right-from-r1926
		:precondition
			(and 
				(in r1926)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1926))
				(in r1927)
			)
	)


	(:action move-left-from-r1926
		:precondition
			(and 
				(in r1926)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1926))
				(in r1925)
			)
	)

	(:action move-right-from-r1927
		:precondition
			(and 
				(in r1927)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1927))
				(in r1928)
			)
	)


	(:action move-left-from-r1927
		:precondition
			(and 
				(in r1927)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1927))
				(in r1926)
			)
	)

	(:action move-right-from-r1928
		:precondition
			(and 
				(in r1928)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1928))
				(in r1929)
			)
	)


	(:action move-left-from-r1928
		:precondition
			(and 
				(in r1928)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1928))
				(in r1927)
			)
	)

	(:action move-right-from-r1929
		:precondition
			(and 
				(in r1929)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1929))
				(in r1930)
			)
	)


	(:action move-left-from-r1929
		:precondition
			(and 
				(in r1929)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1929))
				(in r1928)
			)
	)

	(:action move-right-from-r1930
		:precondition
			(and 
				(in r1930)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1930))
				(in r1931)
			)
	)


	(:action move-left-from-r1930
		:precondition
			(and 
				(in r1930)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1930))
				(in r1929)
			)
	)

	(:action move-right-from-r1931
		:precondition
			(and 
				(in r1931)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1931))
				(in r1932)
			)
	)


	(:action move-left-from-r1931
		:precondition
			(and 
				(in r1931)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1931))
				(in r1930)
			)
	)

	(:action move-right-from-r1932
		:precondition
			(and 
				(in r1932)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1932))
				(in r1933)
			)
	)


	(:action move-left-from-r1932
		:precondition
			(and 
				(in r1932)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1932))
				(in r1931)
			)
	)

	(:action move-right-from-r1933
		:precondition
			(and 
				(in r1933)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1933))
				(in r1934)
			)
	)


	(:action move-left-from-r1933
		:precondition
			(and 
				(in r1933)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1933))
				(in r1932)
			)
	)

	(:action move-right-from-r1934
		:precondition
			(and 
				(in r1934)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1934))
				(in r1935)
			)
	)


	(:action move-left-from-r1934
		:precondition
			(and 
				(in r1934)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1934))
				(in r1933)
			)
	)

	(:action move-right-from-r1935
		:precondition
			(and 
				(in r1935)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1935))
				(in r1936)
			)
	)


	(:action move-left-from-r1935
		:precondition
			(and 
				(in r1935)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1935))
				(in r1934)
			)
	)

	(:action move-right-from-r1936
		:precondition
			(and 
				(in r1936)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1936))
				(in r1937)
			)
	)


	(:action move-left-from-r1936
		:precondition
			(and 
				(in r1936)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1936))
				(in r1935)
			)
	)

	(:action move-right-from-r1937
		:precondition
			(and 
				(in r1937)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1937))
				(in r1938)
			)
	)


	(:action move-left-from-r1937
		:precondition
			(and 
				(in r1937)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1937))
				(in r1936)
			)
	)

	(:action move-right-from-r1938
		:precondition
			(and 
				(in r1938)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1938))
				(in r1939)
			)
	)


	(:action move-left-from-r1938
		:precondition
			(and 
				(in r1938)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1938))
				(in r1937)
			)
	)

	(:action move-right-from-r1939
		:precondition
			(and 
				(in r1939)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1939))
				(in r1940)
			)
	)


	(:action move-left-from-r1939
		:precondition
			(and 
				(in r1939)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1939))
				(in r1938)
			)
	)

	(:action move-right-from-r1940
		:precondition
			(and 
				(in r1940)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1940))
				(in r1941)
			)
	)


	(:action move-left-from-r1940
		:precondition
			(and 
				(in r1940)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1940))
				(in r1939)
			)
	)

	(:action move-right-from-r1941
		:precondition
			(and 
				(in r1941)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1941))
				(in r1942)
			)
	)


	(:action move-left-from-r1941
		:precondition
			(and 
				(in r1941)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1941))
				(in r1940)
			)
	)

	(:action move-right-from-r1942
		:precondition
			(and 
				(in r1942)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1942))
				(in r1943)
			)
	)


	(:action move-left-from-r1942
		:precondition
			(and 
				(in r1942)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1942))
				(in r1941)
			)
	)

	(:action move-right-from-r1943
		:precondition
			(and 
				(in r1943)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1943))
				(in r1944)
			)
	)


	(:action move-left-from-r1943
		:precondition
			(and 
				(in r1943)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1943))
				(in r1942)
			)
	)

	(:action move-right-from-r1944
		:precondition
			(and 
				(in r1944)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1944))
				(in r1945)
			)
	)


	(:action move-left-from-r1944
		:precondition
			(and 
				(in r1944)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1944))
				(in r1943)
			)
	)

	(:action move-right-from-r1945
		:precondition
			(and 
				(in r1945)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1945))
				(in r1946)
			)
	)


	(:action move-left-from-r1945
		:precondition
			(and 
				(in r1945)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1945))
				(in r1944)
			)
	)

	(:action move-right-from-r1946
		:precondition
			(and 
				(in r1946)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1946))
				(in r1947)
			)
	)


	(:action move-left-from-r1946
		:precondition
			(and 
				(in r1946)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1946))
				(in r1945)
			)
	)

	(:action move-right-from-r1947
		:precondition
			(and 
				(in r1947)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1947))
				(in r1948)
			)
	)


	(:action move-left-from-r1947
		:precondition
			(and 
				(in r1947)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1947))
				(in r1946)
			)
	)

	(:action move-right-from-r1948
		:precondition
			(and 
				(in r1948)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1948))
				(in r1949)
			)
	)


	(:action move-left-from-r1948
		:precondition
			(and 
				(in r1948)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1948))
				(in r1947)
			)
	)

	(:action move-right-from-r1949
		:precondition
			(and 
				(in r1949)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1949))
				(in r1950)
			)
	)


	(:action move-left-from-r1949
		:precondition
			(and 
				(in r1949)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1949))
				(in r1948)
			)
	)

	(:action move-right-from-r1950
		:precondition
			(and 
				(in r1950)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1950))
				(in r1951)
			)
	)


	(:action move-left-from-r1950
		:precondition
			(and 
				(in r1950)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1950))
				(in r1949)
			)
	)

	(:action move-right-from-r1951
		:precondition
			(and 
				(in r1951)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1951))
				(in r1952)
			)
	)


	(:action move-left-from-r1951
		:precondition
			(and 
				(in r1951)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1951))
				(in r1950)
			)
	)

	(:action move-right-from-r1952
		:precondition
			(and 
				(in r1952)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1952))
				(in r1953)
			)
	)


	(:action move-left-from-r1952
		:precondition
			(and 
				(in r1952)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1952))
				(in r1951)
			)
	)

	(:action move-right-from-r1953
		:precondition
			(and 
				(in r1953)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1953))
				(in r1954)
			)
	)


	(:action move-left-from-r1953
		:precondition
			(and 
				(in r1953)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1953))
				(in r1952)
			)
	)

	(:action move-right-from-r1954
		:precondition
			(and 
				(in r1954)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1954))
				(in r1955)
			)
	)


	(:action move-left-from-r1954
		:precondition
			(and 
				(in r1954)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1954))
				(in r1953)
			)
	)

	(:action move-right-from-r1955
		:precondition
			(and 
				(in r1955)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1955))
				(in r1956)
			)
	)


	(:action move-left-from-r1955
		:precondition
			(and 
				(in r1955)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1955))
				(in r1954)
			)
	)

	(:action move-right-from-r1956
		:precondition
			(and 
				(in r1956)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1956))
				(in r1957)
			)
	)


	(:action move-left-from-r1956
		:precondition
			(and 
				(in r1956)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1956))
				(in r1955)
			)
	)

	(:action move-right-from-r1957
		:precondition
			(and 
				(in r1957)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1957))
				(in r1958)
			)
	)


	(:action move-left-from-r1957
		:precondition
			(and 
				(in r1957)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1957))
				(in r1956)
			)
	)

	(:action move-right-from-r1958
		:precondition
			(and 
				(in r1958)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1958))
				(in r1959)
			)
	)


	(:action move-left-from-r1958
		:precondition
			(and 
				(in r1958)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1958))
				(in r1957)
			)
	)

	(:action move-right-from-r1959
		:precondition
			(and 
				(in r1959)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1959))
				(in r1960)
			)
	)


	(:action move-left-from-r1959
		:precondition
			(and 
				(in r1959)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1959))
				(in r1958)
			)
	)

	(:action move-right-from-r1960
		:precondition
			(and 
				(in r1960)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1960))
				(in r1961)
			)
	)


	(:action move-left-from-r1960
		:precondition
			(and 
				(in r1960)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1960))
				(in r1959)
			)
	)

	(:action move-right-from-r1961
		:precondition
			(and 
				(in r1961)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1961))
				(in r1962)
			)
	)


	(:action move-left-from-r1961
		:precondition
			(and 
				(in r1961)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1961))
				(in r1960)
			)
	)

	(:action move-right-from-r1962
		:precondition
			(and 
				(in r1962)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1962))
				(in r1963)
			)
	)


	(:action move-left-from-r1962
		:precondition
			(and 
				(in r1962)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1962))
				(in r1961)
			)
	)

	(:action move-right-from-r1963
		:precondition
			(and 
				(in r1963)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1963))
				(in r1964)
			)
	)


	(:action move-left-from-r1963
		:precondition
			(and 
				(in r1963)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1963))
				(in r1962)
			)
	)

	(:action move-right-from-r1964
		:precondition
			(and 
				(in r1964)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1964))
				(in r1965)
			)
	)


	(:action move-left-from-r1964
		:precondition
			(and 
				(in r1964)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1964))
				(in r1963)
			)
	)

	(:action move-right-from-r1965
		:precondition
			(and 
				(in r1965)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1965))
				(in r1966)
			)
	)


	(:action move-left-from-r1965
		:precondition
			(and 
				(in r1965)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1965))
				(in r1964)
			)
	)

	(:action move-right-from-r1966
		:precondition
			(and 
				(in r1966)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1966))
				(in r1967)
			)
	)


	(:action move-left-from-r1966
		:precondition
			(and 
				(in r1966)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1966))
				(in r1965)
			)
	)

	(:action move-right-from-r1967
		:precondition
			(and 
				(in r1967)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1967))
				(in r1968)
			)
	)


	(:action move-left-from-r1967
		:precondition
			(and 
				(in r1967)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1967))
				(in r1966)
			)
	)

	(:action move-right-from-r1968
		:precondition
			(and 
				(in r1968)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1968))
				(in r1969)
			)
	)


	(:action move-left-from-r1968
		:precondition
			(and 
				(in r1968)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1968))
				(in r1967)
			)
	)

	(:action move-right-from-r1969
		:precondition
			(and 
				(in r1969)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1969))
				(in r1970)
			)
	)


	(:action move-left-from-r1969
		:precondition
			(and 
				(in r1969)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1969))
				(in r1968)
			)
	)

	(:action move-right-from-r1970
		:precondition
			(and 
				(in r1970)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1970))
				(in r1971)
			)
	)


	(:action move-left-from-r1970
		:precondition
			(and 
				(in r1970)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1970))
				(in r1969)
			)
	)

	(:action move-right-from-r1971
		:precondition
			(and 
				(in r1971)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1971))
				(in r1972)
			)
	)


	(:action move-left-from-r1971
		:precondition
			(and 
				(in r1971)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1971))
				(in r1970)
			)
	)

	(:action move-right-from-r1972
		:precondition
			(and 
				(in r1972)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1972))
				(in r1973)
			)
	)


	(:action move-left-from-r1972
		:precondition
			(and 
				(in r1972)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1972))
				(in r1971)
			)
	)

	(:action move-right-from-r1973
		:precondition
			(and 
				(in r1973)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1973))
				(in r1974)
			)
	)


	(:action move-left-from-r1973
		:precondition
			(and 
				(in r1973)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1973))
				(in r1972)
			)
	)

	(:action move-right-from-r1974
		:precondition
			(and 
				(in r1974)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1974))
				(in r1975)
			)
	)


	(:action move-left-from-r1974
		:precondition
			(and 
				(in r1974)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1974))
				(in r1973)
			)
	)

	(:action move-right-from-r1975
		:precondition
			(and 
				(in r1975)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1975))
				(in r1976)
			)
	)


	(:action move-left-from-r1975
		:precondition
			(and 
				(in r1975)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1975))
				(in r1974)
			)
	)

	(:action move-right-from-r1976
		:precondition
			(and 
				(in r1976)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1976))
				(in r1977)
			)
	)


	(:action move-left-from-r1976
		:precondition
			(and 
				(in r1976)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1976))
				(in r1975)
			)
	)

	(:action move-right-from-r1977
		:precondition
			(and 
				(in r1977)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1977))
				(in r1978)
			)
	)


	(:action move-left-from-r1977
		:precondition
			(and 
				(in r1977)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1977))
				(in r1976)
			)
	)

	(:action move-right-from-r1978
		:precondition
			(and 
				(in r1978)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1978))
				(in r1979)
			)
	)


	(:action move-left-from-r1978
		:precondition
			(and 
				(in r1978)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1978))
				(in r1977)
			)
	)

	(:action move-right-from-r1979
		:precondition
			(and 
				(in r1979)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1979))
				(in r1980)
			)
	)


	(:action move-left-from-r1979
		:precondition
			(and 
				(in r1979)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1979))
				(in r1978)
			)
	)

	(:action move-right-from-r1980
		:precondition
			(and 
				(in r1980)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1980))
				(in r1981)
			)
	)


	(:action move-left-from-r1980
		:precondition
			(and 
				(in r1980)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1980))
				(in r1979)
			)
	)

	(:action move-right-from-r1981
		:precondition
			(and 
				(in r1981)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1981))
				(in r1982)
			)
	)


	(:action move-left-from-r1981
		:precondition
			(and 
				(in r1981)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1981))
				(in r1980)
			)
	)

	(:action move-right-from-r1982
		:precondition
			(and 
				(in r1982)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1982))
				(in r1983)
			)
	)


	(:action move-left-from-r1982
		:precondition
			(and 
				(in r1982)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1982))
				(in r1981)
			)
	)

	(:action move-right-from-r1983
		:precondition
			(and 
				(in r1983)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1983))
				(in r1984)
			)
	)


	(:action move-left-from-r1983
		:precondition
			(and 
				(in r1983)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1983))
				(in r1982)
			)
	)

	(:action move-right-from-r1984
		:precondition
			(and 
				(in r1984)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1984))
				(in r1985)
			)
	)


	(:action move-left-from-r1984
		:precondition
			(and 
				(in r1984)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1984))
				(in r1983)
			)
	)

	(:action move-right-from-r1985
		:precondition
			(and 
				(in r1985)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1985))
				(in r1986)
			)
	)


	(:action move-left-from-r1985
		:precondition
			(and 
				(in r1985)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1985))
				(in r1984)
			)
	)

	(:action move-right-from-r1986
		:precondition
			(and 
				(in r1986)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1986))
				(in r1987)
			)
	)


	(:action move-left-from-r1986
		:precondition
			(and 
				(in r1986)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1986))
				(in r1985)
			)
	)

	(:action move-right-from-r1987
		:precondition
			(and 
				(in r1987)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1987))
				(in r1988)
			)
	)


	(:action move-left-from-r1987
		:precondition
			(and 
				(in r1987)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1987))
				(in r1986)
			)
	)

	(:action move-right-from-r1988
		:precondition
			(and 
				(in r1988)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1988))
				(in r1989)
			)
	)


	(:action move-left-from-r1988
		:precondition
			(and 
				(in r1988)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1988))
				(in r1987)
			)
	)

	(:action move-right-from-r1989
		:precondition
			(and 
				(in r1989)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1989))
				(in r1990)
			)
	)


	(:action move-left-from-r1989
		:precondition
			(and 
				(in r1989)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1989))
				(in r1988)
			)
	)

	(:action move-right-from-r1990
		:precondition
			(and 
				(in r1990)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1990))
				(in r1991)
			)
	)


	(:action move-left-from-r1990
		:precondition
			(and 
				(in r1990)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1990))
				(in r1989)
			)
	)

	(:action move-right-from-r1991
		:precondition
			(and 
				(in r1991)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1991))
				(in r1992)
			)
	)


	(:action move-left-from-r1991
		:precondition
			(and 
				(in r1991)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1991))
				(in r1990)
			)
	)

	(:action move-right-from-r1992
		:precondition
			(and 
				(in r1992)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1992))
				(in r1993)
			)
	)


	(:action move-left-from-r1992
		:precondition
			(and 
				(in r1992)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1992))
				(in r1991)
			)
	)

	(:action move-right-from-r1993
		:precondition
			(and 
				(in r1993)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1993))
				(in r1994)
			)
	)


	(:action move-left-from-r1993
		:precondition
			(and 
				(in r1993)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1993))
				(in r1992)
			)
	)

	(:action move-right-from-r1994
		:precondition
			(and 
				(in r1994)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1994))
				(in r1995)
			)
	)


	(:action move-left-from-r1994
		:precondition
			(and 
				(in r1994)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1994))
				(in r1993)
			)
	)

	(:action move-right-from-r1995
		:precondition
			(and 
				(in r1995)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1995))
				(in r1996)
			)
	)


	(:action move-left-from-r1995
		:precondition
			(and 
				(in r1995)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1995))
				(in r1994)
			)
	)

	(:action move-right-from-r1996
		:precondition
			(and 
				(in r1996)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1996))
				(in r1997)
			)
	)


	(:action move-left-from-r1996
		:precondition
			(and 
				(in r1996)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1996))
				(in r1995)
			)
	)

	(:action move-right-from-r1997
		:precondition
			(and 
				(in r1997)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1997))
				(in r1998)
			)
	)


	(:action move-left-from-r1997
		:precondition
			(and 
				(in r1997)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1997))
				(in r1996)
			)
	)

	(:action move-right-from-r1998
		:precondition
			(and 
				(in r1998)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1998))
				(in r1999)
			)
	)


	(:action move-left-from-r1998
		:precondition
			(and 
				(in r1998)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1998))
				(in r1997)
			)
	)

	(:action move-right-from-r1999
		:precondition
			(and 
				(in r1999)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1999))
				(in r2000)
			)
	)


	(:action move-left-from-r1999
		:precondition
			(and 
				(in r1999)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1999))
				(in r1998)
			)
	)

	(:action move-right-from-r2000
		:precondition
			(and 
				(in r2000)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2000))
				(in r2001)
			)
	)


	(:action move-left-from-r2000
		:precondition
			(and 
				(in r2000)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2000))
				(in r1999)
			)
	)

	(:action move-right-from-r2001
		:precondition
			(and 
				(in r2001)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2001))
				(in r2002)
			)
	)


	(:action move-left-from-r2001
		:precondition
			(and 
				(in r2001)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2001))
				(in r2000)
			)
	)

	(:action move-right-from-r2002
		:precondition
			(and 
				(in r2002)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2002))
				(in r2003)
			)
	)


	(:action move-left-from-r2002
		:precondition
			(and 
				(in r2002)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2002))
				(in r2001)
			)
	)

	(:action move-right-from-r2003
		:precondition
			(and 
				(in r2003)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2003))
				(in r2004)
			)
	)


	(:action move-left-from-r2003
		:precondition
			(and 
				(in r2003)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2003))
				(in r2002)
			)
	)

	(:action move-right-from-r2004
		:precondition
			(and 
				(in r2004)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2004))
				(in r2005)
			)
	)


	(:action move-left-from-r2004
		:precondition
			(and 
				(in r2004)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2004))
				(in r2003)
			)
	)

	(:action move-right-from-r2005
		:precondition
			(and 
				(in r2005)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2005))
				(in r2006)
			)
	)


	(:action move-left-from-r2005
		:precondition
			(and 
				(in r2005)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2005))
				(in r2004)
			)
	)

	(:action move-right-from-r2006
		:precondition
			(and 
				(in r2006)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2006))
				(in r2007)
			)
	)


	(:action move-left-from-r2006
		:precondition
			(and 
				(in r2006)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2006))
				(in r2005)
			)
	)

	(:action move-right-from-r2007
		:precondition
			(and 
				(in r2007)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2007))
				(in r2008)
			)
	)


	(:action move-left-from-r2007
		:precondition
			(and 
				(in r2007)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2007))
				(in r2006)
			)
	)

	(:action move-right-from-r2008
		:precondition
			(and 
				(in r2008)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2008))
				(in r2009)
			)
	)


	(:action move-left-from-r2008
		:precondition
			(and 
				(in r2008)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2008))
				(in r2007)
			)
	)

	(:action move-right-from-r2009
		:precondition
			(and 
				(in r2009)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2009))
				(in r2010)
			)
	)


	(:action move-left-from-r2009
		:precondition
			(and 
				(in r2009)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2009))
				(in r2008)
			)
	)

	(:action move-right-from-r2010
		:precondition
			(and 
				(in r2010)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2010))
				(in r2011)
			)
	)


	(:action move-left-from-r2010
		:precondition
			(and 
				(in r2010)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2010))
				(in r2009)
			)
	)

	(:action move-right-from-r2011
		:precondition
			(and 
				(in r2011)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2011))
				(in r2012)
			)
	)


	(:action move-left-from-r2011
		:precondition
			(and 
				(in r2011)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2011))
				(in r2010)
			)
	)

	(:action move-right-from-r2012
		:precondition
			(and 
				(in r2012)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2012))
				(in r2013)
			)
	)


	(:action move-left-from-r2012
		:precondition
			(and 
				(in r2012)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2012))
				(in r2011)
			)
	)

	(:action move-right-from-r2013
		:precondition
			(and 
				(in r2013)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2013))
				(in r2014)
			)
	)


	(:action move-left-from-r2013
		:precondition
			(and 
				(in r2013)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2013))
				(in r2012)
			)
	)

	(:action move-right-from-r2014
		:precondition
			(and 
				(in r2014)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2014))
				(in r2015)
			)
	)


	(:action move-left-from-r2014
		:precondition
			(and 
				(in r2014)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2014))
				(in r2013)
			)
	)

	(:action move-right-from-r2015
		:precondition
			(and 
				(in r2015)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2015))
				(in r2016)
			)
	)


	(:action move-left-from-r2015
		:precondition
			(and 
				(in r2015)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2015))
				(in r2014)
			)
	)

	(:action move-right-from-r2016
		:precondition
			(and 
				(in r2016)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2016))
				(in r2017)
			)
	)


	(:action move-left-from-r2016
		:precondition
			(and 
				(in r2016)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2016))
				(in r2015)
			)
	)

	(:action move-right-from-r2017
		:precondition
			(and 
				(in r2017)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2017))
				(in r2018)
			)
	)


	(:action move-left-from-r2017
		:precondition
			(and 
				(in r2017)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2017))
				(in r2016)
			)
	)

	(:action move-right-from-r2018
		:precondition
			(and 
				(in r2018)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2018))
				(in r2019)
			)
	)


	(:action move-left-from-r2018
		:precondition
			(and 
				(in r2018)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2018))
				(in r2017)
			)
	)

	(:action move-right-from-r2019
		:precondition
			(and 
				(in r2019)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2019))
				(in r2020)
			)
	)


	(:action move-left-from-r2019
		:precondition
			(and 
				(in r2019)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2019))
				(in r2018)
			)
	)

	(:action move-right-from-r2020
		:precondition
			(and 
				(in r2020)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2020))
				(in r2021)
			)
	)


	(:action move-left-from-r2020
		:precondition
			(and 
				(in r2020)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2020))
				(in r2019)
			)
	)

	(:action move-right-from-r2021
		:precondition
			(and 
				(in r2021)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2021))
				(in r2022)
			)
	)


	(:action move-left-from-r2021
		:precondition
			(and 
				(in r2021)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2021))
				(in r2020)
			)
	)

	(:action move-right-from-r2022
		:precondition
			(and 
				(in r2022)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2022))
				(in r2023)
			)
	)


	(:action move-left-from-r2022
		:precondition
			(and 
				(in r2022)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2022))
				(in r2021)
			)
	)

	(:action move-right-from-r2023
		:precondition
			(and 
				(in r2023)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2023))
				(in r2024)
			)
	)


	(:action move-left-from-r2023
		:precondition
			(and 
				(in r2023)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2023))
				(in r2022)
			)
	)

	(:action move-right-from-r2024
		:precondition
			(and 
				(in r2024)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2024))
				(in r2025)
			)
	)


	(:action move-left-from-r2024
		:precondition
			(and 
				(in r2024)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2024))
				(in r2023)
			)
	)

	(:action move-right-from-r2025
		:precondition
			(and 
				(in r2025)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2025))
				(in r2026)
			)
	)


	(:action move-left-from-r2025
		:precondition
			(and 
				(in r2025)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2025))
				(in r2024)
			)
	)

	(:action move-right-from-r2026
		:precondition
			(and 
				(in r2026)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2026))
				(in r2027)
			)
	)


	(:action move-left-from-r2026
		:precondition
			(and 
				(in r2026)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2026))
				(in r2025)
			)
	)

	(:action move-right-from-r2027
		:precondition
			(and 
				(in r2027)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2027))
				(in r2028)
			)
	)


	(:action move-left-from-r2027
		:precondition
			(and 
				(in r2027)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2027))
				(in r2026)
			)
	)

	(:action move-right-from-r2028
		:precondition
			(and 
				(in r2028)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2028))
				(in r2029)
			)
	)


	(:action move-left-from-r2028
		:precondition
			(and 
				(in r2028)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2028))
				(in r2027)
			)
	)

	(:action move-right-from-r2029
		:precondition
			(and 
				(in r2029)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2029))
				(in r2030)
			)
	)


	(:action move-left-from-r2029
		:precondition
			(and 
				(in r2029)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2029))
				(in r2028)
			)
	)

	(:action move-right-from-r2030
		:precondition
			(and 
				(in r2030)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2030))
				(in r2031)
			)
	)


	(:action move-left-from-r2030
		:precondition
			(and 
				(in r2030)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2030))
				(in r2029)
			)
	)

	(:action move-right-from-r2031
		:precondition
			(and 
				(in r2031)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2031))
				(in r2032)
			)
	)


	(:action move-left-from-r2031
		:precondition
			(and 
				(in r2031)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2031))
				(in r2030)
			)
	)

	(:action move-right-from-r2032
		:precondition
			(and 
				(in r2032)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2032))
				(in r2033)
			)
	)


	(:action move-left-from-r2032
		:precondition
			(and 
				(in r2032)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2032))
				(in r2031)
			)
	)

	(:action move-right-from-r2033
		:precondition
			(and 
				(in r2033)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2033))
				(in r2034)
			)
	)


	(:action move-left-from-r2033
		:precondition
			(and 
				(in r2033)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2033))
				(in r2032)
			)
	)

	(:action move-right-from-r2034
		:precondition
			(and 
				(in r2034)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2034))
				(in r2035)
			)
	)


	(:action move-left-from-r2034
		:precondition
			(and 
				(in r2034)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2034))
				(in r2033)
			)
	)

	(:action move-right-from-r2035
		:precondition
			(and 
				(in r2035)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2035))
				(in r2036)
			)
	)


	(:action move-left-from-r2035
		:precondition
			(and 
				(in r2035)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2035))
				(in r2034)
			)
	)

	(:action move-right-from-r2036
		:precondition
			(and 
				(in r2036)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2036))
				(in r2037)
			)
	)


	(:action move-left-from-r2036
		:precondition
			(and 
				(in r2036)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2036))
				(in r2035)
			)
	)

	(:action move-right-from-r2037
		:precondition
			(and 
				(in r2037)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2037))
				(in r2038)
			)
	)


	(:action move-left-from-r2037
		:precondition
			(and 
				(in r2037)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2037))
				(in r2036)
			)
	)

	(:action move-right-from-r2038
		:precondition
			(and 
				(in r2038)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2038))
				(in r2039)
			)
	)


	(:action move-left-from-r2038
		:precondition
			(and 
				(in r2038)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2038))
				(in r2037)
			)
	)

	(:action move-right-from-r2039
		:precondition
			(and 
				(in r2039)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2039))
				(in r2040)
			)
	)


	(:action move-left-from-r2039
		:precondition
			(and 
				(in r2039)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2039))
				(in r2038)
			)
	)

	(:action move-right-from-r2040
		:precondition
			(and 
				(in r2040)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2040))
				(in r2041)
			)
	)


	(:action move-left-from-r2040
		:precondition
			(and 
				(in r2040)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2040))
				(in r2039)
			)
	)

	(:action move-right-from-r2041
		:precondition
			(and 
				(in r2041)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2041))
				(in r2042)
			)
	)


	(:action move-left-from-r2041
		:precondition
			(and 
				(in r2041)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2041))
				(in r2040)
			)
	)

	(:action move-right-from-r2042
		:precondition
			(and 
				(in r2042)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2042))
				(in r2043)
			)
	)


	(:action move-left-from-r2042
		:precondition
			(and 
				(in r2042)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2042))
				(in r2041)
			)
	)

	(:action move-right-from-r2043
		:precondition
			(and 
				(in r2043)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2043))
				(in r2044)
			)
	)


	(:action move-left-from-r2043
		:precondition
			(and 
				(in r2043)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2043))
				(in r2042)
			)
	)

	(:action move-right-from-r2044
		:precondition
			(and 
				(in r2044)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2044))
				(in r2045)
			)
	)


	(:action move-left-from-r2044
		:precondition
			(and 
				(in r2044)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2044))
				(in r2043)
			)
	)

	(:action move-right-from-r2045
		:precondition
			(and 
				(in r2045)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2045))
				(in r2046)
			)
	)


	(:action move-left-from-r2045
		:precondition
			(and 
				(in r2045)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2045))
				(in r2044)
			)
	)

	(:action move-right-from-r2046
		:precondition
			(and 
				(in r2046)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2046))
				(in r2047)
			)
	)


	(:action move-left-from-r2046
		:precondition
			(and 
				(in r2046)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2046))
				(in r2045)
			)
	)

	(:action move-right-from-r2047
		:precondition
			(and 
				(in r2047)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2047))
				(in r2048)
			)
	)


	(:action move-left-from-r2047
		:precondition
			(and 
				(in r2047)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2047))
				(in r2046)
			)
	)

	(:action move-right-from-r2048
		:precondition
			(and 
				(in r2048)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2048))
				(in r2049)
			)
	)


	(:action move-left-from-r2048
		:precondition
			(and 
				(in r2048)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2048))
				(in r2047)
			)
	)

	(:action move-right-from-r2049
		:precondition
			(and 
				(in r2049)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2049))
				(in r2050)
			)
	)


	(:action move-left-from-r2049
		:precondition
			(and 
				(in r2049)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2049))
				(in r2048)
			)
	)

	(:action move-right-from-r2050
		:precondition
			(and 
				(in r2050)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2050))
				(in r2051)
			)
	)


	(:action move-left-from-r2050
		:precondition
			(and 
				(in r2050)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2050))
				(in r2049)
			)
	)

	(:action move-right-from-r2051
		:precondition
			(and 
				(in r2051)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2051))
				(in r2052)
			)
	)


	(:action move-left-from-r2051
		:precondition
			(and 
				(in r2051)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2051))
				(in r2050)
			)
	)

	(:action move-right-from-r2052
		:precondition
			(and 
				(in r2052)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2052))
				(in r2053)
			)
	)


	(:action move-left-from-r2052
		:precondition
			(and 
				(in r2052)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2052))
				(in r2051)
			)
	)

	(:action move-right-from-r2053
		:precondition
			(and 
				(in r2053)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2053))
				(in r2054)
			)
	)


	(:action move-left-from-r2053
		:precondition
			(and 
				(in r2053)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2053))
				(in r2052)
			)
	)

	(:action move-right-from-r2054
		:precondition
			(and 
				(in r2054)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2054))
				(in r2055)
			)
	)


	(:action move-left-from-r2054
		:precondition
			(and 
				(in r2054)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2054))
				(in r2053)
			)
	)

	(:action move-right-from-r2055
		:precondition
			(and 
				(in r2055)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2055))
				(in r2056)
			)
	)


	(:action move-left-from-r2055
		:precondition
			(and 
				(in r2055)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2055))
				(in r2054)
			)
	)

	(:action move-right-from-r2056
		:precondition
			(and 
				(in r2056)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2056))
				(in r2057)
			)
	)


	(:action move-left-from-r2056
		:precondition
			(and 
				(in r2056)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2056))
				(in r2055)
			)
	)

	(:action move-right-from-r2057
		:precondition
			(and 
				(in r2057)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2057))
				(in r2058)
			)
	)


	(:action move-left-from-r2057
		:precondition
			(and 
				(in r2057)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2057))
				(in r2056)
			)
	)

	(:action move-right-from-r2058
		:precondition
			(and 
				(in r2058)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2058))
				(in r2059)
			)
	)


	(:action move-left-from-r2058
		:precondition
			(and 
				(in r2058)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2058))
				(in r2057)
			)
	)

	(:action move-right-from-r2059
		:precondition
			(and 
				(in r2059)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2059))
				(in r2060)
			)
	)


	(:action move-left-from-r2059
		:precondition
			(and 
				(in r2059)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2059))
				(in r2058)
			)
	)

	(:action move-right-from-r2060
		:precondition
			(and 
				(in r2060)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2060))
				(in r2061)
			)
	)


	(:action move-left-from-r2060
		:precondition
			(and 
				(in r2060)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2060))
				(in r2059)
			)
	)

	(:action move-right-from-r2061
		:precondition
			(and 
				(in r2061)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2061))
				(in r2062)
			)
	)


	(:action move-left-from-r2061
		:precondition
			(and 
				(in r2061)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2061))
				(in r2060)
			)
	)

	(:action move-right-from-r2062
		:precondition
			(and 
				(in r2062)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2062))
				(in r2063)
			)
	)


	(:action move-left-from-r2062
		:precondition
			(and 
				(in r2062)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2062))
				(in r2061)
			)
	)

	(:action move-right-from-r2063
		:precondition
			(and 
				(in r2063)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2063))
				(in r2064)
			)
	)


	(:action move-left-from-r2063
		:precondition
			(and 
				(in r2063)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2063))
				(in r2062)
			)
	)

	(:action move-right-from-r2064
		:precondition
			(and 
				(in r2064)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2064))
				(in r2065)
			)
	)


	(:action move-left-from-r2064
		:precondition
			(and 
				(in r2064)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2064))
				(in r2063)
			)
	)

	(:action move-right-from-r2065
		:precondition
			(and 
				(in r2065)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2065))
				(in r2066)
			)
	)


	(:action move-left-from-r2065
		:precondition
			(and 
				(in r2065)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2065))
				(in r2064)
			)
	)

	(:action move-right-from-r2066
		:precondition
			(and 
				(in r2066)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2066))
				(in r2067)
			)
	)


	(:action move-left-from-r2066
		:precondition
			(and 
				(in r2066)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2066))
				(in r2065)
			)
	)

	(:action move-right-from-r2067
		:precondition
			(and 
				(in r2067)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2067))
				(in r2068)
			)
	)


	(:action move-left-from-r2067
		:precondition
			(and 
				(in r2067)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2067))
				(in r2066)
			)
	)

	(:action move-right-from-r2068
		:precondition
			(and 
				(in r2068)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2068))
				(in r2069)
			)
	)


	(:action move-left-from-r2068
		:precondition
			(and 
				(in r2068)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2068))
				(in r2067)
			)
	)

	(:action move-right-from-r2069
		:precondition
			(and 
				(in r2069)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2069))
				(in r2070)
			)
	)


	(:action move-left-from-r2069
		:precondition
			(and 
				(in r2069)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2069))
				(in r2068)
			)
	)

	(:action move-right-from-r2070
		:precondition
			(and 
				(in r2070)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2070))
				(in r2071)
			)
	)


	(:action move-left-from-r2070
		:precondition
			(and 
				(in r2070)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2070))
				(in r2069)
			)
	)

	(:action move-right-from-r2071
		:precondition
			(and 
				(in r2071)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2071))
				(in r2072)
			)
	)


	(:action move-left-from-r2071
		:precondition
			(and 
				(in r2071)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2071))
				(in r2070)
			)
	)

	(:action move-right-from-r2072
		:precondition
			(and 
				(in r2072)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2072))
				(in r2073)
			)
	)


	(:action move-left-from-r2072
		:precondition
			(and 
				(in r2072)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2072))
				(in r2071)
			)
	)

	(:action move-right-from-r2073
		:precondition
			(and 
				(in r2073)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2073))
				(in r2074)
			)
	)


	(:action move-left-from-r2073
		:precondition
			(and 
				(in r2073)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2073))
				(in r2072)
			)
	)

	(:action move-right-from-r2074
		:precondition
			(and 
				(in r2074)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2074))
				(in r2075)
			)
	)


	(:action move-left-from-r2074
		:precondition
			(and 
				(in r2074)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2074))
				(in r2073)
			)
	)

	(:action move-right-from-r2075
		:precondition
			(and 
				(in r2075)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2075))
				(in r2076)
			)
	)


	(:action move-left-from-r2075
		:precondition
			(and 
				(in r2075)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2075))
				(in r2074)
			)
	)

	(:action move-right-from-r2076
		:precondition
			(and 
				(in r2076)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2076))
				(in r2077)
			)
	)


	(:action move-left-from-r2076
		:precondition
			(and 
				(in r2076)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2076))
				(in r2075)
			)
	)

	(:action move-right-from-r2077
		:precondition
			(and 
				(in r2077)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2077))
				(in r2078)
			)
	)


	(:action move-left-from-r2077
		:precondition
			(and 
				(in r2077)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2077))
				(in r2076)
			)
	)

	(:action move-right-from-r2078
		:precondition
			(and 
				(in r2078)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2078))
				(in r2079)
			)
	)


	(:action move-left-from-r2078
		:precondition
			(and 
				(in r2078)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2078))
				(in r2077)
			)
	)

	(:action move-right-from-r2079
		:precondition
			(and 
				(in r2079)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2079))
				(in r2080)
			)
	)


	(:action move-left-from-r2079
		:precondition
			(and 
				(in r2079)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2079))
				(in r2078)
			)
	)

	(:action move-right-from-r2080
		:precondition
			(and 
				(in r2080)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2080))
				(in r2081)
			)
	)


	(:action move-left-from-r2080
		:precondition
			(and 
				(in r2080)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2080))
				(in r2079)
			)
	)

	(:action move-right-from-r2081
		:precondition
			(and 
				(in r2081)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2081))
				(in r2082)
			)
	)


	(:action move-left-from-r2081
		:precondition
			(and 
				(in r2081)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2081))
				(in r2080)
			)
	)

	(:action move-right-from-r2082
		:precondition
			(and 
				(in r2082)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2082))
				(in r2083)
			)
	)


	(:action move-left-from-r2082
		:precondition
			(and 
				(in r2082)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2082))
				(in r2081)
			)
	)

	(:action move-right-from-r2083
		:precondition
			(and 
				(in r2083)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2083))
				(in r2084)
			)
	)


	(:action move-left-from-r2083
		:precondition
			(and 
				(in r2083)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2083))
				(in r2082)
			)
	)

	(:action move-right-from-r2084
		:precondition
			(and 
				(in r2084)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2084))
				(in r2085)
			)
	)


	(:action move-left-from-r2084
		:precondition
			(and 
				(in r2084)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2084))
				(in r2083)
			)
	)

	(:action move-right-from-r2085
		:precondition
			(and 
				(in r2085)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2085))
				(in r2086)
			)
	)


	(:action move-left-from-r2085
		:precondition
			(and 
				(in r2085)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2085))
				(in r2084)
			)
	)

	(:action move-right-from-r2086
		:precondition
			(and 
				(in r2086)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2086))
				(in r2087)
			)
	)


	(:action move-left-from-r2086
		:precondition
			(and 
				(in r2086)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2086))
				(in r2085)
			)
	)

	(:action move-right-from-r2087
		:precondition
			(and 
				(in r2087)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2087))
				(in r2088)
			)
	)


	(:action move-left-from-r2087
		:precondition
			(and 
				(in r2087)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2087))
				(in r2086)
			)
	)

	(:action move-right-from-r2088
		:precondition
			(and 
				(in r2088)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2088))
				(in r2089)
			)
	)


	(:action move-left-from-r2088
		:precondition
			(and 
				(in r2088)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2088))
				(in r2087)
			)
	)

	(:action move-right-from-r2089
		:precondition
			(and 
				(in r2089)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2089))
				(in r2090)
			)
	)


	(:action move-left-from-r2089
		:precondition
			(and 
				(in r2089)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2089))
				(in r2088)
			)
	)

	(:action move-right-from-r2090
		:precondition
			(and 
				(in r2090)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2090))
				(in r2091)
			)
	)


	(:action move-left-from-r2090
		:precondition
			(and 
				(in r2090)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2090))
				(in r2089)
			)
	)

	(:action move-right-from-r2091
		:precondition
			(and 
				(in r2091)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2091))
				(in r2092)
			)
	)


	(:action move-left-from-r2091
		:precondition
			(and 
				(in r2091)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2091))
				(in r2090)
			)
	)

	(:action move-right-from-r2092
		:precondition
			(and 
				(in r2092)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2092))
				(in r2093)
			)
	)


	(:action move-left-from-r2092
		:precondition
			(and 
				(in r2092)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2092))
				(in r2091)
			)
	)

	(:action move-right-from-r2093
		:precondition
			(and 
				(in r2093)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2093))
				(in r2094)
			)
	)


	(:action move-left-from-r2093
		:precondition
			(and 
				(in r2093)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2093))
				(in r2092)
			)
	)

	(:action move-right-from-r2094
		:precondition
			(and 
				(in r2094)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2094))
				(in r2095)
			)
	)


	(:action move-left-from-r2094
		:precondition
			(and 
				(in r2094)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2094))
				(in r2093)
			)
	)

	(:action move-right-from-r2095
		:precondition
			(and 
				(in r2095)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2095))
				(in r2096)
			)
	)


	(:action move-left-from-r2095
		:precondition
			(and 
				(in r2095)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2095))
				(in r2094)
			)
	)

	(:action move-right-from-r2096
		:precondition
			(and 
				(in r2096)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2096))
				(in r2097)
			)
	)


	(:action move-left-from-r2096
		:precondition
			(and 
				(in r2096)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2096))
				(in r2095)
			)
	)

	(:action move-right-from-r2097
		:precondition
			(and 
				(in r2097)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2097))
				(in r2098)
			)
	)


	(:action move-left-from-r2097
		:precondition
			(and 
				(in r2097)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2097))
				(in r2096)
			)
	)

	(:action move-right-from-r2098
		:precondition
			(and 
				(in r2098)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2098))
				(in r2099)
			)
	)


	(:action move-left-from-r2098
		:precondition
			(and 
				(in r2098)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2098))
				(in r2097)
			)
	)

	(:action move-right-from-r2099
		:precondition
			(and 
				(in r2099)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2099))
				(in r2100)
			)
	)


	(:action move-left-from-r2099
		:precondition
			(and 
				(in r2099)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2099))
				(in r2098)
			)
	)

	(:action move-right-from-r2100
		:precondition
			(and 
				(in r2100)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2100))
				(in r2101)
			)
	)


	(:action move-left-from-r2100
		:precondition
			(and 
				(in r2100)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2100))
				(in r2099)
			)
	)

	(:action move-right-from-r2101
		:precondition
			(and 
				(in r2101)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2101))
				(in r2102)
			)
	)


	(:action move-left-from-r2101
		:precondition
			(and 
				(in r2101)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2101))
				(in r2100)
			)
	)

	(:action move-right-from-r2102
		:precondition
			(and 
				(in r2102)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2102))
				(in r2103)
			)
	)


	(:action move-left-from-r2102
		:precondition
			(and 
				(in r2102)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2102))
				(in r2101)
			)
	)

	(:action move-right-from-r2103
		:precondition
			(and 
				(in r2103)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2103))
				(in r2104)
			)
	)


	(:action move-left-from-r2103
		:precondition
			(and 
				(in r2103)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2103))
				(in r2102)
			)
	)

	(:action move-right-from-r2104
		:precondition
			(and 
				(in r2104)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2104))
				(in r2105)
			)
	)


	(:action move-left-from-r2104
		:precondition
			(and 
				(in r2104)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2104))
				(in r2103)
			)
	)

	(:action move-right-from-r2105
		:precondition
			(and 
				(in r2105)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2105))
				(in r2106)
			)
	)


	(:action move-left-from-r2105
		:precondition
			(and 
				(in r2105)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2105))
				(in r2104)
			)
	)

	(:action move-right-from-r2106
		:precondition
			(and 
				(in r2106)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2106))
				(in r2107)
			)
	)


	(:action move-left-from-r2106
		:precondition
			(and 
				(in r2106)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2106))
				(in r2105)
			)
	)

	(:action move-right-from-r2107
		:precondition
			(and 
				(in r2107)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2107))
				(in r2108)
			)
	)


	(:action move-left-from-r2107
		:precondition
			(and 
				(in r2107)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2107))
				(in r2106)
			)
	)

	(:action move-right-from-r2108
		:precondition
			(and 
				(in r2108)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2108))
				(in r2109)
			)
	)


	(:action move-left-from-r2108
		:precondition
			(and 
				(in r2108)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2108))
				(in r2107)
			)
	)

	(:action move-right-from-r2109
		:precondition
			(and 
				(in r2109)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2109))
				(in r2110)
			)
	)


	(:action move-left-from-r2109
		:precondition
			(and 
				(in r2109)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2109))
				(in r2108)
			)
	)

	(:action move-right-from-r2110
		:precondition
			(and 
				(in r2110)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2110))
				(in r2111)
			)
	)


	(:action move-left-from-r2110
		:precondition
			(and 
				(in r2110)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2110))
				(in r2109)
			)
	)

	(:action move-right-from-r2111
		:precondition
			(and 
				(in r2111)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2111))
				(in r2112)
			)
	)


	(:action move-left-from-r2111
		:precondition
			(and 
				(in r2111)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2111))
				(in r2110)
			)
	)

	(:action move-right-from-r2112
		:precondition
			(and 
				(in r2112)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2112))
				(in r2113)
			)
	)


	(:action move-left-from-r2112
		:precondition
			(and 
				(in r2112)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2112))
				(in r2111)
			)
	)

	(:action move-right-from-r2113
		:precondition
			(and 
				(in r2113)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2113))
				(in r2114)
			)
	)


	(:action move-left-from-r2113
		:precondition
			(and 
				(in r2113)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2113))
				(in r2112)
			)
	)

	(:action move-right-from-r2114
		:precondition
			(and 
				(in r2114)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2114))
				(in r2115)
			)
	)


	(:action move-left-from-r2114
		:precondition
			(and 
				(in r2114)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2114))
				(in r2113)
			)
	)

	(:action move-right-from-r2115
		:precondition
			(and 
				(in r2115)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2115))
				(in r2116)
			)
	)


	(:action move-left-from-r2115
		:precondition
			(and 
				(in r2115)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2115))
				(in r2114)
			)
	)

	(:action move-right-from-r2116
		:precondition
			(and 
				(in r2116)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2116))
				(in r2117)
			)
	)


	(:action move-left-from-r2116
		:precondition
			(and 
				(in r2116)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2116))
				(in r2115)
			)
	)

	(:action move-right-from-r2117
		:precondition
			(and 
				(in r2117)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2117))
				(in r2118)
			)
	)


	(:action move-left-from-r2117
		:precondition
			(and 
				(in r2117)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2117))
				(in r2116)
			)
	)

	(:action move-right-from-r2118
		:precondition
			(and 
				(in r2118)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2118))
				(in r2119)
			)
	)


	(:action move-left-from-r2118
		:precondition
			(and 
				(in r2118)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2118))
				(in r2117)
			)
	)

	(:action move-right-from-r2119
		:precondition
			(and 
				(in r2119)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2119))
				(in r2120)
			)
	)


	(:action move-left-from-r2119
		:precondition
			(and 
				(in r2119)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2119))
				(in r2118)
			)
	)

	(:action move-right-from-r2120
		:precondition
			(and 
				(in r2120)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2120))
				(in r2121)
			)
	)


	(:action move-left-from-r2120
		:precondition
			(and 
				(in r2120)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2120))
				(in r2119)
			)
	)

	(:action move-right-from-r2121
		:precondition
			(and 
				(in r2121)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2121))
				(in r2122)
			)
	)


	(:action move-left-from-r2121
		:precondition
			(and 
				(in r2121)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2121))
				(in r2120)
			)
	)

	(:action move-right-from-r2122
		:precondition
			(and 
				(in r2122)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2122))
				(in r2123)
			)
	)


	(:action move-left-from-r2122
		:precondition
			(and 
				(in r2122)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2122))
				(in r2121)
			)
	)

	(:action move-right-from-r2123
		:precondition
			(and 
				(in r2123)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2123))
				(in r2124)
			)
	)


	(:action move-left-from-r2123
		:precondition
			(and 
				(in r2123)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2123))
				(in r2122)
			)
	)

	(:action move-right-from-r2124
		:precondition
			(and 
				(in r2124)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2124))
				(in r2125)
			)
	)


	(:action move-left-from-r2124
		:precondition
			(and 
				(in r2124)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2124))
				(in r2123)
			)
	)

	(:action move-right-from-r2125
		:precondition
			(and 
				(in r2125)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2125))
				(in r2126)
			)
	)


	(:action move-left-from-r2125
		:precondition
			(and 
				(in r2125)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2125))
				(in r2124)
			)
	)

	(:action move-right-from-r2126
		:precondition
			(and 
				(in r2126)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2126))
				(in r2127)
			)
	)


	(:action move-left-from-r2126
		:precondition
			(and 
				(in r2126)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2126))
				(in r2125)
			)
	)

	(:action move-right-from-r2127
		:precondition
			(and 
				(in r2127)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2127))
				(in r2128)
			)
	)


	(:action move-left-from-r2127
		:precondition
			(and 
				(in r2127)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2127))
				(in r2126)
			)
	)

	(:action move-right-from-r2128
		:precondition
			(and 
				(in r2128)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2128))
				(in r2129)
			)
	)


	(:action move-left-from-r2128
		:precondition
			(and 
				(in r2128)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2128))
				(in r2127)
			)
	)

	(:action move-right-from-r2129
		:precondition
			(and 
				(in r2129)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2129))
				(in r2130)
			)
	)


	(:action move-left-from-r2129
		:precondition
			(and 
				(in r2129)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2129))
				(in r2128)
			)
	)

	(:action move-right-from-r2130
		:precondition
			(and 
				(in r2130)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2130))
				(in r2131)
			)
	)


	(:action move-left-from-r2130
		:precondition
			(and 
				(in r2130)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2130))
				(in r2129)
			)
	)

	(:action move-right-from-r2131
		:precondition
			(and 
				(in r2131)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2131))
				(in r2132)
			)
	)


	(:action move-left-from-r2131
		:precondition
			(and 
				(in r2131)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2131))
				(in r2130)
			)
	)

	(:action move-right-from-r2132
		:precondition
			(and 
				(in r2132)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2132))
				(in r2133)
			)
	)


	(:action move-left-from-r2132
		:precondition
			(and 
				(in r2132)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2132))
				(in r2131)
			)
	)

	(:action move-right-from-r2133
		:precondition
			(and 
				(in r2133)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2133))
				(in r2134)
			)
	)


	(:action move-left-from-r2133
		:precondition
			(and 
				(in r2133)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2133))
				(in r2132)
			)
	)

	(:action move-right-from-r2134
		:precondition
			(and 
				(in r2134)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2134))
				(in r2135)
			)
	)


	(:action move-left-from-r2134
		:precondition
			(and 
				(in r2134)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2134))
				(in r2133)
			)
	)

	(:action move-right-from-r2135
		:precondition
			(and 
				(in r2135)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2135))
				(in r2136)
			)
	)


	(:action move-left-from-r2135
		:precondition
			(and 
				(in r2135)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2135))
				(in r2134)
			)
	)

	(:action move-right-from-r2136
		:precondition
			(and 
				(in r2136)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2136))
				(in r2137)
			)
	)


	(:action move-left-from-r2136
		:precondition
			(and 
				(in r2136)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2136))
				(in r2135)
			)
	)

	(:action move-right-from-r2137
		:precondition
			(and 
				(in r2137)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2137))
				(in r2138)
			)
	)


	(:action move-left-from-r2137
		:precondition
			(and 
				(in r2137)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2137))
				(in r2136)
			)
	)

	(:action move-right-from-r2138
		:precondition
			(and 
				(in r2138)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2138))
				(in r2139)
			)
	)


	(:action move-left-from-r2138
		:precondition
			(and 
				(in r2138)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2138))
				(in r2137)
			)
	)

	(:action move-right-from-r2139
		:precondition
			(and 
				(in r2139)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2139))
				(in r2140)
			)
	)


	(:action move-left-from-r2139
		:precondition
			(and 
				(in r2139)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2139))
				(in r2138)
			)
	)

	(:action move-right-from-r2140
		:precondition
			(and 
				(in r2140)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2140))
				(in r2141)
			)
	)


	(:action move-left-from-r2140
		:precondition
			(and 
				(in r2140)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2140))
				(in r2139)
			)
	)

	(:action move-right-from-r2141
		:precondition
			(and 
				(in r2141)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2141))
				(in r2142)
			)
	)


	(:action move-left-from-r2141
		:precondition
			(and 
				(in r2141)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2141))
				(in r2140)
			)
	)

	(:action move-right-from-r2142
		:precondition
			(and 
				(in r2142)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2142))
				(in r2143)
			)
	)


	(:action move-left-from-r2142
		:precondition
			(and 
				(in r2142)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2142))
				(in r2141)
			)
	)

	(:action move-right-from-r2143
		:precondition
			(and 
				(in r2143)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2143))
				(in r2144)
			)
	)


	(:action move-left-from-r2143
		:precondition
			(and 
				(in r2143)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2143))
				(in r2142)
			)
	)

	(:action move-right-from-r2144
		:precondition
			(and 
				(in r2144)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2144))
				(in r2145)
			)
	)


	(:action move-left-from-r2144
		:precondition
			(and 
				(in r2144)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2144))
				(in r2143)
			)
	)

	(:action move-right-from-r2145
		:precondition
			(and 
				(in r2145)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2145))
				(in r2146)
			)
	)


	(:action move-left-from-r2145
		:precondition
			(and 
				(in r2145)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2145))
				(in r2144)
			)
	)

	(:action move-right-from-r2146
		:precondition
			(and 
				(in r2146)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2146))
				(in r2147)
			)
	)


	(:action move-left-from-r2146
		:precondition
			(and 
				(in r2146)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2146))
				(in r2145)
			)
	)

	(:action move-right-from-r2147
		:precondition
			(and 
				(in r2147)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2147))
				(in r2148)
			)
	)


	(:action move-left-from-r2147
		:precondition
			(and 
				(in r2147)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2147))
				(in r2146)
			)
	)

	(:action move-right-from-r2148
		:precondition
			(and 
				(in r2148)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2148))
				(in r2149)
			)
	)


	(:action move-left-from-r2148
		:precondition
			(and 
				(in r2148)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2148))
				(in r2147)
			)
	)

	(:action move-right-from-r2149
		:precondition
			(and 
				(in r2149)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2149))
				(in r2150)
			)
	)


	(:action move-left-from-r2149
		:precondition
			(and 
				(in r2149)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2149))
				(in r2148)
			)
	)

	(:action move-right-from-r2150
		:precondition
			(and 
				(in r2150)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2150))
				(in r2151)
			)
	)


	(:action move-left-from-r2150
		:precondition
			(and 
				(in r2150)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2150))
				(in r2149)
			)
	)

	(:action move-right-from-r2151
		:precondition
			(and 
				(in r2151)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2151))
				(in r2152)
			)
	)


	(:action move-left-from-r2151
		:precondition
			(and 
				(in r2151)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2151))
				(in r2150)
			)
	)

	(:action move-right-from-r2152
		:precondition
			(and 
				(in r2152)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2152))
				(in r2153)
			)
	)


	(:action move-left-from-r2152
		:precondition
			(and 
				(in r2152)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2152))
				(in r2151)
			)
	)

	(:action move-right-from-r2153
		:precondition
			(and 
				(in r2153)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2153))
				(in r2154)
			)
	)


	(:action move-left-from-r2153
		:precondition
			(and 
				(in r2153)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2153))
				(in r2152)
			)
	)

	(:action move-right-from-r2154
		:precondition
			(and 
				(in r2154)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2154))
				(in r2155)
			)
	)


	(:action move-left-from-r2154
		:precondition
			(and 
				(in r2154)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2154))
				(in r2153)
			)
	)

	(:action move-right-from-r2155
		:precondition
			(and 
				(in r2155)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2155))
				(in r2156)
			)
	)


	(:action move-left-from-r2155
		:precondition
			(and 
				(in r2155)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2155))
				(in r2154)
			)
	)

	(:action move-right-from-r2156
		:precondition
			(and 
				(in r2156)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2156))
				(in r2157)
			)
	)


	(:action move-left-from-r2156
		:precondition
			(and 
				(in r2156)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2156))
				(in r2155)
			)
	)

	(:action move-right-from-r2157
		:precondition
			(and 
				(in r2157)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2157))
				(in r2158)
			)
	)


	(:action move-left-from-r2157
		:precondition
			(and 
				(in r2157)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2157))
				(in r2156)
			)
	)

	(:action move-right-from-r2158
		:precondition
			(and 
				(in r2158)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2158))
				(in r2159)
			)
	)


	(:action move-left-from-r2158
		:precondition
			(and 
				(in r2158)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2158))
				(in r2157)
			)
	)

	(:action move-right-from-r2159
		:precondition
			(and 
				(in r2159)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2159))
				(in r2160)
			)
	)


	(:action move-left-from-r2159
		:precondition
			(and 
				(in r2159)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2159))
				(in r2158)
			)
	)

	(:action move-right-from-r2160
		:precondition
			(and 
				(in r2160)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2160))
				(in r2161)
			)
	)


	(:action move-left-from-r2160
		:precondition
			(and 
				(in r2160)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2160))
				(in r2159)
			)
	)

	(:action move-right-from-r2161
		:precondition
			(and 
				(in r2161)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2161))
				(in r2162)
			)
	)


	(:action move-left-from-r2161
		:precondition
			(and 
				(in r2161)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2161))
				(in r2160)
			)
	)

	(:action move-right-from-r2162
		:precondition
			(and 
				(in r2162)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2162))
				(in r2163)
			)
	)


	(:action move-left-from-r2162
		:precondition
			(and 
				(in r2162)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2162))
				(in r2161)
			)
	)

	(:action move-right-from-r2163
		:precondition
			(and 
				(in r2163)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2163))
				(in r2164)
			)
	)


	(:action move-left-from-r2163
		:precondition
			(and 
				(in r2163)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2163))
				(in r2162)
			)
	)

	(:action move-right-from-r2164
		:precondition
			(and 
				(in r2164)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2164))
				(in r2165)
			)
	)


	(:action move-left-from-r2164
		:precondition
			(and 
				(in r2164)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2164))
				(in r2163)
			)
	)

	(:action move-right-from-r2165
		:precondition
			(and 
				(in r2165)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2165))
				(in r2166)
			)
	)


	(:action move-left-from-r2165
		:precondition
			(and 
				(in r2165)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2165))
				(in r2164)
			)
	)

	(:action move-right-from-r2166
		:precondition
			(and 
				(in r2166)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2166))
				(in r2167)
			)
	)


	(:action move-left-from-r2166
		:precondition
			(and 
				(in r2166)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2166))
				(in r2165)
			)
	)

	(:action move-right-from-r2167
		:precondition
			(and 
				(in r2167)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2167))
				(in r2168)
			)
	)


	(:action move-left-from-r2167
		:precondition
			(and 
				(in r2167)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2167))
				(in r2166)
			)
	)

	(:action move-right-from-r2168
		:precondition
			(and 
				(in r2168)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2168))
				(in r2169)
			)
	)


	(:action move-left-from-r2168
		:precondition
			(and 
				(in r2168)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2168))
				(in r2167)
			)
	)

	(:action move-right-from-r2169
		:precondition
			(and 
				(in r2169)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2169))
				(in r2170)
			)
	)


	(:action move-left-from-r2169
		:precondition
			(and 
				(in r2169)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2169))
				(in r2168)
			)
	)

	(:action move-right-from-r2170
		:precondition
			(and 
				(in r2170)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2170))
				(in r2171)
			)
	)


	(:action move-left-from-r2170
		:precondition
			(and 
				(in r2170)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2170))
				(in r2169)
			)
	)

	(:action move-right-from-r2171
		:precondition
			(and 
				(in r2171)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2171))
				(in r2172)
			)
	)


	(:action move-left-from-r2171
		:precondition
			(and 
				(in r2171)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2171))
				(in r2170)
			)
	)

	(:action move-right-from-r2172
		:precondition
			(and 
				(in r2172)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2172))
				(in r2173)
			)
	)


	(:action move-left-from-r2172
		:precondition
			(and 
				(in r2172)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2172))
				(in r2171)
			)
	)

	(:action move-right-from-r2173
		:precondition
			(and 
				(in r2173)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2173))
				(in r2174)
			)
	)


	(:action move-left-from-r2173
		:precondition
			(and 
				(in r2173)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2173))
				(in r2172)
			)
	)

	(:action move-right-from-r2174
		:precondition
			(and 
				(in r2174)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2174))
				(in r2175)
			)
	)


	(:action move-left-from-r2174
		:precondition
			(and 
				(in r2174)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2174))
				(in r2173)
			)
	)

	(:action move-right-from-r2175
		:precondition
			(and 
				(in r2175)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2175))
				(in r2176)
			)
	)


	(:action move-left-from-r2175
		:precondition
			(and 
				(in r2175)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2175))
				(in r2174)
			)
	)

	(:action move-right-from-r2176
		:precondition
			(and 
				(in r2176)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2176))
				(in r2177)
			)
	)


	(:action move-left-from-r2176
		:precondition
			(and 
				(in r2176)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2176))
				(in r2175)
			)
	)

	(:action move-right-from-r2177
		:precondition
			(and 
				(in r2177)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2177))
				(in r2178)
			)
	)


	(:action move-left-from-r2177
		:precondition
			(and 
				(in r2177)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2177))
				(in r2176)
			)
	)

	(:action move-right-from-r2178
		:precondition
			(and 
				(in r2178)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2178))
				(in r2179)
			)
	)


	(:action move-left-from-r2178
		:precondition
			(and 
				(in r2178)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2178))
				(in r2177)
			)
	)

	(:action move-right-from-r2179
		:precondition
			(and 
				(in r2179)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2179))
				(in r2180)
			)
	)


	(:action move-left-from-r2179
		:precondition
			(and 
				(in r2179)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2179))
				(in r2178)
			)
	)

	(:action move-right-from-r2180
		:precondition
			(and 
				(in r2180)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2180))
				(in r2181)
			)
	)


	(:action move-left-from-r2180
		:precondition
			(and 
				(in r2180)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2180))
				(in r2179)
			)
	)

	(:action move-right-from-r2181
		:precondition
			(and 
				(in r2181)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2181))
				(in r2182)
			)
	)


	(:action move-left-from-r2181
		:precondition
			(and 
				(in r2181)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2181))
				(in r2180)
			)
	)

	(:action move-right-from-r2182
		:precondition
			(and 
				(in r2182)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2182))
				(in r2183)
			)
	)


	(:action move-left-from-r2182
		:precondition
			(and 
				(in r2182)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2182))
				(in r2181)
			)
	)

	(:action move-right-from-r2183
		:precondition
			(and 
				(in r2183)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2183))
				(in r2184)
			)
	)


	(:action move-left-from-r2183
		:precondition
			(and 
				(in r2183)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2183))
				(in r2182)
			)
	)

	(:action move-right-from-r2184
		:precondition
			(and 
				(in r2184)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2184))
				(in r2185)
			)
	)


	(:action move-left-from-r2184
		:precondition
			(and 
				(in r2184)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2184))
				(in r2183)
			)
	)

	(:action move-right-from-r2185
		:precondition
			(and 
				(in r2185)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2185))
				(in r2186)
			)
	)


	(:action move-left-from-r2185
		:precondition
			(and 
				(in r2185)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2185))
				(in r2184)
			)
	)

	(:action move-right-from-r2186
		:precondition
			(and 
				(in r2186)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2186))
				(in r2187)
			)
	)


	(:action move-left-from-r2186
		:precondition
			(and 
				(in r2186)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2186))
				(in r2185)
			)
	)

	(:action move-right-from-r2187
		:precondition
			(and 
				(in r2187)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2187))
				(in r2188)
			)
	)


	(:action move-left-from-r2187
		:precondition
			(and 
				(in r2187)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2187))
				(in r2186)
			)
	)

	(:action move-right-from-r2188
		:precondition
			(and 
				(in r2188)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2188))
				(in r2189)
			)
	)


	(:action move-left-from-r2188
		:precondition
			(and 
				(in r2188)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2188))
				(in r2187)
			)
	)

	(:action move-right-from-r2189
		:precondition
			(and 
				(in r2189)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2189))
				(in r2190)
			)
	)


	(:action move-left-from-r2189
		:precondition
			(and 
				(in r2189)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2189))
				(in r2188)
			)
	)

	(:action move-right-from-r2190
		:precondition
			(and 
				(in r2190)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2190))
				(in r2191)
			)
	)


	(:action move-left-from-r2190
		:precondition
			(and 
				(in r2190)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2190))
				(in r2189)
			)
	)

	(:action move-right-from-r2191
		:precondition
			(and 
				(in r2191)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2191))
				(in r2192)
			)
	)


	(:action move-left-from-r2191
		:precondition
			(and 
				(in r2191)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2191))
				(in r2190)
			)
	)

	(:action move-right-from-r2192
		:precondition
			(and 
				(in r2192)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2192))
				(in r2193)
			)
	)


	(:action move-left-from-r2192
		:precondition
			(and 
				(in r2192)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2192))
				(in r2191)
			)
	)

	(:action move-right-from-r2193
		:precondition
			(and 
				(in r2193)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2193))
				(in r2194)
			)
	)


	(:action move-left-from-r2193
		:precondition
			(and 
				(in r2193)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2193))
				(in r2192)
			)
	)

	(:action move-right-from-r2194
		:precondition
			(and 
				(in r2194)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2194))
				(in r2195)
			)
	)


	(:action move-left-from-r2194
		:precondition
			(and 
				(in r2194)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2194))
				(in r2193)
			)
	)

	(:action move-right-from-r2195
		:precondition
			(and 
				(in r2195)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2195))
				(in r2196)
			)
	)


	(:action move-left-from-r2195
		:precondition
			(and 
				(in r2195)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2195))
				(in r2194)
			)
	)

	(:action move-right-from-r2196
		:precondition
			(and 
				(in r2196)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2196))
				(in r2197)
			)
	)


	(:action move-left-from-r2196
		:precondition
			(and 
				(in r2196)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2196))
				(in r2195)
			)
	)

	(:action move-right-from-r2197
		:precondition
			(and 
				(in r2197)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2197))
				(in r2198)
			)
	)


	(:action move-left-from-r2197
		:precondition
			(and 
				(in r2197)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2197))
				(in r2196)
			)
	)

	(:action move-right-from-r2198
		:precondition
			(and 
				(in r2198)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2198))
				(in r2199)
			)
	)


	(:action move-left-from-r2198
		:precondition
			(and 
				(in r2198)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2198))
				(in r2197)
			)
	)

	(:action move-right-from-r2199
		:precondition
			(and 
				(in r2199)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2199))
				(in r2200)
			)
	)


	(:action move-left-from-r2199
		:precondition
			(and 
				(in r2199)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2199))
				(in r2198)
			)
	)

	(:action move-right-from-r2200
		:precondition
			(and 
				(in r2200)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2200))
				(in r2201)
			)
	)


	(:action move-left-from-r2200
		:precondition
			(and 
				(in r2200)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2200))
				(in r2199)
			)
	)

	(:action move-right-from-r2201
		:precondition
			(and 
				(in r2201)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2201))
				(in r2202)
			)
	)


	(:action move-left-from-r2201
		:precondition
			(and 
				(in r2201)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2201))
				(in r2200)
			)
	)

	(:action move-right-from-r2202
		:precondition
			(and 
				(in r2202)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2202))
				(in r2203)
			)
	)


	(:action move-left-from-r2202
		:precondition
			(and 
				(in r2202)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2202))
				(in r2201)
			)
	)

	(:action move-right-from-r2203
		:precondition
			(and 
				(in r2203)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2203))
				(in r2204)
			)
	)


	(:action move-left-from-r2203
		:precondition
			(and 
				(in r2203)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2203))
				(in r2202)
			)
	)

	(:action move-right-from-r2204
		:precondition
			(and 
				(in r2204)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2204))
				(in r2205)
			)
	)


	(:action move-left-from-r2204
		:precondition
			(and 
				(in r2204)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2204))
				(in r2203)
			)
	)

	(:action move-right-from-r2205
		:precondition
			(and 
				(in r2205)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2205))
				(in r2206)
			)
	)


	(:action move-left-from-r2205
		:precondition
			(and 
				(in r2205)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2205))
				(in r2204)
			)
	)

	(:action move-right-from-r2206
		:precondition
			(and 
				(in r2206)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2206))
				(in r2207)
			)
	)


	(:action move-left-from-r2206
		:precondition
			(and 
				(in r2206)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2206))
				(in r2205)
			)
	)

	(:action move-right-from-r2207
		:precondition
			(and 
				(in r2207)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2207))
				(in r2208)
			)
	)


	(:action move-left-from-r2207
		:precondition
			(and 
				(in r2207)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2207))
				(in r2206)
			)
	)

	(:action move-right-from-r2208
		:precondition
			(and 
				(in r2208)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2208))
				(in r2209)
			)
	)


	(:action move-left-from-r2208
		:precondition
			(and 
				(in r2208)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2208))
				(in r2207)
			)
	)

	(:action move-right-from-r2209
		:precondition
			(and 
				(in r2209)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2209))
				(in r2210)
			)
	)


	(:action move-left-from-r2209
		:precondition
			(and 
				(in r2209)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2209))
				(in r2208)
			)
	)

	(:action move-right-from-r2210
		:precondition
			(and 
				(in r2210)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2210))
				(in r2211)
			)
	)


	(:action move-left-from-r2210
		:precondition
			(and 
				(in r2210)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2210))
				(in r2209)
			)
	)

	(:action move-right-from-r2211
		:precondition
			(and 
				(in r2211)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2211))
				(in r2212)
			)
	)


	(:action move-left-from-r2211
		:precondition
			(and 
				(in r2211)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2211))
				(in r2210)
			)
	)

	(:action move-right-from-r2212
		:precondition
			(and 
				(in r2212)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2212))
				(in r2213)
			)
	)


	(:action move-left-from-r2212
		:precondition
			(and 
				(in r2212)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2212))
				(in r2211)
			)
	)

	(:action move-right-from-r2213
		:precondition
			(and 
				(in r2213)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2213))
				(in r2214)
			)
	)


	(:action move-left-from-r2213
		:precondition
			(and 
				(in r2213)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2213))
				(in r2212)
			)
	)

	(:action move-right-from-r2214
		:precondition
			(and 
				(in r2214)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2214))
				(in r2215)
			)
	)


	(:action move-left-from-r2214
		:precondition
			(and 
				(in r2214)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2214))
				(in r2213)
			)
	)

	(:action move-right-from-r2215
		:precondition
			(and 
				(in r2215)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2215))
				(in r2216)
			)
	)


	(:action move-left-from-r2215
		:precondition
			(and 
				(in r2215)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2215))
				(in r2214)
			)
	)

	(:action move-right-from-r2216
		:precondition
			(and 
				(in r2216)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2216))
				(in r2217)
			)
	)


	(:action move-left-from-r2216
		:precondition
			(and 
				(in r2216)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2216))
				(in r2215)
			)
	)

	(:action move-right-from-r2217
		:precondition
			(and 
				(in r2217)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2217))
				(in r2218)
			)
	)


	(:action move-left-from-r2217
		:precondition
			(and 
				(in r2217)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2217))
				(in r2216)
			)
	)

	(:action move-right-from-r2218
		:precondition
			(and 
				(in r2218)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2218))
				(in r2219)
			)
	)


	(:action move-left-from-r2218
		:precondition
			(and 
				(in r2218)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2218))
				(in r2217)
			)
	)

	(:action move-right-from-r2219
		:precondition
			(and 
				(in r2219)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2219))
				(in r2220)
			)
	)


	(:action move-left-from-r2219
		:precondition
			(and 
				(in r2219)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2219))
				(in r2218)
			)
	)

	(:action move-right-from-r2220
		:precondition
			(and 
				(in r2220)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2220))
				(in r2221)
			)
	)


	(:action move-left-from-r2220
		:precondition
			(and 
				(in r2220)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2220))
				(in r2219)
			)
	)

	(:action move-right-from-r2221
		:precondition
			(and 
				(in r2221)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2221))
				(in r2222)
			)
	)


	(:action move-left-from-r2221
		:precondition
			(and 
				(in r2221)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2221))
				(in r2220)
			)
	)

	(:action move-right-from-r2222
		:precondition
			(and 
				(in r2222)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2222))
				(in r2223)
			)
	)


	(:action move-left-from-r2222
		:precondition
			(and 
				(in r2222)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2222))
				(in r2221)
			)
	)

	(:action move-right-from-r2223
		:precondition
			(and 
				(in r2223)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2223))
				(in r2224)
			)
	)


	(:action move-left-from-r2223
		:precondition
			(and 
				(in r2223)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2223))
				(in r2222)
			)
	)

	(:action move-right-from-r2224
		:precondition
			(and 
				(in r2224)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2224))
				(in r2225)
			)
	)


	(:action move-left-from-r2224
		:precondition
			(and 
				(in r2224)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2224))
				(in r2223)
			)
	)

	(:action move-right-from-r2225
		:precondition
			(and 
				(in r2225)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2225))
				(in r2226)
			)
	)


	(:action move-left-from-r2225
		:precondition
			(and 
				(in r2225)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2225))
				(in r2224)
			)
	)

	(:action move-right-from-r2226
		:precondition
			(and 
				(in r2226)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2226))
				(in r2227)
			)
	)


	(:action move-left-from-r2226
		:precondition
			(and 
				(in r2226)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2226))
				(in r2225)
			)
	)

	(:action move-right-from-r2227
		:precondition
			(and 
				(in r2227)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2227))
				(in r2228)
			)
	)


	(:action move-left-from-r2227
		:precondition
			(and 
				(in r2227)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2227))
				(in r2226)
			)
	)

	(:action move-right-from-r2228
		:precondition
			(and 
				(in r2228)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2228))
				(in r2229)
			)
	)


	(:action move-left-from-r2228
		:precondition
			(and 
				(in r2228)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2228))
				(in r2227)
			)
	)

	(:action move-right-from-r2229
		:precondition
			(and 
				(in r2229)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2229))
				(in r2230)
			)
	)


	(:action move-left-from-r2229
		:precondition
			(and 
				(in r2229)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2229))
				(in r2228)
			)
	)

	(:action move-right-from-r2230
		:precondition
			(and 
				(in r2230)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2230))
				(in r2231)
			)
	)


	(:action move-left-from-r2230
		:precondition
			(and 
				(in r2230)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2230))
				(in r2229)
			)
	)

	(:action move-right-from-r2231
		:precondition
			(and 
				(in r2231)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2231))
				(in r2232)
			)
	)


	(:action move-left-from-r2231
		:precondition
			(and 
				(in r2231)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2231))
				(in r2230)
			)
	)

	(:action move-right-from-r2232
		:precondition
			(and 
				(in r2232)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2232))
				(in r2233)
			)
	)


	(:action move-left-from-r2232
		:precondition
			(and 
				(in r2232)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2232))
				(in r2231)
			)
	)

	(:action move-right-from-r2233
		:precondition
			(and 
				(in r2233)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2233))
				(in r2234)
			)
	)


	(:action move-left-from-r2233
		:precondition
			(and 
				(in r2233)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2233))
				(in r2232)
			)
	)

	(:action move-right-from-r2234
		:precondition
			(and 
				(in r2234)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2234))
				(in r2235)
			)
	)


	(:action move-left-from-r2234
		:precondition
			(and 
				(in r2234)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2234))
				(in r2233)
			)
	)

	(:action move-right-from-r2235
		:precondition
			(and 
				(in r2235)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2235))
				(in r2236)
			)
	)


	(:action move-left-from-r2235
		:precondition
			(and 
				(in r2235)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2235))
				(in r2234)
			)
	)

	(:action move-right-from-r2236
		:precondition
			(and 
				(in r2236)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2236))
				(in r2237)
			)
	)


	(:action move-left-from-r2236
		:precondition
			(and 
				(in r2236)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2236))
				(in r2235)
			)
	)

	(:action move-right-from-r2237
		:precondition
			(and 
				(in r2237)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2237))
				(in r2238)
			)
	)


	(:action move-left-from-r2237
		:precondition
			(and 
				(in r2237)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2237))
				(in r2236)
			)
	)

	(:action move-right-from-r2238
		:precondition
			(and 
				(in r2238)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2238))
				(in r2239)
			)
	)


	(:action move-left-from-r2238
		:precondition
			(and 
				(in r2238)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2238))
				(in r2237)
			)
	)

	(:action move-right-from-r2239
		:precondition
			(and 
				(in r2239)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2239))
				(in r2240)
			)
	)


	(:action move-left-from-r2239
		:precondition
			(and 
				(in r2239)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2239))
				(in r2238)
			)
	)

	(:action move-right-from-r2240
		:precondition
			(and 
				(in r2240)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2240))
				(in r2241)
			)
	)


	(:action move-left-from-r2240
		:precondition
			(and 
				(in r2240)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2240))
				(in r2239)
			)
	)

	(:action move-right-from-r2241
		:precondition
			(and 
				(in r2241)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2241))
				(in r2242)
			)
	)


	(:action move-left-from-r2241
		:precondition
			(and 
				(in r2241)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2241))
				(in r2240)
			)
	)

	(:action move-right-from-r2242
		:precondition
			(and 
				(in r2242)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2242))
				(in r2243)
			)
	)


	(:action move-left-from-r2242
		:precondition
			(and 
				(in r2242)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2242))
				(in r2241)
			)
	)

	(:action move-right-from-r2243
		:precondition
			(and 
				(in r2243)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2243))
				(in r2244)
			)
	)


	(:action move-left-from-r2243
		:precondition
			(and 
				(in r2243)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2243))
				(in r2242)
			)
	)

	(:action move-right-from-r2244
		:precondition
			(and 
				(in r2244)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2244))
				(in r2245)
			)
	)


	(:action move-left-from-r2244
		:precondition
			(and 
				(in r2244)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2244))
				(in r2243)
			)
	)

	(:action move-right-from-r2245
		:precondition
			(and 
				(in r2245)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2245))
				(in r2246)
			)
	)


	(:action move-left-from-r2245
		:precondition
			(and 
				(in r2245)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2245))
				(in r2244)
			)
	)

	(:action move-right-from-r2246
		:precondition
			(and 
				(in r2246)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2246))
				(in r2247)
			)
	)


	(:action move-left-from-r2246
		:precondition
			(and 
				(in r2246)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2246))
				(in r2245)
			)
	)

	(:action move-right-from-r2247
		:precondition
			(and 
				(in r2247)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2247))
				(in r2248)
			)
	)


	(:action move-left-from-r2247
		:precondition
			(and 
				(in r2247)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2247))
				(in r2246)
			)
	)

	(:action move-right-from-r2248
		:precondition
			(and 
				(in r2248)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2248))
				(in r2249)
			)
	)


	(:action move-left-from-r2248
		:precondition
			(and 
				(in r2248)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2248))
				(in r2247)
			)
	)

	(:action move-right-from-r2249
		:precondition
			(and 
				(in r2249)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2249))
				(in r2250)
			)
	)


	(:action move-left-from-r2249
		:precondition
			(and 
				(in r2249)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2249))
				(in r2248)
			)
	)

	(:action move-right-from-r2250
		:precondition
			(and 
				(in r2250)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2250))
				(in r2251)
			)
	)


	(:action move-left-from-r2250
		:precondition
			(and 
				(in r2250)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2250))
				(in r2249)
			)
	)

	(:action move-right-from-r2251
		:precondition
			(and 
				(in r2251)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2251))
				(in r2252)
			)
	)


	(:action move-left-from-r2251
		:precondition
			(and 
				(in r2251)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2251))
				(in r2250)
			)
	)

	(:action move-right-from-r2252
		:precondition
			(and 
				(in r2252)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2252))
				(in r2253)
			)
	)


	(:action move-left-from-r2252
		:precondition
			(and 
				(in r2252)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2252))
				(in r2251)
			)
	)

	(:action move-right-from-r2253
		:precondition
			(and 
				(in r2253)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2253))
				(in r2254)
			)
	)


	(:action move-left-from-r2253
		:precondition
			(and 
				(in r2253)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2253))
				(in r2252)
			)
	)

	(:action move-right-from-r2254
		:precondition
			(and 
				(in r2254)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2254))
				(in r2255)
			)
	)


	(:action move-left-from-r2254
		:precondition
			(and 
				(in r2254)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2254))
				(in r2253)
			)
	)

	(:action move-right-from-r2255
		:precondition
			(and 
				(in r2255)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2255))
				(in r2256)
			)
	)


	(:action move-left-from-r2255
		:precondition
			(and 
				(in r2255)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2255))
				(in r2254)
			)
	)

	(:action move-right-from-r2256
		:precondition
			(and 
				(in r2256)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2256))
				(in r2257)
			)
	)


	(:action move-left-from-r2256
		:precondition
			(and 
				(in r2256)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2256))
				(in r2255)
			)
	)

	(:action move-right-from-r2257
		:precondition
			(and 
				(in r2257)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2257))
				(in r2258)
			)
	)


	(:action move-left-from-r2257
		:precondition
			(and 
				(in r2257)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2257))
				(in r2256)
			)
	)

	(:action move-right-from-r2258
		:precondition
			(and 
				(in r2258)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2258))
				(in r2259)
			)
	)


	(:action move-left-from-r2258
		:precondition
			(and 
				(in r2258)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2258))
				(in r2257)
			)
	)

	(:action move-right-from-r2259
		:precondition
			(and 
				(in r2259)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2259))
				(in r2260)
			)
	)


	(:action move-left-from-r2259
		:precondition
			(and 
				(in r2259)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2259))
				(in r2258)
			)
	)

	(:action move-right-from-r2260
		:precondition
			(and 
				(in r2260)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2260))
				(in r2261)
			)
	)


	(:action move-left-from-r2260
		:precondition
			(and 
				(in r2260)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2260))
				(in r2259)
			)
	)

	(:action move-right-from-r2261
		:precondition
			(and 
				(in r2261)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2261))
				(in r2262)
			)
	)


	(:action move-left-from-r2261
		:precondition
			(and 
				(in r2261)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2261))
				(in r2260)
			)
	)

	(:action move-right-from-r2262
		:precondition
			(and 
				(in r2262)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2262))
				(in r2263)
			)
	)


	(:action move-left-from-r2262
		:precondition
			(and 
				(in r2262)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2262))
				(in r2261)
			)
	)

	(:action move-right-from-r2263
		:precondition
			(and 
				(in r2263)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2263))
				(in r2264)
			)
	)


	(:action move-left-from-r2263
		:precondition
			(and 
				(in r2263)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2263))
				(in r2262)
			)
	)

	(:action move-right-from-r2264
		:precondition
			(and 
				(in r2264)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2264))
				(in r2265)
			)
	)


	(:action move-left-from-r2264
		:precondition
			(and 
				(in r2264)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2264))
				(in r2263)
			)
	)

	(:action move-right-from-r2265
		:precondition
			(and 
				(in r2265)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2265))
				(in r2266)
			)
	)


	(:action move-left-from-r2265
		:precondition
			(and 
				(in r2265)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2265))
				(in r2264)
			)
	)

	(:action move-right-from-r2266
		:precondition
			(and 
				(in r2266)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2266))
				(in r2267)
			)
	)


	(:action move-left-from-r2266
		:precondition
			(and 
				(in r2266)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2266))
				(in r2265)
			)
	)

	(:action move-right-from-r2267
		:precondition
			(and 
				(in r2267)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2267))
				(in r2268)
			)
	)


	(:action move-left-from-r2267
		:precondition
			(and 
				(in r2267)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2267))
				(in r2266)
			)
	)

	(:action move-right-from-r2268
		:precondition
			(and 
				(in r2268)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2268))
				(in r2269)
			)
	)


	(:action move-left-from-r2268
		:precondition
			(and 
				(in r2268)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2268))
				(in r2267)
			)
	)

	(:action move-right-from-r2269
		:precondition
			(and 
				(in r2269)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2269))
				(in r2270)
			)
	)


	(:action move-left-from-r2269
		:precondition
			(and 
				(in r2269)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2269))
				(in r2268)
			)
	)

	(:action move-right-from-r2270
		:precondition
			(and 
				(in r2270)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2270))
				(in r2271)
			)
	)


	(:action move-left-from-r2270
		:precondition
			(and 
				(in r2270)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2270))
				(in r2269)
			)
	)

	(:action move-right-from-r2271
		:precondition
			(and 
				(in r2271)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2271))
				(in r2272)
			)
	)


	(:action move-left-from-r2271
		:precondition
			(and 
				(in r2271)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2271))
				(in r2270)
			)
	)

	(:action move-right-from-r2272
		:precondition
			(and 
				(in r2272)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2272))
				(in r2273)
			)
	)


	(:action move-left-from-r2272
		:precondition
			(and 
				(in r2272)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2272))
				(in r2271)
			)
	)

	(:action move-right-from-r2273
		:precondition
			(and 
				(in r2273)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2273))
				(in r2274)
			)
	)


	(:action move-left-from-r2273
		:precondition
			(and 
				(in r2273)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2273))
				(in r2272)
			)
	)

	(:action move-right-from-r2274
		:precondition
			(and 
				(in r2274)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2274))
				(in r2275)
			)
	)


	(:action move-left-from-r2274
		:precondition
			(and 
				(in r2274)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2274))
				(in r2273)
			)
	)

	(:action move-right-from-r2275
		:precondition
			(and 
				(in r2275)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2275))
				(in r2276)
			)
	)


	(:action move-left-from-r2275
		:precondition
			(and 
				(in r2275)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2275))
				(in r2274)
			)
	)

	(:action move-right-from-r2276
		:precondition
			(and 
				(in r2276)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2276))
				(in r2277)
			)
	)


	(:action move-left-from-r2276
		:precondition
			(and 
				(in r2276)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2276))
				(in r2275)
			)
	)

	(:action move-right-from-r2277
		:precondition
			(and 
				(in r2277)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2277))
				(in r2278)
			)
	)


	(:action move-left-from-r2277
		:precondition
			(and 
				(in r2277)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2277))
				(in r2276)
			)
	)

	(:action move-right-from-r2278
		:precondition
			(and 
				(in r2278)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2278))
				(in r2279)
			)
	)


	(:action move-left-from-r2278
		:precondition
			(and 
				(in r2278)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2278))
				(in r2277)
			)
	)

	(:action move-right-from-r2279
		:precondition
			(and 
				(in r2279)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2279))
				(in r2280)
			)
	)


	(:action move-left-from-r2279
		:precondition
			(and 
				(in r2279)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2279))
				(in r2278)
			)
	)

	(:action move-right-from-r2280
		:precondition
			(and 
				(in r2280)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2280))
				(in r2281)
			)
	)


	(:action move-left-from-r2280
		:precondition
			(and 
				(in r2280)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2280))
				(in r2279)
			)
	)

	(:action move-right-from-r2281
		:precondition
			(and 
				(in r2281)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2281))
				(in r2282)
			)
	)


	(:action move-left-from-r2281
		:precondition
			(and 
				(in r2281)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2281))
				(in r2280)
			)
	)

	(:action move-right-from-r2282
		:precondition
			(and 
				(in r2282)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2282))
				(in r2283)
			)
	)


	(:action move-left-from-r2282
		:precondition
			(and 
				(in r2282)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2282))
				(in r2281)
			)
	)

	(:action move-right-from-r2283
		:precondition
			(and 
				(in r2283)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2283))
				(in r2284)
			)
	)


	(:action move-left-from-r2283
		:precondition
			(and 
				(in r2283)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2283))
				(in r2282)
			)
	)

	(:action move-right-from-r2284
		:precondition
			(and 
				(in r2284)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2284))
				(in r2285)
			)
	)


	(:action move-left-from-r2284
		:precondition
			(and 
				(in r2284)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2284))
				(in r2283)
			)
	)

	(:action move-right-from-r2285
		:precondition
			(and 
				(in r2285)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2285))
				(in r2286)
			)
	)


	(:action move-left-from-r2285
		:precondition
			(and 
				(in r2285)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2285))
				(in r2284)
			)
	)

	(:action move-right-from-r2286
		:precondition
			(and 
				(in r2286)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2286))
				(in r2287)
			)
	)


	(:action move-left-from-r2286
		:precondition
			(and 
				(in r2286)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2286))
				(in r2285)
			)
	)

	(:action move-right-from-r2287
		:precondition
			(and 
				(in r2287)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2287))
				(in r2288)
			)
	)


	(:action move-left-from-r2287
		:precondition
			(and 
				(in r2287)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2287))
				(in r2286)
			)
	)

	(:action move-right-from-r2288
		:precondition
			(and 
				(in r2288)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2288))
				(in r2289)
			)
	)


	(:action move-left-from-r2288
		:precondition
			(and 
				(in r2288)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2288))
				(in r2287)
			)
	)

	(:action move-right-from-r2289
		:precondition
			(and 
				(in r2289)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2289))
				(in r2290)
			)
	)


	(:action move-left-from-r2289
		:precondition
			(and 
				(in r2289)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2289))
				(in r2288)
			)
	)

	(:action move-right-from-r2290
		:precondition
			(and 
				(in r2290)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2290))
				(in r2291)
			)
	)


	(:action move-left-from-r2290
		:precondition
			(and 
				(in r2290)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2290))
				(in r2289)
			)
	)

	(:action move-right-from-r2291
		:precondition
			(and 
				(in r2291)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2291))
				(in r2292)
			)
	)


	(:action move-left-from-r2291
		:precondition
			(and 
				(in r2291)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2291))
				(in r2290)
			)
	)

	(:action move-right-from-r2292
		:precondition
			(and 
				(in r2292)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2292))
				(in r2293)
			)
	)


	(:action move-left-from-r2292
		:precondition
			(and 
				(in r2292)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2292))
				(in r2291)
			)
	)

	(:action move-right-from-r2293
		:precondition
			(and 
				(in r2293)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2293))
				(in r2294)
			)
	)


	(:action move-left-from-r2293
		:precondition
			(and 
				(in r2293)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2293))
				(in r2292)
			)
	)

	(:action move-right-from-r2294
		:precondition
			(and 
				(in r2294)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2294))
				(in r2295)
			)
	)


	(:action move-left-from-r2294
		:precondition
			(and 
				(in r2294)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2294))
				(in r2293)
			)
	)

	(:action move-right-from-r2295
		:precondition
			(and 
				(in r2295)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2295))
				(in r2296)
			)
	)


	(:action move-left-from-r2295
		:precondition
			(and 
				(in r2295)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2295))
				(in r2294)
			)
	)

	(:action move-right-from-r2296
		:precondition
			(and 
				(in r2296)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2296))
				(in r2297)
			)
	)


	(:action move-left-from-r2296
		:precondition
			(and 
				(in r2296)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2296))
				(in r2295)
			)
	)

	(:action move-right-from-r2297
		:precondition
			(and 
				(in r2297)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2297))
				(in r2298)
			)
	)


	(:action move-left-from-r2297
		:precondition
			(and 
				(in r2297)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2297))
				(in r2296)
			)
	)

	(:action move-right-from-r2298
		:precondition
			(and 
				(in r2298)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2298))
				(in r2299)
			)
	)


	(:action move-left-from-r2298
		:precondition
			(and 
				(in r2298)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2298))
				(in r2297)
			)
	)

	(:action move-right-from-r2299
		:precondition
			(and 
				(in r2299)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2299))
				(in r2300)
			)
	)


	(:action move-left-from-r2299
		:precondition
			(and 
				(in r2299)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2299))
				(in r2298)
			)
	)

	(:action move-right-from-r2300
		:precondition
			(and 
				(in r2300)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2300))
				(in r2301)
			)
	)


	(:action move-left-from-r2300
		:precondition
			(and 
				(in r2300)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2300))
				(in r2299)
			)
	)

	(:action move-right-from-r2301
		:precondition
			(and 
				(in r2301)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2301))
				(in r2302)
			)
	)


	(:action move-left-from-r2301
		:precondition
			(and 
				(in r2301)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2301))
				(in r2300)
			)
	)

	(:action move-right-from-r2302
		:precondition
			(and 
				(in r2302)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2302))
				(in r2303)
			)
	)


	(:action move-left-from-r2302
		:precondition
			(and 
				(in r2302)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2302))
				(in r2301)
			)
	)

	(:action move-right-from-r2303
		:precondition
			(and 
				(in r2303)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2303))
				(in r2304)
			)
	)


	(:action move-left-from-r2303
		:precondition
			(and 
				(in r2303)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2303))
				(in r2302)
			)
	)

	(:action move-right-from-r2304
		:precondition
			(and 
				(in r2304)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2304))
				(in r2305)
			)
	)


	(:action move-left-from-r2304
		:precondition
			(and 
				(in r2304)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2304))
				(in r2303)
			)
	)

	(:action move-right-from-r2305
		:precondition
			(and 
				(in r2305)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2305))
				(in r2306)
			)
	)


	(:action move-left-from-r2305
		:precondition
			(and 
				(in r2305)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2305))
				(in r2304)
			)
	)

	(:action move-right-from-r2306
		:precondition
			(and 
				(in r2306)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2306))
				(in r2307)
			)
	)


	(:action move-left-from-r2306
		:precondition
			(and 
				(in r2306)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2306))
				(in r2305)
			)
	)

	(:action move-right-from-r2307
		:precondition
			(and 
				(in r2307)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2307))
				(in r2308)
			)
	)


	(:action move-left-from-r2307
		:precondition
			(and 
				(in r2307)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2307))
				(in r2306)
			)
	)

	(:action move-right-from-r2308
		:precondition
			(and 
				(in r2308)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2308))
				(in r2309)
			)
	)


	(:action move-left-from-r2308
		:precondition
			(and 
				(in r2308)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2308))
				(in r2307)
			)
	)

	(:action move-right-from-r2309
		:precondition
			(and 
				(in r2309)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2309))
				(in r2310)
			)
	)


	(:action move-left-from-r2309
		:precondition
			(and 
				(in r2309)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2309))
				(in r2308)
			)
	)

	(:action move-right-from-r2310
		:precondition
			(and 
				(in r2310)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2310))
				(in r2311)
			)
	)


	(:action move-left-from-r2310
		:precondition
			(and 
				(in r2310)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2310))
				(in r2309)
			)
	)

	(:action move-right-from-r2311
		:precondition
			(and 
				(in r2311)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2311))
				(in r2312)
			)
	)


	(:action move-left-from-r2311
		:precondition
			(and 
				(in r2311)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2311))
				(in r2310)
			)
	)

	(:action move-right-from-r2312
		:precondition
			(and 
				(in r2312)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2312))
				(in r2313)
			)
	)


	(:action move-left-from-r2312
		:precondition
			(and 
				(in r2312)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2312))
				(in r2311)
			)
	)

	(:action move-right-from-r2313
		:precondition
			(and 
				(in r2313)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2313))
				(in r2314)
			)
	)


	(:action move-left-from-r2313
		:precondition
			(and 
				(in r2313)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2313))
				(in r2312)
			)
	)

	(:action move-right-from-r2314
		:precondition
			(and 
				(in r2314)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2314))
				(in r2315)
			)
	)


	(:action move-left-from-r2314
		:precondition
			(and 
				(in r2314)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2314))
				(in r2313)
			)
	)

	(:action move-right-from-r2315
		:precondition
			(and 
				(in r2315)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2315))
				(in r2316)
			)
	)


	(:action move-left-from-r2315
		:precondition
			(and 
				(in r2315)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2315))
				(in r2314)
			)
	)

	(:action move-right-from-r2316
		:precondition
			(and 
				(in r2316)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2316))
				(in r2317)
			)
	)


	(:action move-left-from-r2316
		:precondition
			(and 
				(in r2316)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2316))
				(in r2315)
			)
	)

	(:action move-right-from-r2317
		:precondition
			(and 
				(in r2317)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2317))
				(in r2318)
			)
	)


	(:action move-left-from-r2317
		:precondition
			(and 
				(in r2317)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2317))
				(in r2316)
			)
	)

	(:action move-right-from-r2318
		:precondition
			(and 
				(in r2318)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2318))
				(in r2319)
			)
	)


	(:action move-left-from-r2318
		:precondition
			(and 
				(in r2318)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2318))
				(in r2317)
			)
	)

	(:action move-right-from-r2319
		:precondition
			(and 
				(in r2319)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2319))
				(in r2320)
			)
	)


	(:action move-left-from-r2319
		:precondition
			(and 
				(in r2319)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2319))
				(in r2318)
			)
	)

	(:action move-right-from-r2320
		:precondition
			(and 
				(in r2320)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2320))
				(in r2321)
			)
	)


	(:action move-left-from-r2320
		:precondition
			(and 
				(in r2320)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2320))
				(in r2319)
			)
	)

	(:action move-right-from-r2321
		:precondition
			(and 
				(in r2321)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2321))
				(in r2322)
			)
	)


	(:action move-left-from-r2321
		:precondition
			(and 
				(in r2321)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2321))
				(in r2320)
			)
	)

	(:action move-right-from-r2322
		:precondition
			(and 
				(in r2322)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2322))
				(in r2323)
			)
	)


	(:action move-left-from-r2322
		:precondition
			(and 
				(in r2322)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2322))
				(in r2321)
			)
	)

	(:action move-right-from-r2323
		:precondition
			(and 
				(in r2323)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2323))
				(in r2324)
			)
	)


	(:action move-left-from-r2323
		:precondition
			(and 
				(in r2323)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2323))
				(in r2322)
			)
	)

	(:action move-right-from-r2324
		:precondition
			(and 
				(in r2324)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2324))
				(in r2325)
			)
	)


	(:action move-left-from-r2324
		:precondition
			(and 
				(in r2324)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2324))
				(in r2323)
			)
	)

	(:action move-right-from-r2325
		:precondition
			(and 
				(in r2325)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2325))
				(in r2326)
			)
	)


	(:action move-left-from-r2325
		:precondition
			(and 
				(in r2325)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2325))
				(in r2324)
			)
	)

	(:action move-right-from-r2326
		:precondition
			(and 
				(in r2326)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2326))
				(in r2327)
			)
	)


	(:action move-left-from-r2326
		:precondition
			(and 
				(in r2326)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2326))
				(in r2325)
			)
	)

	(:action move-right-from-r2327
		:precondition
			(and 
				(in r2327)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2327))
				(in r2328)
			)
	)


	(:action move-left-from-r2327
		:precondition
			(and 
				(in r2327)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2327))
				(in r2326)
			)
	)

	(:action move-right-from-r2328
		:precondition
			(and 
				(in r2328)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2328))
				(in r2329)
			)
	)


	(:action move-left-from-r2328
		:precondition
			(and 
				(in r2328)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2328))
				(in r2327)
			)
	)

	(:action move-right-from-r2329
		:precondition
			(and 
				(in r2329)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2329))
				(in r2330)
			)
	)


	(:action move-left-from-r2329
		:precondition
			(and 
				(in r2329)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2329))
				(in r2328)
			)
	)

	(:action move-right-from-r2330
		:precondition
			(and 
				(in r2330)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2330))
				(in r2331)
			)
	)


	(:action move-left-from-r2330
		:precondition
			(and 
				(in r2330)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2330))
				(in r2329)
			)
	)

	(:action move-right-from-r2331
		:precondition
			(and 
				(in r2331)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2331))
				(in r2332)
			)
	)


	(:action move-left-from-r2331
		:precondition
			(and 
				(in r2331)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2331))
				(in r2330)
			)
	)

	(:action move-right-from-r2332
		:precondition
			(and 
				(in r2332)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2332))
				(in r2333)
			)
	)


	(:action move-left-from-r2332
		:precondition
			(and 
				(in r2332)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2332))
				(in r2331)
			)
	)

	(:action move-right-from-r2333
		:precondition
			(and 
				(in r2333)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2333))
				(in r2334)
			)
	)


	(:action move-left-from-r2333
		:precondition
			(and 
				(in r2333)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2333))
				(in r2332)
			)
	)

	(:action move-right-from-r2334
		:precondition
			(and 
				(in r2334)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2334))
				(in r2335)
			)
	)


	(:action move-left-from-r2334
		:precondition
			(and 
				(in r2334)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2334))
				(in r2333)
			)
	)

	(:action move-right-from-r2335
		:precondition
			(and 
				(in r2335)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2335))
				(in r2336)
			)
	)


	(:action move-left-from-r2335
		:precondition
			(and 
				(in r2335)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2335))
				(in r2334)
			)
	)

	(:action move-right-from-r2336
		:precondition
			(and 
				(in r2336)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2336))
				(in r2337)
			)
	)


	(:action move-left-from-r2336
		:precondition
			(and 
				(in r2336)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2336))
				(in r2335)
			)
	)

	(:action move-right-from-r2337
		:precondition
			(and 
				(in r2337)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2337))
				(in r2338)
			)
	)


	(:action move-left-from-r2337
		:precondition
			(and 
				(in r2337)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2337))
				(in r2336)
			)
	)

	(:action move-right-from-r2338
		:precondition
			(and 
				(in r2338)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2338))
				(in r2339)
			)
	)


	(:action move-left-from-r2338
		:precondition
			(and 
				(in r2338)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2338))
				(in r2337)
			)
	)

	(:action move-right-from-r2339
		:precondition
			(and 
				(in r2339)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2339))
				(in r2340)
			)
	)


	(:action move-left-from-r2339
		:precondition
			(and 
				(in r2339)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2339))
				(in r2338)
			)
	)

	(:action move-right-from-r2340
		:precondition
			(and 
				(in r2340)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2340))
				(in r2341)
			)
	)


	(:action move-left-from-r2340
		:precondition
			(and 
				(in r2340)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2340))
				(in r2339)
			)
	)

	(:action move-right-from-r2341
		:precondition
			(and 
				(in r2341)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2341))
				(in r2342)
			)
	)


	(:action move-left-from-r2341
		:precondition
			(and 
				(in r2341)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2341))
				(in r2340)
			)
	)

	(:action move-right-from-r2342
		:precondition
			(and 
				(in r2342)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2342))
				(in r2343)
			)
	)


	(:action move-left-from-r2342
		:precondition
			(and 
				(in r2342)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2342))
				(in r2341)
			)
	)

	(:action move-right-from-r2343
		:precondition
			(and 
				(in r2343)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2343))
				(in r2344)
			)
	)


	(:action move-left-from-r2343
		:precondition
			(and 
				(in r2343)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2343))
				(in r2342)
			)
	)

	(:action move-right-from-r2344
		:precondition
			(and 
				(in r2344)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2344))
				(in r2345)
			)
	)


	(:action move-left-from-r2344
		:precondition
			(and 
				(in r2344)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2344))
				(in r2343)
			)
	)

	(:action move-right-from-r2345
		:precondition
			(and 
				(in r2345)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2345))
				(in r2346)
			)
	)


	(:action move-left-from-r2345
		:precondition
			(and 
				(in r2345)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2345))
				(in r2344)
			)
	)

	(:action move-right-from-r2346
		:precondition
			(and 
				(in r2346)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2346))
				(in r2347)
			)
	)


	(:action move-left-from-r2346
		:precondition
			(and 
				(in r2346)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2346))
				(in r2345)
			)
	)

	(:action move-right-from-r2347
		:precondition
			(and 
				(in r2347)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2347))
				(in r2348)
			)
	)


	(:action move-left-from-r2347
		:precondition
			(and 
				(in r2347)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2347))
				(in r2346)
			)
	)

	(:action move-right-from-r2348
		:precondition
			(and 
				(in r2348)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2348))
				(in r2349)
			)
	)


	(:action move-left-from-r2348
		:precondition
			(and 
				(in r2348)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2348))
				(in r2347)
			)
	)

	(:action move-right-from-r2349
		:precondition
			(and 
				(in r2349)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2349))
				(in r2350)
			)
	)


	(:action move-left-from-r2349
		:precondition
			(and 
				(in r2349)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2349))
				(in r2348)
			)
	)

	(:action move-right-from-r2350
		:precondition
			(and 
				(in r2350)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2350))
				(in r2351)
			)
	)


	(:action move-left-from-r2350
		:precondition
			(and 
				(in r2350)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2350))
				(in r2349)
			)
	)

	(:action move-right-from-r2351
		:precondition
			(and 
				(in r2351)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2351))
				(in r2352)
			)
	)


	(:action move-left-from-r2351
		:precondition
			(and 
				(in r2351)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2351))
				(in r2350)
			)
	)

	(:action move-right-from-r2352
		:precondition
			(and 
				(in r2352)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2352))
				(in r2353)
			)
	)


	(:action move-left-from-r2352
		:precondition
			(and 
				(in r2352)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2352))
				(in r2351)
			)
	)

	(:action move-right-from-r2353
		:precondition
			(and 
				(in r2353)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2353))
				(in r2354)
			)
	)


	(:action move-left-from-r2353
		:precondition
			(and 
				(in r2353)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2353))
				(in r2352)
			)
	)

	(:action move-right-from-r2354
		:precondition
			(and 
				(in r2354)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2354))
				(in r2355)
			)
	)


	(:action move-left-from-r2354
		:precondition
			(and 
				(in r2354)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2354))
				(in r2353)
			)
	)

	(:action move-right-from-r2355
		:precondition
			(and 
				(in r2355)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2355))
				(in r2356)
			)
	)


	(:action move-left-from-r2355
		:precondition
			(and 
				(in r2355)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2355))
				(in r2354)
			)
	)

	(:action move-right-from-r2356
		:precondition
			(and 
				(in r2356)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2356))
				(in r2357)
			)
	)


	(:action move-left-from-r2356
		:precondition
			(and 
				(in r2356)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2356))
				(in r2355)
			)
	)

	(:action move-right-from-r2357
		:precondition
			(and 
				(in r2357)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2357))
				(in r2358)
			)
	)


	(:action move-left-from-r2357
		:precondition
			(and 
				(in r2357)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2357))
				(in r2356)
			)
	)

	(:action move-right-from-r2358
		:precondition
			(and 
				(in r2358)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2358))
				(in r2359)
			)
	)


	(:action move-left-from-r2358
		:precondition
			(and 
				(in r2358)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2358))
				(in r2357)
			)
	)

	(:action move-right-from-r2359
		:precondition
			(and 
				(in r2359)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2359))
				(in r2360)
			)
	)


	(:action move-left-from-r2359
		:precondition
			(and 
				(in r2359)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2359))
				(in r2358)
			)
	)

	(:action move-right-from-r2360
		:precondition
			(and 
				(in r2360)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2360))
				(in r2361)
			)
	)


	(:action move-left-from-r2360
		:precondition
			(and 
				(in r2360)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2360))
				(in r2359)
			)
	)

	(:action move-right-from-r2361
		:precondition
			(and 
				(in r2361)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2361))
				(in r2362)
			)
	)


	(:action move-left-from-r2361
		:precondition
			(and 
				(in r2361)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2361))
				(in r2360)
			)
	)

	(:action move-right-from-r2362
		:precondition
			(and 
				(in r2362)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2362))
				(in r2363)
			)
	)


	(:action move-left-from-r2362
		:precondition
			(and 
				(in r2362)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2362))
				(in r2361)
			)
	)

	(:action move-right-from-r2363
		:precondition
			(and 
				(in r2363)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2363))
				(in r2364)
			)
	)


	(:action move-left-from-r2363
		:precondition
			(and 
				(in r2363)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2363))
				(in r2362)
			)
	)

	(:action move-right-from-r2364
		:precondition
			(and 
				(in r2364)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2364))
				(in r2365)
			)
	)


	(:action move-left-from-r2364
		:precondition
			(and 
				(in r2364)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2364))
				(in r2363)
			)
	)

	(:action move-right-from-r2365
		:precondition
			(and 
				(in r2365)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2365))
				(in r2366)
			)
	)


	(:action move-left-from-r2365
		:precondition
			(and 
				(in r2365)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2365))
				(in r2364)
			)
	)

	(:action move-right-from-r2366
		:precondition
			(and 
				(in r2366)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2366))
				(in r2367)
			)
	)


	(:action move-left-from-r2366
		:precondition
			(and 
				(in r2366)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2366))
				(in r2365)
			)
	)

	(:action move-right-from-r2367
		:precondition
			(and 
				(in r2367)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2367))
				(in r2368)
			)
	)


	(:action move-left-from-r2367
		:precondition
			(and 
				(in r2367)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2367))
				(in r2366)
			)
	)

	(:action move-right-from-r2368
		:precondition
			(and 
				(in r2368)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2368))
				(in r2369)
			)
	)


	(:action move-left-from-r2368
		:precondition
			(and 
				(in r2368)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2368))
				(in r2367)
			)
	)

	(:action move-right-from-r2369
		:precondition
			(and 
				(in r2369)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2369))
				(in r2370)
			)
	)


	(:action move-left-from-r2369
		:precondition
			(and 
				(in r2369)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2369))
				(in r2368)
			)
	)

	(:action move-right-from-r2370
		:precondition
			(and 
				(in r2370)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2370))
				(in r2371)
			)
	)


	(:action move-left-from-r2370
		:precondition
			(and 
				(in r2370)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2370))
				(in r2369)
			)
	)

	(:action move-right-from-r2371
		:precondition
			(and 
				(in r2371)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2371))
				(in r2372)
			)
	)


	(:action move-left-from-r2371
		:precondition
			(and 
				(in r2371)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2371))
				(in r2370)
			)
	)

	(:action move-right-from-r2372
		:precondition
			(and 
				(in r2372)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2372))
				(in r2373)
			)
	)


	(:action move-left-from-r2372
		:precondition
			(and 
				(in r2372)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2372))
				(in r2371)
			)
	)

	(:action move-right-from-r2373
		:precondition
			(and 
				(in r2373)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2373))
				(in r2374)
			)
	)


	(:action move-left-from-r2373
		:precondition
			(and 
				(in r2373)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2373))
				(in r2372)
			)
	)

	(:action move-right-from-r2374
		:precondition
			(and 
				(in r2374)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2374))
				(in r2375)
			)
	)


	(:action move-left-from-r2374
		:precondition
			(and 
				(in r2374)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2374))
				(in r2373)
			)
	)

	(:action move-right-from-r2375
		:precondition
			(and 
				(in r2375)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2375))
				(in r2376)
			)
	)


	(:action move-left-from-r2375
		:precondition
			(and 
				(in r2375)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2375))
				(in r2374)
			)
	)

	(:action move-right-from-r2376
		:precondition
			(and 
				(in r2376)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2376))
				(in r2377)
			)
	)


	(:action move-left-from-r2376
		:precondition
			(and 
				(in r2376)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2376))
				(in r2375)
			)
	)

	(:action move-right-from-r2377
		:precondition
			(and 
				(in r2377)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2377))
				(in r2378)
			)
	)


	(:action move-left-from-r2377
		:precondition
			(and 
				(in r2377)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2377))
				(in r2376)
			)
	)

	(:action move-right-from-r2378
		:precondition
			(and 
				(in r2378)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2378))
				(in r2379)
			)
	)


	(:action move-left-from-r2378
		:precondition
			(and 
				(in r2378)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2378))
				(in r2377)
			)
	)

	(:action move-right-from-r2379
		:precondition
			(and 
				(in r2379)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2379))
				(in r2380)
			)
	)


	(:action move-left-from-r2379
		:precondition
			(and 
				(in r2379)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2379))
				(in r2378)
			)
	)

	(:action move-right-from-r2380
		:precondition
			(and 
				(in r2380)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2380))
				(in r2381)
			)
	)


	(:action move-left-from-r2380
		:precondition
			(and 
				(in r2380)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2380))
				(in r2379)
			)
	)

	(:action move-right-from-r2381
		:precondition
			(and 
				(in r2381)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2381))
				(in r2382)
			)
	)


	(:action move-left-from-r2381
		:precondition
			(and 
				(in r2381)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2381))
				(in r2380)
			)
	)

	(:action move-right-from-r2382
		:precondition
			(and 
				(in r2382)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2382))
				(in r2383)
			)
	)


	(:action move-left-from-r2382
		:precondition
			(and 
				(in r2382)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2382))
				(in r2381)
			)
	)

	(:action move-right-from-r2383
		:precondition
			(and 
				(in r2383)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2383))
				(in r2384)
			)
	)


	(:action move-left-from-r2383
		:precondition
			(and 
				(in r2383)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2383))
				(in r2382)
			)
	)

	(:action move-right-from-r2384
		:precondition
			(and 
				(in r2384)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2384))
				(in r2385)
			)
	)


	(:action move-left-from-r2384
		:precondition
			(and 
				(in r2384)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2384))
				(in r2383)
			)
	)

	(:action move-right-from-r2385
		:precondition
			(and 
				(in r2385)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2385))
				(in r2386)
			)
	)


	(:action move-left-from-r2385
		:precondition
			(and 
				(in r2385)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2385))
				(in r2384)
			)
	)

	(:action move-right-from-r2386
		:precondition
			(and 
				(in r2386)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2386))
				(in r2387)
			)
	)


	(:action move-left-from-r2386
		:precondition
			(and 
				(in r2386)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2386))
				(in r2385)
			)
	)

	(:action move-right-from-r2387
		:precondition
			(and 
				(in r2387)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2387))
				(in r2388)
			)
	)


	(:action move-left-from-r2387
		:precondition
			(and 
				(in r2387)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2387))
				(in r2386)
			)
	)

	(:action move-right-from-r2388
		:precondition
			(and 
				(in r2388)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2388))
				(in r2389)
			)
	)


	(:action move-left-from-r2388
		:precondition
			(and 
				(in r2388)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2388))
				(in r2387)
			)
	)

	(:action move-right-from-r2389
		:precondition
			(and 
				(in r2389)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2389))
				(in r2390)
			)
	)


	(:action move-left-from-r2389
		:precondition
			(and 
				(in r2389)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2389))
				(in r2388)
			)
	)

	(:action move-right-from-r2390
		:precondition
			(and 
				(in r2390)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2390))
				(in r2391)
			)
	)


	(:action move-left-from-r2390
		:precondition
			(and 
				(in r2390)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2390))
				(in r2389)
			)
	)

	(:action move-right-from-r2391
		:precondition
			(and 
				(in r2391)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2391))
				(in r2392)
			)
	)


	(:action move-left-from-r2391
		:precondition
			(and 
				(in r2391)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2391))
				(in r2390)
			)
	)

	(:action move-right-from-r2392
		:precondition
			(and 
				(in r2392)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2392))
				(in r2393)
			)
	)


	(:action move-left-from-r2392
		:precondition
			(and 
				(in r2392)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2392))
				(in r2391)
			)
	)

	(:action move-right-from-r2393
		:precondition
			(and 
				(in r2393)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2393))
				(in r2394)
			)
	)


	(:action move-left-from-r2393
		:precondition
			(and 
				(in r2393)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2393))
				(in r2392)
			)
	)

	(:action move-right-from-r2394
		:precondition
			(and 
				(in r2394)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2394))
				(in r2395)
			)
	)


	(:action move-left-from-r2394
		:precondition
			(and 
				(in r2394)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2394))
				(in r2393)
			)
	)

	(:action move-right-from-r2395
		:precondition
			(and 
				(in r2395)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2395))
				(in r2396)
			)
	)


	(:action move-left-from-r2395
		:precondition
			(and 
				(in r2395)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2395))
				(in r2394)
			)
	)

	(:action move-right-from-r2396
		:precondition
			(and 
				(in r2396)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2396))
				(in r2397)
			)
	)


	(:action move-left-from-r2396
		:precondition
			(and 
				(in r2396)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2396))
				(in r2395)
			)
	)

	(:action move-right-from-r2397
		:precondition
			(and 
				(in r2397)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2397))
				(in r2398)
			)
	)


	(:action move-left-from-r2397
		:precondition
			(and 
				(in r2397)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2397))
				(in r2396)
			)
	)

	(:action move-right-from-r2398
		:precondition
			(and 
				(in r2398)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2398))
				(in r2399)
			)
	)


	(:action move-left-from-r2398
		:precondition
			(and 
				(in r2398)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2398))
				(in r2397)
			)
	)

	(:action move-right-from-r2399
		:precondition
			(and 
				(in r2399)
				(not (searched_in r2400))
			)
		:effect
			(and
				(not (search_again))
				(not (in r2399))
				(in r2400)
				(oneof (not (seen)) (seen))
				(searched_in r2400)
			)
	)


	(:action move-left-from-r2399
		:precondition
			(and 
				(in r2399)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2399))
				(in r2398)
			)
	)

	(:action move-right-from-r2400
		:precondition
			(and 
				(in r2400)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r2400))
				(in r1)
			)
	)


	(:action move-left-from-r2400
		:precondition
			(and 
				(in r2400)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r2400))
				(in r2399)
			)
	)


	(:action stay
		:precondition
			(f_ok 
			)
		:effect
			(and
			)
	)


	(:action searching_again
		:precondition
			(and 
				(searched_in r1200)
				(searched_in r2400)
				(not (seen))
			)
		:effect
			(and
				(not (searched_in r1200))
				(not (searched_in r2400))
				(search_again)
			)
	)

)

