
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Store example for 3 packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Goal formula:
;;
;; A( (pkg_requested) OR (pkg_arrived) --> E (pkg_served) OR E (pkg_stored ) )
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [](  active_request -> ( <> pkg_stored | <> pkg_served) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain packages)
	(:requirements :strips :typing :equality)
	(:types 
		pkg
		loc
		baState ;;BAencoding
	)
	(:constants
		p1 p2 p3 - pkg
		l1 l2 l3 desk - loc
		BA-S0 BA-S1  - baState ;;BAencoding
	)
	(:predicates
		(in_store ?p  - pkg)
		(want ?p  - pkg)
		(active_request)
		(pkg_served)
		(pkg_requested)
		(pkg_arrived)
		(pkg_stored)
		(holding ?p - pkg)
		(pkg_at ?l - loc)
		(robot_at ?l - loc)
		(move) ;; store's turn
		(currentBAstate ?s  - baState) ;;BAencoding
		(ok) ;;BAencoding
	)

	(:action serve-p1
		:precondition
			(and 
				(not (pkg_served))
				(move)
				(in_store p1)
				(want p1)
				(holding p1)
				(robot_at desk)
			)
		:effect
			(and
				(not (move))
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p1))
				(not (want p1))
				(not (holding p1))
			)
	)


	(:action pickup-p1
		:precondition
			(and 
				(not (pkg_served))
				(move)
				(in_store p1)
				(want p1)
				(robot_at l1)
			)
		:effect
			(and
				(not (pkg_at l1))
				(holding p1)
			)
	)


	(:action putdown-p1
		:precondition
			(and 
				(pkg_arrived)
				(move)
				(holding p1)
				(robot_at l1)
			)
		:effect
			(and
				(in_store p1)
				(pkg_at l1)
				(not (holding p1))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
				(not (move))
			)
	)


	(:action restock-p1
		:precondition
			(and 
				(not (pkg_served))
				(move)
				(not (in_store p1))
				(want p1)
			)
		:effect
			(and
				(in_store p1)
			)
	)


	(:action serve-p2
		:precondition
			(and 
				(not (pkg_served))
				(move)
				(in_store p2)
				(want p2)
				(holding p2)
				(robot_at desk)
			)
		:effect
			(and
				(not (move))
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p2))
				(not (want p2))
				(not (holding p2))
			)
	)


	(:action pickup-p2
		:precondition
			(and 
				(not (pkg_served))
				(move)
				(in_store p2)
				(want p2)
				(robot_at l2)
			)
		:effect
			(and
				(not (pkg_at l2))
				(holding p2)
			)
	)


	(:action putdown-p2
		:precondition
			(and 
				(pkg_arrived)
				(move)
				(holding p2)
				(robot_at l2)
			)
		:effect
			(and
				(in_store p2)
				(pkg_at l2)
				(not (holding p2))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
				(not (move))
			)
	)


	(:action restock-p2
		:precondition
			(and 
				(not (pkg_served))
				(move)
				(not (in_store p2))
				(want p2)
			)
		:effect
			(and
				(in_store p2)
			)
	)


	(:action serve-p3
		:precondition
			(and 
				(not (pkg_served))
				(move)
				(in_store p3)
				(want p3)
				(holding p3)
				(robot_at desk)
			)
		:effect
			(and
				(not (move))
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p3))
				(not (want p3))
				(not (holding p3))
			)
	)


	(:action pickup-p3
		:precondition
			(and 
				(not (pkg_served))
				(move)
				(in_store p3)
				(want p3)
				(robot_at l3)
			)
		:effect
			(and
				(not (pkg_at l3))
				(holding p3)
			)
	)


	(:action putdown-p3
		:precondition
			(and 
				(pkg_arrived)
				(move)
				(holding p3)
				(robot_at l3)
			)
		:effect
			(and
				(in_store p3)
				(pkg_at l3)
				(not (holding p3))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
				(not (move))
			)
	)


	(:action restock-p3
		:precondition
			(and 
				(not (pkg_served))
				(move)
				(not (in_store p3))
				(want p3)
			)
		:effect
			(and
				(in_store p3)
			)
	)


	(:action move1_2
		:precondition
			(and 
				(robot_at l1)
				(move)
			)
		:effect
			(and
				(robot_at l2)
				(not (robot_at l1))
			)
	)


	(:action move2_1
		:precondition
			(and 
				(robot_at l2)
				(move)
			)
		:effect
			(and
				(robot_at l1)
				(not (robot_at l2))
			)
	)


	(:action move2_3
		:precondition
			(and 
				(robot_at l2)
				(move)
			)
		:effect
			(and
				(robot_at l3)
				(not (robot_at l2))
			)
	)


	(:action move3_2
		:precondition
			(and 
				(robot_at l3)
				(move)
			)
		:effect
			(and
				(robot_at l2)
				(not (robot_at l3))
			)
	)


	(:action movedesk_l1
		:precondition
			(and 
				(robot_at desk)
				(move)
			)
		:effect
			(and
				(robot_at l1)
				(not (robot_at desk))
			)
	)


	(:action movel1_desk
		:precondition
			(and 
				(robot_at l1)
				(move)
			)
		:effect
			(and
				(robot_at desk)
				(not (robot_at l1))
			)
	)


	(:action request
		:precondition
			(and 
				(robot_at desk)
				(not (active_request))
				(move)
			)
		:effect
			(and
				(not (move))
				(not (pkg_served))
				(not (pkg_stored))
				(active_request)
				(oneof
					(and
						(pkg_requested)
						(want p1)
					)
					(and
						(pkg_requested)
						(want p2)
					)
					(and
						(pkg_requested)
						(want p3)
					)
					(and
						(pkg_arrived)
						(holding p1)
					)
					(and
						(pkg_arrived)
						(holding p2)
					)
					(and
						(pkg_arrived)
						(holding p3)
					)
				)
			)
	)


	(:action buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(not (move))
				(currentBAstate BA-S0)
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(move)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(not (move))
				(currentBAstate BA-S0)
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(move)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(not (move))
				(currentBAstate BA-S0)
				(pkg_stored) 
			)
		:effect
			(and
				(move)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-1_3
		:precondition
			(and
				(not (move))
				(currentBAstate BA-S0)
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(move)
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S1-0_4
		:precondition
			(and
				(not (move))
				(currentBAstate BA-S1)
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(move)
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(not (move))
				(currentBAstate BA-S1)
				(pkg_stored) 
			)
		:effect
			(and
				(move)
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-1_6
		:precondition
			(and
				(not (move))
				(currentBAstate BA-S1)
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(move)
			)
	)
)

