
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Store example for 16 packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Goal formula:
;;
;; A( (pkg_requested) OR (pkg_arrived) --> E (pkg_served) OR E (pkg_stored ) )
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [](  active_request -> ( <> pkg_stored | <> pkg_served) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain packages)
	(:requirements :strips :typing :equality)
	(:types 
		pkg
		loc
		baState ;;BAencoding
	)
	(:constants
		p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 - pkg
		l1 l2 l3 l4 l5 l6 l7 l8 l9 l10 l11 l12 l13 l14 l15 l16 desk - loc
		BA-S0 BA-S1  - baState ;;BAencoding
	)
	(:predicates
		(in_store ?p  - pkg)
		(want ?p  - pkg)
		(active_request)
		(pkg_served)
		(pkg_requested)
		(pkg_arrived)
		(pkg_stored)
		(holding ?p - pkg)
		(pkg_at ?l - loc)
		(robot_at ?l - loc)
		(move) ;; store's turn
		(currentBAstate ?s  - baState) ;;BAencoding
		(ok) ;;BAencoding
	)


































































































	(:action serve-p1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p1) 
				(not (pkg_at l1)) 
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p1) 
			)
	)
	(:action serve-p2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p2) 
				(not (pkg_at l2)) 
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p2) 
			)
	)
	(:action serve-p3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p3) 
				(not (pkg_at l3)) 
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p3) 
			)
	)
	(:action serve-p4_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p4) 
				(not (pkg_at l4)) 
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p4_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p4) 
			)
	)
	(:action serve-p5_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(holding p5) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p5)) 
				(not (want p5)) 
				(not (holding p5)) 
			)
	)
	(:action pickup-p5_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p5) 
				(not (pkg_at l5)) 
			)
	)
	(:action putdown-p5_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p5) 
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l5) 
				(pkg_stored) 
				(in_store p5) 
				(not (holding p5)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p5_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p5)) 
				(want p5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p5) 
			)
	)
	(:action serve-p6_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(holding p6) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p6)) 
				(not (want p6)) 
				(not (holding p6)) 
			)
	)
	(:action pickup-p6_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p6) 
				(not (pkg_at l6)) 
			)
	)
	(:action putdown-p6_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p6) 
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l6) 
				(pkg_stored) 
				(in_store p6) 
				(not (holding p6)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p6_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p6)) 
				(want p6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p6) 
			)
	)
	(:action serve-p7_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(holding p7) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p7)) 
				(not (want p7)) 
				(not (holding p7)) 
			)
	)
	(:action pickup-p7_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p7) 
				(not (pkg_at l7)) 
			)
	)
	(:action putdown-p7_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p7) 
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l7) 
				(pkg_stored) 
				(in_store p7) 
				(not (holding p7)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p7_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p7)) 
				(want p7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p7) 
			)
	)
	(:action serve-p8_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(holding p8) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p8)) 
				(not (want p8)) 
				(not (holding p8)) 
			)
	)
	(:action pickup-p8_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p8) 
				(not (pkg_at l8)) 
			)
	)
	(:action putdown-p8_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p8) 
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l8) 
				(pkg_stored) 
				(in_store p8) 
				(not (holding p8)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p8_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p8)) 
				(want p8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p8) 
			)
	)
	(:action serve-p9_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(holding p9) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p9)) 
				(not (want p9)) 
				(not (holding p9)) 
			)
	)
	(:action pickup-p9_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p9) 
				(not (pkg_at l9)) 
			)
	)
	(:action putdown-p9_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p9) 
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l9) 
				(pkg_stored) 
				(in_store p9) 
				(not (holding p9)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p9_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p9)) 
				(want p9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p9) 
			)
	)
	(:action serve-p10_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(holding p10) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p10)) 
				(not (want p10)) 
				(not (holding p10)) 
			)
	)
	(:action pickup-p10_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p10) 
				(not (pkg_at l10)) 
			)
	)
	(:action putdown-p10_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p10) 
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l10) 
				(pkg_stored) 
				(in_store p10) 
				(not (holding p10)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p10_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p10)) 
				(want p10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p10) 
			)
	)
	(:action serve-p11_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(holding p11) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p11)) 
				(not (want p11)) 
				(not (holding p11)) 
			)
	)
	(:action pickup-p11_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p11) 
				(not (pkg_at l11)) 
			)
	)
	(:action putdown-p11_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p11) 
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l11) 
				(pkg_stored) 
				(in_store p11) 
				(not (holding p11)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p11_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p11)) 
				(want p11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p11) 
			)
	)
	(:action serve-p12_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(holding p12) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p12)) 
				(not (want p12)) 
				(not (holding p12)) 
			)
	)
	(:action pickup-p12_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p12) 
				(not (pkg_at l12)) 
			)
	)
	(:action putdown-p12_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p12) 
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l12) 
				(pkg_stored) 
				(in_store p12) 
				(not (holding p12)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p12_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p12)) 
				(want p12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p12) 
			)
	)
	(:action serve-p13_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(holding p13) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p13)) 
				(not (want p13)) 
				(not (holding p13)) 
			)
	)
	(:action pickup-p13_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p13) 
				(not (pkg_at l13)) 
			)
	)
	(:action putdown-p13_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p13) 
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l13) 
				(pkg_stored) 
				(in_store p13) 
				(not (holding p13)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p13_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p13)) 
				(want p13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p13) 
			)
	)
	(:action serve-p14_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(holding p14) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p14)) 
				(not (want p14)) 
				(not (holding p14)) 
			)
	)
	(:action pickup-p14_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p14) 
				(not (pkg_at l14)) 
			)
	)
	(:action putdown-p14_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p14) 
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l14) 
				(pkg_stored) 
				(in_store p14) 
				(not (holding p14)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p14_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p14)) 
				(want p14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p14) 
			)
	)
	(:action serve-p15_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(holding p15) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p15)) 
				(not (want p15)) 
				(not (holding p15)) 
			)
	)
	(:action pickup-p15_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p15) 
				(not (pkg_at l15)) 
			)
	)
	(:action putdown-p15_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p15) 
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l15) 
				(pkg_stored) 
				(in_store p15) 
				(not (holding p15)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p15_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p15)) 
				(want p15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p15) 
			)
	)
	(:action serve-p16_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(holding p16) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p16)) 
				(not (want p16)) 
				(not (holding p16)) 
			)
	)
	(:action pickup-p16_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(robot_at l16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p16) 
				(not (pkg_at l16)) 
			)
	)
	(:action putdown-p16_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p16) 
				(robot_at l16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l16) 
				(pkg_stored) 
				(in_store p16) 
				(not (holding p16)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p16_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p16)) 
				(want p16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p16) 
			)
	)
	(:action move1_2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (robot_at l1)) 
			)
	)
	(:action move2_1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (robot_at l2)) 
			)
	)
	(:action move2_3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (robot_at l2)) 
			)
	)
	(:action move3_2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (robot_at l3)) 
			)
	)
	(:action move3_4_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l4) 
				(not (robot_at l3)) 
			)
	)
	(:action move4_3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (robot_at l4)) 
			)
	)
	(:action move4_5_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l5) 
				(not (robot_at l4)) 
			)
	)
	(:action move5_4_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l4) 
				(not (robot_at l5)) 
			)
	)
	(:action move5_6_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l6) 
				(not (robot_at l5)) 
			)
	)
	(:action move6_5_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l5) 
				(not (robot_at l6)) 
			)
	)
	(:action move6_7_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l7) 
				(not (robot_at l6)) 
			)
	)
	(:action move7_6_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l6) 
				(not (robot_at l7)) 
			)
	)
	(:action move7_8_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l8) 
				(not (robot_at l7)) 
			)
	)
	(:action move8_7_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l7) 
				(not (robot_at l8)) 
			)
	)
	(:action move8_9_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l9) 
				(not (robot_at l8)) 
			)
	)
	(:action move9_8_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l8) 
				(not (robot_at l9)) 
			)
	)
	(:action move9_10_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l10) 
				(not (robot_at l9)) 
			)
	)
	(:action move10_9_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l9) 
				(not (robot_at l10)) 
			)
	)
	(:action move10_11_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l11) 
				(not (robot_at l10)) 
			)
	)
	(:action move11_10_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l10) 
				(not (robot_at l11)) 
			)
	)
	(:action move11_12_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l12) 
				(not (robot_at l11)) 
			)
	)
	(:action move12_11_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l11) 
				(not (robot_at l12)) 
			)
	)
	(:action move12_13_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l13) 
				(not (robot_at l12)) 
			)
	)
	(:action move13_12_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l12) 
				(not (robot_at l13)) 
			)
	)
	(:action move13_14_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l14) 
				(not (robot_at l13)) 
			)
	)
	(:action move14_13_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l13) 
				(not (robot_at l14)) 
			)
	)
	(:action move14_15_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l15) 
				(not (robot_at l14)) 
			)
	)
	(:action move15_14_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l14) 
				(not (robot_at l15)) 
			)
	)
	(:action move15_16_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l16) 
				(not (robot_at l15)) 
			)
	)
	(:action move16_15_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l15) 
				(not (robot_at l16)) 
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(active_request) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (robot_at desk)) 
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at desk) 
				(not (robot_at l1)) 
			)
	)
	(:action request_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (active_request)) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_requested) (want p5) )(and (pkg_requested) (want p6) )(and (pkg_requested) (want p7) )(and (pkg_requested) (want p8) )(and (pkg_requested) (want p9) )(and (pkg_requested) (want p10) )(and (pkg_requested) (want p11) )(and (pkg_requested) (want p12) )(and (pkg_requested) (want p13) )(and (pkg_requested) (want p14) )(and (pkg_requested) (want p15) )(and (pkg_requested) (want p16) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) )(and (pkg_arrived) (holding p5) )(and (pkg_arrived) (holding p6) )(and (pkg_arrived) (holding p7) )(and (pkg_arrived) (holding p8) )(and (pkg_arrived) (holding p9) )(and (pkg_arrived) (holding p10) )(and (pkg_arrived) (holding p11) )(and (pkg_arrived) (holding p12) )(and (pkg_arrived) (holding p13) )(and (pkg_arrived) (holding p14) )(and (pkg_arrived) (holding p15) )(and (pkg_arrived) (holding p16) ) ) 
			)
	)
	(:action serve-p1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p1) 
				(not (pkg_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p1) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p2) 
				(not (pkg_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p2) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p3) 
				(not (pkg_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p3) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p4_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p4) 
				(not (pkg_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p4_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p4) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p5_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(holding p5) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p5)) 
				(not (want p5)) 
				(not (holding p5)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p5_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p5) 
				(not (pkg_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p5_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p5) 
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l5) 
				(pkg_stored) 
				(in_store p5) 
				(not (holding p5)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p5_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p5)) 
				(want p5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p5) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p6_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(holding p6) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p6)) 
				(not (want p6)) 
				(not (holding p6)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p6_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p6) 
				(not (pkg_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p6_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p6) 
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l6) 
				(pkg_stored) 
				(in_store p6) 
				(not (holding p6)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p6_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p6)) 
				(want p6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p6) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p7_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(holding p7) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p7)) 
				(not (want p7)) 
				(not (holding p7)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p7_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p7) 
				(not (pkg_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p7_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p7) 
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l7) 
				(pkg_stored) 
				(in_store p7) 
				(not (holding p7)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p7_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p7)) 
				(want p7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p7) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p8_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(holding p8) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p8)) 
				(not (want p8)) 
				(not (holding p8)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p8_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p8) 
				(not (pkg_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p8_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p8) 
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l8) 
				(pkg_stored) 
				(in_store p8) 
				(not (holding p8)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p8_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p8)) 
				(want p8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p8) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p9_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(holding p9) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p9)) 
				(not (want p9)) 
				(not (holding p9)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p9_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p9) 
				(not (pkg_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p9_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p9) 
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l9) 
				(pkg_stored) 
				(in_store p9) 
				(not (holding p9)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p9_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p9)) 
				(want p9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p9) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p10_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(holding p10) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p10)) 
				(not (want p10)) 
				(not (holding p10)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p10_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p10) 
				(not (pkg_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p10_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p10) 
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l10) 
				(pkg_stored) 
				(in_store p10) 
				(not (holding p10)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p10_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p10)) 
				(want p10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p10) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p11_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(holding p11) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p11)) 
				(not (want p11)) 
				(not (holding p11)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p11_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p11) 
				(not (pkg_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p11_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p11) 
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l11) 
				(pkg_stored) 
				(in_store p11) 
				(not (holding p11)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p11_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p11)) 
				(want p11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p11) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p12_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(holding p12) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p12)) 
				(not (want p12)) 
				(not (holding p12)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p12_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p12) 
				(not (pkg_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p12_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p12) 
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l12) 
				(pkg_stored) 
				(in_store p12) 
				(not (holding p12)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p12_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p12)) 
				(want p12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p12) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p13_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(holding p13) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p13)) 
				(not (want p13)) 
				(not (holding p13)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p13_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p13) 
				(not (pkg_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p13_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p13) 
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l13) 
				(pkg_stored) 
				(in_store p13) 
				(not (holding p13)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p13_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p13)) 
				(want p13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p13) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p14_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(holding p14) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p14)) 
				(not (want p14)) 
				(not (holding p14)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p14_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p14) 
				(not (pkg_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p14_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p14) 
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l14) 
				(pkg_stored) 
				(in_store p14) 
				(not (holding p14)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p14_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p14)) 
				(want p14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p14) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p15_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(holding p15) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p15)) 
				(not (want p15)) 
				(not (holding p15)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p15_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p15) 
				(not (pkg_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p15_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p15) 
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l15) 
				(pkg_stored) 
				(in_store p15) 
				(not (holding p15)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p15_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p15)) 
				(want p15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p15) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p16_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(holding p16) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p16)) 
				(not (want p16)) 
				(not (holding p16)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p16_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(robot_at l16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p16) 
				(not (pkg_at l16)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p16_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p16) 
				(robot_at l16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l16) 
				(pkg_stored) 
				(in_store p16) 
				(not (holding p16)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p16_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p16)) 
				(want p16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p16) 
				(oneof (and) (ok))
			)
	)
	(:action move1_2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_4_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_5_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l5) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action move5_4_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action move5_6_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l6) 
				(not (robot_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action move6_5_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l5) 
				(not (robot_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action move6_7_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l7) 
				(not (robot_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action move7_6_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l6) 
				(not (robot_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action move7_8_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l8) 
				(not (robot_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action move8_7_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l7) 
				(not (robot_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action move8_9_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l9) 
				(not (robot_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action move9_8_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l8) 
				(not (robot_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action move9_10_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l10) 
				(not (robot_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action move10_9_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l9) 
				(not (robot_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action move10_11_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l11) 
				(not (robot_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action move11_10_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l10) 
				(not (robot_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action move11_12_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l12) 
				(not (robot_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action move12_11_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l11) 
				(not (robot_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action move12_13_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l13) 
				(not (robot_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action move13_12_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l12) 
				(not (robot_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action move13_14_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l14) 
				(not (robot_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action move14_13_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l13) 
				(not (robot_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action move14_15_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l15) 
				(not (robot_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action move15_14_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l14) 
				(not (robot_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action move15_16_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l16) 
				(not (robot_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action move16_15_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l15) 
				(not (robot_at l16)) 
				(oneof (and) (ok))
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(active_request) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at desk)) 
				(oneof (and) (ok))
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at desk) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action request_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (active_request)) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_requested) (want p5) )(and (pkg_requested) (want p6) )(and (pkg_requested) (want p7) )(and (pkg_requested) (want p8) )(and (pkg_requested) (want p9) )(and (pkg_requested) (want p10) )(and (pkg_requested) (want p11) )(and (pkg_requested) (want p12) )(and (pkg_requested) (want p13) )(and (pkg_requested) (want p14) )(and (pkg_requested) (want p15) )(and (pkg_requested) (want p16) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) )(and (pkg_arrived) (holding p5) )(and (pkg_arrived) (holding p6) )(and (pkg_arrived) (holding p7) )(and (pkg_arrived) (holding p8) )(and (pkg_arrived) (holding p9) )(and (pkg_arrived) (holding p10) )(and (pkg_arrived) (holding p11) )(and (pkg_arrived) (holding p12) )(and (pkg_arrived) (holding p13) )(and (pkg_arrived) (holding p14) )(and (pkg_arrived) (holding p15) )(and (pkg_arrived) (holding p16) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p1) 
				(not (pkg_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p1) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p2) 
				(not (pkg_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p2) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p3) 
				(not (pkg_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p3) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p4_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p4) 
				(not (pkg_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p4_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p4) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p5_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(holding p5) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p5)) 
				(not (want p5)) 
				(not (holding p5)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p5_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(robot_at l5) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p5) 
				(not (pkg_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p5_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p5) 
				(robot_at l5) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l5) 
				(pkg_stored) 
				(in_store p5) 
				(not (holding p5)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p5_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p5)) 
				(want p5) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p5) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p6_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(holding p6) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p6)) 
				(not (want p6)) 
				(not (holding p6)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p6_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(robot_at l6) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p6) 
				(not (pkg_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p6_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p6) 
				(robot_at l6) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l6) 
				(pkg_stored) 
				(in_store p6) 
				(not (holding p6)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p6_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p6)) 
				(want p6) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p6) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p7_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(holding p7) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p7)) 
				(not (want p7)) 
				(not (holding p7)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p7_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(robot_at l7) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p7) 
				(not (pkg_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p7_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p7) 
				(robot_at l7) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l7) 
				(pkg_stored) 
				(in_store p7) 
				(not (holding p7)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p7_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p7)) 
				(want p7) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p7) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p8_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(holding p8) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p8)) 
				(not (want p8)) 
				(not (holding p8)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p8_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(robot_at l8) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p8) 
				(not (pkg_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p8_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p8) 
				(robot_at l8) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l8) 
				(pkg_stored) 
				(in_store p8) 
				(not (holding p8)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p8_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p8)) 
				(want p8) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p8) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p9_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(holding p9) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p9)) 
				(not (want p9)) 
				(not (holding p9)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p9_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(robot_at l9) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p9) 
				(not (pkg_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p9_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p9) 
				(robot_at l9) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l9) 
				(pkg_stored) 
				(in_store p9) 
				(not (holding p9)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p9_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p9)) 
				(want p9) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p9) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p10_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(holding p10) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p10)) 
				(not (want p10)) 
				(not (holding p10)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p10_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(robot_at l10) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p10) 
				(not (pkg_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p10_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p10) 
				(robot_at l10) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l10) 
				(pkg_stored) 
				(in_store p10) 
				(not (holding p10)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p10_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p10)) 
				(want p10) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p10) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p11_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(holding p11) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p11)) 
				(not (want p11)) 
				(not (holding p11)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p11_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(robot_at l11) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p11) 
				(not (pkg_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p11_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p11) 
				(robot_at l11) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l11) 
				(pkg_stored) 
				(in_store p11) 
				(not (holding p11)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p11_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p11)) 
				(want p11) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p11) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p12_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(holding p12) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p12)) 
				(not (want p12)) 
				(not (holding p12)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p12_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(robot_at l12) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p12) 
				(not (pkg_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p12_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p12) 
				(robot_at l12) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l12) 
				(pkg_stored) 
				(in_store p12) 
				(not (holding p12)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p12_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p12)) 
				(want p12) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p12) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p13_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(holding p13) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p13)) 
				(not (want p13)) 
				(not (holding p13)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p13_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(robot_at l13) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p13) 
				(not (pkg_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p13_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p13) 
				(robot_at l13) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l13) 
				(pkg_stored) 
				(in_store p13) 
				(not (holding p13)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p13_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p13)) 
				(want p13) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p13) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p14_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(holding p14) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p14)) 
				(not (want p14)) 
				(not (holding p14)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p14_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(robot_at l14) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p14) 
				(not (pkg_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p14_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p14) 
				(robot_at l14) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l14) 
				(pkg_stored) 
				(in_store p14) 
				(not (holding p14)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p14_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p14)) 
				(want p14) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p14) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p15_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(holding p15) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p15)) 
				(not (want p15)) 
				(not (holding p15)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p15_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(robot_at l15) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p15) 
				(not (pkg_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p15_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p15) 
				(robot_at l15) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l15) 
				(pkg_stored) 
				(in_store p15) 
				(not (holding p15)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p15_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p15)) 
				(want p15) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p15) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p16_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(holding p16) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p16)) 
				(not (want p16)) 
				(not (holding p16)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p16_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(robot_at l16) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p16) 
				(not (pkg_at l16)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p16_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p16) 
				(robot_at l16) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l16) 
				(pkg_stored) 
				(in_store p16) 
				(not (holding p16)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p16_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p16)) 
				(want p16) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p16) 
				(oneof (and) (ok))
			)
	)
	(:action move1_2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_4_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_5_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l5) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action move5_4_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l5) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action move5_6_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l5) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l6) 
				(not (robot_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action move6_5_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l6) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l5) 
				(not (robot_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action move6_7_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l6) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l7) 
				(not (robot_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action move7_6_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l7) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l6) 
				(not (robot_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action move7_8_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l7) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l8) 
				(not (robot_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action move8_7_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l8) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l7) 
				(not (robot_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action move8_9_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l8) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l9) 
				(not (robot_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action move9_8_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l9) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l8) 
				(not (robot_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action move9_10_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l9) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l10) 
				(not (robot_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action move10_9_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l10) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l9) 
				(not (robot_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action move10_11_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l10) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l11) 
				(not (robot_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action move11_10_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l11) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l10) 
				(not (robot_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action move11_12_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l11) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l12) 
				(not (robot_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action move12_11_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l12) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l11) 
				(not (robot_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action move12_13_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l12) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l13) 
				(not (robot_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action move13_12_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l13) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l12) 
				(not (robot_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action move13_14_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l13) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l14) 
				(not (robot_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action move14_13_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l14) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l13) 
				(not (robot_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action move14_15_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l14) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l15) 
				(not (robot_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action move15_14_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l15) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l14) 
				(not (robot_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action move15_16_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l15) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l16) 
				(not (robot_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action move16_15_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l16) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l15) 
				(not (robot_at l16)) 
				(oneof (and) (ok))
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(active_request) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at desk)) 
				(oneof (and) (ok))
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at desk) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action request_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (active_request)) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_requested) (want p5) )(and (pkg_requested) (want p6) )(and (pkg_requested) (want p7) )(and (pkg_requested) (want p8) )(and (pkg_requested) (want p9) )(and (pkg_requested) (want p10) )(and (pkg_requested) (want p11) )(and (pkg_requested) (want p12) )(and (pkg_requested) (want p13) )(and (pkg_requested) (want p14) )(and (pkg_requested) (want p15) )(and (pkg_requested) (want p16) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) )(and (pkg_arrived) (holding p5) )(and (pkg_arrived) (holding p6) )(and (pkg_arrived) (holding p7) )(and (pkg_arrived) (holding p8) )(and (pkg_arrived) (holding p9) )(and (pkg_arrived) (holding p10) )(and (pkg_arrived) (holding p11) )(and (pkg_arrived) (holding p12) )(and (pkg_arrived) (holding p13) )(and (pkg_arrived) (holding p14) )(and (pkg_arrived) (holding p15) )(and (pkg_arrived) (holding p16) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p1) 
				(not (pkg_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p1) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p2) 
				(not (pkg_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p2) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p3) 
				(not (pkg_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p3) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p4_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p4) 
				(not (pkg_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p4_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p4) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p5_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(holding p5) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p5)) 
				(not (want p5)) 
				(not (holding p5)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p5_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(robot_at l5) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p5) 
				(not (pkg_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p5_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p5) 
				(robot_at l5) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l5) 
				(pkg_stored) 
				(in_store p5) 
				(not (holding p5)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p5_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p5)) 
				(want p5) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p5) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p6_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(holding p6) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p6)) 
				(not (want p6)) 
				(not (holding p6)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p6_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(robot_at l6) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p6) 
				(not (pkg_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p6_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p6) 
				(robot_at l6) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l6) 
				(pkg_stored) 
				(in_store p6) 
				(not (holding p6)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p6_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p6)) 
				(want p6) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p6) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p7_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(holding p7) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p7)) 
				(not (want p7)) 
				(not (holding p7)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p7_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(robot_at l7) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p7) 
				(not (pkg_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p7_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p7) 
				(robot_at l7) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l7) 
				(pkg_stored) 
				(in_store p7) 
				(not (holding p7)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p7_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p7)) 
				(want p7) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p7) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p8_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(holding p8) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p8)) 
				(not (want p8)) 
				(not (holding p8)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p8_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(robot_at l8) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p8) 
				(not (pkg_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p8_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p8) 
				(robot_at l8) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l8) 
				(pkg_stored) 
				(in_store p8) 
				(not (holding p8)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p8_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p8)) 
				(want p8) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p8) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p9_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(holding p9) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p9)) 
				(not (want p9)) 
				(not (holding p9)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p9_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(robot_at l9) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p9) 
				(not (pkg_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p9_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p9) 
				(robot_at l9) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l9) 
				(pkg_stored) 
				(in_store p9) 
				(not (holding p9)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p9_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p9)) 
				(want p9) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p9) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p10_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(holding p10) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p10)) 
				(not (want p10)) 
				(not (holding p10)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p10_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(robot_at l10) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p10) 
				(not (pkg_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p10_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p10) 
				(robot_at l10) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l10) 
				(pkg_stored) 
				(in_store p10) 
				(not (holding p10)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p10_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p10)) 
				(want p10) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p10) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p11_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(holding p11) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p11)) 
				(not (want p11)) 
				(not (holding p11)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p11_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(robot_at l11) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p11) 
				(not (pkg_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p11_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p11) 
				(robot_at l11) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l11) 
				(pkg_stored) 
				(in_store p11) 
				(not (holding p11)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p11_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p11)) 
				(want p11) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p11) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p12_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(holding p12) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p12)) 
				(not (want p12)) 
				(not (holding p12)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p12_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(robot_at l12) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p12) 
				(not (pkg_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p12_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p12) 
				(robot_at l12) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l12) 
				(pkg_stored) 
				(in_store p12) 
				(not (holding p12)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p12_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p12)) 
				(want p12) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p12) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p13_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(holding p13) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p13)) 
				(not (want p13)) 
				(not (holding p13)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p13_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(robot_at l13) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p13) 
				(not (pkg_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p13_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p13) 
				(robot_at l13) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l13) 
				(pkg_stored) 
				(in_store p13) 
				(not (holding p13)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p13_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p13)) 
				(want p13) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p13) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p14_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(holding p14) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p14)) 
				(not (want p14)) 
				(not (holding p14)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p14_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(robot_at l14) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p14) 
				(not (pkg_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p14_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p14) 
				(robot_at l14) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l14) 
				(pkg_stored) 
				(in_store p14) 
				(not (holding p14)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p14_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p14)) 
				(want p14) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p14) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p15_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(holding p15) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p15)) 
				(not (want p15)) 
				(not (holding p15)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p15_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(robot_at l15) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p15) 
				(not (pkg_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p15_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p15) 
				(robot_at l15) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l15) 
				(pkg_stored) 
				(in_store p15) 
				(not (holding p15)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p15_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p15)) 
				(want p15) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p15) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p16_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(holding p16) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p16)) 
				(not (want p16)) 
				(not (holding p16)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p16_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(robot_at l16) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p16) 
				(not (pkg_at l16)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p16_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p16) 
				(robot_at l16) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l16) 
				(pkg_stored) 
				(in_store p16) 
				(not (holding p16)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p16_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p16)) 
				(want p16) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p16) 
				(oneof (and) (ok))
			)
	)
	(:action move1_2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_4_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_5_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l5) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action move5_4_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l5) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action move5_6_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l5) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l6) 
				(not (robot_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action move6_5_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l6) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l5) 
				(not (robot_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action move6_7_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l6) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l7) 
				(not (robot_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action move7_6_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l7) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l6) 
				(not (robot_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action move7_8_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l7) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l8) 
				(not (robot_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action move8_7_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l8) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l7) 
				(not (robot_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action move8_9_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l8) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l9) 
				(not (robot_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action move9_8_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l9) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l8) 
				(not (robot_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action move9_10_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l9) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l10) 
				(not (robot_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action move10_9_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l10) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l9) 
				(not (robot_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action move10_11_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l10) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l11) 
				(not (robot_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action move11_10_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l11) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l10) 
				(not (robot_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action move11_12_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l11) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l12) 
				(not (robot_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action move12_11_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l12) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l11) 
				(not (robot_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action move12_13_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l12) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l13) 
				(not (robot_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action move13_12_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l13) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l12) 
				(not (robot_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action move13_14_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l13) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l14) 
				(not (robot_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action move14_13_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l14) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l13) 
				(not (robot_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action move14_15_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l14) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l15) 
				(not (robot_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action move15_14_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l15) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l14) 
				(not (robot_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action move15_16_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l15) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l16) 
				(not (robot_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action move16_15_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l16) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l15) 
				(not (robot_at l16)) 
				(oneof (and) (ok))
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(active_request) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at desk)) 
				(oneof (and) (ok))
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at desk) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action request_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (active_request)) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_requested) (want p5) )(and (pkg_requested) (want p6) )(and (pkg_requested) (want p7) )(and (pkg_requested) (want p8) )(and (pkg_requested) (want p9) )(and (pkg_requested) (want p10) )(and (pkg_requested) (want p11) )(and (pkg_requested) (want p12) )(and (pkg_requested) (want p13) )(and (pkg_requested) (want p14) )(and (pkg_requested) (want p15) )(and (pkg_requested) (want p16) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) )(and (pkg_arrived) (holding p5) )(and (pkg_arrived) (holding p6) )(and (pkg_arrived) (holding p7) )(and (pkg_arrived) (holding p8) )(and (pkg_arrived) (holding p9) )(and (pkg_arrived) (holding p10) )(and (pkg_arrived) (holding p11) )(and (pkg_arrived) (holding p12) )(and (pkg_arrived) (holding p13) )(and (pkg_arrived) (holding p14) )(and (pkg_arrived) (holding p15) )(and (pkg_arrived) (holding p16) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p1) 
				(not (pkg_at l1)) 
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p1) 
			)
	)
	(:action serve-p2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p2) 
				(not (pkg_at l2)) 
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p2) 
			)
	)
	(:action serve-p3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p3) 
				(not (pkg_at l3)) 
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p3) 
			)
	)
	(:action serve-p4_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p4) 
				(not (pkg_at l4)) 
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p4_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p4) 
			)
	)
	(:action serve-p5_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(holding p5) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p5)) 
				(not (want p5)) 
				(not (holding p5)) 
			)
	)
	(:action pickup-p5_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p5) 
				(not (pkg_at l5)) 
			)
	)
	(:action putdown-p5_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p5) 
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l5) 
				(pkg_stored) 
				(in_store p5) 
				(not (holding p5)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p5_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p5)) 
				(want p5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p5) 
			)
	)
	(:action serve-p6_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(holding p6) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p6)) 
				(not (want p6)) 
				(not (holding p6)) 
			)
	)
	(:action pickup-p6_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p6) 
				(not (pkg_at l6)) 
			)
	)
	(:action putdown-p6_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p6) 
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l6) 
				(pkg_stored) 
				(in_store p6) 
				(not (holding p6)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p6_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p6)) 
				(want p6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p6) 
			)
	)
	(:action serve-p7_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(holding p7) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p7)) 
				(not (want p7)) 
				(not (holding p7)) 
			)
	)
	(:action pickup-p7_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p7) 
				(not (pkg_at l7)) 
			)
	)
	(:action putdown-p7_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p7) 
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l7) 
				(pkg_stored) 
				(in_store p7) 
				(not (holding p7)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p7_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p7)) 
				(want p7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p7) 
			)
	)
	(:action serve-p8_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(holding p8) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p8)) 
				(not (want p8)) 
				(not (holding p8)) 
			)
	)
	(:action pickup-p8_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p8) 
				(not (pkg_at l8)) 
			)
	)
	(:action putdown-p8_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p8) 
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l8) 
				(pkg_stored) 
				(in_store p8) 
				(not (holding p8)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p8_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p8)) 
				(want p8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p8) 
			)
	)
	(:action serve-p9_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(holding p9) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p9)) 
				(not (want p9)) 
				(not (holding p9)) 
			)
	)
	(:action pickup-p9_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p9) 
				(not (pkg_at l9)) 
			)
	)
	(:action putdown-p9_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p9) 
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l9) 
				(pkg_stored) 
				(in_store p9) 
				(not (holding p9)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p9_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p9)) 
				(want p9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p9) 
			)
	)
	(:action serve-p10_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(holding p10) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p10)) 
				(not (want p10)) 
				(not (holding p10)) 
			)
	)
	(:action pickup-p10_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p10) 
				(not (pkg_at l10)) 
			)
	)
	(:action putdown-p10_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p10) 
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l10) 
				(pkg_stored) 
				(in_store p10) 
				(not (holding p10)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p10_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p10)) 
				(want p10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p10) 
			)
	)
	(:action serve-p11_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(holding p11) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p11)) 
				(not (want p11)) 
				(not (holding p11)) 
			)
	)
	(:action pickup-p11_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p11) 
				(not (pkg_at l11)) 
			)
	)
	(:action putdown-p11_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p11) 
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l11) 
				(pkg_stored) 
				(in_store p11) 
				(not (holding p11)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p11_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p11)) 
				(want p11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p11) 
			)
	)
	(:action serve-p12_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(holding p12) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p12)) 
				(not (want p12)) 
				(not (holding p12)) 
			)
	)
	(:action pickup-p12_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p12) 
				(not (pkg_at l12)) 
			)
	)
	(:action putdown-p12_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p12) 
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l12) 
				(pkg_stored) 
				(in_store p12) 
				(not (holding p12)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p12_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p12)) 
				(want p12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p12) 
			)
	)
	(:action serve-p13_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(holding p13) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p13)) 
				(not (want p13)) 
				(not (holding p13)) 
			)
	)
	(:action pickup-p13_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p13) 
				(not (pkg_at l13)) 
			)
	)
	(:action putdown-p13_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p13) 
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l13) 
				(pkg_stored) 
				(in_store p13) 
				(not (holding p13)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p13_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p13)) 
				(want p13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p13) 
			)
	)
	(:action serve-p14_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(holding p14) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p14)) 
				(not (want p14)) 
				(not (holding p14)) 
			)
	)
	(:action pickup-p14_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p14) 
				(not (pkg_at l14)) 
			)
	)
	(:action putdown-p14_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p14) 
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l14) 
				(pkg_stored) 
				(in_store p14) 
				(not (holding p14)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p14_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p14)) 
				(want p14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p14) 
			)
	)
	(:action serve-p15_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(holding p15) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p15)) 
				(not (want p15)) 
				(not (holding p15)) 
			)
	)
	(:action pickup-p15_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p15) 
				(not (pkg_at l15)) 
			)
	)
	(:action putdown-p15_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p15) 
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l15) 
				(pkg_stored) 
				(in_store p15) 
				(not (holding p15)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p15_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p15)) 
				(want p15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p15) 
			)
	)
	(:action serve-p16_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(holding p16) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p16)) 
				(not (want p16)) 
				(not (holding p16)) 
			)
	)
	(:action pickup-p16_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(robot_at l16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p16) 
				(not (pkg_at l16)) 
			)
	)
	(:action putdown-p16_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p16) 
				(robot_at l16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l16) 
				(pkg_stored) 
				(in_store p16) 
				(not (holding p16)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p16_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p16)) 
				(want p16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p16) 
			)
	)
	(:action move1_2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l1)) 
			)
	)
	(:action move2_1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at l2)) 
			)
	)
	(:action move2_3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l2)) 
			)
	)
	(:action move3_2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l3)) 
			)
	)
	(:action move3_4_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l3)) 
			)
	)
	(:action move4_3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l4)) 
			)
	)
	(:action move4_5_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l5) 
				(not (robot_at l4)) 
			)
	)
	(:action move5_4_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l5)) 
			)
	)
	(:action move5_6_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l5) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l6) 
				(not (robot_at l5)) 
			)
	)
	(:action move6_5_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l5) 
				(not (robot_at l6)) 
			)
	)
	(:action move6_7_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l6) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l7) 
				(not (robot_at l6)) 
			)
	)
	(:action move7_6_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l6) 
				(not (robot_at l7)) 
			)
	)
	(:action move7_8_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l7) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l8) 
				(not (robot_at l7)) 
			)
	)
	(:action move8_7_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l7) 
				(not (robot_at l8)) 
			)
	)
	(:action move8_9_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l8) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l9) 
				(not (robot_at l8)) 
			)
	)
	(:action move9_8_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l8) 
				(not (robot_at l9)) 
			)
	)
	(:action move9_10_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l9) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l10) 
				(not (robot_at l9)) 
			)
	)
	(:action move10_9_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l9) 
				(not (robot_at l10)) 
			)
	)
	(:action move10_11_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l10) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l11) 
				(not (robot_at l10)) 
			)
	)
	(:action move11_10_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l10) 
				(not (robot_at l11)) 
			)
	)
	(:action move11_12_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l11) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l12) 
				(not (robot_at l11)) 
			)
	)
	(:action move12_11_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l11) 
				(not (robot_at l12)) 
			)
	)
	(:action move12_13_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l12) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l13) 
				(not (robot_at l12)) 
			)
	)
	(:action move13_12_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l12) 
				(not (robot_at l13)) 
			)
	)
	(:action move13_14_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l13) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l14) 
				(not (robot_at l13)) 
			)
	)
	(:action move14_13_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l13) 
				(not (robot_at l14)) 
			)
	)
	(:action move14_15_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l14) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l15) 
				(not (robot_at l14)) 
			)
	)
	(:action move15_14_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l14) 
				(not (robot_at l15)) 
			)
	)
	(:action move15_16_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l15) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l16) 
				(not (robot_at l15)) 
			)
	)
	(:action move16_15_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l16) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l15) 
				(not (robot_at l16)) 
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(active_request) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at desk)) 
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at desk) 
				(not (robot_at l1)) 
			)
	)
	(:action request_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(not (active_request)) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_requested) (want p5) )(and (pkg_requested) (want p6) )(and (pkg_requested) (want p7) )(and (pkg_requested) (want p8) )(and (pkg_requested) (want p9) )(and (pkg_requested) (want p10) )(and (pkg_requested) (want p11) )(and (pkg_requested) (want p12) )(and (pkg_requested) (want p13) )(and (pkg_requested) (want p14) )(and (pkg_requested) (want p15) )(and (pkg_requested) (want p16) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) )(and (pkg_arrived) (holding p5) )(and (pkg_arrived) (holding p6) )(and (pkg_arrived) (holding p7) )(and (pkg_arrived) (holding p8) )(and (pkg_arrived) (holding p9) )(and (pkg_arrived) (holding p10) )(and (pkg_arrived) (holding p11) )(and (pkg_arrived) (holding p12) )(and (pkg_arrived) (holding p13) )(and (pkg_arrived) (holding p14) )(and (pkg_arrived) (holding p15) )(and (pkg_arrived) (holding p16) ) ) 
			)
	)
	(:action serve-p1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p1) 
				(not (pkg_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p1) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p2) 
				(not (pkg_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p2) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p3) 
				(not (pkg_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p3) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p4_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p4) 
				(not (pkg_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p4_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p4) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p5_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(holding p5) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p5)) 
				(not (want p5)) 
				(not (holding p5)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p5_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(robot_at l5) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p5) 
				(not (pkg_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p5_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p5) 
				(robot_at l5) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l5) 
				(pkg_stored) 
				(in_store p5) 
				(not (holding p5)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p5_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p5)) 
				(want p5) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p5) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p6_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(holding p6) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p6)) 
				(not (want p6)) 
				(not (holding p6)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p6_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(robot_at l6) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p6) 
				(not (pkg_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p6_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p6) 
				(robot_at l6) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l6) 
				(pkg_stored) 
				(in_store p6) 
				(not (holding p6)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p6_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p6)) 
				(want p6) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p6) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p7_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(holding p7) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p7)) 
				(not (want p7)) 
				(not (holding p7)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p7_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(robot_at l7) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p7) 
				(not (pkg_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p7_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p7) 
				(robot_at l7) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l7) 
				(pkg_stored) 
				(in_store p7) 
				(not (holding p7)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p7_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p7)) 
				(want p7) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p7) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p8_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(holding p8) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p8)) 
				(not (want p8)) 
				(not (holding p8)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p8_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(robot_at l8) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p8) 
				(not (pkg_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p8_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p8) 
				(robot_at l8) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l8) 
				(pkg_stored) 
				(in_store p8) 
				(not (holding p8)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p8_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p8)) 
				(want p8) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p8) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p9_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(holding p9) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p9)) 
				(not (want p9)) 
				(not (holding p9)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p9_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(robot_at l9) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p9) 
				(not (pkg_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p9_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p9) 
				(robot_at l9) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l9) 
				(pkg_stored) 
				(in_store p9) 
				(not (holding p9)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p9_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p9)) 
				(want p9) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p9) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p10_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(holding p10) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p10)) 
				(not (want p10)) 
				(not (holding p10)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p10_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(robot_at l10) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p10) 
				(not (pkg_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p10_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p10) 
				(robot_at l10) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l10) 
				(pkg_stored) 
				(in_store p10) 
				(not (holding p10)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p10_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p10)) 
				(want p10) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p10) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p11_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(holding p11) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p11)) 
				(not (want p11)) 
				(not (holding p11)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p11_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(robot_at l11) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p11) 
				(not (pkg_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p11_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p11) 
				(robot_at l11) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l11) 
				(pkg_stored) 
				(in_store p11) 
				(not (holding p11)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p11_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p11)) 
				(want p11) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p11) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p12_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(holding p12) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p12)) 
				(not (want p12)) 
				(not (holding p12)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p12_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(robot_at l12) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p12) 
				(not (pkg_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p12_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p12) 
				(robot_at l12) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l12) 
				(pkg_stored) 
				(in_store p12) 
				(not (holding p12)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p12_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p12)) 
				(want p12) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p12) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p13_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(holding p13) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p13)) 
				(not (want p13)) 
				(not (holding p13)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p13_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(robot_at l13) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p13) 
				(not (pkg_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p13_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p13) 
				(robot_at l13) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l13) 
				(pkg_stored) 
				(in_store p13) 
				(not (holding p13)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p13_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p13)) 
				(want p13) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p13) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p14_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(holding p14) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p14)) 
				(not (want p14)) 
				(not (holding p14)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p14_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(robot_at l14) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p14) 
				(not (pkg_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p14_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p14) 
				(robot_at l14) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l14) 
				(pkg_stored) 
				(in_store p14) 
				(not (holding p14)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p14_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p14)) 
				(want p14) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p14) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p15_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(holding p15) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p15)) 
				(not (want p15)) 
				(not (holding p15)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p15_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(robot_at l15) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p15) 
				(not (pkg_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p15_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p15) 
				(robot_at l15) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l15) 
				(pkg_stored) 
				(in_store p15) 
				(not (holding p15)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p15_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p15)) 
				(want p15) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p15) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p16_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(holding p16) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p16)) 
				(not (want p16)) 
				(not (holding p16)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p16_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(robot_at l16) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p16) 
				(not (pkg_at l16)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p16_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p16) 
				(robot_at l16) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l16) 
				(pkg_stored) 
				(in_store p16) 
				(not (holding p16)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p16_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p16)) 
				(want p16) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p16) 
				(oneof (and) (ok))
			)
	)
	(:action move1_2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_4_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_5_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l5) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action move5_4_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l5) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (robot_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action move5_6_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l5) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l6) 
				(not (robot_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action move6_5_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l6) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l5) 
				(not (robot_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action move6_7_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l6) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l7) 
				(not (robot_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action move7_6_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l7) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l6) 
				(not (robot_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action move7_8_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l7) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l8) 
				(not (robot_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action move8_7_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l8) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l7) 
				(not (robot_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action move8_9_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l8) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l9) 
				(not (robot_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action move9_8_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l9) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l8) 
				(not (robot_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action move9_10_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l9) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l10) 
				(not (robot_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action move10_9_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l10) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l9) 
				(not (robot_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action move10_11_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l10) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l11) 
				(not (robot_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action move11_10_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l11) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l10) 
				(not (robot_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action move11_12_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l11) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l12) 
				(not (robot_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action move12_11_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l12) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l11) 
				(not (robot_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action move12_13_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l12) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l13) 
				(not (robot_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action move13_12_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l13) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l12) 
				(not (robot_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action move13_14_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l13) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l14) 
				(not (robot_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action move14_13_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l14) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l13) 
				(not (robot_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action move14_15_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l14) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l15) 
				(not (robot_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action move15_14_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l15) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l14) 
				(not (robot_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action move15_16_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l15) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l16) 
				(not (robot_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action move16_15_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l16) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l15) 
				(not (robot_at l16)) 
				(oneof (and) (ok))
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(active_request) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (robot_at desk)) 
				(oneof (and) (ok))
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action request_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(not (active_request)) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_requested) (want p5) )(and (pkg_requested) (want p6) )(and (pkg_requested) (want p7) )(and (pkg_requested) (want p8) )(and (pkg_requested) (want p9) )(and (pkg_requested) (want p10) )(and (pkg_requested) (want p11) )(and (pkg_requested) (want p12) )(and (pkg_requested) (want p13) )(and (pkg_requested) (want p14) )(and (pkg_requested) (want p15) )(and (pkg_requested) (want p16) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) )(and (pkg_arrived) (holding p5) )(and (pkg_arrived) (holding p6) )(and (pkg_arrived) (holding p7) )(and (pkg_arrived) (holding p8) )(and (pkg_arrived) (holding p9) )(and (pkg_arrived) (holding p10) )(and (pkg_arrived) (holding p11) )(and (pkg_arrived) (holding p12) )(and (pkg_arrived) (holding p13) )(and (pkg_arrived) (holding p14) )(and (pkg_arrived) (holding p15) )(and (pkg_arrived) (holding p16) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p1) 
				(not (pkg_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p1) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p2) 
				(not (pkg_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p2) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p3) 
				(not (pkg_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p3) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p4_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p4) 
				(not (pkg_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p4_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p4) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p5_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(holding p5) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p5)) 
				(not (want p5)) 
				(not (holding p5)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p5_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p5) 
				(want p5) 
				(robot_at l5) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p5) 
				(not (pkg_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p5_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p5) 
				(robot_at l5) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l5) 
				(pkg_stored) 
				(in_store p5) 
				(not (holding p5)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p5_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p5)) 
				(want p5) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p5) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p6_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(holding p6) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p6)) 
				(not (want p6)) 
				(not (holding p6)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p6_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p6) 
				(want p6) 
				(robot_at l6) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p6) 
				(not (pkg_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p6_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p6) 
				(robot_at l6) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l6) 
				(pkg_stored) 
				(in_store p6) 
				(not (holding p6)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p6_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p6)) 
				(want p6) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p6) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p7_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(holding p7) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p7)) 
				(not (want p7)) 
				(not (holding p7)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p7_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p7) 
				(want p7) 
				(robot_at l7) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p7) 
				(not (pkg_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p7_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p7) 
				(robot_at l7) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l7) 
				(pkg_stored) 
				(in_store p7) 
				(not (holding p7)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p7_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p7)) 
				(want p7) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p7) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p8_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(holding p8) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p8)) 
				(not (want p8)) 
				(not (holding p8)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p8_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p8) 
				(want p8) 
				(robot_at l8) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p8) 
				(not (pkg_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p8_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p8) 
				(robot_at l8) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l8) 
				(pkg_stored) 
				(in_store p8) 
				(not (holding p8)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p8_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p8)) 
				(want p8) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p8) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p9_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(holding p9) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p9)) 
				(not (want p9)) 
				(not (holding p9)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p9_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p9) 
				(want p9) 
				(robot_at l9) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p9) 
				(not (pkg_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p9_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p9) 
				(robot_at l9) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l9) 
				(pkg_stored) 
				(in_store p9) 
				(not (holding p9)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p9_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p9)) 
				(want p9) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p9) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p10_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(holding p10) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p10)) 
				(not (want p10)) 
				(not (holding p10)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p10_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p10) 
				(want p10) 
				(robot_at l10) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p10) 
				(not (pkg_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p10_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p10) 
				(robot_at l10) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l10) 
				(pkg_stored) 
				(in_store p10) 
				(not (holding p10)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p10_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p10)) 
				(want p10) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p10) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p11_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(holding p11) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p11)) 
				(not (want p11)) 
				(not (holding p11)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p11_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p11) 
				(want p11) 
				(robot_at l11) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p11) 
				(not (pkg_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p11_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p11) 
				(robot_at l11) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l11) 
				(pkg_stored) 
				(in_store p11) 
				(not (holding p11)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p11_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p11)) 
				(want p11) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p11) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p12_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(holding p12) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p12)) 
				(not (want p12)) 
				(not (holding p12)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p12_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p12) 
				(want p12) 
				(robot_at l12) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p12) 
				(not (pkg_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p12_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p12) 
				(robot_at l12) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l12) 
				(pkg_stored) 
				(in_store p12) 
				(not (holding p12)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p12_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p12)) 
				(want p12) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p12) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p13_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(holding p13) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p13)) 
				(not (want p13)) 
				(not (holding p13)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p13_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p13) 
				(want p13) 
				(robot_at l13) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p13) 
				(not (pkg_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p13_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p13) 
				(robot_at l13) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l13) 
				(pkg_stored) 
				(in_store p13) 
				(not (holding p13)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p13_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p13)) 
				(want p13) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p13) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p14_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(holding p14) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p14)) 
				(not (want p14)) 
				(not (holding p14)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p14_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p14) 
				(want p14) 
				(robot_at l14) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p14) 
				(not (pkg_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p14_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p14) 
				(robot_at l14) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l14) 
				(pkg_stored) 
				(in_store p14) 
				(not (holding p14)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p14_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p14)) 
				(want p14) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p14) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p15_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(holding p15) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p15)) 
				(not (want p15)) 
				(not (holding p15)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p15_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p15) 
				(want p15) 
				(robot_at l15) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p15) 
				(not (pkg_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p15_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p15) 
				(robot_at l15) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l15) 
				(pkg_stored) 
				(in_store p15) 
				(not (holding p15)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p15_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p15)) 
				(want p15) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p15) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p16_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(holding p16) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p16)) 
				(not (want p16)) 
				(not (holding p16)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p16_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p16) 
				(want p16) 
				(robot_at l16) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p16) 
				(not (pkg_at l16)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p16_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p16) 
				(robot_at l16) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l16) 
				(pkg_stored) 
				(in_store p16) 
				(not (holding p16)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p16_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p16)) 
				(want p16) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p16) 
				(oneof (and) (ok))
			)
	)
	(:action move1_2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_4_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_5_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l5) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action move5_4_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l5) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (robot_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action move5_6_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l5) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l6) 
				(not (robot_at l5)) 
				(oneof (and) (ok))
			)
	)
	(:action move6_5_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l6) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l5) 
				(not (robot_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action move6_7_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l6) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l7) 
				(not (robot_at l6)) 
				(oneof (and) (ok))
			)
	)
	(:action move7_6_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l7) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l6) 
				(not (robot_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action move7_8_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l7) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l8) 
				(not (robot_at l7)) 
				(oneof (and) (ok))
			)
	)
	(:action move8_7_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l8) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l7) 
				(not (robot_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action move8_9_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l8) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l9) 
				(not (robot_at l8)) 
				(oneof (and) (ok))
			)
	)
	(:action move9_8_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l9) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l8) 
				(not (robot_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action move9_10_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l9) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l10) 
				(not (robot_at l9)) 
				(oneof (and) (ok))
			)
	)
	(:action move10_9_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l10) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l9) 
				(not (robot_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action move10_11_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l10) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l11) 
				(not (robot_at l10)) 
				(oneof (and) (ok))
			)
	)
	(:action move11_10_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l11) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l10) 
				(not (robot_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action move11_12_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l11) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l12) 
				(not (robot_at l11)) 
				(oneof (and) (ok))
			)
	)
	(:action move12_11_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l12) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l11) 
				(not (robot_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action move12_13_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l12) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l13) 
				(not (robot_at l12)) 
				(oneof (and) (ok))
			)
	)
	(:action move13_12_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l13) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l12) 
				(not (robot_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action move13_14_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l13) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l14) 
				(not (robot_at l13)) 
				(oneof (and) (ok))
			)
	)
	(:action move14_13_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l14) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l13) 
				(not (robot_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action move14_15_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l14) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l15) 
				(not (robot_at l14)) 
				(oneof (and) (ok))
			)
	)
	(:action move15_14_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l15) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l14) 
				(not (robot_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action move15_16_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l15) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l16) 
				(not (robot_at l15)) 
				(oneof (and) (ok))
			)
	)
	(:action move16_15_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l16) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l15) 
				(not (robot_at l16)) 
				(oneof (and) (ok))
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(active_request) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (robot_at desk)) 
				(oneof (and) (ok))
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action request_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(not (active_request)) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_requested) (want p5) )(and (pkg_requested) (want p6) )(and (pkg_requested) (want p7) )(and (pkg_requested) (want p8) )(and (pkg_requested) (want p9) )(and (pkg_requested) (want p10) )(and (pkg_requested) (want p11) )(and (pkg_requested) (want p12) )(and (pkg_requested) (want p13) )(and (pkg_requested) (want p14) )(and (pkg_requested) (want p15) )(and (pkg_requested) (want p16) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) )(and (pkg_arrived) (holding p5) )(and (pkg_arrived) (holding p6) )(and (pkg_arrived) (holding p7) )(and (pkg_arrived) (holding p8) )(and (pkg_arrived) (holding p9) )(and (pkg_arrived) (holding p10) )(and (pkg_arrived) (holding p11) )(and (pkg_arrived) (holding p12) )(and (pkg_arrived) (holding p13) )(and (pkg_arrived) (holding p14) )(and (pkg_arrived) (holding p15) )(and (pkg_arrived) (holding p16) ) ) 
				(oneof (and) (ok))
			)
	)
)

