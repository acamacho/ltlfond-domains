
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Store example for 4 packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Goal formula:
;;
;; A( (pkg_requested) OR (pkg_arrived) --> E (pkg_served) OR E (pkg_stored ) )
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [](  active_request -> ( <> pkg_stored | <> pkg_served) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain packages)
	(:requirements :strips :typing :equality)
	(:types 
		pkg
		loc
		baState ;;BAencoding
	)
	(:constants
		p1 p2 p3 p4 - pkg
		l1 l2 l3 l4 desk - loc
		BA-S0 BA-S1  - baState ;;BAencoding
	)
	(:predicates
		(in_store ?p  - pkg)
		(want ?p  - pkg)
		(active_request)
		(pkg_served)
		(pkg_requested)
		(pkg_arrived)
		(pkg_stored)
		(holding ?p - pkg)
		(pkg_at ?l - loc)
		(robot_at ?l - loc)
		(move) ;; store's turn
		(currentBAstate ?s  - baState) ;;BAencoding
		(ok) ;;BAencoding
	)


























	(:action serve-p1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p1) 
				(not (pkg_at l1)) 
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p1) 
			)
	)
	(:action serve-p2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p2) 
				(not (pkg_at l2)) 
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p2) 
			)
	)
	(:action serve-p3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p3) 
				(not (pkg_at l3)) 
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p3) 
			)
	)
	(:action serve-p4_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(holding p4) 
				(not (pkg_at l4)) 
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p4_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(in_store p4) 
			)
	)
	(:action move1_2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (robot_at l1)) 
			)
	)
	(:action move2_1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (robot_at l2)) 
			)
	)
	(:action move2_3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (robot_at l2)) 
			)
	)
	(:action move3_2_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (robot_at l3)) 
			)
	)
	(:action move3_4_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l4) 
				(not (robot_at l3)) 
			)
	)
	(:action move4_3_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (robot_at l4)) 
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(active_request) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (robot_at desk)) 
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(robot_at desk) 
				(not (robot_at l1)) 
			)
	)
	(:action request_buchi_move_from_BA-S0-1_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (active_request)) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(active_request) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) ) ) 
			)
	)
	(:action serve-p1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p1) 
				(not (pkg_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p1) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p2) 
				(not (pkg_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p2) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p3) 
				(not (pkg_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p3) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p4_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(holding p4) 
				(not (pkg_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p4_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(in_store p4) 
				(oneof (and) (ok))
			)
	)
	(:action move1_2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_4_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(active_request) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at desk)) 
				(oneof (and) (ok))
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(robot_at desk) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action request_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (active_request)) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
				(not (active_request)) 
			)
		:effect
			(and
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p1) 
				(not (pkg_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p1) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p2) 
				(not (pkg_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p2) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p3) 
				(not (pkg_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p3) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p4_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(holding p4) 
				(not (pkg_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p4_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(in_store p4) 
				(oneof (and) (ok))
			)
	)
	(:action move1_2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_4_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(active_request) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at desk)) 
				(oneof (and) (ok))
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(robot_at desk) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action request_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (active_request)) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p1) 
				(not (pkg_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p1) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p2) 
				(not (pkg_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p2) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p3) 
				(not (pkg_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p3) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p4_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(holding p4) 
				(not (pkg_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p4_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(pkg_stored) 
			)
		:effect
			(and
				(in_store p4) 
				(oneof (and) (ok))
			)
	)
	(:action move1_2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_4_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(active_request) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at desk)) 
				(oneof (and) (ok))
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(robot_at desk) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action request_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (active_request)) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p1) 
				(not (pkg_at l1)) 
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p1) 
			)
	)
	(:action serve-p2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p2) 
				(not (pkg_at l2)) 
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p2) 
			)
	)
	(:action serve-p3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p3) 
				(not (pkg_at l3)) 
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p3) 
			)
	)
	(:action serve-p4_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(holding p4) 
				(not (pkg_at l4)) 
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
			)
	)
	(:action restock-p4_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(in_store p4) 
			)
	)
	(:action move1_2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l1)) 
			)
	)
	(:action move2_1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at l2)) 
			)
	)
	(:action move2_3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l2)) 
			)
	)
	(:action move3_2_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l2) 
				(not (robot_at l3)) 
			)
	)
	(:action move3_4_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l4) 
				(not (robot_at l3)) 
			)
	)
	(:action move4_3_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l4) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l3) 
				(not (robot_at l4)) 
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(active_request) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at l1) 
				(not (robot_at desk)) 
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(robot_at desk) 
				(not (robot_at l1)) 
			)
	)
	(:action request_buchi_move_from_BA-S1-1_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(not (active_request)) 
				(not (pkg_stored)) 
				(not (pkg_served)) 
			)
		:effect
			(and
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) ) ) 
			)
	)
	(:action serve-p1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p1) 
				(not (pkg_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p1) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p2) 
				(not (pkg_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p2) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p3) 
				(not (pkg_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p3) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p4_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p4) 
				(not (pkg_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p4_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p4) 
				(oneof (and) (ok))
			)
	)
	(:action move1_2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_2_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_4_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_3_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l4) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(active_request) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (robot_at desk)) 
				(oneof (and) (ok))
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action request_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(not (active_request)) 
				(not (pkg_stored)) 
				(pkg_served) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(holding p1) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p1)) 
				(not (want p1)) 
				(not (holding p1)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p1) 
				(want p1) 
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p1) 
				(not (pkg_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p1) 
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l1) 
				(pkg_stored) 
				(in_store p1) 
				(not (holding p1)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p1)) 
				(want p1) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p1) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(holding p2) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p2)) 
				(not (want p2)) 
				(not (holding p2)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p2) 
				(want p2) 
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p2) 
				(not (pkg_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p2) 
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l2) 
				(pkg_stored) 
				(in_store p2) 
				(not (holding p2)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p2)) 
				(want p2) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p2) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(holding p3) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p3)) 
				(not (want p3)) 
				(not (holding p3)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p3) 
				(want p3) 
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p3) 
				(not (pkg_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p3) 
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l3) 
				(pkg_stored) 
				(in_store p3) 
				(not (holding p3)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p3)) 
				(want p3) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p3) 
				(oneof (and) (ok))
			)
	)
	(:action serve-p4_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(holding p4) 
				(robot_at desk) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_served) 
				(not (pkg_requested)) 
				(not (active_request)) 
				(not (in_store p4)) 
				(not (want p4)) 
				(not (holding p4)) 
				(oneof (and) (ok))
			)
	)
	(:action pickup-p4_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(in_store p4) 
				(want p4) 
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(holding p4) 
				(not (pkg_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action putdown-p4_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(pkg_arrived) 
				(holding p4) 
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(pkg_at l4) 
				(pkg_stored) 
				(in_store p4) 
				(not (holding p4)) 
				(not (pkg_arrived)) 
				(not (active_request)) 
				(oneof (and) (ok))
			)
	)
	(:action restock-p4_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (pkg_served)) 
				(not (in_store p4)) 
				(want p4) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(in_store p4) 
				(oneof (and) (ok))
			)
	)
	(:action move1_2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move2_3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l2) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (robot_at l2)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_2_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l2) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move3_4_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l3) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l4) 
				(not (robot_at l3)) 
				(oneof (and) (ok))
			)
	)
	(:action move4_3_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l4) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l3) 
				(not (robot_at l4)) 
				(oneof (and) (ok))
			)
	)
	(:action movedesk_l1_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(active_request) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at l1) 
				(not (robot_at desk)) 
				(oneof (and) (ok))
			)
	)
	(:action movel1_desk_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at l1) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(robot_at desk) 
				(not (robot_at l1)) 
				(oneof (and) (ok))
			)
	)
	(:action request_buchi_move_from_BA-S1-0_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(robot_at desk) 
				(not (active_request)) 
				(pkg_stored) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (pkg_served)) 
				(not (pkg_stored)) 
				(active_request) 
				(oneof (and (pkg_requested) (want p1) )(and (pkg_requested) (want p2) )(and (pkg_requested) (want p3) )(and (pkg_requested) (want p4) )(and (pkg_arrived) (holding p1) )(and (pkg_arrived) (holding p2) )(and (pkg_arrived) (holding p3) )(and (pkg_arrived) (holding p4) ) ) 
				(oneof (and) (ok))
			)
	)
)

