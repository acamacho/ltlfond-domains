
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lift for a 3-floor building
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Goal formula:
;;
;; AE((req f1) -> (at f1))
;; &
;; AE((req f2) -> (at f2))
;; &
;; AE((req f3) -> (at f3))
;; &
;; AE((at f1) | ( called))
;;
;; (Same as in Piterman-etal@VMCAI06)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ([] <> (req.f1 -> at.f1 )) & ([] <> (req.f2 -> at.f2 )) & ([] <> (req.f3 -> at.f3 )) & ([] <> (at.f1 | called ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain lift)
	(:requirements :strips :typing :equality)
	(:types 
		floor
		baState ;;BAencoding
	)
	(:constants
		f1 f2 f3 - floor
		BA-S0 BA-S1 BA-S2 BA-S3 BA-S4  - baState ;;BAencoding
	)
	(:predicates
		(at ?f  - floor)
		(req ?f  - floor)
		(turn ?f  - floor)
		(check)
		(called)
		(move)
		(currentBAstate ?s  - baState) ;;BAencoding
		(ok) ;;BAencoding
	)

	(:action push_f1
		:precondition
			(and 
				(turn f1)
				(not (check))
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
				(oneof
					(and)
					(req f1)
				)
			)
	)
	(:action push_f2
		:precondition
			(and 
				(turn f2)
				(not (check))
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
				(oneof
					(and)
					(req f2)
				)
			)
	)
	(:action push_f3
		:precondition
			(and 
				(turn f3)
				(not (check))
			)
		:effect
			(and
				(not (turn f3))
				(check)
				(turn f1)
				(oneof
					(and)
					(req f3)
				)
			)
	)
	(:action check_called_true_f1
		:precondition
			(and 
				(check)
				(turn f1)
				(req f1)
			)
		:effect
			(and
				(called)
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_false_f1
		:precondition
			(and 
				(not (req f1))
				(check)
				(turn f1)
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_true_f2
		:precondition
			(and 
				(check)
				(turn f2)
				(req f2)
			)
		:effect
			(and
				(called)
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_false_f2
		:precondition
			(and 
				(not (req f2))
				(check)
				(turn f2)
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_true_f3
		:precondition
			(and 
				(check)
				(turn f3)
				(req f3)
			)
		:effect
			(and
				(called)
				(not (turn f3))
				(not (check))
				(move)
			)
	)
	(:action check_called_false_f3
		:precondition
			(and 
				(not (req f3))
				(check)
				(turn f3)
			)
		:effect
			(and
				(not (turn f3))
				(not (check))
				(move)
			)
	)
	(:action move_up_from_f1
		:precondition
			(and
				(at f1)
				(not (req f2))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f1
		:precondition
			(and
				(at f1)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(not (req f2))
				(not (move))
			)
	)
	(:action move_up_from_f2
		:precondition
			(and
				(at f2)
				(not (req f3))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(not (req f3))
				(not (move))
			)
	)
	(:action move_down_from_f3
		:precondition
			(and
				(at f3)
				(not (req f2))
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f3
		:precondition
			(and
				(at f3)
				(req f2)
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(not (req f2))
				(not (move))
			)
	)
	(:action move_down_from_f2
		:precondition
			(and
				(at f2)
				(not (req f1))
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f1)
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(not (req f1))
				(not (move))
			)
	)
	(:action stay_and_serve_at_f1
		:precondition
			(and
				(at f1)
				(req f1)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f1))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f2
		:precondition
			(and
				(at f2)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f2))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f3
		:precondition
			(and
				(at f3)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f3))
				(not (move))
				(turn f1)
			)
	)
	(:action no_op
		:precondition
			(and (move))
		:effect
			(and (not (move)))
	)
	(:action buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(at f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(at f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
)

