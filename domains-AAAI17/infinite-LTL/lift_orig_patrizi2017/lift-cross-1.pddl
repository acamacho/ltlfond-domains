
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lift for a 1-floor building
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Goal formula:
;;
;; AE((req f1) -> (at f1))
;; &
;; AE((at f1) | ( called))
;;
;; (Same as in Piterman-etal@VMCAI06)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ([] <> (req.f1 -> at.f1 )) & ([] <> (at.f1 | called ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain lift)
	(:requirements :strips :typing :equality)
	(:types 
		floor
		baState ;;BAencoding
	)
	(:constants
		f1 - floor
		BA-S0 BA-S1 BA-S2  - baState ;;BAencoding
	)
	(:predicates
		(at ?f  - floor)
		(req ?f  - floor)
		(turn ?f  - floor)
		(check)
		(called)
		(move)
		(currentBAstate ?s  - baState) ;;BAencoding
		(ok) ;;BAencoding
	)

	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-1_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-1_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-1_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-2_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-2_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-2_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-0_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S1-0_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-0_4
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-1_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S1-1_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-1_6
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-2_7
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S1-2_7
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-2_7
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S2-0_8
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S2-0_8
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S2-0_8
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (req f1)) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S2-0_9
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S2-0_9
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S2-0_9
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (req f1)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S2-2_10
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S2-2_10
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S2-2_10
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (req f1)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) ) ) 
			)
	)
)

