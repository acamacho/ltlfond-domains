
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lift for a 6-floor building
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Goal formula:
;;
;; AE((req f1) -> (at f1))
;; &
;; AE((req f2) -> (at f2))
;; &
;; AE((req f3) -> (at f3))
;; &
;; AE((req f4) -> (at f4))
;; &
;; AE((req f5) -> (at f5))
;; &
;; AE((req f6) -> (at f6))
;; &
;; AE((at f1) | ( called))
;;
;; (Same as in Piterman-etal@VMCAI06)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ([] <> (req.f1 -> at.f1 )) & ([] <> (req.f2 -> at.f2 )) & ([] <> (req.f3 -> at.f3 )) & ([] <> (req.f4 -> at.f4 )) & ([] <> (req.f5 -> at.f5 )) & ([] <> (req.f6 -> at.f6 )) & ([] <> (at.f1 | called ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain lift)
	(:requirements :strips :typing :equality)
	(:types 
		floor
		baState ;;BAencoding
	)
	(:constants
		f1 f2 f3 f4 f5 f6 - floor
		BA-S0 BA-S1 BA-S2 BA-S3 BA-S4 BA-S5 BA-S6 BA-S7  - baState ;;BAencoding
	)
	(:predicates
		(at ?f  - floor)
		(req ?f  - floor)
		(turn ?f  - floor)
		(check)
		(called)
		(move)
		(currentBAstate ?s  - baState) ;;BAencoding
		(ok) ;;BAencoding
	)

	(:action push_f1
		:precondition
			(and 
				(turn f1)
				(not (check))
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
				(oneof
					(and)
					(req f1)
				)
			)
	)
	(:action push_f2
		:precondition
			(and 
				(turn f2)
				(not (check))
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
				(oneof
					(and)
					(req f2)
				)
			)
	)
	(:action push_f3
		:precondition
			(and 
				(turn f3)
				(not (check))
			)
		:effect
			(and
				(not (turn f3))
				(turn f4)
				(oneof
					(and)
					(req f3)
				)
			)
	)
	(:action push_f4
		:precondition
			(and 
				(turn f4)
				(not (check))
			)
		:effect
			(and
				(not (turn f4))
				(turn f5)
				(oneof
					(and)
					(req f4)
				)
			)
	)
	(:action push_f5
		:precondition
			(and 
				(turn f5)
				(not (check))
			)
		:effect
			(and
				(not (turn f5))
				(turn f6)
				(oneof
					(and)
					(req f5)
				)
			)
	)
	(:action push_f6
		:precondition
			(and 
				(turn f6)
				(not (check))
			)
		:effect
			(and
				(not (turn f6))
				(check)
				(turn f1)
				(oneof
					(and)
					(req f6)
				)
			)
	)
	(:action check_called_true_f1
		:precondition
			(and 
				(check)
				(turn f1)
				(req f1)
			)
		:effect
			(and
				(called)
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_false_f1
		:precondition
			(and 
				(not (req f1))
				(check)
				(turn f1)
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_true_f2
		:precondition
			(and 
				(check)
				(turn f2)
				(req f2)
			)
		:effect
			(and
				(called)
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_false_f2
		:precondition
			(and 
				(not (req f2))
				(check)
				(turn f2)
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_true_f3
		:precondition
			(and 
				(check)
				(turn f3)
				(req f3)
			)
		:effect
			(and
				(called)
				(not (turn f3))
				(turn f4)
			)
	)
	(:action check_called_false_f3
		:precondition
			(and 
				(not (req f3))
				(check)
				(turn f3)
			)
		:effect
			(and
				(not (turn f3))
				(turn f4)
			)
	)
	(:action check_called_true_f4
		:precondition
			(and 
				(check)
				(turn f4)
				(req f4)
			)
		:effect
			(and
				(called)
				(not (turn f4))
				(turn f5)
			)
	)
	(:action check_called_false_f4
		:precondition
			(and 
				(not (req f4))
				(check)
				(turn f4)
			)
		:effect
			(and
				(not (turn f4))
				(turn f5)
			)
	)
	(:action check_called_true_f5
		:precondition
			(and 
				(check)
				(turn f5)
				(req f5)
			)
		:effect
			(and
				(called)
				(not (turn f5))
				(turn f6)
			)
	)
	(:action check_called_false_f5
		:precondition
			(and 
				(not (req f5))
				(check)
				(turn f5)
			)
		:effect
			(and
				(not (turn f5))
				(turn f6)
			)
	)
	(:action check_called_true_f6
		:precondition
			(and 
				(check)
				(turn f6)
				(req f6)
			)
		:effect
			(and
				(called)
				(not (turn f6))
				(not (check))
				(move)
			)
	)
	(:action check_called_false_f6
		:precondition
			(and 
				(not (req f6))
				(check)
				(turn f6)
			)
		:effect
			(and
				(not (turn f6))
				(not (check))
				(move)
			)
	)
	(:action move_up_from_f1
		:precondition
			(and
				(at f1)
				(not (req f2))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f1
		:precondition
			(and
				(at f1)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(not (req f2))
				(not (move))
			)
	)
	(:action move_up_from_f2
		:precondition
			(and
				(at f2)
				(not (req f3))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(not (req f3))
				(not (move))
			)
	)
	(:action move_up_from_f3
		:precondition
			(and
				(at f3)
				(not (req f4))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f4)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f3
		:precondition
			(and
				(at f3)
				(req f4)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f4)
				(not (req f4))
				(not (move))
			)
	)
	(:action move_up_from_f4
		:precondition
			(and
				(at f4)
				(not (req f5))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f5)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f4
		:precondition
			(and
				(at f4)
				(req f5)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f5)
				(not (req f5))
				(not (move))
			)
	)
	(:action move_up_from_f5
		:precondition
			(and
				(at f5)
				(not (req f6))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f5))
				(at f6)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f5
		:precondition
			(and
				(at f5)
				(req f6)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f5))
				(at f6)
				(not (req f6))
				(not (move))
			)
	)
	(:action move_down_from_f6
		:precondition
			(and
				(at f6)
				(not (req f5))
				(move)
			)
		:effect
			(and
				(not (at f6))
				(at f5)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f6
		:precondition
			(and
				(at f6)
				(req f5)
				(move)
			)
		:effect
			(and
				(not (at f6))
				(at f5)
				(not (req f5))
				(not (move))
			)
	)
	(:action move_down_from_f5
		:precondition
			(and
				(at f5)
				(not (req f4))
				(move)
			)
		:effect
			(and
				(not (at f5))
				(at f4)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f5
		:precondition
			(and
				(at f5)
				(req f4)
				(move)
			)
		:effect
			(and
				(not (at f5))
				(at f4)
				(not (req f4))
				(not (move))
			)
	)
	(:action move_down_from_f4
		:precondition
			(and
				(at f4)
				(not (req f3))
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f3)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f4
		:precondition
			(and
				(at f4)
				(req f3)
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f3)
				(not (req f3))
				(not (move))
			)
	)
	(:action move_down_from_f3
		:precondition
			(and
				(at f3)
				(not (req f2))
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f3
		:precondition
			(and
				(at f3)
				(req f2)
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(not (req f2))
				(not (move))
			)
	)
	(:action move_down_from_f2
		:precondition
			(and
				(at f2)
				(not (req f1))
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f1)
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(not (req f1))
				(not (move))
			)
	)
	(:action stay_and_serve_at_f1
		:precondition
			(and
				(at f1)
				(req f1)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f1))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f2
		:precondition
			(and
				(at f2)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f2))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f3
		:precondition
			(and
				(at f3)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f3))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f4
		:precondition
			(and
				(at f4)
				(req f4)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f4))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f5
		:precondition
			(and
				(at f5)
				(req f5)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f5))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f6
		:precondition
			(and
				(at f6)
				(req f6)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f6))
				(not (move))
				(turn f1)
			)
	)
	(:action no_op
		:precondition
			(and (move))
		:effect
			(and (not (move)))
	)
	(:action buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_8
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_9
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_10
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_11
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_12
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_13
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_14
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_15
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_16
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_17
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_18
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_19
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_20
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_21
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_22
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_23
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_24
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_25
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_26
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_27
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_28
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_29
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_30
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_31
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_32
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_33
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_34
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_35
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_36
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_37
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_38
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_39
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_40
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_41
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_42
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_43
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_44
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_45
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_46
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_47
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_48
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_49
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_50
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_51
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_52
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_53
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_54
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_55
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_56
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_57
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_58
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_59
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_60
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_61
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_62
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_63
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-1_64
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_65
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_66
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_67
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_68
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_69
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_70
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_71
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_72
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_73
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_74
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_75
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_76
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_77
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_78
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_79
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_80
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_81
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_82
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_83
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_84
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_85
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_86
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_87
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_88
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_89
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_90
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_91
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_92
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_93
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_94
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_95
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-2_96
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_97
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_98
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_99
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_100
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_101
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_102
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_103
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_104
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_105
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_106
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_107
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_108
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_109
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_110
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_111
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_112
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_113
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_114
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_115
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_116
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_117
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_118
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_119
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_120
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_121
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_122
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_123
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_124
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_125
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_126
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_127
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-3_128
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_129
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_130
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_131
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_132
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_133
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_134
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_135
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_136
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_137
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_138
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_139
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_140
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_141
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_142
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_143
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-4_144
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-4_145
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-4_146
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-4_147
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-4_148
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-4_149
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-4_150
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-4_151
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-5_152
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f4)) 
				(req f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S0-5_153
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f4)) 
				(req f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S0-5_154
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f4)) 
				(req f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S0-5_155
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f4)) 
				(req f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S0-6_156
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f5)) 
				(req f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S6)
			)
	)
	(:action buchi_move_from_BA-S0-6_157
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f5)) 
				(req f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S6)
			)
	)
	(:action buchi_move_from_BA-S0-7_158
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f6)) 
				(req f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S7)
			)
	)
	(:action buchi_move_from_BA-S1-0_159
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-0_160
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(at f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-1_161
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S1-2_162
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S2-0_163
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S2-0_164
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(at f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S2-2_165
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S3-0_166
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_167
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_168
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_169
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-1_170
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S3-1_171
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S3-2_172
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S3-2_173
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S3-3_174
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S4-0_175
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_176
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_177
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_178
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_179
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_180
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_181
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_182
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-1_183
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_184
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_185
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_186
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-2_187
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_188
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_189
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_190
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-3_191
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S4-3_192
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S4-4_193
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S5-0_194
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_195
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_196
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_197
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_198
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_199
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_200
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_201
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_202
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_203
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_204
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_205
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_206
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_207
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_208
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_209
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-1_210
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_211
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_212
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_213
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_214
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_215
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_216
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_217
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-2_218
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_219
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_220
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_221
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_222
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_223
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_224
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_225
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-3_226
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-3_227
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-3_228
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-3_229
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-4_230
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S5-4_231
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f3)) 
				(req f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S5-5_232
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f4)) 
				(req f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S6-0_233
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_234
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_235
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_236
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_237
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_238
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_239
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_240
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_241
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_242
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_243
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_244
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_245
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_246
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_247
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_248
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_249
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_250
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_251
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_252
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_253
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_254
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_255
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_256
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_257
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_258
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_259
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_260
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_261
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_262
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_263
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_264
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-1_265
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_266
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_267
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_268
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_269
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_270
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_271
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_272
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_273
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_274
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_275
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_276
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_277
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_278
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_279
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_280
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-2_281
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_282
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_283
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_284
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_285
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_286
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_287
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_288
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_289
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_290
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_291
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_292
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_293
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_294
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_295
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_296
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-3_297
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_298
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_299
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_300
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_301
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_302
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_303
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_304
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-4_305
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S6-4_306
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S6-4_307
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S6-4_308
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S6-5_309
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f4)) 
				(req f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S6-5_310
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f4)) 
				(req f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S6-6_311
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f5)) 
				(req f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S7-0_312
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_313
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_314
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_315
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_316
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_317
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_318
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_319
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_320
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_321
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_322
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_323
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_324
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_325
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_326
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_327
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_328
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_329
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_330
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_331
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_332
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_333
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_334
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_335
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_336
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_337
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_338
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_339
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_340
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_341
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_342
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_343
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_344
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_345
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_346
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_347
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_348
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_349
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_350
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_351
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_352
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_353
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_354
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_355
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_356
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_357
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_358
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_359
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_360
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_361
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_362
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_363
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_364
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_365
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_366
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_367
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_368
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_369
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_370
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_371
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_372
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_373
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_374
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-0_375
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S7-1_376
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_377
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_378
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_379
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_380
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_381
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_382
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_383
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_384
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_385
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_386
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_387
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_388
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_389
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_390
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_391
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_392
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_393
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_394
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_395
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_396
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_397
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_398
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_399
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_400
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_401
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_402
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_403
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_404
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_405
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_406
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-1_407
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S7-2_408
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_409
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_410
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_411
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_412
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_413
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_414
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_415
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_416
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_417
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_418
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_419
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_420
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_421
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_422
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_423
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_424
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_425
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_426
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_427
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_428
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_429
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_430
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_431
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_432
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_433
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_434
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_435
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_436
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_437
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_438
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-2_439
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S7-3_440
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_441
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_442
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_443
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_444
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_445
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_446
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_447
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_448
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_449
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_450
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_451
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_452
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_453
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_454
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-3_455
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S7-4_456
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S7-4_457
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S7-4_458
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S7-4_459
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S7-4_460
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S7-4_461
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S7-4_462
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S7-4_463
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S7-5_464
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f4)) 
				(req f4) 
				(not (at f5)) 
				(not (req f5)) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S7-5_465
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f4)) 
				(req f4) 
				(not (at f5)) 
				(not (req f5)) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S7-5_466
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f4)) 
				(req f4) 
				(at f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S7-5_467
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f4)) 
				(req f4) 
				(at f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S7-6_468
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f5)) 
				(req f5) 
				(not (at f6)) 
				(not (req f6)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S6)
			)
	)
	(:action buchi_move_from_BA-S7-6_469
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f5)) 
				(req f5) 
				(at f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S7))
				(currentBAstate BA-S6)
			)
	)
	(:action buchi_move_from_BA-S7-7_470
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (turn f6))
				(not (check))
				(not (move))
				(currentBAstate BA-S7)
				(not (at f6)) 
				(req f6) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
)

