
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lift for a 2-floor building
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Goal formula:
;;
;; AE((req f1) -> (at f1))
;; &
;; AE((req f2) -> (at f2))
;; &
;; AE((at f1) | ( called))
;;
;; (Same as in Piterman-etal@VMCAI06)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ([] <> (req.f1 -> at.f1 )) & ([] <> (req.f2 -> at.f2 )) & ([] <> (at.f1 | called ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain lift)
	(:requirements :strips :typing :equality)
	(:types 
		floor
		baState ;;BAencoding
	)
	(:constants
		f1 f2 - floor
		BA-S0 BA-S1 BA-S2 BA-S3  - baState ;;BAencoding
	)
	(:predicates
		(at ?f  - floor)
		(req ?f  - floor)
		(turn ?f  - floor)
		(check)
		(called)
		(move)
		(currentBAstate ?s  - baState) ;;BAencoding
		(ok) ;;BAencoding
	)

	(:action move_up_from_f1_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-1_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-1_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-1_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-1_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-1_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-1_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-1_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-1_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-1_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-1_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-1_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-1_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-1_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-1_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-1_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-1_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-2_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-2_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-2_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-2_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-2_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-2_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-2_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-2_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-2_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-2_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-2_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-2_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-2_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-2_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-2_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-2_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-3_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-3_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-3_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-3_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-3_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-3_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-3_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-3_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S1-0_9
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S1-0_9
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S1-0_9
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S1-0_9
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-0_9
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S1-0_9
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S1-0_9
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-0_9
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S1-0_10
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S1-0_10
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S1-0_10
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f1)) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S1-0_10
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f1) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-0_10
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S1-0_10
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S1-0_10
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-0_10
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S1-1_11
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S1-1_11
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S1-1_11
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S1-1_11
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-1_11
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S1-1_11
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S1-1_11
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-1_11
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S1-2_12
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S1-2_12
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S1-2_12
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S1-2_12
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-2_12
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S1-2_12
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S1-2_12
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-2_12
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S2-0_13
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S2-0_13
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S2-0_13
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S2-0_13
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S2-0_13
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S2-0_13
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S2-0_13
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S2-0_13
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S2-0_14
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S2-0_14
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S2-0_14
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(not (req f1)) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S2-0_14
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f1) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S2-0_14
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S2-0_14
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S2-0_14
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S2-0_14
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S2-2_15
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S2-2_15
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S2-2_15
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S2-2_15
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S2-2_15
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S2-2_15
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S2-2_15
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S2-2_15
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-0_16
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-0_16
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-0_16
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-0_16
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-0_16
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-0_16
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S3-0_16
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-0_16
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-0_17
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-0_17
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-0_17
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-0_17
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-0_17
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-0_17
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S3-0_17
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-0_17
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-0_18
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-0_18
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-0_18
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-0_18
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-0_18
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-0_18
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S3-0_18
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-0_18
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-0_19
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-0_19
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-0_19
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-0_19
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-0_19
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-0_19
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S3-0_19
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-0_19
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-1_20
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-1_20
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-1_20
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-1_20
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-1_20
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-1_20
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S3-1_20
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-1_20
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-1_21
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-1_21
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-1_21
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-1_21
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-1_21
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-1_21
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S3-1_21
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-1_21
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-2_22
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-2_22
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-2_22
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-2_22
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-2_22
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-2_22
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S3-2_22
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-2_22
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-2_23
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-2_23
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-2_23
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-2_23
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-2_23
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-2_23
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S3-2_23
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-2_23
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-3_24
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-3_24
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-3_24
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-3_24
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-3_24
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-3_24
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S3-3_24
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-3_24
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) ) ) 
			)
	)
)

