
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lift for a 3-floor building
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Goal formula:
;;
;; AE((req f1) -> (at f1))
;; &
;; AE((req f2) -> (at f2))
;; &
;; AE((req f3) -> (at f3))
;; &
;; AE((at f1) | ( called))
;;
;; (Same as in Piterman-etal@VMCAI06)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ([] <> (req.f1 -> at.f1 )) & ([] <> (req.f2 -> at.f2 )) & ([] <> (req.f3 -> at.f3 )) & ([] <> (at.f1 | called ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain lift)
	(:requirements :strips :typing :equality)
	(:types 
		floor
		baState ;;BAencoding
	)
	(:constants
		f1 f2 f3 - floor
		BA-S0 BA-S1 BA-S2 BA-S3 BA-S4  - baState ;;BAencoding
	)
	(:predicates
		(at ?f  - floor)
		(req ?f  - floor)
		(turn ?f  - floor)
		(check)
		(called)
		(move)
		(currentBAstate ?s  - baState) ;;BAencoding
		(ok) ;;BAencoding
	)

	(:action move_up_from_f1_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-1_8
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-1_9
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-1_10
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-1_11
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-2_12
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-2_13
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-2_14
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-2_15
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-3_16
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-3_17
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(not (req f2)) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f2) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(not (req f1)) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f1) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S0-4_18
		:precondition
			(and
				(currentBAstate BA-S0)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-0_19
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(not (req f2)) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(req f2) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f1)) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f1) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-0_20
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-1_21
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S1-2_22
		:precondition
			(and
				(currentBAstate BA-S1)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S2-0_23
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f3) 
				(not (req f2)) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f3) 
				(req f2) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(not (req f1)) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f1) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S2-0_24
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
			)
		:effect
			(and
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S2-2_25
		:precondition
			(and
				(currentBAstate BA-S2)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-0_26
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-0_27
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(not (req f2)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f2) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-0_28
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(not (req f2)) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f2) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-0_29
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-1_30
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-1_31
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-2_32
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-2_33
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(not (req f2)) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f2) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(not (req f1)) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f1) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S3-3_34
		:precondition
			(and
				(currentBAstate BA-S3)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-0_35
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-0_36
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-0_37
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-0_38
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-0_39
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-0_40
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-0_41
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action no_op__buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-0_42
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
				(oneof (and) (ok))
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-1_43
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-1_44
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-1_45
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-1_46
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-2_47
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-2_48
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-2_49
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-2_50
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-3_51
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-3_52
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f1_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(not (req f2)) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f1_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f2) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (at f1)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_from_f2_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f3)) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_up_and_serve_from_f2_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f3) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f3)
 
				(not (req f3)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f3_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(not (req f2)) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f3_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f2) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (at f3)) 
				(at f2)
 
				(not (req f2)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_from_f2_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(not (req f1)) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action move_down_and_serve_from_f2_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f1) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (at f2)) 
				(at f1)
 
				(not (req f1)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f1_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f1) 
				(req f1) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (req f1)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f2_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f2) 
				(req f2) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (req f2)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action stay_and_serve_at_f3_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(at f3) 
				(req f3) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (req f3)) 
				(turn f1) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action no_op__buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
	(:action cancel_called_buchi_move_from_BA-S4-4_53
		:precondition
			(and
				(currentBAstate BA-S4)
				(not (req f1)) 
				(not (req f2)) 
				(not (req f3)) 
				(called) 
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(not (called)) 
				(oneof (and) (and  (called) )(and (req f1)  (called) )(and (req f2)  (called) )(and (req f1) (req f2)  (called) )(and (req f3)  (called) )(and (req f1) (req f3)  (called) )(and (req f2) (req f3)  (called) )(and (req f1) (req f2) (req f3)  (called) ) ) 
			)
	)
)

