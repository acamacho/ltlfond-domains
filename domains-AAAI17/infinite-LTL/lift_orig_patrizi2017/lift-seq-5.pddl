
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lift for a 5-floor building
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Goal formula:
;;
;; AE((req f1) -> (at f1))
;; &
;; AE((req f2) -> (at f2))
;; &
;; AE((req f3) -> (at f3))
;; &
;; AE((req f4) -> (at f4))
;; &
;; AE((req f5) -> (at f5))
;; &
;; AE((at f1) | ( called))
;;
;; (Same as in Piterman-etal@VMCAI06)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ([] <> (req.f1 -> at.f1 )) & ([] <> (req.f2 -> at.f2 )) & ([] <> (req.f3 -> at.f3 )) & ([] <> (req.f4 -> at.f4 )) & ([] <> (req.f5 -> at.f5 )) & ([] <> (at.f1 | called ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain lift)
	(:requirements :strips :typing :equality)
	(:types 
		floor
		baState ;;BAencoding
	)
	(:constants
		f1 f2 f3 f4 f5 - floor
		BA-S0 BA-S1 BA-S2 BA-S3 BA-S4 BA-S5 BA-S6  - baState ;;BAencoding
	)
	(:predicates
		(at ?f  - floor)
		(req ?f  - floor)
		(turn ?f  - floor)
		(check)
		(called)
		(move)
		(currentBAstate ?s  - baState) ;;BAencoding
		(ok) ;;BAencoding
	)

	(:action push_f1
		:precondition
			(and 
				(turn f1)
				(not (check))
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
				(oneof
					(and)
					(req f1)
				)
			)
	)
	(:action push_f2
		:precondition
			(and 
				(turn f2)
				(not (check))
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
				(oneof
					(and)
					(req f2)
				)
			)
	)
	(:action push_f3
		:precondition
			(and 
				(turn f3)
				(not (check))
			)
		:effect
			(and
				(not (turn f3))
				(turn f4)
				(oneof
					(and)
					(req f3)
				)
			)
	)
	(:action push_f4
		:precondition
			(and 
				(turn f4)
				(not (check))
			)
		:effect
			(and
				(not (turn f4))
				(turn f5)
				(oneof
					(and)
					(req f4)
				)
			)
	)
	(:action push_f5
		:precondition
			(and 
				(turn f5)
				(not (check))
			)
		:effect
			(and
				(not (turn f5))
				(check)
				(turn f1)
				(oneof
					(and)
					(req f5)
				)
			)
	)
	(:action check_called_true_f1
		:precondition
			(and 
				(check)
				(turn f1)
				(req f1)
			)
		:effect
			(and
				(called)
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_false_f1
		:precondition
			(and 
				(not (req f1))
				(check)
				(turn f1)
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_true_f2
		:precondition
			(and 
				(check)
				(turn f2)
				(req f2)
			)
		:effect
			(and
				(called)
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_false_f2
		:precondition
			(and 
				(not (req f2))
				(check)
				(turn f2)
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_true_f3
		:precondition
			(and 
				(check)
				(turn f3)
				(req f3)
			)
		:effect
			(and
				(called)
				(not (turn f3))
				(turn f4)
			)
	)
	(:action check_called_false_f3
		:precondition
			(and 
				(not (req f3))
				(check)
				(turn f3)
			)
		:effect
			(and
				(not (turn f3))
				(turn f4)
			)
	)
	(:action check_called_true_f4
		:precondition
			(and 
				(check)
				(turn f4)
				(req f4)
			)
		:effect
			(and
				(called)
				(not (turn f4))
				(turn f5)
			)
	)
	(:action check_called_false_f4
		:precondition
			(and 
				(not (req f4))
				(check)
				(turn f4)
			)
		:effect
			(and
				(not (turn f4))
				(turn f5)
			)
	)
	(:action check_called_true_f5
		:precondition
			(and 
				(check)
				(turn f5)
				(req f5)
			)
		:effect
			(and
				(called)
				(not (turn f5))
				(not (check))
				(move)
			)
	)
	(:action check_called_false_f5
		:precondition
			(and 
				(not (req f5))
				(check)
				(turn f5)
			)
		:effect
			(and
				(not (turn f5))
				(not (check))
				(move)
			)
	)
	(:action move_up_from_f1
		:precondition
			(and
				(at f1)
				(not (req f2))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f1
		:precondition
			(and
				(at f1)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(not (req f2))
				(not (move))
			)
	)
	(:action move_up_from_f2
		:precondition
			(and
				(at f2)
				(not (req f3))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(not (req f3))
				(not (move))
			)
	)
	(:action move_up_from_f3
		:precondition
			(and
				(at f3)
				(not (req f4))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f4)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f3
		:precondition
			(and
				(at f3)
				(req f4)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f4)
				(not (req f4))
				(not (move))
			)
	)
	(:action move_up_from_f4
		:precondition
			(and
				(at f4)
				(not (req f5))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f5)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f4
		:precondition
			(and
				(at f4)
				(req f5)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f5)
				(not (req f5))
				(not (move))
			)
	)
	(:action move_down_from_f5
		:precondition
			(and
				(at f5)
				(not (req f4))
				(move)
			)
		:effect
			(and
				(not (at f5))
				(at f4)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f5
		:precondition
			(and
				(at f5)
				(req f4)
				(move)
			)
		:effect
			(and
				(not (at f5))
				(at f4)
				(not (req f4))
				(not (move))
			)
	)
	(:action move_down_from_f4
		:precondition
			(and
				(at f4)
				(not (req f3))
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f3)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f4
		:precondition
			(and
				(at f4)
				(req f3)
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f3)
				(not (req f3))
				(not (move))
			)
	)
	(:action move_down_from_f3
		:precondition
			(and
				(at f3)
				(not (req f2))
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f3
		:precondition
			(and
				(at f3)
				(req f2)
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(not (req f2))
				(not (move))
			)
	)
	(:action move_down_from_f2
		:precondition
			(and
				(at f2)
				(not (req f1))
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f1)
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(not (req f1))
				(not (move))
			)
	)
	(:action stay_and_serve_at_f1
		:precondition
			(and
				(at f1)
				(req f1)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f1))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f2
		:precondition
			(and
				(at f2)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f2))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f3
		:precondition
			(and
				(at f3)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f3))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f4
		:precondition
			(and
				(at f4)
				(req f4)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f4))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f5
		:precondition
			(and
				(at f5)
				(req f5)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f5))
				(not (move))
				(turn f1)
			)
	)
	(:action no_op
		:precondition
			(and (move))
		:effect
			(and (not (move)))
	)
	(:action buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_8
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_9
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_10
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_11
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_12
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_13
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_14
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_15
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_16
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_17
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_18
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_19
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_20
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_21
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_22
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_23
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_24
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_25
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_26
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_27
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_28
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_29
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_30
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_31
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-1_32
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_33
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_34
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_35
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_36
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_37
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_38
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_39
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_40
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_41
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_42
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_43
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_44
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_45
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_46
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_47
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-2_48
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_49
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_50
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_51
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_52
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_53
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_54
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_55
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_56
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_57
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_58
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_59
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_60
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_61
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_62
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_63
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-3_64
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_65
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_66
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_67
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_68
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_69
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_70
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_71
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-4_72
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-4_73
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-4_74
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-4_75
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-5_76
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f4)) 
				(req f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S0-5_77
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f4)) 
				(req f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S0-6_78
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f5)) 
				(req f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S6)
			)
	)
	(:action buchi_move_from_BA-S1-0_79
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-0_80
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(at f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-1_81
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S1-2_82
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S2-0_83
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S2-0_84
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(at f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S2-2_85
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S3-0_86
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_87
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_88
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_89
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-1_90
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S3-1_91
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S3-2_92
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S3-2_93
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S3-3_94
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S4-0_95
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_96
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_97
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_98
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_99
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_100
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_101
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_102
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-1_103
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_104
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_105
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_106
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-2_107
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_108
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_109
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_110
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-3_111
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S4-3_112
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S4-4_113
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S5-0_114
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_115
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_116
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_117
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_118
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_119
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_120
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_121
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_122
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_123
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_124
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_125
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_126
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_127
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_128
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_129
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-1_130
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_131
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_132
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_133
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_134
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_135
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_136
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_137
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-2_138
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_139
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_140
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_141
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_142
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_143
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_144
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_145
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-3_146
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-3_147
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-3_148
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-3_149
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-4_150
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S5-4_151
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f3)) 
				(req f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S5-5_152
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f4)) 
				(req f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S6-0_153
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_154
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_155
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_156
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_157
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_158
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_159
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_160
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_161
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_162
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_163
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_164
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_165
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_166
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_167
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_168
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_169
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_170
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_171
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_172
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_173
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_174
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_175
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_176
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_177
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_178
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_179
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_180
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_181
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_182
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_183
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-0_184
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S6-1_185
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_186
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_187
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_188
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_189
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_190
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_191
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_192
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_193
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_194
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_195
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_196
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_197
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_198
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_199
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-1_200
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S6-2_201
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_202
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_203
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_204
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_205
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_206
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_207
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_208
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_209
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_210
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_211
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_212
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_213
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_214
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_215
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-2_216
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S6-3_217
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_218
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_219
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_220
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_221
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_222
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_223
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-3_224
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S6-4_225
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S6-4_226
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S6-4_227
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S6-4_228
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f3)) 
				(req f3) 
				(at f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S6-5_229
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f4)) 
				(req f4) 
				(not (at f5)) 
				(not (req f5)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S6-5_230
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f4)) 
				(req f4) 
				(at f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S6))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S6-6_231
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (turn f5))
				(not (check))
				(not (move))
				(currentBAstate BA-S6)
				(not (at f5)) 
				(req f5) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
)

