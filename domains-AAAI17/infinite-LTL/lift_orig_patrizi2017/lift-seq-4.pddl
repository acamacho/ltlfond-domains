
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lift for a 4-floor building
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Goal formula:
;;
;; AE((req f1) -> (at f1))
;; &
;; AE((req f2) -> (at f2))
;; &
;; AE((req f3) -> (at f3))
;; &
;; AE((req f4) -> (at f4))
;; &
;; AE((at f1) | ( called))
;;
;; (Same as in Piterman-etal@VMCAI06)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ([] <> (req.f1 -> at.f1 )) & ([] <> (req.f2 -> at.f2 )) & ([] <> (req.f3 -> at.f3 )) & ([] <> (req.f4 -> at.f4 )) & ([] <> (at.f1 | called ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain lift)
	(:requirements :strips :typing :equality)
	(:types 
		floor
		baState ;;BAencoding
	)
	(:constants
		f1 f2 f3 f4 - floor
		BA-S0 BA-S1 BA-S2 BA-S3 BA-S4 BA-S5  - baState ;;BAencoding
	)
	(:predicates
		(at ?f  - floor)
		(req ?f  - floor)
		(turn ?f  - floor)
		(check)
		(called)
		(move)
		(currentBAstate ?s  - baState) ;;BAencoding
		(ok) ;;BAencoding
	)

	(:action push_f1
		:precondition
			(and 
				(turn f1)
				(not (check))
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
				(oneof
					(and)
					(req f1)
				)
			)
	)
	(:action push_f2
		:precondition
			(and 
				(turn f2)
				(not (check))
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
				(oneof
					(and)
					(req f2)
				)
			)
	)
	(:action push_f3
		:precondition
			(and 
				(turn f3)
				(not (check))
			)
		:effect
			(and
				(not (turn f3))
				(turn f4)
				(oneof
					(and)
					(req f3)
				)
			)
	)
	(:action push_f4
		:precondition
			(and 
				(turn f4)
				(not (check))
			)
		:effect
			(and
				(not (turn f4))
				(check)
				(turn f1)
				(oneof
					(and)
					(req f4)
				)
			)
	)
	(:action check_called_true_f1
		:precondition
			(and 
				(check)
				(turn f1)
				(req f1)
			)
		:effect
			(and
				(called)
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_false_f1
		:precondition
			(and 
				(not (req f1))
				(check)
				(turn f1)
			)
		:effect
			(and
				(not (turn f1))
				(turn f2)
			)
	)
	(:action check_called_true_f2
		:precondition
			(and 
				(check)
				(turn f2)
				(req f2)
			)
		:effect
			(and
				(called)
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_false_f2
		:precondition
			(and 
				(not (req f2))
				(check)
				(turn f2)
			)
		:effect
			(and
				(not (turn f2))
				(turn f3)
			)
	)
	(:action check_called_true_f3
		:precondition
			(and 
				(check)
				(turn f3)
				(req f3)
			)
		:effect
			(and
				(called)
				(not (turn f3))
				(turn f4)
			)
	)
	(:action check_called_false_f3
		:precondition
			(and 
				(not (req f3))
				(check)
				(turn f3)
			)
		:effect
			(and
				(not (turn f3))
				(turn f4)
			)
	)
	(:action check_called_true_f4
		:precondition
			(and 
				(check)
				(turn f4)
				(req f4)
			)
		:effect
			(and
				(called)
				(not (turn f4))
				(not (check))
				(move)
			)
	)
	(:action check_called_false_f4
		:precondition
			(and 
				(not (req f4))
				(check)
				(turn f4)
			)
		:effect
			(and
				(not (turn f4))
				(not (check))
				(move)
			)
	)
	(:action move_up_from_f1
		:precondition
			(and
				(at f1)
				(not (req f2))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f1
		:precondition
			(and
				(at f1)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f1))
				(at f2)
				(not (req f2))
				(not (move))
			)
	)
	(:action move_up_from_f2
		:precondition
			(and
				(at f2)
				(not (req f3))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f3)
				(not (req f3))
				(not (move))
			)
	)
	(:action move_up_from_f3
		:precondition
			(and
				(at f3)
				(not (req f4))
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f4)
				(not (move))
			)
	)
	(:action move_up_and_serve_from_f3
		:precondition
			(and
				(at f3)
				(req f4)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f4)
				(not (req f4))
				(not (move))
			)
	)
	(:action move_down_from_f4
		:precondition
			(and
				(at f4)
				(not (req f3))
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f3)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f4
		:precondition
			(and
				(at f4)
				(req f3)
				(move)
			)
		:effect
			(and
				(not (at f4))
				(at f3)
				(not (req f3))
				(not (move))
			)
	)
	(:action move_down_from_f3
		:precondition
			(and
				(at f3)
				(not (req f2))
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f3
		:precondition
			(and
				(at f3)
				(req f2)
				(move)
			)
		:effect
			(and
				(not (at f3))
				(at f2)
				(not (req f2))
				(not (move))
			)
	)
	(:action move_down_from_f2
		:precondition
			(and
				(at f2)
				(not (req f1))
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(not (move))
			)
	)
	(:action move_down_and_serve_from_f2
		:precondition
			(and
				(at f2)
				(req f1)
				(move)
			)
		:effect
			(and
				(not (at f2))
				(at f1)
				(not (req f1))
				(not (move))
			)
	)
	(:action stay_and_serve_at_f1
		:precondition
			(and
				(at f1)
				(req f1)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f1))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f2
		:precondition
			(and
				(at f2)
				(req f2)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f2))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f3
		:precondition
			(and
				(at f3)
				(req f3)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f3))
				(not (move))
				(turn f1)
			)
	)
	(:action stay_and_serve_at_f4
		:precondition
			(and
				(at f4)
				(req f4)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f4))
				(not (move))
				(turn f1)
			)
	)
	(:action no_op
		:precondition
			(and (move))
		:effect
			(and (not (move)))
	)
	(:action buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_2
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_3
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_4
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_5
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_6
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_7
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_8
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_9
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_10
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_11
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_12
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_13
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_14
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_15
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-1_16
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_17
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_18
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_19
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_20
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_21
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_22
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-1_23
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-2_24
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_25
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_26
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_27
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_28
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_29
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_30
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-2_31
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S0-3_32
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_33
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_34
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-3_35
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S0-4_36
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-4_37
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f3)) 
				(req f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S0-5_38
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f4)) 
				(req f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S5)
			)
	)
	(:action buchi_move_from_BA-S1-0_39
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-0_40
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(at f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-1_41
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S1-2_42
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S2-0_43
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S2-0_44
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(at f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S2-2_45
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S3-0_46
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_47
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_48
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-0_49
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(at f1) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S3-1_50
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S3-1_51
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(req f1) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S3-2_52
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S3-2_53
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S3))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S3-3_54
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S3)
				(not (at f2)) 
				(req f2) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S4-0_55
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_56
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_57
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_58
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_59
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_60
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_61
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-0_62
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(at f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S4-1_63
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_64
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_65
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-1_66
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S4-2_67
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_68
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_69
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-2_70
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S4-3_71
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S4-3_72
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f2)) 
				(req f2) 
				(at f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S4))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S4-4_73
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S4)
				(not (at f3)) 
				(req f3) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S5-0_74
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_75
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_76
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_77
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_78
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_79
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_80
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_81
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_82
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_83
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_84
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_85
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_86
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_87
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_88
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-0_89
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(at f1) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S5-1_90
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_91
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_92
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_93
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_94
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_95
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_96
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-1_97
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(req f1) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S5-2_98
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_99
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_100
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_101
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(not (at f2)) 
				(not (req f2)) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_102
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_103
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_104
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-2_105
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
				(at f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S5-3_106
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-3_107
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(not (at f3)) 
				(not (req f3)) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-3_108
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-3_109
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f2)) 
				(req f2) 
				(at f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S3)
			)
	)
	(:action buchi_move_from_BA-S5-4_110
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f3)) 
				(req f3) 
				(not (at f4)) 
				(not (req f4)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S5-4_111
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f3)) 
				(req f3) 
				(at f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S5))
				(currentBAstate BA-S4)
			)
	)
	(:action buchi_move_from_BA-S5-5_112
		:precondition
			(and
				(not (turn f1))
				(not (turn f2))
				(not (turn f3))
				(not (turn f4))
				(not (check))
				(not (move))
				(currentBAstate BA-S5)
				(not (at f4)) 
				(req f4) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
)

