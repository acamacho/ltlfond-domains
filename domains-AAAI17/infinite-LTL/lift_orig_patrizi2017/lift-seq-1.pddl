
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lift for a 1-floor building
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Goal formula:
;;
;; AE((req f1) -> (at f1))
;; &
;; AE((at f1) | ( called))
;;
;; (Same as in Piterman-etal@VMCAI06)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ([] <> (req.f1 -> at.f1 )) & ([] <> (at.f1 | called ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain lift)
	(:requirements :strips :typing :equality)
	(:types 
		floor
		baState ;;BAencoding
	)
	(:constants
		f1 - floor
		BA-S0 BA-S1 BA-S2  - baState ;;BAencoding
	)
	(:predicates
		(at ?f  - floor)
		(req ?f  - floor)
		(turn ?f  - floor)
		(check)
		(called)
		(move)
		(currentBAstate ?s  - baState) ;;BAencoding
		(ok) ;;BAencoding
	)

	(:action push_f1
		:precondition
			(and 
				(turn f1)
				(not (check))
			)
		:effect
			(and
				(not (turn f1))
				(check)
				(turn f1)
				(oneof
					(and)
					(req f1)
				)
			)
	)
	(:action check_called_true_f1
		:precondition
			(and 
				(check)
				(turn f1)
				(req f1)
			)
		:effect
			(and
				(called)
				(not (turn f1))
				(not (check))
				(move)
			)
	)
	(:action check_called_false_f1
		:precondition
			(and 
				(not (req f1))
				(check)
				(turn f1)
			)
		:effect
			(and
				(not (turn f1))
				(not (check))
				(move)
			)
	)
	(:action stay_and_serve_at_f1
		:precondition
			(and
				(at f1)
				(req f1)
				(called) ;; can move up only if called
				(move)
			)
		:effect
			(and
				(not (req f1))
				(not (move))
				(turn f1)
			)
	)
	(:action no_op
		:precondition
			(and (move))
		:effect
			(and (not (move)))
	)
	(:action buchi_move_from_BA-S0-0_0
		:precondition
			(and
				(not (turn f1))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-0_1
		:precondition
			(and
				(not (turn f1))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(at f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S0-1_2
		:precondition
			(and
				(not (turn f1))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S1)
			)
	)
	(:action buchi_move_from_BA-S0-2_3
		:precondition
			(and
				(not (turn f1))
				(not (check))
				(not (move))
				(currentBAstate BA-S0)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S0))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S1-0_4
		:precondition
			(and
				(not (turn f1))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(called) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-0_5
		:precondition
			(and
				(not (turn f1))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(at f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S1-1_6
		:precondition
			(and
				(not (turn f1))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(req f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
	(:action buchi_move_from_BA-S1-2_7
		:precondition
			(and
				(not (turn f1))
				(not (check))
				(not (move))
				(currentBAstate BA-S1)
				(not (at f1)) 
				(not (called)) 
				(not (req f1)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S1))
				(currentBAstate BA-S2)
			)
	)
	(:action buchi_move_from_BA-S2-0_8
		:precondition
			(and
				(not (turn f1))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(called) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S2-0_9
		:precondition
			(and
				(not (turn f1))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(at f1) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
				(not (currentBAstate BA-S2))
				(currentBAstate BA-S0)
				(oneof (and) (ok))
			)
	)
	(:action buchi_move_from_BA-S2-2_10
		:precondition
			(and
				(not (turn f1))
				(not (check))
				(not (move))
				(currentBAstate BA-S2)
				(not (at f1)) 
				(not (called)) 
			)
		:effect
			(and
				(turn f1)
				(not (called)) ;; reset
			)
	)
)

