(define (problem waldo)

  (:domain waldo)
  (:init 
    (in r1)
    (q_3)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)