(define (problem 'lift-2-lift-seq-2_dp1')

  (:domain lift-seq-2-lift-2_dp1)
  (:objects f1 - floor f2 - floor)
  (:init 
    (at f1)
    (move)
    (q_13)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)