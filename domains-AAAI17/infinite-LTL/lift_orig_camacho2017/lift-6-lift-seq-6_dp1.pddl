(define (problem 'lift-6-lift-seq-6_dp1')

  (:domain lift-seq-6-lift-6_dp1)
  (:objects f1 - floor f2 - floor f3 - floor f4 - floor f5 - floor f6 - floor)
  (:init 
    (at f1)
    (move)
    (q_29)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)