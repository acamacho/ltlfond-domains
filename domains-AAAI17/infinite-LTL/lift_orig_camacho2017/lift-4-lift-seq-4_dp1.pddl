(define (problem 'lift-4-lift-seq-4_dp1')

  (:domain lift-seq-4-lift-4_dp1)
  (:objects f1 - floor f2 - floor f3 - floor f4 - floor)
  (:init 
    (at f1)
    (move)
    (q_21)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)