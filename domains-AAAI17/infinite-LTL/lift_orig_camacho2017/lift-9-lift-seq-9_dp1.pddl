(define (problem 'lift-9-lift-seq-9_dp1')

  (:domain lift-seq-9-lift-9_dp1)
  (:objects f1 - floor f2 - floor f3 - floor f4 - floor f5 - floor f6 - floor f7 - floor f8 - floor f9 - floor)
  (:init 
    (at f1)
    (move)
    (q_41)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)