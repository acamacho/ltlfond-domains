(define (problem 'lift-10-lift-seq-10_dp1')

  (:domain lift-seq-10-lift-10_dp1)
  (:objects f1 - floor f10 - floor f2 - floor f3 - floor f4 - floor f5 - floor f6 - floor f7 - floor f8 - floor f9 - floor)
  (:init 
    (at f1)
    (move)
    (q_45)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)