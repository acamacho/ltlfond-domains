(define (problem 'lift-7-lift-seq-7_dp1')

  (:domain lift-seq-7-lift-7_dp1)
  (:objects f1 - floor f2 - floor f3 - floor f4 - floor f5 - floor f6 - floor f7 - floor)
  (:init 
    (at f1)
    (move)
    (q_33)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)