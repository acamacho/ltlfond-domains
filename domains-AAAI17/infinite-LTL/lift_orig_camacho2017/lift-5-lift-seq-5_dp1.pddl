(define (problem 'lift-5-lift-seq-5_dp1')

  (:domain lift-seq-5-lift-5_dp1)
  (:objects f1 - floor f2 - floor f3 - floor f4 - floor f5 - floor)
  (:init 
    (at f1)
    (move)
    (q_25)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)