(define (problem 'lift-8-lift-seq-8_dp1')

  (:domain lift-seq-8-lift-8_dp1)
  (:objects f1 - floor f2 - floor f3 - floor f4 - floor f5 - floor f6 - floor f7 - floor f8 - floor)
  (:init 
    (at f1)
    (move)
    (q_37)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)