(define (domain lift-seq-10-lift-10_dp1)
  (:types
    floor - NO_TYPE
  )

  (:predicates
    (at ?x0 - floor)
    (req ?x0 - floor)
    (turn ?x0 - floor)
    (check)
    (called)
    (move)
    (served)
    (f_copy)
    (f_sync)
    (f_world)
    (f_ok)
    (f_goal)
    (q_1)
    (q_1s)
    (q_2)
    (q_2s)
    (q_3)
    (q_3s)
    (q_4)
    (q_4s)
    (q_5)
    (q_5s)
    (q_6)
    (q_6s)
    (q_7)
    (q_7s)
    (q_8)
    (q_8s)
    (q_9)
    (q_9s)
    (q_10)
    (q_10s)
    (q_11)
    (q_11s)
    (q_12)
    (q_12s)
    (q_13)
    (q_13s)
    (q_14)
    (q_14s)
    (q_15)
    (q_15s)
    (q_16)
    (q_16s)
    (q_17)
    (q_17s)
    (q_18)
    (q_18s)
    (q_19)
    (q_19s)
    (q_20)
    (q_20s)
    (q_21)
    (q_21s)
    (q_22)
    (q_22s)
    (q_23)
    (q_23s)
    (q_24)
    (q_24s)
    (q_25)
    (q_25s)
    (q_26)
    (q_26s)
    (q_27)
    (q_27s)
    (q_28)
    (q_28s)
    (q_29)
    (q_29s)
    (q_30)
    (q_30s)
    (q_31)
    (q_31s)
    (q_32)
    (q_32s)
    (q_33)
    (q_33s)
    (q_34)
    (q_34s)
    (q_35)
    (q_35s)
    (q_36)
    (q_36s)
    (q_37)
    (q_37s)
    (q_38)
    (q_38s)
    (q_39)
    (q_39s)
    (q_40)
    (q_40s)
    (q_41)
    (q_41s)
    (q_42)
    (q_42s)
    (q_43)
    (q_43s)
    (q_44)
    (q_44s)
    (q_45)
    (q_45s)
    (q_46)
    (q_46s)
    (q_47)
    (q_47s)
    (q_48)
    (q_48s)
    (q_49)
    (q_49s)
    (q_50)
    (q_50s)
    (q_51)
    (q_51s)
    (q_52)
    (q_52s)
    (q_53)
    (q_53s)
    (q_54)
    (q_54s)
    (q_55)
    (q_55s)
    (q_56)
    (q_56s)
    (q_57)
    (q_57s)
    (q_58)
    (q_58s)
    (q_59)
    (q_59s)
    (q_60)
    (q_60s)
    (q_61)
    (q_61s)
    (q_62)
    (q_62s)
    (q_63)
    (q_63s)
    (q_64)
    (q_64s)
    (q_1token)
    (q_2token)
    (q_3token)
    (q_4token)
    (q_5token)
    (q_6token)
    (q_7token)
    (q_8token)
    (q_9token)
    (q_10token)
    (q_11token)
    (q_12token)
    (q_13token)
    (q_14token)
    (q_15token)
    (q_16token)
    (q_17token)
    (q_18token)
    (q_19token)
    (q_20token)
    (q_21token)
    (q_22token)
    (q_23token)
    (q_24token)
    (q_25token)
    (q_26token)
    (q_27token)
    (q_28token)
    (q_29token)
    (q_30token)
    (q_31token)
    (q_32token)
    (q_33token)
    (q_34token)
    (q_35token)
    (q_36token)
    (q_37token)
    (q_38token)
    (q_39token)
    (q_40token)
    (q_41token)
    (q_42token)
    (q_43token)
    (q_44token)
    (q_45token)
    (q_46token)
    (q_47token)
    (q_48token)
    (q_49token)
    (q_50token)
    (q_51token)
    (q_52token)
    (q_53token)
    (q_54token)
    (q_55token)
    (q_56token)
    (q_57token)
    (q_58token)
    (q_59token)
    (q_60token)
    (q_61token)
    (q_62token)
    (q_63token)
    (q_64token)
  )
  (:action push_f1
    :parameters ()
    :precondition 
      (and
        (turn f1)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f2)
        (oneof
          (empty)
          (req f1))
        (f_copy)
        (not 
          (turn f1))
        (not 
          (f_world))
      )
    )
  (:action push_f2
    :parameters ()
    :precondition 
      (and
        (turn f2)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f3)
        (oneof
          (empty)
          (req f2))
        (f_copy)
        (not 
          (turn f2))
        (not 
          (f_world))
      )
    )
  (:action push_f3
    :parameters ()
    :precondition 
      (and
        (turn f3)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f4)
        (oneof
          (empty)
          (req f3))
        (f_copy)
        (not 
          (turn f3))
        (not 
          (f_world))
      )
    )
  (:action push_f4
    :parameters ()
    :precondition 
      (and
        (turn f4)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f5)
        (oneof
          (empty)
          (req f4))
        (f_copy)
        (not 
          (turn f4))
        (not 
          (f_world))
      )
    )
  (:action push_f5
    :parameters ()
    :precondition 
      (and
        (turn f5)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f6)
        (oneof
          (empty)
          (req f5))
        (f_copy)
        (not 
          (turn f5))
        (not 
          (f_world))
      )
    )
  (:action push_f6
    :parameters ()
    :precondition 
      (and
        (turn f6)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f7)
        (oneof
          (empty)
          (req f6))
        (f_copy)
        (not 
          (turn f6))
        (not 
          (f_world))
      )
    )
  (:action push_f7
    :parameters ()
    :precondition 
      (and
        (turn f7)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f8)
        (oneof
          (empty)
          (req f7))
        (f_copy)
        (not 
          (turn f7))
        (not 
          (f_world))
      )
    )
  (:action push_f8
    :parameters ()
    :precondition 
      (and
        (turn f8)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f9)
        (oneof
          (empty)
          (req f8))
        (f_copy)
        (not 
          (turn f8))
        (not 
          (f_world))
      )
    )
  (:action push_f9
    :parameters ()
    :precondition 
      (and
        (turn f9)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f10)
        (oneof
          (empty)
          (req f9))
        (f_copy)
        (not 
          (turn f9))
        (not 
          (f_world))
      )
    )
  (:action push_f10
    :parameters ()
    :precondition 
      (and
        (turn f10)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (check)
        (turn f1)
        (oneof
          (empty)
          (req f10))
        (f_copy)
        (not 
          (turn f10))
        (not 
          (push))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f1
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f1)
        (req f1)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f2)
        (f_copy)
        (not 
          (turn f1))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f1
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f1))
        (check)
        (turn f1)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f2)
        (f_copy)
        (not 
          (turn f1))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f2
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f2)
        (req f2)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f3)
        (f_copy)
        (not 
          (turn f2))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f2
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f2))
        (check)
        (turn f2)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f3)
        (f_copy)
        (not 
          (turn f2))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f3
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f3)
        (req f3)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f4)
        (f_copy)
        (not 
          (turn f3))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f3
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f3))
        (check)
        (turn f3)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f4)
        (f_copy)
        (not 
          (turn f3))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f4
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f4)
        (req f4)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f5)
        (f_copy)
        (not 
          (turn f4))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f4
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f4))
        (check)
        (turn f4)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f5)
        (f_copy)
        (not 
          (turn f4))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f5
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f5)
        (req f5)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f6)
        (f_copy)
        (not 
          (turn f5))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f5
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f5))
        (check)
        (turn f5)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f6)
        (f_copy)
        (not 
          (turn f5))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f6
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f6)
        (req f6)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f7)
        (f_copy)
        (not 
          (turn f6))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f6
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f6))
        (check)
        (turn f6)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f7)
        (f_copy)
        (not 
          (turn f6))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f7
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f7)
        (req f7)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f8)
        (f_copy)
        (not 
          (turn f7))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f7
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f7))
        (check)
        (turn f7)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f8)
        (f_copy)
        (not 
          (turn f7))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f8
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f8)
        (req f8)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f9)
        (f_copy)
        (not 
          (turn f8))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f8
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f8))
        (check)
        (turn f8)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f9)
        (f_copy)
        (not 
          (turn f8))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f9
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f9)
        (req f9)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f10)
        (f_copy)
        (not 
          (turn f9))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f9
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f9))
        (check)
        (turn f9)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f10)
        (f_copy)
        (not 
          (turn f9))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f10
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f10)
        (req f10)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (f_copy)
        (not 
          (turn f10))
        (not 
          (check))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f10
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f10))
        (check)
        (turn f10)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (turn f10))
        (not 
          (check))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f1
    :parameters ()
    :precondition 
      (and
        (at f1)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f2)
        (turn f1)
        (f_copy)
        (not 
          (at f1))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f1
    :parameters ()
    :precondition 
      (and
        (at f1)
        (req f2)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f2)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f1))
        (not 
          (req f2))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f2
    :parameters ()
    :precondition 
      (and
        (at f2)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f3)
        (turn f1)
        (f_copy)
        (not 
          (at f2))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f2
    :parameters ()
    :precondition 
      (and
        (at f2)
        (req f3)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f3)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f2))
        (not 
          (req f3))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f3
    :parameters ()
    :precondition 
      (and
        (at f3)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f4)
        (turn f1)
        (f_copy)
        (not 
          (at f3))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f3
    :parameters ()
    :precondition 
      (and
        (at f3)
        (req f4)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f4)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f3))
        (not 
          (req f4))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f4
    :parameters ()
    :precondition 
      (and
        (at f4)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f5)
        (turn f1)
        (f_copy)
        (not 
          (at f4))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f4
    :parameters ()
    :precondition 
      (and
        (at f4)
        (req f5)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f5)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f4))
        (not 
          (req f5))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f5
    :parameters ()
    :precondition 
      (and
        (at f5)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f6)
        (turn f1)
        (f_copy)
        (not 
          (at f5))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f5
    :parameters ()
    :precondition 
      (and
        (at f5)
        (req f6)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f6)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f5))
        (not 
          (req f6))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f6
    :parameters ()
    :precondition 
      (and
        (at f6)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f7)
        (turn f1)
        (f_copy)
        (not 
          (at f6))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f6
    :parameters ()
    :precondition 
      (and
        (at f6)
        (req f7)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f7)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f6))
        (not 
          (req f7))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f7
    :parameters ()
    :precondition 
      (and
        (at f7)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f8)
        (turn f1)
        (f_copy)
        (not 
          (at f7))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f7
    :parameters ()
    :precondition 
      (and
        (at f7)
        (req f8)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f8)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f7))
        (not 
          (req f8))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f8
    :parameters ()
    :precondition 
      (and
        (at f8)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f9)
        (turn f1)
        (f_copy)
        (not 
          (at f8))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f8
    :parameters ()
    :precondition 
      (and
        (at f8)
        (req f9)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f9)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f8))
        (not 
          (req f9))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f9
    :parameters ()
    :precondition 
      (and
        (at f9)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f10)
        (turn f1)
        (f_copy)
        (not 
          (at f9))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f9
    :parameters ()
    :precondition 
      (and
        (at f9)
        (req f10)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f10)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f9))
        (not 
          (req f10))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f10
    :parameters ()
    :precondition 
      (and
        (at f10)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f9)
        (turn f1)
        (f_copy)
        (not 
          (at f10))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f10
    :parameters ()
    :precondition 
      (and
        (at f10)
        (req f9)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f9)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f10))
        (not 
          (req f9))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f9
    :parameters ()
    :precondition 
      (and
        (at f9)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f8)
        (turn f1)
        (f_copy)
        (not 
          (at f9))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f9
    :parameters ()
    :precondition 
      (and
        (at f9)
        (req f8)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f8)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f9))
        (not 
          (req f8))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f8
    :parameters ()
    :precondition 
      (and
        (at f8)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f7)
        (turn f1)
        (f_copy)
        (not 
          (at f8))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f8
    :parameters ()
    :precondition 
      (and
        (at f8)
        (req f7)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f7)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f8))
        (not 
          (req f7))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f7
    :parameters ()
    :precondition 
      (and
        (at f7)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f6)
        (turn f1)
        (f_copy)
        (not 
          (at f7))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f7
    :parameters ()
    :precondition 
      (and
        (at f7)
        (req f6)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f6)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f7))
        (not 
          (req f6))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f6
    :parameters ()
    :precondition 
      (and
        (at f6)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f5)
        (turn f1)
        (f_copy)
        (not 
          (at f6))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f6
    :parameters ()
    :precondition 
      (and
        (at f6)
        (req f5)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f5)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f6))
        (not 
          (req f5))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f5
    :parameters ()
    :precondition 
      (and
        (at f5)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f4)
        (turn f1)
        (f_copy)
        (not 
          (at f5))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f5
    :parameters ()
    :precondition 
      (and
        (at f5)
        (req f4)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f4)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f5))
        (not 
          (req f4))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f4
    :parameters ()
    :precondition 
      (and
        (at f4)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f3)
        (turn f1)
        (f_copy)
        (not 
          (at f4))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f4
    :parameters ()
    :precondition 
      (and
        (at f4)
        (req f3)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f3)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f4))
        (not 
          (req f3))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f3
    :parameters ()
    :precondition 
      (and
        (at f3)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f2)
        (turn f1)
        (f_copy)
        (not 
          (at f3))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f3
    :parameters ()
    :precondition 
      (and
        (at f3)
        (req f2)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f2)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f3))
        (not 
          (req f2))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f2
    :parameters ()
    :precondition 
      (and
        (at f2)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f1)
        (turn f1)
        (f_copy)
        (not 
          (at f2))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f2
    :parameters ()
    :precondition 
      (and
        (at f2)
        (req f1)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f1)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f2))
        (not 
          (req f1))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f1
    :parameters ()
    :precondition 
      (and
        (at f1)
        (req f1)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f1))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f2
    :parameters ()
    :precondition 
      (and
        (at f2)
        (req f2)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f2))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f3
    :parameters ()
    :precondition 
      (and
        (at f3)
        (req f3)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f3))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f4
    :parameters ()
    :precondition 
      (and
        (at f4)
        (req f4)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f4))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f5
    :parameters ()
    :precondition 
      (and
        (at f5)
        (req f5)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f5))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f6
    :parameters ()
    :precondition 
      (and
        (at f6)
        (req f6)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f6))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f7
    :parameters ()
    :precondition 
      (and
        (at f7)
        (req f7)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f7))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f8
    :parameters ()
    :precondition 
      (and
        (at f8)
        (req f8)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f8))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f9
    :parameters ()
    :precondition 
      (and
        (at f9)
        (req f9)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f9))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f10
    :parameters ()
    :precondition 
      (and
        (at f10)
        (req f10)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f10))
        (not 
          (f_world))
      )
    )
  (:action no_op
    :parameters ()
    :precondition 
      (and
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action mark_served
    :parameters ()
    :precondition 
      (and
        (not 
          (move))
        (f_ok)
        (f_world))
    :effect
      (and
        (move)
        (f_copy)
        (not 
          (served))
        (not 
          (f_world))
      )
    )
  (:action o_copy
    :parameters ()
    :precondition 
      (and
        (f_ok)
        (f_copy))
    :effect
      (and
        (f_sync)
        (when
          (q_1)
          (q_1s))
        (when
          (q_2)
          (q_2s))
        (when
          (q_3)
          (q_3s))
        (when
          (q_4)
          (q_4s))
        (when
          (q_5)
          (q_5s))
        (when
          (q_6)
          (q_6s))
        (when
          (q_7)
          (q_7s))
        (when
          (q_8)
          (q_8s))
        (when
          (q_9)
          (q_9s))
        (when
          (q_10)
          (q_10s))
        (when
          (q_11)
          (q_11s))
        (when
          (q_12)
          (q_12s))
        (when
          (q_13)
          (q_13s))
        (when
          (q_14)
          (q_14s))
        (when
          (q_15)
          (q_15s))
        (when
          (q_16)
          (q_16s))
        (when
          (q_17)
          (q_17s))
        (when
          (q_18)
          (q_18s))
        (when
          (q_19)
          (q_19s))
        (when
          (q_20)
          (q_20s))
        (when
          (q_21)
          (q_21s))
        (when
          (q_22)
          (q_22s))
        (when
          (q_23)
          (q_23s))
        (when
          (q_24)
          (q_24s))
        (when
          (q_25)
          (q_25s))
        (when
          (q_26)
          (q_26s))
        (when
          (q_27)
          (q_27s))
        (when
          (q_28)
          (q_28s))
        (when
          (q_29)
          (q_29s))
        (when
          (q_30)
          (q_30s))
        (when
          (q_31)
          (q_31s))
        (when
          (q_32)
          (q_32s))
        (when
          (q_33)
          (q_33s))
        (when
          (q_34)
          (q_34s))
        (when
          (q_35)
          (q_35s))
        (when
          (q_36)
          (q_36s))
        (when
          (q_37)
          (q_37s))
        (when
          (q_38)
          (q_38s))
        (when
          (q_39)
          (q_39s))
        (when
          (q_40)
          (q_40s))
        (when
          (q_41)
          (q_41s))
        (when
          (q_42)
          (q_42s))
        (when
          (q_43)
          (q_43s))
        (when
          (q_44)
          (q_44s))
        (when
          (q_45)
          (q_45s))
        (when
          (q_46)
          (q_46s))
        (when
          (q_47)
          (q_47s))
        (when
          (q_48)
          (q_48s))
        (when
          (q_49)
          (q_49s))
        (when
          (q_50)
          (q_50s))
        (when
          (q_51)
          (q_51s))
        (when
          (q_52)
          (q_52s))
        (when
          (q_53)
          (q_53s))
        (when
          (q_54)
          (q_54s))
        (when
          (q_55)
          (q_55s))
        (when
          (q_56)
          (q_56s))
        (when
          (q_57)
          (q_57s))
        (when
          (q_58)
          (q_58s))
        (when
          (q_59)
          (q_59s))
        (when
          (q_60)
          (q_60s))
        (when
          (q_61)
          (q_61s))
        (when
          (q_62)
          (q_62s))
        (when
          (q_63)
          (q_63s))
        (when
          (q_64)
          (q_64s))
        (not 
          (f_copy))
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3))
        (not 
          (q_4))
        (not 
          (q_5))
        (not 
          (q_6))
        (not 
          (q_7))
        (not 
          (q_8))
        (not 
          (q_9))
        (not 
          (q_10))
        (not 
          (q_11))
        (not 
          (q_12))
        (not 
          (q_13))
        (not 
          (q_14))
        (not 
          (q_15))
        (not 
          (q_16))
        (not 
          (q_17))
        (not 
          (q_18))
        (not 
          (q_19))
        (not 
          (q_20))
        (not 
          (q_21))
        (not 
          (q_22))
        (not 
          (q_23))
        (not 
          (q_24))
        (not 
          (q_25))
        (not 
          (q_26))
        (not 
          (q_27))
        (not 
          (q_28))
        (not 
          (q_29))
        (not 
          (q_30))
        (not 
          (q_31))
        (not 
          (q_32))
        (not 
          (q_33))
        (not 
          (q_34))
        (not 
          (q_35))
        (not 
          (q_36))
        (not 
          (q_37))
        (not 
          (q_38))
        (not 
          (q_39))
        (not 
          (q_40))
        (not 
          (q_41))
        (not 
          (q_42))
        (not 
          (q_43))
        (not 
          (q_44))
        (not 
          (q_45))
        (not 
          (q_46))
        (not 
          (q_47))
        (not 
          (q_48))
        (not 
          (q_49))
        (not 
          (q_50))
        (not 
          (q_51))
        (not 
          (q_52))
        (not 
          (q_53))
        (not 
          (q_54))
        (not 
          (q_55))
        (not 
          (q_56))
        (not 
          (q_57))
        (not 
          (q_58))
        (not 
          (q_59))
        (not 
          (q_60))
        (not 
          (q_61))
        (not 
          (q_62))
        (not 
          (q_63))
        (not 
          (q_64))
      )
    )
  (:action o_world
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
        (not 
          (q_5s))
        (not 
          (q_6s))
        (not 
          (q_7s))
        (not 
          (q_8s))
        (not 
          (q_9s))
        (not 
          (q_10s))
        (not 
          (q_11s))
        (not 
          (q_12s))
        (not 
          (q_13s))
        (not 
          (q_14s))
        (not 
          (q_15s))
        (not 
          (q_16s))
        (not 
          (q_17s))
        (not 
          (q_18s))
        (not 
          (q_19s))
        (not 
          (q_20s))
        (not 
          (q_21s))
        (not 
          (q_22s))
        (not 
          (q_23s))
        (not 
          (q_24s))
        (not 
          (q_25s))
        (not 
          (q_26s))
        (not 
          (q_27s))
        (not 
          (q_28s))
        (not 
          (q_29s))
        (not 
          (q_30s))
        (not 
          (q_31s))
        (not 
          (q_32s))
        (not 
          (q_33s))
        (not 
          (q_34s))
        (not 
          (q_35s))
        (not 
          (q_36s))
        (not 
          (q_37s))
        (not 
          (q_38s))
        (not 
          (q_39s))
        (not 
          (q_40s))
        (not 
          (q_41s))
        (not 
          (q_42s))
        (not 
          (q_43s))
        (not 
          (q_44s))
        (not 
          (q_45s))
        (not 
          (q_46s))
        (not 
          (q_47s))
        (not 
          (q_48s))
        (not 
          (q_49s))
        (not 
          (q_50s))
        (not 
          (q_51s))
        (not 
          (q_52s))
        (not 
          (q_53s))
        (not 
          (q_54s))
        (not 
          (q_55s))
        (not 
          (q_56s))
        (not 
          (q_57s))
        (not 
          (q_58s))
        (not 
          (q_59s))
        (not 
          (q_60s))
        (not 
          (q_61s))
        (not 
          (q_62s))
        (not 
          (q_63s))
        (not 
          (q_64s)))
    :effect
      (and
        (f_world)
        (not 
          (f_sync))
      )
    )
  (:action o_sync_q_1s
    :parameters ()
    :precondition 
      (and
        (called)
        (f_sync)
        (f_ok)
        (q_1s))
    :effect
      (and
        (not 
          (q_1s))
        (when
          (q_1token)
          (not 
            (q_1token)))
      )
    )
  (:action o_sync_q_2s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_2s))
    :effect
      (and
        (q_23s)
        (q_2)
        (not 
          (q_2s))
        (not 
          (q_2token))
      )
    )
  (:action o_sync_q_3s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_3s))
    :effect
      (and
        (q_24s)
        (q_3)
        (not 
          (q_3s))
        (not 
          (q_3token))
      )
    )
  (:action o_sync_q_4s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_4s))
    :effect
      (and
        (q_25s)
        (q_4)
        (not 
          (q_4s))
        (not 
          (q_4token))
      )
    )
  (:action o_sync_q_5s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_5s))
    :effect
      (and
        (q_26s)
        (q_5)
        (not 
          (q_5s))
        (not 
          (q_5token))
      )
    )
  (:action o_sync_q_6s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_6s))
    :effect
      (and
        (q_27s)
        (q_6)
        (not 
          (q_6s))
        (not 
          (q_6token))
      )
    )
  (:action o_sync_q_7s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_7s))
    :effect
      (and
        (q_28s)
        (q_7)
        (not 
          (q_7s))
        (not 
          (q_7token))
      )
    )
  (:action o_sync_q_8s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_8s))
    :effect
      (and
        (q_29s)
        (q_8)
        (not 
          (q_8s))
        (not 
          (q_8token))
      )
    )
  (:action o_sync_q_9s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_9s))
    :effect
      (and
        (q_30s)
        (q_9)
        (not 
          (q_9s))
        (not 
          (q_9token))
      )
    )
  (:action o_sync_q_10s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_10s))
    :effect
      (and
        (q_31s)
        (q_10)
        (not 
          (q_10s))
        (not 
          (q_10token))
      )
    )
  (:action o_sync_q_11s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_11s))
    :effect
      (and
        (q_32s)
        (q_11)
        (not 
          (q_11s))
        (not 
          (q_11token))
      )
    )
  (:action o_sync_q_12s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_12s))
    :effect
      (and
        (q_33s)
        (q_12)
        (not 
          (q_12s))
        (not 
          (q_12token))
      )
    )
  (:action o_sync_q_13s
    :parameters ()
    :precondition 
      (and
        (at f1)
        (f_sync)
        (f_ok)
        (q_13s))
    :effect
      (and
        (not 
          (q_13s))
        (when
          (q_13token)
          (not 
            (q_13token)))
      )
    )
  (:action o_sync_q_14s
    :parameters ()
    :precondition 
      (and
        (at f10)
        (f_sync)
        (f_ok)
        (q_14s))
    :effect
      (and
        (not 
          (q_14s))
        (when
          (q_14token)
          (not 
            (q_14token)))
      )
    )
  (:action o_sync_q_15s
    :parameters ()
    :precondition 
      (and
        (at f2)
        (f_sync)
        (f_ok)
        (q_15s))
    :effect
      (and
        (not 
          (q_15s))
        (when
          (q_15token)
          (not 
            (q_15token)))
      )
    )
  (:action o_sync_q_16s
    :parameters ()
    :precondition 
      (and
        (at f3)
        (f_sync)
        (f_ok)
        (q_16s))
    :effect
      (and
        (not 
          (q_16s))
        (when
          (q_16token)
          (not 
            (q_16token)))
      )
    )
  (:action o_sync_q_17s
    :parameters ()
    :precondition 
      (and
        (at f4)
        (f_sync)
        (f_ok)
        (q_17s))
    :effect
      (and
        (not 
          (q_17s))
        (when
          (q_17token)
          (not 
            (q_17token)))
      )
    )
  (:action o_sync_q_18s
    :parameters ()
    :precondition 
      (and
        (at f5)
        (f_sync)
        (f_ok)
        (q_18s))
    :effect
      (and
        (not 
          (q_18s))
        (when
          (q_18token)
          (not 
            (q_18token)))
      )
    )
  (:action o_sync_q_19s
    :parameters ()
    :precondition 
      (and
        (at f6)
        (f_sync)
        (f_ok)
        (q_19s))
    :effect
      (and
        (not 
          (q_19s))
        (when
          (q_19token)
          (not 
            (q_19token)))
      )
    )
  (:action o_sync_q_20s
    :parameters ()
    :precondition 
      (and
        (at f7)
        (f_sync)
        (f_ok)
        (q_20s))
    :effect
      (and
        (not 
          (q_20s))
        (when
          (q_20token)
          (not 
            (q_20token)))
      )
    )
  (:action o_sync_q_21s
    :parameters ()
    :precondition 
      (and
        (at f8)
        (f_sync)
        (f_ok)
        (q_21s))
    :effect
      (and
        (not 
          (q_21s))
        (when
          (q_21token)
          (not 
            (q_21token)))
      )
    )
  (:action o_sync_q_22s
    :parameters ()
    :precondition 
      (and
        (at f9)
        (f_sync)
        (f_ok)
        (q_22s))
    :effect
      (and
        (not 
          (q_22s))
        (when
          (q_22token)
          (not 
            (q_22token)))
      )
    )
  (:action o_sync_q_23s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_23s))
    :effect
      (and
        (q_54s)
        (when
          (q_23token)
          (q_54token))
        (not 
          (q_23s))
        (when
          (q_23token)
          (not 
            (q_23token)))
      )
    )
  (:action o_sync_q_23s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_23s))
    :effect
      (and
        (q_23)
        (not 
          (q_23s))
      )
    )
  (:action o_sync_q_24s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_24s))
    :effect
      (and
        (q_55s)
        (when
          (q_24token)
          (q_55token))
        (not 
          (q_24s))
        (when
          (q_24token)
          (not 
            (q_24token)))
      )
    )
  (:action o_sync_q_24s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_24s))
    :effect
      (and
        (q_24)
        (not 
          (q_24s))
      )
    )
  (:action o_sync_q_25s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_25s))
    :effect
      (and
        (q_56s)
        (when
          (q_25token)
          (q_56token))
        (not 
          (q_25s))
        (when
          (q_25token)
          (not 
            (q_25token)))
      )
    )
  (:action o_sync_q_25s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_25s))
    :effect
      (and
        (q_25)
        (not 
          (q_25s))
      )
    )
  (:action o_sync_q_26s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_26s))
    :effect
      (and
        (q_57s)
        (when
          (q_26token)
          (q_57token))
        (not 
          (q_26s))
        (when
          (q_26token)
          (not 
            (q_26token)))
      )
    )
  (:action o_sync_q_26s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_26s))
    :effect
      (and
        (q_26)
        (not 
          (q_26s))
      )
    )
  (:action o_sync_q_27s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_27s))
    :effect
      (and
        (q_58s)
        (when
          (q_27token)
          (q_58token))
        (not 
          (q_27s))
        (when
          (q_27token)
          (not 
            (q_27token)))
      )
    )
  (:action o_sync_q_27s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_27s))
    :effect
      (and
        (q_27)
        (not 
          (q_27s))
      )
    )
  (:action o_sync_q_28s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_28s))
    :effect
      (and
        (q_59s)
        (when
          (q_28token)
          (q_59token))
        (not 
          (q_28s))
        (when
          (q_28token)
          (not 
            (q_28token)))
      )
    )
  (:action o_sync_q_28s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_28s))
    :effect
      (and
        (q_28)
        (not 
          (q_28s))
      )
    )
  (:action o_sync_q_29s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_29s))
    :effect
      (and
        (q_60s)
        (when
          (q_29token)
          (q_60token))
        (not 
          (q_29s))
        (when
          (q_29token)
          (not 
            (q_29token)))
      )
    )
  (:action o_sync_q_29s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_29s))
    :effect
      (and
        (q_29)
        (not 
          (q_29s))
      )
    )
  (:action o_sync_q_30s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_30s))
    :effect
      (and
        (q_61s)
        (when
          (q_30token)
          (q_61token))
        (not 
          (q_30s))
        (when
          (q_30token)
          (not 
            (q_30token)))
      )
    )
  (:action o_sync_q_30s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_30s))
    :effect
      (and
        (q_30)
        (not 
          (q_30s))
      )
    )
  (:action o_sync_q_31s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_31s))
    :effect
      (and
        (q_62s)
        (when
          (q_31token)
          (q_62token))
        (not 
          (q_31s))
        (when
          (q_31token)
          (not 
            (q_31token)))
      )
    )
  (:action o_sync_q_31s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_31s))
    :effect
      (and
        (q_31)
        (not 
          (q_31s))
      )
    )
  (:action o_sync_q_32s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_32s))
    :effect
      (and
        (q_63s)
        (when
          (q_32token)
          (q_63token))
        (not 
          (q_32s))
        (when
          (q_32token)
          (not 
            (q_32token)))
      )
    )
  (:action o_sync_q_32s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_32s))
    :effect
      (and
        (q_32)
        (not 
          (q_32s))
      )
    )
  (:action o_sync_q_33s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_33s))
    :effect
      (and
        (q_64s)
        (when
          (q_33token)
          (q_64token))
        (not 
          (q_33s))
        (when
          (q_33token)
          (not 
            (q_33token)))
      )
    )
  (:action o_sync_q_33s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_33s))
    :effect
      (and
        (q_33)
        (not 
          (q_33s))
      )
    )
  (:action o_sync_q_34s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f1))
        (f_sync)
        (f_ok)
        (q_34s))
    :effect
      (and
        (not 
          (q_34s))
        (when
          (q_34token)
          (not 
            (q_34token)))
      )
    )
  (:action o_sync_q_35s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f10))
        (f_sync)
        (f_ok)
        (q_35s))
    :effect
      (and
        (not 
          (q_35s))
        (when
          (q_35token)
          (not 
            (q_35token)))
      )
    )
  (:action o_sync_q_36s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f2))
        (f_sync)
        (f_ok)
        (q_36s))
    :effect
      (and
        (not 
          (q_36s))
        (when
          (q_36token)
          (not 
            (q_36token)))
      )
    )
  (:action o_sync_q_37s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f3))
        (f_sync)
        (f_ok)
        (q_37s))
    :effect
      (and
        (not 
          (q_37s))
        (when
          (q_37token)
          (not 
            (q_37token)))
      )
    )
  (:action o_sync_q_38s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f4))
        (f_sync)
        (f_ok)
        (q_38s))
    :effect
      (and
        (not 
          (q_38s))
        (when
          (q_38token)
          (not 
            (q_38token)))
      )
    )
  (:action o_sync_q_39s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f5))
        (f_sync)
        (f_ok)
        (q_39s))
    :effect
      (and
        (not 
          (q_39s))
        (when
          (q_39token)
          (not 
            (q_39token)))
      )
    )
  (:action o_sync_q_40s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f6))
        (f_sync)
        (f_ok)
        (q_40s))
    :effect
      (and
        (not 
          (q_40s))
        (when
          (q_40token)
          (not 
            (q_40token)))
      )
    )
  (:action o_sync_q_41s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f7))
        (f_sync)
        (f_ok)
        (q_41s))
    :effect
      (and
        (not 
          (q_41s))
        (when
          (q_41token)
          (not 
            (q_41token)))
      )
    )
  (:action o_sync_q_42s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f8))
        (f_sync)
        (f_ok)
        (q_42s))
    :effect
      (and
        (not 
          (q_42s))
        (when
          (q_42token)
          (not 
            (q_42token)))
      )
    )
  (:action o_sync_q_43s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f9))
        (f_sync)
        (f_ok)
        (q_43s))
    :effect
      (and
        (not 
          (q_43s))
        (when
          (q_43token)
          (not 
            (q_43token)))
      )
    )
  (:action o_sync_q_44s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_44s))
    :effect
      (and
        (q_2s)
        (q_4s)
        (when
          (q_44token)
          (q_2token))
        (when
          (q_44token)
          (q_4token))
        (not 
          (q_44s))
        (when
          (q_44token)
          (not 
            (q_44token)))
      )
    )
  (:action o_sync_q_45s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_45s))
    :effect
      (and
        (q_3s)
        (q_46s)
        (when
          (q_45token)
          (q_3token))
        (when
          (q_45token)
          (q_46token))
        (not 
          (q_45s))
        (when
          (q_45token)
          (not 
            (q_45token)))
      )
    )
  (:action o_sync_q_46s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_46s))
    :effect
      (and
        (q_5s)
        (q_47s)
        (when
          (q_46token)
          (q_5token))
        (when
          (q_46token)
          (q_47token))
        (not 
          (q_46s))
        (when
          (q_46token)
          (not 
            (q_46token)))
      )
    )
  (:action o_sync_q_47s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_47s))
    :effect
      (and
        (q_6s)
        (q_48s)
        (when
          (q_47token)
          (q_6token))
        (when
          (q_47token)
          (q_48token))
        (not 
          (q_47s))
        (when
          (q_47token)
          (not 
            (q_47token)))
      )
    )
  (:action o_sync_q_48s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_48s))
    :effect
      (and
        (q_7s)
        (q_49s)
        (when
          (q_48token)
          (q_7token))
        (when
          (q_48token)
          (q_49token))
        (not 
          (q_48s))
        (when
          (q_48token)
          (not 
            (q_48token)))
      )
    )
  (:action o_sync_q_49s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_49s))
    :effect
      (and
        (q_8s)
        (q_50s)
        (when
          (q_49token)
          (q_8token))
        (when
          (q_49token)
          (q_50token))
        (not 
          (q_49s))
        (when
          (q_49token)
          (not 
            (q_49token)))
      )
    )
  (:action o_sync_q_50s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_50s))
    :effect
      (and
        (q_9s)
        (q_51s)
        (when
          (q_50token)
          (q_9token))
        (when
          (q_50token)
          (q_51token))
        (not 
          (q_50s))
        (when
          (q_50token)
          (not 
            (q_50token)))
      )
    )
  (:action o_sync_q_51s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_51s))
    :effect
      (and
        (q_10s)
        (q_52s)
        (when
          (q_51token)
          (q_10token))
        (when
          (q_51token)
          (q_52token))
        (not 
          (q_51s))
        (when
          (q_51token)
          (not 
            (q_51token)))
      )
    )
  (:action o_sync_q_52s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_52s))
    :effect
      (and
        (q_11s)
        (q_53s)
        (when
          (q_52token)
          (q_11token))
        (when
          (q_52token)
          (q_53token))
        (not 
          (q_52s))
        (when
          (q_52token)
          (not 
            (q_52token)))
      )
    )
  (:action o_sync_q_53s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_53s))
    :effect
      (and
        (q_12s)
        (q_44s)
        (when
          (q_53token)
          (q_12token))
        (when
          (q_53token)
          (q_44token))
        (not 
          (q_53s))
        (when
          (q_53token)
          (not 
            (q_53token)))
      )
    )
  (:action o_sync_q_54s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_54s))
    :effect
      (and
        (q_1s)
        (when
          (q_54token)
          (q_1token))
        (not 
          (q_54s))
        (when
          (q_54token)
          (not 
            (q_54token)))
      )
    )
  (:action o_sync_q_54s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_54s))
    :effect
      (and
        (q_13s)
        (when
          (q_54token)
          (q_13token))
        (not 
          (q_54s))
        (when
          (q_54token)
          (not 
            (q_54token)))
      )
    )
  (:action o_sync_q_55s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_55s))
    :effect
      (and
        (q_13s)
        (when
          (q_55token)
          (q_13token))
        (not 
          (q_55s))
        (when
          (q_55token)
          (not 
            (q_55token)))
      )
    )
  (:action o_sync_q_55s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_55s))
    :effect
      (and
        (q_34s)
        (when
          (q_55token)
          (q_34token))
        (not 
          (q_55s))
        (when
          (q_55token)
          (not 
            (q_55token)))
      )
    )
  (:action o_sync_q_56s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_56s))
    :effect
      (and
        (q_14s)
        (when
          (q_56token)
          (q_14token))
        (not 
          (q_56s))
        (when
          (q_56token)
          (not 
            (q_56token)))
      )
    )
  (:action o_sync_q_56s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_56s))
    :effect
      (and
        (q_35s)
        (when
          (q_56token)
          (q_35token))
        (not 
          (q_56s))
        (when
          (q_56token)
          (not 
            (q_56token)))
      )
    )
  (:action o_sync_q_57s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_57s))
    :effect
      (and
        (q_15s)
        (when
          (q_57token)
          (q_15token))
        (not 
          (q_57s))
        (when
          (q_57token)
          (not 
            (q_57token)))
      )
    )
  (:action o_sync_q_57s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_57s))
    :effect
      (and
        (q_36s)
        (when
          (q_57token)
          (q_36token))
        (not 
          (q_57s))
        (when
          (q_57token)
          (not 
            (q_57token)))
      )
    )
  (:action o_sync_q_58s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_58s))
    :effect
      (and
        (q_16s)
        (when
          (q_58token)
          (q_16token))
        (not 
          (q_58s))
        (when
          (q_58token)
          (not 
            (q_58token)))
      )
    )
  (:action o_sync_q_58s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_58s))
    :effect
      (and
        (q_37s)
        (when
          (q_58token)
          (q_37token))
        (not 
          (q_58s))
        (when
          (q_58token)
          (not 
            (q_58token)))
      )
    )
  (:action o_sync_q_59s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_59s))
    :effect
      (and
        (q_17s)
        (when
          (q_59token)
          (q_17token))
        (not 
          (q_59s))
        (when
          (q_59token)
          (not 
            (q_59token)))
      )
    )
  (:action o_sync_q_59s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_59s))
    :effect
      (and
        (q_38s)
        (when
          (q_59token)
          (q_38token))
        (not 
          (q_59s))
        (when
          (q_59token)
          (not 
            (q_59token)))
      )
    )
  (:action o_sync_q_60s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_60s))
    :effect
      (and
        (q_18s)
        (when
          (q_60token)
          (q_18token))
        (not 
          (q_60s))
        (when
          (q_60token)
          (not 
            (q_60token)))
      )
    )
  (:action o_sync_q_60s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_60s))
    :effect
      (and
        (q_39s)
        (when
          (q_60token)
          (q_39token))
        (not 
          (q_60s))
        (when
          (q_60token)
          (not 
            (q_60token)))
      )
    )
  (:action o_sync_q_61s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_61s))
    :effect
      (and
        (q_19s)
        (when
          (q_61token)
          (q_19token))
        (not 
          (q_61s))
        (when
          (q_61token)
          (not 
            (q_61token)))
      )
    )
  (:action o_sync_q_61s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_61s))
    :effect
      (and
        (q_40s)
        (when
          (q_61token)
          (q_40token))
        (not 
          (q_61s))
        (when
          (q_61token)
          (not 
            (q_61token)))
      )
    )
  (:action o_sync_q_62s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_62s))
    :effect
      (and
        (q_20s)
        (when
          (q_62token)
          (q_20token))
        (not 
          (q_62s))
        (when
          (q_62token)
          (not 
            (q_62token)))
      )
    )
  (:action o_sync_q_62s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_62s))
    :effect
      (and
        (q_41s)
        (when
          (q_62token)
          (q_41token))
        (not 
          (q_62s))
        (when
          (q_62token)
          (not 
            (q_62token)))
      )
    )
  (:action o_sync_q_63s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_63s))
    :effect
      (and
        (q_21s)
        (when
          (q_63token)
          (q_21token))
        (not 
          (q_63s))
        (when
          (q_63token)
          (not 
            (q_63token)))
      )
    )
  (:action o_sync_q_63s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_63s))
    :effect
      (and
        (q_42s)
        (when
          (q_63token)
          (q_42token))
        (not 
          (q_63s))
        (when
          (q_63token)
          (not 
            (q_63token)))
      )
    )
  (:action o_sync_q_64s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_64s))
    :effect
      (and
        (q_22s)
        (when
          (q_64token)
          (q_22token))
        (not 
          (q_64s))
        (when
          (q_64token)
          (not 
            (q_64token)))
      )
    )
  (:action o_sync_q_64s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_64s))
    :effect
      (and
        (q_43s)
        (when
          (q_64token)
          (q_43token))
        (not 
          (q_64s))
        (when
          (q_64token)
          (not 
            (q_64token)))
      )
    )
  (:action jaces_DETDUP_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (not 
          (q_64token))
        (not 
          (q_63token))
        (not 
          (q_62token))
        (not 
          (q_61token))
        (not 
          (q_60token))
        (not 
          (q_59token))
        (not 
          (q_58token))
        (not 
          (q_57token))
        (not 
          (q_56token))
        (not 
          (q_55token))
        (not 
          (q_54token))
        (not 
          (q_53token))
        (not 
          (q_52token))
        (not 
          (q_51token))
        (not 
          (q_50token))
        (not 
          (q_49token))
        (not 
          (q_48token))
        (not 
          (q_47token))
        (not 
          (q_46token))
        (not 
          (q_45token))
        (not 
          (q_44token))
        (not 
          (q_43token))
        (not 
          (q_42token))
        (not 
          (q_41token))
        (not 
          (q_40token))
        (not 
          (q_39token))
        (not 
          (q_38token))
        (not 
          (q_37token))
        (not 
          (q_36token))
        (not 
          (q_35token))
        (not 
          (q_34token))
        (not 
          (q_33token))
        (not 
          (q_32token))
        (not 
          (q_31token))
        (not 
          (q_30token))
        (not 
          (q_29token))
        (not 
          (q_28token))
        (not 
          (q_27token))
        (not 
          (q_26token))
        (not 
          (q_25token))
        (not 
          (q_24token))
        (not 
          (q_23token))
        (not 
          (q_22token))
        (not 
          (q_21token))
        (not 
          (q_20token))
        (not 
          (q_19token))
        (not 
          (q_18token))
        (not 
          (q_17token))
        (not 
          (q_16token))
        (not 
          (q_15token))
        (not 
          (q_14token))
        (not 
          (q_13token))
        (not 
          (q_1token))
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
        (not 
          (q_5s))
        (not 
          (q_6s))
        (not 
          (q_7s))
        (not 
          (q_8s))
        (not 
          (q_9s))
        (not 
          (q_10s))
        (not 
          (q_11s))
        (not 
          (q_12s))
        (not 
          (q_13s))
        (not 
          (q_14s))
        (not 
          (q_15s))
        (not 
          (q_16s))
        (not 
          (q_17s))
        (not 
          (q_18s))
        (not 
          (q_19s))
        (not 
          (q_20s))
        (not 
          (q_21s))
        (not 
          (q_22s))
        (not 
          (q_23s))
        (not 
          (q_24s))
        (not 
          (q_25s))
        (not 
          (q_26s))
        (not 
          (q_27s))
        (not 
          (q_28s))
        (not 
          (q_29s))
        (not 
          (q_30s))
        (not 
          (q_31s))
        (not 
          (q_32s))
        (not 
          (q_33s))
        (not 
          (q_34s))
        (not 
          (q_35s))
        (not 
          (q_36s))
        (not 
          (q_37s))
        (not 
          (q_38s))
        (not 
          (q_39s))
        (not 
          (q_40s))
        (not 
          (q_41s))
        (not 
          (q_42s))
        (not 
          (q_43s))
        (not 
          (q_44s))
        (not 
          (q_45s))
        (not 
          (q_46s))
        (not 
          (q_47s))
        (not 
          (q_48s))
        (not 
          (q_49s))
        (not 
          (q_50s))
        (not 
          (q_51s))
        (not 
          (q_52s))
        (not 
          (q_53s))
        (not 
          (q_54s))
        (not 
          (q_55s))
        (not 
          (q_56s))
        (not 
          (q_57s))
        (not 
          (q_58s))
        (not 
          (q_59s))
        (not 
          (q_60s))
        (not 
          (q_61s))
        (not 
          (q_62s))
        (not 
          (q_63s))
        (not 
          (q_64s)))
    :effect
      (and
        (f_goal)
        (f_world)
        (not 
          (f_sync))
      )
    )
  (:action jaces_DETDUP_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (not 
          (q_64token))
        (not 
          (q_63token))
        (not 
          (q_62token))
        (not 
          (q_61token))
        (not 
          (q_60token))
        (not 
          (q_59token))
        (not 
          (q_58token))
        (not 
          (q_57token))
        (not 
          (q_56token))
        (not 
          (q_55token))
        (not 
          (q_54token))
        (not 
          (q_53token))
        (not 
          (q_52token))
        (not 
          (q_51token))
        (not 
          (q_50token))
        (not 
          (q_49token))
        (not 
          (q_48token))
        (not 
          (q_47token))
        (not 
          (q_46token))
        (not 
          (q_45token))
        (not 
          (q_44token))
        (not 
          (q_43token))
        (not 
          (q_42token))
        (not 
          (q_41token))
        (not 
          (q_40token))
        (not 
          (q_39token))
        (not 
          (q_38token))
        (not 
          (q_37token))
        (not 
          (q_36token))
        (not 
          (q_35token))
        (not 
          (q_34token))
        (not 
          (q_33token))
        (not 
          (q_32token))
        (not 
          (q_31token))
        (not 
          (q_30token))
        (not 
          (q_29token))
        (not 
          (q_28token))
        (not 
          (q_27token))
        (not 
          (q_26token))
        (not 
          (q_25token))
        (not 
          (q_24token))
        (not 
          (q_23token))
        (not 
          (q_22token))
        (not 
          (q_21token))
        (not 
          (q_20token))
        (not 
          (q_19token))
        (not 
          (q_18token))
        (not 
          (q_17token))
        (not 
          (q_16token))
        (not 
          (q_15token))
        (not 
          (q_14token))
        (not 
          (q_13token))
        (not 
          (q_1token))
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
        (not 
          (q_5s))
        (not 
          (q_6s))
        (not 
          (q_7s))
        (not 
          (q_8s))
        (not 
          (q_9s))
        (not 
          (q_10s))
        (not 
          (q_11s))
        (not 
          (q_12s))
        (not 
          (q_13s))
        (not 
          (q_14s))
        (not 
          (q_15s))
        (not 
          (q_16s))
        (not 
          (q_17s))
        (not 
          (q_18s))
        (not 
          (q_19s))
        (not 
          (q_20s))
        (not 
          (q_21s))
        (not 
          (q_22s))
        (not 
          (q_23s))
        (not 
          (q_24s))
        (not 
          (q_25s))
        (not 
          (q_26s))
        (not 
          (q_27s))
        (not 
          (q_28s))
        (not 
          (q_29s))
        (not 
          (q_30s))
        (not 
          (q_31s))
        (not 
          (q_32s))
        (not 
          (q_33s))
        (not 
          (q_34s))
        (not 
          (q_35s))
        (not 
          (q_36s))
        (not 
          (q_37s))
        (not 
          (q_38s))
        (not 
          (q_39s))
        (not 
          (q_40s))
        (not 
          (q_41s))
        (not 
          (q_42s))
        (not 
          (q_43s))
        (not 
          (q_44s))
        (not 
          (q_45s))
        (not 
          (q_46s))
        (not 
          (q_47s))
        (not 
          (q_48s))
        (not 
          (q_49s))
        (not 
          (q_50s))
        (not 
          (q_51s))
        (not 
          (q_52s))
        (not 
          (q_53s))
        (not 
          (q_54s))
        (not 
          (q_55s))
        (not 
          (q_56s))
        (not 
          (q_57s))
        (not 
          (q_58s))
        (not 
          (q_59s))
        (not 
          (q_60s))
        (not 
          (q_61s))
        (not 
          (q_62s))
        (not 
          (q_63s))
        (not 
          (q_64s)))
    :effect
      (and
        (when
          (q_64)
          (q_64token))
        (when
          (q_63)
          (q_63token))
        (when
          (q_62)
          (q_62token))
        (when
          (q_61)
          (q_61token))
        (when
          (q_60)
          (q_60token))
        (when
          (q_59)
          (q_59token))
        (when
          (q_58)
          (q_58token))
        (when
          (q_57)
          (q_57token))
        (when
          (q_56)
          (q_56token))
        (when
          (q_55)
          (q_55token))
        (when
          (q_54)
          (q_54token))
        (when
          (q_53)
          (q_53token))
        (when
          (q_52)
          (q_52token))
        (when
          (q_51)
          (q_51token))
        (when
          (q_50)
          (q_50token))
        (when
          (q_49)
          (q_49token))
        (when
          (q_48)
          (q_48token))
        (when
          (q_47)
          (q_47token))
        (when
          (q_46)
          (q_46token))
        (when
          (q_45)
          (q_45token))
        (when
          (q_44)
          (q_44token))
        (when
          (q_43)
          (q_43token))
        (when
          (q_42)
          (q_42token))
        (when
          (q_41)
          (q_41token))
        (when
          (q_40)
          (q_40token))
        (when
          (q_39)
          (q_39token))
        (when
          (q_38)
          (q_38token))
        (when
          (q_37)
          (q_37token))
        (when
          (q_36)
          (q_36token))
        (when
          (q_35)
          (q_35token))
        (when
          (q_34)
          (q_34token))
        (when
          (q_33)
          (q_33token))
        (when
          (q_32)
          (q_32token))
        (when
          (q_31)
          (q_31token))
        (when
          (q_30)
          (q_30token))
        (when
          (q_29)
          (q_29token))
        (when
          (q_28)
          (q_28token))
        (when
          (q_27)
          (q_27token))
        (when
          (q_26)
          (q_26token))
        (when
          (q_25)
          (q_25token))
        (when
          (q_24)
          (q_24token))
        (when
          (q_23)
          (q_23token))
        (when
          (q_22)
          (q_22token))
        (when
          (q_21)
          (q_21token))
        (when
          (q_20)
          (q_20token))
        (when
          (q_19)
          (q_19token))
        (when
          (q_18)
          (q_18token))
        (when
          (q_17)
          (q_17token))
        (when
          (q_16)
          (q_16token))
        (when
          (q_15)
          (q_15token))
        (when
          (q_14)
          (q_14token))
        (when
          (q_13)
          (q_13token))
        (when
          (q_1)
          (q_1token))
        (f_world)
        (not 
          (f_sync))
      )
    )
)