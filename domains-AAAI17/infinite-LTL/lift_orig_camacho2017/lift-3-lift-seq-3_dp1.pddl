(define (problem 'lift-3-lift-seq-3_dp1')

  (:domain lift-seq-3-lift-3_dp1)
  (:objects f1 - floor f2 - floor f3 - floor)
  (:init 
    (at f1)
    (move)
    (q_17)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)