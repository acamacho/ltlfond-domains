(define (domain lift-seq-1-lift-1_dp1)
  (:types
    floor - NO_TYPE
  )

  (:predicates
    (at ?x0 - floor)
    (req ?x0 - floor)
    (turn ?x0 - floor)
    (check)
    (called)
    (move)
    (served)
    (f_copy)
    (f_sync)
    (f_world)
    (f_ok)
    (f_goal)
    (q_1)
    (q_1s)
    (q_2)
    (q_2s)
    (q_3)
    (q_3s)
    (q_4)
    (q_4s)
    (q_5)
    (q_5s)
    (q_6)
    (q_6s)
    (q_7)
    (q_7s)
    (q_8)
    (q_8s)
    (q_9)
    (q_9s)
    (q_10)
    (q_10s)
    (q_1token)
    (q_2token)
    (q_3token)
    (q_4token)
    (q_5token)
    (q_6token)
    (q_7token)
    (q_8token)
    (q_9token)
    (q_10token)
  )
  (:action push_f1
    :parameters ()
    :precondition 
      (and
        (turn f1)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (check)
        (turn f1)
        (oneof
          (empty)
          (req f1))
        (f_copy)
        (not 
          (turn f1))
        (not 
          (push))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f1
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f1)
        (req f1)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (f_copy)
        (not 
          (turn f1))
        (not 
          (check))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f1
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f1))
        (check)
        (turn f1)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (turn f1))
        (not 
          (check))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f1
    :parameters ()
    :precondition 
      (and
        (at f1)
        (req f1)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f1))
        (not 
          (f_world))
      )
    )
  (:action no_op
    :parameters ()
    :precondition 
      (and
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action mark_served
    :parameters ()
    :precondition 
      (and
        (not 
          (move))
        (f_ok)
        (f_world))
    :effect
      (and
        (move)
        (f_copy)
        (not 
          (served))
        (not 
          (f_world))
      )
    )
  (:action o_copy
    :parameters ()
    :precondition 
      (and
        (f_ok)
        (f_copy))
    :effect
      (and
        (f_sync)
        (when
          (q_1)
          (q_1s))
        (when
          (q_2)
          (q_2s))
        (when
          (q_3)
          (q_3s))
        (when
          (q_4)
          (q_4s))
        (when
          (q_5)
          (q_5s))
        (when
          (q_6)
          (q_6s))
        (when
          (q_7)
          (q_7s))
        (when
          (q_8)
          (q_8s))
        (when
          (q_9)
          (q_9s))
        (when
          (q_10)
          (q_10s))
        (not 
          (f_copy))
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3))
        (not 
          (q_4))
        (not 
          (q_5))
        (not 
          (q_6))
        (not 
          (q_7))
        (not 
          (q_8))
        (not 
          (q_9))
        (not 
          (q_10))
      )
    )
  (:action o_world
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
        (not 
          (q_5s))
        (not 
          (q_6s))
        (not 
          (q_7s))
        (not 
          (q_8s))
        (not 
          (q_9s))
        (not 
          (q_10s)))
    :effect
      (and
        (f_world)
        (not 
          (f_sync))
      )
    )
  (:action o_sync_q_1s
    :parameters ()
    :precondition 
      (and
        (called)
        (f_sync)
        (f_ok)
        (q_1s))
    :effect
      (and
        (not 
          (q_1s))
        (when
          (q_1token)
          (not 
            (q_1token)))
      )
    )
  (:action o_sync_q_2s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_2s))
    :effect
      (and
        (q_5s)
        (q_2)
        (not 
          (q_2s))
        (not 
          (q_2token))
      )
    )
  (:action o_sync_q_3s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_3s))
    :effect
      (and
        (q_6s)
        (q_3)
        (not 
          (q_3s))
        (not 
          (q_3token))
      )
    )
  (:action o_sync_q_4s
    :parameters ()
    :precondition 
      (and
        (at f1)
        (f_sync)
        (f_ok)
        (q_4s))
    :effect
      (and
        (not 
          (q_4s))
        (when
          (q_4token)
          (not 
            (q_4token)))
      )
    )
  (:action o_sync_q_5s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_5s))
    :effect
      (and
        (q_9s)
        (when
          (q_5token)
          (q_9token))
        (not 
          (q_5s))
        (when
          (q_5token)
          (not 
            (q_5token)))
      )
    )
  (:action o_sync_q_5s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_5s))
    :effect
      (and
        (q_5)
        (not 
          (q_5s))
      )
    )
  (:action o_sync_q_6s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_6s))
    :effect
      (and
        (q_10s)
        (when
          (q_6token)
          (q_10token))
        (not 
          (q_6s))
        (when
          (q_6token)
          (not 
            (q_6token)))
      )
    )
  (:action o_sync_q_6s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_6s))
    :effect
      (and
        (q_6)
        (not 
          (q_6s))
      )
    )
  (:action o_sync_q_7s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f1))
        (f_sync)
        (f_ok)
        (q_7s))
    :effect
      (and
        (not 
          (q_7s))
        (when
          (q_7token)
          (not 
            (q_7token)))
      )
    )
  (:action o_sync_q_8s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_8s))
    :effect
      (and
        (q_2s)
        (q_3s)
        (when
          (q_8token)
          (q_2token))
        (when
          (q_8token)
          (q_3token))
        (not 
          (q_8s))
        (when
          (q_8token)
          (not 
            (q_8token)))
      )
    )
  (:action o_sync_q_9s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_9s))
    :effect
      (and
        (q_1s)
        (when
          (q_9token)
          (q_1token))
        (not 
          (q_9s))
        (when
          (q_9token)
          (not 
            (q_9token)))
      )
    )
  (:action o_sync_q_9s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_9s))
    :effect
      (and
        (q_4s)
        (when
          (q_9token)
          (q_4token))
        (not 
          (q_9s))
        (when
          (q_9token)
          (not 
            (q_9token)))
      )
    )
  (:action o_sync_q_10s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_10s))
    :effect
      (and
        (q_4s)
        (when
          (q_10token)
          (q_4token))
        (not 
          (q_10s))
        (when
          (q_10token)
          (not 
            (q_10token)))
      )
    )
  (:action o_sync_q_10s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_10s))
    :effect
      (and
        (q_7s)
        (when
          (q_10token)
          (q_7token))
        (not 
          (q_10s))
        (when
          (q_10token)
          (not 
            (q_10token)))
      )
    )
  (:action jaces_DETDUP_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (not 
          (q_10token))
        (not 
          (q_9token))
        (not 
          (q_8token))
        (not 
          (q_7token))
        (not 
          (q_6token))
        (not 
          (q_5token))
        (not 
          (q_4token))
        (not 
          (q_1token))
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
        (not 
          (q_5s))
        (not 
          (q_6s))
        (not 
          (q_7s))
        (not 
          (q_8s))
        (not 
          (q_9s))
        (not 
          (q_10s)))
    :effect
      (and
        (f_goal)
        (f_world)
        (not 
          (f_sync))
      )
    )
  (:action jaces_DETDUP_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (not 
          (q_10token))
        (not 
          (q_9token))
        (not 
          (q_8token))
        (not 
          (q_7token))
        (not 
          (q_6token))
        (not 
          (q_5token))
        (not 
          (q_4token))
        (not 
          (q_1token))
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
        (not 
          (q_5s))
        (not 
          (q_6s))
        (not 
          (q_7s))
        (not 
          (q_8s))
        (not 
          (q_9s))
        (not 
          (q_10s)))
    :effect
      (and
        (when
          (q_10)
          (q_10token))
        (when
          (q_9)
          (q_9token))
        (when
          (q_8)
          (q_8token))
        (when
          (q_7)
          (q_7token))
        (when
          (q_6)
          (q_6token))
        (when
          (q_5)
          (q_5token))
        (when
          (q_4)
          (q_4token))
        (when
          (q_1)
          (q_1token))
        (f_world)
        (not 
          (f_sync))
      )
    )
)