(define (domain lift-seq-5-lift-5_dp1)
  (:types
    floor - NO_TYPE
  )

  (:predicates
    (at ?x0 - floor)
    (req ?x0 - floor)
    (turn ?x0 - floor)
    (check)
    (called)
    (move)
    (served)
    (f_copy)
    (f_sync)
    (f_world)
    (f_ok)
    (f_goal)
    (q_1)
    (q_1s)
    (q_2)
    (q_2s)
    (q_3)
    (q_3s)
    (q_4)
    (q_4s)
    (q_5)
    (q_5s)
    (q_6)
    (q_6s)
    (q_7)
    (q_7s)
    (q_8)
    (q_8s)
    (q_9)
    (q_9s)
    (q_10)
    (q_10s)
    (q_11)
    (q_11s)
    (q_12)
    (q_12s)
    (q_13)
    (q_13s)
    (q_14)
    (q_14s)
    (q_15)
    (q_15s)
    (q_16)
    (q_16s)
    (q_17)
    (q_17s)
    (q_18)
    (q_18s)
    (q_19)
    (q_19s)
    (q_20)
    (q_20s)
    (q_21)
    (q_21s)
    (q_22)
    (q_22s)
    (q_23)
    (q_23s)
    (q_24)
    (q_24s)
    (q_25)
    (q_25s)
    (q_26)
    (q_26s)
    (q_27)
    (q_27s)
    (q_28)
    (q_28s)
    (q_29)
    (q_29s)
    (q_30)
    (q_30s)
    (q_31)
    (q_31s)
    (q_32)
    (q_32s)
    (q_33)
    (q_33s)
    (q_34)
    (q_34s)
    (q_1token)
    (q_2token)
    (q_3token)
    (q_4token)
    (q_5token)
    (q_6token)
    (q_7token)
    (q_8token)
    (q_9token)
    (q_10token)
    (q_11token)
    (q_12token)
    (q_13token)
    (q_14token)
    (q_15token)
    (q_16token)
    (q_17token)
    (q_18token)
    (q_19token)
    (q_20token)
    (q_21token)
    (q_22token)
    (q_23token)
    (q_24token)
    (q_25token)
    (q_26token)
    (q_27token)
    (q_28token)
    (q_29token)
    (q_30token)
    (q_31token)
    (q_32token)
    (q_33token)
    (q_34token)
  )
  (:action push_f1
    :parameters ()
    :precondition 
      (and
        (turn f1)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f2)
        (oneof
          (empty)
          (req f1))
        (f_copy)
        (not 
          (turn f1))
        (not 
          (f_world))
      )
    )
  (:action push_f2
    :parameters ()
    :precondition 
      (and
        (turn f2)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f3)
        (oneof
          (empty)
          (req f2))
        (f_copy)
        (not 
          (turn f2))
        (not 
          (f_world))
      )
    )
  (:action push_f3
    :parameters ()
    :precondition 
      (and
        (turn f3)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f4)
        (oneof
          (empty)
          (req f3))
        (f_copy)
        (not 
          (turn f3))
        (not 
          (f_world))
      )
    )
  (:action push_f4
    :parameters ()
    :precondition 
      (and
        (turn f4)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f5)
        (oneof
          (empty)
          (req f4))
        (f_copy)
        (not 
          (turn f4))
        (not 
          (f_world))
      )
    )
  (:action push_f5
    :parameters ()
    :precondition 
      (and
        (turn f5)
        (push)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (check)
        (turn f1)
        (oneof
          (empty)
          (req f5))
        (f_copy)
        (not 
          (turn f5))
        (not 
          (push))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f1
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f1)
        (req f1)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f2)
        (f_copy)
        (not 
          (turn f1))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f1
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f1))
        (check)
        (turn f1)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f2)
        (f_copy)
        (not 
          (turn f1))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f2
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f2)
        (req f2)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f3)
        (f_copy)
        (not 
          (turn f2))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f2
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f2))
        (check)
        (turn f2)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f3)
        (f_copy)
        (not 
          (turn f2))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f3
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f3)
        (req f3)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f4)
        (f_copy)
        (not 
          (turn f3))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f3
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f3))
        (check)
        (turn f3)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f4)
        (f_copy)
        (not 
          (turn f3))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f4
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f4)
        (req f4)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (turn f5)
        (f_copy)
        (not 
          (turn f4))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f4
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f4))
        (check)
        (turn f4)
        (f_ok)
        (f_world))
    :effect
      (and
        (turn f5)
        (f_copy)
        (not 
          (turn f4))
        (not 
          (f_world))
      )
    )
  (:action check_called_true_f5
    :parameters ()
    :precondition 
      (and
        (move)
        (check)
        (turn f5)
        (req f5)
        (f_ok)
        (f_world))
    :effect
      (and
        (called)
        (f_copy)
        (not 
          (turn f5))
        (not 
          (check))
        (not 
          (f_world))
      )
    )
  (:action check_called_false_f5
    :parameters ()
    :precondition 
      (and
        (move)
        (not 
          (req f5))
        (check)
        (turn f5)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (turn f5))
        (not 
          (check))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f1
    :parameters ()
    :precondition 
      (and
        (at f1)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f2)
        (turn f1)
        (f_copy)
        (not 
          (at f1))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f1
    :parameters ()
    :precondition 
      (and
        (at f1)
        (req f2)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f2)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f1))
        (not 
          (req f2))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f2
    :parameters ()
    :precondition 
      (and
        (at f2)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f3)
        (turn f1)
        (f_copy)
        (not 
          (at f2))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f2
    :parameters ()
    :precondition 
      (and
        (at f2)
        (req f3)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f3)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f2))
        (not 
          (req f3))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f3
    :parameters ()
    :precondition 
      (and
        (at f3)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f4)
        (turn f1)
        (f_copy)
        (not 
          (at f3))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f3
    :parameters ()
    :precondition 
      (and
        (at f3)
        (req f4)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f4)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f3))
        (not 
          (req f4))
        (not 
          (f_world))
      )
    )
  (:action move_up_from_f4
    :parameters ()
    :precondition 
      (and
        (at f4)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f5)
        (turn f1)
        (f_copy)
        (not 
          (at f4))
        (not 
          (f_world))
      )
    )
  (:action move_up_and_serve_from_f4
    :parameters ()
    :precondition 
      (and
        (at f4)
        (req f5)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f5)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f4))
        (not 
          (req f5))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f5
    :parameters ()
    :precondition 
      (and
        (at f5)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f4)
        (turn f1)
        (f_copy)
        (not 
          (at f5))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f5
    :parameters ()
    :precondition 
      (and
        (at f5)
        (req f4)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f4)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f5))
        (not 
          (req f4))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f4
    :parameters ()
    :precondition 
      (and
        (at f4)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f3)
        (turn f1)
        (f_copy)
        (not 
          (at f4))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f4
    :parameters ()
    :precondition 
      (and
        (at f4)
        (req f3)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f3)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f4))
        (not 
          (req f3))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f3
    :parameters ()
    :precondition 
      (and
        (at f3)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f2)
        (turn f1)
        (f_copy)
        (not 
          (at f3))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f3
    :parameters ()
    :precondition 
      (and
        (at f3)
        (req f2)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f2)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f3))
        (not 
          (req f2))
        (not 
          (f_world))
      )
    )
  (:action move_down_from_f2
    :parameters ()
    :precondition 
      (and
        (at f2)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f1)
        (turn f1)
        (f_copy)
        (not 
          (at f2))
        (not 
          (f_world))
      )
    )
  (:action move_down_and_serve_from_f2
    :parameters ()
    :precondition 
      (and
        (at f2)
        (req f1)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (at f1)
        (served)
        (turn f1)
        (f_copy)
        (not 
          (at f2))
        (not 
          (req f1))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f1
    :parameters ()
    :precondition 
      (and
        (at f1)
        (req f1)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f1))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f2
    :parameters ()
    :precondition 
      (and
        (at f2)
        (req f2)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f2))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f3
    :parameters ()
    :precondition 
      (and
        (at f3)
        (req f3)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f3))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f4
    :parameters ()
    :precondition 
      (and
        (at f4)
        (req f4)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f4))
        (not 
          (f_world))
      )
    )
  (:action stay_and_serve_at_f5
    :parameters ()
    :precondition 
      (and
        (at f5)
        (req f5)
        (called)
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (served)
        (turn f1)
        (f_copy)
        (not 
          (req f5))
        (not 
          (f_world))
      )
    )
  (:action no_op
    :parameters ()
    :precondition 
      (and
        (move)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action mark_served
    :parameters ()
    :precondition 
      (and
        (not 
          (move))
        (f_ok)
        (f_world))
    :effect
      (and
        (move)
        (f_copy)
        (not 
          (served))
        (not 
          (f_world))
      )
    )
  (:action o_copy
    :parameters ()
    :precondition 
      (and
        (f_ok)
        (f_copy))
    :effect
      (and
        (f_sync)
        (when
          (q_1)
          (q_1s))
        (when
          (q_2)
          (q_2s))
        (when
          (q_3)
          (q_3s))
        (when
          (q_4)
          (q_4s))
        (when
          (q_5)
          (q_5s))
        (when
          (q_6)
          (q_6s))
        (when
          (q_7)
          (q_7s))
        (when
          (q_8)
          (q_8s))
        (when
          (q_9)
          (q_9s))
        (when
          (q_10)
          (q_10s))
        (when
          (q_11)
          (q_11s))
        (when
          (q_12)
          (q_12s))
        (when
          (q_13)
          (q_13s))
        (when
          (q_14)
          (q_14s))
        (when
          (q_15)
          (q_15s))
        (when
          (q_16)
          (q_16s))
        (when
          (q_17)
          (q_17s))
        (when
          (q_18)
          (q_18s))
        (when
          (q_19)
          (q_19s))
        (when
          (q_20)
          (q_20s))
        (when
          (q_21)
          (q_21s))
        (when
          (q_22)
          (q_22s))
        (when
          (q_23)
          (q_23s))
        (when
          (q_24)
          (q_24s))
        (when
          (q_25)
          (q_25s))
        (when
          (q_26)
          (q_26s))
        (when
          (q_27)
          (q_27s))
        (when
          (q_28)
          (q_28s))
        (when
          (q_29)
          (q_29s))
        (when
          (q_30)
          (q_30s))
        (when
          (q_31)
          (q_31s))
        (when
          (q_32)
          (q_32s))
        (when
          (q_33)
          (q_33s))
        (when
          (q_34)
          (q_34s))
        (not 
          (f_copy))
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3))
        (not 
          (q_4))
        (not 
          (q_5))
        (not 
          (q_6))
        (not 
          (q_7))
        (not 
          (q_8))
        (not 
          (q_9))
        (not 
          (q_10))
        (not 
          (q_11))
        (not 
          (q_12))
        (not 
          (q_13))
        (not 
          (q_14))
        (not 
          (q_15))
        (not 
          (q_16))
        (not 
          (q_17))
        (not 
          (q_18))
        (not 
          (q_19))
        (not 
          (q_20))
        (not 
          (q_21))
        (not 
          (q_22))
        (not 
          (q_23))
        (not 
          (q_24))
        (not 
          (q_25))
        (not 
          (q_26))
        (not 
          (q_27))
        (not 
          (q_28))
        (not 
          (q_29))
        (not 
          (q_30))
        (not 
          (q_31))
        (not 
          (q_32))
        (not 
          (q_33))
        (not 
          (q_34))
      )
    )
  (:action o_world
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
        (not 
          (q_5s))
        (not 
          (q_6s))
        (not 
          (q_7s))
        (not 
          (q_8s))
        (not 
          (q_9s))
        (not 
          (q_10s))
        (not 
          (q_11s))
        (not 
          (q_12s))
        (not 
          (q_13s))
        (not 
          (q_14s))
        (not 
          (q_15s))
        (not 
          (q_16s))
        (not 
          (q_17s))
        (not 
          (q_18s))
        (not 
          (q_19s))
        (not 
          (q_20s))
        (not 
          (q_21s))
        (not 
          (q_22s))
        (not 
          (q_23s))
        (not 
          (q_24s))
        (not 
          (q_25s))
        (not 
          (q_26s))
        (not 
          (q_27s))
        (not 
          (q_28s))
        (not 
          (q_29s))
        (not 
          (q_30s))
        (not 
          (q_31s))
        (not 
          (q_32s))
        (not 
          (q_33s))
        (not 
          (q_34s)))
    :effect
      (and
        (f_world)
        (not 
          (f_sync))
      )
    )
  (:action o_sync_q_1s
    :parameters ()
    :precondition 
      (and
        (called)
        (f_sync)
        (f_ok)
        (q_1s))
    :effect
      (and
        (not 
          (q_1s))
        (when
          (q_1token)
          (not 
            (q_1token)))
      )
    )
  (:action o_sync_q_2s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_2s))
    :effect
      (and
        (q_13s)
        (q_2)
        (not 
          (q_2s))
        (not 
          (q_2token))
      )
    )
  (:action o_sync_q_3s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_3s))
    :effect
      (and
        (q_14s)
        (q_3)
        (not 
          (q_3s))
        (not 
          (q_3token))
      )
    )
  (:action o_sync_q_4s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_4s))
    :effect
      (and
        (q_15s)
        (q_4)
        (not 
          (q_4s))
        (not 
          (q_4token))
      )
    )
  (:action o_sync_q_5s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_5s))
    :effect
      (and
        (q_16s)
        (q_5)
        (not 
          (q_5s))
        (not 
          (q_5token))
      )
    )
  (:action o_sync_q_6s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_6s))
    :effect
      (and
        (q_17s)
        (q_6)
        (not 
          (q_6s))
        (not 
          (q_6token))
      )
    )
  (:action o_sync_q_7s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_7s))
    :effect
      (and
        (q_18s)
        (q_7)
        (not 
          (q_7s))
        (not 
          (q_7token))
      )
    )
  (:action o_sync_q_8s
    :parameters ()
    :precondition 
      (and
        (at f1)
        (f_sync)
        (f_ok)
        (q_8s))
    :effect
      (and
        (not 
          (q_8s))
        (when
          (q_8token)
          (not 
            (q_8token)))
      )
    )
  (:action o_sync_q_9s
    :parameters ()
    :precondition 
      (and
        (at f2)
        (f_sync)
        (f_ok)
        (q_9s))
    :effect
      (and
        (not 
          (q_9s))
        (when
          (q_9token)
          (not 
            (q_9token)))
      )
    )
  (:action o_sync_q_10s
    :parameters ()
    :precondition 
      (and
        (at f3)
        (f_sync)
        (f_ok)
        (q_10s))
    :effect
      (and
        (not 
          (q_10s))
        (when
          (q_10token)
          (not 
            (q_10token)))
      )
    )
  (:action o_sync_q_11s
    :parameters ()
    :precondition 
      (and
        (at f4)
        (f_sync)
        (f_ok)
        (q_11s))
    :effect
      (and
        (not 
          (q_11s))
        (when
          (q_11token)
          (not 
            (q_11token)))
      )
    )
  (:action o_sync_q_12s
    :parameters ()
    :precondition 
      (and
        (at f5)
        (f_sync)
        (f_ok)
        (q_12s))
    :effect
      (and
        (not 
          (q_12s))
        (when
          (q_12token)
          (not 
            (q_12token)))
      )
    )
  (:action o_sync_q_13s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_13s))
    :effect
      (and
        (q_29s)
        (when
          (q_13token)
          (q_29token))
        (not 
          (q_13s))
        (when
          (q_13token)
          (not 
            (q_13token)))
      )
    )
  (:action o_sync_q_13s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_13s))
    :effect
      (and
        (q_13)
        (not 
          (q_13s))
      )
    )
  (:action o_sync_q_14s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_14s))
    :effect
      (and
        (q_30s)
        (when
          (q_14token)
          (q_30token))
        (not 
          (q_14s))
        (when
          (q_14token)
          (not 
            (q_14token)))
      )
    )
  (:action o_sync_q_14s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_14s))
    :effect
      (and
        (q_14)
        (not 
          (q_14s))
      )
    )
  (:action o_sync_q_15s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_15s))
    :effect
      (and
        (q_31s)
        (when
          (q_15token)
          (q_31token))
        (not 
          (q_15s))
        (when
          (q_15token)
          (not 
            (q_15token)))
      )
    )
  (:action o_sync_q_15s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_15s))
    :effect
      (and
        (q_15)
        (not 
          (q_15s))
      )
    )
  (:action o_sync_q_16s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_16s))
    :effect
      (and
        (q_32s)
        (when
          (q_16token)
          (q_32token))
        (not 
          (q_16s))
        (when
          (q_16token)
          (not 
            (q_16token)))
      )
    )
  (:action o_sync_q_16s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_16s))
    :effect
      (and
        (q_16)
        (not 
          (q_16s))
      )
    )
  (:action o_sync_q_17s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_17s))
    :effect
      (and
        (q_33s)
        (when
          (q_17token)
          (q_33token))
        (not 
          (q_17s))
        (when
          (q_17token)
          (not 
            (q_17token)))
      )
    )
  (:action o_sync_q_17s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_17s))
    :effect
      (and
        (q_17)
        (not 
          (q_17s))
      )
    )
  (:action o_sync_q_18s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_18s))
    :effect
      (and
        (q_34s)
        (when
          (q_18token)
          (q_34token))
        (not 
          (q_18s))
        (when
          (q_18token)
          (not 
            (q_18token)))
      )
    )
  (:action o_sync_q_18s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_18s))
    :effect
      (and
        (q_18)
        (not 
          (q_18s))
      )
    )
  (:action o_sync_q_19s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f1))
        (f_sync)
        (f_ok)
        (q_19s))
    :effect
      (and
        (not 
          (q_19s))
        (when
          (q_19token)
          (not 
            (q_19token)))
      )
    )
  (:action o_sync_q_20s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f2))
        (f_sync)
        (f_ok)
        (q_20s))
    :effect
      (and
        (not 
          (q_20s))
        (when
          (q_20token)
          (not 
            (q_20token)))
      )
    )
  (:action o_sync_q_21s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f3))
        (f_sync)
        (f_ok)
        (q_21s))
    :effect
      (and
        (not 
          (q_21s))
        (when
          (q_21token)
          (not 
            (q_21token)))
      )
    )
  (:action o_sync_q_22s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f4))
        (f_sync)
        (f_ok)
        (q_22s))
    :effect
      (and
        (not 
          (q_22s))
        (when
          (q_22token)
          (not 
            (q_22token)))
      )
    )
  (:action o_sync_q_23s
    :parameters ()
    :precondition 
      (and
        (not 
          (req f5))
        (f_sync)
        (f_ok)
        (q_23s))
    :effect
      (and
        (not 
          (q_23s))
        (when
          (q_23token)
          (not 
            (q_23token)))
      )
    )
  (:action o_sync_q_24s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_24s))
    :effect
      (and
        (q_2s)
        (q_7s)
        (when
          (q_24token)
          (q_2token))
        (when
          (q_24token)
          (q_7token))
        (not 
          (q_24s))
        (when
          (q_24token)
          (not 
            (q_24token)))
      )
    )
  (:action o_sync_q_25s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_25s))
    :effect
      (and
        (q_3s)
        (q_26s)
        (when
          (q_25token)
          (q_3token))
        (when
          (q_25token)
          (q_26token))
        (not 
          (q_25s))
        (when
          (q_25token)
          (not 
            (q_25token)))
      )
    )
  (:action o_sync_q_26s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_26s))
    :effect
      (and
        (q_4s)
        (q_27s)
        (when
          (q_26token)
          (q_4token))
        (when
          (q_26token)
          (q_27token))
        (not 
          (q_26s))
        (when
          (q_26token)
          (not 
            (q_26token)))
      )
    )
  (:action o_sync_q_27s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_27s))
    :effect
      (and
        (q_5s)
        (q_28s)
        (when
          (q_27token)
          (q_5token))
        (when
          (q_27token)
          (q_28token))
        (not 
          (q_27s))
        (when
          (q_27token)
          (not 
            (q_27token)))
      )
    )
  (:action o_sync_q_28s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_28s))
    :effect
      (and
        (q_6s)
        (q_24s)
        (when
          (q_28token)
          (q_6token))
        (when
          (q_28token)
          (q_24token))
        (not 
          (q_28s))
        (when
          (q_28token)
          (not 
            (q_28token)))
      )
    )
  (:action o_sync_q_29s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_29s))
    :effect
      (and
        (q_1s)
        (when
          (q_29token)
          (q_1token))
        (not 
          (q_29s))
        (when
          (q_29token)
          (not 
            (q_29token)))
      )
    )
  (:action o_sync_q_29s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_29s))
    :effect
      (and
        (q_8s)
        (when
          (q_29token)
          (q_8token))
        (not 
          (q_29s))
        (when
          (q_29token)
          (not 
            (q_29token)))
      )
    )
  (:action o_sync_q_30s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_30s))
    :effect
      (and
        (q_8s)
        (when
          (q_30token)
          (q_8token))
        (not 
          (q_30s))
        (when
          (q_30token)
          (not 
            (q_30token)))
      )
    )
  (:action o_sync_q_30s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_30s))
    :effect
      (and
        (q_19s)
        (when
          (q_30token)
          (q_19token))
        (not 
          (q_30s))
        (when
          (q_30token)
          (not 
            (q_30token)))
      )
    )
  (:action o_sync_q_31s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_31s))
    :effect
      (and
        (q_9s)
        (when
          (q_31token)
          (q_9token))
        (not 
          (q_31s))
        (when
          (q_31token)
          (not 
            (q_31token)))
      )
    )
  (:action o_sync_q_31s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_31s))
    :effect
      (and
        (q_20s)
        (when
          (q_31token)
          (q_20token))
        (not 
          (q_31s))
        (when
          (q_31token)
          (not 
            (q_31token)))
      )
    )
  (:action o_sync_q_32s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_32s))
    :effect
      (and
        (q_10s)
        (when
          (q_32token)
          (q_10token))
        (not 
          (q_32s))
        (when
          (q_32token)
          (not 
            (q_32token)))
      )
    )
  (:action o_sync_q_32s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_32s))
    :effect
      (and
        (q_21s)
        (when
          (q_32token)
          (q_21token))
        (not 
          (q_32s))
        (when
          (q_32token)
          (not 
            (q_32token)))
      )
    )
  (:action o_sync_q_33s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_33s))
    :effect
      (and
        (q_11s)
        (when
          (q_33token)
          (q_11token))
        (not 
          (q_33s))
        (when
          (q_33token)
          (not 
            (q_33token)))
      )
    )
  (:action o_sync_q_33s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_33s))
    :effect
      (and
        (q_22s)
        (when
          (q_33token)
          (q_22token))
        (not 
          (q_33s))
        (when
          (q_33token)
          (not 
            (q_33token)))
      )
    )
  (:action o_sync_q_34s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_34s))
    :effect
      (and
        (q_12s)
        (when
          (q_34token)
          (q_12token))
        (not 
          (q_34s))
        (when
          (q_34token)
          (not 
            (q_34token)))
      )
    )
  (:action o_sync_q_34s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_34s))
    :effect
      (and
        (q_23s)
        (when
          (q_34token)
          (q_23token))
        (not 
          (q_34s))
        (when
          (q_34token)
          (not 
            (q_34token)))
      )
    )
  (:action jaces_DETDUP_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (not 
          (q_34token))
        (not 
          (q_33token))
        (not 
          (q_32token))
        (not 
          (q_31token))
        (not 
          (q_30token))
        (not 
          (q_29token))
        (not 
          (q_28token))
        (not 
          (q_27token))
        (not 
          (q_26token))
        (not 
          (q_25token))
        (not 
          (q_24token))
        (not 
          (q_23token))
        (not 
          (q_22token))
        (not 
          (q_21token))
        (not 
          (q_20token))
        (not 
          (q_19token))
        (not 
          (q_18token))
        (not 
          (q_17token))
        (not 
          (q_16token))
        (not 
          (q_15token))
        (not 
          (q_14token))
        (not 
          (q_13token))
        (not 
          (q_12token))
        (not 
          (q_11token))
        (not 
          (q_10token))
        (not 
          (q_9token))
        (not 
          (q_8token))
        (not 
          (q_1token))
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
        (not 
          (q_5s))
        (not 
          (q_6s))
        (not 
          (q_7s))
        (not 
          (q_8s))
        (not 
          (q_9s))
        (not 
          (q_10s))
        (not 
          (q_11s))
        (not 
          (q_12s))
        (not 
          (q_13s))
        (not 
          (q_14s))
        (not 
          (q_15s))
        (not 
          (q_16s))
        (not 
          (q_17s))
        (not 
          (q_18s))
        (not 
          (q_19s))
        (not 
          (q_20s))
        (not 
          (q_21s))
        (not 
          (q_22s))
        (not 
          (q_23s))
        (not 
          (q_24s))
        (not 
          (q_25s))
        (not 
          (q_26s))
        (not 
          (q_27s))
        (not 
          (q_28s))
        (not 
          (q_29s))
        (not 
          (q_30s))
        (not 
          (q_31s))
        (not 
          (q_32s))
        (not 
          (q_33s))
        (not 
          (q_34s)))
    :effect
      (and
        (f_goal)
        (f_world)
        (not 
          (f_sync))
      )
    )
  (:action jaces_DETDUP_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (not 
          (q_34token))
        (not 
          (q_33token))
        (not 
          (q_32token))
        (not 
          (q_31token))
        (not 
          (q_30token))
        (not 
          (q_29token))
        (not 
          (q_28token))
        (not 
          (q_27token))
        (not 
          (q_26token))
        (not 
          (q_25token))
        (not 
          (q_24token))
        (not 
          (q_23token))
        (not 
          (q_22token))
        (not 
          (q_21token))
        (not 
          (q_20token))
        (not 
          (q_19token))
        (not 
          (q_18token))
        (not 
          (q_17token))
        (not 
          (q_16token))
        (not 
          (q_15token))
        (not 
          (q_14token))
        (not 
          (q_13token))
        (not 
          (q_12token))
        (not 
          (q_11token))
        (not 
          (q_10token))
        (not 
          (q_9token))
        (not 
          (q_8token))
        (not 
          (q_1token))
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
        (not 
          (q_5s))
        (not 
          (q_6s))
        (not 
          (q_7s))
        (not 
          (q_8s))
        (not 
          (q_9s))
        (not 
          (q_10s))
        (not 
          (q_11s))
        (not 
          (q_12s))
        (not 
          (q_13s))
        (not 
          (q_14s))
        (not 
          (q_15s))
        (not 
          (q_16s))
        (not 
          (q_17s))
        (not 
          (q_18s))
        (not 
          (q_19s))
        (not 
          (q_20s))
        (not 
          (q_21s))
        (not 
          (q_22s))
        (not 
          (q_23s))
        (not 
          (q_24s))
        (not 
          (q_25s))
        (not 
          (q_26s))
        (not 
          (q_27s))
        (not 
          (q_28s))
        (not 
          (q_29s))
        (not 
          (q_30s))
        (not 
          (q_31s))
        (not 
          (q_32s))
        (not 
          (q_33s))
        (not 
          (q_34s)))
    :effect
      (and
        (when
          (q_34)
          (q_34token))
        (when
          (q_33)
          (q_33token))
        (when
          (q_32)
          (q_32token))
        (when
          (q_31)
          (q_31token))
        (when
          (q_30)
          (q_30token))
        (when
          (q_29)
          (q_29token))
        (when
          (q_28)
          (q_28token))
        (when
          (q_27)
          (q_27token))
        (when
          (q_26)
          (q_26token))
        (when
          (q_25)
          (q_25token))
        (when
          (q_24)
          (q_24token))
        (when
          (q_23)
          (q_23token))
        (when
          (q_22)
          (q_22token))
        (when
          (q_21)
          (q_21token))
        (when
          (q_20)
          (q_20token))
        (when
          (q_19)
          (q_19token))
        (when
          (q_18)
          (q_18token))
        (when
          (q_17)
          (q_17token))
        (when
          (q_16)
          (q_16token))
        (when
          (q_15)
          (q_15token))
        (when
          (q_14)
          (q_14token))
        (when
          (q_13)
          (q_13token))
        (when
          (q_12)
          (q_12token))
        (when
          (q_11)
          (q_11token))
        (when
          (q_10)
          (q_10token))
        (when
          (q_9)
          (q_9token))
        (when
          (q_8)
          (q_8token))
        (when
          (q_1)
          (q_1token))
        (f_world)
        (not 
          (f_sync))
      )
    )
)