(define (problem 'lift-1-lift-seq-1_dp1')

  (:domain lift-seq-1-lift-1_dp1)
  (:objects f1 - floor)
  (:init 
    (at f1)
    (move)
    (q_8)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)