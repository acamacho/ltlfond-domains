(define (domain waldo-seq-1000-instance-nobucci_dp2)
  (:types
    room - NO_TYPE
  )

  (:predicates
    (seen)
    (in ?x0 - room)
    (f_copy)
    (f_sync)
    (f_world)
    (f_ok)
    (f_goal)
    (q_1)
    (q_1s)
    (q_1t)
    (q_2)
    (q_2s)
    (q_2t)
    (q_3)
    (q_3s)
    (q_3t)
    (q_4)
    (q_4s)
    (q_4t)
  )
  (:action move-right-from-r1
    :parameters ()
    :precondition 
      (and
        (in r1)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r2)
        (f_copy)
        (not 
          (in r1))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r1
    :parameters ()
    :precondition 
      (and
        (in r1)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r1000)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r1))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r2
    :parameters ()
    :precondition 
      (and
        (in r2)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r3)
        (f_copy)
        (not 
          (in r2))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r2
    :parameters ()
    :precondition 
      (and
        (in r2)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r1)
        (f_copy)
        (not 
          (in r2))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r3
    :parameters ()
    :precondition 
      (and
        (in r3)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r4)
        (f_copy)
        (not 
          (in r3))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r3
    :parameters ()
    :precondition 
      (and
        (in r3)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r2)
        (f_copy)
        (not 
          (in r3))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r4
    :parameters ()
    :precondition 
      (and
        (in r4)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r5)
        (f_copy)
        (not 
          (in r4))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r4
    :parameters ()
    :precondition 
      (and
        (in r4)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r3)
        (f_copy)
        (not 
          (in r4))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r5
    :parameters ()
    :precondition 
      (and
        (in r5)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r6)
        (f_copy)
        (not 
          (in r5))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r5
    :parameters ()
    :precondition 
      (and
        (in r5)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r4)
        (f_copy)
        (not 
          (in r5))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r6
    :parameters ()
    :precondition 
      (and
        (in r6)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r7)
        (f_copy)
        (not 
          (in r6))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r6
    :parameters ()
    :precondition 
      (and
        (in r6)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r5)
        (f_copy)
        (not 
          (in r6))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r7
    :parameters ()
    :precondition 
      (and
        (in r7)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r8)
        (f_copy)
        (not 
          (in r7))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r7
    :parameters ()
    :precondition 
      (and
        (in r7)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r6)
        (f_copy)
        (not 
          (in r7))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r8
    :parameters ()
    :precondition 
      (and
        (in r8)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r9)
        (f_copy)
        (not 
          (in r8))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r8
    :parameters ()
    :precondition 
      (and
        (in r8)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r7)
        (f_copy)
        (not 
          (in r8))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r9
    :parameters ()
    :precondition 
      (and
        (in r9)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r10)
        (f_copy)
        (not 
          (in r9))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r9
    :parameters ()
    :precondition 
      (and
        (in r9)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r8)
        (f_copy)
        (not 
          (in r9))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r10
    :parameters ()
    :precondition 
      (and
        (in r10)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r11)
        (f_copy)
        (not 
          (in r10))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r10
    :parameters ()
    :precondition 
      (and
        (in r10)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r9)
        (f_copy)
        (not 
          (in r10))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r11
    :parameters ()
    :precondition 
      (and
        (in r11)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r12)
        (f_copy)
        (not 
          (in r11))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r11
    :parameters ()
    :precondition 
      (and
        (in r11)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r10)
        (f_copy)
        (not 
          (in r11))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r12
    :parameters ()
    :precondition 
      (and
        (in r12)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r13)
        (f_copy)
        (not 
          (in r12))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r12
    :parameters ()
    :precondition 
      (and
        (in r12)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r11)
        (f_copy)
        (not 
          (in r12))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r13
    :parameters ()
    :precondition 
      (and
        (in r13)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r14)
        (f_copy)
        (not 
          (in r13))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r13
    :parameters ()
    :precondition 
      (and
        (in r13)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r12)
        (f_copy)
        (not 
          (in r13))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r14
    :parameters ()
    :precondition 
      (and
        (in r14)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r15)
        (f_copy)
        (not 
          (in r14))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r14
    :parameters ()
    :precondition 
      (and
        (in r14)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r13)
        (f_copy)
        (not 
          (in r14))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r15
    :parameters ()
    :precondition 
      (and
        (in r15)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r16)
        (f_copy)
        (not 
          (in r15))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r15
    :parameters ()
    :precondition 
      (and
        (in r15)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r14)
        (f_copy)
        (not 
          (in r15))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r16
    :parameters ()
    :precondition 
      (and
        (in r16)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r17)
        (f_copy)
        (not 
          (in r16))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r16
    :parameters ()
    :precondition 
      (and
        (in r16)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r15)
        (f_copy)
        (not 
          (in r16))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r17
    :parameters ()
    :precondition 
      (and
        (in r17)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r18)
        (f_copy)
        (not 
          (in r17))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r17
    :parameters ()
    :precondition 
      (and
        (in r17)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r16)
        (f_copy)
        (not 
          (in r17))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r18
    :parameters ()
    :precondition 
      (and
        (in r18)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r19)
        (f_copy)
        (not 
          (in r18))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r18
    :parameters ()
    :precondition 
      (and
        (in r18)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r17)
        (f_copy)
        (not 
          (in r18))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r19
    :parameters ()
    :precondition 
      (and
        (in r19)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r20)
        (f_copy)
        (not 
          (in r19))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r19
    :parameters ()
    :precondition 
      (and
        (in r19)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r18)
        (f_copy)
        (not 
          (in r19))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r20
    :parameters ()
    :precondition 
      (and
        (in r20)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r21)
        (f_copy)
        (not 
          (in r20))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r20
    :parameters ()
    :precondition 
      (and
        (in r20)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r19)
        (f_copy)
        (not 
          (in r20))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r21
    :parameters ()
    :precondition 
      (and
        (in r21)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r22)
        (f_copy)
        (not 
          (in r21))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r21
    :parameters ()
    :precondition 
      (and
        (in r21)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r20)
        (f_copy)
        (not 
          (in r21))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r22
    :parameters ()
    :precondition 
      (and
        (in r22)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r23)
        (f_copy)
        (not 
          (in r22))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r22
    :parameters ()
    :precondition 
      (and
        (in r22)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r21)
        (f_copy)
        (not 
          (in r22))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r23
    :parameters ()
    :precondition 
      (and
        (in r23)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r24)
        (f_copy)
        (not 
          (in r23))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r23
    :parameters ()
    :precondition 
      (and
        (in r23)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r22)
        (f_copy)
        (not 
          (in r23))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r24
    :parameters ()
    :precondition 
      (and
        (in r24)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r25)
        (f_copy)
        (not 
          (in r24))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r24
    :parameters ()
    :precondition 
      (and
        (in r24)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r23)
        (f_copy)
        (not 
          (in r24))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r25
    :parameters ()
    :precondition 
      (and
        (in r25)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r26)
        (f_copy)
        (not 
          (in r25))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r25
    :parameters ()
    :precondition 
      (and
        (in r25)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r24)
        (f_copy)
        (not 
          (in r25))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r26
    :parameters ()
    :precondition 
      (and
        (in r26)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r27)
        (f_copy)
        (not 
          (in r26))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r26
    :parameters ()
    :precondition 
      (and
        (in r26)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r25)
        (f_copy)
        (not 
          (in r26))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r27
    :parameters ()
    :precondition 
      (and
        (in r27)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r28)
        (f_copy)
        (not 
          (in r27))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r27
    :parameters ()
    :precondition 
      (and
        (in r27)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r26)
        (f_copy)
        (not 
          (in r27))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r28
    :parameters ()
    :precondition 
      (and
        (in r28)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r29)
        (f_copy)
        (not 
          (in r28))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r28
    :parameters ()
    :precondition 
      (and
        (in r28)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r27)
        (f_copy)
        (not 
          (in r28))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r29
    :parameters ()
    :precondition 
      (and
        (in r29)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r30)
        (f_copy)
        (not 
          (in r29))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r29
    :parameters ()
    :precondition 
      (and
        (in r29)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r28)
        (f_copy)
        (not 
          (in r29))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r30
    :parameters ()
    :precondition 
      (and
        (in r30)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r31)
        (f_copy)
        (not 
          (in r30))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r30
    :parameters ()
    :precondition 
      (and
        (in r30)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r29)
        (f_copy)
        (not 
          (in r30))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r31
    :parameters ()
    :precondition 
      (and
        (in r31)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r32)
        (f_copy)
        (not 
          (in r31))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r31
    :parameters ()
    :precondition 
      (and
        (in r31)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r30)
        (f_copy)
        (not 
          (in r31))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r32
    :parameters ()
    :precondition 
      (and
        (in r32)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r33)
        (f_copy)
        (not 
          (in r32))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r32
    :parameters ()
    :precondition 
      (and
        (in r32)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r31)
        (f_copy)
        (not 
          (in r32))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r33
    :parameters ()
    :precondition 
      (and
        (in r33)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r34)
        (f_copy)
        (not 
          (in r33))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r33
    :parameters ()
    :precondition 
      (and
        (in r33)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r32)
        (f_copy)
        (not 
          (in r33))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r34
    :parameters ()
    :precondition 
      (and
        (in r34)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r35)
        (f_copy)
        (not 
          (in r34))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r34
    :parameters ()
    :precondition 
      (and
        (in r34)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r33)
        (f_copy)
        (not 
          (in r34))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r35
    :parameters ()
    :precondition 
      (and
        (in r35)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r36)
        (f_copy)
        (not 
          (in r35))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r35
    :parameters ()
    :precondition 
      (and
        (in r35)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r34)
        (f_copy)
        (not 
          (in r35))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r36
    :parameters ()
    :precondition 
      (and
        (in r36)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r37)
        (f_copy)
        (not 
          (in r36))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r36
    :parameters ()
    :precondition 
      (and
        (in r36)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r35)
        (f_copy)
        (not 
          (in r36))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r37
    :parameters ()
    :precondition 
      (and
        (in r37)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r38)
        (f_copy)
        (not 
          (in r37))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r37
    :parameters ()
    :precondition 
      (and
        (in r37)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r36)
        (f_copy)
        (not 
          (in r37))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r38
    :parameters ()
    :precondition 
      (and
        (in r38)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r39)
        (f_copy)
        (not 
          (in r38))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r38
    :parameters ()
    :precondition 
      (and
        (in r38)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r37)
        (f_copy)
        (not 
          (in r38))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r39
    :parameters ()
    :precondition 
      (and
        (in r39)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r40)
        (f_copy)
        (not 
          (in r39))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r39
    :parameters ()
    :precondition 
      (and
        (in r39)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r38)
        (f_copy)
        (not 
          (in r39))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r40
    :parameters ()
    :precondition 
      (and
        (in r40)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r41)
        (f_copy)
        (not 
          (in r40))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r40
    :parameters ()
    :precondition 
      (and
        (in r40)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r39)
        (f_copy)
        (not 
          (in r40))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r41
    :parameters ()
    :precondition 
      (and
        (in r41)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r42)
        (f_copy)
        (not 
          (in r41))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r41
    :parameters ()
    :precondition 
      (and
        (in r41)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r40)
        (f_copy)
        (not 
          (in r41))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r42
    :parameters ()
    :precondition 
      (and
        (in r42)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r43)
        (f_copy)
        (not 
          (in r42))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r42
    :parameters ()
    :precondition 
      (and
        (in r42)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r41)
        (f_copy)
        (not 
          (in r42))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r43
    :parameters ()
    :precondition 
      (and
        (in r43)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r44)
        (f_copy)
        (not 
          (in r43))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r43
    :parameters ()
    :precondition 
      (and
        (in r43)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r42)
        (f_copy)
        (not 
          (in r43))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r44
    :parameters ()
    :precondition 
      (and
        (in r44)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r45)
        (f_copy)
        (not 
          (in r44))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r44
    :parameters ()
    :precondition 
      (and
        (in r44)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r43)
        (f_copy)
        (not 
          (in r44))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r45
    :parameters ()
    :precondition 
      (and
        (in r45)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r46)
        (f_copy)
        (not 
          (in r45))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r45
    :parameters ()
    :precondition 
      (and
        (in r45)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r44)
        (f_copy)
        (not 
          (in r45))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r46
    :parameters ()
    :precondition 
      (and
        (in r46)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r47)
        (f_copy)
        (not 
          (in r46))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r46
    :parameters ()
    :precondition 
      (and
        (in r46)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r45)
        (f_copy)
        (not 
          (in r46))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r47
    :parameters ()
    :precondition 
      (and
        (in r47)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r48)
        (f_copy)
        (not 
          (in r47))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r47
    :parameters ()
    :precondition 
      (and
        (in r47)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r46)
        (f_copy)
        (not 
          (in r47))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r48
    :parameters ()
    :precondition 
      (and
        (in r48)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r49)
        (f_copy)
        (not 
          (in r48))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r48
    :parameters ()
    :precondition 
      (and
        (in r48)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r47)
        (f_copy)
        (not 
          (in r48))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r49
    :parameters ()
    :precondition 
      (and
        (in r49)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r50)
        (f_copy)
        (not 
          (in r49))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r49
    :parameters ()
    :precondition 
      (and
        (in r49)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r48)
        (f_copy)
        (not 
          (in r49))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r50
    :parameters ()
    :precondition 
      (and
        (in r50)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r51)
        (f_copy)
        (not 
          (in r50))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r50
    :parameters ()
    :precondition 
      (and
        (in r50)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r49)
        (f_copy)
        (not 
          (in r50))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r51
    :parameters ()
    :precondition 
      (and
        (in r51)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r52)
        (f_copy)
        (not 
          (in r51))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r51
    :parameters ()
    :precondition 
      (and
        (in r51)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r50)
        (f_copy)
        (not 
          (in r51))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r52
    :parameters ()
    :precondition 
      (and
        (in r52)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r53)
        (f_copy)
        (not 
          (in r52))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r52
    :parameters ()
    :precondition 
      (and
        (in r52)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r51)
        (f_copy)
        (not 
          (in r52))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r53
    :parameters ()
    :precondition 
      (and
        (in r53)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r54)
        (f_copy)
        (not 
          (in r53))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r53
    :parameters ()
    :precondition 
      (and
        (in r53)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r52)
        (f_copy)
        (not 
          (in r53))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r54
    :parameters ()
    :precondition 
      (and
        (in r54)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r55)
        (f_copy)
        (not 
          (in r54))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r54
    :parameters ()
    :precondition 
      (and
        (in r54)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r53)
        (f_copy)
        (not 
          (in r54))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r55
    :parameters ()
    :precondition 
      (and
        (in r55)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r56)
        (f_copy)
        (not 
          (in r55))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r55
    :parameters ()
    :precondition 
      (and
        (in r55)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r54)
        (f_copy)
        (not 
          (in r55))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r56
    :parameters ()
    :precondition 
      (and
        (in r56)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r57)
        (f_copy)
        (not 
          (in r56))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r56
    :parameters ()
    :precondition 
      (and
        (in r56)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r55)
        (f_copy)
        (not 
          (in r56))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r57
    :parameters ()
    :precondition 
      (and
        (in r57)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r58)
        (f_copy)
        (not 
          (in r57))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r57
    :parameters ()
    :precondition 
      (and
        (in r57)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r56)
        (f_copy)
        (not 
          (in r57))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r58
    :parameters ()
    :precondition 
      (and
        (in r58)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r59)
        (f_copy)
        (not 
          (in r58))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r58
    :parameters ()
    :precondition 
      (and
        (in r58)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r57)
        (f_copy)
        (not 
          (in r58))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r59
    :parameters ()
    :precondition 
      (and
        (in r59)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r60)
        (f_copy)
        (not 
          (in r59))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r59
    :parameters ()
    :precondition 
      (and
        (in r59)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r58)
        (f_copy)
        (not 
          (in r59))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r60
    :parameters ()
    :precondition 
      (and
        (in r60)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r61)
        (f_copy)
        (not 
          (in r60))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r60
    :parameters ()
    :precondition 
      (and
        (in r60)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r59)
        (f_copy)
        (not 
          (in r60))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r61
    :parameters ()
    :precondition 
      (and
        (in r61)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r62)
        (f_copy)
        (not 
          (in r61))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r61
    :parameters ()
    :precondition 
      (and
        (in r61)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r60)
        (f_copy)
        (not 
          (in r61))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r62
    :parameters ()
    :precondition 
      (and
        (in r62)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r63)
        (f_copy)
        (not 
          (in r62))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r62
    :parameters ()
    :precondition 
      (and
        (in r62)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r61)
        (f_copy)
        (not 
          (in r62))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r63
    :parameters ()
    :precondition 
      (and
        (in r63)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r64)
        (f_copy)
        (not 
          (in r63))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r63
    :parameters ()
    :precondition 
      (and
        (in r63)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r62)
        (f_copy)
        (not 
          (in r63))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r64
    :parameters ()
    :precondition 
      (and
        (in r64)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r65)
        (f_copy)
        (not 
          (in r64))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r64
    :parameters ()
    :precondition 
      (and
        (in r64)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r63)
        (f_copy)
        (not 
          (in r64))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r65
    :parameters ()
    :precondition 
      (and
        (in r65)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r66)
        (f_copy)
        (not 
          (in r65))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r65
    :parameters ()
    :precondition 
      (and
        (in r65)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r64)
        (f_copy)
        (not 
          (in r65))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r66
    :parameters ()
    :precondition 
      (and
        (in r66)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r67)
        (f_copy)
        (not 
          (in r66))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r66
    :parameters ()
    :precondition 
      (and
        (in r66)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r65)
        (f_copy)
        (not 
          (in r66))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r67
    :parameters ()
    :precondition 
      (and
        (in r67)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r68)
        (f_copy)
        (not 
          (in r67))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r67
    :parameters ()
    :precondition 
      (and
        (in r67)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r66)
        (f_copy)
        (not 
          (in r67))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r68
    :parameters ()
    :precondition 
      (and
        (in r68)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r69)
        (f_copy)
        (not 
          (in r68))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r68
    :parameters ()
    :precondition 
      (and
        (in r68)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r67)
        (f_copy)
        (not 
          (in r68))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r69
    :parameters ()
    :precondition 
      (and
        (in r69)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r70)
        (f_copy)
        (not 
          (in r69))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r69
    :parameters ()
    :precondition 
      (and
        (in r69)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r68)
        (f_copy)
        (not 
          (in r69))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r70
    :parameters ()
    :precondition 
      (and
        (in r70)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r71)
        (f_copy)
        (not 
          (in r70))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r70
    :parameters ()
    :precondition 
      (and
        (in r70)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r69)
        (f_copy)
        (not 
          (in r70))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r71
    :parameters ()
    :precondition 
      (and
        (in r71)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r72)
        (f_copy)
        (not 
          (in r71))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r71
    :parameters ()
    :precondition 
      (and
        (in r71)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r70)
        (f_copy)
        (not 
          (in r71))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r72
    :parameters ()
    :precondition 
      (and
        (in r72)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r73)
        (f_copy)
        (not 
          (in r72))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r72
    :parameters ()
    :precondition 
      (and
        (in r72)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r71)
        (f_copy)
        (not 
          (in r72))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r73
    :parameters ()
    :precondition 
      (and
        (in r73)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r74)
        (f_copy)
        (not 
          (in r73))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r73
    :parameters ()
    :precondition 
      (and
        (in r73)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r72)
        (f_copy)
        (not 
          (in r73))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r74
    :parameters ()
    :precondition 
      (and
        (in r74)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r75)
        (f_copy)
        (not 
          (in r74))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r74
    :parameters ()
    :precondition 
      (and
        (in r74)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r73)
        (f_copy)
        (not 
          (in r74))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r75
    :parameters ()
    :precondition 
      (and
        (in r75)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r76)
        (f_copy)
        (not 
          (in r75))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r75
    :parameters ()
    :precondition 
      (and
        (in r75)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r74)
        (f_copy)
        (not 
          (in r75))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r76
    :parameters ()
    :precondition 
      (and
        (in r76)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r77)
        (f_copy)
        (not 
          (in r76))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r76
    :parameters ()
    :precondition 
      (and
        (in r76)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r75)
        (f_copy)
        (not 
          (in r76))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r77
    :parameters ()
    :precondition 
      (and
        (in r77)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r78)
        (f_copy)
        (not 
          (in r77))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r77
    :parameters ()
    :precondition 
      (and
        (in r77)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r76)
        (f_copy)
        (not 
          (in r77))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r78
    :parameters ()
    :precondition 
      (and
        (in r78)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r79)
        (f_copy)
        (not 
          (in r78))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r78
    :parameters ()
    :precondition 
      (and
        (in r78)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r77)
        (f_copy)
        (not 
          (in r78))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r79
    :parameters ()
    :precondition 
      (and
        (in r79)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r80)
        (f_copy)
        (not 
          (in r79))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r79
    :parameters ()
    :precondition 
      (and
        (in r79)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r78)
        (f_copy)
        (not 
          (in r79))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r80
    :parameters ()
    :precondition 
      (and
        (in r80)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r81)
        (f_copy)
        (not 
          (in r80))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r80
    :parameters ()
    :precondition 
      (and
        (in r80)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r79)
        (f_copy)
        (not 
          (in r80))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r81
    :parameters ()
    :precondition 
      (and
        (in r81)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r82)
        (f_copy)
        (not 
          (in r81))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r81
    :parameters ()
    :precondition 
      (and
        (in r81)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r80)
        (f_copy)
        (not 
          (in r81))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r82
    :parameters ()
    :precondition 
      (and
        (in r82)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r83)
        (f_copy)
        (not 
          (in r82))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r82
    :parameters ()
    :precondition 
      (and
        (in r82)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r81)
        (f_copy)
        (not 
          (in r82))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r83
    :parameters ()
    :precondition 
      (and
        (in r83)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r84)
        (f_copy)
        (not 
          (in r83))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r83
    :parameters ()
    :precondition 
      (and
        (in r83)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r82)
        (f_copy)
        (not 
          (in r83))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r84
    :parameters ()
    :precondition 
      (and
        (in r84)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r85)
        (f_copy)
        (not 
          (in r84))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r84
    :parameters ()
    :precondition 
      (and
        (in r84)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r83)
        (f_copy)
        (not 
          (in r84))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r85
    :parameters ()
    :precondition 
      (and
        (in r85)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r86)
        (f_copy)
        (not 
          (in r85))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r85
    :parameters ()
    :precondition 
      (and
        (in r85)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r84)
        (f_copy)
        (not 
          (in r85))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r86
    :parameters ()
    :precondition 
      (and
        (in r86)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r87)
        (f_copy)
        (not 
          (in r86))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r86
    :parameters ()
    :precondition 
      (and
        (in r86)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r85)
        (f_copy)
        (not 
          (in r86))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r87
    :parameters ()
    :precondition 
      (and
        (in r87)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r88)
        (f_copy)
        (not 
          (in r87))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r87
    :parameters ()
    :precondition 
      (and
        (in r87)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r86)
        (f_copy)
        (not 
          (in r87))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r88
    :parameters ()
    :precondition 
      (and
        (in r88)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r89)
        (f_copy)
        (not 
          (in r88))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r88
    :parameters ()
    :precondition 
      (and
        (in r88)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r87)
        (f_copy)
        (not 
          (in r88))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r89
    :parameters ()
    :precondition 
      (and
        (in r89)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r90)
        (f_copy)
        (not 
          (in r89))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r89
    :parameters ()
    :precondition 
      (and
        (in r89)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r88)
        (f_copy)
        (not 
          (in r89))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r90
    :parameters ()
    :precondition 
      (and
        (in r90)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r91)
        (f_copy)
        (not 
          (in r90))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r90
    :parameters ()
    :precondition 
      (and
        (in r90)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r89)
        (f_copy)
        (not 
          (in r90))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r91
    :parameters ()
    :precondition 
      (and
        (in r91)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r92)
        (f_copy)
        (not 
          (in r91))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r91
    :parameters ()
    :precondition 
      (and
        (in r91)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r90)
        (f_copy)
        (not 
          (in r91))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r92
    :parameters ()
    :precondition 
      (and
        (in r92)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r93)
        (f_copy)
        (not 
          (in r92))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r92
    :parameters ()
    :precondition 
      (and
        (in r92)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r91)
        (f_copy)
        (not 
          (in r92))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r93
    :parameters ()
    :precondition 
      (and
        (in r93)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r94)
        (f_copy)
        (not 
          (in r93))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r93
    :parameters ()
    :precondition 
      (and
        (in r93)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r92)
        (f_copy)
        (not 
          (in r93))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r94
    :parameters ()
    :precondition 
      (and
        (in r94)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r95)
        (f_copy)
        (not 
          (in r94))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r94
    :parameters ()
    :precondition 
      (and
        (in r94)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r93)
        (f_copy)
        (not 
          (in r94))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r95
    :parameters ()
    :precondition 
      (and
        (in r95)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r96)
        (f_copy)
        (not 
          (in r95))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r95
    :parameters ()
    :precondition 
      (and
        (in r95)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r94)
        (f_copy)
        (not 
          (in r95))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r96
    :parameters ()
    :precondition 
      (and
        (in r96)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r97)
        (f_copy)
        (not 
          (in r96))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r96
    :parameters ()
    :precondition 
      (and
        (in r96)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r95)
        (f_copy)
        (not 
          (in r96))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r97
    :parameters ()
    :precondition 
      (and
        (in r97)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r98)
        (f_copy)
        (not 
          (in r97))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r97
    :parameters ()
    :precondition 
      (and
        (in r97)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r96)
        (f_copy)
        (not 
          (in r97))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r98
    :parameters ()
    :precondition 
      (and
        (in r98)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r99)
        (f_copy)
        (not 
          (in r98))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r98
    :parameters ()
    :precondition 
      (and
        (in r98)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r97)
        (f_copy)
        (not 
          (in r98))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r99
    :parameters ()
    :precondition 
      (and
        (in r99)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r100)
        (f_copy)
        (not 
          (in r99))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r99
    :parameters ()
    :precondition 
      (and
        (in r99)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r98)
        (f_copy)
        (not 
          (in r99))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r101)
        (f_copy)
        (not 
          (in r100))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r99)
        (f_copy)
        (not 
          (in r100))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r101
    :parameters ()
    :precondition 
      (and
        (in r101)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r102)
        (f_copy)
        (not 
          (in r101))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r101
    :parameters ()
    :precondition 
      (and
        (in r101)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r100)
        (f_copy)
        (not 
          (in r101))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r102
    :parameters ()
    :precondition 
      (and
        (in r102)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r103)
        (f_copy)
        (not 
          (in r102))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r102
    :parameters ()
    :precondition 
      (and
        (in r102)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r101)
        (f_copy)
        (not 
          (in r102))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r103
    :parameters ()
    :precondition 
      (and
        (in r103)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r104)
        (f_copy)
        (not 
          (in r103))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r103
    :parameters ()
    :precondition 
      (and
        (in r103)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r102)
        (f_copy)
        (not 
          (in r103))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r104
    :parameters ()
    :precondition 
      (and
        (in r104)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r105)
        (f_copy)
        (not 
          (in r104))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r104
    :parameters ()
    :precondition 
      (and
        (in r104)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r103)
        (f_copy)
        (not 
          (in r104))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r105
    :parameters ()
    :precondition 
      (and
        (in r105)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r106)
        (f_copy)
        (not 
          (in r105))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r105
    :parameters ()
    :precondition 
      (and
        (in r105)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r104)
        (f_copy)
        (not 
          (in r105))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r106
    :parameters ()
    :precondition 
      (and
        (in r106)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r107)
        (f_copy)
        (not 
          (in r106))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r106
    :parameters ()
    :precondition 
      (and
        (in r106)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r105)
        (f_copy)
        (not 
          (in r106))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r107
    :parameters ()
    :precondition 
      (and
        (in r107)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r108)
        (f_copy)
        (not 
          (in r107))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r107
    :parameters ()
    :precondition 
      (and
        (in r107)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r106)
        (f_copy)
        (not 
          (in r107))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r108
    :parameters ()
    :precondition 
      (and
        (in r108)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r109)
        (f_copy)
        (not 
          (in r108))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r108
    :parameters ()
    :precondition 
      (and
        (in r108)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r107)
        (f_copy)
        (not 
          (in r108))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r109
    :parameters ()
    :precondition 
      (and
        (in r109)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r110)
        (f_copy)
        (not 
          (in r109))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r109
    :parameters ()
    :precondition 
      (and
        (in r109)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r108)
        (f_copy)
        (not 
          (in r109))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r110
    :parameters ()
    :precondition 
      (and
        (in r110)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r111)
        (f_copy)
        (not 
          (in r110))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r110
    :parameters ()
    :precondition 
      (and
        (in r110)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r109)
        (f_copy)
        (not 
          (in r110))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r111
    :parameters ()
    :precondition 
      (and
        (in r111)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r112)
        (f_copy)
        (not 
          (in r111))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r111
    :parameters ()
    :precondition 
      (and
        (in r111)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r110)
        (f_copy)
        (not 
          (in r111))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r112
    :parameters ()
    :precondition 
      (and
        (in r112)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r113)
        (f_copy)
        (not 
          (in r112))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r112
    :parameters ()
    :precondition 
      (and
        (in r112)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r111)
        (f_copy)
        (not 
          (in r112))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r113
    :parameters ()
    :precondition 
      (and
        (in r113)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r114)
        (f_copy)
        (not 
          (in r113))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r113
    :parameters ()
    :precondition 
      (and
        (in r113)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r112)
        (f_copy)
        (not 
          (in r113))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r114
    :parameters ()
    :precondition 
      (and
        (in r114)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r115)
        (f_copy)
        (not 
          (in r114))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r114
    :parameters ()
    :precondition 
      (and
        (in r114)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r113)
        (f_copy)
        (not 
          (in r114))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r115
    :parameters ()
    :precondition 
      (and
        (in r115)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r116)
        (f_copy)
        (not 
          (in r115))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r115
    :parameters ()
    :precondition 
      (and
        (in r115)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r114)
        (f_copy)
        (not 
          (in r115))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r116
    :parameters ()
    :precondition 
      (and
        (in r116)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r117)
        (f_copy)
        (not 
          (in r116))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r116
    :parameters ()
    :precondition 
      (and
        (in r116)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r115)
        (f_copy)
        (not 
          (in r116))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r117
    :parameters ()
    :precondition 
      (and
        (in r117)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r118)
        (f_copy)
        (not 
          (in r117))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r117
    :parameters ()
    :precondition 
      (and
        (in r117)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r116)
        (f_copy)
        (not 
          (in r117))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r118
    :parameters ()
    :precondition 
      (and
        (in r118)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r119)
        (f_copy)
        (not 
          (in r118))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r118
    :parameters ()
    :precondition 
      (and
        (in r118)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r117)
        (f_copy)
        (not 
          (in r118))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r119
    :parameters ()
    :precondition 
      (and
        (in r119)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r120)
        (f_copy)
        (not 
          (in r119))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r119
    :parameters ()
    :precondition 
      (and
        (in r119)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r118)
        (f_copy)
        (not 
          (in r119))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r120
    :parameters ()
    :precondition 
      (and
        (in r120)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r121)
        (f_copy)
        (not 
          (in r120))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r120
    :parameters ()
    :precondition 
      (and
        (in r120)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r119)
        (f_copy)
        (not 
          (in r120))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r121
    :parameters ()
    :precondition 
      (and
        (in r121)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r122)
        (f_copy)
        (not 
          (in r121))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r121
    :parameters ()
    :precondition 
      (and
        (in r121)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r120)
        (f_copy)
        (not 
          (in r121))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r122
    :parameters ()
    :precondition 
      (and
        (in r122)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r123)
        (f_copy)
        (not 
          (in r122))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r122
    :parameters ()
    :precondition 
      (and
        (in r122)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r121)
        (f_copy)
        (not 
          (in r122))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r123
    :parameters ()
    :precondition 
      (and
        (in r123)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r124)
        (f_copy)
        (not 
          (in r123))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r123
    :parameters ()
    :precondition 
      (and
        (in r123)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r122)
        (f_copy)
        (not 
          (in r123))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r124
    :parameters ()
    :precondition 
      (and
        (in r124)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r125)
        (f_copy)
        (not 
          (in r124))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r124
    :parameters ()
    :precondition 
      (and
        (in r124)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r123)
        (f_copy)
        (not 
          (in r124))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r125
    :parameters ()
    :precondition 
      (and
        (in r125)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r126)
        (f_copy)
        (not 
          (in r125))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r125
    :parameters ()
    :precondition 
      (and
        (in r125)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r124)
        (f_copy)
        (not 
          (in r125))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r126
    :parameters ()
    :precondition 
      (and
        (in r126)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r127)
        (f_copy)
        (not 
          (in r126))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r126
    :parameters ()
    :precondition 
      (and
        (in r126)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r125)
        (f_copy)
        (not 
          (in r126))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r127
    :parameters ()
    :precondition 
      (and
        (in r127)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r128)
        (f_copy)
        (not 
          (in r127))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r127
    :parameters ()
    :precondition 
      (and
        (in r127)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r126)
        (f_copy)
        (not 
          (in r127))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r128
    :parameters ()
    :precondition 
      (and
        (in r128)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r129)
        (f_copy)
        (not 
          (in r128))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r128
    :parameters ()
    :precondition 
      (and
        (in r128)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r127)
        (f_copy)
        (not 
          (in r128))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r129
    :parameters ()
    :precondition 
      (and
        (in r129)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r130)
        (f_copy)
        (not 
          (in r129))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r129
    :parameters ()
    :precondition 
      (and
        (in r129)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r128)
        (f_copy)
        (not 
          (in r129))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r130
    :parameters ()
    :precondition 
      (and
        (in r130)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r131)
        (f_copy)
        (not 
          (in r130))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r130
    :parameters ()
    :precondition 
      (and
        (in r130)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r129)
        (f_copy)
        (not 
          (in r130))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r131
    :parameters ()
    :precondition 
      (and
        (in r131)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r132)
        (f_copy)
        (not 
          (in r131))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r131
    :parameters ()
    :precondition 
      (and
        (in r131)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r130)
        (f_copy)
        (not 
          (in r131))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r132
    :parameters ()
    :precondition 
      (and
        (in r132)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r133)
        (f_copy)
        (not 
          (in r132))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r132
    :parameters ()
    :precondition 
      (and
        (in r132)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r131)
        (f_copy)
        (not 
          (in r132))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r133
    :parameters ()
    :precondition 
      (and
        (in r133)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r134)
        (f_copy)
        (not 
          (in r133))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r133
    :parameters ()
    :precondition 
      (and
        (in r133)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r132)
        (f_copy)
        (not 
          (in r133))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r134
    :parameters ()
    :precondition 
      (and
        (in r134)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r135)
        (f_copy)
        (not 
          (in r134))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r134
    :parameters ()
    :precondition 
      (and
        (in r134)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r133)
        (f_copy)
        (not 
          (in r134))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r135
    :parameters ()
    :precondition 
      (and
        (in r135)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r136)
        (f_copy)
        (not 
          (in r135))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r135
    :parameters ()
    :precondition 
      (and
        (in r135)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r134)
        (f_copy)
        (not 
          (in r135))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r136
    :parameters ()
    :precondition 
      (and
        (in r136)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r137)
        (f_copy)
        (not 
          (in r136))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r136
    :parameters ()
    :precondition 
      (and
        (in r136)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r135)
        (f_copy)
        (not 
          (in r136))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r137
    :parameters ()
    :precondition 
      (and
        (in r137)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r138)
        (f_copy)
        (not 
          (in r137))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r137
    :parameters ()
    :precondition 
      (and
        (in r137)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r136)
        (f_copy)
        (not 
          (in r137))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r138
    :parameters ()
    :precondition 
      (and
        (in r138)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r139)
        (f_copy)
        (not 
          (in r138))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r138
    :parameters ()
    :precondition 
      (and
        (in r138)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r137)
        (f_copy)
        (not 
          (in r138))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r139
    :parameters ()
    :precondition 
      (and
        (in r139)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r140)
        (f_copy)
        (not 
          (in r139))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r139
    :parameters ()
    :precondition 
      (and
        (in r139)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r138)
        (f_copy)
        (not 
          (in r139))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r140
    :parameters ()
    :precondition 
      (and
        (in r140)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r141)
        (f_copy)
        (not 
          (in r140))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r140
    :parameters ()
    :precondition 
      (and
        (in r140)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r139)
        (f_copy)
        (not 
          (in r140))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r141
    :parameters ()
    :precondition 
      (and
        (in r141)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r142)
        (f_copy)
        (not 
          (in r141))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r141
    :parameters ()
    :precondition 
      (and
        (in r141)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r140)
        (f_copy)
        (not 
          (in r141))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r142
    :parameters ()
    :precondition 
      (and
        (in r142)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r143)
        (f_copy)
        (not 
          (in r142))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r142
    :parameters ()
    :precondition 
      (and
        (in r142)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r141)
        (f_copy)
        (not 
          (in r142))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r143
    :parameters ()
    :precondition 
      (and
        (in r143)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r144)
        (f_copy)
        (not 
          (in r143))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r143
    :parameters ()
    :precondition 
      (and
        (in r143)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r142)
        (f_copy)
        (not 
          (in r143))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r144
    :parameters ()
    :precondition 
      (and
        (in r144)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r145)
        (f_copy)
        (not 
          (in r144))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r144
    :parameters ()
    :precondition 
      (and
        (in r144)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r143)
        (f_copy)
        (not 
          (in r144))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r145
    :parameters ()
    :precondition 
      (and
        (in r145)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r146)
        (f_copy)
        (not 
          (in r145))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r145
    :parameters ()
    :precondition 
      (and
        (in r145)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r144)
        (f_copy)
        (not 
          (in r145))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r146
    :parameters ()
    :precondition 
      (and
        (in r146)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r147)
        (f_copy)
        (not 
          (in r146))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r146
    :parameters ()
    :precondition 
      (and
        (in r146)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r145)
        (f_copy)
        (not 
          (in r146))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r147
    :parameters ()
    :precondition 
      (and
        (in r147)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r148)
        (f_copy)
        (not 
          (in r147))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r147
    :parameters ()
    :precondition 
      (and
        (in r147)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r146)
        (f_copy)
        (not 
          (in r147))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r148
    :parameters ()
    :precondition 
      (and
        (in r148)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r149)
        (f_copy)
        (not 
          (in r148))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r148
    :parameters ()
    :precondition 
      (and
        (in r148)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r147)
        (f_copy)
        (not 
          (in r148))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r149
    :parameters ()
    :precondition 
      (and
        (in r149)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r150)
        (f_copy)
        (not 
          (in r149))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r149
    :parameters ()
    :precondition 
      (and
        (in r149)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r148)
        (f_copy)
        (not 
          (in r149))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r150
    :parameters ()
    :precondition 
      (and
        (in r150)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r151)
        (f_copy)
        (not 
          (in r150))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r150
    :parameters ()
    :precondition 
      (and
        (in r150)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r149)
        (f_copy)
        (not 
          (in r150))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r151
    :parameters ()
    :precondition 
      (and
        (in r151)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r152)
        (f_copy)
        (not 
          (in r151))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r151
    :parameters ()
    :precondition 
      (and
        (in r151)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r150)
        (f_copy)
        (not 
          (in r151))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r152
    :parameters ()
    :precondition 
      (and
        (in r152)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r153)
        (f_copy)
        (not 
          (in r152))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r152
    :parameters ()
    :precondition 
      (and
        (in r152)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r151)
        (f_copy)
        (not 
          (in r152))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r153
    :parameters ()
    :precondition 
      (and
        (in r153)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r154)
        (f_copy)
        (not 
          (in r153))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r153
    :parameters ()
    :precondition 
      (and
        (in r153)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r152)
        (f_copy)
        (not 
          (in r153))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r154
    :parameters ()
    :precondition 
      (and
        (in r154)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r155)
        (f_copy)
        (not 
          (in r154))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r154
    :parameters ()
    :precondition 
      (and
        (in r154)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r153)
        (f_copy)
        (not 
          (in r154))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r155
    :parameters ()
    :precondition 
      (and
        (in r155)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r156)
        (f_copy)
        (not 
          (in r155))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r155
    :parameters ()
    :precondition 
      (and
        (in r155)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r154)
        (f_copy)
        (not 
          (in r155))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r156
    :parameters ()
    :precondition 
      (and
        (in r156)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r157)
        (f_copy)
        (not 
          (in r156))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r156
    :parameters ()
    :precondition 
      (and
        (in r156)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r155)
        (f_copy)
        (not 
          (in r156))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r157
    :parameters ()
    :precondition 
      (and
        (in r157)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r158)
        (f_copy)
        (not 
          (in r157))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r157
    :parameters ()
    :precondition 
      (and
        (in r157)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r156)
        (f_copy)
        (not 
          (in r157))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r158
    :parameters ()
    :precondition 
      (and
        (in r158)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r159)
        (f_copy)
        (not 
          (in r158))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r158
    :parameters ()
    :precondition 
      (and
        (in r158)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r157)
        (f_copy)
        (not 
          (in r158))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r159
    :parameters ()
    :precondition 
      (and
        (in r159)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r160)
        (f_copy)
        (not 
          (in r159))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r159
    :parameters ()
    :precondition 
      (and
        (in r159)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r158)
        (f_copy)
        (not 
          (in r159))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r160
    :parameters ()
    :precondition 
      (and
        (in r160)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r161)
        (f_copy)
        (not 
          (in r160))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r160
    :parameters ()
    :precondition 
      (and
        (in r160)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r159)
        (f_copy)
        (not 
          (in r160))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r161
    :parameters ()
    :precondition 
      (and
        (in r161)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r162)
        (f_copy)
        (not 
          (in r161))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r161
    :parameters ()
    :precondition 
      (and
        (in r161)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r160)
        (f_copy)
        (not 
          (in r161))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r162
    :parameters ()
    :precondition 
      (and
        (in r162)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r163)
        (f_copy)
        (not 
          (in r162))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r162
    :parameters ()
    :precondition 
      (and
        (in r162)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r161)
        (f_copy)
        (not 
          (in r162))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r163
    :parameters ()
    :precondition 
      (and
        (in r163)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r164)
        (f_copy)
        (not 
          (in r163))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r163
    :parameters ()
    :precondition 
      (and
        (in r163)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r162)
        (f_copy)
        (not 
          (in r163))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r164
    :parameters ()
    :precondition 
      (and
        (in r164)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r165)
        (f_copy)
        (not 
          (in r164))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r164
    :parameters ()
    :precondition 
      (and
        (in r164)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r163)
        (f_copy)
        (not 
          (in r164))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r165
    :parameters ()
    :precondition 
      (and
        (in r165)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r166)
        (f_copy)
        (not 
          (in r165))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r165
    :parameters ()
    :precondition 
      (and
        (in r165)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r164)
        (f_copy)
        (not 
          (in r165))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r166
    :parameters ()
    :precondition 
      (and
        (in r166)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r167)
        (f_copy)
        (not 
          (in r166))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r166
    :parameters ()
    :precondition 
      (and
        (in r166)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r165)
        (f_copy)
        (not 
          (in r166))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r167
    :parameters ()
    :precondition 
      (and
        (in r167)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r168)
        (f_copy)
        (not 
          (in r167))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r167
    :parameters ()
    :precondition 
      (and
        (in r167)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r166)
        (f_copy)
        (not 
          (in r167))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r168
    :parameters ()
    :precondition 
      (and
        (in r168)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r169)
        (f_copy)
        (not 
          (in r168))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r168
    :parameters ()
    :precondition 
      (and
        (in r168)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r167)
        (f_copy)
        (not 
          (in r168))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r169
    :parameters ()
    :precondition 
      (and
        (in r169)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r170)
        (f_copy)
        (not 
          (in r169))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r169
    :parameters ()
    :precondition 
      (and
        (in r169)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r168)
        (f_copy)
        (not 
          (in r169))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r170
    :parameters ()
    :precondition 
      (and
        (in r170)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r171)
        (f_copy)
        (not 
          (in r170))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r170
    :parameters ()
    :precondition 
      (and
        (in r170)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r169)
        (f_copy)
        (not 
          (in r170))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r171
    :parameters ()
    :precondition 
      (and
        (in r171)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r172)
        (f_copy)
        (not 
          (in r171))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r171
    :parameters ()
    :precondition 
      (and
        (in r171)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r170)
        (f_copy)
        (not 
          (in r171))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r172
    :parameters ()
    :precondition 
      (and
        (in r172)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r173)
        (f_copy)
        (not 
          (in r172))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r172
    :parameters ()
    :precondition 
      (and
        (in r172)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r171)
        (f_copy)
        (not 
          (in r172))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r173
    :parameters ()
    :precondition 
      (and
        (in r173)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r174)
        (f_copy)
        (not 
          (in r173))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r173
    :parameters ()
    :precondition 
      (and
        (in r173)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r172)
        (f_copy)
        (not 
          (in r173))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r174
    :parameters ()
    :precondition 
      (and
        (in r174)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r175)
        (f_copy)
        (not 
          (in r174))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r174
    :parameters ()
    :precondition 
      (and
        (in r174)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r173)
        (f_copy)
        (not 
          (in r174))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r175
    :parameters ()
    :precondition 
      (and
        (in r175)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r176)
        (f_copy)
        (not 
          (in r175))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r175
    :parameters ()
    :precondition 
      (and
        (in r175)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r174)
        (f_copy)
        (not 
          (in r175))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r176
    :parameters ()
    :precondition 
      (and
        (in r176)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r177)
        (f_copy)
        (not 
          (in r176))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r176
    :parameters ()
    :precondition 
      (and
        (in r176)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r175)
        (f_copy)
        (not 
          (in r176))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r177
    :parameters ()
    :precondition 
      (and
        (in r177)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r178)
        (f_copy)
        (not 
          (in r177))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r177
    :parameters ()
    :precondition 
      (and
        (in r177)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r176)
        (f_copy)
        (not 
          (in r177))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r178
    :parameters ()
    :precondition 
      (and
        (in r178)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r179)
        (f_copy)
        (not 
          (in r178))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r178
    :parameters ()
    :precondition 
      (and
        (in r178)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r177)
        (f_copy)
        (not 
          (in r178))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r179
    :parameters ()
    :precondition 
      (and
        (in r179)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r180)
        (f_copy)
        (not 
          (in r179))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r179
    :parameters ()
    :precondition 
      (and
        (in r179)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r178)
        (f_copy)
        (not 
          (in r179))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r180
    :parameters ()
    :precondition 
      (and
        (in r180)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r181)
        (f_copy)
        (not 
          (in r180))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r180
    :parameters ()
    :precondition 
      (and
        (in r180)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r179)
        (f_copy)
        (not 
          (in r180))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r181
    :parameters ()
    :precondition 
      (and
        (in r181)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r182)
        (f_copy)
        (not 
          (in r181))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r181
    :parameters ()
    :precondition 
      (and
        (in r181)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r180)
        (f_copy)
        (not 
          (in r181))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r182
    :parameters ()
    :precondition 
      (and
        (in r182)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r183)
        (f_copy)
        (not 
          (in r182))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r182
    :parameters ()
    :precondition 
      (and
        (in r182)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r181)
        (f_copy)
        (not 
          (in r182))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r183
    :parameters ()
    :precondition 
      (and
        (in r183)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r184)
        (f_copy)
        (not 
          (in r183))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r183
    :parameters ()
    :precondition 
      (and
        (in r183)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r182)
        (f_copy)
        (not 
          (in r183))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r184
    :parameters ()
    :precondition 
      (and
        (in r184)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r185)
        (f_copy)
        (not 
          (in r184))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r184
    :parameters ()
    :precondition 
      (and
        (in r184)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r183)
        (f_copy)
        (not 
          (in r184))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r185
    :parameters ()
    :precondition 
      (and
        (in r185)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r186)
        (f_copy)
        (not 
          (in r185))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r185
    :parameters ()
    :precondition 
      (and
        (in r185)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r184)
        (f_copy)
        (not 
          (in r185))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r186
    :parameters ()
    :precondition 
      (and
        (in r186)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r187)
        (f_copy)
        (not 
          (in r186))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r186
    :parameters ()
    :precondition 
      (and
        (in r186)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r185)
        (f_copy)
        (not 
          (in r186))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r187
    :parameters ()
    :precondition 
      (and
        (in r187)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r188)
        (f_copy)
        (not 
          (in r187))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r187
    :parameters ()
    :precondition 
      (and
        (in r187)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r186)
        (f_copy)
        (not 
          (in r187))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r188
    :parameters ()
    :precondition 
      (and
        (in r188)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r189)
        (f_copy)
        (not 
          (in r188))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r188
    :parameters ()
    :precondition 
      (and
        (in r188)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r187)
        (f_copy)
        (not 
          (in r188))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r189
    :parameters ()
    :precondition 
      (and
        (in r189)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r190)
        (f_copy)
        (not 
          (in r189))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r189
    :parameters ()
    :precondition 
      (and
        (in r189)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r188)
        (f_copy)
        (not 
          (in r189))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r190
    :parameters ()
    :precondition 
      (and
        (in r190)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r191)
        (f_copy)
        (not 
          (in r190))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r190
    :parameters ()
    :precondition 
      (and
        (in r190)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r189)
        (f_copy)
        (not 
          (in r190))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r191
    :parameters ()
    :precondition 
      (and
        (in r191)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r192)
        (f_copy)
        (not 
          (in r191))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r191
    :parameters ()
    :precondition 
      (and
        (in r191)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r190)
        (f_copy)
        (not 
          (in r191))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r192
    :parameters ()
    :precondition 
      (and
        (in r192)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r193)
        (f_copy)
        (not 
          (in r192))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r192
    :parameters ()
    :precondition 
      (and
        (in r192)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r191)
        (f_copy)
        (not 
          (in r192))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r193
    :parameters ()
    :precondition 
      (and
        (in r193)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r194)
        (f_copy)
        (not 
          (in r193))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r193
    :parameters ()
    :precondition 
      (and
        (in r193)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r192)
        (f_copy)
        (not 
          (in r193))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r194
    :parameters ()
    :precondition 
      (and
        (in r194)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r195)
        (f_copy)
        (not 
          (in r194))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r194
    :parameters ()
    :precondition 
      (and
        (in r194)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r193)
        (f_copy)
        (not 
          (in r194))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r195
    :parameters ()
    :precondition 
      (and
        (in r195)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r196)
        (f_copy)
        (not 
          (in r195))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r195
    :parameters ()
    :precondition 
      (and
        (in r195)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r194)
        (f_copy)
        (not 
          (in r195))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r196
    :parameters ()
    :precondition 
      (and
        (in r196)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r197)
        (f_copy)
        (not 
          (in r196))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r196
    :parameters ()
    :precondition 
      (and
        (in r196)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r195)
        (f_copy)
        (not 
          (in r196))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r197
    :parameters ()
    :precondition 
      (and
        (in r197)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r198)
        (f_copy)
        (not 
          (in r197))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r197
    :parameters ()
    :precondition 
      (and
        (in r197)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r196)
        (f_copy)
        (not 
          (in r197))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r198
    :parameters ()
    :precondition 
      (and
        (in r198)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r199)
        (f_copy)
        (not 
          (in r198))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r198
    :parameters ()
    :precondition 
      (and
        (in r198)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r197)
        (f_copy)
        (not 
          (in r198))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r199
    :parameters ()
    :precondition 
      (and
        (in r199)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r200)
        (f_copy)
        (not 
          (in r199))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r199
    :parameters ()
    :precondition 
      (and
        (in r199)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r198)
        (f_copy)
        (not 
          (in r199))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r200
    :parameters ()
    :precondition 
      (and
        (in r200)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r201)
        (f_copy)
        (not 
          (in r200))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r200
    :parameters ()
    :precondition 
      (and
        (in r200)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r199)
        (f_copy)
        (not 
          (in r200))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r201
    :parameters ()
    :precondition 
      (and
        (in r201)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r202)
        (f_copy)
        (not 
          (in r201))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r201
    :parameters ()
    :precondition 
      (and
        (in r201)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r200)
        (f_copy)
        (not 
          (in r201))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r202
    :parameters ()
    :precondition 
      (and
        (in r202)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r203)
        (f_copy)
        (not 
          (in r202))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r202
    :parameters ()
    :precondition 
      (and
        (in r202)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r201)
        (f_copy)
        (not 
          (in r202))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r203
    :parameters ()
    :precondition 
      (and
        (in r203)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r204)
        (f_copy)
        (not 
          (in r203))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r203
    :parameters ()
    :precondition 
      (and
        (in r203)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r202)
        (f_copy)
        (not 
          (in r203))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r204
    :parameters ()
    :precondition 
      (and
        (in r204)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r205)
        (f_copy)
        (not 
          (in r204))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r204
    :parameters ()
    :precondition 
      (and
        (in r204)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r203)
        (f_copy)
        (not 
          (in r204))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r205
    :parameters ()
    :precondition 
      (and
        (in r205)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r206)
        (f_copy)
        (not 
          (in r205))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r205
    :parameters ()
    :precondition 
      (and
        (in r205)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r204)
        (f_copy)
        (not 
          (in r205))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r206
    :parameters ()
    :precondition 
      (and
        (in r206)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r207)
        (f_copy)
        (not 
          (in r206))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r206
    :parameters ()
    :precondition 
      (and
        (in r206)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r205)
        (f_copy)
        (not 
          (in r206))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r207
    :parameters ()
    :precondition 
      (and
        (in r207)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r208)
        (f_copy)
        (not 
          (in r207))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r207
    :parameters ()
    :precondition 
      (and
        (in r207)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r206)
        (f_copy)
        (not 
          (in r207))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r208
    :parameters ()
    :precondition 
      (and
        (in r208)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r209)
        (f_copy)
        (not 
          (in r208))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r208
    :parameters ()
    :precondition 
      (and
        (in r208)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r207)
        (f_copy)
        (not 
          (in r208))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r209
    :parameters ()
    :precondition 
      (and
        (in r209)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r210)
        (f_copy)
        (not 
          (in r209))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r209
    :parameters ()
    :precondition 
      (and
        (in r209)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r208)
        (f_copy)
        (not 
          (in r209))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r210
    :parameters ()
    :precondition 
      (and
        (in r210)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r211)
        (f_copy)
        (not 
          (in r210))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r210
    :parameters ()
    :precondition 
      (and
        (in r210)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r209)
        (f_copy)
        (not 
          (in r210))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r211
    :parameters ()
    :precondition 
      (and
        (in r211)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r212)
        (f_copy)
        (not 
          (in r211))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r211
    :parameters ()
    :precondition 
      (and
        (in r211)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r210)
        (f_copy)
        (not 
          (in r211))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r212
    :parameters ()
    :precondition 
      (and
        (in r212)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r213)
        (f_copy)
        (not 
          (in r212))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r212
    :parameters ()
    :precondition 
      (and
        (in r212)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r211)
        (f_copy)
        (not 
          (in r212))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r213
    :parameters ()
    :precondition 
      (and
        (in r213)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r214)
        (f_copy)
        (not 
          (in r213))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r213
    :parameters ()
    :precondition 
      (and
        (in r213)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r212)
        (f_copy)
        (not 
          (in r213))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r214
    :parameters ()
    :precondition 
      (and
        (in r214)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r215)
        (f_copy)
        (not 
          (in r214))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r214
    :parameters ()
    :precondition 
      (and
        (in r214)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r213)
        (f_copy)
        (not 
          (in r214))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r215
    :parameters ()
    :precondition 
      (and
        (in r215)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r216)
        (f_copy)
        (not 
          (in r215))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r215
    :parameters ()
    :precondition 
      (and
        (in r215)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r214)
        (f_copy)
        (not 
          (in r215))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r216
    :parameters ()
    :precondition 
      (and
        (in r216)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r217)
        (f_copy)
        (not 
          (in r216))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r216
    :parameters ()
    :precondition 
      (and
        (in r216)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r215)
        (f_copy)
        (not 
          (in r216))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r217
    :parameters ()
    :precondition 
      (and
        (in r217)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r218)
        (f_copy)
        (not 
          (in r217))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r217
    :parameters ()
    :precondition 
      (and
        (in r217)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r216)
        (f_copy)
        (not 
          (in r217))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r218
    :parameters ()
    :precondition 
      (and
        (in r218)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r219)
        (f_copy)
        (not 
          (in r218))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r218
    :parameters ()
    :precondition 
      (and
        (in r218)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r217)
        (f_copy)
        (not 
          (in r218))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r219
    :parameters ()
    :precondition 
      (and
        (in r219)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r220)
        (f_copy)
        (not 
          (in r219))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r219
    :parameters ()
    :precondition 
      (and
        (in r219)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r218)
        (f_copy)
        (not 
          (in r219))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r220
    :parameters ()
    :precondition 
      (and
        (in r220)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r221)
        (f_copy)
        (not 
          (in r220))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r220
    :parameters ()
    :precondition 
      (and
        (in r220)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r219)
        (f_copy)
        (not 
          (in r220))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r221
    :parameters ()
    :precondition 
      (and
        (in r221)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r222)
        (f_copy)
        (not 
          (in r221))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r221
    :parameters ()
    :precondition 
      (and
        (in r221)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r220)
        (f_copy)
        (not 
          (in r221))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r222
    :parameters ()
    :precondition 
      (and
        (in r222)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r223)
        (f_copy)
        (not 
          (in r222))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r222
    :parameters ()
    :precondition 
      (and
        (in r222)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r221)
        (f_copy)
        (not 
          (in r222))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r223
    :parameters ()
    :precondition 
      (and
        (in r223)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r224)
        (f_copy)
        (not 
          (in r223))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r223
    :parameters ()
    :precondition 
      (and
        (in r223)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r222)
        (f_copy)
        (not 
          (in r223))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r224
    :parameters ()
    :precondition 
      (and
        (in r224)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r225)
        (f_copy)
        (not 
          (in r224))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r224
    :parameters ()
    :precondition 
      (and
        (in r224)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r223)
        (f_copy)
        (not 
          (in r224))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r225
    :parameters ()
    :precondition 
      (and
        (in r225)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r226)
        (f_copy)
        (not 
          (in r225))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r225
    :parameters ()
    :precondition 
      (and
        (in r225)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r224)
        (f_copy)
        (not 
          (in r225))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r226
    :parameters ()
    :precondition 
      (and
        (in r226)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r227)
        (f_copy)
        (not 
          (in r226))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r226
    :parameters ()
    :precondition 
      (and
        (in r226)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r225)
        (f_copy)
        (not 
          (in r226))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r227
    :parameters ()
    :precondition 
      (and
        (in r227)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r228)
        (f_copy)
        (not 
          (in r227))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r227
    :parameters ()
    :precondition 
      (and
        (in r227)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r226)
        (f_copy)
        (not 
          (in r227))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r228
    :parameters ()
    :precondition 
      (and
        (in r228)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r229)
        (f_copy)
        (not 
          (in r228))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r228
    :parameters ()
    :precondition 
      (and
        (in r228)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r227)
        (f_copy)
        (not 
          (in r228))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r229
    :parameters ()
    :precondition 
      (and
        (in r229)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r230)
        (f_copy)
        (not 
          (in r229))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r229
    :parameters ()
    :precondition 
      (and
        (in r229)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r228)
        (f_copy)
        (not 
          (in r229))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r230
    :parameters ()
    :precondition 
      (and
        (in r230)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r231)
        (f_copy)
        (not 
          (in r230))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r230
    :parameters ()
    :precondition 
      (and
        (in r230)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r229)
        (f_copy)
        (not 
          (in r230))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r231
    :parameters ()
    :precondition 
      (and
        (in r231)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r232)
        (f_copy)
        (not 
          (in r231))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r231
    :parameters ()
    :precondition 
      (and
        (in r231)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r230)
        (f_copy)
        (not 
          (in r231))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r232
    :parameters ()
    :precondition 
      (and
        (in r232)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r233)
        (f_copy)
        (not 
          (in r232))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r232
    :parameters ()
    :precondition 
      (and
        (in r232)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r231)
        (f_copy)
        (not 
          (in r232))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r233
    :parameters ()
    :precondition 
      (and
        (in r233)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r234)
        (f_copy)
        (not 
          (in r233))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r233
    :parameters ()
    :precondition 
      (and
        (in r233)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r232)
        (f_copy)
        (not 
          (in r233))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r234
    :parameters ()
    :precondition 
      (and
        (in r234)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r235)
        (f_copy)
        (not 
          (in r234))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r234
    :parameters ()
    :precondition 
      (and
        (in r234)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r233)
        (f_copy)
        (not 
          (in r234))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r235
    :parameters ()
    :precondition 
      (and
        (in r235)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r236)
        (f_copy)
        (not 
          (in r235))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r235
    :parameters ()
    :precondition 
      (and
        (in r235)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r234)
        (f_copy)
        (not 
          (in r235))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r236
    :parameters ()
    :precondition 
      (and
        (in r236)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r237)
        (f_copy)
        (not 
          (in r236))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r236
    :parameters ()
    :precondition 
      (and
        (in r236)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r235)
        (f_copy)
        (not 
          (in r236))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r237
    :parameters ()
    :precondition 
      (and
        (in r237)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r238)
        (f_copy)
        (not 
          (in r237))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r237
    :parameters ()
    :precondition 
      (and
        (in r237)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r236)
        (f_copy)
        (not 
          (in r237))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r238
    :parameters ()
    :precondition 
      (and
        (in r238)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r239)
        (f_copy)
        (not 
          (in r238))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r238
    :parameters ()
    :precondition 
      (and
        (in r238)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r237)
        (f_copy)
        (not 
          (in r238))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r239
    :parameters ()
    :precondition 
      (and
        (in r239)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r240)
        (f_copy)
        (not 
          (in r239))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r239
    :parameters ()
    :precondition 
      (and
        (in r239)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r238)
        (f_copy)
        (not 
          (in r239))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r240
    :parameters ()
    :precondition 
      (and
        (in r240)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r241)
        (f_copy)
        (not 
          (in r240))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r240
    :parameters ()
    :precondition 
      (and
        (in r240)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r239)
        (f_copy)
        (not 
          (in r240))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r241
    :parameters ()
    :precondition 
      (and
        (in r241)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r242)
        (f_copy)
        (not 
          (in r241))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r241
    :parameters ()
    :precondition 
      (and
        (in r241)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r240)
        (f_copy)
        (not 
          (in r241))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r242
    :parameters ()
    :precondition 
      (and
        (in r242)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r243)
        (f_copy)
        (not 
          (in r242))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r242
    :parameters ()
    :precondition 
      (and
        (in r242)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r241)
        (f_copy)
        (not 
          (in r242))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r243
    :parameters ()
    :precondition 
      (and
        (in r243)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r244)
        (f_copy)
        (not 
          (in r243))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r243
    :parameters ()
    :precondition 
      (and
        (in r243)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r242)
        (f_copy)
        (not 
          (in r243))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r244
    :parameters ()
    :precondition 
      (and
        (in r244)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r245)
        (f_copy)
        (not 
          (in r244))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r244
    :parameters ()
    :precondition 
      (and
        (in r244)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r243)
        (f_copy)
        (not 
          (in r244))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r245
    :parameters ()
    :precondition 
      (and
        (in r245)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r246)
        (f_copy)
        (not 
          (in r245))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r245
    :parameters ()
    :precondition 
      (and
        (in r245)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r244)
        (f_copy)
        (not 
          (in r245))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r246
    :parameters ()
    :precondition 
      (and
        (in r246)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r247)
        (f_copy)
        (not 
          (in r246))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r246
    :parameters ()
    :precondition 
      (and
        (in r246)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r245)
        (f_copy)
        (not 
          (in r246))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r247
    :parameters ()
    :precondition 
      (and
        (in r247)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r248)
        (f_copy)
        (not 
          (in r247))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r247
    :parameters ()
    :precondition 
      (and
        (in r247)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r246)
        (f_copy)
        (not 
          (in r247))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r248
    :parameters ()
    :precondition 
      (and
        (in r248)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r249)
        (f_copy)
        (not 
          (in r248))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r248
    :parameters ()
    :precondition 
      (and
        (in r248)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r247)
        (f_copy)
        (not 
          (in r248))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r249
    :parameters ()
    :precondition 
      (and
        (in r249)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r250)
        (f_copy)
        (not 
          (in r249))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r249
    :parameters ()
    :precondition 
      (and
        (in r249)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r248)
        (f_copy)
        (not 
          (in r249))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r250
    :parameters ()
    :precondition 
      (and
        (in r250)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r251)
        (f_copy)
        (not 
          (in r250))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r250
    :parameters ()
    :precondition 
      (and
        (in r250)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r249)
        (f_copy)
        (not 
          (in r250))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r251
    :parameters ()
    :precondition 
      (and
        (in r251)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r252)
        (f_copy)
        (not 
          (in r251))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r251
    :parameters ()
    :precondition 
      (and
        (in r251)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r250)
        (f_copy)
        (not 
          (in r251))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r252
    :parameters ()
    :precondition 
      (and
        (in r252)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r253)
        (f_copy)
        (not 
          (in r252))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r252
    :parameters ()
    :precondition 
      (and
        (in r252)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r251)
        (f_copy)
        (not 
          (in r252))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r253
    :parameters ()
    :precondition 
      (and
        (in r253)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r254)
        (f_copy)
        (not 
          (in r253))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r253
    :parameters ()
    :precondition 
      (and
        (in r253)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r252)
        (f_copy)
        (not 
          (in r253))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r254
    :parameters ()
    :precondition 
      (and
        (in r254)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r255)
        (f_copy)
        (not 
          (in r254))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r254
    :parameters ()
    :precondition 
      (and
        (in r254)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r253)
        (f_copy)
        (not 
          (in r254))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r255
    :parameters ()
    :precondition 
      (and
        (in r255)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r256)
        (f_copy)
        (not 
          (in r255))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r255
    :parameters ()
    :precondition 
      (and
        (in r255)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r254)
        (f_copy)
        (not 
          (in r255))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r256
    :parameters ()
    :precondition 
      (and
        (in r256)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r257)
        (f_copy)
        (not 
          (in r256))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r256
    :parameters ()
    :precondition 
      (and
        (in r256)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r255)
        (f_copy)
        (not 
          (in r256))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r257
    :parameters ()
    :precondition 
      (and
        (in r257)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r258)
        (f_copy)
        (not 
          (in r257))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r257
    :parameters ()
    :precondition 
      (and
        (in r257)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r256)
        (f_copy)
        (not 
          (in r257))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r258
    :parameters ()
    :precondition 
      (and
        (in r258)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r259)
        (f_copy)
        (not 
          (in r258))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r258
    :parameters ()
    :precondition 
      (and
        (in r258)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r257)
        (f_copy)
        (not 
          (in r258))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r259
    :parameters ()
    :precondition 
      (and
        (in r259)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r260)
        (f_copy)
        (not 
          (in r259))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r259
    :parameters ()
    :precondition 
      (and
        (in r259)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r258)
        (f_copy)
        (not 
          (in r259))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r260
    :parameters ()
    :precondition 
      (and
        (in r260)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r261)
        (f_copy)
        (not 
          (in r260))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r260
    :parameters ()
    :precondition 
      (and
        (in r260)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r259)
        (f_copy)
        (not 
          (in r260))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r261
    :parameters ()
    :precondition 
      (and
        (in r261)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r262)
        (f_copy)
        (not 
          (in r261))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r261
    :parameters ()
    :precondition 
      (and
        (in r261)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r260)
        (f_copy)
        (not 
          (in r261))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r262
    :parameters ()
    :precondition 
      (and
        (in r262)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r263)
        (f_copy)
        (not 
          (in r262))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r262
    :parameters ()
    :precondition 
      (and
        (in r262)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r261)
        (f_copy)
        (not 
          (in r262))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r263
    :parameters ()
    :precondition 
      (and
        (in r263)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r264)
        (f_copy)
        (not 
          (in r263))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r263
    :parameters ()
    :precondition 
      (and
        (in r263)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r262)
        (f_copy)
        (not 
          (in r263))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r264
    :parameters ()
    :precondition 
      (and
        (in r264)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r265)
        (f_copy)
        (not 
          (in r264))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r264
    :parameters ()
    :precondition 
      (and
        (in r264)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r263)
        (f_copy)
        (not 
          (in r264))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r265
    :parameters ()
    :precondition 
      (and
        (in r265)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r266)
        (f_copy)
        (not 
          (in r265))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r265
    :parameters ()
    :precondition 
      (and
        (in r265)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r264)
        (f_copy)
        (not 
          (in r265))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r266
    :parameters ()
    :precondition 
      (and
        (in r266)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r267)
        (f_copy)
        (not 
          (in r266))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r266
    :parameters ()
    :precondition 
      (and
        (in r266)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r265)
        (f_copy)
        (not 
          (in r266))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r267
    :parameters ()
    :precondition 
      (and
        (in r267)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r268)
        (f_copy)
        (not 
          (in r267))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r267
    :parameters ()
    :precondition 
      (and
        (in r267)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r266)
        (f_copy)
        (not 
          (in r267))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r268
    :parameters ()
    :precondition 
      (and
        (in r268)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r269)
        (f_copy)
        (not 
          (in r268))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r268
    :parameters ()
    :precondition 
      (and
        (in r268)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r267)
        (f_copy)
        (not 
          (in r268))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r269
    :parameters ()
    :precondition 
      (and
        (in r269)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r270)
        (f_copy)
        (not 
          (in r269))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r269
    :parameters ()
    :precondition 
      (and
        (in r269)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r268)
        (f_copy)
        (not 
          (in r269))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r270
    :parameters ()
    :precondition 
      (and
        (in r270)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r271)
        (f_copy)
        (not 
          (in r270))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r270
    :parameters ()
    :precondition 
      (and
        (in r270)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r269)
        (f_copy)
        (not 
          (in r270))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r271
    :parameters ()
    :precondition 
      (and
        (in r271)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r272)
        (f_copy)
        (not 
          (in r271))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r271
    :parameters ()
    :precondition 
      (and
        (in r271)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r270)
        (f_copy)
        (not 
          (in r271))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r272
    :parameters ()
    :precondition 
      (and
        (in r272)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r273)
        (f_copy)
        (not 
          (in r272))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r272
    :parameters ()
    :precondition 
      (and
        (in r272)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r271)
        (f_copy)
        (not 
          (in r272))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r273
    :parameters ()
    :precondition 
      (and
        (in r273)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r274)
        (f_copy)
        (not 
          (in r273))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r273
    :parameters ()
    :precondition 
      (and
        (in r273)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r272)
        (f_copy)
        (not 
          (in r273))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r274
    :parameters ()
    :precondition 
      (and
        (in r274)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r275)
        (f_copy)
        (not 
          (in r274))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r274
    :parameters ()
    :precondition 
      (and
        (in r274)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r273)
        (f_copy)
        (not 
          (in r274))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r275
    :parameters ()
    :precondition 
      (and
        (in r275)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r276)
        (f_copy)
        (not 
          (in r275))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r275
    :parameters ()
    :precondition 
      (and
        (in r275)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r274)
        (f_copy)
        (not 
          (in r275))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r276
    :parameters ()
    :precondition 
      (and
        (in r276)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r277)
        (f_copy)
        (not 
          (in r276))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r276
    :parameters ()
    :precondition 
      (and
        (in r276)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r275)
        (f_copy)
        (not 
          (in r276))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r277
    :parameters ()
    :precondition 
      (and
        (in r277)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r278)
        (f_copy)
        (not 
          (in r277))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r277
    :parameters ()
    :precondition 
      (and
        (in r277)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r276)
        (f_copy)
        (not 
          (in r277))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r278
    :parameters ()
    :precondition 
      (and
        (in r278)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r279)
        (f_copy)
        (not 
          (in r278))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r278
    :parameters ()
    :precondition 
      (and
        (in r278)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r277)
        (f_copy)
        (not 
          (in r278))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r279
    :parameters ()
    :precondition 
      (and
        (in r279)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r280)
        (f_copy)
        (not 
          (in r279))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r279
    :parameters ()
    :precondition 
      (and
        (in r279)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r278)
        (f_copy)
        (not 
          (in r279))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r280
    :parameters ()
    :precondition 
      (and
        (in r280)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r281)
        (f_copy)
        (not 
          (in r280))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r280
    :parameters ()
    :precondition 
      (and
        (in r280)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r279)
        (f_copy)
        (not 
          (in r280))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r281
    :parameters ()
    :precondition 
      (and
        (in r281)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r282)
        (f_copy)
        (not 
          (in r281))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r281
    :parameters ()
    :precondition 
      (and
        (in r281)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r280)
        (f_copy)
        (not 
          (in r281))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r282
    :parameters ()
    :precondition 
      (and
        (in r282)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r283)
        (f_copy)
        (not 
          (in r282))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r282
    :parameters ()
    :precondition 
      (and
        (in r282)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r281)
        (f_copy)
        (not 
          (in r282))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r283
    :parameters ()
    :precondition 
      (and
        (in r283)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r284)
        (f_copy)
        (not 
          (in r283))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r283
    :parameters ()
    :precondition 
      (and
        (in r283)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r282)
        (f_copy)
        (not 
          (in r283))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r284
    :parameters ()
    :precondition 
      (and
        (in r284)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r285)
        (f_copy)
        (not 
          (in r284))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r284
    :parameters ()
    :precondition 
      (and
        (in r284)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r283)
        (f_copy)
        (not 
          (in r284))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r285
    :parameters ()
    :precondition 
      (and
        (in r285)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r286)
        (f_copy)
        (not 
          (in r285))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r285
    :parameters ()
    :precondition 
      (and
        (in r285)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r284)
        (f_copy)
        (not 
          (in r285))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r286
    :parameters ()
    :precondition 
      (and
        (in r286)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r287)
        (f_copy)
        (not 
          (in r286))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r286
    :parameters ()
    :precondition 
      (and
        (in r286)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r285)
        (f_copy)
        (not 
          (in r286))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r287
    :parameters ()
    :precondition 
      (and
        (in r287)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r288)
        (f_copy)
        (not 
          (in r287))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r287
    :parameters ()
    :precondition 
      (and
        (in r287)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r286)
        (f_copy)
        (not 
          (in r287))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r288
    :parameters ()
    :precondition 
      (and
        (in r288)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r289)
        (f_copy)
        (not 
          (in r288))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r288
    :parameters ()
    :precondition 
      (and
        (in r288)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r287)
        (f_copy)
        (not 
          (in r288))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r289
    :parameters ()
    :precondition 
      (and
        (in r289)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r290)
        (f_copy)
        (not 
          (in r289))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r289
    :parameters ()
    :precondition 
      (and
        (in r289)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r288)
        (f_copy)
        (not 
          (in r289))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r290
    :parameters ()
    :precondition 
      (and
        (in r290)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r291)
        (f_copy)
        (not 
          (in r290))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r290
    :parameters ()
    :precondition 
      (and
        (in r290)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r289)
        (f_copy)
        (not 
          (in r290))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r291
    :parameters ()
    :precondition 
      (and
        (in r291)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r292)
        (f_copy)
        (not 
          (in r291))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r291
    :parameters ()
    :precondition 
      (and
        (in r291)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r290)
        (f_copy)
        (not 
          (in r291))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r292
    :parameters ()
    :precondition 
      (and
        (in r292)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r293)
        (f_copy)
        (not 
          (in r292))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r292
    :parameters ()
    :precondition 
      (and
        (in r292)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r291)
        (f_copy)
        (not 
          (in r292))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r293
    :parameters ()
    :precondition 
      (and
        (in r293)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r294)
        (f_copy)
        (not 
          (in r293))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r293
    :parameters ()
    :precondition 
      (and
        (in r293)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r292)
        (f_copy)
        (not 
          (in r293))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r294
    :parameters ()
    :precondition 
      (and
        (in r294)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r295)
        (f_copy)
        (not 
          (in r294))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r294
    :parameters ()
    :precondition 
      (and
        (in r294)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r293)
        (f_copy)
        (not 
          (in r294))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r295
    :parameters ()
    :precondition 
      (and
        (in r295)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r296)
        (f_copy)
        (not 
          (in r295))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r295
    :parameters ()
    :precondition 
      (and
        (in r295)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r294)
        (f_copy)
        (not 
          (in r295))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r296
    :parameters ()
    :precondition 
      (and
        (in r296)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r297)
        (f_copy)
        (not 
          (in r296))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r296
    :parameters ()
    :precondition 
      (and
        (in r296)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r295)
        (f_copy)
        (not 
          (in r296))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r297
    :parameters ()
    :precondition 
      (and
        (in r297)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r298)
        (f_copy)
        (not 
          (in r297))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r297
    :parameters ()
    :precondition 
      (and
        (in r297)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r296)
        (f_copy)
        (not 
          (in r297))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r298
    :parameters ()
    :precondition 
      (and
        (in r298)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r299)
        (f_copy)
        (not 
          (in r298))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r298
    :parameters ()
    :precondition 
      (and
        (in r298)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r297)
        (f_copy)
        (not 
          (in r298))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r299
    :parameters ()
    :precondition 
      (and
        (in r299)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r300)
        (f_copy)
        (not 
          (in r299))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r299
    :parameters ()
    :precondition 
      (and
        (in r299)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r298)
        (f_copy)
        (not 
          (in r299))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r300
    :parameters ()
    :precondition 
      (and
        (in r300)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r301)
        (f_copy)
        (not 
          (in r300))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r300
    :parameters ()
    :precondition 
      (and
        (in r300)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r299)
        (f_copy)
        (not 
          (in r300))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r301
    :parameters ()
    :precondition 
      (and
        (in r301)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r302)
        (f_copy)
        (not 
          (in r301))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r301
    :parameters ()
    :precondition 
      (and
        (in r301)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r300)
        (f_copy)
        (not 
          (in r301))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r302
    :parameters ()
    :precondition 
      (and
        (in r302)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r303)
        (f_copy)
        (not 
          (in r302))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r302
    :parameters ()
    :precondition 
      (and
        (in r302)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r301)
        (f_copy)
        (not 
          (in r302))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r303
    :parameters ()
    :precondition 
      (and
        (in r303)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r304)
        (f_copy)
        (not 
          (in r303))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r303
    :parameters ()
    :precondition 
      (and
        (in r303)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r302)
        (f_copy)
        (not 
          (in r303))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r304
    :parameters ()
    :precondition 
      (and
        (in r304)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r305)
        (f_copy)
        (not 
          (in r304))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r304
    :parameters ()
    :precondition 
      (and
        (in r304)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r303)
        (f_copy)
        (not 
          (in r304))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r305
    :parameters ()
    :precondition 
      (and
        (in r305)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r306)
        (f_copy)
        (not 
          (in r305))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r305
    :parameters ()
    :precondition 
      (and
        (in r305)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r304)
        (f_copy)
        (not 
          (in r305))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r306
    :parameters ()
    :precondition 
      (and
        (in r306)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r307)
        (f_copy)
        (not 
          (in r306))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r306
    :parameters ()
    :precondition 
      (and
        (in r306)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r305)
        (f_copy)
        (not 
          (in r306))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r307
    :parameters ()
    :precondition 
      (and
        (in r307)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r308)
        (f_copy)
        (not 
          (in r307))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r307
    :parameters ()
    :precondition 
      (and
        (in r307)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r306)
        (f_copy)
        (not 
          (in r307))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r308
    :parameters ()
    :precondition 
      (and
        (in r308)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r309)
        (f_copy)
        (not 
          (in r308))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r308
    :parameters ()
    :precondition 
      (and
        (in r308)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r307)
        (f_copy)
        (not 
          (in r308))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r309
    :parameters ()
    :precondition 
      (and
        (in r309)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r310)
        (f_copy)
        (not 
          (in r309))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r309
    :parameters ()
    :precondition 
      (and
        (in r309)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r308)
        (f_copy)
        (not 
          (in r309))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r310
    :parameters ()
    :precondition 
      (and
        (in r310)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r311)
        (f_copy)
        (not 
          (in r310))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r310
    :parameters ()
    :precondition 
      (and
        (in r310)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r309)
        (f_copy)
        (not 
          (in r310))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r311
    :parameters ()
    :precondition 
      (and
        (in r311)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r312)
        (f_copy)
        (not 
          (in r311))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r311
    :parameters ()
    :precondition 
      (and
        (in r311)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r310)
        (f_copy)
        (not 
          (in r311))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r312
    :parameters ()
    :precondition 
      (and
        (in r312)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r313)
        (f_copy)
        (not 
          (in r312))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r312
    :parameters ()
    :precondition 
      (and
        (in r312)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r311)
        (f_copy)
        (not 
          (in r312))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r313
    :parameters ()
    :precondition 
      (and
        (in r313)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r314)
        (f_copy)
        (not 
          (in r313))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r313
    :parameters ()
    :precondition 
      (and
        (in r313)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r312)
        (f_copy)
        (not 
          (in r313))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r314
    :parameters ()
    :precondition 
      (and
        (in r314)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r315)
        (f_copy)
        (not 
          (in r314))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r314
    :parameters ()
    :precondition 
      (and
        (in r314)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r313)
        (f_copy)
        (not 
          (in r314))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r315
    :parameters ()
    :precondition 
      (and
        (in r315)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r316)
        (f_copy)
        (not 
          (in r315))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r315
    :parameters ()
    :precondition 
      (and
        (in r315)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r314)
        (f_copy)
        (not 
          (in r315))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r316
    :parameters ()
    :precondition 
      (and
        (in r316)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r317)
        (f_copy)
        (not 
          (in r316))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r316
    :parameters ()
    :precondition 
      (and
        (in r316)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r315)
        (f_copy)
        (not 
          (in r316))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r317
    :parameters ()
    :precondition 
      (and
        (in r317)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r318)
        (f_copy)
        (not 
          (in r317))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r317
    :parameters ()
    :precondition 
      (and
        (in r317)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r316)
        (f_copy)
        (not 
          (in r317))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r318
    :parameters ()
    :precondition 
      (and
        (in r318)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r319)
        (f_copy)
        (not 
          (in r318))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r318
    :parameters ()
    :precondition 
      (and
        (in r318)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r317)
        (f_copy)
        (not 
          (in r318))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r319
    :parameters ()
    :precondition 
      (and
        (in r319)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r320)
        (f_copy)
        (not 
          (in r319))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r319
    :parameters ()
    :precondition 
      (and
        (in r319)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r318)
        (f_copy)
        (not 
          (in r319))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r320
    :parameters ()
    :precondition 
      (and
        (in r320)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r321)
        (f_copy)
        (not 
          (in r320))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r320
    :parameters ()
    :precondition 
      (and
        (in r320)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r319)
        (f_copy)
        (not 
          (in r320))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r321
    :parameters ()
    :precondition 
      (and
        (in r321)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r322)
        (f_copy)
        (not 
          (in r321))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r321
    :parameters ()
    :precondition 
      (and
        (in r321)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r320)
        (f_copy)
        (not 
          (in r321))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r322
    :parameters ()
    :precondition 
      (and
        (in r322)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r323)
        (f_copy)
        (not 
          (in r322))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r322
    :parameters ()
    :precondition 
      (and
        (in r322)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r321)
        (f_copy)
        (not 
          (in r322))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r323
    :parameters ()
    :precondition 
      (and
        (in r323)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r324)
        (f_copy)
        (not 
          (in r323))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r323
    :parameters ()
    :precondition 
      (and
        (in r323)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r322)
        (f_copy)
        (not 
          (in r323))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r324
    :parameters ()
    :precondition 
      (and
        (in r324)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r325)
        (f_copy)
        (not 
          (in r324))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r324
    :parameters ()
    :precondition 
      (and
        (in r324)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r323)
        (f_copy)
        (not 
          (in r324))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r325
    :parameters ()
    :precondition 
      (and
        (in r325)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r326)
        (f_copy)
        (not 
          (in r325))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r325
    :parameters ()
    :precondition 
      (and
        (in r325)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r324)
        (f_copy)
        (not 
          (in r325))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r326
    :parameters ()
    :precondition 
      (and
        (in r326)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r327)
        (f_copy)
        (not 
          (in r326))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r326
    :parameters ()
    :precondition 
      (and
        (in r326)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r325)
        (f_copy)
        (not 
          (in r326))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r327
    :parameters ()
    :precondition 
      (and
        (in r327)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r328)
        (f_copy)
        (not 
          (in r327))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r327
    :parameters ()
    :precondition 
      (and
        (in r327)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r326)
        (f_copy)
        (not 
          (in r327))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r328
    :parameters ()
    :precondition 
      (and
        (in r328)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r329)
        (f_copy)
        (not 
          (in r328))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r328
    :parameters ()
    :precondition 
      (and
        (in r328)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r327)
        (f_copy)
        (not 
          (in r328))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r329
    :parameters ()
    :precondition 
      (and
        (in r329)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r330)
        (f_copy)
        (not 
          (in r329))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r329
    :parameters ()
    :precondition 
      (and
        (in r329)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r328)
        (f_copy)
        (not 
          (in r329))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r330
    :parameters ()
    :precondition 
      (and
        (in r330)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r331)
        (f_copy)
        (not 
          (in r330))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r330
    :parameters ()
    :precondition 
      (and
        (in r330)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r329)
        (f_copy)
        (not 
          (in r330))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r331
    :parameters ()
    :precondition 
      (and
        (in r331)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r332)
        (f_copy)
        (not 
          (in r331))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r331
    :parameters ()
    :precondition 
      (and
        (in r331)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r330)
        (f_copy)
        (not 
          (in r331))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r332
    :parameters ()
    :precondition 
      (and
        (in r332)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r333)
        (f_copy)
        (not 
          (in r332))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r332
    :parameters ()
    :precondition 
      (and
        (in r332)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r331)
        (f_copy)
        (not 
          (in r332))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r333
    :parameters ()
    :precondition 
      (and
        (in r333)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r334)
        (f_copy)
        (not 
          (in r333))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r333
    :parameters ()
    :precondition 
      (and
        (in r333)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r332)
        (f_copy)
        (not 
          (in r333))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r334
    :parameters ()
    :precondition 
      (and
        (in r334)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r335)
        (f_copy)
        (not 
          (in r334))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r334
    :parameters ()
    :precondition 
      (and
        (in r334)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r333)
        (f_copy)
        (not 
          (in r334))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r335
    :parameters ()
    :precondition 
      (and
        (in r335)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r336)
        (f_copy)
        (not 
          (in r335))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r335
    :parameters ()
    :precondition 
      (and
        (in r335)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r334)
        (f_copy)
        (not 
          (in r335))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r336
    :parameters ()
    :precondition 
      (and
        (in r336)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r337)
        (f_copy)
        (not 
          (in r336))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r336
    :parameters ()
    :precondition 
      (and
        (in r336)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r335)
        (f_copy)
        (not 
          (in r336))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r337
    :parameters ()
    :precondition 
      (and
        (in r337)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r338)
        (f_copy)
        (not 
          (in r337))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r337
    :parameters ()
    :precondition 
      (and
        (in r337)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r336)
        (f_copy)
        (not 
          (in r337))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r338
    :parameters ()
    :precondition 
      (and
        (in r338)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r339)
        (f_copy)
        (not 
          (in r338))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r338
    :parameters ()
    :precondition 
      (and
        (in r338)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r337)
        (f_copy)
        (not 
          (in r338))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r339
    :parameters ()
    :precondition 
      (and
        (in r339)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r340)
        (f_copy)
        (not 
          (in r339))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r339
    :parameters ()
    :precondition 
      (and
        (in r339)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r338)
        (f_copy)
        (not 
          (in r339))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r340
    :parameters ()
    :precondition 
      (and
        (in r340)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r341)
        (f_copy)
        (not 
          (in r340))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r340
    :parameters ()
    :precondition 
      (and
        (in r340)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r339)
        (f_copy)
        (not 
          (in r340))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r341
    :parameters ()
    :precondition 
      (and
        (in r341)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r342)
        (f_copy)
        (not 
          (in r341))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r341
    :parameters ()
    :precondition 
      (and
        (in r341)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r340)
        (f_copy)
        (not 
          (in r341))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r342
    :parameters ()
    :precondition 
      (and
        (in r342)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r343)
        (f_copy)
        (not 
          (in r342))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r342
    :parameters ()
    :precondition 
      (and
        (in r342)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r341)
        (f_copy)
        (not 
          (in r342))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r343
    :parameters ()
    :precondition 
      (and
        (in r343)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r344)
        (f_copy)
        (not 
          (in r343))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r343
    :parameters ()
    :precondition 
      (and
        (in r343)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r342)
        (f_copy)
        (not 
          (in r343))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r344
    :parameters ()
    :precondition 
      (and
        (in r344)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r345)
        (f_copy)
        (not 
          (in r344))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r344
    :parameters ()
    :precondition 
      (and
        (in r344)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r343)
        (f_copy)
        (not 
          (in r344))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r345
    :parameters ()
    :precondition 
      (and
        (in r345)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r346)
        (f_copy)
        (not 
          (in r345))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r345
    :parameters ()
    :precondition 
      (and
        (in r345)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r344)
        (f_copy)
        (not 
          (in r345))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r346
    :parameters ()
    :precondition 
      (and
        (in r346)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r347)
        (f_copy)
        (not 
          (in r346))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r346
    :parameters ()
    :precondition 
      (and
        (in r346)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r345)
        (f_copy)
        (not 
          (in r346))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r347
    :parameters ()
    :precondition 
      (and
        (in r347)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r348)
        (f_copy)
        (not 
          (in r347))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r347
    :parameters ()
    :precondition 
      (and
        (in r347)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r346)
        (f_copy)
        (not 
          (in r347))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r348
    :parameters ()
    :precondition 
      (and
        (in r348)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r349)
        (f_copy)
        (not 
          (in r348))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r348
    :parameters ()
    :precondition 
      (and
        (in r348)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r347)
        (f_copy)
        (not 
          (in r348))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r349
    :parameters ()
    :precondition 
      (and
        (in r349)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r350)
        (f_copy)
        (not 
          (in r349))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r349
    :parameters ()
    :precondition 
      (and
        (in r349)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r348)
        (f_copy)
        (not 
          (in r349))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r350
    :parameters ()
    :precondition 
      (and
        (in r350)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r351)
        (f_copy)
        (not 
          (in r350))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r350
    :parameters ()
    :precondition 
      (and
        (in r350)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r349)
        (f_copy)
        (not 
          (in r350))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r351
    :parameters ()
    :precondition 
      (and
        (in r351)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r352)
        (f_copy)
        (not 
          (in r351))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r351
    :parameters ()
    :precondition 
      (and
        (in r351)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r350)
        (f_copy)
        (not 
          (in r351))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r352
    :parameters ()
    :precondition 
      (and
        (in r352)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r353)
        (f_copy)
        (not 
          (in r352))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r352
    :parameters ()
    :precondition 
      (and
        (in r352)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r351)
        (f_copy)
        (not 
          (in r352))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r353
    :parameters ()
    :precondition 
      (and
        (in r353)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r354)
        (f_copy)
        (not 
          (in r353))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r353
    :parameters ()
    :precondition 
      (and
        (in r353)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r352)
        (f_copy)
        (not 
          (in r353))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r354
    :parameters ()
    :precondition 
      (and
        (in r354)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r355)
        (f_copy)
        (not 
          (in r354))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r354
    :parameters ()
    :precondition 
      (and
        (in r354)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r353)
        (f_copy)
        (not 
          (in r354))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r355
    :parameters ()
    :precondition 
      (and
        (in r355)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r356)
        (f_copy)
        (not 
          (in r355))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r355
    :parameters ()
    :precondition 
      (and
        (in r355)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r354)
        (f_copy)
        (not 
          (in r355))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r356
    :parameters ()
    :precondition 
      (and
        (in r356)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r357)
        (f_copy)
        (not 
          (in r356))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r356
    :parameters ()
    :precondition 
      (and
        (in r356)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r355)
        (f_copy)
        (not 
          (in r356))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r357
    :parameters ()
    :precondition 
      (and
        (in r357)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r358)
        (f_copy)
        (not 
          (in r357))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r357
    :parameters ()
    :precondition 
      (and
        (in r357)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r356)
        (f_copy)
        (not 
          (in r357))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r358
    :parameters ()
    :precondition 
      (and
        (in r358)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r359)
        (f_copy)
        (not 
          (in r358))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r358
    :parameters ()
    :precondition 
      (and
        (in r358)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r357)
        (f_copy)
        (not 
          (in r358))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r359
    :parameters ()
    :precondition 
      (and
        (in r359)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r360)
        (f_copy)
        (not 
          (in r359))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r359
    :parameters ()
    :precondition 
      (and
        (in r359)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r358)
        (f_copy)
        (not 
          (in r359))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r360
    :parameters ()
    :precondition 
      (and
        (in r360)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r361)
        (f_copy)
        (not 
          (in r360))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r360
    :parameters ()
    :precondition 
      (and
        (in r360)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r359)
        (f_copy)
        (not 
          (in r360))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r361
    :parameters ()
    :precondition 
      (and
        (in r361)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r362)
        (f_copy)
        (not 
          (in r361))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r361
    :parameters ()
    :precondition 
      (and
        (in r361)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r360)
        (f_copy)
        (not 
          (in r361))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r362
    :parameters ()
    :precondition 
      (and
        (in r362)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r363)
        (f_copy)
        (not 
          (in r362))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r362
    :parameters ()
    :precondition 
      (and
        (in r362)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r361)
        (f_copy)
        (not 
          (in r362))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r363
    :parameters ()
    :precondition 
      (and
        (in r363)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r364)
        (f_copy)
        (not 
          (in r363))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r363
    :parameters ()
    :precondition 
      (and
        (in r363)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r362)
        (f_copy)
        (not 
          (in r363))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r364
    :parameters ()
    :precondition 
      (and
        (in r364)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r365)
        (f_copy)
        (not 
          (in r364))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r364
    :parameters ()
    :precondition 
      (and
        (in r364)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r363)
        (f_copy)
        (not 
          (in r364))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r365
    :parameters ()
    :precondition 
      (and
        (in r365)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r366)
        (f_copy)
        (not 
          (in r365))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r365
    :parameters ()
    :precondition 
      (and
        (in r365)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r364)
        (f_copy)
        (not 
          (in r365))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r366
    :parameters ()
    :precondition 
      (and
        (in r366)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r367)
        (f_copy)
        (not 
          (in r366))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r366
    :parameters ()
    :precondition 
      (and
        (in r366)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r365)
        (f_copy)
        (not 
          (in r366))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r367
    :parameters ()
    :precondition 
      (and
        (in r367)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r368)
        (f_copy)
        (not 
          (in r367))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r367
    :parameters ()
    :precondition 
      (and
        (in r367)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r366)
        (f_copy)
        (not 
          (in r367))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r368
    :parameters ()
    :precondition 
      (and
        (in r368)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r369)
        (f_copy)
        (not 
          (in r368))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r368
    :parameters ()
    :precondition 
      (and
        (in r368)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r367)
        (f_copy)
        (not 
          (in r368))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r369
    :parameters ()
    :precondition 
      (and
        (in r369)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r370)
        (f_copy)
        (not 
          (in r369))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r369
    :parameters ()
    :precondition 
      (and
        (in r369)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r368)
        (f_copy)
        (not 
          (in r369))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r370
    :parameters ()
    :precondition 
      (and
        (in r370)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r371)
        (f_copy)
        (not 
          (in r370))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r370
    :parameters ()
    :precondition 
      (and
        (in r370)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r369)
        (f_copy)
        (not 
          (in r370))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r371
    :parameters ()
    :precondition 
      (and
        (in r371)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r372)
        (f_copy)
        (not 
          (in r371))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r371
    :parameters ()
    :precondition 
      (and
        (in r371)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r370)
        (f_copy)
        (not 
          (in r371))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r372
    :parameters ()
    :precondition 
      (and
        (in r372)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r373)
        (f_copy)
        (not 
          (in r372))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r372
    :parameters ()
    :precondition 
      (and
        (in r372)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r371)
        (f_copy)
        (not 
          (in r372))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r373
    :parameters ()
    :precondition 
      (and
        (in r373)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r374)
        (f_copy)
        (not 
          (in r373))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r373
    :parameters ()
    :precondition 
      (and
        (in r373)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r372)
        (f_copy)
        (not 
          (in r373))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r374
    :parameters ()
    :precondition 
      (and
        (in r374)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r375)
        (f_copy)
        (not 
          (in r374))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r374
    :parameters ()
    :precondition 
      (and
        (in r374)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r373)
        (f_copy)
        (not 
          (in r374))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r375
    :parameters ()
    :precondition 
      (and
        (in r375)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r376)
        (f_copy)
        (not 
          (in r375))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r375
    :parameters ()
    :precondition 
      (and
        (in r375)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r374)
        (f_copy)
        (not 
          (in r375))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r376
    :parameters ()
    :precondition 
      (and
        (in r376)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r377)
        (f_copy)
        (not 
          (in r376))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r376
    :parameters ()
    :precondition 
      (and
        (in r376)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r375)
        (f_copy)
        (not 
          (in r376))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r377
    :parameters ()
    :precondition 
      (and
        (in r377)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r378)
        (f_copy)
        (not 
          (in r377))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r377
    :parameters ()
    :precondition 
      (and
        (in r377)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r376)
        (f_copy)
        (not 
          (in r377))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r378
    :parameters ()
    :precondition 
      (and
        (in r378)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r379)
        (f_copy)
        (not 
          (in r378))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r378
    :parameters ()
    :precondition 
      (and
        (in r378)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r377)
        (f_copy)
        (not 
          (in r378))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r379
    :parameters ()
    :precondition 
      (and
        (in r379)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r380)
        (f_copy)
        (not 
          (in r379))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r379
    :parameters ()
    :precondition 
      (and
        (in r379)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r378)
        (f_copy)
        (not 
          (in r379))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r380
    :parameters ()
    :precondition 
      (and
        (in r380)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r381)
        (f_copy)
        (not 
          (in r380))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r380
    :parameters ()
    :precondition 
      (and
        (in r380)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r379)
        (f_copy)
        (not 
          (in r380))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r381
    :parameters ()
    :precondition 
      (and
        (in r381)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r382)
        (f_copy)
        (not 
          (in r381))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r381
    :parameters ()
    :precondition 
      (and
        (in r381)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r380)
        (f_copy)
        (not 
          (in r381))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r382
    :parameters ()
    :precondition 
      (and
        (in r382)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r383)
        (f_copy)
        (not 
          (in r382))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r382
    :parameters ()
    :precondition 
      (and
        (in r382)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r381)
        (f_copy)
        (not 
          (in r382))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r383
    :parameters ()
    :precondition 
      (and
        (in r383)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r384)
        (f_copy)
        (not 
          (in r383))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r383
    :parameters ()
    :precondition 
      (and
        (in r383)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r382)
        (f_copy)
        (not 
          (in r383))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r384
    :parameters ()
    :precondition 
      (and
        (in r384)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r385)
        (f_copy)
        (not 
          (in r384))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r384
    :parameters ()
    :precondition 
      (and
        (in r384)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r383)
        (f_copy)
        (not 
          (in r384))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r385
    :parameters ()
    :precondition 
      (and
        (in r385)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r386)
        (f_copy)
        (not 
          (in r385))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r385
    :parameters ()
    :precondition 
      (and
        (in r385)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r384)
        (f_copy)
        (not 
          (in r385))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r386
    :parameters ()
    :precondition 
      (and
        (in r386)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r387)
        (f_copy)
        (not 
          (in r386))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r386
    :parameters ()
    :precondition 
      (and
        (in r386)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r385)
        (f_copy)
        (not 
          (in r386))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r387
    :parameters ()
    :precondition 
      (and
        (in r387)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r388)
        (f_copy)
        (not 
          (in r387))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r387
    :parameters ()
    :precondition 
      (and
        (in r387)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r386)
        (f_copy)
        (not 
          (in r387))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r388
    :parameters ()
    :precondition 
      (and
        (in r388)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r389)
        (f_copy)
        (not 
          (in r388))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r388
    :parameters ()
    :precondition 
      (and
        (in r388)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r387)
        (f_copy)
        (not 
          (in r388))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r389
    :parameters ()
    :precondition 
      (and
        (in r389)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r390)
        (f_copy)
        (not 
          (in r389))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r389
    :parameters ()
    :precondition 
      (and
        (in r389)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r388)
        (f_copy)
        (not 
          (in r389))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r390
    :parameters ()
    :precondition 
      (and
        (in r390)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r391)
        (f_copy)
        (not 
          (in r390))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r390
    :parameters ()
    :precondition 
      (and
        (in r390)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r389)
        (f_copy)
        (not 
          (in r390))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r391
    :parameters ()
    :precondition 
      (and
        (in r391)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r392)
        (f_copy)
        (not 
          (in r391))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r391
    :parameters ()
    :precondition 
      (and
        (in r391)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r390)
        (f_copy)
        (not 
          (in r391))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r392
    :parameters ()
    :precondition 
      (and
        (in r392)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r393)
        (f_copy)
        (not 
          (in r392))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r392
    :parameters ()
    :precondition 
      (and
        (in r392)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r391)
        (f_copy)
        (not 
          (in r392))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r393
    :parameters ()
    :precondition 
      (and
        (in r393)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r394)
        (f_copy)
        (not 
          (in r393))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r393
    :parameters ()
    :precondition 
      (and
        (in r393)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r392)
        (f_copy)
        (not 
          (in r393))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r394
    :parameters ()
    :precondition 
      (and
        (in r394)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r395)
        (f_copy)
        (not 
          (in r394))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r394
    :parameters ()
    :precondition 
      (and
        (in r394)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r393)
        (f_copy)
        (not 
          (in r394))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r395
    :parameters ()
    :precondition 
      (and
        (in r395)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r396)
        (f_copy)
        (not 
          (in r395))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r395
    :parameters ()
    :precondition 
      (and
        (in r395)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r394)
        (f_copy)
        (not 
          (in r395))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r396
    :parameters ()
    :precondition 
      (and
        (in r396)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r397)
        (f_copy)
        (not 
          (in r396))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r396
    :parameters ()
    :precondition 
      (and
        (in r396)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r395)
        (f_copy)
        (not 
          (in r396))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r397
    :parameters ()
    :precondition 
      (and
        (in r397)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r398)
        (f_copy)
        (not 
          (in r397))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r397
    :parameters ()
    :precondition 
      (and
        (in r397)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r396)
        (f_copy)
        (not 
          (in r397))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r398
    :parameters ()
    :precondition 
      (and
        (in r398)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r399)
        (f_copy)
        (not 
          (in r398))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r398
    :parameters ()
    :precondition 
      (and
        (in r398)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r397)
        (f_copy)
        (not 
          (in r398))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r399
    :parameters ()
    :precondition 
      (and
        (in r399)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r400)
        (f_copy)
        (not 
          (in r399))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r399
    :parameters ()
    :precondition 
      (and
        (in r399)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r398)
        (f_copy)
        (not 
          (in r399))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r400
    :parameters ()
    :precondition 
      (and
        (in r400)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r401)
        (f_copy)
        (not 
          (in r400))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r400
    :parameters ()
    :precondition 
      (and
        (in r400)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r399)
        (f_copy)
        (not 
          (in r400))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r401
    :parameters ()
    :precondition 
      (and
        (in r401)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r402)
        (f_copy)
        (not 
          (in r401))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r401
    :parameters ()
    :precondition 
      (and
        (in r401)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r400)
        (f_copy)
        (not 
          (in r401))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r402
    :parameters ()
    :precondition 
      (and
        (in r402)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r403)
        (f_copy)
        (not 
          (in r402))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r402
    :parameters ()
    :precondition 
      (and
        (in r402)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r401)
        (f_copy)
        (not 
          (in r402))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r403
    :parameters ()
    :precondition 
      (and
        (in r403)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r404)
        (f_copy)
        (not 
          (in r403))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r403
    :parameters ()
    :precondition 
      (and
        (in r403)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r402)
        (f_copy)
        (not 
          (in r403))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r404
    :parameters ()
    :precondition 
      (and
        (in r404)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r405)
        (f_copy)
        (not 
          (in r404))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r404
    :parameters ()
    :precondition 
      (and
        (in r404)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r403)
        (f_copy)
        (not 
          (in r404))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r405
    :parameters ()
    :precondition 
      (and
        (in r405)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r406)
        (f_copy)
        (not 
          (in r405))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r405
    :parameters ()
    :precondition 
      (and
        (in r405)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r404)
        (f_copy)
        (not 
          (in r405))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r406
    :parameters ()
    :precondition 
      (and
        (in r406)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r407)
        (f_copy)
        (not 
          (in r406))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r406
    :parameters ()
    :precondition 
      (and
        (in r406)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r405)
        (f_copy)
        (not 
          (in r406))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r407
    :parameters ()
    :precondition 
      (and
        (in r407)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r408)
        (f_copy)
        (not 
          (in r407))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r407
    :parameters ()
    :precondition 
      (and
        (in r407)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r406)
        (f_copy)
        (not 
          (in r407))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r408
    :parameters ()
    :precondition 
      (and
        (in r408)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r409)
        (f_copy)
        (not 
          (in r408))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r408
    :parameters ()
    :precondition 
      (and
        (in r408)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r407)
        (f_copy)
        (not 
          (in r408))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r409
    :parameters ()
    :precondition 
      (and
        (in r409)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r410)
        (f_copy)
        (not 
          (in r409))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r409
    :parameters ()
    :precondition 
      (and
        (in r409)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r408)
        (f_copy)
        (not 
          (in r409))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r410
    :parameters ()
    :precondition 
      (and
        (in r410)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r411)
        (f_copy)
        (not 
          (in r410))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r410
    :parameters ()
    :precondition 
      (and
        (in r410)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r409)
        (f_copy)
        (not 
          (in r410))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r411
    :parameters ()
    :precondition 
      (and
        (in r411)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r412)
        (f_copy)
        (not 
          (in r411))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r411
    :parameters ()
    :precondition 
      (and
        (in r411)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r410)
        (f_copy)
        (not 
          (in r411))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r412
    :parameters ()
    :precondition 
      (and
        (in r412)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r413)
        (f_copy)
        (not 
          (in r412))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r412
    :parameters ()
    :precondition 
      (and
        (in r412)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r411)
        (f_copy)
        (not 
          (in r412))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r413
    :parameters ()
    :precondition 
      (and
        (in r413)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r414)
        (f_copy)
        (not 
          (in r413))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r413
    :parameters ()
    :precondition 
      (and
        (in r413)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r412)
        (f_copy)
        (not 
          (in r413))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r414
    :parameters ()
    :precondition 
      (and
        (in r414)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r415)
        (f_copy)
        (not 
          (in r414))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r414
    :parameters ()
    :precondition 
      (and
        (in r414)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r413)
        (f_copy)
        (not 
          (in r414))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r415
    :parameters ()
    :precondition 
      (and
        (in r415)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r416)
        (f_copy)
        (not 
          (in r415))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r415
    :parameters ()
    :precondition 
      (and
        (in r415)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r414)
        (f_copy)
        (not 
          (in r415))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r416
    :parameters ()
    :precondition 
      (and
        (in r416)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r417)
        (f_copy)
        (not 
          (in r416))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r416
    :parameters ()
    :precondition 
      (and
        (in r416)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r415)
        (f_copy)
        (not 
          (in r416))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r417
    :parameters ()
    :precondition 
      (and
        (in r417)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r418)
        (f_copy)
        (not 
          (in r417))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r417
    :parameters ()
    :precondition 
      (and
        (in r417)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r416)
        (f_copy)
        (not 
          (in r417))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r418
    :parameters ()
    :precondition 
      (and
        (in r418)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r419)
        (f_copy)
        (not 
          (in r418))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r418
    :parameters ()
    :precondition 
      (and
        (in r418)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r417)
        (f_copy)
        (not 
          (in r418))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r419
    :parameters ()
    :precondition 
      (and
        (in r419)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r420)
        (f_copy)
        (not 
          (in r419))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r419
    :parameters ()
    :precondition 
      (and
        (in r419)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r418)
        (f_copy)
        (not 
          (in r419))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r420
    :parameters ()
    :precondition 
      (and
        (in r420)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r421)
        (f_copy)
        (not 
          (in r420))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r420
    :parameters ()
    :precondition 
      (and
        (in r420)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r419)
        (f_copy)
        (not 
          (in r420))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r421
    :parameters ()
    :precondition 
      (and
        (in r421)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r422)
        (f_copy)
        (not 
          (in r421))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r421
    :parameters ()
    :precondition 
      (and
        (in r421)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r420)
        (f_copy)
        (not 
          (in r421))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r422
    :parameters ()
    :precondition 
      (and
        (in r422)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r423)
        (f_copy)
        (not 
          (in r422))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r422
    :parameters ()
    :precondition 
      (and
        (in r422)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r421)
        (f_copy)
        (not 
          (in r422))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r423
    :parameters ()
    :precondition 
      (and
        (in r423)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r424)
        (f_copy)
        (not 
          (in r423))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r423
    :parameters ()
    :precondition 
      (and
        (in r423)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r422)
        (f_copy)
        (not 
          (in r423))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r424
    :parameters ()
    :precondition 
      (and
        (in r424)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r425)
        (f_copy)
        (not 
          (in r424))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r424
    :parameters ()
    :precondition 
      (and
        (in r424)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r423)
        (f_copy)
        (not 
          (in r424))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r425
    :parameters ()
    :precondition 
      (and
        (in r425)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r426)
        (f_copy)
        (not 
          (in r425))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r425
    :parameters ()
    :precondition 
      (and
        (in r425)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r424)
        (f_copy)
        (not 
          (in r425))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r426
    :parameters ()
    :precondition 
      (and
        (in r426)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r427)
        (f_copy)
        (not 
          (in r426))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r426
    :parameters ()
    :precondition 
      (and
        (in r426)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r425)
        (f_copy)
        (not 
          (in r426))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r427
    :parameters ()
    :precondition 
      (and
        (in r427)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r428)
        (f_copy)
        (not 
          (in r427))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r427
    :parameters ()
    :precondition 
      (and
        (in r427)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r426)
        (f_copy)
        (not 
          (in r427))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r428
    :parameters ()
    :precondition 
      (and
        (in r428)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r429)
        (f_copy)
        (not 
          (in r428))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r428
    :parameters ()
    :precondition 
      (and
        (in r428)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r427)
        (f_copy)
        (not 
          (in r428))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r429
    :parameters ()
    :precondition 
      (and
        (in r429)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r430)
        (f_copy)
        (not 
          (in r429))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r429
    :parameters ()
    :precondition 
      (and
        (in r429)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r428)
        (f_copy)
        (not 
          (in r429))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r430
    :parameters ()
    :precondition 
      (and
        (in r430)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r431)
        (f_copy)
        (not 
          (in r430))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r430
    :parameters ()
    :precondition 
      (and
        (in r430)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r429)
        (f_copy)
        (not 
          (in r430))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r431
    :parameters ()
    :precondition 
      (and
        (in r431)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r432)
        (f_copy)
        (not 
          (in r431))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r431
    :parameters ()
    :precondition 
      (and
        (in r431)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r430)
        (f_copy)
        (not 
          (in r431))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r432
    :parameters ()
    :precondition 
      (and
        (in r432)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r433)
        (f_copy)
        (not 
          (in r432))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r432
    :parameters ()
    :precondition 
      (and
        (in r432)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r431)
        (f_copy)
        (not 
          (in r432))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r433
    :parameters ()
    :precondition 
      (and
        (in r433)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r434)
        (f_copy)
        (not 
          (in r433))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r433
    :parameters ()
    :precondition 
      (and
        (in r433)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r432)
        (f_copy)
        (not 
          (in r433))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r434
    :parameters ()
    :precondition 
      (and
        (in r434)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r435)
        (f_copy)
        (not 
          (in r434))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r434
    :parameters ()
    :precondition 
      (and
        (in r434)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r433)
        (f_copy)
        (not 
          (in r434))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r435
    :parameters ()
    :precondition 
      (and
        (in r435)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r436)
        (f_copy)
        (not 
          (in r435))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r435
    :parameters ()
    :precondition 
      (and
        (in r435)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r434)
        (f_copy)
        (not 
          (in r435))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r436
    :parameters ()
    :precondition 
      (and
        (in r436)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r437)
        (f_copy)
        (not 
          (in r436))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r436
    :parameters ()
    :precondition 
      (and
        (in r436)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r435)
        (f_copy)
        (not 
          (in r436))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r437
    :parameters ()
    :precondition 
      (and
        (in r437)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r438)
        (f_copy)
        (not 
          (in r437))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r437
    :parameters ()
    :precondition 
      (and
        (in r437)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r436)
        (f_copy)
        (not 
          (in r437))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r438
    :parameters ()
    :precondition 
      (and
        (in r438)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r439)
        (f_copy)
        (not 
          (in r438))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r438
    :parameters ()
    :precondition 
      (and
        (in r438)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r437)
        (f_copy)
        (not 
          (in r438))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r439
    :parameters ()
    :precondition 
      (and
        (in r439)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r440)
        (f_copy)
        (not 
          (in r439))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r439
    :parameters ()
    :precondition 
      (and
        (in r439)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r438)
        (f_copy)
        (not 
          (in r439))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r440
    :parameters ()
    :precondition 
      (and
        (in r440)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r441)
        (f_copy)
        (not 
          (in r440))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r440
    :parameters ()
    :precondition 
      (and
        (in r440)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r439)
        (f_copy)
        (not 
          (in r440))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r441
    :parameters ()
    :precondition 
      (and
        (in r441)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r442)
        (f_copy)
        (not 
          (in r441))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r441
    :parameters ()
    :precondition 
      (and
        (in r441)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r440)
        (f_copy)
        (not 
          (in r441))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r442
    :parameters ()
    :precondition 
      (and
        (in r442)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r443)
        (f_copy)
        (not 
          (in r442))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r442
    :parameters ()
    :precondition 
      (and
        (in r442)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r441)
        (f_copy)
        (not 
          (in r442))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r443
    :parameters ()
    :precondition 
      (and
        (in r443)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r444)
        (f_copy)
        (not 
          (in r443))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r443
    :parameters ()
    :precondition 
      (and
        (in r443)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r442)
        (f_copy)
        (not 
          (in r443))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r444
    :parameters ()
    :precondition 
      (and
        (in r444)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r445)
        (f_copy)
        (not 
          (in r444))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r444
    :parameters ()
    :precondition 
      (and
        (in r444)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r443)
        (f_copy)
        (not 
          (in r444))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r445
    :parameters ()
    :precondition 
      (and
        (in r445)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r446)
        (f_copy)
        (not 
          (in r445))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r445
    :parameters ()
    :precondition 
      (and
        (in r445)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r444)
        (f_copy)
        (not 
          (in r445))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r446
    :parameters ()
    :precondition 
      (and
        (in r446)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r447)
        (f_copy)
        (not 
          (in r446))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r446
    :parameters ()
    :precondition 
      (and
        (in r446)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r445)
        (f_copy)
        (not 
          (in r446))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r447
    :parameters ()
    :precondition 
      (and
        (in r447)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r448)
        (f_copy)
        (not 
          (in r447))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r447
    :parameters ()
    :precondition 
      (and
        (in r447)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r446)
        (f_copy)
        (not 
          (in r447))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r448
    :parameters ()
    :precondition 
      (and
        (in r448)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r449)
        (f_copy)
        (not 
          (in r448))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r448
    :parameters ()
    :precondition 
      (and
        (in r448)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r447)
        (f_copy)
        (not 
          (in r448))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r449
    :parameters ()
    :precondition 
      (and
        (in r449)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r450)
        (f_copy)
        (not 
          (in r449))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r449
    :parameters ()
    :precondition 
      (and
        (in r449)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r448)
        (f_copy)
        (not 
          (in r449))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r450
    :parameters ()
    :precondition 
      (and
        (in r450)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r451)
        (f_copy)
        (not 
          (in r450))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r450
    :parameters ()
    :precondition 
      (and
        (in r450)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r449)
        (f_copy)
        (not 
          (in r450))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r451
    :parameters ()
    :precondition 
      (and
        (in r451)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r452)
        (f_copy)
        (not 
          (in r451))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r451
    :parameters ()
    :precondition 
      (and
        (in r451)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r450)
        (f_copy)
        (not 
          (in r451))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r452
    :parameters ()
    :precondition 
      (and
        (in r452)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r453)
        (f_copy)
        (not 
          (in r452))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r452
    :parameters ()
    :precondition 
      (and
        (in r452)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r451)
        (f_copy)
        (not 
          (in r452))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r453
    :parameters ()
    :precondition 
      (and
        (in r453)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r454)
        (f_copy)
        (not 
          (in r453))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r453
    :parameters ()
    :precondition 
      (and
        (in r453)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r452)
        (f_copy)
        (not 
          (in r453))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r454
    :parameters ()
    :precondition 
      (and
        (in r454)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r455)
        (f_copy)
        (not 
          (in r454))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r454
    :parameters ()
    :precondition 
      (and
        (in r454)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r453)
        (f_copy)
        (not 
          (in r454))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r455
    :parameters ()
    :precondition 
      (and
        (in r455)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r456)
        (f_copy)
        (not 
          (in r455))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r455
    :parameters ()
    :precondition 
      (and
        (in r455)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r454)
        (f_copy)
        (not 
          (in r455))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r456
    :parameters ()
    :precondition 
      (and
        (in r456)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r457)
        (f_copy)
        (not 
          (in r456))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r456
    :parameters ()
    :precondition 
      (and
        (in r456)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r455)
        (f_copy)
        (not 
          (in r456))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r457
    :parameters ()
    :precondition 
      (and
        (in r457)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r458)
        (f_copy)
        (not 
          (in r457))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r457
    :parameters ()
    :precondition 
      (and
        (in r457)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r456)
        (f_copy)
        (not 
          (in r457))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r458
    :parameters ()
    :precondition 
      (and
        (in r458)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r459)
        (f_copy)
        (not 
          (in r458))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r458
    :parameters ()
    :precondition 
      (and
        (in r458)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r457)
        (f_copy)
        (not 
          (in r458))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r459
    :parameters ()
    :precondition 
      (and
        (in r459)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r460)
        (f_copy)
        (not 
          (in r459))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r459
    :parameters ()
    :precondition 
      (and
        (in r459)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r458)
        (f_copy)
        (not 
          (in r459))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r460
    :parameters ()
    :precondition 
      (and
        (in r460)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r461)
        (f_copy)
        (not 
          (in r460))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r460
    :parameters ()
    :precondition 
      (and
        (in r460)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r459)
        (f_copy)
        (not 
          (in r460))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r461
    :parameters ()
    :precondition 
      (and
        (in r461)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r462)
        (f_copy)
        (not 
          (in r461))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r461
    :parameters ()
    :precondition 
      (and
        (in r461)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r460)
        (f_copy)
        (not 
          (in r461))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r462
    :parameters ()
    :precondition 
      (and
        (in r462)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r463)
        (f_copy)
        (not 
          (in r462))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r462
    :parameters ()
    :precondition 
      (and
        (in r462)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r461)
        (f_copy)
        (not 
          (in r462))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r463
    :parameters ()
    :precondition 
      (and
        (in r463)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r464)
        (f_copy)
        (not 
          (in r463))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r463
    :parameters ()
    :precondition 
      (and
        (in r463)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r462)
        (f_copy)
        (not 
          (in r463))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r464
    :parameters ()
    :precondition 
      (and
        (in r464)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r465)
        (f_copy)
        (not 
          (in r464))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r464
    :parameters ()
    :precondition 
      (and
        (in r464)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r463)
        (f_copy)
        (not 
          (in r464))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r465
    :parameters ()
    :precondition 
      (and
        (in r465)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r466)
        (f_copy)
        (not 
          (in r465))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r465
    :parameters ()
    :precondition 
      (and
        (in r465)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r464)
        (f_copy)
        (not 
          (in r465))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r466
    :parameters ()
    :precondition 
      (and
        (in r466)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r467)
        (f_copy)
        (not 
          (in r466))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r466
    :parameters ()
    :precondition 
      (and
        (in r466)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r465)
        (f_copy)
        (not 
          (in r466))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r467
    :parameters ()
    :precondition 
      (and
        (in r467)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r468)
        (f_copy)
        (not 
          (in r467))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r467
    :parameters ()
    :precondition 
      (and
        (in r467)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r466)
        (f_copy)
        (not 
          (in r467))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r468
    :parameters ()
    :precondition 
      (and
        (in r468)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r469)
        (f_copy)
        (not 
          (in r468))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r468
    :parameters ()
    :precondition 
      (and
        (in r468)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r467)
        (f_copy)
        (not 
          (in r468))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r469
    :parameters ()
    :precondition 
      (and
        (in r469)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r470)
        (f_copy)
        (not 
          (in r469))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r469
    :parameters ()
    :precondition 
      (and
        (in r469)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r468)
        (f_copy)
        (not 
          (in r469))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r470
    :parameters ()
    :precondition 
      (and
        (in r470)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r471)
        (f_copy)
        (not 
          (in r470))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r470
    :parameters ()
    :precondition 
      (and
        (in r470)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r469)
        (f_copy)
        (not 
          (in r470))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r471
    :parameters ()
    :precondition 
      (and
        (in r471)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r472)
        (f_copy)
        (not 
          (in r471))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r471
    :parameters ()
    :precondition 
      (and
        (in r471)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r470)
        (f_copy)
        (not 
          (in r471))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r472
    :parameters ()
    :precondition 
      (and
        (in r472)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r473)
        (f_copy)
        (not 
          (in r472))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r472
    :parameters ()
    :precondition 
      (and
        (in r472)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r471)
        (f_copy)
        (not 
          (in r472))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r473
    :parameters ()
    :precondition 
      (and
        (in r473)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r474)
        (f_copy)
        (not 
          (in r473))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r473
    :parameters ()
    :precondition 
      (and
        (in r473)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r472)
        (f_copy)
        (not 
          (in r473))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r474
    :parameters ()
    :precondition 
      (and
        (in r474)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r475)
        (f_copy)
        (not 
          (in r474))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r474
    :parameters ()
    :precondition 
      (and
        (in r474)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r473)
        (f_copy)
        (not 
          (in r474))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r475
    :parameters ()
    :precondition 
      (and
        (in r475)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r476)
        (f_copy)
        (not 
          (in r475))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r475
    :parameters ()
    :precondition 
      (and
        (in r475)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r474)
        (f_copy)
        (not 
          (in r475))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r476
    :parameters ()
    :precondition 
      (and
        (in r476)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r477)
        (f_copy)
        (not 
          (in r476))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r476
    :parameters ()
    :precondition 
      (and
        (in r476)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r475)
        (f_copy)
        (not 
          (in r476))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r477
    :parameters ()
    :precondition 
      (and
        (in r477)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r478)
        (f_copy)
        (not 
          (in r477))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r477
    :parameters ()
    :precondition 
      (and
        (in r477)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r476)
        (f_copy)
        (not 
          (in r477))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r478
    :parameters ()
    :precondition 
      (and
        (in r478)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r479)
        (f_copy)
        (not 
          (in r478))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r478
    :parameters ()
    :precondition 
      (and
        (in r478)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r477)
        (f_copy)
        (not 
          (in r478))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r479
    :parameters ()
    :precondition 
      (and
        (in r479)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r480)
        (f_copy)
        (not 
          (in r479))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r479
    :parameters ()
    :precondition 
      (and
        (in r479)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r478)
        (f_copy)
        (not 
          (in r479))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r480
    :parameters ()
    :precondition 
      (and
        (in r480)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r481)
        (f_copy)
        (not 
          (in r480))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r480
    :parameters ()
    :precondition 
      (and
        (in r480)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r479)
        (f_copy)
        (not 
          (in r480))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r481
    :parameters ()
    :precondition 
      (and
        (in r481)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r482)
        (f_copy)
        (not 
          (in r481))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r481
    :parameters ()
    :precondition 
      (and
        (in r481)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r480)
        (f_copy)
        (not 
          (in r481))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r482
    :parameters ()
    :precondition 
      (and
        (in r482)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r483)
        (f_copy)
        (not 
          (in r482))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r482
    :parameters ()
    :precondition 
      (and
        (in r482)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r481)
        (f_copy)
        (not 
          (in r482))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r483
    :parameters ()
    :precondition 
      (and
        (in r483)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r484)
        (f_copy)
        (not 
          (in r483))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r483
    :parameters ()
    :precondition 
      (and
        (in r483)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r482)
        (f_copy)
        (not 
          (in r483))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r484
    :parameters ()
    :precondition 
      (and
        (in r484)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r485)
        (f_copy)
        (not 
          (in r484))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r484
    :parameters ()
    :precondition 
      (and
        (in r484)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r483)
        (f_copy)
        (not 
          (in r484))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r485
    :parameters ()
    :precondition 
      (and
        (in r485)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r486)
        (f_copy)
        (not 
          (in r485))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r485
    :parameters ()
    :precondition 
      (and
        (in r485)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r484)
        (f_copy)
        (not 
          (in r485))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r486
    :parameters ()
    :precondition 
      (and
        (in r486)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r487)
        (f_copy)
        (not 
          (in r486))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r486
    :parameters ()
    :precondition 
      (and
        (in r486)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r485)
        (f_copy)
        (not 
          (in r486))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r487
    :parameters ()
    :precondition 
      (and
        (in r487)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r488)
        (f_copy)
        (not 
          (in r487))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r487
    :parameters ()
    :precondition 
      (and
        (in r487)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r486)
        (f_copy)
        (not 
          (in r487))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r488
    :parameters ()
    :precondition 
      (and
        (in r488)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r489)
        (f_copy)
        (not 
          (in r488))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r488
    :parameters ()
    :precondition 
      (and
        (in r488)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r487)
        (f_copy)
        (not 
          (in r488))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r489
    :parameters ()
    :precondition 
      (and
        (in r489)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r490)
        (f_copy)
        (not 
          (in r489))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r489
    :parameters ()
    :precondition 
      (and
        (in r489)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r488)
        (f_copy)
        (not 
          (in r489))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r490
    :parameters ()
    :precondition 
      (and
        (in r490)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r491)
        (f_copy)
        (not 
          (in r490))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r490
    :parameters ()
    :precondition 
      (and
        (in r490)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r489)
        (f_copy)
        (not 
          (in r490))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r491
    :parameters ()
    :precondition 
      (and
        (in r491)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r492)
        (f_copy)
        (not 
          (in r491))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r491
    :parameters ()
    :precondition 
      (and
        (in r491)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r490)
        (f_copy)
        (not 
          (in r491))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r492
    :parameters ()
    :precondition 
      (and
        (in r492)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r493)
        (f_copy)
        (not 
          (in r492))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r492
    :parameters ()
    :precondition 
      (and
        (in r492)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r491)
        (f_copy)
        (not 
          (in r492))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r493
    :parameters ()
    :precondition 
      (and
        (in r493)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r494)
        (f_copy)
        (not 
          (in r493))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r493
    :parameters ()
    :precondition 
      (and
        (in r493)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r492)
        (f_copy)
        (not 
          (in r493))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r494
    :parameters ()
    :precondition 
      (and
        (in r494)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r495)
        (f_copy)
        (not 
          (in r494))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r494
    :parameters ()
    :precondition 
      (and
        (in r494)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r493)
        (f_copy)
        (not 
          (in r494))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r495
    :parameters ()
    :precondition 
      (and
        (in r495)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r496)
        (f_copy)
        (not 
          (in r495))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r495
    :parameters ()
    :precondition 
      (and
        (in r495)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r494)
        (f_copy)
        (not 
          (in r495))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r496
    :parameters ()
    :precondition 
      (and
        (in r496)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r497)
        (f_copy)
        (not 
          (in r496))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r496
    :parameters ()
    :precondition 
      (and
        (in r496)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r495)
        (f_copy)
        (not 
          (in r496))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r497
    :parameters ()
    :precondition 
      (and
        (in r497)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r498)
        (f_copy)
        (not 
          (in r497))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r497
    :parameters ()
    :precondition 
      (and
        (in r497)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r496)
        (f_copy)
        (not 
          (in r497))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r498
    :parameters ()
    :precondition 
      (and
        (in r498)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r499)
        (f_copy)
        (not 
          (in r498))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r498
    :parameters ()
    :precondition 
      (and
        (in r498)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r497)
        (f_copy)
        (not 
          (in r498))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r499
    :parameters ()
    :precondition 
      (and
        (in r499)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r500)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r499))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r499
    :parameters ()
    :precondition 
      (and
        (in r499)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r498)
        (f_copy)
        (not 
          (in r499))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r500
    :parameters ()
    :precondition 
      (and
        (in r500)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r501)
        (f_copy)
        (not 
          (in r500))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r500
    :parameters ()
    :precondition 
      (and
        (in r500)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r499)
        (f_copy)
        (not 
          (in r500))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r501
    :parameters ()
    :precondition 
      (and
        (in r501)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r502)
        (f_copy)
        (not 
          (in r501))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r501
    :parameters ()
    :precondition 
      (and
        (in r501)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r500)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r501))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r502
    :parameters ()
    :precondition 
      (and
        (in r502)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r503)
        (f_copy)
        (not 
          (in r502))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r502
    :parameters ()
    :precondition 
      (and
        (in r502)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r501)
        (f_copy)
        (not 
          (in r502))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r503
    :parameters ()
    :precondition 
      (and
        (in r503)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r504)
        (f_copy)
        (not 
          (in r503))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r503
    :parameters ()
    :precondition 
      (and
        (in r503)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r502)
        (f_copy)
        (not 
          (in r503))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r504
    :parameters ()
    :precondition 
      (and
        (in r504)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r505)
        (f_copy)
        (not 
          (in r504))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r504
    :parameters ()
    :precondition 
      (and
        (in r504)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r503)
        (f_copy)
        (not 
          (in r504))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r505
    :parameters ()
    :precondition 
      (and
        (in r505)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r506)
        (f_copy)
        (not 
          (in r505))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r505
    :parameters ()
    :precondition 
      (and
        (in r505)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r504)
        (f_copy)
        (not 
          (in r505))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r506
    :parameters ()
    :precondition 
      (and
        (in r506)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r507)
        (f_copy)
        (not 
          (in r506))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r506
    :parameters ()
    :precondition 
      (and
        (in r506)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r505)
        (f_copy)
        (not 
          (in r506))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r507
    :parameters ()
    :precondition 
      (and
        (in r507)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r508)
        (f_copy)
        (not 
          (in r507))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r507
    :parameters ()
    :precondition 
      (and
        (in r507)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r506)
        (f_copy)
        (not 
          (in r507))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r508
    :parameters ()
    :precondition 
      (and
        (in r508)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r509)
        (f_copy)
        (not 
          (in r508))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r508
    :parameters ()
    :precondition 
      (and
        (in r508)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r507)
        (f_copy)
        (not 
          (in r508))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r509
    :parameters ()
    :precondition 
      (and
        (in r509)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r510)
        (f_copy)
        (not 
          (in r509))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r509
    :parameters ()
    :precondition 
      (and
        (in r509)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r508)
        (f_copy)
        (not 
          (in r509))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r510
    :parameters ()
    :precondition 
      (and
        (in r510)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r511)
        (f_copy)
        (not 
          (in r510))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r510
    :parameters ()
    :precondition 
      (and
        (in r510)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r509)
        (f_copy)
        (not 
          (in r510))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r511
    :parameters ()
    :precondition 
      (and
        (in r511)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r512)
        (f_copy)
        (not 
          (in r511))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r511
    :parameters ()
    :precondition 
      (and
        (in r511)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r510)
        (f_copy)
        (not 
          (in r511))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r512
    :parameters ()
    :precondition 
      (and
        (in r512)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r513)
        (f_copy)
        (not 
          (in r512))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r512
    :parameters ()
    :precondition 
      (and
        (in r512)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r511)
        (f_copy)
        (not 
          (in r512))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r513
    :parameters ()
    :precondition 
      (and
        (in r513)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r514)
        (f_copy)
        (not 
          (in r513))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r513
    :parameters ()
    :precondition 
      (and
        (in r513)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r512)
        (f_copy)
        (not 
          (in r513))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r514
    :parameters ()
    :precondition 
      (and
        (in r514)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r515)
        (f_copy)
        (not 
          (in r514))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r514
    :parameters ()
    :precondition 
      (and
        (in r514)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r513)
        (f_copy)
        (not 
          (in r514))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r515
    :parameters ()
    :precondition 
      (and
        (in r515)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r516)
        (f_copy)
        (not 
          (in r515))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r515
    :parameters ()
    :precondition 
      (and
        (in r515)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r514)
        (f_copy)
        (not 
          (in r515))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r516
    :parameters ()
    :precondition 
      (and
        (in r516)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r517)
        (f_copy)
        (not 
          (in r516))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r516
    :parameters ()
    :precondition 
      (and
        (in r516)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r515)
        (f_copy)
        (not 
          (in r516))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r517
    :parameters ()
    :precondition 
      (and
        (in r517)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r518)
        (f_copy)
        (not 
          (in r517))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r517
    :parameters ()
    :precondition 
      (and
        (in r517)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r516)
        (f_copy)
        (not 
          (in r517))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r518
    :parameters ()
    :precondition 
      (and
        (in r518)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r519)
        (f_copy)
        (not 
          (in r518))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r518
    :parameters ()
    :precondition 
      (and
        (in r518)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r517)
        (f_copy)
        (not 
          (in r518))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r519
    :parameters ()
    :precondition 
      (and
        (in r519)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r520)
        (f_copy)
        (not 
          (in r519))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r519
    :parameters ()
    :precondition 
      (and
        (in r519)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r518)
        (f_copy)
        (not 
          (in r519))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r520
    :parameters ()
    :precondition 
      (and
        (in r520)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r521)
        (f_copy)
        (not 
          (in r520))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r520
    :parameters ()
    :precondition 
      (and
        (in r520)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r519)
        (f_copy)
        (not 
          (in r520))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r521
    :parameters ()
    :precondition 
      (and
        (in r521)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r522)
        (f_copy)
        (not 
          (in r521))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r521
    :parameters ()
    :precondition 
      (and
        (in r521)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r520)
        (f_copy)
        (not 
          (in r521))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r522
    :parameters ()
    :precondition 
      (and
        (in r522)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r523)
        (f_copy)
        (not 
          (in r522))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r522
    :parameters ()
    :precondition 
      (and
        (in r522)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r521)
        (f_copy)
        (not 
          (in r522))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r523
    :parameters ()
    :precondition 
      (and
        (in r523)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r524)
        (f_copy)
        (not 
          (in r523))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r523
    :parameters ()
    :precondition 
      (and
        (in r523)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r522)
        (f_copy)
        (not 
          (in r523))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r524
    :parameters ()
    :precondition 
      (and
        (in r524)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r525)
        (f_copy)
        (not 
          (in r524))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r524
    :parameters ()
    :precondition 
      (and
        (in r524)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r523)
        (f_copy)
        (not 
          (in r524))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r525
    :parameters ()
    :precondition 
      (and
        (in r525)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r526)
        (f_copy)
        (not 
          (in r525))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r525
    :parameters ()
    :precondition 
      (and
        (in r525)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r524)
        (f_copy)
        (not 
          (in r525))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r526
    :parameters ()
    :precondition 
      (and
        (in r526)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r527)
        (f_copy)
        (not 
          (in r526))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r526
    :parameters ()
    :precondition 
      (and
        (in r526)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r525)
        (f_copy)
        (not 
          (in r526))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r527
    :parameters ()
    :precondition 
      (and
        (in r527)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r528)
        (f_copy)
        (not 
          (in r527))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r527
    :parameters ()
    :precondition 
      (and
        (in r527)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r526)
        (f_copy)
        (not 
          (in r527))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r528
    :parameters ()
    :precondition 
      (and
        (in r528)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r529)
        (f_copy)
        (not 
          (in r528))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r528
    :parameters ()
    :precondition 
      (and
        (in r528)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r527)
        (f_copy)
        (not 
          (in r528))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r529
    :parameters ()
    :precondition 
      (and
        (in r529)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r530)
        (f_copy)
        (not 
          (in r529))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r529
    :parameters ()
    :precondition 
      (and
        (in r529)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r528)
        (f_copy)
        (not 
          (in r529))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r530
    :parameters ()
    :precondition 
      (and
        (in r530)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r531)
        (f_copy)
        (not 
          (in r530))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r530
    :parameters ()
    :precondition 
      (and
        (in r530)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r529)
        (f_copy)
        (not 
          (in r530))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r531
    :parameters ()
    :precondition 
      (and
        (in r531)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r532)
        (f_copy)
        (not 
          (in r531))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r531
    :parameters ()
    :precondition 
      (and
        (in r531)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r530)
        (f_copy)
        (not 
          (in r531))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r532
    :parameters ()
    :precondition 
      (and
        (in r532)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r533)
        (f_copy)
        (not 
          (in r532))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r532
    :parameters ()
    :precondition 
      (and
        (in r532)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r531)
        (f_copy)
        (not 
          (in r532))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r533
    :parameters ()
    :precondition 
      (and
        (in r533)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r534)
        (f_copy)
        (not 
          (in r533))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r533
    :parameters ()
    :precondition 
      (and
        (in r533)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r532)
        (f_copy)
        (not 
          (in r533))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r534
    :parameters ()
    :precondition 
      (and
        (in r534)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r535)
        (f_copy)
        (not 
          (in r534))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r534
    :parameters ()
    :precondition 
      (and
        (in r534)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r533)
        (f_copy)
        (not 
          (in r534))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r535
    :parameters ()
    :precondition 
      (and
        (in r535)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r536)
        (f_copy)
        (not 
          (in r535))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r535
    :parameters ()
    :precondition 
      (and
        (in r535)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r534)
        (f_copy)
        (not 
          (in r535))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r536
    :parameters ()
    :precondition 
      (and
        (in r536)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r537)
        (f_copy)
        (not 
          (in r536))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r536
    :parameters ()
    :precondition 
      (and
        (in r536)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r535)
        (f_copy)
        (not 
          (in r536))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r537
    :parameters ()
    :precondition 
      (and
        (in r537)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r538)
        (f_copy)
        (not 
          (in r537))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r537
    :parameters ()
    :precondition 
      (and
        (in r537)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r536)
        (f_copy)
        (not 
          (in r537))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r538
    :parameters ()
    :precondition 
      (and
        (in r538)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r539)
        (f_copy)
        (not 
          (in r538))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r538
    :parameters ()
    :precondition 
      (and
        (in r538)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r537)
        (f_copy)
        (not 
          (in r538))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r539
    :parameters ()
    :precondition 
      (and
        (in r539)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r540)
        (f_copy)
        (not 
          (in r539))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r539
    :parameters ()
    :precondition 
      (and
        (in r539)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r538)
        (f_copy)
        (not 
          (in r539))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r540
    :parameters ()
    :precondition 
      (and
        (in r540)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r541)
        (f_copy)
        (not 
          (in r540))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r540
    :parameters ()
    :precondition 
      (and
        (in r540)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r539)
        (f_copy)
        (not 
          (in r540))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r541
    :parameters ()
    :precondition 
      (and
        (in r541)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r542)
        (f_copy)
        (not 
          (in r541))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r541
    :parameters ()
    :precondition 
      (and
        (in r541)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r540)
        (f_copy)
        (not 
          (in r541))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r542
    :parameters ()
    :precondition 
      (and
        (in r542)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r543)
        (f_copy)
        (not 
          (in r542))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r542
    :parameters ()
    :precondition 
      (and
        (in r542)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r541)
        (f_copy)
        (not 
          (in r542))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r543
    :parameters ()
    :precondition 
      (and
        (in r543)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r544)
        (f_copy)
        (not 
          (in r543))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r543
    :parameters ()
    :precondition 
      (and
        (in r543)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r542)
        (f_copy)
        (not 
          (in r543))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r544
    :parameters ()
    :precondition 
      (and
        (in r544)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r545)
        (f_copy)
        (not 
          (in r544))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r544
    :parameters ()
    :precondition 
      (and
        (in r544)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r543)
        (f_copy)
        (not 
          (in r544))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r545
    :parameters ()
    :precondition 
      (and
        (in r545)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r546)
        (f_copy)
        (not 
          (in r545))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r545
    :parameters ()
    :precondition 
      (and
        (in r545)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r544)
        (f_copy)
        (not 
          (in r545))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r546
    :parameters ()
    :precondition 
      (and
        (in r546)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r547)
        (f_copy)
        (not 
          (in r546))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r546
    :parameters ()
    :precondition 
      (and
        (in r546)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r545)
        (f_copy)
        (not 
          (in r546))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r547
    :parameters ()
    :precondition 
      (and
        (in r547)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r548)
        (f_copy)
        (not 
          (in r547))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r547
    :parameters ()
    :precondition 
      (and
        (in r547)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r546)
        (f_copy)
        (not 
          (in r547))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r548
    :parameters ()
    :precondition 
      (and
        (in r548)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r549)
        (f_copy)
        (not 
          (in r548))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r548
    :parameters ()
    :precondition 
      (and
        (in r548)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r547)
        (f_copy)
        (not 
          (in r548))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r549
    :parameters ()
    :precondition 
      (and
        (in r549)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r550)
        (f_copy)
        (not 
          (in r549))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r549
    :parameters ()
    :precondition 
      (and
        (in r549)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r548)
        (f_copy)
        (not 
          (in r549))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r550
    :parameters ()
    :precondition 
      (and
        (in r550)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r551)
        (f_copy)
        (not 
          (in r550))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r550
    :parameters ()
    :precondition 
      (and
        (in r550)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r549)
        (f_copy)
        (not 
          (in r550))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r551
    :parameters ()
    :precondition 
      (and
        (in r551)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r552)
        (f_copy)
        (not 
          (in r551))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r551
    :parameters ()
    :precondition 
      (and
        (in r551)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r550)
        (f_copy)
        (not 
          (in r551))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r552
    :parameters ()
    :precondition 
      (and
        (in r552)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r553)
        (f_copy)
        (not 
          (in r552))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r552
    :parameters ()
    :precondition 
      (and
        (in r552)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r551)
        (f_copy)
        (not 
          (in r552))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r553
    :parameters ()
    :precondition 
      (and
        (in r553)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r554)
        (f_copy)
        (not 
          (in r553))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r553
    :parameters ()
    :precondition 
      (and
        (in r553)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r552)
        (f_copy)
        (not 
          (in r553))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r554
    :parameters ()
    :precondition 
      (and
        (in r554)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r555)
        (f_copy)
        (not 
          (in r554))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r554
    :parameters ()
    :precondition 
      (and
        (in r554)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r553)
        (f_copy)
        (not 
          (in r554))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r555
    :parameters ()
    :precondition 
      (and
        (in r555)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r556)
        (f_copy)
        (not 
          (in r555))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r555
    :parameters ()
    :precondition 
      (and
        (in r555)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r554)
        (f_copy)
        (not 
          (in r555))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r556
    :parameters ()
    :precondition 
      (and
        (in r556)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r557)
        (f_copy)
        (not 
          (in r556))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r556
    :parameters ()
    :precondition 
      (and
        (in r556)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r555)
        (f_copy)
        (not 
          (in r556))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r557
    :parameters ()
    :precondition 
      (and
        (in r557)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r558)
        (f_copy)
        (not 
          (in r557))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r557
    :parameters ()
    :precondition 
      (and
        (in r557)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r556)
        (f_copy)
        (not 
          (in r557))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r558
    :parameters ()
    :precondition 
      (and
        (in r558)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r559)
        (f_copy)
        (not 
          (in r558))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r558
    :parameters ()
    :precondition 
      (and
        (in r558)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r557)
        (f_copy)
        (not 
          (in r558))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r559
    :parameters ()
    :precondition 
      (and
        (in r559)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r560)
        (f_copy)
        (not 
          (in r559))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r559
    :parameters ()
    :precondition 
      (and
        (in r559)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r558)
        (f_copy)
        (not 
          (in r559))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r560
    :parameters ()
    :precondition 
      (and
        (in r560)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r561)
        (f_copy)
        (not 
          (in r560))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r560
    :parameters ()
    :precondition 
      (and
        (in r560)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r559)
        (f_copy)
        (not 
          (in r560))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r561
    :parameters ()
    :precondition 
      (and
        (in r561)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r562)
        (f_copy)
        (not 
          (in r561))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r561
    :parameters ()
    :precondition 
      (and
        (in r561)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r560)
        (f_copy)
        (not 
          (in r561))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r562
    :parameters ()
    :precondition 
      (and
        (in r562)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r563)
        (f_copy)
        (not 
          (in r562))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r562
    :parameters ()
    :precondition 
      (and
        (in r562)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r561)
        (f_copy)
        (not 
          (in r562))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r563
    :parameters ()
    :precondition 
      (and
        (in r563)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r564)
        (f_copy)
        (not 
          (in r563))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r563
    :parameters ()
    :precondition 
      (and
        (in r563)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r562)
        (f_copy)
        (not 
          (in r563))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r564
    :parameters ()
    :precondition 
      (and
        (in r564)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r565)
        (f_copy)
        (not 
          (in r564))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r564
    :parameters ()
    :precondition 
      (and
        (in r564)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r563)
        (f_copy)
        (not 
          (in r564))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r565
    :parameters ()
    :precondition 
      (and
        (in r565)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r566)
        (f_copy)
        (not 
          (in r565))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r565
    :parameters ()
    :precondition 
      (and
        (in r565)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r564)
        (f_copy)
        (not 
          (in r565))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r566
    :parameters ()
    :precondition 
      (and
        (in r566)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r567)
        (f_copy)
        (not 
          (in r566))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r566
    :parameters ()
    :precondition 
      (and
        (in r566)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r565)
        (f_copy)
        (not 
          (in r566))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r567
    :parameters ()
    :precondition 
      (and
        (in r567)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r568)
        (f_copy)
        (not 
          (in r567))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r567
    :parameters ()
    :precondition 
      (and
        (in r567)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r566)
        (f_copy)
        (not 
          (in r567))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r568
    :parameters ()
    :precondition 
      (and
        (in r568)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r569)
        (f_copy)
        (not 
          (in r568))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r568
    :parameters ()
    :precondition 
      (and
        (in r568)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r567)
        (f_copy)
        (not 
          (in r568))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r569
    :parameters ()
    :precondition 
      (and
        (in r569)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r570)
        (f_copy)
        (not 
          (in r569))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r569
    :parameters ()
    :precondition 
      (and
        (in r569)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r568)
        (f_copy)
        (not 
          (in r569))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r570
    :parameters ()
    :precondition 
      (and
        (in r570)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r571)
        (f_copy)
        (not 
          (in r570))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r570
    :parameters ()
    :precondition 
      (and
        (in r570)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r569)
        (f_copy)
        (not 
          (in r570))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r571
    :parameters ()
    :precondition 
      (and
        (in r571)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r572)
        (f_copy)
        (not 
          (in r571))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r571
    :parameters ()
    :precondition 
      (and
        (in r571)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r570)
        (f_copy)
        (not 
          (in r571))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r572
    :parameters ()
    :precondition 
      (and
        (in r572)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r573)
        (f_copy)
        (not 
          (in r572))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r572
    :parameters ()
    :precondition 
      (and
        (in r572)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r571)
        (f_copy)
        (not 
          (in r572))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r573
    :parameters ()
    :precondition 
      (and
        (in r573)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r574)
        (f_copy)
        (not 
          (in r573))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r573
    :parameters ()
    :precondition 
      (and
        (in r573)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r572)
        (f_copy)
        (not 
          (in r573))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r574
    :parameters ()
    :precondition 
      (and
        (in r574)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r575)
        (f_copy)
        (not 
          (in r574))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r574
    :parameters ()
    :precondition 
      (and
        (in r574)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r573)
        (f_copy)
        (not 
          (in r574))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r575
    :parameters ()
    :precondition 
      (and
        (in r575)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r576)
        (f_copy)
        (not 
          (in r575))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r575
    :parameters ()
    :precondition 
      (and
        (in r575)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r574)
        (f_copy)
        (not 
          (in r575))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r576
    :parameters ()
    :precondition 
      (and
        (in r576)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r577)
        (f_copy)
        (not 
          (in r576))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r576
    :parameters ()
    :precondition 
      (and
        (in r576)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r575)
        (f_copy)
        (not 
          (in r576))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r577
    :parameters ()
    :precondition 
      (and
        (in r577)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r578)
        (f_copy)
        (not 
          (in r577))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r577
    :parameters ()
    :precondition 
      (and
        (in r577)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r576)
        (f_copy)
        (not 
          (in r577))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r578
    :parameters ()
    :precondition 
      (and
        (in r578)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r579)
        (f_copy)
        (not 
          (in r578))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r578
    :parameters ()
    :precondition 
      (and
        (in r578)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r577)
        (f_copy)
        (not 
          (in r578))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r579
    :parameters ()
    :precondition 
      (and
        (in r579)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r580)
        (f_copy)
        (not 
          (in r579))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r579
    :parameters ()
    :precondition 
      (and
        (in r579)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r578)
        (f_copy)
        (not 
          (in r579))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r580
    :parameters ()
    :precondition 
      (and
        (in r580)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r581)
        (f_copy)
        (not 
          (in r580))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r580
    :parameters ()
    :precondition 
      (and
        (in r580)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r579)
        (f_copy)
        (not 
          (in r580))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r581
    :parameters ()
    :precondition 
      (and
        (in r581)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r582)
        (f_copy)
        (not 
          (in r581))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r581
    :parameters ()
    :precondition 
      (and
        (in r581)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r580)
        (f_copy)
        (not 
          (in r581))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r582
    :parameters ()
    :precondition 
      (and
        (in r582)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r583)
        (f_copy)
        (not 
          (in r582))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r582
    :parameters ()
    :precondition 
      (and
        (in r582)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r581)
        (f_copy)
        (not 
          (in r582))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r583
    :parameters ()
    :precondition 
      (and
        (in r583)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r584)
        (f_copy)
        (not 
          (in r583))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r583
    :parameters ()
    :precondition 
      (and
        (in r583)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r582)
        (f_copy)
        (not 
          (in r583))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r584
    :parameters ()
    :precondition 
      (and
        (in r584)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r585)
        (f_copy)
        (not 
          (in r584))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r584
    :parameters ()
    :precondition 
      (and
        (in r584)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r583)
        (f_copy)
        (not 
          (in r584))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r585
    :parameters ()
    :precondition 
      (and
        (in r585)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r586)
        (f_copy)
        (not 
          (in r585))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r585
    :parameters ()
    :precondition 
      (and
        (in r585)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r584)
        (f_copy)
        (not 
          (in r585))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r586
    :parameters ()
    :precondition 
      (and
        (in r586)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r587)
        (f_copy)
        (not 
          (in r586))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r586
    :parameters ()
    :precondition 
      (and
        (in r586)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r585)
        (f_copy)
        (not 
          (in r586))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r587
    :parameters ()
    :precondition 
      (and
        (in r587)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r588)
        (f_copy)
        (not 
          (in r587))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r587
    :parameters ()
    :precondition 
      (and
        (in r587)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r586)
        (f_copy)
        (not 
          (in r587))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r588
    :parameters ()
    :precondition 
      (and
        (in r588)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r589)
        (f_copy)
        (not 
          (in r588))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r588
    :parameters ()
    :precondition 
      (and
        (in r588)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r587)
        (f_copy)
        (not 
          (in r588))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r589
    :parameters ()
    :precondition 
      (and
        (in r589)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r590)
        (f_copy)
        (not 
          (in r589))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r589
    :parameters ()
    :precondition 
      (and
        (in r589)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r588)
        (f_copy)
        (not 
          (in r589))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r590
    :parameters ()
    :precondition 
      (and
        (in r590)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r591)
        (f_copy)
        (not 
          (in r590))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r590
    :parameters ()
    :precondition 
      (and
        (in r590)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r589)
        (f_copy)
        (not 
          (in r590))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r591
    :parameters ()
    :precondition 
      (and
        (in r591)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r592)
        (f_copy)
        (not 
          (in r591))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r591
    :parameters ()
    :precondition 
      (and
        (in r591)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r590)
        (f_copy)
        (not 
          (in r591))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r592
    :parameters ()
    :precondition 
      (and
        (in r592)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r593)
        (f_copy)
        (not 
          (in r592))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r592
    :parameters ()
    :precondition 
      (and
        (in r592)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r591)
        (f_copy)
        (not 
          (in r592))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r593
    :parameters ()
    :precondition 
      (and
        (in r593)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r594)
        (f_copy)
        (not 
          (in r593))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r593
    :parameters ()
    :precondition 
      (and
        (in r593)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r592)
        (f_copy)
        (not 
          (in r593))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r594
    :parameters ()
    :precondition 
      (and
        (in r594)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r595)
        (f_copy)
        (not 
          (in r594))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r594
    :parameters ()
    :precondition 
      (and
        (in r594)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r593)
        (f_copy)
        (not 
          (in r594))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r595
    :parameters ()
    :precondition 
      (and
        (in r595)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r596)
        (f_copy)
        (not 
          (in r595))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r595
    :parameters ()
    :precondition 
      (and
        (in r595)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r594)
        (f_copy)
        (not 
          (in r595))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r596
    :parameters ()
    :precondition 
      (and
        (in r596)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r597)
        (f_copy)
        (not 
          (in r596))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r596
    :parameters ()
    :precondition 
      (and
        (in r596)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r595)
        (f_copy)
        (not 
          (in r596))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r597
    :parameters ()
    :precondition 
      (and
        (in r597)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r598)
        (f_copy)
        (not 
          (in r597))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r597
    :parameters ()
    :precondition 
      (and
        (in r597)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r596)
        (f_copy)
        (not 
          (in r597))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r598
    :parameters ()
    :precondition 
      (and
        (in r598)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r599)
        (f_copy)
        (not 
          (in r598))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r598
    :parameters ()
    :precondition 
      (and
        (in r598)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r597)
        (f_copy)
        (not 
          (in r598))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r599
    :parameters ()
    :precondition 
      (and
        (in r599)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r600)
        (f_copy)
        (not 
          (in r599))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r599
    :parameters ()
    :precondition 
      (and
        (in r599)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r598)
        (f_copy)
        (not 
          (in r599))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r600
    :parameters ()
    :precondition 
      (and
        (in r600)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r601)
        (f_copy)
        (not 
          (in r600))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r600
    :parameters ()
    :precondition 
      (and
        (in r600)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r599)
        (f_copy)
        (not 
          (in r600))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r601
    :parameters ()
    :precondition 
      (and
        (in r601)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r602)
        (f_copy)
        (not 
          (in r601))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r601
    :parameters ()
    :precondition 
      (and
        (in r601)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r600)
        (f_copy)
        (not 
          (in r601))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r602
    :parameters ()
    :precondition 
      (and
        (in r602)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r603)
        (f_copy)
        (not 
          (in r602))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r602
    :parameters ()
    :precondition 
      (and
        (in r602)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r601)
        (f_copy)
        (not 
          (in r602))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r603
    :parameters ()
    :precondition 
      (and
        (in r603)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r604)
        (f_copy)
        (not 
          (in r603))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r603
    :parameters ()
    :precondition 
      (and
        (in r603)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r602)
        (f_copy)
        (not 
          (in r603))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r604
    :parameters ()
    :precondition 
      (and
        (in r604)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r605)
        (f_copy)
        (not 
          (in r604))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r604
    :parameters ()
    :precondition 
      (and
        (in r604)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r603)
        (f_copy)
        (not 
          (in r604))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r605
    :parameters ()
    :precondition 
      (and
        (in r605)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r606)
        (f_copy)
        (not 
          (in r605))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r605
    :parameters ()
    :precondition 
      (and
        (in r605)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r604)
        (f_copy)
        (not 
          (in r605))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r606
    :parameters ()
    :precondition 
      (and
        (in r606)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r607)
        (f_copy)
        (not 
          (in r606))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r606
    :parameters ()
    :precondition 
      (and
        (in r606)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r605)
        (f_copy)
        (not 
          (in r606))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r607
    :parameters ()
    :precondition 
      (and
        (in r607)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r608)
        (f_copy)
        (not 
          (in r607))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r607
    :parameters ()
    :precondition 
      (and
        (in r607)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r606)
        (f_copy)
        (not 
          (in r607))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r608
    :parameters ()
    :precondition 
      (and
        (in r608)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r609)
        (f_copy)
        (not 
          (in r608))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r608
    :parameters ()
    :precondition 
      (and
        (in r608)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r607)
        (f_copy)
        (not 
          (in r608))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r609
    :parameters ()
    :precondition 
      (and
        (in r609)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r610)
        (f_copy)
        (not 
          (in r609))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r609
    :parameters ()
    :precondition 
      (and
        (in r609)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r608)
        (f_copy)
        (not 
          (in r609))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r610
    :parameters ()
    :precondition 
      (and
        (in r610)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r611)
        (f_copy)
        (not 
          (in r610))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r610
    :parameters ()
    :precondition 
      (and
        (in r610)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r609)
        (f_copy)
        (not 
          (in r610))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r611
    :parameters ()
    :precondition 
      (and
        (in r611)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r612)
        (f_copy)
        (not 
          (in r611))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r611
    :parameters ()
    :precondition 
      (and
        (in r611)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r610)
        (f_copy)
        (not 
          (in r611))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r612
    :parameters ()
    :precondition 
      (and
        (in r612)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r613)
        (f_copy)
        (not 
          (in r612))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r612
    :parameters ()
    :precondition 
      (and
        (in r612)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r611)
        (f_copy)
        (not 
          (in r612))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r613
    :parameters ()
    :precondition 
      (and
        (in r613)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r614)
        (f_copy)
        (not 
          (in r613))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r613
    :parameters ()
    :precondition 
      (and
        (in r613)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r612)
        (f_copy)
        (not 
          (in r613))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r614
    :parameters ()
    :precondition 
      (and
        (in r614)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r615)
        (f_copy)
        (not 
          (in r614))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r614
    :parameters ()
    :precondition 
      (and
        (in r614)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r613)
        (f_copy)
        (not 
          (in r614))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r615
    :parameters ()
    :precondition 
      (and
        (in r615)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r616)
        (f_copy)
        (not 
          (in r615))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r615
    :parameters ()
    :precondition 
      (and
        (in r615)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r614)
        (f_copy)
        (not 
          (in r615))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r616
    :parameters ()
    :precondition 
      (and
        (in r616)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r617)
        (f_copy)
        (not 
          (in r616))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r616
    :parameters ()
    :precondition 
      (and
        (in r616)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r615)
        (f_copy)
        (not 
          (in r616))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r617
    :parameters ()
    :precondition 
      (and
        (in r617)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r618)
        (f_copy)
        (not 
          (in r617))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r617
    :parameters ()
    :precondition 
      (and
        (in r617)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r616)
        (f_copy)
        (not 
          (in r617))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r618
    :parameters ()
    :precondition 
      (and
        (in r618)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r619)
        (f_copy)
        (not 
          (in r618))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r618
    :parameters ()
    :precondition 
      (and
        (in r618)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r617)
        (f_copy)
        (not 
          (in r618))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r619
    :parameters ()
    :precondition 
      (and
        (in r619)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r620)
        (f_copy)
        (not 
          (in r619))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r619
    :parameters ()
    :precondition 
      (and
        (in r619)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r618)
        (f_copy)
        (not 
          (in r619))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r620
    :parameters ()
    :precondition 
      (and
        (in r620)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r621)
        (f_copy)
        (not 
          (in r620))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r620
    :parameters ()
    :precondition 
      (and
        (in r620)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r619)
        (f_copy)
        (not 
          (in r620))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r621
    :parameters ()
    :precondition 
      (and
        (in r621)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r622)
        (f_copy)
        (not 
          (in r621))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r621
    :parameters ()
    :precondition 
      (and
        (in r621)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r620)
        (f_copy)
        (not 
          (in r621))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r622
    :parameters ()
    :precondition 
      (and
        (in r622)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r623)
        (f_copy)
        (not 
          (in r622))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r622
    :parameters ()
    :precondition 
      (and
        (in r622)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r621)
        (f_copy)
        (not 
          (in r622))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r623
    :parameters ()
    :precondition 
      (and
        (in r623)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r624)
        (f_copy)
        (not 
          (in r623))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r623
    :parameters ()
    :precondition 
      (and
        (in r623)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r622)
        (f_copy)
        (not 
          (in r623))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r624
    :parameters ()
    :precondition 
      (and
        (in r624)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r625)
        (f_copy)
        (not 
          (in r624))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r624
    :parameters ()
    :precondition 
      (and
        (in r624)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r623)
        (f_copy)
        (not 
          (in r624))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r625
    :parameters ()
    :precondition 
      (and
        (in r625)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r626)
        (f_copy)
        (not 
          (in r625))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r625
    :parameters ()
    :precondition 
      (and
        (in r625)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r624)
        (f_copy)
        (not 
          (in r625))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r626
    :parameters ()
    :precondition 
      (and
        (in r626)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r627)
        (f_copy)
        (not 
          (in r626))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r626
    :parameters ()
    :precondition 
      (and
        (in r626)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r625)
        (f_copy)
        (not 
          (in r626))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r627
    :parameters ()
    :precondition 
      (and
        (in r627)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r628)
        (f_copy)
        (not 
          (in r627))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r627
    :parameters ()
    :precondition 
      (and
        (in r627)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r626)
        (f_copy)
        (not 
          (in r627))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r628
    :parameters ()
    :precondition 
      (and
        (in r628)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r629)
        (f_copy)
        (not 
          (in r628))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r628
    :parameters ()
    :precondition 
      (and
        (in r628)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r627)
        (f_copy)
        (not 
          (in r628))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r629
    :parameters ()
    :precondition 
      (and
        (in r629)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r630)
        (f_copy)
        (not 
          (in r629))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r629
    :parameters ()
    :precondition 
      (and
        (in r629)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r628)
        (f_copy)
        (not 
          (in r629))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r630
    :parameters ()
    :precondition 
      (and
        (in r630)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r631)
        (f_copy)
        (not 
          (in r630))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r630
    :parameters ()
    :precondition 
      (and
        (in r630)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r629)
        (f_copy)
        (not 
          (in r630))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r631
    :parameters ()
    :precondition 
      (and
        (in r631)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r632)
        (f_copy)
        (not 
          (in r631))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r631
    :parameters ()
    :precondition 
      (and
        (in r631)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r630)
        (f_copy)
        (not 
          (in r631))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r632
    :parameters ()
    :precondition 
      (and
        (in r632)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r633)
        (f_copy)
        (not 
          (in r632))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r632
    :parameters ()
    :precondition 
      (and
        (in r632)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r631)
        (f_copy)
        (not 
          (in r632))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r633
    :parameters ()
    :precondition 
      (and
        (in r633)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r634)
        (f_copy)
        (not 
          (in r633))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r633
    :parameters ()
    :precondition 
      (and
        (in r633)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r632)
        (f_copy)
        (not 
          (in r633))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r634
    :parameters ()
    :precondition 
      (and
        (in r634)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r635)
        (f_copy)
        (not 
          (in r634))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r634
    :parameters ()
    :precondition 
      (and
        (in r634)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r633)
        (f_copy)
        (not 
          (in r634))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r635
    :parameters ()
    :precondition 
      (and
        (in r635)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r636)
        (f_copy)
        (not 
          (in r635))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r635
    :parameters ()
    :precondition 
      (and
        (in r635)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r634)
        (f_copy)
        (not 
          (in r635))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r636
    :parameters ()
    :precondition 
      (and
        (in r636)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r637)
        (f_copy)
        (not 
          (in r636))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r636
    :parameters ()
    :precondition 
      (and
        (in r636)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r635)
        (f_copy)
        (not 
          (in r636))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r637
    :parameters ()
    :precondition 
      (and
        (in r637)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r638)
        (f_copy)
        (not 
          (in r637))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r637
    :parameters ()
    :precondition 
      (and
        (in r637)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r636)
        (f_copy)
        (not 
          (in r637))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r638
    :parameters ()
    :precondition 
      (and
        (in r638)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r639)
        (f_copy)
        (not 
          (in r638))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r638
    :parameters ()
    :precondition 
      (and
        (in r638)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r637)
        (f_copy)
        (not 
          (in r638))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r639
    :parameters ()
    :precondition 
      (and
        (in r639)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r640)
        (f_copy)
        (not 
          (in r639))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r639
    :parameters ()
    :precondition 
      (and
        (in r639)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r638)
        (f_copy)
        (not 
          (in r639))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r640
    :parameters ()
    :precondition 
      (and
        (in r640)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r641)
        (f_copy)
        (not 
          (in r640))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r640
    :parameters ()
    :precondition 
      (and
        (in r640)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r639)
        (f_copy)
        (not 
          (in r640))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r641
    :parameters ()
    :precondition 
      (and
        (in r641)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r642)
        (f_copy)
        (not 
          (in r641))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r641
    :parameters ()
    :precondition 
      (and
        (in r641)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r640)
        (f_copy)
        (not 
          (in r641))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r642
    :parameters ()
    :precondition 
      (and
        (in r642)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r643)
        (f_copy)
        (not 
          (in r642))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r642
    :parameters ()
    :precondition 
      (and
        (in r642)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r641)
        (f_copy)
        (not 
          (in r642))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r643
    :parameters ()
    :precondition 
      (and
        (in r643)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r644)
        (f_copy)
        (not 
          (in r643))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r643
    :parameters ()
    :precondition 
      (and
        (in r643)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r642)
        (f_copy)
        (not 
          (in r643))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r644
    :parameters ()
    :precondition 
      (and
        (in r644)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r645)
        (f_copy)
        (not 
          (in r644))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r644
    :parameters ()
    :precondition 
      (and
        (in r644)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r643)
        (f_copy)
        (not 
          (in r644))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r645
    :parameters ()
    :precondition 
      (and
        (in r645)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r646)
        (f_copy)
        (not 
          (in r645))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r645
    :parameters ()
    :precondition 
      (and
        (in r645)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r644)
        (f_copy)
        (not 
          (in r645))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r646
    :parameters ()
    :precondition 
      (and
        (in r646)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r647)
        (f_copy)
        (not 
          (in r646))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r646
    :parameters ()
    :precondition 
      (and
        (in r646)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r645)
        (f_copy)
        (not 
          (in r646))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r647
    :parameters ()
    :precondition 
      (and
        (in r647)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r648)
        (f_copy)
        (not 
          (in r647))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r647
    :parameters ()
    :precondition 
      (and
        (in r647)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r646)
        (f_copy)
        (not 
          (in r647))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r648
    :parameters ()
    :precondition 
      (and
        (in r648)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r649)
        (f_copy)
        (not 
          (in r648))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r648
    :parameters ()
    :precondition 
      (and
        (in r648)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r647)
        (f_copy)
        (not 
          (in r648))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r649
    :parameters ()
    :precondition 
      (and
        (in r649)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r650)
        (f_copy)
        (not 
          (in r649))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r649
    :parameters ()
    :precondition 
      (and
        (in r649)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r648)
        (f_copy)
        (not 
          (in r649))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r650
    :parameters ()
    :precondition 
      (and
        (in r650)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r651)
        (f_copy)
        (not 
          (in r650))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r650
    :parameters ()
    :precondition 
      (and
        (in r650)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r649)
        (f_copy)
        (not 
          (in r650))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r651
    :parameters ()
    :precondition 
      (and
        (in r651)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r652)
        (f_copy)
        (not 
          (in r651))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r651
    :parameters ()
    :precondition 
      (and
        (in r651)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r650)
        (f_copy)
        (not 
          (in r651))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r652
    :parameters ()
    :precondition 
      (and
        (in r652)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r653)
        (f_copy)
        (not 
          (in r652))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r652
    :parameters ()
    :precondition 
      (and
        (in r652)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r651)
        (f_copy)
        (not 
          (in r652))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r653
    :parameters ()
    :precondition 
      (and
        (in r653)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r654)
        (f_copy)
        (not 
          (in r653))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r653
    :parameters ()
    :precondition 
      (and
        (in r653)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r652)
        (f_copy)
        (not 
          (in r653))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r654
    :parameters ()
    :precondition 
      (and
        (in r654)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r655)
        (f_copy)
        (not 
          (in r654))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r654
    :parameters ()
    :precondition 
      (and
        (in r654)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r653)
        (f_copy)
        (not 
          (in r654))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r655
    :parameters ()
    :precondition 
      (and
        (in r655)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r656)
        (f_copy)
        (not 
          (in r655))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r655
    :parameters ()
    :precondition 
      (and
        (in r655)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r654)
        (f_copy)
        (not 
          (in r655))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r656
    :parameters ()
    :precondition 
      (and
        (in r656)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r657)
        (f_copy)
        (not 
          (in r656))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r656
    :parameters ()
    :precondition 
      (and
        (in r656)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r655)
        (f_copy)
        (not 
          (in r656))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r657
    :parameters ()
    :precondition 
      (and
        (in r657)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r658)
        (f_copy)
        (not 
          (in r657))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r657
    :parameters ()
    :precondition 
      (and
        (in r657)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r656)
        (f_copy)
        (not 
          (in r657))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r658
    :parameters ()
    :precondition 
      (and
        (in r658)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r659)
        (f_copy)
        (not 
          (in r658))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r658
    :parameters ()
    :precondition 
      (and
        (in r658)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r657)
        (f_copy)
        (not 
          (in r658))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r659
    :parameters ()
    :precondition 
      (and
        (in r659)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r660)
        (f_copy)
        (not 
          (in r659))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r659
    :parameters ()
    :precondition 
      (and
        (in r659)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r658)
        (f_copy)
        (not 
          (in r659))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r660
    :parameters ()
    :precondition 
      (and
        (in r660)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r661)
        (f_copy)
        (not 
          (in r660))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r660
    :parameters ()
    :precondition 
      (and
        (in r660)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r659)
        (f_copy)
        (not 
          (in r660))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r661
    :parameters ()
    :precondition 
      (and
        (in r661)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r662)
        (f_copy)
        (not 
          (in r661))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r661
    :parameters ()
    :precondition 
      (and
        (in r661)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r660)
        (f_copy)
        (not 
          (in r661))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r662
    :parameters ()
    :precondition 
      (and
        (in r662)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r663)
        (f_copy)
        (not 
          (in r662))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r662
    :parameters ()
    :precondition 
      (and
        (in r662)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r661)
        (f_copy)
        (not 
          (in r662))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r663
    :parameters ()
    :precondition 
      (and
        (in r663)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r664)
        (f_copy)
        (not 
          (in r663))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r663
    :parameters ()
    :precondition 
      (and
        (in r663)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r662)
        (f_copy)
        (not 
          (in r663))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r664
    :parameters ()
    :precondition 
      (and
        (in r664)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r665)
        (f_copy)
        (not 
          (in r664))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r664
    :parameters ()
    :precondition 
      (and
        (in r664)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r663)
        (f_copy)
        (not 
          (in r664))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r665
    :parameters ()
    :precondition 
      (and
        (in r665)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r666)
        (f_copy)
        (not 
          (in r665))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r665
    :parameters ()
    :precondition 
      (and
        (in r665)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r664)
        (f_copy)
        (not 
          (in r665))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r666
    :parameters ()
    :precondition 
      (and
        (in r666)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r667)
        (f_copy)
        (not 
          (in r666))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r666
    :parameters ()
    :precondition 
      (and
        (in r666)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r665)
        (f_copy)
        (not 
          (in r666))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r667
    :parameters ()
    :precondition 
      (and
        (in r667)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r668)
        (f_copy)
        (not 
          (in r667))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r667
    :parameters ()
    :precondition 
      (and
        (in r667)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r666)
        (f_copy)
        (not 
          (in r667))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r668
    :parameters ()
    :precondition 
      (and
        (in r668)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r669)
        (f_copy)
        (not 
          (in r668))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r668
    :parameters ()
    :precondition 
      (and
        (in r668)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r667)
        (f_copy)
        (not 
          (in r668))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r669
    :parameters ()
    :precondition 
      (and
        (in r669)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r670)
        (f_copy)
        (not 
          (in r669))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r669
    :parameters ()
    :precondition 
      (and
        (in r669)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r668)
        (f_copy)
        (not 
          (in r669))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r670
    :parameters ()
    :precondition 
      (and
        (in r670)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r671)
        (f_copy)
        (not 
          (in r670))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r670
    :parameters ()
    :precondition 
      (and
        (in r670)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r669)
        (f_copy)
        (not 
          (in r670))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r671
    :parameters ()
    :precondition 
      (and
        (in r671)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r672)
        (f_copy)
        (not 
          (in r671))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r671
    :parameters ()
    :precondition 
      (and
        (in r671)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r670)
        (f_copy)
        (not 
          (in r671))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r672
    :parameters ()
    :precondition 
      (and
        (in r672)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r673)
        (f_copy)
        (not 
          (in r672))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r672
    :parameters ()
    :precondition 
      (and
        (in r672)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r671)
        (f_copy)
        (not 
          (in r672))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r673
    :parameters ()
    :precondition 
      (and
        (in r673)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r674)
        (f_copy)
        (not 
          (in r673))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r673
    :parameters ()
    :precondition 
      (and
        (in r673)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r672)
        (f_copy)
        (not 
          (in r673))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r674
    :parameters ()
    :precondition 
      (and
        (in r674)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r675)
        (f_copy)
        (not 
          (in r674))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r674
    :parameters ()
    :precondition 
      (and
        (in r674)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r673)
        (f_copy)
        (not 
          (in r674))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r675
    :parameters ()
    :precondition 
      (and
        (in r675)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r676)
        (f_copy)
        (not 
          (in r675))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r675
    :parameters ()
    :precondition 
      (and
        (in r675)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r674)
        (f_copy)
        (not 
          (in r675))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r676
    :parameters ()
    :precondition 
      (and
        (in r676)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r677)
        (f_copy)
        (not 
          (in r676))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r676
    :parameters ()
    :precondition 
      (and
        (in r676)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r675)
        (f_copy)
        (not 
          (in r676))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r677
    :parameters ()
    :precondition 
      (and
        (in r677)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r678)
        (f_copy)
        (not 
          (in r677))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r677
    :parameters ()
    :precondition 
      (and
        (in r677)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r676)
        (f_copy)
        (not 
          (in r677))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r678
    :parameters ()
    :precondition 
      (and
        (in r678)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r679)
        (f_copy)
        (not 
          (in r678))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r678
    :parameters ()
    :precondition 
      (and
        (in r678)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r677)
        (f_copy)
        (not 
          (in r678))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r679
    :parameters ()
    :precondition 
      (and
        (in r679)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r680)
        (f_copy)
        (not 
          (in r679))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r679
    :parameters ()
    :precondition 
      (and
        (in r679)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r678)
        (f_copy)
        (not 
          (in r679))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r680
    :parameters ()
    :precondition 
      (and
        (in r680)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r681)
        (f_copy)
        (not 
          (in r680))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r680
    :parameters ()
    :precondition 
      (and
        (in r680)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r679)
        (f_copy)
        (not 
          (in r680))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r681
    :parameters ()
    :precondition 
      (and
        (in r681)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r682)
        (f_copy)
        (not 
          (in r681))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r681
    :parameters ()
    :precondition 
      (and
        (in r681)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r680)
        (f_copy)
        (not 
          (in r681))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r682
    :parameters ()
    :precondition 
      (and
        (in r682)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r683)
        (f_copy)
        (not 
          (in r682))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r682
    :parameters ()
    :precondition 
      (and
        (in r682)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r681)
        (f_copy)
        (not 
          (in r682))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r683
    :parameters ()
    :precondition 
      (and
        (in r683)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r684)
        (f_copy)
        (not 
          (in r683))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r683
    :parameters ()
    :precondition 
      (and
        (in r683)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r682)
        (f_copy)
        (not 
          (in r683))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r684
    :parameters ()
    :precondition 
      (and
        (in r684)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r685)
        (f_copy)
        (not 
          (in r684))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r684
    :parameters ()
    :precondition 
      (and
        (in r684)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r683)
        (f_copy)
        (not 
          (in r684))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r685
    :parameters ()
    :precondition 
      (and
        (in r685)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r686)
        (f_copy)
        (not 
          (in r685))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r685
    :parameters ()
    :precondition 
      (and
        (in r685)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r684)
        (f_copy)
        (not 
          (in r685))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r686
    :parameters ()
    :precondition 
      (and
        (in r686)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r687)
        (f_copy)
        (not 
          (in r686))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r686
    :parameters ()
    :precondition 
      (and
        (in r686)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r685)
        (f_copy)
        (not 
          (in r686))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r687
    :parameters ()
    :precondition 
      (and
        (in r687)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r688)
        (f_copy)
        (not 
          (in r687))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r687
    :parameters ()
    :precondition 
      (and
        (in r687)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r686)
        (f_copy)
        (not 
          (in r687))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r688
    :parameters ()
    :precondition 
      (and
        (in r688)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r689)
        (f_copy)
        (not 
          (in r688))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r688
    :parameters ()
    :precondition 
      (and
        (in r688)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r687)
        (f_copy)
        (not 
          (in r688))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r689
    :parameters ()
    :precondition 
      (and
        (in r689)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r690)
        (f_copy)
        (not 
          (in r689))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r689
    :parameters ()
    :precondition 
      (and
        (in r689)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r688)
        (f_copy)
        (not 
          (in r689))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r690
    :parameters ()
    :precondition 
      (and
        (in r690)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r691)
        (f_copy)
        (not 
          (in r690))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r690
    :parameters ()
    :precondition 
      (and
        (in r690)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r689)
        (f_copy)
        (not 
          (in r690))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r691
    :parameters ()
    :precondition 
      (and
        (in r691)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r692)
        (f_copy)
        (not 
          (in r691))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r691
    :parameters ()
    :precondition 
      (and
        (in r691)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r690)
        (f_copy)
        (not 
          (in r691))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r692
    :parameters ()
    :precondition 
      (and
        (in r692)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r693)
        (f_copy)
        (not 
          (in r692))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r692
    :parameters ()
    :precondition 
      (and
        (in r692)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r691)
        (f_copy)
        (not 
          (in r692))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r693
    :parameters ()
    :precondition 
      (and
        (in r693)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r694)
        (f_copy)
        (not 
          (in r693))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r693
    :parameters ()
    :precondition 
      (and
        (in r693)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r692)
        (f_copy)
        (not 
          (in r693))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r694
    :parameters ()
    :precondition 
      (and
        (in r694)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r695)
        (f_copy)
        (not 
          (in r694))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r694
    :parameters ()
    :precondition 
      (and
        (in r694)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r693)
        (f_copy)
        (not 
          (in r694))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r695
    :parameters ()
    :precondition 
      (and
        (in r695)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r696)
        (f_copy)
        (not 
          (in r695))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r695
    :parameters ()
    :precondition 
      (and
        (in r695)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r694)
        (f_copy)
        (not 
          (in r695))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r696
    :parameters ()
    :precondition 
      (and
        (in r696)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r697)
        (f_copy)
        (not 
          (in r696))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r696
    :parameters ()
    :precondition 
      (and
        (in r696)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r695)
        (f_copy)
        (not 
          (in r696))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r697
    :parameters ()
    :precondition 
      (and
        (in r697)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r698)
        (f_copy)
        (not 
          (in r697))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r697
    :parameters ()
    :precondition 
      (and
        (in r697)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r696)
        (f_copy)
        (not 
          (in r697))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r698
    :parameters ()
    :precondition 
      (and
        (in r698)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r699)
        (f_copy)
        (not 
          (in r698))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r698
    :parameters ()
    :precondition 
      (and
        (in r698)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r697)
        (f_copy)
        (not 
          (in r698))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r699
    :parameters ()
    :precondition 
      (and
        (in r699)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r700)
        (f_copy)
        (not 
          (in r699))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r699
    :parameters ()
    :precondition 
      (and
        (in r699)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r698)
        (f_copy)
        (not 
          (in r699))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r700
    :parameters ()
    :precondition 
      (and
        (in r700)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r701)
        (f_copy)
        (not 
          (in r700))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r700
    :parameters ()
    :precondition 
      (and
        (in r700)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r699)
        (f_copy)
        (not 
          (in r700))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r701
    :parameters ()
    :precondition 
      (and
        (in r701)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r702)
        (f_copy)
        (not 
          (in r701))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r701
    :parameters ()
    :precondition 
      (and
        (in r701)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r700)
        (f_copy)
        (not 
          (in r701))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r702
    :parameters ()
    :precondition 
      (and
        (in r702)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r703)
        (f_copy)
        (not 
          (in r702))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r702
    :parameters ()
    :precondition 
      (and
        (in r702)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r701)
        (f_copy)
        (not 
          (in r702))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r703
    :parameters ()
    :precondition 
      (and
        (in r703)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r704)
        (f_copy)
        (not 
          (in r703))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r703
    :parameters ()
    :precondition 
      (and
        (in r703)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r702)
        (f_copy)
        (not 
          (in r703))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r704
    :parameters ()
    :precondition 
      (and
        (in r704)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r705)
        (f_copy)
        (not 
          (in r704))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r704
    :parameters ()
    :precondition 
      (and
        (in r704)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r703)
        (f_copy)
        (not 
          (in r704))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r705
    :parameters ()
    :precondition 
      (and
        (in r705)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r706)
        (f_copy)
        (not 
          (in r705))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r705
    :parameters ()
    :precondition 
      (and
        (in r705)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r704)
        (f_copy)
        (not 
          (in r705))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r706
    :parameters ()
    :precondition 
      (and
        (in r706)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r707)
        (f_copy)
        (not 
          (in r706))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r706
    :parameters ()
    :precondition 
      (and
        (in r706)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r705)
        (f_copy)
        (not 
          (in r706))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r707
    :parameters ()
    :precondition 
      (and
        (in r707)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r708)
        (f_copy)
        (not 
          (in r707))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r707
    :parameters ()
    :precondition 
      (and
        (in r707)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r706)
        (f_copy)
        (not 
          (in r707))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r708
    :parameters ()
    :precondition 
      (and
        (in r708)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r709)
        (f_copy)
        (not 
          (in r708))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r708
    :parameters ()
    :precondition 
      (and
        (in r708)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r707)
        (f_copy)
        (not 
          (in r708))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r709
    :parameters ()
    :precondition 
      (and
        (in r709)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r710)
        (f_copy)
        (not 
          (in r709))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r709
    :parameters ()
    :precondition 
      (and
        (in r709)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r708)
        (f_copy)
        (not 
          (in r709))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r710
    :parameters ()
    :precondition 
      (and
        (in r710)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r711)
        (f_copy)
        (not 
          (in r710))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r710
    :parameters ()
    :precondition 
      (and
        (in r710)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r709)
        (f_copy)
        (not 
          (in r710))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r711
    :parameters ()
    :precondition 
      (and
        (in r711)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r712)
        (f_copy)
        (not 
          (in r711))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r711
    :parameters ()
    :precondition 
      (and
        (in r711)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r710)
        (f_copy)
        (not 
          (in r711))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r712
    :parameters ()
    :precondition 
      (and
        (in r712)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r713)
        (f_copy)
        (not 
          (in r712))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r712
    :parameters ()
    :precondition 
      (and
        (in r712)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r711)
        (f_copy)
        (not 
          (in r712))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r713
    :parameters ()
    :precondition 
      (and
        (in r713)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r714)
        (f_copy)
        (not 
          (in r713))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r713
    :parameters ()
    :precondition 
      (and
        (in r713)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r712)
        (f_copy)
        (not 
          (in r713))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r714
    :parameters ()
    :precondition 
      (and
        (in r714)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r715)
        (f_copy)
        (not 
          (in r714))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r714
    :parameters ()
    :precondition 
      (and
        (in r714)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r713)
        (f_copy)
        (not 
          (in r714))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r715
    :parameters ()
    :precondition 
      (and
        (in r715)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r716)
        (f_copy)
        (not 
          (in r715))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r715
    :parameters ()
    :precondition 
      (and
        (in r715)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r714)
        (f_copy)
        (not 
          (in r715))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r716
    :parameters ()
    :precondition 
      (and
        (in r716)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r717)
        (f_copy)
        (not 
          (in r716))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r716
    :parameters ()
    :precondition 
      (and
        (in r716)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r715)
        (f_copy)
        (not 
          (in r716))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r717
    :parameters ()
    :precondition 
      (and
        (in r717)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r718)
        (f_copy)
        (not 
          (in r717))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r717
    :parameters ()
    :precondition 
      (and
        (in r717)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r716)
        (f_copy)
        (not 
          (in r717))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r718
    :parameters ()
    :precondition 
      (and
        (in r718)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r719)
        (f_copy)
        (not 
          (in r718))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r718
    :parameters ()
    :precondition 
      (and
        (in r718)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r717)
        (f_copy)
        (not 
          (in r718))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r719
    :parameters ()
    :precondition 
      (and
        (in r719)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r720)
        (f_copy)
        (not 
          (in r719))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r719
    :parameters ()
    :precondition 
      (and
        (in r719)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r718)
        (f_copy)
        (not 
          (in r719))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r720
    :parameters ()
    :precondition 
      (and
        (in r720)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r721)
        (f_copy)
        (not 
          (in r720))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r720
    :parameters ()
    :precondition 
      (and
        (in r720)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r719)
        (f_copy)
        (not 
          (in r720))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r721
    :parameters ()
    :precondition 
      (and
        (in r721)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r722)
        (f_copy)
        (not 
          (in r721))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r721
    :parameters ()
    :precondition 
      (and
        (in r721)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r720)
        (f_copy)
        (not 
          (in r721))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r722
    :parameters ()
    :precondition 
      (and
        (in r722)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r723)
        (f_copy)
        (not 
          (in r722))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r722
    :parameters ()
    :precondition 
      (and
        (in r722)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r721)
        (f_copy)
        (not 
          (in r722))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r723
    :parameters ()
    :precondition 
      (and
        (in r723)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r724)
        (f_copy)
        (not 
          (in r723))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r723
    :parameters ()
    :precondition 
      (and
        (in r723)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r722)
        (f_copy)
        (not 
          (in r723))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r724
    :parameters ()
    :precondition 
      (and
        (in r724)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r725)
        (f_copy)
        (not 
          (in r724))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r724
    :parameters ()
    :precondition 
      (and
        (in r724)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r723)
        (f_copy)
        (not 
          (in r724))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r725
    :parameters ()
    :precondition 
      (and
        (in r725)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r726)
        (f_copy)
        (not 
          (in r725))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r725
    :parameters ()
    :precondition 
      (and
        (in r725)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r724)
        (f_copy)
        (not 
          (in r725))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r726
    :parameters ()
    :precondition 
      (and
        (in r726)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r727)
        (f_copy)
        (not 
          (in r726))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r726
    :parameters ()
    :precondition 
      (and
        (in r726)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r725)
        (f_copy)
        (not 
          (in r726))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r727
    :parameters ()
    :precondition 
      (and
        (in r727)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r728)
        (f_copy)
        (not 
          (in r727))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r727
    :parameters ()
    :precondition 
      (and
        (in r727)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r726)
        (f_copy)
        (not 
          (in r727))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r728
    :parameters ()
    :precondition 
      (and
        (in r728)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r729)
        (f_copy)
        (not 
          (in r728))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r728
    :parameters ()
    :precondition 
      (and
        (in r728)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r727)
        (f_copy)
        (not 
          (in r728))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r729
    :parameters ()
    :precondition 
      (and
        (in r729)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r730)
        (f_copy)
        (not 
          (in r729))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r729
    :parameters ()
    :precondition 
      (and
        (in r729)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r728)
        (f_copy)
        (not 
          (in r729))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r730
    :parameters ()
    :precondition 
      (and
        (in r730)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r731)
        (f_copy)
        (not 
          (in r730))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r730
    :parameters ()
    :precondition 
      (and
        (in r730)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r729)
        (f_copy)
        (not 
          (in r730))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r731
    :parameters ()
    :precondition 
      (and
        (in r731)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r732)
        (f_copy)
        (not 
          (in r731))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r731
    :parameters ()
    :precondition 
      (and
        (in r731)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r730)
        (f_copy)
        (not 
          (in r731))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r732
    :parameters ()
    :precondition 
      (and
        (in r732)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r733)
        (f_copy)
        (not 
          (in r732))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r732
    :parameters ()
    :precondition 
      (and
        (in r732)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r731)
        (f_copy)
        (not 
          (in r732))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r733
    :parameters ()
    :precondition 
      (and
        (in r733)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r734)
        (f_copy)
        (not 
          (in r733))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r733
    :parameters ()
    :precondition 
      (and
        (in r733)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r732)
        (f_copy)
        (not 
          (in r733))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r734
    :parameters ()
    :precondition 
      (and
        (in r734)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r735)
        (f_copy)
        (not 
          (in r734))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r734
    :parameters ()
    :precondition 
      (and
        (in r734)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r733)
        (f_copy)
        (not 
          (in r734))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r735
    :parameters ()
    :precondition 
      (and
        (in r735)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r736)
        (f_copy)
        (not 
          (in r735))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r735
    :parameters ()
    :precondition 
      (and
        (in r735)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r734)
        (f_copy)
        (not 
          (in r735))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r736
    :parameters ()
    :precondition 
      (and
        (in r736)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r737)
        (f_copy)
        (not 
          (in r736))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r736
    :parameters ()
    :precondition 
      (and
        (in r736)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r735)
        (f_copy)
        (not 
          (in r736))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r737
    :parameters ()
    :precondition 
      (and
        (in r737)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r738)
        (f_copy)
        (not 
          (in r737))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r737
    :parameters ()
    :precondition 
      (and
        (in r737)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r736)
        (f_copy)
        (not 
          (in r737))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r738
    :parameters ()
    :precondition 
      (and
        (in r738)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r739)
        (f_copy)
        (not 
          (in r738))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r738
    :parameters ()
    :precondition 
      (and
        (in r738)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r737)
        (f_copy)
        (not 
          (in r738))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r739
    :parameters ()
    :precondition 
      (and
        (in r739)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r740)
        (f_copy)
        (not 
          (in r739))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r739
    :parameters ()
    :precondition 
      (and
        (in r739)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r738)
        (f_copy)
        (not 
          (in r739))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r740
    :parameters ()
    :precondition 
      (and
        (in r740)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r741)
        (f_copy)
        (not 
          (in r740))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r740
    :parameters ()
    :precondition 
      (and
        (in r740)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r739)
        (f_copy)
        (not 
          (in r740))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r741
    :parameters ()
    :precondition 
      (and
        (in r741)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r742)
        (f_copy)
        (not 
          (in r741))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r741
    :parameters ()
    :precondition 
      (and
        (in r741)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r740)
        (f_copy)
        (not 
          (in r741))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r742
    :parameters ()
    :precondition 
      (and
        (in r742)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r743)
        (f_copy)
        (not 
          (in r742))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r742
    :parameters ()
    :precondition 
      (and
        (in r742)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r741)
        (f_copy)
        (not 
          (in r742))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r743
    :parameters ()
    :precondition 
      (and
        (in r743)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r744)
        (f_copy)
        (not 
          (in r743))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r743
    :parameters ()
    :precondition 
      (and
        (in r743)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r742)
        (f_copy)
        (not 
          (in r743))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r744
    :parameters ()
    :precondition 
      (and
        (in r744)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r745)
        (f_copy)
        (not 
          (in r744))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r744
    :parameters ()
    :precondition 
      (and
        (in r744)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r743)
        (f_copy)
        (not 
          (in r744))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r745
    :parameters ()
    :precondition 
      (and
        (in r745)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r746)
        (f_copy)
        (not 
          (in r745))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r745
    :parameters ()
    :precondition 
      (and
        (in r745)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r744)
        (f_copy)
        (not 
          (in r745))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r746
    :parameters ()
    :precondition 
      (and
        (in r746)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r747)
        (f_copy)
        (not 
          (in r746))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r746
    :parameters ()
    :precondition 
      (and
        (in r746)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r745)
        (f_copy)
        (not 
          (in r746))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r747
    :parameters ()
    :precondition 
      (and
        (in r747)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r748)
        (f_copy)
        (not 
          (in r747))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r747
    :parameters ()
    :precondition 
      (and
        (in r747)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r746)
        (f_copy)
        (not 
          (in r747))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r748
    :parameters ()
    :precondition 
      (and
        (in r748)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r749)
        (f_copy)
        (not 
          (in r748))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r748
    :parameters ()
    :precondition 
      (and
        (in r748)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r747)
        (f_copy)
        (not 
          (in r748))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r749
    :parameters ()
    :precondition 
      (and
        (in r749)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r750)
        (f_copy)
        (not 
          (in r749))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r749
    :parameters ()
    :precondition 
      (and
        (in r749)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r748)
        (f_copy)
        (not 
          (in r749))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r750
    :parameters ()
    :precondition 
      (and
        (in r750)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r751)
        (f_copy)
        (not 
          (in r750))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r750
    :parameters ()
    :precondition 
      (and
        (in r750)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r749)
        (f_copy)
        (not 
          (in r750))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r751
    :parameters ()
    :precondition 
      (and
        (in r751)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r752)
        (f_copy)
        (not 
          (in r751))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r751
    :parameters ()
    :precondition 
      (and
        (in r751)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r750)
        (f_copy)
        (not 
          (in r751))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r752
    :parameters ()
    :precondition 
      (and
        (in r752)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r753)
        (f_copy)
        (not 
          (in r752))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r752
    :parameters ()
    :precondition 
      (and
        (in r752)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r751)
        (f_copy)
        (not 
          (in r752))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r753
    :parameters ()
    :precondition 
      (and
        (in r753)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r754)
        (f_copy)
        (not 
          (in r753))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r753
    :parameters ()
    :precondition 
      (and
        (in r753)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r752)
        (f_copy)
        (not 
          (in r753))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r754
    :parameters ()
    :precondition 
      (and
        (in r754)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r755)
        (f_copy)
        (not 
          (in r754))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r754
    :parameters ()
    :precondition 
      (and
        (in r754)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r753)
        (f_copy)
        (not 
          (in r754))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r755
    :parameters ()
    :precondition 
      (and
        (in r755)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r756)
        (f_copy)
        (not 
          (in r755))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r755
    :parameters ()
    :precondition 
      (and
        (in r755)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r754)
        (f_copy)
        (not 
          (in r755))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r756
    :parameters ()
    :precondition 
      (and
        (in r756)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r757)
        (f_copy)
        (not 
          (in r756))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r756
    :parameters ()
    :precondition 
      (and
        (in r756)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r755)
        (f_copy)
        (not 
          (in r756))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r757
    :parameters ()
    :precondition 
      (and
        (in r757)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r758)
        (f_copy)
        (not 
          (in r757))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r757
    :parameters ()
    :precondition 
      (and
        (in r757)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r756)
        (f_copy)
        (not 
          (in r757))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r758
    :parameters ()
    :precondition 
      (and
        (in r758)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r759)
        (f_copy)
        (not 
          (in r758))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r758
    :parameters ()
    :precondition 
      (and
        (in r758)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r757)
        (f_copy)
        (not 
          (in r758))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r759
    :parameters ()
    :precondition 
      (and
        (in r759)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r760)
        (f_copy)
        (not 
          (in r759))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r759
    :parameters ()
    :precondition 
      (and
        (in r759)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r758)
        (f_copy)
        (not 
          (in r759))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r760
    :parameters ()
    :precondition 
      (and
        (in r760)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r761)
        (f_copy)
        (not 
          (in r760))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r760
    :parameters ()
    :precondition 
      (and
        (in r760)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r759)
        (f_copy)
        (not 
          (in r760))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r761
    :parameters ()
    :precondition 
      (and
        (in r761)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r762)
        (f_copy)
        (not 
          (in r761))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r761
    :parameters ()
    :precondition 
      (and
        (in r761)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r760)
        (f_copy)
        (not 
          (in r761))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r762
    :parameters ()
    :precondition 
      (and
        (in r762)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r763)
        (f_copy)
        (not 
          (in r762))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r762
    :parameters ()
    :precondition 
      (and
        (in r762)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r761)
        (f_copy)
        (not 
          (in r762))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r763
    :parameters ()
    :precondition 
      (and
        (in r763)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r764)
        (f_copy)
        (not 
          (in r763))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r763
    :parameters ()
    :precondition 
      (and
        (in r763)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r762)
        (f_copy)
        (not 
          (in r763))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r764
    :parameters ()
    :precondition 
      (and
        (in r764)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r765)
        (f_copy)
        (not 
          (in r764))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r764
    :parameters ()
    :precondition 
      (and
        (in r764)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r763)
        (f_copy)
        (not 
          (in r764))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r765
    :parameters ()
    :precondition 
      (and
        (in r765)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r766)
        (f_copy)
        (not 
          (in r765))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r765
    :parameters ()
    :precondition 
      (and
        (in r765)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r764)
        (f_copy)
        (not 
          (in r765))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r766
    :parameters ()
    :precondition 
      (and
        (in r766)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r767)
        (f_copy)
        (not 
          (in r766))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r766
    :parameters ()
    :precondition 
      (and
        (in r766)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r765)
        (f_copy)
        (not 
          (in r766))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r767
    :parameters ()
    :precondition 
      (and
        (in r767)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r768)
        (f_copy)
        (not 
          (in r767))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r767
    :parameters ()
    :precondition 
      (and
        (in r767)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r766)
        (f_copy)
        (not 
          (in r767))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r768
    :parameters ()
    :precondition 
      (and
        (in r768)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r769)
        (f_copy)
        (not 
          (in r768))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r768
    :parameters ()
    :precondition 
      (and
        (in r768)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r767)
        (f_copy)
        (not 
          (in r768))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r769
    :parameters ()
    :precondition 
      (and
        (in r769)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r770)
        (f_copy)
        (not 
          (in r769))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r769
    :parameters ()
    :precondition 
      (and
        (in r769)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r768)
        (f_copy)
        (not 
          (in r769))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r770
    :parameters ()
    :precondition 
      (and
        (in r770)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r771)
        (f_copy)
        (not 
          (in r770))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r770
    :parameters ()
    :precondition 
      (and
        (in r770)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r769)
        (f_copy)
        (not 
          (in r770))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r771
    :parameters ()
    :precondition 
      (and
        (in r771)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r772)
        (f_copy)
        (not 
          (in r771))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r771
    :parameters ()
    :precondition 
      (and
        (in r771)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r770)
        (f_copy)
        (not 
          (in r771))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r772
    :parameters ()
    :precondition 
      (and
        (in r772)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r773)
        (f_copy)
        (not 
          (in r772))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r772
    :parameters ()
    :precondition 
      (and
        (in r772)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r771)
        (f_copy)
        (not 
          (in r772))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r773
    :parameters ()
    :precondition 
      (and
        (in r773)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r774)
        (f_copy)
        (not 
          (in r773))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r773
    :parameters ()
    :precondition 
      (and
        (in r773)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r772)
        (f_copy)
        (not 
          (in r773))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r774
    :parameters ()
    :precondition 
      (and
        (in r774)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r775)
        (f_copy)
        (not 
          (in r774))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r774
    :parameters ()
    :precondition 
      (and
        (in r774)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r773)
        (f_copy)
        (not 
          (in r774))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r775
    :parameters ()
    :precondition 
      (and
        (in r775)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r776)
        (f_copy)
        (not 
          (in r775))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r775
    :parameters ()
    :precondition 
      (and
        (in r775)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r774)
        (f_copy)
        (not 
          (in r775))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r776
    :parameters ()
    :precondition 
      (and
        (in r776)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r777)
        (f_copy)
        (not 
          (in r776))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r776
    :parameters ()
    :precondition 
      (and
        (in r776)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r775)
        (f_copy)
        (not 
          (in r776))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r777
    :parameters ()
    :precondition 
      (and
        (in r777)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r778)
        (f_copy)
        (not 
          (in r777))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r777
    :parameters ()
    :precondition 
      (and
        (in r777)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r776)
        (f_copy)
        (not 
          (in r777))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r778
    :parameters ()
    :precondition 
      (and
        (in r778)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r779)
        (f_copy)
        (not 
          (in r778))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r778
    :parameters ()
    :precondition 
      (and
        (in r778)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r777)
        (f_copy)
        (not 
          (in r778))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r779
    :parameters ()
    :precondition 
      (and
        (in r779)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r780)
        (f_copy)
        (not 
          (in r779))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r779
    :parameters ()
    :precondition 
      (and
        (in r779)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r778)
        (f_copy)
        (not 
          (in r779))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r780
    :parameters ()
    :precondition 
      (and
        (in r780)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r781)
        (f_copy)
        (not 
          (in r780))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r780
    :parameters ()
    :precondition 
      (and
        (in r780)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r779)
        (f_copy)
        (not 
          (in r780))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r781
    :parameters ()
    :precondition 
      (and
        (in r781)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r782)
        (f_copy)
        (not 
          (in r781))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r781
    :parameters ()
    :precondition 
      (and
        (in r781)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r780)
        (f_copy)
        (not 
          (in r781))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r782
    :parameters ()
    :precondition 
      (and
        (in r782)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r783)
        (f_copy)
        (not 
          (in r782))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r782
    :parameters ()
    :precondition 
      (and
        (in r782)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r781)
        (f_copy)
        (not 
          (in r782))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r783
    :parameters ()
    :precondition 
      (and
        (in r783)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r784)
        (f_copy)
        (not 
          (in r783))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r783
    :parameters ()
    :precondition 
      (and
        (in r783)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r782)
        (f_copy)
        (not 
          (in r783))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r784
    :parameters ()
    :precondition 
      (and
        (in r784)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r785)
        (f_copy)
        (not 
          (in r784))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r784
    :parameters ()
    :precondition 
      (and
        (in r784)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r783)
        (f_copy)
        (not 
          (in r784))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r785
    :parameters ()
    :precondition 
      (and
        (in r785)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r786)
        (f_copy)
        (not 
          (in r785))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r785
    :parameters ()
    :precondition 
      (and
        (in r785)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r784)
        (f_copy)
        (not 
          (in r785))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r786
    :parameters ()
    :precondition 
      (and
        (in r786)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r787)
        (f_copy)
        (not 
          (in r786))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r786
    :parameters ()
    :precondition 
      (and
        (in r786)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r785)
        (f_copy)
        (not 
          (in r786))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r787
    :parameters ()
    :precondition 
      (and
        (in r787)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r788)
        (f_copy)
        (not 
          (in r787))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r787
    :parameters ()
    :precondition 
      (and
        (in r787)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r786)
        (f_copy)
        (not 
          (in r787))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r788
    :parameters ()
    :precondition 
      (and
        (in r788)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r789)
        (f_copy)
        (not 
          (in r788))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r788
    :parameters ()
    :precondition 
      (and
        (in r788)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r787)
        (f_copy)
        (not 
          (in r788))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r789
    :parameters ()
    :precondition 
      (and
        (in r789)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r790)
        (f_copy)
        (not 
          (in r789))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r789
    :parameters ()
    :precondition 
      (and
        (in r789)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r788)
        (f_copy)
        (not 
          (in r789))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r790
    :parameters ()
    :precondition 
      (and
        (in r790)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r791)
        (f_copy)
        (not 
          (in r790))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r790
    :parameters ()
    :precondition 
      (and
        (in r790)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r789)
        (f_copy)
        (not 
          (in r790))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r791
    :parameters ()
    :precondition 
      (and
        (in r791)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r792)
        (f_copy)
        (not 
          (in r791))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r791
    :parameters ()
    :precondition 
      (and
        (in r791)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r790)
        (f_copy)
        (not 
          (in r791))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r792
    :parameters ()
    :precondition 
      (and
        (in r792)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r793)
        (f_copy)
        (not 
          (in r792))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r792
    :parameters ()
    :precondition 
      (and
        (in r792)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r791)
        (f_copy)
        (not 
          (in r792))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r793
    :parameters ()
    :precondition 
      (and
        (in r793)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r794)
        (f_copy)
        (not 
          (in r793))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r793
    :parameters ()
    :precondition 
      (and
        (in r793)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r792)
        (f_copy)
        (not 
          (in r793))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r794
    :parameters ()
    :precondition 
      (and
        (in r794)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r795)
        (f_copy)
        (not 
          (in r794))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r794
    :parameters ()
    :precondition 
      (and
        (in r794)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r793)
        (f_copy)
        (not 
          (in r794))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r795
    :parameters ()
    :precondition 
      (and
        (in r795)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r796)
        (f_copy)
        (not 
          (in r795))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r795
    :parameters ()
    :precondition 
      (and
        (in r795)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r794)
        (f_copy)
        (not 
          (in r795))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r796
    :parameters ()
    :precondition 
      (and
        (in r796)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r797)
        (f_copy)
        (not 
          (in r796))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r796
    :parameters ()
    :precondition 
      (and
        (in r796)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r795)
        (f_copy)
        (not 
          (in r796))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r797
    :parameters ()
    :precondition 
      (and
        (in r797)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r798)
        (f_copy)
        (not 
          (in r797))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r797
    :parameters ()
    :precondition 
      (and
        (in r797)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r796)
        (f_copy)
        (not 
          (in r797))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r798
    :parameters ()
    :precondition 
      (and
        (in r798)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r799)
        (f_copy)
        (not 
          (in r798))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r798
    :parameters ()
    :precondition 
      (and
        (in r798)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r797)
        (f_copy)
        (not 
          (in r798))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r799
    :parameters ()
    :precondition 
      (and
        (in r799)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r800)
        (f_copy)
        (not 
          (in r799))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r799
    :parameters ()
    :precondition 
      (and
        (in r799)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r798)
        (f_copy)
        (not 
          (in r799))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r800
    :parameters ()
    :precondition 
      (and
        (in r800)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r801)
        (f_copy)
        (not 
          (in r800))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r800
    :parameters ()
    :precondition 
      (and
        (in r800)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r799)
        (f_copy)
        (not 
          (in r800))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r801
    :parameters ()
    :precondition 
      (and
        (in r801)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r802)
        (f_copy)
        (not 
          (in r801))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r801
    :parameters ()
    :precondition 
      (and
        (in r801)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r800)
        (f_copy)
        (not 
          (in r801))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r802
    :parameters ()
    :precondition 
      (and
        (in r802)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r803)
        (f_copy)
        (not 
          (in r802))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r802
    :parameters ()
    :precondition 
      (and
        (in r802)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r801)
        (f_copy)
        (not 
          (in r802))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r803
    :parameters ()
    :precondition 
      (and
        (in r803)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r804)
        (f_copy)
        (not 
          (in r803))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r803
    :parameters ()
    :precondition 
      (and
        (in r803)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r802)
        (f_copy)
        (not 
          (in r803))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r804
    :parameters ()
    :precondition 
      (and
        (in r804)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r805)
        (f_copy)
        (not 
          (in r804))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r804
    :parameters ()
    :precondition 
      (and
        (in r804)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r803)
        (f_copy)
        (not 
          (in r804))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r805
    :parameters ()
    :precondition 
      (and
        (in r805)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r806)
        (f_copy)
        (not 
          (in r805))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r805
    :parameters ()
    :precondition 
      (and
        (in r805)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r804)
        (f_copy)
        (not 
          (in r805))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r806
    :parameters ()
    :precondition 
      (and
        (in r806)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r807)
        (f_copy)
        (not 
          (in r806))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r806
    :parameters ()
    :precondition 
      (and
        (in r806)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r805)
        (f_copy)
        (not 
          (in r806))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r807
    :parameters ()
    :precondition 
      (and
        (in r807)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r808)
        (f_copy)
        (not 
          (in r807))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r807
    :parameters ()
    :precondition 
      (and
        (in r807)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r806)
        (f_copy)
        (not 
          (in r807))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r808
    :parameters ()
    :precondition 
      (and
        (in r808)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r809)
        (f_copy)
        (not 
          (in r808))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r808
    :parameters ()
    :precondition 
      (and
        (in r808)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r807)
        (f_copy)
        (not 
          (in r808))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r809
    :parameters ()
    :precondition 
      (and
        (in r809)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r810)
        (f_copy)
        (not 
          (in r809))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r809
    :parameters ()
    :precondition 
      (and
        (in r809)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r808)
        (f_copy)
        (not 
          (in r809))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r810
    :parameters ()
    :precondition 
      (and
        (in r810)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r811)
        (f_copy)
        (not 
          (in r810))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r810
    :parameters ()
    :precondition 
      (and
        (in r810)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r809)
        (f_copy)
        (not 
          (in r810))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r811
    :parameters ()
    :precondition 
      (and
        (in r811)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r812)
        (f_copy)
        (not 
          (in r811))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r811
    :parameters ()
    :precondition 
      (and
        (in r811)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r810)
        (f_copy)
        (not 
          (in r811))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r812
    :parameters ()
    :precondition 
      (and
        (in r812)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r813)
        (f_copy)
        (not 
          (in r812))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r812
    :parameters ()
    :precondition 
      (and
        (in r812)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r811)
        (f_copy)
        (not 
          (in r812))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r813
    :parameters ()
    :precondition 
      (and
        (in r813)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r814)
        (f_copy)
        (not 
          (in r813))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r813
    :parameters ()
    :precondition 
      (and
        (in r813)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r812)
        (f_copy)
        (not 
          (in r813))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r814
    :parameters ()
    :precondition 
      (and
        (in r814)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r815)
        (f_copy)
        (not 
          (in r814))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r814
    :parameters ()
    :precondition 
      (and
        (in r814)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r813)
        (f_copy)
        (not 
          (in r814))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r815
    :parameters ()
    :precondition 
      (and
        (in r815)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r816)
        (f_copy)
        (not 
          (in r815))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r815
    :parameters ()
    :precondition 
      (and
        (in r815)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r814)
        (f_copy)
        (not 
          (in r815))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r816
    :parameters ()
    :precondition 
      (and
        (in r816)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r817)
        (f_copy)
        (not 
          (in r816))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r816
    :parameters ()
    :precondition 
      (and
        (in r816)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r815)
        (f_copy)
        (not 
          (in r816))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r817
    :parameters ()
    :precondition 
      (and
        (in r817)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r818)
        (f_copy)
        (not 
          (in r817))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r817
    :parameters ()
    :precondition 
      (and
        (in r817)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r816)
        (f_copy)
        (not 
          (in r817))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r818
    :parameters ()
    :precondition 
      (and
        (in r818)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r819)
        (f_copy)
        (not 
          (in r818))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r818
    :parameters ()
    :precondition 
      (and
        (in r818)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r817)
        (f_copy)
        (not 
          (in r818))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r819
    :parameters ()
    :precondition 
      (and
        (in r819)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r820)
        (f_copy)
        (not 
          (in r819))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r819
    :parameters ()
    :precondition 
      (and
        (in r819)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r818)
        (f_copy)
        (not 
          (in r819))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r820
    :parameters ()
    :precondition 
      (and
        (in r820)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r821)
        (f_copy)
        (not 
          (in r820))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r820
    :parameters ()
    :precondition 
      (and
        (in r820)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r819)
        (f_copy)
        (not 
          (in r820))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r821
    :parameters ()
    :precondition 
      (and
        (in r821)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r822)
        (f_copy)
        (not 
          (in r821))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r821
    :parameters ()
    :precondition 
      (and
        (in r821)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r820)
        (f_copy)
        (not 
          (in r821))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r822
    :parameters ()
    :precondition 
      (and
        (in r822)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r823)
        (f_copy)
        (not 
          (in r822))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r822
    :parameters ()
    :precondition 
      (and
        (in r822)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r821)
        (f_copy)
        (not 
          (in r822))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r823
    :parameters ()
    :precondition 
      (and
        (in r823)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r824)
        (f_copy)
        (not 
          (in r823))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r823
    :parameters ()
    :precondition 
      (and
        (in r823)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r822)
        (f_copy)
        (not 
          (in r823))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r824
    :parameters ()
    :precondition 
      (and
        (in r824)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r825)
        (f_copy)
        (not 
          (in r824))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r824
    :parameters ()
    :precondition 
      (and
        (in r824)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r823)
        (f_copy)
        (not 
          (in r824))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r825
    :parameters ()
    :precondition 
      (and
        (in r825)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r826)
        (f_copy)
        (not 
          (in r825))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r825
    :parameters ()
    :precondition 
      (and
        (in r825)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r824)
        (f_copy)
        (not 
          (in r825))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r826
    :parameters ()
    :precondition 
      (and
        (in r826)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r827)
        (f_copy)
        (not 
          (in r826))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r826
    :parameters ()
    :precondition 
      (and
        (in r826)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r825)
        (f_copy)
        (not 
          (in r826))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r827
    :parameters ()
    :precondition 
      (and
        (in r827)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r828)
        (f_copy)
        (not 
          (in r827))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r827
    :parameters ()
    :precondition 
      (and
        (in r827)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r826)
        (f_copy)
        (not 
          (in r827))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r828
    :parameters ()
    :precondition 
      (and
        (in r828)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r829)
        (f_copy)
        (not 
          (in r828))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r828
    :parameters ()
    :precondition 
      (and
        (in r828)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r827)
        (f_copy)
        (not 
          (in r828))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r829
    :parameters ()
    :precondition 
      (and
        (in r829)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r830)
        (f_copy)
        (not 
          (in r829))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r829
    :parameters ()
    :precondition 
      (and
        (in r829)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r828)
        (f_copy)
        (not 
          (in r829))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r830
    :parameters ()
    :precondition 
      (and
        (in r830)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r831)
        (f_copy)
        (not 
          (in r830))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r830
    :parameters ()
    :precondition 
      (and
        (in r830)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r829)
        (f_copy)
        (not 
          (in r830))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r831
    :parameters ()
    :precondition 
      (and
        (in r831)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r832)
        (f_copy)
        (not 
          (in r831))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r831
    :parameters ()
    :precondition 
      (and
        (in r831)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r830)
        (f_copy)
        (not 
          (in r831))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r832
    :parameters ()
    :precondition 
      (and
        (in r832)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r833)
        (f_copy)
        (not 
          (in r832))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r832
    :parameters ()
    :precondition 
      (and
        (in r832)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r831)
        (f_copy)
        (not 
          (in r832))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r833
    :parameters ()
    :precondition 
      (and
        (in r833)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r834)
        (f_copy)
        (not 
          (in r833))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r833
    :parameters ()
    :precondition 
      (and
        (in r833)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r832)
        (f_copy)
        (not 
          (in r833))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r834
    :parameters ()
    :precondition 
      (and
        (in r834)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r835)
        (f_copy)
        (not 
          (in r834))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r834
    :parameters ()
    :precondition 
      (and
        (in r834)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r833)
        (f_copy)
        (not 
          (in r834))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r835
    :parameters ()
    :precondition 
      (and
        (in r835)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r836)
        (f_copy)
        (not 
          (in r835))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r835
    :parameters ()
    :precondition 
      (and
        (in r835)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r834)
        (f_copy)
        (not 
          (in r835))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r836
    :parameters ()
    :precondition 
      (and
        (in r836)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r837)
        (f_copy)
        (not 
          (in r836))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r836
    :parameters ()
    :precondition 
      (and
        (in r836)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r835)
        (f_copy)
        (not 
          (in r836))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r837
    :parameters ()
    :precondition 
      (and
        (in r837)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r838)
        (f_copy)
        (not 
          (in r837))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r837
    :parameters ()
    :precondition 
      (and
        (in r837)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r836)
        (f_copy)
        (not 
          (in r837))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r838
    :parameters ()
    :precondition 
      (and
        (in r838)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r839)
        (f_copy)
        (not 
          (in r838))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r838
    :parameters ()
    :precondition 
      (and
        (in r838)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r837)
        (f_copy)
        (not 
          (in r838))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r839
    :parameters ()
    :precondition 
      (and
        (in r839)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r840)
        (f_copy)
        (not 
          (in r839))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r839
    :parameters ()
    :precondition 
      (and
        (in r839)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r838)
        (f_copy)
        (not 
          (in r839))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r840
    :parameters ()
    :precondition 
      (and
        (in r840)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r841)
        (f_copy)
        (not 
          (in r840))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r840
    :parameters ()
    :precondition 
      (and
        (in r840)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r839)
        (f_copy)
        (not 
          (in r840))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r841
    :parameters ()
    :precondition 
      (and
        (in r841)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r842)
        (f_copy)
        (not 
          (in r841))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r841
    :parameters ()
    :precondition 
      (and
        (in r841)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r840)
        (f_copy)
        (not 
          (in r841))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r842
    :parameters ()
    :precondition 
      (and
        (in r842)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r843)
        (f_copy)
        (not 
          (in r842))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r842
    :parameters ()
    :precondition 
      (and
        (in r842)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r841)
        (f_copy)
        (not 
          (in r842))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r843
    :parameters ()
    :precondition 
      (and
        (in r843)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r844)
        (f_copy)
        (not 
          (in r843))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r843
    :parameters ()
    :precondition 
      (and
        (in r843)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r842)
        (f_copy)
        (not 
          (in r843))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r844
    :parameters ()
    :precondition 
      (and
        (in r844)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r845)
        (f_copy)
        (not 
          (in r844))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r844
    :parameters ()
    :precondition 
      (and
        (in r844)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r843)
        (f_copy)
        (not 
          (in r844))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r845
    :parameters ()
    :precondition 
      (and
        (in r845)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r846)
        (f_copy)
        (not 
          (in r845))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r845
    :parameters ()
    :precondition 
      (and
        (in r845)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r844)
        (f_copy)
        (not 
          (in r845))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r846
    :parameters ()
    :precondition 
      (and
        (in r846)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r847)
        (f_copy)
        (not 
          (in r846))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r846
    :parameters ()
    :precondition 
      (and
        (in r846)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r845)
        (f_copy)
        (not 
          (in r846))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r847
    :parameters ()
    :precondition 
      (and
        (in r847)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r848)
        (f_copy)
        (not 
          (in r847))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r847
    :parameters ()
    :precondition 
      (and
        (in r847)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r846)
        (f_copy)
        (not 
          (in r847))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r848
    :parameters ()
    :precondition 
      (and
        (in r848)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r849)
        (f_copy)
        (not 
          (in r848))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r848
    :parameters ()
    :precondition 
      (and
        (in r848)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r847)
        (f_copy)
        (not 
          (in r848))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r849
    :parameters ()
    :precondition 
      (and
        (in r849)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r850)
        (f_copy)
        (not 
          (in r849))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r849
    :parameters ()
    :precondition 
      (and
        (in r849)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r848)
        (f_copy)
        (not 
          (in r849))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r850
    :parameters ()
    :precondition 
      (and
        (in r850)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r851)
        (f_copy)
        (not 
          (in r850))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r850
    :parameters ()
    :precondition 
      (and
        (in r850)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r849)
        (f_copy)
        (not 
          (in r850))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r851
    :parameters ()
    :precondition 
      (and
        (in r851)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r852)
        (f_copy)
        (not 
          (in r851))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r851
    :parameters ()
    :precondition 
      (and
        (in r851)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r850)
        (f_copy)
        (not 
          (in r851))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r852
    :parameters ()
    :precondition 
      (and
        (in r852)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r853)
        (f_copy)
        (not 
          (in r852))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r852
    :parameters ()
    :precondition 
      (and
        (in r852)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r851)
        (f_copy)
        (not 
          (in r852))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r853
    :parameters ()
    :precondition 
      (and
        (in r853)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r854)
        (f_copy)
        (not 
          (in r853))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r853
    :parameters ()
    :precondition 
      (and
        (in r853)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r852)
        (f_copy)
        (not 
          (in r853))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r854
    :parameters ()
    :precondition 
      (and
        (in r854)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r855)
        (f_copy)
        (not 
          (in r854))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r854
    :parameters ()
    :precondition 
      (and
        (in r854)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r853)
        (f_copy)
        (not 
          (in r854))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r855
    :parameters ()
    :precondition 
      (and
        (in r855)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r856)
        (f_copy)
        (not 
          (in r855))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r855
    :parameters ()
    :precondition 
      (and
        (in r855)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r854)
        (f_copy)
        (not 
          (in r855))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r856
    :parameters ()
    :precondition 
      (and
        (in r856)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r857)
        (f_copy)
        (not 
          (in r856))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r856
    :parameters ()
    :precondition 
      (and
        (in r856)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r855)
        (f_copy)
        (not 
          (in r856))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r857
    :parameters ()
    :precondition 
      (and
        (in r857)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r858)
        (f_copy)
        (not 
          (in r857))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r857
    :parameters ()
    :precondition 
      (and
        (in r857)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r856)
        (f_copy)
        (not 
          (in r857))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r858
    :parameters ()
    :precondition 
      (and
        (in r858)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r859)
        (f_copy)
        (not 
          (in r858))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r858
    :parameters ()
    :precondition 
      (and
        (in r858)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r857)
        (f_copy)
        (not 
          (in r858))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r859
    :parameters ()
    :precondition 
      (and
        (in r859)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r860)
        (f_copy)
        (not 
          (in r859))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r859
    :parameters ()
    :precondition 
      (and
        (in r859)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r858)
        (f_copy)
        (not 
          (in r859))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r860
    :parameters ()
    :precondition 
      (and
        (in r860)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r861)
        (f_copy)
        (not 
          (in r860))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r860
    :parameters ()
    :precondition 
      (and
        (in r860)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r859)
        (f_copy)
        (not 
          (in r860))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r861
    :parameters ()
    :precondition 
      (and
        (in r861)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r862)
        (f_copy)
        (not 
          (in r861))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r861
    :parameters ()
    :precondition 
      (and
        (in r861)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r860)
        (f_copy)
        (not 
          (in r861))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r862
    :parameters ()
    :precondition 
      (and
        (in r862)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r863)
        (f_copy)
        (not 
          (in r862))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r862
    :parameters ()
    :precondition 
      (and
        (in r862)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r861)
        (f_copy)
        (not 
          (in r862))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r863
    :parameters ()
    :precondition 
      (and
        (in r863)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r864)
        (f_copy)
        (not 
          (in r863))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r863
    :parameters ()
    :precondition 
      (and
        (in r863)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r862)
        (f_copy)
        (not 
          (in r863))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r864
    :parameters ()
    :precondition 
      (and
        (in r864)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r865)
        (f_copy)
        (not 
          (in r864))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r864
    :parameters ()
    :precondition 
      (and
        (in r864)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r863)
        (f_copy)
        (not 
          (in r864))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r865
    :parameters ()
    :precondition 
      (and
        (in r865)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r866)
        (f_copy)
        (not 
          (in r865))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r865
    :parameters ()
    :precondition 
      (and
        (in r865)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r864)
        (f_copy)
        (not 
          (in r865))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r866
    :parameters ()
    :precondition 
      (and
        (in r866)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r867)
        (f_copy)
        (not 
          (in r866))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r866
    :parameters ()
    :precondition 
      (and
        (in r866)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r865)
        (f_copy)
        (not 
          (in r866))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r867
    :parameters ()
    :precondition 
      (and
        (in r867)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r868)
        (f_copy)
        (not 
          (in r867))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r867
    :parameters ()
    :precondition 
      (and
        (in r867)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r866)
        (f_copy)
        (not 
          (in r867))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r868
    :parameters ()
    :precondition 
      (and
        (in r868)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r869)
        (f_copy)
        (not 
          (in r868))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r868
    :parameters ()
    :precondition 
      (and
        (in r868)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r867)
        (f_copy)
        (not 
          (in r868))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r869
    :parameters ()
    :precondition 
      (and
        (in r869)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r870)
        (f_copy)
        (not 
          (in r869))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r869
    :parameters ()
    :precondition 
      (and
        (in r869)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r868)
        (f_copy)
        (not 
          (in r869))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r870
    :parameters ()
    :precondition 
      (and
        (in r870)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r871)
        (f_copy)
        (not 
          (in r870))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r870
    :parameters ()
    :precondition 
      (and
        (in r870)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r869)
        (f_copy)
        (not 
          (in r870))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r871
    :parameters ()
    :precondition 
      (and
        (in r871)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r872)
        (f_copy)
        (not 
          (in r871))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r871
    :parameters ()
    :precondition 
      (and
        (in r871)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r870)
        (f_copy)
        (not 
          (in r871))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r872
    :parameters ()
    :precondition 
      (and
        (in r872)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r873)
        (f_copy)
        (not 
          (in r872))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r872
    :parameters ()
    :precondition 
      (and
        (in r872)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r871)
        (f_copy)
        (not 
          (in r872))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r873
    :parameters ()
    :precondition 
      (and
        (in r873)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r874)
        (f_copy)
        (not 
          (in r873))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r873
    :parameters ()
    :precondition 
      (and
        (in r873)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r872)
        (f_copy)
        (not 
          (in r873))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r874
    :parameters ()
    :precondition 
      (and
        (in r874)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r875)
        (f_copy)
        (not 
          (in r874))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r874
    :parameters ()
    :precondition 
      (and
        (in r874)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r873)
        (f_copy)
        (not 
          (in r874))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r875
    :parameters ()
    :precondition 
      (and
        (in r875)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r876)
        (f_copy)
        (not 
          (in r875))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r875
    :parameters ()
    :precondition 
      (and
        (in r875)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r874)
        (f_copy)
        (not 
          (in r875))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r876
    :parameters ()
    :precondition 
      (and
        (in r876)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r877)
        (f_copy)
        (not 
          (in r876))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r876
    :parameters ()
    :precondition 
      (and
        (in r876)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r875)
        (f_copy)
        (not 
          (in r876))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r877
    :parameters ()
    :precondition 
      (and
        (in r877)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r878)
        (f_copy)
        (not 
          (in r877))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r877
    :parameters ()
    :precondition 
      (and
        (in r877)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r876)
        (f_copy)
        (not 
          (in r877))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r878
    :parameters ()
    :precondition 
      (and
        (in r878)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r879)
        (f_copy)
        (not 
          (in r878))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r878
    :parameters ()
    :precondition 
      (and
        (in r878)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r877)
        (f_copy)
        (not 
          (in r878))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r879
    :parameters ()
    :precondition 
      (and
        (in r879)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r880)
        (f_copy)
        (not 
          (in r879))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r879
    :parameters ()
    :precondition 
      (and
        (in r879)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r878)
        (f_copy)
        (not 
          (in r879))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r880
    :parameters ()
    :precondition 
      (and
        (in r880)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r881)
        (f_copy)
        (not 
          (in r880))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r880
    :parameters ()
    :precondition 
      (and
        (in r880)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r879)
        (f_copy)
        (not 
          (in r880))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r881
    :parameters ()
    :precondition 
      (and
        (in r881)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r882)
        (f_copy)
        (not 
          (in r881))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r881
    :parameters ()
    :precondition 
      (and
        (in r881)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r880)
        (f_copy)
        (not 
          (in r881))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r882
    :parameters ()
    :precondition 
      (and
        (in r882)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r883)
        (f_copy)
        (not 
          (in r882))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r882
    :parameters ()
    :precondition 
      (and
        (in r882)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r881)
        (f_copy)
        (not 
          (in r882))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r883
    :parameters ()
    :precondition 
      (and
        (in r883)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r884)
        (f_copy)
        (not 
          (in r883))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r883
    :parameters ()
    :precondition 
      (and
        (in r883)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r882)
        (f_copy)
        (not 
          (in r883))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r884
    :parameters ()
    :precondition 
      (and
        (in r884)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r885)
        (f_copy)
        (not 
          (in r884))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r884
    :parameters ()
    :precondition 
      (and
        (in r884)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r883)
        (f_copy)
        (not 
          (in r884))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r885
    :parameters ()
    :precondition 
      (and
        (in r885)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r886)
        (f_copy)
        (not 
          (in r885))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r885
    :parameters ()
    :precondition 
      (and
        (in r885)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r884)
        (f_copy)
        (not 
          (in r885))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r886
    :parameters ()
    :precondition 
      (and
        (in r886)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r887)
        (f_copy)
        (not 
          (in r886))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r886
    :parameters ()
    :precondition 
      (and
        (in r886)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r885)
        (f_copy)
        (not 
          (in r886))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r887
    :parameters ()
    :precondition 
      (and
        (in r887)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r888)
        (f_copy)
        (not 
          (in r887))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r887
    :parameters ()
    :precondition 
      (and
        (in r887)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r886)
        (f_copy)
        (not 
          (in r887))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r888
    :parameters ()
    :precondition 
      (and
        (in r888)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r889)
        (f_copy)
        (not 
          (in r888))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r888
    :parameters ()
    :precondition 
      (and
        (in r888)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r887)
        (f_copy)
        (not 
          (in r888))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r889
    :parameters ()
    :precondition 
      (and
        (in r889)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r890)
        (f_copy)
        (not 
          (in r889))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r889
    :parameters ()
    :precondition 
      (and
        (in r889)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r888)
        (f_copy)
        (not 
          (in r889))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r890
    :parameters ()
    :precondition 
      (and
        (in r890)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r891)
        (f_copy)
        (not 
          (in r890))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r890
    :parameters ()
    :precondition 
      (and
        (in r890)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r889)
        (f_copy)
        (not 
          (in r890))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r891
    :parameters ()
    :precondition 
      (and
        (in r891)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r892)
        (f_copy)
        (not 
          (in r891))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r891
    :parameters ()
    :precondition 
      (and
        (in r891)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r890)
        (f_copy)
        (not 
          (in r891))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r892
    :parameters ()
    :precondition 
      (and
        (in r892)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r893)
        (f_copy)
        (not 
          (in r892))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r892
    :parameters ()
    :precondition 
      (and
        (in r892)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r891)
        (f_copy)
        (not 
          (in r892))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r893
    :parameters ()
    :precondition 
      (and
        (in r893)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r894)
        (f_copy)
        (not 
          (in r893))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r893
    :parameters ()
    :precondition 
      (and
        (in r893)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r892)
        (f_copy)
        (not 
          (in r893))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r894
    :parameters ()
    :precondition 
      (and
        (in r894)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r895)
        (f_copy)
        (not 
          (in r894))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r894
    :parameters ()
    :precondition 
      (and
        (in r894)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r893)
        (f_copy)
        (not 
          (in r894))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r895
    :parameters ()
    :precondition 
      (and
        (in r895)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r896)
        (f_copy)
        (not 
          (in r895))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r895
    :parameters ()
    :precondition 
      (and
        (in r895)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r894)
        (f_copy)
        (not 
          (in r895))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r896
    :parameters ()
    :precondition 
      (and
        (in r896)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r897)
        (f_copy)
        (not 
          (in r896))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r896
    :parameters ()
    :precondition 
      (and
        (in r896)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r895)
        (f_copy)
        (not 
          (in r896))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r897
    :parameters ()
    :precondition 
      (and
        (in r897)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r898)
        (f_copy)
        (not 
          (in r897))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r897
    :parameters ()
    :precondition 
      (and
        (in r897)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r896)
        (f_copy)
        (not 
          (in r897))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r898
    :parameters ()
    :precondition 
      (and
        (in r898)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r899)
        (f_copy)
        (not 
          (in r898))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r898
    :parameters ()
    :precondition 
      (and
        (in r898)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r897)
        (f_copy)
        (not 
          (in r898))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r899
    :parameters ()
    :precondition 
      (and
        (in r899)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r900)
        (f_copy)
        (not 
          (in r899))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r899
    :parameters ()
    :precondition 
      (and
        (in r899)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r898)
        (f_copy)
        (not 
          (in r899))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r900
    :parameters ()
    :precondition 
      (and
        (in r900)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r901)
        (f_copy)
        (not 
          (in r900))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r900
    :parameters ()
    :precondition 
      (and
        (in r900)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r899)
        (f_copy)
        (not 
          (in r900))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r901
    :parameters ()
    :precondition 
      (and
        (in r901)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r902)
        (f_copy)
        (not 
          (in r901))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r901
    :parameters ()
    :precondition 
      (and
        (in r901)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r900)
        (f_copy)
        (not 
          (in r901))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r902
    :parameters ()
    :precondition 
      (and
        (in r902)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r903)
        (f_copy)
        (not 
          (in r902))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r902
    :parameters ()
    :precondition 
      (and
        (in r902)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r901)
        (f_copy)
        (not 
          (in r902))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r903
    :parameters ()
    :precondition 
      (and
        (in r903)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r904)
        (f_copy)
        (not 
          (in r903))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r903
    :parameters ()
    :precondition 
      (and
        (in r903)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r902)
        (f_copy)
        (not 
          (in r903))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r904
    :parameters ()
    :precondition 
      (and
        (in r904)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r905)
        (f_copy)
        (not 
          (in r904))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r904
    :parameters ()
    :precondition 
      (and
        (in r904)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r903)
        (f_copy)
        (not 
          (in r904))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r905
    :parameters ()
    :precondition 
      (and
        (in r905)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r906)
        (f_copy)
        (not 
          (in r905))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r905
    :parameters ()
    :precondition 
      (and
        (in r905)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r904)
        (f_copy)
        (not 
          (in r905))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r906
    :parameters ()
    :precondition 
      (and
        (in r906)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r907)
        (f_copy)
        (not 
          (in r906))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r906
    :parameters ()
    :precondition 
      (and
        (in r906)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r905)
        (f_copy)
        (not 
          (in r906))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r907
    :parameters ()
    :precondition 
      (and
        (in r907)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r908)
        (f_copy)
        (not 
          (in r907))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r907
    :parameters ()
    :precondition 
      (and
        (in r907)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r906)
        (f_copy)
        (not 
          (in r907))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r908
    :parameters ()
    :precondition 
      (and
        (in r908)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r909)
        (f_copy)
        (not 
          (in r908))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r908
    :parameters ()
    :precondition 
      (and
        (in r908)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r907)
        (f_copy)
        (not 
          (in r908))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r909
    :parameters ()
    :precondition 
      (and
        (in r909)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r910)
        (f_copy)
        (not 
          (in r909))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r909
    :parameters ()
    :precondition 
      (and
        (in r909)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r908)
        (f_copy)
        (not 
          (in r909))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r910
    :parameters ()
    :precondition 
      (and
        (in r910)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r911)
        (f_copy)
        (not 
          (in r910))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r910
    :parameters ()
    :precondition 
      (and
        (in r910)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r909)
        (f_copy)
        (not 
          (in r910))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r911
    :parameters ()
    :precondition 
      (and
        (in r911)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r912)
        (f_copy)
        (not 
          (in r911))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r911
    :parameters ()
    :precondition 
      (and
        (in r911)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r910)
        (f_copy)
        (not 
          (in r911))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r912
    :parameters ()
    :precondition 
      (and
        (in r912)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r913)
        (f_copy)
        (not 
          (in r912))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r912
    :parameters ()
    :precondition 
      (and
        (in r912)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r911)
        (f_copy)
        (not 
          (in r912))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r913
    :parameters ()
    :precondition 
      (and
        (in r913)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r914)
        (f_copy)
        (not 
          (in r913))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r913
    :parameters ()
    :precondition 
      (and
        (in r913)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r912)
        (f_copy)
        (not 
          (in r913))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r914
    :parameters ()
    :precondition 
      (and
        (in r914)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r915)
        (f_copy)
        (not 
          (in r914))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r914
    :parameters ()
    :precondition 
      (and
        (in r914)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r913)
        (f_copy)
        (not 
          (in r914))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r915
    :parameters ()
    :precondition 
      (and
        (in r915)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r916)
        (f_copy)
        (not 
          (in r915))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r915
    :parameters ()
    :precondition 
      (and
        (in r915)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r914)
        (f_copy)
        (not 
          (in r915))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r916
    :parameters ()
    :precondition 
      (and
        (in r916)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r917)
        (f_copy)
        (not 
          (in r916))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r916
    :parameters ()
    :precondition 
      (and
        (in r916)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r915)
        (f_copy)
        (not 
          (in r916))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r917
    :parameters ()
    :precondition 
      (and
        (in r917)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r918)
        (f_copy)
        (not 
          (in r917))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r917
    :parameters ()
    :precondition 
      (and
        (in r917)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r916)
        (f_copy)
        (not 
          (in r917))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r918
    :parameters ()
    :precondition 
      (and
        (in r918)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r919)
        (f_copy)
        (not 
          (in r918))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r918
    :parameters ()
    :precondition 
      (and
        (in r918)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r917)
        (f_copy)
        (not 
          (in r918))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r919
    :parameters ()
    :precondition 
      (and
        (in r919)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r920)
        (f_copy)
        (not 
          (in r919))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r919
    :parameters ()
    :precondition 
      (and
        (in r919)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r918)
        (f_copy)
        (not 
          (in r919))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r920
    :parameters ()
    :precondition 
      (and
        (in r920)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r921)
        (f_copy)
        (not 
          (in r920))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r920
    :parameters ()
    :precondition 
      (and
        (in r920)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r919)
        (f_copy)
        (not 
          (in r920))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r921
    :parameters ()
    :precondition 
      (and
        (in r921)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r922)
        (f_copy)
        (not 
          (in r921))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r921
    :parameters ()
    :precondition 
      (and
        (in r921)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r920)
        (f_copy)
        (not 
          (in r921))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r922
    :parameters ()
    :precondition 
      (and
        (in r922)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r923)
        (f_copy)
        (not 
          (in r922))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r922
    :parameters ()
    :precondition 
      (and
        (in r922)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r921)
        (f_copy)
        (not 
          (in r922))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r923
    :parameters ()
    :precondition 
      (and
        (in r923)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r924)
        (f_copy)
        (not 
          (in r923))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r923
    :parameters ()
    :precondition 
      (and
        (in r923)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r922)
        (f_copy)
        (not 
          (in r923))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r924
    :parameters ()
    :precondition 
      (and
        (in r924)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r925)
        (f_copy)
        (not 
          (in r924))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r924
    :parameters ()
    :precondition 
      (and
        (in r924)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r923)
        (f_copy)
        (not 
          (in r924))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r925
    :parameters ()
    :precondition 
      (and
        (in r925)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r926)
        (f_copy)
        (not 
          (in r925))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r925
    :parameters ()
    :precondition 
      (and
        (in r925)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r924)
        (f_copy)
        (not 
          (in r925))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r926
    :parameters ()
    :precondition 
      (and
        (in r926)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r927)
        (f_copy)
        (not 
          (in r926))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r926
    :parameters ()
    :precondition 
      (and
        (in r926)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r925)
        (f_copy)
        (not 
          (in r926))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r927
    :parameters ()
    :precondition 
      (and
        (in r927)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r928)
        (f_copy)
        (not 
          (in r927))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r927
    :parameters ()
    :precondition 
      (and
        (in r927)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r926)
        (f_copy)
        (not 
          (in r927))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r928
    :parameters ()
    :precondition 
      (and
        (in r928)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r929)
        (f_copy)
        (not 
          (in r928))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r928
    :parameters ()
    :precondition 
      (and
        (in r928)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r927)
        (f_copy)
        (not 
          (in r928))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r929
    :parameters ()
    :precondition 
      (and
        (in r929)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r930)
        (f_copy)
        (not 
          (in r929))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r929
    :parameters ()
    :precondition 
      (and
        (in r929)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r928)
        (f_copy)
        (not 
          (in r929))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r930
    :parameters ()
    :precondition 
      (and
        (in r930)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r931)
        (f_copy)
        (not 
          (in r930))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r930
    :parameters ()
    :precondition 
      (and
        (in r930)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r929)
        (f_copy)
        (not 
          (in r930))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r931
    :parameters ()
    :precondition 
      (and
        (in r931)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r932)
        (f_copy)
        (not 
          (in r931))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r931
    :parameters ()
    :precondition 
      (and
        (in r931)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r930)
        (f_copy)
        (not 
          (in r931))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r932
    :parameters ()
    :precondition 
      (and
        (in r932)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r933)
        (f_copy)
        (not 
          (in r932))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r932
    :parameters ()
    :precondition 
      (and
        (in r932)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r931)
        (f_copy)
        (not 
          (in r932))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r933
    :parameters ()
    :precondition 
      (and
        (in r933)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r934)
        (f_copy)
        (not 
          (in r933))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r933
    :parameters ()
    :precondition 
      (and
        (in r933)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r932)
        (f_copy)
        (not 
          (in r933))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r934
    :parameters ()
    :precondition 
      (and
        (in r934)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r935)
        (f_copy)
        (not 
          (in r934))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r934
    :parameters ()
    :precondition 
      (and
        (in r934)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r933)
        (f_copy)
        (not 
          (in r934))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r935
    :parameters ()
    :precondition 
      (and
        (in r935)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r936)
        (f_copy)
        (not 
          (in r935))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r935
    :parameters ()
    :precondition 
      (and
        (in r935)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r934)
        (f_copy)
        (not 
          (in r935))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r936
    :parameters ()
    :precondition 
      (and
        (in r936)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r937)
        (f_copy)
        (not 
          (in r936))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r936
    :parameters ()
    :precondition 
      (and
        (in r936)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r935)
        (f_copy)
        (not 
          (in r936))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r937
    :parameters ()
    :precondition 
      (and
        (in r937)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r938)
        (f_copy)
        (not 
          (in r937))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r937
    :parameters ()
    :precondition 
      (and
        (in r937)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r936)
        (f_copy)
        (not 
          (in r937))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r938
    :parameters ()
    :precondition 
      (and
        (in r938)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r939)
        (f_copy)
        (not 
          (in r938))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r938
    :parameters ()
    :precondition 
      (and
        (in r938)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r937)
        (f_copy)
        (not 
          (in r938))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r939
    :parameters ()
    :precondition 
      (and
        (in r939)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r940)
        (f_copy)
        (not 
          (in r939))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r939
    :parameters ()
    :precondition 
      (and
        (in r939)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r938)
        (f_copy)
        (not 
          (in r939))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r940
    :parameters ()
    :precondition 
      (and
        (in r940)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r941)
        (f_copy)
        (not 
          (in r940))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r940
    :parameters ()
    :precondition 
      (and
        (in r940)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r939)
        (f_copy)
        (not 
          (in r940))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r941
    :parameters ()
    :precondition 
      (and
        (in r941)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r942)
        (f_copy)
        (not 
          (in r941))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r941
    :parameters ()
    :precondition 
      (and
        (in r941)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r940)
        (f_copy)
        (not 
          (in r941))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r942
    :parameters ()
    :precondition 
      (and
        (in r942)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r943)
        (f_copy)
        (not 
          (in r942))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r942
    :parameters ()
    :precondition 
      (and
        (in r942)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r941)
        (f_copy)
        (not 
          (in r942))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r943
    :parameters ()
    :precondition 
      (and
        (in r943)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r944)
        (f_copy)
        (not 
          (in r943))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r943
    :parameters ()
    :precondition 
      (and
        (in r943)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r942)
        (f_copy)
        (not 
          (in r943))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r944
    :parameters ()
    :precondition 
      (and
        (in r944)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r945)
        (f_copy)
        (not 
          (in r944))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r944
    :parameters ()
    :precondition 
      (and
        (in r944)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r943)
        (f_copy)
        (not 
          (in r944))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r945
    :parameters ()
    :precondition 
      (and
        (in r945)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r946)
        (f_copy)
        (not 
          (in r945))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r945
    :parameters ()
    :precondition 
      (and
        (in r945)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r944)
        (f_copy)
        (not 
          (in r945))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r946
    :parameters ()
    :precondition 
      (and
        (in r946)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r947)
        (f_copy)
        (not 
          (in r946))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r946
    :parameters ()
    :precondition 
      (and
        (in r946)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r945)
        (f_copy)
        (not 
          (in r946))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r947
    :parameters ()
    :precondition 
      (and
        (in r947)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r948)
        (f_copy)
        (not 
          (in r947))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r947
    :parameters ()
    :precondition 
      (and
        (in r947)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r946)
        (f_copy)
        (not 
          (in r947))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r948
    :parameters ()
    :precondition 
      (and
        (in r948)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r949)
        (f_copy)
        (not 
          (in r948))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r948
    :parameters ()
    :precondition 
      (and
        (in r948)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r947)
        (f_copy)
        (not 
          (in r948))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r949
    :parameters ()
    :precondition 
      (and
        (in r949)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r950)
        (f_copy)
        (not 
          (in r949))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r949
    :parameters ()
    :precondition 
      (and
        (in r949)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r948)
        (f_copy)
        (not 
          (in r949))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r950
    :parameters ()
    :precondition 
      (and
        (in r950)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r951)
        (f_copy)
        (not 
          (in r950))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r950
    :parameters ()
    :precondition 
      (and
        (in r950)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r949)
        (f_copy)
        (not 
          (in r950))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r951
    :parameters ()
    :precondition 
      (and
        (in r951)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r952)
        (f_copy)
        (not 
          (in r951))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r951
    :parameters ()
    :precondition 
      (and
        (in r951)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r950)
        (f_copy)
        (not 
          (in r951))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r952
    :parameters ()
    :precondition 
      (and
        (in r952)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r953)
        (f_copy)
        (not 
          (in r952))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r952
    :parameters ()
    :precondition 
      (and
        (in r952)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r951)
        (f_copy)
        (not 
          (in r952))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r953
    :parameters ()
    :precondition 
      (and
        (in r953)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r954)
        (f_copy)
        (not 
          (in r953))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r953
    :parameters ()
    :precondition 
      (and
        (in r953)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r952)
        (f_copy)
        (not 
          (in r953))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r954
    :parameters ()
    :precondition 
      (and
        (in r954)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r955)
        (f_copy)
        (not 
          (in r954))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r954
    :parameters ()
    :precondition 
      (and
        (in r954)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r953)
        (f_copy)
        (not 
          (in r954))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r955
    :parameters ()
    :precondition 
      (and
        (in r955)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r956)
        (f_copy)
        (not 
          (in r955))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r955
    :parameters ()
    :precondition 
      (and
        (in r955)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r954)
        (f_copy)
        (not 
          (in r955))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r956
    :parameters ()
    :precondition 
      (and
        (in r956)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r957)
        (f_copy)
        (not 
          (in r956))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r956
    :parameters ()
    :precondition 
      (and
        (in r956)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r955)
        (f_copy)
        (not 
          (in r956))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r957
    :parameters ()
    :precondition 
      (and
        (in r957)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r958)
        (f_copy)
        (not 
          (in r957))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r957
    :parameters ()
    :precondition 
      (and
        (in r957)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r956)
        (f_copy)
        (not 
          (in r957))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r958
    :parameters ()
    :precondition 
      (and
        (in r958)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r959)
        (f_copy)
        (not 
          (in r958))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r958
    :parameters ()
    :precondition 
      (and
        (in r958)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r957)
        (f_copy)
        (not 
          (in r958))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r959
    :parameters ()
    :precondition 
      (and
        (in r959)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r960)
        (f_copy)
        (not 
          (in r959))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r959
    :parameters ()
    :precondition 
      (and
        (in r959)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r958)
        (f_copy)
        (not 
          (in r959))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r960
    :parameters ()
    :precondition 
      (and
        (in r960)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r961)
        (f_copy)
        (not 
          (in r960))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r960
    :parameters ()
    :precondition 
      (and
        (in r960)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r959)
        (f_copy)
        (not 
          (in r960))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r961
    :parameters ()
    :precondition 
      (and
        (in r961)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r962)
        (f_copy)
        (not 
          (in r961))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r961
    :parameters ()
    :precondition 
      (and
        (in r961)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r960)
        (f_copy)
        (not 
          (in r961))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r962
    :parameters ()
    :precondition 
      (and
        (in r962)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r963)
        (f_copy)
        (not 
          (in r962))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r962
    :parameters ()
    :precondition 
      (and
        (in r962)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r961)
        (f_copy)
        (not 
          (in r962))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r963
    :parameters ()
    :precondition 
      (and
        (in r963)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r964)
        (f_copy)
        (not 
          (in r963))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r963
    :parameters ()
    :precondition 
      (and
        (in r963)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r962)
        (f_copy)
        (not 
          (in r963))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r964
    :parameters ()
    :precondition 
      (and
        (in r964)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r965)
        (f_copy)
        (not 
          (in r964))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r964
    :parameters ()
    :precondition 
      (and
        (in r964)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r963)
        (f_copy)
        (not 
          (in r964))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r965
    :parameters ()
    :precondition 
      (and
        (in r965)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r966)
        (f_copy)
        (not 
          (in r965))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r965
    :parameters ()
    :precondition 
      (and
        (in r965)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r964)
        (f_copy)
        (not 
          (in r965))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r966
    :parameters ()
    :precondition 
      (and
        (in r966)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r967)
        (f_copy)
        (not 
          (in r966))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r966
    :parameters ()
    :precondition 
      (and
        (in r966)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r965)
        (f_copy)
        (not 
          (in r966))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r967
    :parameters ()
    :precondition 
      (and
        (in r967)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r968)
        (f_copy)
        (not 
          (in r967))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r967
    :parameters ()
    :precondition 
      (and
        (in r967)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r966)
        (f_copy)
        (not 
          (in r967))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r968
    :parameters ()
    :precondition 
      (and
        (in r968)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r969)
        (f_copy)
        (not 
          (in r968))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r968
    :parameters ()
    :precondition 
      (and
        (in r968)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r967)
        (f_copy)
        (not 
          (in r968))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r969
    :parameters ()
    :precondition 
      (and
        (in r969)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r970)
        (f_copy)
        (not 
          (in r969))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r969
    :parameters ()
    :precondition 
      (and
        (in r969)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r968)
        (f_copy)
        (not 
          (in r969))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r970
    :parameters ()
    :precondition 
      (and
        (in r970)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r971)
        (f_copy)
        (not 
          (in r970))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r970
    :parameters ()
    :precondition 
      (and
        (in r970)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r969)
        (f_copy)
        (not 
          (in r970))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r971
    :parameters ()
    :precondition 
      (and
        (in r971)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r972)
        (f_copy)
        (not 
          (in r971))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r971
    :parameters ()
    :precondition 
      (and
        (in r971)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r970)
        (f_copy)
        (not 
          (in r971))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r972
    :parameters ()
    :precondition 
      (and
        (in r972)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r973)
        (f_copy)
        (not 
          (in r972))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r972
    :parameters ()
    :precondition 
      (and
        (in r972)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r971)
        (f_copy)
        (not 
          (in r972))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r973
    :parameters ()
    :precondition 
      (and
        (in r973)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r974)
        (f_copy)
        (not 
          (in r973))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r973
    :parameters ()
    :precondition 
      (and
        (in r973)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r972)
        (f_copy)
        (not 
          (in r973))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r974
    :parameters ()
    :precondition 
      (and
        (in r974)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r975)
        (f_copy)
        (not 
          (in r974))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r974
    :parameters ()
    :precondition 
      (and
        (in r974)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r973)
        (f_copy)
        (not 
          (in r974))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r975
    :parameters ()
    :precondition 
      (and
        (in r975)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r976)
        (f_copy)
        (not 
          (in r975))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r975
    :parameters ()
    :precondition 
      (and
        (in r975)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r974)
        (f_copy)
        (not 
          (in r975))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r976
    :parameters ()
    :precondition 
      (and
        (in r976)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r977)
        (f_copy)
        (not 
          (in r976))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r976
    :parameters ()
    :precondition 
      (and
        (in r976)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r975)
        (f_copy)
        (not 
          (in r976))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r977
    :parameters ()
    :precondition 
      (and
        (in r977)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r978)
        (f_copy)
        (not 
          (in r977))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r977
    :parameters ()
    :precondition 
      (and
        (in r977)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r976)
        (f_copy)
        (not 
          (in r977))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r978
    :parameters ()
    :precondition 
      (and
        (in r978)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r979)
        (f_copy)
        (not 
          (in r978))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r978
    :parameters ()
    :precondition 
      (and
        (in r978)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r977)
        (f_copy)
        (not 
          (in r978))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r979
    :parameters ()
    :precondition 
      (and
        (in r979)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r980)
        (f_copy)
        (not 
          (in r979))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r979
    :parameters ()
    :precondition 
      (and
        (in r979)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r978)
        (f_copy)
        (not 
          (in r979))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r980
    :parameters ()
    :precondition 
      (and
        (in r980)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r981)
        (f_copy)
        (not 
          (in r980))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r980
    :parameters ()
    :precondition 
      (and
        (in r980)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r979)
        (f_copy)
        (not 
          (in r980))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r981
    :parameters ()
    :precondition 
      (and
        (in r981)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r982)
        (f_copy)
        (not 
          (in r981))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r981
    :parameters ()
    :precondition 
      (and
        (in r981)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r980)
        (f_copy)
        (not 
          (in r981))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r982
    :parameters ()
    :precondition 
      (and
        (in r982)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r983)
        (f_copy)
        (not 
          (in r982))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r982
    :parameters ()
    :precondition 
      (and
        (in r982)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r981)
        (f_copy)
        (not 
          (in r982))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r983
    :parameters ()
    :precondition 
      (and
        (in r983)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r984)
        (f_copy)
        (not 
          (in r983))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r983
    :parameters ()
    :precondition 
      (and
        (in r983)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r982)
        (f_copy)
        (not 
          (in r983))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r984
    :parameters ()
    :precondition 
      (and
        (in r984)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r985)
        (f_copy)
        (not 
          (in r984))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r984
    :parameters ()
    :precondition 
      (and
        (in r984)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r983)
        (f_copy)
        (not 
          (in r984))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r985
    :parameters ()
    :precondition 
      (and
        (in r985)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r986)
        (f_copy)
        (not 
          (in r985))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r985
    :parameters ()
    :precondition 
      (and
        (in r985)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r984)
        (f_copy)
        (not 
          (in r985))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r986
    :parameters ()
    :precondition 
      (and
        (in r986)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r987)
        (f_copy)
        (not 
          (in r986))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r986
    :parameters ()
    :precondition 
      (and
        (in r986)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r985)
        (f_copy)
        (not 
          (in r986))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r987
    :parameters ()
    :precondition 
      (and
        (in r987)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r988)
        (f_copy)
        (not 
          (in r987))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r987
    :parameters ()
    :precondition 
      (and
        (in r987)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r986)
        (f_copy)
        (not 
          (in r987))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r988
    :parameters ()
    :precondition 
      (and
        (in r988)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r989)
        (f_copy)
        (not 
          (in r988))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r988
    :parameters ()
    :precondition 
      (and
        (in r988)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r987)
        (f_copy)
        (not 
          (in r988))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r989
    :parameters ()
    :precondition 
      (and
        (in r989)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r990)
        (f_copy)
        (not 
          (in r989))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r989
    :parameters ()
    :precondition 
      (and
        (in r989)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r988)
        (f_copy)
        (not 
          (in r989))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r990
    :parameters ()
    :precondition 
      (and
        (in r990)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r991)
        (f_copy)
        (not 
          (in r990))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r990
    :parameters ()
    :precondition 
      (and
        (in r990)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r989)
        (f_copy)
        (not 
          (in r990))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r991
    :parameters ()
    :precondition 
      (and
        (in r991)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r992)
        (f_copy)
        (not 
          (in r991))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r991
    :parameters ()
    :precondition 
      (and
        (in r991)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r990)
        (f_copy)
        (not 
          (in r991))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r992
    :parameters ()
    :precondition 
      (and
        (in r992)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r993)
        (f_copy)
        (not 
          (in r992))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r992
    :parameters ()
    :precondition 
      (and
        (in r992)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r991)
        (f_copy)
        (not 
          (in r992))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r993
    :parameters ()
    :precondition 
      (and
        (in r993)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r994)
        (f_copy)
        (not 
          (in r993))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r993
    :parameters ()
    :precondition 
      (and
        (in r993)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r992)
        (f_copy)
        (not 
          (in r993))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r994
    :parameters ()
    :precondition 
      (and
        (in r994)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r995)
        (f_copy)
        (not 
          (in r994))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r994
    :parameters ()
    :precondition 
      (and
        (in r994)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r993)
        (f_copy)
        (not 
          (in r994))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r995
    :parameters ()
    :precondition 
      (and
        (in r995)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r996)
        (f_copy)
        (not 
          (in r995))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r995
    :parameters ()
    :precondition 
      (and
        (in r995)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r994)
        (f_copy)
        (not 
          (in r995))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r996
    :parameters ()
    :precondition 
      (and
        (in r996)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r997)
        (f_copy)
        (not 
          (in r996))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r996
    :parameters ()
    :precondition 
      (and
        (in r996)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r995)
        (f_copy)
        (not 
          (in r996))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r997
    :parameters ()
    :precondition 
      (and
        (in r997)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r998)
        (f_copy)
        (not 
          (in r997))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r997
    :parameters ()
    :precondition 
      (and
        (in r997)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r996)
        (f_copy)
        (not 
          (in r997))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r998
    :parameters ()
    :precondition 
      (and
        (in r998)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r999)
        (f_copy)
        (not 
          (in r998))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r998
    :parameters ()
    :precondition 
      (and
        (in r998)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r997)
        (f_copy)
        (not 
          (in r998))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r999
    :parameters ()
    :precondition 
      (and
        (in r999)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r1000)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r999))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r999
    :parameters ()
    :precondition 
      (and
        (in r999)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r998)
        (f_copy)
        (not 
          (in r999))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r1000
    :parameters ()
    :precondition 
      (and
        (in r1000)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r1)
        (f_copy)
        (not 
          (in r1000))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r1000
    :parameters ()
    :precondition 
      (and
        (in r1000)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r999)
        (f_copy)
        (not 
          (in r1000))
        (not 
          (f_world))
      )
    )
  (:action stay
    :parameters ()
    :precondition 
      (and
        (not 
          (in r500))
        (not 
          (in r1000))
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r500-seen
    :parameters ()
    :precondition 
      (and
        (in r500)
        (seen)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r500-not-seen
    :parameters ()
    :precondition 
      (and
        (in r500)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r1000-seen
    :parameters ()
    :precondition 
      (and
        (in r1000)
        (seen)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r1000-not-seen
    :parameters ()
    :precondition 
      (and
        (in r1000)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action o_copy
    :parameters ()
    :precondition 
      (and
        (f_ok)
        (f_copy))
    :effect
      (and
        (f_sync)
        (q_1t)
        (when
          (q_1)
          (q_1s))
        (when
          (q_2)
          (q_2s))
        (when
          (q_3)
          (q_3s))
        (when
          (q_4)
          (q_4s))
        (not 
          (f_copy))
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3))
        (not 
          (q_4))
      )
    )
  (:action o_goal
    :parameters ()
    :precondition 
      (and
        (f_world)
        (f_ok)
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3)))
    :effect
(f_goal)    )
  (:action o_sync_q_1s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_1t))
    :effect
      (and
        (when
          (q_1s)
          (q_2s))
        (q_2t)
        (when
          (q_1s)
          (not 
            (q_1s)))
        (not 
          (q_1t))
      )
    )
  (:action o_sync_q_1s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_1t))
    :effect
      (and
        (when
          (q_1s)
          (q_3))
        (when
          (q_1s)
          (q_1))
        (q_2t)
        (when
          (q_1s)
          (not 
            (q_1s)))
        (not 
          (q_1t))
      )
    )
  (:action o_sync_q_2s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_2t))
    :effect
      (and
        (q_3t)
        (when
          (q_2s)
          (not 
            (q_2s)))
        (when
          (and
            (q_2s)
            (not 
              (seen)))
          (not 
            (f_ok)))
        (not 
          (q_2t))
      )
    )
  (:action o_sync_q_3s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_3t))
    :effect
      (and
        (q_4t)
        (when
          (q_3s)
          (not 
            (q_3s)))
        (not 
          (q_3t))
      )
    )
  (:action o_sync_q_4s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_4t))
    :effect
      (and
        (f_world)
        (when
          (q_1s)
          (q_1))
        (when
          (q_2s)
          (q_2))
        (when
          (q_3s)
          (q_3))
        (when
          (q_4s)
          (q_4))
        (when
          (q_4s)
          (not 
            (f_ok)))
        (not 
          (q_4t))
        (not 
          (f_sync))
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
      )
    )
)