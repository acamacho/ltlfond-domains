(define (domain waldo-seq-100-instance-nobucci_dp2)
  (:types
    room - NO_TYPE
  )

  (:predicates
    (seen)
    (in ?x0 - room)
    (f_copy)
    (f_sync)
    (f_world)
    (f_ok)
    (f_goal)
    (q_1)
    (q_1s)
    (q_1t)
    (q_2)
    (q_2s)
    (q_2t)
    (q_3)
    (q_3s)
    (q_3t)
    (q_4)
    (q_4s)
    (q_4t)
  )
  (:action move-right-from-r1
    :parameters ()
    :precondition 
      (and
        (in r1)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r2)
        (f_copy)
        (not 
          (in r1))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r1
    :parameters ()
    :precondition 
      (and
        (in r1)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r100)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r1))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r2
    :parameters ()
    :precondition 
      (and
        (in r2)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r3)
        (f_copy)
        (not 
          (in r2))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r2
    :parameters ()
    :precondition 
      (and
        (in r2)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r1)
        (f_copy)
        (not 
          (in r2))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r3
    :parameters ()
    :precondition 
      (and
        (in r3)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r4)
        (f_copy)
        (not 
          (in r3))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r3
    :parameters ()
    :precondition 
      (and
        (in r3)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r2)
        (f_copy)
        (not 
          (in r3))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r4
    :parameters ()
    :precondition 
      (and
        (in r4)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r5)
        (f_copy)
        (not 
          (in r4))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r4
    :parameters ()
    :precondition 
      (and
        (in r4)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r3)
        (f_copy)
        (not 
          (in r4))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r5
    :parameters ()
    :precondition 
      (and
        (in r5)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r6)
        (f_copy)
        (not 
          (in r5))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r5
    :parameters ()
    :precondition 
      (and
        (in r5)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r4)
        (f_copy)
        (not 
          (in r5))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r6
    :parameters ()
    :precondition 
      (and
        (in r6)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r7)
        (f_copy)
        (not 
          (in r6))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r6
    :parameters ()
    :precondition 
      (and
        (in r6)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r5)
        (f_copy)
        (not 
          (in r6))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r7
    :parameters ()
    :precondition 
      (and
        (in r7)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r8)
        (f_copy)
        (not 
          (in r7))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r7
    :parameters ()
    :precondition 
      (and
        (in r7)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r6)
        (f_copy)
        (not 
          (in r7))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r8
    :parameters ()
    :precondition 
      (and
        (in r8)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r9)
        (f_copy)
        (not 
          (in r8))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r8
    :parameters ()
    :precondition 
      (and
        (in r8)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r7)
        (f_copy)
        (not 
          (in r8))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r9
    :parameters ()
    :precondition 
      (and
        (in r9)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r10)
        (f_copy)
        (not 
          (in r9))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r9
    :parameters ()
    :precondition 
      (and
        (in r9)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r8)
        (f_copy)
        (not 
          (in r9))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r10
    :parameters ()
    :precondition 
      (and
        (in r10)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r11)
        (f_copy)
        (not 
          (in r10))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r10
    :parameters ()
    :precondition 
      (and
        (in r10)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r9)
        (f_copy)
        (not 
          (in r10))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r11
    :parameters ()
    :precondition 
      (and
        (in r11)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r12)
        (f_copy)
        (not 
          (in r11))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r11
    :parameters ()
    :precondition 
      (and
        (in r11)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r10)
        (f_copy)
        (not 
          (in r11))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r12
    :parameters ()
    :precondition 
      (and
        (in r12)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r13)
        (f_copy)
        (not 
          (in r12))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r12
    :parameters ()
    :precondition 
      (and
        (in r12)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r11)
        (f_copy)
        (not 
          (in r12))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r13
    :parameters ()
    :precondition 
      (and
        (in r13)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r14)
        (f_copy)
        (not 
          (in r13))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r13
    :parameters ()
    :precondition 
      (and
        (in r13)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r12)
        (f_copy)
        (not 
          (in r13))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r14
    :parameters ()
    :precondition 
      (and
        (in r14)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r15)
        (f_copy)
        (not 
          (in r14))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r14
    :parameters ()
    :precondition 
      (and
        (in r14)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r13)
        (f_copy)
        (not 
          (in r14))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r15
    :parameters ()
    :precondition 
      (and
        (in r15)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r16)
        (f_copy)
        (not 
          (in r15))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r15
    :parameters ()
    :precondition 
      (and
        (in r15)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r14)
        (f_copy)
        (not 
          (in r15))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r16
    :parameters ()
    :precondition 
      (and
        (in r16)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r17)
        (f_copy)
        (not 
          (in r16))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r16
    :parameters ()
    :precondition 
      (and
        (in r16)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r15)
        (f_copy)
        (not 
          (in r16))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r17
    :parameters ()
    :precondition 
      (and
        (in r17)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r18)
        (f_copy)
        (not 
          (in r17))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r17
    :parameters ()
    :precondition 
      (and
        (in r17)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r16)
        (f_copy)
        (not 
          (in r17))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r18
    :parameters ()
    :precondition 
      (and
        (in r18)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r19)
        (f_copy)
        (not 
          (in r18))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r18
    :parameters ()
    :precondition 
      (and
        (in r18)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r17)
        (f_copy)
        (not 
          (in r18))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r19
    :parameters ()
    :precondition 
      (and
        (in r19)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r20)
        (f_copy)
        (not 
          (in r19))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r19
    :parameters ()
    :precondition 
      (and
        (in r19)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r18)
        (f_copy)
        (not 
          (in r19))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r20
    :parameters ()
    :precondition 
      (and
        (in r20)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r21)
        (f_copy)
        (not 
          (in r20))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r20
    :parameters ()
    :precondition 
      (and
        (in r20)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r19)
        (f_copy)
        (not 
          (in r20))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r21
    :parameters ()
    :precondition 
      (and
        (in r21)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r22)
        (f_copy)
        (not 
          (in r21))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r21
    :parameters ()
    :precondition 
      (and
        (in r21)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r20)
        (f_copy)
        (not 
          (in r21))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r22
    :parameters ()
    :precondition 
      (and
        (in r22)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r23)
        (f_copy)
        (not 
          (in r22))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r22
    :parameters ()
    :precondition 
      (and
        (in r22)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r21)
        (f_copy)
        (not 
          (in r22))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r23
    :parameters ()
    :precondition 
      (and
        (in r23)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r24)
        (f_copy)
        (not 
          (in r23))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r23
    :parameters ()
    :precondition 
      (and
        (in r23)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r22)
        (f_copy)
        (not 
          (in r23))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r24
    :parameters ()
    :precondition 
      (and
        (in r24)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r25)
        (f_copy)
        (not 
          (in r24))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r24
    :parameters ()
    :precondition 
      (and
        (in r24)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r23)
        (f_copy)
        (not 
          (in r24))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r25
    :parameters ()
    :precondition 
      (and
        (in r25)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r26)
        (f_copy)
        (not 
          (in r25))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r25
    :parameters ()
    :precondition 
      (and
        (in r25)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r24)
        (f_copy)
        (not 
          (in r25))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r26
    :parameters ()
    :precondition 
      (and
        (in r26)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r27)
        (f_copy)
        (not 
          (in r26))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r26
    :parameters ()
    :precondition 
      (and
        (in r26)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r25)
        (f_copy)
        (not 
          (in r26))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r27
    :parameters ()
    :precondition 
      (and
        (in r27)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r28)
        (f_copy)
        (not 
          (in r27))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r27
    :parameters ()
    :precondition 
      (and
        (in r27)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r26)
        (f_copy)
        (not 
          (in r27))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r28
    :parameters ()
    :precondition 
      (and
        (in r28)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r29)
        (f_copy)
        (not 
          (in r28))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r28
    :parameters ()
    :precondition 
      (and
        (in r28)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r27)
        (f_copy)
        (not 
          (in r28))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r29
    :parameters ()
    :precondition 
      (and
        (in r29)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r30)
        (f_copy)
        (not 
          (in r29))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r29
    :parameters ()
    :precondition 
      (and
        (in r29)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r28)
        (f_copy)
        (not 
          (in r29))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r30
    :parameters ()
    :precondition 
      (and
        (in r30)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r31)
        (f_copy)
        (not 
          (in r30))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r30
    :parameters ()
    :precondition 
      (and
        (in r30)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r29)
        (f_copy)
        (not 
          (in r30))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r31
    :parameters ()
    :precondition 
      (and
        (in r31)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r32)
        (f_copy)
        (not 
          (in r31))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r31
    :parameters ()
    :precondition 
      (and
        (in r31)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r30)
        (f_copy)
        (not 
          (in r31))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r32
    :parameters ()
    :precondition 
      (and
        (in r32)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r33)
        (f_copy)
        (not 
          (in r32))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r32
    :parameters ()
    :precondition 
      (and
        (in r32)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r31)
        (f_copy)
        (not 
          (in r32))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r33
    :parameters ()
    :precondition 
      (and
        (in r33)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r34)
        (f_copy)
        (not 
          (in r33))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r33
    :parameters ()
    :precondition 
      (and
        (in r33)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r32)
        (f_copy)
        (not 
          (in r33))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r34
    :parameters ()
    :precondition 
      (and
        (in r34)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r35)
        (f_copy)
        (not 
          (in r34))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r34
    :parameters ()
    :precondition 
      (and
        (in r34)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r33)
        (f_copy)
        (not 
          (in r34))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r35
    :parameters ()
    :precondition 
      (and
        (in r35)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r36)
        (f_copy)
        (not 
          (in r35))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r35
    :parameters ()
    :precondition 
      (and
        (in r35)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r34)
        (f_copy)
        (not 
          (in r35))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r36
    :parameters ()
    :precondition 
      (and
        (in r36)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r37)
        (f_copy)
        (not 
          (in r36))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r36
    :parameters ()
    :precondition 
      (and
        (in r36)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r35)
        (f_copy)
        (not 
          (in r36))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r37
    :parameters ()
    :precondition 
      (and
        (in r37)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r38)
        (f_copy)
        (not 
          (in r37))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r37
    :parameters ()
    :precondition 
      (and
        (in r37)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r36)
        (f_copy)
        (not 
          (in r37))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r38
    :parameters ()
    :precondition 
      (and
        (in r38)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r39)
        (f_copy)
        (not 
          (in r38))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r38
    :parameters ()
    :precondition 
      (and
        (in r38)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r37)
        (f_copy)
        (not 
          (in r38))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r39
    :parameters ()
    :precondition 
      (and
        (in r39)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r40)
        (f_copy)
        (not 
          (in r39))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r39
    :parameters ()
    :precondition 
      (and
        (in r39)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r38)
        (f_copy)
        (not 
          (in r39))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r40
    :parameters ()
    :precondition 
      (and
        (in r40)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r41)
        (f_copy)
        (not 
          (in r40))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r40
    :parameters ()
    :precondition 
      (and
        (in r40)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r39)
        (f_copy)
        (not 
          (in r40))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r41
    :parameters ()
    :precondition 
      (and
        (in r41)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r42)
        (f_copy)
        (not 
          (in r41))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r41
    :parameters ()
    :precondition 
      (and
        (in r41)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r40)
        (f_copy)
        (not 
          (in r41))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r42
    :parameters ()
    :precondition 
      (and
        (in r42)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r43)
        (f_copy)
        (not 
          (in r42))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r42
    :parameters ()
    :precondition 
      (and
        (in r42)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r41)
        (f_copy)
        (not 
          (in r42))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r43
    :parameters ()
    :precondition 
      (and
        (in r43)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r44)
        (f_copy)
        (not 
          (in r43))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r43
    :parameters ()
    :precondition 
      (and
        (in r43)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r42)
        (f_copy)
        (not 
          (in r43))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r44
    :parameters ()
    :precondition 
      (and
        (in r44)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r45)
        (f_copy)
        (not 
          (in r44))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r44
    :parameters ()
    :precondition 
      (and
        (in r44)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r43)
        (f_copy)
        (not 
          (in r44))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r45
    :parameters ()
    :precondition 
      (and
        (in r45)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r46)
        (f_copy)
        (not 
          (in r45))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r45
    :parameters ()
    :precondition 
      (and
        (in r45)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r44)
        (f_copy)
        (not 
          (in r45))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r46
    :parameters ()
    :precondition 
      (and
        (in r46)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r47)
        (f_copy)
        (not 
          (in r46))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r46
    :parameters ()
    :precondition 
      (and
        (in r46)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r45)
        (f_copy)
        (not 
          (in r46))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r47
    :parameters ()
    :precondition 
      (and
        (in r47)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r48)
        (f_copy)
        (not 
          (in r47))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r47
    :parameters ()
    :precondition 
      (and
        (in r47)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r46)
        (f_copy)
        (not 
          (in r47))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r48
    :parameters ()
    :precondition 
      (and
        (in r48)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r49)
        (f_copy)
        (not 
          (in r48))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r48
    :parameters ()
    :precondition 
      (and
        (in r48)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r47)
        (f_copy)
        (not 
          (in r48))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r49
    :parameters ()
    :precondition 
      (and
        (in r49)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r50)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r49))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r49
    :parameters ()
    :precondition 
      (and
        (in r49)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r48)
        (f_copy)
        (not 
          (in r49))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r50
    :parameters ()
    :precondition 
      (and
        (in r50)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r51)
        (f_copy)
        (not 
          (in r50))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r50
    :parameters ()
    :precondition 
      (and
        (in r50)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r49)
        (f_copy)
        (not 
          (in r50))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r51
    :parameters ()
    :precondition 
      (and
        (in r51)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r52)
        (f_copy)
        (not 
          (in r51))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r51
    :parameters ()
    :precondition 
      (and
        (in r51)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r50)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r51))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r52
    :parameters ()
    :precondition 
      (and
        (in r52)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r53)
        (f_copy)
        (not 
          (in r52))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r52
    :parameters ()
    :precondition 
      (and
        (in r52)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r51)
        (f_copy)
        (not 
          (in r52))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r53
    :parameters ()
    :precondition 
      (and
        (in r53)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r54)
        (f_copy)
        (not 
          (in r53))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r53
    :parameters ()
    :precondition 
      (and
        (in r53)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r52)
        (f_copy)
        (not 
          (in r53))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r54
    :parameters ()
    :precondition 
      (and
        (in r54)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r55)
        (f_copy)
        (not 
          (in r54))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r54
    :parameters ()
    :precondition 
      (and
        (in r54)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r53)
        (f_copy)
        (not 
          (in r54))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r55
    :parameters ()
    :precondition 
      (and
        (in r55)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r56)
        (f_copy)
        (not 
          (in r55))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r55
    :parameters ()
    :precondition 
      (and
        (in r55)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r54)
        (f_copy)
        (not 
          (in r55))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r56
    :parameters ()
    :precondition 
      (and
        (in r56)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r57)
        (f_copy)
        (not 
          (in r56))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r56
    :parameters ()
    :precondition 
      (and
        (in r56)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r55)
        (f_copy)
        (not 
          (in r56))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r57
    :parameters ()
    :precondition 
      (and
        (in r57)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r58)
        (f_copy)
        (not 
          (in r57))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r57
    :parameters ()
    :precondition 
      (and
        (in r57)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r56)
        (f_copy)
        (not 
          (in r57))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r58
    :parameters ()
    :precondition 
      (and
        (in r58)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r59)
        (f_copy)
        (not 
          (in r58))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r58
    :parameters ()
    :precondition 
      (and
        (in r58)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r57)
        (f_copy)
        (not 
          (in r58))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r59
    :parameters ()
    :precondition 
      (and
        (in r59)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r60)
        (f_copy)
        (not 
          (in r59))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r59
    :parameters ()
    :precondition 
      (and
        (in r59)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r58)
        (f_copy)
        (not 
          (in r59))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r60
    :parameters ()
    :precondition 
      (and
        (in r60)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r61)
        (f_copy)
        (not 
          (in r60))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r60
    :parameters ()
    :precondition 
      (and
        (in r60)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r59)
        (f_copy)
        (not 
          (in r60))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r61
    :parameters ()
    :precondition 
      (and
        (in r61)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r62)
        (f_copy)
        (not 
          (in r61))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r61
    :parameters ()
    :precondition 
      (and
        (in r61)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r60)
        (f_copy)
        (not 
          (in r61))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r62
    :parameters ()
    :precondition 
      (and
        (in r62)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r63)
        (f_copy)
        (not 
          (in r62))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r62
    :parameters ()
    :precondition 
      (and
        (in r62)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r61)
        (f_copy)
        (not 
          (in r62))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r63
    :parameters ()
    :precondition 
      (and
        (in r63)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r64)
        (f_copy)
        (not 
          (in r63))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r63
    :parameters ()
    :precondition 
      (and
        (in r63)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r62)
        (f_copy)
        (not 
          (in r63))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r64
    :parameters ()
    :precondition 
      (and
        (in r64)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r65)
        (f_copy)
        (not 
          (in r64))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r64
    :parameters ()
    :precondition 
      (and
        (in r64)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r63)
        (f_copy)
        (not 
          (in r64))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r65
    :parameters ()
    :precondition 
      (and
        (in r65)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r66)
        (f_copy)
        (not 
          (in r65))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r65
    :parameters ()
    :precondition 
      (and
        (in r65)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r64)
        (f_copy)
        (not 
          (in r65))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r66
    :parameters ()
    :precondition 
      (and
        (in r66)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r67)
        (f_copy)
        (not 
          (in r66))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r66
    :parameters ()
    :precondition 
      (and
        (in r66)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r65)
        (f_copy)
        (not 
          (in r66))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r67
    :parameters ()
    :precondition 
      (and
        (in r67)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r68)
        (f_copy)
        (not 
          (in r67))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r67
    :parameters ()
    :precondition 
      (and
        (in r67)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r66)
        (f_copy)
        (not 
          (in r67))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r68
    :parameters ()
    :precondition 
      (and
        (in r68)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r69)
        (f_copy)
        (not 
          (in r68))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r68
    :parameters ()
    :precondition 
      (and
        (in r68)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r67)
        (f_copy)
        (not 
          (in r68))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r69
    :parameters ()
    :precondition 
      (and
        (in r69)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r70)
        (f_copy)
        (not 
          (in r69))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r69
    :parameters ()
    :precondition 
      (and
        (in r69)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r68)
        (f_copy)
        (not 
          (in r69))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r70
    :parameters ()
    :precondition 
      (and
        (in r70)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r71)
        (f_copy)
        (not 
          (in r70))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r70
    :parameters ()
    :precondition 
      (and
        (in r70)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r69)
        (f_copy)
        (not 
          (in r70))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r71
    :parameters ()
    :precondition 
      (and
        (in r71)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r72)
        (f_copy)
        (not 
          (in r71))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r71
    :parameters ()
    :precondition 
      (and
        (in r71)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r70)
        (f_copy)
        (not 
          (in r71))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r72
    :parameters ()
    :precondition 
      (and
        (in r72)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r73)
        (f_copy)
        (not 
          (in r72))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r72
    :parameters ()
    :precondition 
      (and
        (in r72)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r71)
        (f_copy)
        (not 
          (in r72))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r73
    :parameters ()
    :precondition 
      (and
        (in r73)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r74)
        (f_copy)
        (not 
          (in r73))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r73
    :parameters ()
    :precondition 
      (and
        (in r73)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r72)
        (f_copy)
        (not 
          (in r73))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r74
    :parameters ()
    :precondition 
      (and
        (in r74)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r75)
        (f_copy)
        (not 
          (in r74))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r74
    :parameters ()
    :precondition 
      (and
        (in r74)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r73)
        (f_copy)
        (not 
          (in r74))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r75
    :parameters ()
    :precondition 
      (and
        (in r75)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r76)
        (f_copy)
        (not 
          (in r75))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r75
    :parameters ()
    :precondition 
      (and
        (in r75)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r74)
        (f_copy)
        (not 
          (in r75))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r76
    :parameters ()
    :precondition 
      (and
        (in r76)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r77)
        (f_copy)
        (not 
          (in r76))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r76
    :parameters ()
    :precondition 
      (and
        (in r76)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r75)
        (f_copy)
        (not 
          (in r76))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r77
    :parameters ()
    :precondition 
      (and
        (in r77)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r78)
        (f_copy)
        (not 
          (in r77))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r77
    :parameters ()
    :precondition 
      (and
        (in r77)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r76)
        (f_copy)
        (not 
          (in r77))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r78
    :parameters ()
    :precondition 
      (and
        (in r78)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r79)
        (f_copy)
        (not 
          (in r78))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r78
    :parameters ()
    :precondition 
      (and
        (in r78)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r77)
        (f_copy)
        (not 
          (in r78))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r79
    :parameters ()
    :precondition 
      (and
        (in r79)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r80)
        (f_copy)
        (not 
          (in r79))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r79
    :parameters ()
    :precondition 
      (and
        (in r79)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r78)
        (f_copy)
        (not 
          (in r79))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r80
    :parameters ()
    :precondition 
      (and
        (in r80)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r81)
        (f_copy)
        (not 
          (in r80))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r80
    :parameters ()
    :precondition 
      (and
        (in r80)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r79)
        (f_copy)
        (not 
          (in r80))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r81
    :parameters ()
    :precondition 
      (and
        (in r81)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r82)
        (f_copy)
        (not 
          (in r81))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r81
    :parameters ()
    :precondition 
      (and
        (in r81)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r80)
        (f_copy)
        (not 
          (in r81))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r82
    :parameters ()
    :precondition 
      (and
        (in r82)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r83)
        (f_copy)
        (not 
          (in r82))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r82
    :parameters ()
    :precondition 
      (and
        (in r82)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r81)
        (f_copy)
        (not 
          (in r82))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r83
    :parameters ()
    :precondition 
      (and
        (in r83)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r84)
        (f_copy)
        (not 
          (in r83))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r83
    :parameters ()
    :precondition 
      (and
        (in r83)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r82)
        (f_copy)
        (not 
          (in r83))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r84
    :parameters ()
    :precondition 
      (and
        (in r84)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r85)
        (f_copy)
        (not 
          (in r84))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r84
    :parameters ()
    :precondition 
      (and
        (in r84)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r83)
        (f_copy)
        (not 
          (in r84))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r85
    :parameters ()
    :precondition 
      (and
        (in r85)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r86)
        (f_copy)
        (not 
          (in r85))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r85
    :parameters ()
    :precondition 
      (and
        (in r85)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r84)
        (f_copy)
        (not 
          (in r85))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r86
    :parameters ()
    :precondition 
      (and
        (in r86)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r87)
        (f_copy)
        (not 
          (in r86))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r86
    :parameters ()
    :precondition 
      (and
        (in r86)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r85)
        (f_copy)
        (not 
          (in r86))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r87
    :parameters ()
    :precondition 
      (and
        (in r87)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r88)
        (f_copy)
        (not 
          (in r87))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r87
    :parameters ()
    :precondition 
      (and
        (in r87)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r86)
        (f_copy)
        (not 
          (in r87))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r88
    :parameters ()
    :precondition 
      (and
        (in r88)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r89)
        (f_copy)
        (not 
          (in r88))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r88
    :parameters ()
    :precondition 
      (and
        (in r88)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r87)
        (f_copy)
        (not 
          (in r88))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r89
    :parameters ()
    :precondition 
      (and
        (in r89)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r90)
        (f_copy)
        (not 
          (in r89))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r89
    :parameters ()
    :precondition 
      (and
        (in r89)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r88)
        (f_copy)
        (not 
          (in r89))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r90
    :parameters ()
    :precondition 
      (and
        (in r90)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r91)
        (f_copy)
        (not 
          (in r90))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r90
    :parameters ()
    :precondition 
      (and
        (in r90)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r89)
        (f_copy)
        (not 
          (in r90))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r91
    :parameters ()
    :precondition 
      (and
        (in r91)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r92)
        (f_copy)
        (not 
          (in r91))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r91
    :parameters ()
    :precondition 
      (and
        (in r91)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r90)
        (f_copy)
        (not 
          (in r91))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r92
    :parameters ()
    :precondition 
      (and
        (in r92)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r93)
        (f_copy)
        (not 
          (in r92))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r92
    :parameters ()
    :precondition 
      (and
        (in r92)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r91)
        (f_copy)
        (not 
          (in r92))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r93
    :parameters ()
    :precondition 
      (and
        (in r93)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r94)
        (f_copy)
        (not 
          (in r93))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r93
    :parameters ()
    :precondition 
      (and
        (in r93)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r92)
        (f_copy)
        (not 
          (in r93))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r94
    :parameters ()
    :precondition 
      (and
        (in r94)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r95)
        (f_copy)
        (not 
          (in r94))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r94
    :parameters ()
    :precondition 
      (and
        (in r94)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r93)
        (f_copy)
        (not 
          (in r94))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r95
    :parameters ()
    :precondition 
      (and
        (in r95)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r96)
        (f_copy)
        (not 
          (in r95))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r95
    :parameters ()
    :precondition 
      (and
        (in r95)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r94)
        (f_copy)
        (not 
          (in r95))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r96
    :parameters ()
    :precondition 
      (and
        (in r96)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r97)
        (f_copy)
        (not 
          (in r96))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r96
    :parameters ()
    :precondition 
      (and
        (in r96)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r95)
        (f_copy)
        (not 
          (in r96))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r97
    :parameters ()
    :precondition 
      (and
        (in r97)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r98)
        (f_copy)
        (not 
          (in r97))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r97
    :parameters ()
    :precondition 
      (and
        (in r97)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r96)
        (f_copy)
        (not 
          (in r97))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r98
    :parameters ()
    :precondition 
      (and
        (in r98)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r99)
        (f_copy)
        (not 
          (in r98))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r98
    :parameters ()
    :precondition 
      (and
        (in r98)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r97)
        (f_copy)
        (not 
          (in r98))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r99
    :parameters ()
    :precondition 
      (and
        (in r99)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r100)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r99))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r99
    :parameters ()
    :precondition 
      (and
        (in r99)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r98)
        (f_copy)
        (not 
          (in r99))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r1)
        (f_copy)
        (not 
          (in r100))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r99)
        (f_copy)
        (not 
          (in r100))
        (not 
          (f_world))
      )
    )
  (:action stay
    :parameters ()
    :precondition 
      (and
        (not 
          (in r50))
        (not 
          (in r100))
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r50-seen
    :parameters ()
    :precondition 
      (and
        (in r50)
        (seen)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r50-not-seen
    :parameters ()
    :precondition 
      (and
        (in r50)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r100-seen
    :parameters ()
    :precondition 
      (and
        (in r100)
        (seen)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r100-not-seen
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action o_copy
    :parameters ()
    :precondition 
      (and
        (f_ok)
        (f_copy))
    :effect
      (and
        (f_sync)
        (q_1t)
        (when
          (q_1)
          (q_1s))
        (when
          (q_2)
          (q_2s))
        (when
          (q_3)
          (q_3s))
        (when
          (q_4)
          (q_4s))
        (not 
          (f_copy))
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3))
        (not 
          (q_4))
      )
    )
  (:action o_goal
    :parameters ()
    :precondition 
      (and
        (f_world)
        (f_ok)
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3)))
    :effect
(f_goal)    )
  (:action o_sync_q_1s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_1t))
    :effect
      (and
        (when
          (q_1s)
          (q_2s))
        (q_2t)
        (when
          (q_1s)
          (not 
            (q_1s)))
        (not 
          (q_1t))
      )
    )
  (:action o_sync_q_1s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_1t))
    :effect
      (and
        (when
          (q_1s)
          (q_3))
        (when
          (q_1s)
          (q_1))
        (q_2t)
        (when
          (q_1s)
          (not 
            (q_1s)))
        (not 
          (q_1t))
      )
    )
  (:action o_sync_q_2s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_2t))
    :effect
      (and
        (q_3t)
        (when
          (q_2s)
          (not 
            (q_2s)))
        (when
          (and
            (q_2s)
            (not 
              (seen)))
          (not 
            (f_ok)))
        (not 
          (q_2t))
      )
    )
  (:action o_sync_q_3s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_3t))
    :effect
      (and
        (q_4t)
        (when
          (q_3s)
          (not 
            (q_3s)))
        (not 
          (q_3t))
      )
    )
  (:action o_sync_q_4s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_4t))
    :effect
      (and
        (f_world)
        (when
          (q_1s)
          (q_1))
        (when
          (q_2s)
          (q_2))
        (when
          (q_3s)
          (q_3))
        (when
          (q_4s)
          (q_4))
        (when
          (q_4s)
          (not 
            (f_ok)))
        (not 
          (q_4t))
        (not 
          (f_sync))
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
      )
    )
)