(define (domain waldo-seq-200-instance-nobucci_dp2)
  (:types
    room - NO_TYPE
  )

  (:predicates
    (seen)
    (in ?x0 - room)
    (f_copy)
    (f_sync)
    (f_world)
    (f_ok)
    (f_goal)
    (q_1)
    (q_1s)
    (q_1t)
    (q_2)
    (q_2s)
    (q_2t)
    (q_3)
    (q_3s)
    (q_3t)
    (q_4)
    (q_4s)
    (q_4t)
  )
  (:action move-right-from-r1
    :parameters ()
    :precondition 
      (and
        (in r1)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r2)
        (f_copy)
        (not 
          (in r1))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r1
    :parameters ()
    :precondition 
      (and
        (in r1)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r200)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r1))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r2
    :parameters ()
    :precondition 
      (and
        (in r2)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r3)
        (f_copy)
        (not 
          (in r2))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r2
    :parameters ()
    :precondition 
      (and
        (in r2)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r1)
        (f_copy)
        (not 
          (in r2))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r3
    :parameters ()
    :precondition 
      (and
        (in r3)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r4)
        (f_copy)
        (not 
          (in r3))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r3
    :parameters ()
    :precondition 
      (and
        (in r3)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r2)
        (f_copy)
        (not 
          (in r3))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r4
    :parameters ()
    :precondition 
      (and
        (in r4)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r5)
        (f_copy)
        (not 
          (in r4))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r4
    :parameters ()
    :precondition 
      (and
        (in r4)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r3)
        (f_copy)
        (not 
          (in r4))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r5
    :parameters ()
    :precondition 
      (and
        (in r5)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r6)
        (f_copy)
        (not 
          (in r5))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r5
    :parameters ()
    :precondition 
      (and
        (in r5)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r4)
        (f_copy)
        (not 
          (in r5))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r6
    :parameters ()
    :precondition 
      (and
        (in r6)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r7)
        (f_copy)
        (not 
          (in r6))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r6
    :parameters ()
    :precondition 
      (and
        (in r6)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r5)
        (f_copy)
        (not 
          (in r6))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r7
    :parameters ()
    :precondition 
      (and
        (in r7)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r8)
        (f_copy)
        (not 
          (in r7))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r7
    :parameters ()
    :precondition 
      (and
        (in r7)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r6)
        (f_copy)
        (not 
          (in r7))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r8
    :parameters ()
    :precondition 
      (and
        (in r8)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r9)
        (f_copy)
        (not 
          (in r8))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r8
    :parameters ()
    :precondition 
      (and
        (in r8)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r7)
        (f_copy)
        (not 
          (in r8))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r9
    :parameters ()
    :precondition 
      (and
        (in r9)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r10)
        (f_copy)
        (not 
          (in r9))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r9
    :parameters ()
    :precondition 
      (and
        (in r9)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r8)
        (f_copy)
        (not 
          (in r9))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r10
    :parameters ()
    :precondition 
      (and
        (in r10)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r11)
        (f_copy)
        (not 
          (in r10))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r10
    :parameters ()
    :precondition 
      (and
        (in r10)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r9)
        (f_copy)
        (not 
          (in r10))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r11
    :parameters ()
    :precondition 
      (and
        (in r11)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r12)
        (f_copy)
        (not 
          (in r11))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r11
    :parameters ()
    :precondition 
      (and
        (in r11)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r10)
        (f_copy)
        (not 
          (in r11))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r12
    :parameters ()
    :precondition 
      (and
        (in r12)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r13)
        (f_copy)
        (not 
          (in r12))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r12
    :parameters ()
    :precondition 
      (and
        (in r12)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r11)
        (f_copy)
        (not 
          (in r12))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r13
    :parameters ()
    :precondition 
      (and
        (in r13)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r14)
        (f_copy)
        (not 
          (in r13))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r13
    :parameters ()
    :precondition 
      (and
        (in r13)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r12)
        (f_copy)
        (not 
          (in r13))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r14
    :parameters ()
    :precondition 
      (and
        (in r14)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r15)
        (f_copy)
        (not 
          (in r14))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r14
    :parameters ()
    :precondition 
      (and
        (in r14)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r13)
        (f_copy)
        (not 
          (in r14))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r15
    :parameters ()
    :precondition 
      (and
        (in r15)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r16)
        (f_copy)
        (not 
          (in r15))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r15
    :parameters ()
    :precondition 
      (and
        (in r15)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r14)
        (f_copy)
        (not 
          (in r15))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r16
    :parameters ()
    :precondition 
      (and
        (in r16)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r17)
        (f_copy)
        (not 
          (in r16))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r16
    :parameters ()
    :precondition 
      (and
        (in r16)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r15)
        (f_copy)
        (not 
          (in r16))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r17
    :parameters ()
    :precondition 
      (and
        (in r17)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r18)
        (f_copy)
        (not 
          (in r17))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r17
    :parameters ()
    :precondition 
      (and
        (in r17)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r16)
        (f_copy)
        (not 
          (in r17))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r18
    :parameters ()
    :precondition 
      (and
        (in r18)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r19)
        (f_copy)
        (not 
          (in r18))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r18
    :parameters ()
    :precondition 
      (and
        (in r18)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r17)
        (f_copy)
        (not 
          (in r18))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r19
    :parameters ()
    :precondition 
      (and
        (in r19)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r20)
        (f_copy)
        (not 
          (in r19))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r19
    :parameters ()
    :precondition 
      (and
        (in r19)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r18)
        (f_copy)
        (not 
          (in r19))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r20
    :parameters ()
    :precondition 
      (and
        (in r20)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r21)
        (f_copy)
        (not 
          (in r20))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r20
    :parameters ()
    :precondition 
      (and
        (in r20)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r19)
        (f_copy)
        (not 
          (in r20))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r21
    :parameters ()
    :precondition 
      (and
        (in r21)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r22)
        (f_copy)
        (not 
          (in r21))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r21
    :parameters ()
    :precondition 
      (and
        (in r21)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r20)
        (f_copy)
        (not 
          (in r21))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r22
    :parameters ()
    :precondition 
      (and
        (in r22)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r23)
        (f_copy)
        (not 
          (in r22))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r22
    :parameters ()
    :precondition 
      (and
        (in r22)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r21)
        (f_copy)
        (not 
          (in r22))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r23
    :parameters ()
    :precondition 
      (and
        (in r23)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r24)
        (f_copy)
        (not 
          (in r23))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r23
    :parameters ()
    :precondition 
      (and
        (in r23)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r22)
        (f_copy)
        (not 
          (in r23))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r24
    :parameters ()
    :precondition 
      (and
        (in r24)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r25)
        (f_copy)
        (not 
          (in r24))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r24
    :parameters ()
    :precondition 
      (and
        (in r24)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r23)
        (f_copy)
        (not 
          (in r24))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r25
    :parameters ()
    :precondition 
      (and
        (in r25)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r26)
        (f_copy)
        (not 
          (in r25))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r25
    :parameters ()
    :precondition 
      (and
        (in r25)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r24)
        (f_copy)
        (not 
          (in r25))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r26
    :parameters ()
    :precondition 
      (and
        (in r26)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r27)
        (f_copy)
        (not 
          (in r26))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r26
    :parameters ()
    :precondition 
      (and
        (in r26)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r25)
        (f_copy)
        (not 
          (in r26))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r27
    :parameters ()
    :precondition 
      (and
        (in r27)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r28)
        (f_copy)
        (not 
          (in r27))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r27
    :parameters ()
    :precondition 
      (and
        (in r27)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r26)
        (f_copy)
        (not 
          (in r27))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r28
    :parameters ()
    :precondition 
      (and
        (in r28)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r29)
        (f_copy)
        (not 
          (in r28))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r28
    :parameters ()
    :precondition 
      (and
        (in r28)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r27)
        (f_copy)
        (not 
          (in r28))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r29
    :parameters ()
    :precondition 
      (and
        (in r29)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r30)
        (f_copy)
        (not 
          (in r29))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r29
    :parameters ()
    :precondition 
      (and
        (in r29)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r28)
        (f_copy)
        (not 
          (in r29))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r30
    :parameters ()
    :precondition 
      (and
        (in r30)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r31)
        (f_copy)
        (not 
          (in r30))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r30
    :parameters ()
    :precondition 
      (and
        (in r30)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r29)
        (f_copy)
        (not 
          (in r30))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r31
    :parameters ()
    :precondition 
      (and
        (in r31)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r32)
        (f_copy)
        (not 
          (in r31))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r31
    :parameters ()
    :precondition 
      (and
        (in r31)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r30)
        (f_copy)
        (not 
          (in r31))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r32
    :parameters ()
    :precondition 
      (and
        (in r32)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r33)
        (f_copy)
        (not 
          (in r32))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r32
    :parameters ()
    :precondition 
      (and
        (in r32)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r31)
        (f_copy)
        (not 
          (in r32))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r33
    :parameters ()
    :precondition 
      (and
        (in r33)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r34)
        (f_copy)
        (not 
          (in r33))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r33
    :parameters ()
    :precondition 
      (and
        (in r33)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r32)
        (f_copy)
        (not 
          (in r33))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r34
    :parameters ()
    :precondition 
      (and
        (in r34)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r35)
        (f_copy)
        (not 
          (in r34))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r34
    :parameters ()
    :precondition 
      (and
        (in r34)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r33)
        (f_copy)
        (not 
          (in r34))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r35
    :parameters ()
    :precondition 
      (and
        (in r35)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r36)
        (f_copy)
        (not 
          (in r35))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r35
    :parameters ()
    :precondition 
      (and
        (in r35)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r34)
        (f_copy)
        (not 
          (in r35))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r36
    :parameters ()
    :precondition 
      (and
        (in r36)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r37)
        (f_copy)
        (not 
          (in r36))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r36
    :parameters ()
    :precondition 
      (and
        (in r36)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r35)
        (f_copy)
        (not 
          (in r36))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r37
    :parameters ()
    :precondition 
      (and
        (in r37)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r38)
        (f_copy)
        (not 
          (in r37))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r37
    :parameters ()
    :precondition 
      (and
        (in r37)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r36)
        (f_copy)
        (not 
          (in r37))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r38
    :parameters ()
    :precondition 
      (and
        (in r38)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r39)
        (f_copy)
        (not 
          (in r38))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r38
    :parameters ()
    :precondition 
      (and
        (in r38)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r37)
        (f_copy)
        (not 
          (in r38))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r39
    :parameters ()
    :precondition 
      (and
        (in r39)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r40)
        (f_copy)
        (not 
          (in r39))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r39
    :parameters ()
    :precondition 
      (and
        (in r39)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r38)
        (f_copy)
        (not 
          (in r39))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r40
    :parameters ()
    :precondition 
      (and
        (in r40)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r41)
        (f_copy)
        (not 
          (in r40))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r40
    :parameters ()
    :precondition 
      (and
        (in r40)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r39)
        (f_copy)
        (not 
          (in r40))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r41
    :parameters ()
    :precondition 
      (and
        (in r41)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r42)
        (f_copy)
        (not 
          (in r41))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r41
    :parameters ()
    :precondition 
      (and
        (in r41)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r40)
        (f_copy)
        (not 
          (in r41))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r42
    :parameters ()
    :precondition 
      (and
        (in r42)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r43)
        (f_copy)
        (not 
          (in r42))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r42
    :parameters ()
    :precondition 
      (and
        (in r42)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r41)
        (f_copy)
        (not 
          (in r42))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r43
    :parameters ()
    :precondition 
      (and
        (in r43)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r44)
        (f_copy)
        (not 
          (in r43))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r43
    :parameters ()
    :precondition 
      (and
        (in r43)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r42)
        (f_copy)
        (not 
          (in r43))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r44
    :parameters ()
    :precondition 
      (and
        (in r44)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r45)
        (f_copy)
        (not 
          (in r44))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r44
    :parameters ()
    :precondition 
      (and
        (in r44)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r43)
        (f_copy)
        (not 
          (in r44))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r45
    :parameters ()
    :precondition 
      (and
        (in r45)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r46)
        (f_copy)
        (not 
          (in r45))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r45
    :parameters ()
    :precondition 
      (and
        (in r45)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r44)
        (f_copy)
        (not 
          (in r45))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r46
    :parameters ()
    :precondition 
      (and
        (in r46)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r47)
        (f_copy)
        (not 
          (in r46))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r46
    :parameters ()
    :precondition 
      (and
        (in r46)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r45)
        (f_copy)
        (not 
          (in r46))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r47
    :parameters ()
    :precondition 
      (and
        (in r47)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r48)
        (f_copy)
        (not 
          (in r47))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r47
    :parameters ()
    :precondition 
      (and
        (in r47)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r46)
        (f_copy)
        (not 
          (in r47))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r48
    :parameters ()
    :precondition 
      (and
        (in r48)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r49)
        (f_copy)
        (not 
          (in r48))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r48
    :parameters ()
    :precondition 
      (and
        (in r48)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r47)
        (f_copy)
        (not 
          (in r48))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r49
    :parameters ()
    :precondition 
      (and
        (in r49)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r50)
        (f_copy)
        (not 
          (in r49))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r49
    :parameters ()
    :precondition 
      (and
        (in r49)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r48)
        (f_copy)
        (not 
          (in r49))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r50
    :parameters ()
    :precondition 
      (and
        (in r50)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r51)
        (f_copy)
        (not 
          (in r50))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r50
    :parameters ()
    :precondition 
      (and
        (in r50)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r49)
        (f_copy)
        (not 
          (in r50))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r51
    :parameters ()
    :precondition 
      (and
        (in r51)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r52)
        (f_copy)
        (not 
          (in r51))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r51
    :parameters ()
    :precondition 
      (and
        (in r51)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r50)
        (f_copy)
        (not 
          (in r51))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r52
    :parameters ()
    :precondition 
      (and
        (in r52)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r53)
        (f_copy)
        (not 
          (in r52))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r52
    :parameters ()
    :precondition 
      (and
        (in r52)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r51)
        (f_copy)
        (not 
          (in r52))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r53
    :parameters ()
    :precondition 
      (and
        (in r53)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r54)
        (f_copy)
        (not 
          (in r53))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r53
    :parameters ()
    :precondition 
      (and
        (in r53)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r52)
        (f_copy)
        (not 
          (in r53))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r54
    :parameters ()
    :precondition 
      (and
        (in r54)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r55)
        (f_copy)
        (not 
          (in r54))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r54
    :parameters ()
    :precondition 
      (and
        (in r54)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r53)
        (f_copy)
        (not 
          (in r54))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r55
    :parameters ()
    :precondition 
      (and
        (in r55)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r56)
        (f_copy)
        (not 
          (in r55))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r55
    :parameters ()
    :precondition 
      (and
        (in r55)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r54)
        (f_copy)
        (not 
          (in r55))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r56
    :parameters ()
    :precondition 
      (and
        (in r56)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r57)
        (f_copy)
        (not 
          (in r56))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r56
    :parameters ()
    :precondition 
      (and
        (in r56)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r55)
        (f_copy)
        (not 
          (in r56))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r57
    :parameters ()
    :precondition 
      (and
        (in r57)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r58)
        (f_copy)
        (not 
          (in r57))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r57
    :parameters ()
    :precondition 
      (and
        (in r57)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r56)
        (f_copy)
        (not 
          (in r57))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r58
    :parameters ()
    :precondition 
      (and
        (in r58)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r59)
        (f_copy)
        (not 
          (in r58))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r58
    :parameters ()
    :precondition 
      (and
        (in r58)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r57)
        (f_copy)
        (not 
          (in r58))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r59
    :parameters ()
    :precondition 
      (and
        (in r59)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r60)
        (f_copy)
        (not 
          (in r59))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r59
    :parameters ()
    :precondition 
      (and
        (in r59)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r58)
        (f_copy)
        (not 
          (in r59))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r60
    :parameters ()
    :precondition 
      (and
        (in r60)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r61)
        (f_copy)
        (not 
          (in r60))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r60
    :parameters ()
    :precondition 
      (and
        (in r60)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r59)
        (f_copy)
        (not 
          (in r60))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r61
    :parameters ()
    :precondition 
      (and
        (in r61)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r62)
        (f_copy)
        (not 
          (in r61))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r61
    :parameters ()
    :precondition 
      (and
        (in r61)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r60)
        (f_copy)
        (not 
          (in r61))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r62
    :parameters ()
    :precondition 
      (and
        (in r62)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r63)
        (f_copy)
        (not 
          (in r62))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r62
    :parameters ()
    :precondition 
      (and
        (in r62)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r61)
        (f_copy)
        (not 
          (in r62))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r63
    :parameters ()
    :precondition 
      (and
        (in r63)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r64)
        (f_copy)
        (not 
          (in r63))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r63
    :parameters ()
    :precondition 
      (and
        (in r63)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r62)
        (f_copy)
        (not 
          (in r63))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r64
    :parameters ()
    :precondition 
      (and
        (in r64)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r65)
        (f_copy)
        (not 
          (in r64))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r64
    :parameters ()
    :precondition 
      (and
        (in r64)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r63)
        (f_copy)
        (not 
          (in r64))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r65
    :parameters ()
    :precondition 
      (and
        (in r65)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r66)
        (f_copy)
        (not 
          (in r65))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r65
    :parameters ()
    :precondition 
      (and
        (in r65)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r64)
        (f_copy)
        (not 
          (in r65))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r66
    :parameters ()
    :precondition 
      (and
        (in r66)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r67)
        (f_copy)
        (not 
          (in r66))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r66
    :parameters ()
    :precondition 
      (and
        (in r66)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r65)
        (f_copy)
        (not 
          (in r66))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r67
    :parameters ()
    :precondition 
      (and
        (in r67)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r68)
        (f_copy)
        (not 
          (in r67))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r67
    :parameters ()
    :precondition 
      (and
        (in r67)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r66)
        (f_copy)
        (not 
          (in r67))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r68
    :parameters ()
    :precondition 
      (and
        (in r68)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r69)
        (f_copy)
        (not 
          (in r68))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r68
    :parameters ()
    :precondition 
      (and
        (in r68)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r67)
        (f_copy)
        (not 
          (in r68))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r69
    :parameters ()
    :precondition 
      (and
        (in r69)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r70)
        (f_copy)
        (not 
          (in r69))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r69
    :parameters ()
    :precondition 
      (and
        (in r69)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r68)
        (f_copy)
        (not 
          (in r69))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r70
    :parameters ()
    :precondition 
      (and
        (in r70)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r71)
        (f_copy)
        (not 
          (in r70))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r70
    :parameters ()
    :precondition 
      (and
        (in r70)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r69)
        (f_copy)
        (not 
          (in r70))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r71
    :parameters ()
    :precondition 
      (and
        (in r71)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r72)
        (f_copy)
        (not 
          (in r71))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r71
    :parameters ()
    :precondition 
      (and
        (in r71)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r70)
        (f_copy)
        (not 
          (in r71))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r72
    :parameters ()
    :precondition 
      (and
        (in r72)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r73)
        (f_copy)
        (not 
          (in r72))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r72
    :parameters ()
    :precondition 
      (and
        (in r72)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r71)
        (f_copy)
        (not 
          (in r72))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r73
    :parameters ()
    :precondition 
      (and
        (in r73)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r74)
        (f_copy)
        (not 
          (in r73))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r73
    :parameters ()
    :precondition 
      (and
        (in r73)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r72)
        (f_copy)
        (not 
          (in r73))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r74
    :parameters ()
    :precondition 
      (and
        (in r74)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r75)
        (f_copy)
        (not 
          (in r74))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r74
    :parameters ()
    :precondition 
      (and
        (in r74)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r73)
        (f_copy)
        (not 
          (in r74))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r75
    :parameters ()
    :precondition 
      (and
        (in r75)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r76)
        (f_copy)
        (not 
          (in r75))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r75
    :parameters ()
    :precondition 
      (and
        (in r75)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r74)
        (f_copy)
        (not 
          (in r75))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r76
    :parameters ()
    :precondition 
      (and
        (in r76)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r77)
        (f_copy)
        (not 
          (in r76))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r76
    :parameters ()
    :precondition 
      (and
        (in r76)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r75)
        (f_copy)
        (not 
          (in r76))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r77
    :parameters ()
    :precondition 
      (and
        (in r77)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r78)
        (f_copy)
        (not 
          (in r77))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r77
    :parameters ()
    :precondition 
      (and
        (in r77)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r76)
        (f_copy)
        (not 
          (in r77))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r78
    :parameters ()
    :precondition 
      (and
        (in r78)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r79)
        (f_copy)
        (not 
          (in r78))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r78
    :parameters ()
    :precondition 
      (and
        (in r78)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r77)
        (f_copy)
        (not 
          (in r78))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r79
    :parameters ()
    :precondition 
      (and
        (in r79)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r80)
        (f_copy)
        (not 
          (in r79))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r79
    :parameters ()
    :precondition 
      (and
        (in r79)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r78)
        (f_copy)
        (not 
          (in r79))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r80
    :parameters ()
    :precondition 
      (and
        (in r80)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r81)
        (f_copy)
        (not 
          (in r80))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r80
    :parameters ()
    :precondition 
      (and
        (in r80)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r79)
        (f_copy)
        (not 
          (in r80))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r81
    :parameters ()
    :precondition 
      (and
        (in r81)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r82)
        (f_copy)
        (not 
          (in r81))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r81
    :parameters ()
    :precondition 
      (and
        (in r81)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r80)
        (f_copy)
        (not 
          (in r81))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r82
    :parameters ()
    :precondition 
      (and
        (in r82)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r83)
        (f_copy)
        (not 
          (in r82))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r82
    :parameters ()
    :precondition 
      (and
        (in r82)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r81)
        (f_copy)
        (not 
          (in r82))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r83
    :parameters ()
    :precondition 
      (and
        (in r83)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r84)
        (f_copy)
        (not 
          (in r83))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r83
    :parameters ()
    :precondition 
      (and
        (in r83)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r82)
        (f_copy)
        (not 
          (in r83))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r84
    :parameters ()
    :precondition 
      (and
        (in r84)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r85)
        (f_copy)
        (not 
          (in r84))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r84
    :parameters ()
    :precondition 
      (and
        (in r84)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r83)
        (f_copy)
        (not 
          (in r84))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r85
    :parameters ()
    :precondition 
      (and
        (in r85)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r86)
        (f_copy)
        (not 
          (in r85))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r85
    :parameters ()
    :precondition 
      (and
        (in r85)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r84)
        (f_copy)
        (not 
          (in r85))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r86
    :parameters ()
    :precondition 
      (and
        (in r86)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r87)
        (f_copy)
        (not 
          (in r86))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r86
    :parameters ()
    :precondition 
      (and
        (in r86)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r85)
        (f_copy)
        (not 
          (in r86))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r87
    :parameters ()
    :precondition 
      (and
        (in r87)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r88)
        (f_copy)
        (not 
          (in r87))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r87
    :parameters ()
    :precondition 
      (and
        (in r87)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r86)
        (f_copy)
        (not 
          (in r87))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r88
    :parameters ()
    :precondition 
      (and
        (in r88)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r89)
        (f_copy)
        (not 
          (in r88))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r88
    :parameters ()
    :precondition 
      (and
        (in r88)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r87)
        (f_copy)
        (not 
          (in r88))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r89
    :parameters ()
    :precondition 
      (and
        (in r89)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r90)
        (f_copy)
        (not 
          (in r89))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r89
    :parameters ()
    :precondition 
      (and
        (in r89)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r88)
        (f_copy)
        (not 
          (in r89))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r90
    :parameters ()
    :precondition 
      (and
        (in r90)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r91)
        (f_copy)
        (not 
          (in r90))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r90
    :parameters ()
    :precondition 
      (and
        (in r90)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r89)
        (f_copy)
        (not 
          (in r90))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r91
    :parameters ()
    :precondition 
      (and
        (in r91)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r92)
        (f_copy)
        (not 
          (in r91))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r91
    :parameters ()
    :precondition 
      (and
        (in r91)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r90)
        (f_copy)
        (not 
          (in r91))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r92
    :parameters ()
    :precondition 
      (and
        (in r92)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r93)
        (f_copy)
        (not 
          (in r92))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r92
    :parameters ()
    :precondition 
      (and
        (in r92)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r91)
        (f_copy)
        (not 
          (in r92))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r93
    :parameters ()
    :precondition 
      (and
        (in r93)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r94)
        (f_copy)
        (not 
          (in r93))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r93
    :parameters ()
    :precondition 
      (and
        (in r93)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r92)
        (f_copy)
        (not 
          (in r93))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r94
    :parameters ()
    :precondition 
      (and
        (in r94)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r95)
        (f_copy)
        (not 
          (in r94))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r94
    :parameters ()
    :precondition 
      (and
        (in r94)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r93)
        (f_copy)
        (not 
          (in r94))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r95
    :parameters ()
    :precondition 
      (and
        (in r95)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r96)
        (f_copy)
        (not 
          (in r95))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r95
    :parameters ()
    :precondition 
      (and
        (in r95)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r94)
        (f_copy)
        (not 
          (in r95))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r96
    :parameters ()
    :precondition 
      (and
        (in r96)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r97)
        (f_copy)
        (not 
          (in r96))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r96
    :parameters ()
    :precondition 
      (and
        (in r96)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r95)
        (f_copy)
        (not 
          (in r96))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r97
    :parameters ()
    :precondition 
      (and
        (in r97)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r98)
        (f_copy)
        (not 
          (in r97))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r97
    :parameters ()
    :precondition 
      (and
        (in r97)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r96)
        (f_copy)
        (not 
          (in r97))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r98
    :parameters ()
    :precondition 
      (and
        (in r98)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r99)
        (f_copy)
        (not 
          (in r98))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r98
    :parameters ()
    :precondition 
      (and
        (in r98)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r97)
        (f_copy)
        (not 
          (in r98))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r99
    :parameters ()
    :precondition 
      (and
        (in r99)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r100)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r99))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r99
    :parameters ()
    :precondition 
      (and
        (in r99)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r98)
        (f_copy)
        (not 
          (in r99))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r101)
        (f_copy)
        (not 
          (in r100))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r99)
        (f_copy)
        (not 
          (in r100))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r101
    :parameters ()
    :precondition 
      (and
        (in r101)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r102)
        (f_copy)
        (not 
          (in r101))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r101
    :parameters ()
    :precondition 
      (and
        (in r101)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r100)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r101))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r102
    :parameters ()
    :precondition 
      (and
        (in r102)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r103)
        (f_copy)
        (not 
          (in r102))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r102
    :parameters ()
    :precondition 
      (and
        (in r102)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r101)
        (f_copy)
        (not 
          (in r102))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r103
    :parameters ()
    :precondition 
      (and
        (in r103)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r104)
        (f_copy)
        (not 
          (in r103))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r103
    :parameters ()
    :precondition 
      (and
        (in r103)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r102)
        (f_copy)
        (not 
          (in r103))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r104
    :parameters ()
    :precondition 
      (and
        (in r104)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r105)
        (f_copy)
        (not 
          (in r104))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r104
    :parameters ()
    :precondition 
      (and
        (in r104)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r103)
        (f_copy)
        (not 
          (in r104))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r105
    :parameters ()
    :precondition 
      (and
        (in r105)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r106)
        (f_copy)
        (not 
          (in r105))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r105
    :parameters ()
    :precondition 
      (and
        (in r105)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r104)
        (f_copy)
        (not 
          (in r105))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r106
    :parameters ()
    :precondition 
      (and
        (in r106)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r107)
        (f_copy)
        (not 
          (in r106))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r106
    :parameters ()
    :precondition 
      (and
        (in r106)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r105)
        (f_copy)
        (not 
          (in r106))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r107
    :parameters ()
    :precondition 
      (and
        (in r107)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r108)
        (f_copy)
        (not 
          (in r107))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r107
    :parameters ()
    :precondition 
      (and
        (in r107)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r106)
        (f_copy)
        (not 
          (in r107))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r108
    :parameters ()
    :precondition 
      (and
        (in r108)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r109)
        (f_copy)
        (not 
          (in r108))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r108
    :parameters ()
    :precondition 
      (and
        (in r108)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r107)
        (f_copy)
        (not 
          (in r108))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r109
    :parameters ()
    :precondition 
      (and
        (in r109)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r110)
        (f_copy)
        (not 
          (in r109))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r109
    :parameters ()
    :precondition 
      (and
        (in r109)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r108)
        (f_copy)
        (not 
          (in r109))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r110
    :parameters ()
    :precondition 
      (and
        (in r110)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r111)
        (f_copy)
        (not 
          (in r110))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r110
    :parameters ()
    :precondition 
      (and
        (in r110)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r109)
        (f_copy)
        (not 
          (in r110))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r111
    :parameters ()
    :precondition 
      (and
        (in r111)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r112)
        (f_copy)
        (not 
          (in r111))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r111
    :parameters ()
    :precondition 
      (and
        (in r111)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r110)
        (f_copy)
        (not 
          (in r111))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r112
    :parameters ()
    :precondition 
      (and
        (in r112)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r113)
        (f_copy)
        (not 
          (in r112))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r112
    :parameters ()
    :precondition 
      (and
        (in r112)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r111)
        (f_copy)
        (not 
          (in r112))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r113
    :parameters ()
    :precondition 
      (and
        (in r113)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r114)
        (f_copy)
        (not 
          (in r113))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r113
    :parameters ()
    :precondition 
      (and
        (in r113)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r112)
        (f_copy)
        (not 
          (in r113))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r114
    :parameters ()
    :precondition 
      (and
        (in r114)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r115)
        (f_copy)
        (not 
          (in r114))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r114
    :parameters ()
    :precondition 
      (and
        (in r114)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r113)
        (f_copy)
        (not 
          (in r114))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r115
    :parameters ()
    :precondition 
      (and
        (in r115)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r116)
        (f_copy)
        (not 
          (in r115))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r115
    :parameters ()
    :precondition 
      (and
        (in r115)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r114)
        (f_copy)
        (not 
          (in r115))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r116
    :parameters ()
    :precondition 
      (and
        (in r116)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r117)
        (f_copy)
        (not 
          (in r116))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r116
    :parameters ()
    :precondition 
      (and
        (in r116)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r115)
        (f_copy)
        (not 
          (in r116))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r117
    :parameters ()
    :precondition 
      (and
        (in r117)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r118)
        (f_copy)
        (not 
          (in r117))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r117
    :parameters ()
    :precondition 
      (and
        (in r117)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r116)
        (f_copy)
        (not 
          (in r117))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r118
    :parameters ()
    :precondition 
      (and
        (in r118)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r119)
        (f_copy)
        (not 
          (in r118))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r118
    :parameters ()
    :precondition 
      (and
        (in r118)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r117)
        (f_copy)
        (not 
          (in r118))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r119
    :parameters ()
    :precondition 
      (and
        (in r119)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r120)
        (f_copy)
        (not 
          (in r119))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r119
    :parameters ()
    :precondition 
      (and
        (in r119)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r118)
        (f_copy)
        (not 
          (in r119))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r120
    :parameters ()
    :precondition 
      (and
        (in r120)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r121)
        (f_copy)
        (not 
          (in r120))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r120
    :parameters ()
    :precondition 
      (and
        (in r120)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r119)
        (f_copy)
        (not 
          (in r120))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r121
    :parameters ()
    :precondition 
      (and
        (in r121)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r122)
        (f_copy)
        (not 
          (in r121))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r121
    :parameters ()
    :precondition 
      (and
        (in r121)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r120)
        (f_copy)
        (not 
          (in r121))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r122
    :parameters ()
    :precondition 
      (and
        (in r122)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r123)
        (f_copy)
        (not 
          (in r122))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r122
    :parameters ()
    :precondition 
      (and
        (in r122)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r121)
        (f_copy)
        (not 
          (in r122))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r123
    :parameters ()
    :precondition 
      (and
        (in r123)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r124)
        (f_copy)
        (not 
          (in r123))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r123
    :parameters ()
    :precondition 
      (and
        (in r123)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r122)
        (f_copy)
        (not 
          (in r123))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r124
    :parameters ()
    :precondition 
      (and
        (in r124)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r125)
        (f_copy)
        (not 
          (in r124))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r124
    :parameters ()
    :precondition 
      (and
        (in r124)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r123)
        (f_copy)
        (not 
          (in r124))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r125
    :parameters ()
    :precondition 
      (and
        (in r125)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r126)
        (f_copy)
        (not 
          (in r125))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r125
    :parameters ()
    :precondition 
      (and
        (in r125)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r124)
        (f_copy)
        (not 
          (in r125))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r126
    :parameters ()
    :precondition 
      (and
        (in r126)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r127)
        (f_copy)
        (not 
          (in r126))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r126
    :parameters ()
    :precondition 
      (and
        (in r126)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r125)
        (f_copy)
        (not 
          (in r126))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r127
    :parameters ()
    :precondition 
      (and
        (in r127)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r128)
        (f_copy)
        (not 
          (in r127))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r127
    :parameters ()
    :precondition 
      (and
        (in r127)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r126)
        (f_copy)
        (not 
          (in r127))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r128
    :parameters ()
    :precondition 
      (and
        (in r128)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r129)
        (f_copy)
        (not 
          (in r128))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r128
    :parameters ()
    :precondition 
      (and
        (in r128)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r127)
        (f_copy)
        (not 
          (in r128))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r129
    :parameters ()
    :precondition 
      (and
        (in r129)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r130)
        (f_copy)
        (not 
          (in r129))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r129
    :parameters ()
    :precondition 
      (and
        (in r129)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r128)
        (f_copy)
        (not 
          (in r129))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r130
    :parameters ()
    :precondition 
      (and
        (in r130)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r131)
        (f_copy)
        (not 
          (in r130))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r130
    :parameters ()
    :precondition 
      (and
        (in r130)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r129)
        (f_copy)
        (not 
          (in r130))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r131
    :parameters ()
    :precondition 
      (and
        (in r131)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r132)
        (f_copy)
        (not 
          (in r131))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r131
    :parameters ()
    :precondition 
      (and
        (in r131)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r130)
        (f_copy)
        (not 
          (in r131))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r132
    :parameters ()
    :precondition 
      (and
        (in r132)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r133)
        (f_copy)
        (not 
          (in r132))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r132
    :parameters ()
    :precondition 
      (and
        (in r132)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r131)
        (f_copy)
        (not 
          (in r132))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r133
    :parameters ()
    :precondition 
      (and
        (in r133)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r134)
        (f_copy)
        (not 
          (in r133))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r133
    :parameters ()
    :precondition 
      (and
        (in r133)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r132)
        (f_copy)
        (not 
          (in r133))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r134
    :parameters ()
    :precondition 
      (and
        (in r134)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r135)
        (f_copy)
        (not 
          (in r134))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r134
    :parameters ()
    :precondition 
      (and
        (in r134)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r133)
        (f_copy)
        (not 
          (in r134))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r135
    :parameters ()
    :precondition 
      (and
        (in r135)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r136)
        (f_copy)
        (not 
          (in r135))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r135
    :parameters ()
    :precondition 
      (and
        (in r135)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r134)
        (f_copy)
        (not 
          (in r135))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r136
    :parameters ()
    :precondition 
      (and
        (in r136)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r137)
        (f_copy)
        (not 
          (in r136))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r136
    :parameters ()
    :precondition 
      (and
        (in r136)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r135)
        (f_copy)
        (not 
          (in r136))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r137
    :parameters ()
    :precondition 
      (and
        (in r137)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r138)
        (f_copy)
        (not 
          (in r137))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r137
    :parameters ()
    :precondition 
      (and
        (in r137)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r136)
        (f_copy)
        (not 
          (in r137))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r138
    :parameters ()
    :precondition 
      (and
        (in r138)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r139)
        (f_copy)
        (not 
          (in r138))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r138
    :parameters ()
    :precondition 
      (and
        (in r138)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r137)
        (f_copy)
        (not 
          (in r138))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r139
    :parameters ()
    :precondition 
      (and
        (in r139)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r140)
        (f_copy)
        (not 
          (in r139))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r139
    :parameters ()
    :precondition 
      (and
        (in r139)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r138)
        (f_copy)
        (not 
          (in r139))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r140
    :parameters ()
    :precondition 
      (and
        (in r140)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r141)
        (f_copy)
        (not 
          (in r140))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r140
    :parameters ()
    :precondition 
      (and
        (in r140)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r139)
        (f_copy)
        (not 
          (in r140))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r141
    :parameters ()
    :precondition 
      (and
        (in r141)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r142)
        (f_copy)
        (not 
          (in r141))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r141
    :parameters ()
    :precondition 
      (and
        (in r141)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r140)
        (f_copy)
        (not 
          (in r141))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r142
    :parameters ()
    :precondition 
      (and
        (in r142)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r143)
        (f_copy)
        (not 
          (in r142))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r142
    :parameters ()
    :precondition 
      (and
        (in r142)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r141)
        (f_copy)
        (not 
          (in r142))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r143
    :parameters ()
    :precondition 
      (and
        (in r143)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r144)
        (f_copy)
        (not 
          (in r143))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r143
    :parameters ()
    :precondition 
      (and
        (in r143)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r142)
        (f_copy)
        (not 
          (in r143))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r144
    :parameters ()
    :precondition 
      (and
        (in r144)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r145)
        (f_copy)
        (not 
          (in r144))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r144
    :parameters ()
    :precondition 
      (and
        (in r144)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r143)
        (f_copy)
        (not 
          (in r144))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r145
    :parameters ()
    :precondition 
      (and
        (in r145)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r146)
        (f_copy)
        (not 
          (in r145))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r145
    :parameters ()
    :precondition 
      (and
        (in r145)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r144)
        (f_copy)
        (not 
          (in r145))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r146
    :parameters ()
    :precondition 
      (and
        (in r146)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r147)
        (f_copy)
        (not 
          (in r146))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r146
    :parameters ()
    :precondition 
      (and
        (in r146)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r145)
        (f_copy)
        (not 
          (in r146))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r147
    :parameters ()
    :precondition 
      (and
        (in r147)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r148)
        (f_copy)
        (not 
          (in r147))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r147
    :parameters ()
    :precondition 
      (and
        (in r147)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r146)
        (f_copy)
        (not 
          (in r147))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r148
    :parameters ()
    :precondition 
      (and
        (in r148)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r149)
        (f_copy)
        (not 
          (in r148))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r148
    :parameters ()
    :precondition 
      (and
        (in r148)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r147)
        (f_copy)
        (not 
          (in r148))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r149
    :parameters ()
    :precondition 
      (and
        (in r149)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r150)
        (f_copy)
        (not 
          (in r149))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r149
    :parameters ()
    :precondition 
      (and
        (in r149)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r148)
        (f_copy)
        (not 
          (in r149))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r150
    :parameters ()
    :precondition 
      (and
        (in r150)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r151)
        (f_copy)
        (not 
          (in r150))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r150
    :parameters ()
    :precondition 
      (and
        (in r150)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r149)
        (f_copy)
        (not 
          (in r150))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r151
    :parameters ()
    :precondition 
      (and
        (in r151)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r152)
        (f_copy)
        (not 
          (in r151))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r151
    :parameters ()
    :precondition 
      (and
        (in r151)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r150)
        (f_copy)
        (not 
          (in r151))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r152
    :parameters ()
    :precondition 
      (and
        (in r152)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r153)
        (f_copy)
        (not 
          (in r152))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r152
    :parameters ()
    :precondition 
      (and
        (in r152)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r151)
        (f_copy)
        (not 
          (in r152))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r153
    :parameters ()
    :precondition 
      (and
        (in r153)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r154)
        (f_copy)
        (not 
          (in r153))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r153
    :parameters ()
    :precondition 
      (and
        (in r153)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r152)
        (f_copy)
        (not 
          (in r153))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r154
    :parameters ()
    :precondition 
      (and
        (in r154)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r155)
        (f_copy)
        (not 
          (in r154))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r154
    :parameters ()
    :precondition 
      (and
        (in r154)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r153)
        (f_copy)
        (not 
          (in r154))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r155
    :parameters ()
    :precondition 
      (and
        (in r155)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r156)
        (f_copy)
        (not 
          (in r155))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r155
    :parameters ()
    :precondition 
      (and
        (in r155)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r154)
        (f_copy)
        (not 
          (in r155))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r156
    :parameters ()
    :precondition 
      (and
        (in r156)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r157)
        (f_copy)
        (not 
          (in r156))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r156
    :parameters ()
    :precondition 
      (and
        (in r156)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r155)
        (f_copy)
        (not 
          (in r156))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r157
    :parameters ()
    :precondition 
      (and
        (in r157)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r158)
        (f_copy)
        (not 
          (in r157))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r157
    :parameters ()
    :precondition 
      (and
        (in r157)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r156)
        (f_copy)
        (not 
          (in r157))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r158
    :parameters ()
    :precondition 
      (and
        (in r158)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r159)
        (f_copy)
        (not 
          (in r158))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r158
    :parameters ()
    :precondition 
      (and
        (in r158)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r157)
        (f_copy)
        (not 
          (in r158))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r159
    :parameters ()
    :precondition 
      (and
        (in r159)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r160)
        (f_copy)
        (not 
          (in r159))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r159
    :parameters ()
    :precondition 
      (and
        (in r159)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r158)
        (f_copy)
        (not 
          (in r159))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r160
    :parameters ()
    :precondition 
      (and
        (in r160)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r161)
        (f_copy)
        (not 
          (in r160))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r160
    :parameters ()
    :precondition 
      (and
        (in r160)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r159)
        (f_copy)
        (not 
          (in r160))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r161
    :parameters ()
    :precondition 
      (and
        (in r161)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r162)
        (f_copy)
        (not 
          (in r161))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r161
    :parameters ()
    :precondition 
      (and
        (in r161)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r160)
        (f_copy)
        (not 
          (in r161))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r162
    :parameters ()
    :precondition 
      (and
        (in r162)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r163)
        (f_copy)
        (not 
          (in r162))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r162
    :parameters ()
    :precondition 
      (and
        (in r162)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r161)
        (f_copy)
        (not 
          (in r162))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r163
    :parameters ()
    :precondition 
      (and
        (in r163)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r164)
        (f_copy)
        (not 
          (in r163))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r163
    :parameters ()
    :precondition 
      (and
        (in r163)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r162)
        (f_copy)
        (not 
          (in r163))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r164
    :parameters ()
    :precondition 
      (and
        (in r164)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r165)
        (f_copy)
        (not 
          (in r164))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r164
    :parameters ()
    :precondition 
      (and
        (in r164)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r163)
        (f_copy)
        (not 
          (in r164))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r165
    :parameters ()
    :precondition 
      (and
        (in r165)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r166)
        (f_copy)
        (not 
          (in r165))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r165
    :parameters ()
    :precondition 
      (and
        (in r165)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r164)
        (f_copy)
        (not 
          (in r165))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r166
    :parameters ()
    :precondition 
      (and
        (in r166)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r167)
        (f_copy)
        (not 
          (in r166))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r166
    :parameters ()
    :precondition 
      (and
        (in r166)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r165)
        (f_copy)
        (not 
          (in r166))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r167
    :parameters ()
    :precondition 
      (and
        (in r167)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r168)
        (f_copy)
        (not 
          (in r167))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r167
    :parameters ()
    :precondition 
      (and
        (in r167)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r166)
        (f_copy)
        (not 
          (in r167))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r168
    :parameters ()
    :precondition 
      (and
        (in r168)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r169)
        (f_copy)
        (not 
          (in r168))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r168
    :parameters ()
    :precondition 
      (and
        (in r168)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r167)
        (f_copy)
        (not 
          (in r168))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r169
    :parameters ()
    :precondition 
      (and
        (in r169)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r170)
        (f_copy)
        (not 
          (in r169))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r169
    :parameters ()
    :precondition 
      (and
        (in r169)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r168)
        (f_copy)
        (not 
          (in r169))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r170
    :parameters ()
    :precondition 
      (and
        (in r170)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r171)
        (f_copy)
        (not 
          (in r170))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r170
    :parameters ()
    :precondition 
      (and
        (in r170)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r169)
        (f_copy)
        (not 
          (in r170))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r171
    :parameters ()
    :precondition 
      (and
        (in r171)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r172)
        (f_copy)
        (not 
          (in r171))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r171
    :parameters ()
    :precondition 
      (and
        (in r171)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r170)
        (f_copy)
        (not 
          (in r171))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r172
    :parameters ()
    :precondition 
      (and
        (in r172)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r173)
        (f_copy)
        (not 
          (in r172))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r172
    :parameters ()
    :precondition 
      (and
        (in r172)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r171)
        (f_copy)
        (not 
          (in r172))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r173
    :parameters ()
    :precondition 
      (and
        (in r173)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r174)
        (f_copy)
        (not 
          (in r173))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r173
    :parameters ()
    :precondition 
      (and
        (in r173)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r172)
        (f_copy)
        (not 
          (in r173))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r174
    :parameters ()
    :precondition 
      (and
        (in r174)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r175)
        (f_copy)
        (not 
          (in r174))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r174
    :parameters ()
    :precondition 
      (and
        (in r174)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r173)
        (f_copy)
        (not 
          (in r174))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r175
    :parameters ()
    :precondition 
      (and
        (in r175)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r176)
        (f_copy)
        (not 
          (in r175))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r175
    :parameters ()
    :precondition 
      (and
        (in r175)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r174)
        (f_copy)
        (not 
          (in r175))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r176
    :parameters ()
    :precondition 
      (and
        (in r176)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r177)
        (f_copy)
        (not 
          (in r176))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r176
    :parameters ()
    :precondition 
      (and
        (in r176)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r175)
        (f_copy)
        (not 
          (in r176))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r177
    :parameters ()
    :precondition 
      (and
        (in r177)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r178)
        (f_copy)
        (not 
          (in r177))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r177
    :parameters ()
    :precondition 
      (and
        (in r177)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r176)
        (f_copy)
        (not 
          (in r177))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r178
    :parameters ()
    :precondition 
      (and
        (in r178)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r179)
        (f_copy)
        (not 
          (in r178))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r178
    :parameters ()
    :precondition 
      (and
        (in r178)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r177)
        (f_copy)
        (not 
          (in r178))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r179
    :parameters ()
    :precondition 
      (and
        (in r179)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r180)
        (f_copy)
        (not 
          (in r179))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r179
    :parameters ()
    :precondition 
      (and
        (in r179)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r178)
        (f_copy)
        (not 
          (in r179))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r180
    :parameters ()
    :precondition 
      (and
        (in r180)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r181)
        (f_copy)
        (not 
          (in r180))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r180
    :parameters ()
    :precondition 
      (and
        (in r180)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r179)
        (f_copy)
        (not 
          (in r180))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r181
    :parameters ()
    :precondition 
      (and
        (in r181)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r182)
        (f_copy)
        (not 
          (in r181))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r181
    :parameters ()
    :precondition 
      (and
        (in r181)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r180)
        (f_copy)
        (not 
          (in r181))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r182
    :parameters ()
    :precondition 
      (and
        (in r182)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r183)
        (f_copy)
        (not 
          (in r182))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r182
    :parameters ()
    :precondition 
      (and
        (in r182)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r181)
        (f_copy)
        (not 
          (in r182))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r183
    :parameters ()
    :precondition 
      (and
        (in r183)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r184)
        (f_copy)
        (not 
          (in r183))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r183
    :parameters ()
    :precondition 
      (and
        (in r183)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r182)
        (f_copy)
        (not 
          (in r183))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r184
    :parameters ()
    :precondition 
      (and
        (in r184)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r185)
        (f_copy)
        (not 
          (in r184))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r184
    :parameters ()
    :precondition 
      (and
        (in r184)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r183)
        (f_copy)
        (not 
          (in r184))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r185
    :parameters ()
    :precondition 
      (and
        (in r185)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r186)
        (f_copy)
        (not 
          (in r185))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r185
    :parameters ()
    :precondition 
      (and
        (in r185)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r184)
        (f_copy)
        (not 
          (in r185))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r186
    :parameters ()
    :precondition 
      (and
        (in r186)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r187)
        (f_copy)
        (not 
          (in r186))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r186
    :parameters ()
    :precondition 
      (and
        (in r186)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r185)
        (f_copy)
        (not 
          (in r186))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r187
    :parameters ()
    :precondition 
      (and
        (in r187)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r188)
        (f_copy)
        (not 
          (in r187))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r187
    :parameters ()
    :precondition 
      (and
        (in r187)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r186)
        (f_copy)
        (not 
          (in r187))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r188
    :parameters ()
    :precondition 
      (and
        (in r188)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r189)
        (f_copy)
        (not 
          (in r188))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r188
    :parameters ()
    :precondition 
      (and
        (in r188)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r187)
        (f_copy)
        (not 
          (in r188))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r189
    :parameters ()
    :precondition 
      (and
        (in r189)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r190)
        (f_copy)
        (not 
          (in r189))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r189
    :parameters ()
    :precondition 
      (and
        (in r189)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r188)
        (f_copy)
        (not 
          (in r189))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r190
    :parameters ()
    :precondition 
      (and
        (in r190)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r191)
        (f_copy)
        (not 
          (in r190))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r190
    :parameters ()
    :precondition 
      (and
        (in r190)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r189)
        (f_copy)
        (not 
          (in r190))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r191
    :parameters ()
    :precondition 
      (and
        (in r191)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r192)
        (f_copy)
        (not 
          (in r191))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r191
    :parameters ()
    :precondition 
      (and
        (in r191)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r190)
        (f_copy)
        (not 
          (in r191))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r192
    :parameters ()
    :precondition 
      (and
        (in r192)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r193)
        (f_copy)
        (not 
          (in r192))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r192
    :parameters ()
    :precondition 
      (and
        (in r192)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r191)
        (f_copy)
        (not 
          (in r192))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r193
    :parameters ()
    :precondition 
      (and
        (in r193)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r194)
        (f_copy)
        (not 
          (in r193))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r193
    :parameters ()
    :precondition 
      (and
        (in r193)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r192)
        (f_copy)
        (not 
          (in r193))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r194
    :parameters ()
    :precondition 
      (and
        (in r194)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r195)
        (f_copy)
        (not 
          (in r194))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r194
    :parameters ()
    :precondition 
      (and
        (in r194)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r193)
        (f_copy)
        (not 
          (in r194))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r195
    :parameters ()
    :precondition 
      (and
        (in r195)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r196)
        (f_copy)
        (not 
          (in r195))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r195
    :parameters ()
    :precondition 
      (and
        (in r195)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r194)
        (f_copy)
        (not 
          (in r195))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r196
    :parameters ()
    :precondition 
      (and
        (in r196)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r197)
        (f_copy)
        (not 
          (in r196))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r196
    :parameters ()
    :precondition 
      (and
        (in r196)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r195)
        (f_copy)
        (not 
          (in r196))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r197
    :parameters ()
    :precondition 
      (and
        (in r197)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r198)
        (f_copy)
        (not 
          (in r197))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r197
    :parameters ()
    :precondition 
      (and
        (in r197)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r196)
        (f_copy)
        (not 
          (in r197))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r198
    :parameters ()
    :precondition 
      (and
        (in r198)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r199)
        (f_copy)
        (not 
          (in r198))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r198
    :parameters ()
    :precondition 
      (and
        (in r198)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r197)
        (f_copy)
        (not 
          (in r198))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r199
    :parameters ()
    :precondition 
      (and
        (in r199)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r200)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r199))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r199
    :parameters ()
    :precondition 
      (and
        (in r199)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r198)
        (f_copy)
        (not 
          (in r199))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r200
    :parameters ()
    :precondition 
      (and
        (in r200)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r1)
        (f_copy)
        (not 
          (in r200))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r200
    :parameters ()
    :precondition 
      (and
        (in r200)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r199)
        (f_copy)
        (not 
          (in r200))
        (not 
          (f_world))
      )
    )
  (:action stay
    :parameters ()
    :precondition 
      (and
        (not 
          (in r100))
        (not 
          (in r200))
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r100-seen
    :parameters ()
    :precondition 
      (and
        (in r100)
        (seen)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r100-not-seen
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r200-seen
    :parameters ()
    :precondition 
      (and
        (in r200)
        (seen)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r200-not-seen
    :parameters ()
    :precondition 
      (and
        (in r200)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action o_copy
    :parameters ()
    :precondition 
      (and
        (f_ok)
        (f_copy))
    :effect
      (and
        (f_sync)
        (q_1t)
        (when
          (q_1)
          (q_1s))
        (when
          (q_2)
          (q_2s))
        (when
          (q_3)
          (q_3s))
        (when
          (q_4)
          (q_4s))
        (not 
          (f_copy))
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3))
        (not 
          (q_4))
      )
    )
  (:action o_goal
    :parameters ()
    :precondition 
      (and
        (f_world)
        (f_ok)
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3)))
    :effect
(f_goal)    )
  (:action o_sync_q_1s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_1t))
    :effect
      (and
        (when
          (q_1s)
          (q_2s))
        (q_2t)
        (when
          (q_1s)
          (not 
            (q_1s)))
        (not 
          (q_1t))
      )
    )
  (:action o_sync_q_1s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_1t))
    :effect
      (and
        (when
          (q_1s)
          (q_3))
        (when
          (q_1s)
          (q_1))
        (q_2t)
        (when
          (q_1s)
          (not 
            (q_1s)))
        (not 
          (q_1t))
      )
    )
  (:action o_sync_q_2s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_2t))
    :effect
      (and
        (q_3t)
        (when
          (q_2s)
          (not 
            (q_2s)))
        (when
          (and
            (q_2s)
            (not 
              (seen)))
          (not 
            (f_ok)))
        (not 
          (q_2t))
      )
    )
  (:action o_sync_q_3s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_3t))
    :effect
      (and
        (q_4t)
        (when
          (q_3s)
          (not 
            (q_3s)))
        (not 
          (q_3t))
      )
    )
  (:action o_sync_q_4s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_4t))
    :effect
      (and
        (f_world)
        (when
          (q_1s)
          (q_1))
        (when
          (q_2s)
          (q_2))
        (when
          (q_3s)
          (q_3))
        (when
          (q_4s)
          (q_4))
        (when
          (q_4s)
          (not 
            (f_ok)))
        (not 
          (q_4t))
        (not 
          (f_sync))
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
      )
    )
)