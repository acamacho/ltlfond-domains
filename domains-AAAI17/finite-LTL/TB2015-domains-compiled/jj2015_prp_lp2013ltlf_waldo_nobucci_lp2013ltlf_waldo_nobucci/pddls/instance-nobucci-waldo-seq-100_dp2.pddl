(define (problem 'instance-nobucci-waldo-seq-100_dp2')

  (:domain waldo-seq-100-instance-nobucci_dp2)
  (:objects r1 - room r10 - room r100 - room r11 - room r12 - room r13 - room r14 - room r15 - room r16 - room r17 - room r18 - room r19 - room r2 - room r20 - room r21 - room r22 - room r23 - room r24 - room r25 - room r26 - room r27 - room r28 - room r29 - room r3 - room r30 - room r31 - room r32 - room r33 - room r34 - room r35 - room r36 - room r37 - room r38 - room r39 - room r4 - room r40 - room r41 - room r42 - room r43 - room r44 - room r45 - room r46 - room r47 - room r48 - room r49 - room r5 - room r50 - room r51 - room r52 - room r53 - room r54 - room r55 - room r56 - room r57 - room r58 - room r59 - room r6 - room r60 - room r61 - room r62 - room r63 - room r64 - room r65 - room r66 - room r67 - room r68 - room r69 - room r7 - room r70 - room r71 - room r72 - room r73 - room r74 - room r75 - room r76 - room r77 - room r78 - room r79 - room r8 - room r80 - room r81 - room r82 - room r83 - room r84 - room r85 - room r86 - room r87 - room r88 - room r89 - room r9 - room r90 - room r91 - room r92 - room r93 - room r94 - room r95 - room r96 - room r97 - room r98 - room r99 - room)
  (:init 
    (in r1)
    (q_1)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)