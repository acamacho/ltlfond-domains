(define (domain waldo-seq-600-instance-nobucci_dp2)
  (:types
    room - NO_TYPE
  )

  (:predicates
    (seen)
    (in ?x0 - room)
    (f_copy)
    (f_sync)
    (f_world)
    (f_ok)
    (f_goal)
    (q_1)
    (q_1s)
    (q_1t)
    (q_2)
    (q_2s)
    (q_2t)
    (q_3)
    (q_3s)
    (q_3t)
    (q_4)
    (q_4s)
    (q_4t)
  )
  (:action move-right-from-r1
    :parameters ()
    :precondition 
      (and
        (in r1)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r2)
        (f_copy)
        (not 
          (in r1))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r1
    :parameters ()
    :precondition 
      (and
        (in r1)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r600)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r1))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r2
    :parameters ()
    :precondition 
      (and
        (in r2)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r3)
        (f_copy)
        (not 
          (in r2))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r2
    :parameters ()
    :precondition 
      (and
        (in r2)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r1)
        (f_copy)
        (not 
          (in r2))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r3
    :parameters ()
    :precondition 
      (and
        (in r3)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r4)
        (f_copy)
        (not 
          (in r3))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r3
    :parameters ()
    :precondition 
      (and
        (in r3)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r2)
        (f_copy)
        (not 
          (in r3))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r4
    :parameters ()
    :precondition 
      (and
        (in r4)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r5)
        (f_copy)
        (not 
          (in r4))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r4
    :parameters ()
    :precondition 
      (and
        (in r4)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r3)
        (f_copy)
        (not 
          (in r4))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r5
    :parameters ()
    :precondition 
      (and
        (in r5)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r6)
        (f_copy)
        (not 
          (in r5))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r5
    :parameters ()
    :precondition 
      (and
        (in r5)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r4)
        (f_copy)
        (not 
          (in r5))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r6
    :parameters ()
    :precondition 
      (and
        (in r6)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r7)
        (f_copy)
        (not 
          (in r6))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r6
    :parameters ()
    :precondition 
      (and
        (in r6)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r5)
        (f_copy)
        (not 
          (in r6))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r7
    :parameters ()
    :precondition 
      (and
        (in r7)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r8)
        (f_copy)
        (not 
          (in r7))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r7
    :parameters ()
    :precondition 
      (and
        (in r7)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r6)
        (f_copy)
        (not 
          (in r7))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r8
    :parameters ()
    :precondition 
      (and
        (in r8)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r9)
        (f_copy)
        (not 
          (in r8))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r8
    :parameters ()
    :precondition 
      (and
        (in r8)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r7)
        (f_copy)
        (not 
          (in r8))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r9
    :parameters ()
    :precondition 
      (and
        (in r9)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r10)
        (f_copy)
        (not 
          (in r9))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r9
    :parameters ()
    :precondition 
      (and
        (in r9)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r8)
        (f_copy)
        (not 
          (in r9))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r10
    :parameters ()
    :precondition 
      (and
        (in r10)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r11)
        (f_copy)
        (not 
          (in r10))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r10
    :parameters ()
    :precondition 
      (and
        (in r10)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r9)
        (f_copy)
        (not 
          (in r10))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r11
    :parameters ()
    :precondition 
      (and
        (in r11)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r12)
        (f_copy)
        (not 
          (in r11))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r11
    :parameters ()
    :precondition 
      (and
        (in r11)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r10)
        (f_copy)
        (not 
          (in r11))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r12
    :parameters ()
    :precondition 
      (and
        (in r12)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r13)
        (f_copy)
        (not 
          (in r12))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r12
    :parameters ()
    :precondition 
      (and
        (in r12)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r11)
        (f_copy)
        (not 
          (in r12))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r13
    :parameters ()
    :precondition 
      (and
        (in r13)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r14)
        (f_copy)
        (not 
          (in r13))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r13
    :parameters ()
    :precondition 
      (and
        (in r13)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r12)
        (f_copy)
        (not 
          (in r13))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r14
    :parameters ()
    :precondition 
      (and
        (in r14)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r15)
        (f_copy)
        (not 
          (in r14))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r14
    :parameters ()
    :precondition 
      (and
        (in r14)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r13)
        (f_copy)
        (not 
          (in r14))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r15
    :parameters ()
    :precondition 
      (and
        (in r15)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r16)
        (f_copy)
        (not 
          (in r15))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r15
    :parameters ()
    :precondition 
      (and
        (in r15)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r14)
        (f_copy)
        (not 
          (in r15))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r16
    :parameters ()
    :precondition 
      (and
        (in r16)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r17)
        (f_copy)
        (not 
          (in r16))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r16
    :parameters ()
    :precondition 
      (and
        (in r16)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r15)
        (f_copy)
        (not 
          (in r16))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r17
    :parameters ()
    :precondition 
      (and
        (in r17)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r18)
        (f_copy)
        (not 
          (in r17))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r17
    :parameters ()
    :precondition 
      (and
        (in r17)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r16)
        (f_copy)
        (not 
          (in r17))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r18
    :parameters ()
    :precondition 
      (and
        (in r18)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r19)
        (f_copy)
        (not 
          (in r18))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r18
    :parameters ()
    :precondition 
      (and
        (in r18)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r17)
        (f_copy)
        (not 
          (in r18))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r19
    :parameters ()
    :precondition 
      (and
        (in r19)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r20)
        (f_copy)
        (not 
          (in r19))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r19
    :parameters ()
    :precondition 
      (and
        (in r19)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r18)
        (f_copy)
        (not 
          (in r19))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r20
    :parameters ()
    :precondition 
      (and
        (in r20)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r21)
        (f_copy)
        (not 
          (in r20))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r20
    :parameters ()
    :precondition 
      (and
        (in r20)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r19)
        (f_copy)
        (not 
          (in r20))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r21
    :parameters ()
    :precondition 
      (and
        (in r21)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r22)
        (f_copy)
        (not 
          (in r21))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r21
    :parameters ()
    :precondition 
      (and
        (in r21)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r20)
        (f_copy)
        (not 
          (in r21))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r22
    :parameters ()
    :precondition 
      (and
        (in r22)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r23)
        (f_copy)
        (not 
          (in r22))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r22
    :parameters ()
    :precondition 
      (and
        (in r22)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r21)
        (f_copy)
        (not 
          (in r22))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r23
    :parameters ()
    :precondition 
      (and
        (in r23)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r24)
        (f_copy)
        (not 
          (in r23))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r23
    :parameters ()
    :precondition 
      (and
        (in r23)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r22)
        (f_copy)
        (not 
          (in r23))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r24
    :parameters ()
    :precondition 
      (and
        (in r24)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r25)
        (f_copy)
        (not 
          (in r24))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r24
    :parameters ()
    :precondition 
      (and
        (in r24)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r23)
        (f_copy)
        (not 
          (in r24))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r25
    :parameters ()
    :precondition 
      (and
        (in r25)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r26)
        (f_copy)
        (not 
          (in r25))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r25
    :parameters ()
    :precondition 
      (and
        (in r25)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r24)
        (f_copy)
        (not 
          (in r25))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r26
    :parameters ()
    :precondition 
      (and
        (in r26)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r27)
        (f_copy)
        (not 
          (in r26))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r26
    :parameters ()
    :precondition 
      (and
        (in r26)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r25)
        (f_copy)
        (not 
          (in r26))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r27
    :parameters ()
    :precondition 
      (and
        (in r27)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r28)
        (f_copy)
        (not 
          (in r27))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r27
    :parameters ()
    :precondition 
      (and
        (in r27)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r26)
        (f_copy)
        (not 
          (in r27))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r28
    :parameters ()
    :precondition 
      (and
        (in r28)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r29)
        (f_copy)
        (not 
          (in r28))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r28
    :parameters ()
    :precondition 
      (and
        (in r28)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r27)
        (f_copy)
        (not 
          (in r28))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r29
    :parameters ()
    :precondition 
      (and
        (in r29)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r30)
        (f_copy)
        (not 
          (in r29))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r29
    :parameters ()
    :precondition 
      (and
        (in r29)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r28)
        (f_copy)
        (not 
          (in r29))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r30
    :parameters ()
    :precondition 
      (and
        (in r30)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r31)
        (f_copy)
        (not 
          (in r30))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r30
    :parameters ()
    :precondition 
      (and
        (in r30)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r29)
        (f_copy)
        (not 
          (in r30))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r31
    :parameters ()
    :precondition 
      (and
        (in r31)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r32)
        (f_copy)
        (not 
          (in r31))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r31
    :parameters ()
    :precondition 
      (and
        (in r31)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r30)
        (f_copy)
        (not 
          (in r31))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r32
    :parameters ()
    :precondition 
      (and
        (in r32)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r33)
        (f_copy)
        (not 
          (in r32))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r32
    :parameters ()
    :precondition 
      (and
        (in r32)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r31)
        (f_copy)
        (not 
          (in r32))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r33
    :parameters ()
    :precondition 
      (and
        (in r33)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r34)
        (f_copy)
        (not 
          (in r33))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r33
    :parameters ()
    :precondition 
      (and
        (in r33)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r32)
        (f_copy)
        (not 
          (in r33))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r34
    :parameters ()
    :precondition 
      (and
        (in r34)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r35)
        (f_copy)
        (not 
          (in r34))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r34
    :parameters ()
    :precondition 
      (and
        (in r34)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r33)
        (f_copy)
        (not 
          (in r34))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r35
    :parameters ()
    :precondition 
      (and
        (in r35)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r36)
        (f_copy)
        (not 
          (in r35))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r35
    :parameters ()
    :precondition 
      (and
        (in r35)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r34)
        (f_copy)
        (not 
          (in r35))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r36
    :parameters ()
    :precondition 
      (and
        (in r36)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r37)
        (f_copy)
        (not 
          (in r36))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r36
    :parameters ()
    :precondition 
      (and
        (in r36)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r35)
        (f_copy)
        (not 
          (in r36))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r37
    :parameters ()
    :precondition 
      (and
        (in r37)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r38)
        (f_copy)
        (not 
          (in r37))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r37
    :parameters ()
    :precondition 
      (and
        (in r37)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r36)
        (f_copy)
        (not 
          (in r37))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r38
    :parameters ()
    :precondition 
      (and
        (in r38)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r39)
        (f_copy)
        (not 
          (in r38))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r38
    :parameters ()
    :precondition 
      (and
        (in r38)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r37)
        (f_copy)
        (not 
          (in r38))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r39
    :parameters ()
    :precondition 
      (and
        (in r39)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r40)
        (f_copy)
        (not 
          (in r39))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r39
    :parameters ()
    :precondition 
      (and
        (in r39)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r38)
        (f_copy)
        (not 
          (in r39))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r40
    :parameters ()
    :precondition 
      (and
        (in r40)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r41)
        (f_copy)
        (not 
          (in r40))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r40
    :parameters ()
    :precondition 
      (and
        (in r40)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r39)
        (f_copy)
        (not 
          (in r40))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r41
    :parameters ()
    :precondition 
      (and
        (in r41)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r42)
        (f_copy)
        (not 
          (in r41))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r41
    :parameters ()
    :precondition 
      (and
        (in r41)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r40)
        (f_copy)
        (not 
          (in r41))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r42
    :parameters ()
    :precondition 
      (and
        (in r42)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r43)
        (f_copy)
        (not 
          (in r42))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r42
    :parameters ()
    :precondition 
      (and
        (in r42)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r41)
        (f_copy)
        (not 
          (in r42))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r43
    :parameters ()
    :precondition 
      (and
        (in r43)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r44)
        (f_copy)
        (not 
          (in r43))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r43
    :parameters ()
    :precondition 
      (and
        (in r43)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r42)
        (f_copy)
        (not 
          (in r43))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r44
    :parameters ()
    :precondition 
      (and
        (in r44)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r45)
        (f_copy)
        (not 
          (in r44))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r44
    :parameters ()
    :precondition 
      (and
        (in r44)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r43)
        (f_copy)
        (not 
          (in r44))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r45
    :parameters ()
    :precondition 
      (and
        (in r45)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r46)
        (f_copy)
        (not 
          (in r45))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r45
    :parameters ()
    :precondition 
      (and
        (in r45)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r44)
        (f_copy)
        (not 
          (in r45))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r46
    :parameters ()
    :precondition 
      (and
        (in r46)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r47)
        (f_copy)
        (not 
          (in r46))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r46
    :parameters ()
    :precondition 
      (and
        (in r46)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r45)
        (f_copy)
        (not 
          (in r46))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r47
    :parameters ()
    :precondition 
      (and
        (in r47)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r48)
        (f_copy)
        (not 
          (in r47))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r47
    :parameters ()
    :precondition 
      (and
        (in r47)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r46)
        (f_copy)
        (not 
          (in r47))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r48
    :parameters ()
    :precondition 
      (and
        (in r48)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r49)
        (f_copy)
        (not 
          (in r48))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r48
    :parameters ()
    :precondition 
      (and
        (in r48)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r47)
        (f_copy)
        (not 
          (in r48))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r49
    :parameters ()
    :precondition 
      (and
        (in r49)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r50)
        (f_copy)
        (not 
          (in r49))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r49
    :parameters ()
    :precondition 
      (and
        (in r49)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r48)
        (f_copy)
        (not 
          (in r49))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r50
    :parameters ()
    :precondition 
      (and
        (in r50)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r51)
        (f_copy)
        (not 
          (in r50))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r50
    :parameters ()
    :precondition 
      (and
        (in r50)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r49)
        (f_copy)
        (not 
          (in r50))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r51
    :parameters ()
    :precondition 
      (and
        (in r51)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r52)
        (f_copy)
        (not 
          (in r51))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r51
    :parameters ()
    :precondition 
      (and
        (in r51)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r50)
        (f_copy)
        (not 
          (in r51))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r52
    :parameters ()
    :precondition 
      (and
        (in r52)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r53)
        (f_copy)
        (not 
          (in r52))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r52
    :parameters ()
    :precondition 
      (and
        (in r52)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r51)
        (f_copy)
        (not 
          (in r52))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r53
    :parameters ()
    :precondition 
      (and
        (in r53)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r54)
        (f_copy)
        (not 
          (in r53))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r53
    :parameters ()
    :precondition 
      (and
        (in r53)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r52)
        (f_copy)
        (not 
          (in r53))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r54
    :parameters ()
    :precondition 
      (and
        (in r54)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r55)
        (f_copy)
        (not 
          (in r54))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r54
    :parameters ()
    :precondition 
      (and
        (in r54)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r53)
        (f_copy)
        (not 
          (in r54))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r55
    :parameters ()
    :precondition 
      (and
        (in r55)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r56)
        (f_copy)
        (not 
          (in r55))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r55
    :parameters ()
    :precondition 
      (and
        (in r55)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r54)
        (f_copy)
        (not 
          (in r55))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r56
    :parameters ()
    :precondition 
      (and
        (in r56)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r57)
        (f_copy)
        (not 
          (in r56))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r56
    :parameters ()
    :precondition 
      (and
        (in r56)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r55)
        (f_copy)
        (not 
          (in r56))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r57
    :parameters ()
    :precondition 
      (and
        (in r57)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r58)
        (f_copy)
        (not 
          (in r57))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r57
    :parameters ()
    :precondition 
      (and
        (in r57)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r56)
        (f_copy)
        (not 
          (in r57))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r58
    :parameters ()
    :precondition 
      (and
        (in r58)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r59)
        (f_copy)
        (not 
          (in r58))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r58
    :parameters ()
    :precondition 
      (and
        (in r58)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r57)
        (f_copy)
        (not 
          (in r58))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r59
    :parameters ()
    :precondition 
      (and
        (in r59)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r60)
        (f_copy)
        (not 
          (in r59))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r59
    :parameters ()
    :precondition 
      (and
        (in r59)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r58)
        (f_copy)
        (not 
          (in r59))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r60
    :parameters ()
    :precondition 
      (and
        (in r60)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r61)
        (f_copy)
        (not 
          (in r60))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r60
    :parameters ()
    :precondition 
      (and
        (in r60)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r59)
        (f_copy)
        (not 
          (in r60))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r61
    :parameters ()
    :precondition 
      (and
        (in r61)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r62)
        (f_copy)
        (not 
          (in r61))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r61
    :parameters ()
    :precondition 
      (and
        (in r61)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r60)
        (f_copy)
        (not 
          (in r61))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r62
    :parameters ()
    :precondition 
      (and
        (in r62)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r63)
        (f_copy)
        (not 
          (in r62))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r62
    :parameters ()
    :precondition 
      (and
        (in r62)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r61)
        (f_copy)
        (not 
          (in r62))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r63
    :parameters ()
    :precondition 
      (and
        (in r63)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r64)
        (f_copy)
        (not 
          (in r63))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r63
    :parameters ()
    :precondition 
      (and
        (in r63)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r62)
        (f_copy)
        (not 
          (in r63))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r64
    :parameters ()
    :precondition 
      (and
        (in r64)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r65)
        (f_copy)
        (not 
          (in r64))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r64
    :parameters ()
    :precondition 
      (and
        (in r64)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r63)
        (f_copy)
        (not 
          (in r64))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r65
    :parameters ()
    :precondition 
      (and
        (in r65)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r66)
        (f_copy)
        (not 
          (in r65))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r65
    :parameters ()
    :precondition 
      (and
        (in r65)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r64)
        (f_copy)
        (not 
          (in r65))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r66
    :parameters ()
    :precondition 
      (and
        (in r66)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r67)
        (f_copy)
        (not 
          (in r66))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r66
    :parameters ()
    :precondition 
      (and
        (in r66)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r65)
        (f_copy)
        (not 
          (in r66))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r67
    :parameters ()
    :precondition 
      (and
        (in r67)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r68)
        (f_copy)
        (not 
          (in r67))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r67
    :parameters ()
    :precondition 
      (and
        (in r67)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r66)
        (f_copy)
        (not 
          (in r67))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r68
    :parameters ()
    :precondition 
      (and
        (in r68)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r69)
        (f_copy)
        (not 
          (in r68))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r68
    :parameters ()
    :precondition 
      (and
        (in r68)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r67)
        (f_copy)
        (not 
          (in r68))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r69
    :parameters ()
    :precondition 
      (and
        (in r69)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r70)
        (f_copy)
        (not 
          (in r69))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r69
    :parameters ()
    :precondition 
      (and
        (in r69)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r68)
        (f_copy)
        (not 
          (in r69))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r70
    :parameters ()
    :precondition 
      (and
        (in r70)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r71)
        (f_copy)
        (not 
          (in r70))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r70
    :parameters ()
    :precondition 
      (and
        (in r70)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r69)
        (f_copy)
        (not 
          (in r70))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r71
    :parameters ()
    :precondition 
      (and
        (in r71)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r72)
        (f_copy)
        (not 
          (in r71))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r71
    :parameters ()
    :precondition 
      (and
        (in r71)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r70)
        (f_copy)
        (not 
          (in r71))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r72
    :parameters ()
    :precondition 
      (and
        (in r72)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r73)
        (f_copy)
        (not 
          (in r72))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r72
    :parameters ()
    :precondition 
      (and
        (in r72)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r71)
        (f_copy)
        (not 
          (in r72))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r73
    :parameters ()
    :precondition 
      (and
        (in r73)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r74)
        (f_copy)
        (not 
          (in r73))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r73
    :parameters ()
    :precondition 
      (and
        (in r73)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r72)
        (f_copy)
        (not 
          (in r73))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r74
    :parameters ()
    :precondition 
      (and
        (in r74)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r75)
        (f_copy)
        (not 
          (in r74))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r74
    :parameters ()
    :precondition 
      (and
        (in r74)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r73)
        (f_copy)
        (not 
          (in r74))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r75
    :parameters ()
    :precondition 
      (and
        (in r75)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r76)
        (f_copy)
        (not 
          (in r75))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r75
    :parameters ()
    :precondition 
      (and
        (in r75)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r74)
        (f_copy)
        (not 
          (in r75))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r76
    :parameters ()
    :precondition 
      (and
        (in r76)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r77)
        (f_copy)
        (not 
          (in r76))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r76
    :parameters ()
    :precondition 
      (and
        (in r76)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r75)
        (f_copy)
        (not 
          (in r76))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r77
    :parameters ()
    :precondition 
      (and
        (in r77)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r78)
        (f_copy)
        (not 
          (in r77))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r77
    :parameters ()
    :precondition 
      (and
        (in r77)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r76)
        (f_copy)
        (not 
          (in r77))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r78
    :parameters ()
    :precondition 
      (and
        (in r78)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r79)
        (f_copy)
        (not 
          (in r78))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r78
    :parameters ()
    :precondition 
      (and
        (in r78)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r77)
        (f_copy)
        (not 
          (in r78))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r79
    :parameters ()
    :precondition 
      (and
        (in r79)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r80)
        (f_copy)
        (not 
          (in r79))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r79
    :parameters ()
    :precondition 
      (and
        (in r79)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r78)
        (f_copy)
        (not 
          (in r79))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r80
    :parameters ()
    :precondition 
      (and
        (in r80)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r81)
        (f_copy)
        (not 
          (in r80))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r80
    :parameters ()
    :precondition 
      (and
        (in r80)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r79)
        (f_copy)
        (not 
          (in r80))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r81
    :parameters ()
    :precondition 
      (and
        (in r81)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r82)
        (f_copy)
        (not 
          (in r81))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r81
    :parameters ()
    :precondition 
      (and
        (in r81)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r80)
        (f_copy)
        (not 
          (in r81))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r82
    :parameters ()
    :precondition 
      (and
        (in r82)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r83)
        (f_copy)
        (not 
          (in r82))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r82
    :parameters ()
    :precondition 
      (and
        (in r82)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r81)
        (f_copy)
        (not 
          (in r82))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r83
    :parameters ()
    :precondition 
      (and
        (in r83)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r84)
        (f_copy)
        (not 
          (in r83))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r83
    :parameters ()
    :precondition 
      (and
        (in r83)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r82)
        (f_copy)
        (not 
          (in r83))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r84
    :parameters ()
    :precondition 
      (and
        (in r84)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r85)
        (f_copy)
        (not 
          (in r84))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r84
    :parameters ()
    :precondition 
      (and
        (in r84)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r83)
        (f_copy)
        (not 
          (in r84))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r85
    :parameters ()
    :precondition 
      (and
        (in r85)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r86)
        (f_copy)
        (not 
          (in r85))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r85
    :parameters ()
    :precondition 
      (and
        (in r85)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r84)
        (f_copy)
        (not 
          (in r85))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r86
    :parameters ()
    :precondition 
      (and
        (in r86)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r87)
        (f_copy)
        (not 
          (in r86))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r86
    :parameters ()
    :precondition 
      (and
        (in r86)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r85)
        (f_copy)
        (not 
          (in r86))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r87
    :parameters ()
    :precondition 
      (and
        (in r87)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r88)
        (f_copy)
        (not 
          (in r87))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r87
    :parameters ()
    :precondition 
      (and
        (in r87)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r86)
        (f_copy)
        (not 
          (in r87))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r88
    :parameters ()
    :precondition 
      (and
        (in r88)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r89)
        (f_copy)
        (not 
          (in r88))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r88
    :parameters ()
    :precondition 
      (and
        (in r88)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r87)
        (f_copy)
        (not 
          (in r88))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r89
    :parameters ()
    :precondition 
      (and
        (in r89)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r90)
        (f_copy)
        (not 
          (in r89))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r89
    :parameters ()
    :precondition 
      (and
        (in r89)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r88)
        (f_copy)
        (not 
          (in r89))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r90
    :parameters ()
    :precondition 
      (and
        (in r90)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r91)
        (f_copy)
        (not 
          (in r90))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r90
    :parameters ()
    :precondition 
      (and
        (in r90)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r89)
        (f_copy)
        (not 
          (in r90))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r91
    :parameters ()
    :precondition 
      (and
        (in r91)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r92)
        (f_copy)
        (not 
          (in r91))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r91
    :parameters ()
    :precondition 
      (and
        (in r91)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r90)
        (f_copy)
        (not 
          (in r91))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r92
    :parameters ()
    :precondition 
      (and
        (in r92)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r93)
        (f_copy)
        (not 
          (in r92))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r92
    :parameters ()
    :precondition 
      (and
        (in r92)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r91)
        (f_copy)
        (not 
          (in r92))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r93
    :parameters ()
    :precondition 
      (and
        (in r93)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r94)
        (f_copy)
        (not 
          (in r93))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r93
    :parameters ()
    :precondition 
      (and
        (in r93)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r92)
        (f_copy)
        (not 
          (in r93))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r94
    :parameters ()
    :precondition 
      (and
        (in r94)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r95)
        (f_copy)
        (not 
          (in r94))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r94
    :parameters ()
    :precondition 
      (and
        (in r94)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r93)
        (f_copy)
        (not 
          (in r94))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r95
    :parameters ()
    :precondition 
      (and
        (in r95)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r96)
        (f_copy)
        (not 
          (in r95))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r95
    :parameters ()
    :precondition 
      (and
        (in r95)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r94)
        (f_copy)
        (not 
          (in r95))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r96
    :parameters ()
    :precondition 
      (and
        (in r96)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r97)
        (f_copy)
        (not 
          (in r96))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r96
    :parameters ()
    :precondition 
      (and
        (in r96)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r95)
        (f_copy)
        (not 
          (in r96))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r97
    :parameters ()
    :precondition 
      (and
        (in r97)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r98)
        (f_copy)
        (not 
          (in r97))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r97
    :parameters ()
    :precondition 
      (and
        (in r97)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r96)
        (f_copy)
        (not 
          (in r97))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r98
    :parameters ()
    :precondition 
      (and
        (in r98)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r99)
        (f_copy)
        (not 
          (in r98))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r98
    :parameters ()
    :precondition 
      (and
        (in r98)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r97)
        (f_copy)
        (not 
          (in r98))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r99
    :parameters ()
    :precondition 
      (and
        (in r99)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r100)
        (f_copy)
        (not 
          (in r99))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r99
    :parameters ()
    :precondition 
      (and
        (in r99)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r98)
        (f_copy)
        (not 
          (in r99))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r101)
        (f_copy)
        (not 
          (in r100))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r99)
        (f_copy)
        (not 
          (in r100))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r101
    :parameters ()
    :precondition 
      (and
        (in r101)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r102)
        (f_copy)
        (not 
          (in r101))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r101
    :parameters ()
    :precondition 
      (and
        (in r101)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r100)
        (f_copy)
        (not 
          (in r101))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r102
    :parameters ()
    :precondition 
      (and
        (in r102)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r103)
        (f_copy)
        (not 
          (in r102))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r102
    :parameters ()
    :precondition 
      (and
        (in r102)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r101)
        (f_copy)
        (not 
          (in r102))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r103
    :parameters ()
    :precondition 
      (and
        (in r103)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r104)
        (f_copy)
        (not 
          (in r103))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r103
    :parameters ()
    :precondition 
      (and
        (in r103)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r102)
        (f_copy)
        (not 
          (in r103))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r104
    :parameters ()
    :precondition 
      (and
        (in r104)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r105)
        (f_copy)
        (not 
          (in r104))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r104
    :parameters ()
    :precondition 
      (and
        (in r104)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r103)
        (f_copy)
        (not 
          (in r104))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r105
    :parameters ()
    :precondition 
      (and
        (in r105)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r106)
        (f_copy)
        (not 
          (in r105))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r105
    :parameters ()
    :precondition 
      (and
        (in r105)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r104)
        (f_copy)
        (not 
          (in r105))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r106
    :parameters ()
    :precondition 
      (and
        (in r106)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r107)
        (f_copy)
        (not 
          (in r106))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r106
    :parameters ()
    :precondition 
      (and
        (in r106)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r105)
        (f_copy)
        (not 
          (in r106))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r107
    :parameters ()
    :precondition 
      (and
        (in r107)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r108)
        (f_copy)
        (not 
          (in r107))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r107
    :parameters ()
    :precondition 
      (and
        (in r107)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r106)
        (f_copy)
        (not 
          (in r107))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r108
    :parameters ()
    :precondition 
      (and
        (in r108)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r109)
        (f_copy)
        (not 
          (in r108))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r108
    :parameters ()
    :precondition 
      (and
        (in r108)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r107)
        (f_copy)
        (not 
          (in r108))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r109
    :parameters ()
    :precondition 
      (and
        (in r109)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r110)
        (f_copy)
        (not 
          (in r109))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r109
    :parameters ()
    :precondition 
      (and
        (in r109)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r108)
        (f_copy)
        (not 
          (in r109))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r110
    :parameters ()
    :precondition 
      (and
        (in r110)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r111)
        (f_copy)
        (not 
          (in r110))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r110
    :parameters ()
    :precondition 
      (and
        (in r110)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r109)
        (f_copy)
        (not 
          (in r110))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r111
    :parameters ()
    :precondition 
      (and
        (in r111)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r112)
        (f_copy)
        (not 
          (in r111))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r111
    :parameters ()
    :precondition 
      (and
        (in r111)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r110)
        (f_copy)
        (not 
          (in r111))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r112
    :parameters ()
    :precondition 
      (and
        (in r112)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r113)
        (f_copy)
        (not 
          (in r112))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r112
    :parameters ()
    :precondition 
      (and
        (in r112)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r111)
        (f_copy)
        (not 
          (in r112))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r113
    :parameters ()
    :precondition 
      (and
        (in r113)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r114)
        (f_copy)
        (not 
          (in r113))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r113
    :parameters ()
    :precondition 
      (and
        (in r113)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r112)
        (f_copy)
        (not 
          (in r113))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r114
    :parameters ()
    :precondition 
      (and
        (in r114)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r115)
        (f_copy)
        (not 
          (in r114))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r114
    :parameters ()
    :precondition 
      (and
        (in r114)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r113)
        (f_copy)
        (not 
          (in r114))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r115
    :parameters ()
    :precondition 
      (and
        (in r115)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r116)
        (f_copy)
        (not 
          (in r115))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r115
    :parameters ()
    :precondition 
      (and
        (in r115)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r114)
        (f_copy)
        (not 
          (in r115))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r116
    :parameters ()
    :precondition 
      (and
        (in r116)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r117)
        (f_copy)
        (not 
          (in r116))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r116
    :parameters ()
    :precondition 
      (and
        (in r116)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r115)
        (f_copy)
        (not 
          (in r116))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r117
    :parameters ()
    :precondition 
      (and
        (in r117)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r118)
        (f_copy)
        (not 
          (in r117))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r117
    :parameters ()
    :precondition 
      (and
        (in r117)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r116)
        (f_copy)
        (not 
          (in r117))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r118
    :parameters ()
    :precondition 
      (and
        (in r118)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r119)
        (f_copy)
        (not 
          (in r118))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r118
    :parameters ()
    :precondition 
      (and
        (in r118)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r117)
        (f_copy)
        (not 
          (in r118))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r119
    :parameters ()
    :precondition 
      (and
        (in r119)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r120)
        (f_copy)
        (not 
          (in r119))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r119
    :parameters ()
    :precondition 
      (and
        (in r119)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r118)
        (f_copy)
        (not 
          (in r119))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r120
    :parameters ()
    :precondition 
      (and
        (in r120)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r121)
        (f_copy)
        (not 
          (in r120))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r120
    :parameters ()
    :precondition 
      (and
        (in r120)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r119)
        (f_copy)
        (not 
          (in r120))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r121
    :parameters ()
    :precondition 
      (and
        (in r121)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r122)
        (f_copy)
        (not 
          (in r121))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r121
    :parameters ()
    :precondition 
      (and
        (in r121)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r120)
        (f_copy)
        (not 
          (in r121))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r122
    :parameters ()
    :precondition 
      (and
        (in r122)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r123)
        (f_copy)
        (not 
          (in r122))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r122
    :parameters ()
    :precondition 
      (and
        (in r122)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r121)
        (f_copy)
        (not 
          (in r122))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r123
    :parameters ()
    :precondition 
      (and
        (in r123)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r124)
        (f_copy)
        (not 
          (in r123))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r123
    :parameters ()
    :precondition 
      (and
        (in r123)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r122)
        (f_copy)
        (not 
          (in r123))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r124
    :parameters ()
    :precondition 
      (and
        (in r124)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r125)
        (f_copy)
        (not 
          (in r124))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r124
    :parameters ()
    :precondition 
      (and
        (in r124)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r123)
        (f_copy)
        (not 
          (in r124))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r125
    :parameters ()
    :precondition 
      (and
        (in r125)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r126)
        (f_copy)
        (not 
          (in r125))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r125
    :parameters ()
    :precondition 
      (and
        (in r125)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r124)
        (f_copy)
        (not 
          (in r125))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r126
    :parameters ()
    :precondition 
      (and
        (in r126)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r127)
        (f_copy)
        (not 
          (in r126))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r126
    :parameters ()
    :precondition 
      (and
        (in r126)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r125)
        (f_copy)
        (not 
          (in r126))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r127
    :parameters ()
    :precondition 
      (and
        (in r127)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r128)
        (f_copy)
        (not 
          (in r127))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r127
    :parameters ()
    :precondition 
      (and
        (in r127)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r126)
        (f_copy)
        (not 
          (in r127))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r128
    :parameters ()
    :precondition 
      (and
        (in r128)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r129)
        (f_copy)
        (not 
          (in r128))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r128
    :parameters ()
    :precondition 
      (and
        (in r128)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r127)
        (f_copy)
        (not 
          (in r128))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r129
    :parameters ()
    :precondition 
      (and
        (in r129)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r130)
        (f_copy)
        (not 
          (in r129))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r129
    :parameters ()
    :precondition 
      (and
        (in r129)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r128)
        (f_copy)
        (not 
          (in r129))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r130
    :parameters ()
    :precondition 
      (and
        (in r130)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r131)
        (f_copy)
        (not 
          (in r130))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r130
    :parameters ()
    :precondition 
      (and
        (in r130)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r129)
        (f_copy)
        (not 
          (in r130))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r131
    :parameters ()
    :precondition 
      (and
        (in r131)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r132)
        (f_copy)
        (not 
          (in r131))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r131
    :parameters ()
    :precondition 
      (and
        (in r131)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r130)
        (f_copy)
        (not 
          (in r131))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r132
    :parameters ()
    :precondition 
      (and
        (in r132)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r133)
        (f_copy)
        (not 
          (in r132))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r132
    :parameters ()
    :precondition 
      (and
        (in r132)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r131)
        (f_copy)
        (not 
          (in r132))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r133
    :parameters ()
    :precondition 
      (and
        (in r133)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r134)
        (f_copy)
        (not 
          (in r133))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r133
    :parameters ()
    :precondition 
      (and
        (in r133)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r132)
        (f_copy)
        (not 
          (in r133))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r134
    :parameters ()
    :precondition 
      (and
        (in r134)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r135)
        (f_copy)
        (not 
          (in r134))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r134
    :parameters ()
    :precondition 
      (and
        (in r134)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r133)
        (f_copy)
        (not 
          (in r134))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r135
    :parameters ()
    :precondition 
      (and
        (in r135)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r136)
        (f_copy)
        (not 
          (in r135))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r135
    :parameters ()
    :precondition 
      (and
        (in r135)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r134)
        (f_copy)
        (not 
          (in r135))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r136
    :parameters ()
    :precondition 
      (and
        (in r136)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r137)
        (f_copy)
        (not 
          (in r136))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r136
    :parameters ()
    :precondition 
      (and
        (in r136)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r135)
        (f_copy)
        (not 
          (in r136))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r137
    :parameters ()
    :precondition 
      (and
        (in r137)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r138)
        (f_copy)
        (not 
          (in r137))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r137
    :parameters ()
    :precondition 
      (and
        (in r137)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r136)
        (f_copy)
        (not 
          (in r137))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r138
    :parameters ()
    :precondition 
      (and
        (in r138)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r139)
        (f_copy)
        (not 
          (in r138))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r138
    :parameters ()
    :precondition 
      (and
        (in r138)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r137)
        (f_copy)
        (not 
          (in r138))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r139
    :parameters ()
    :precondition 
      (and
        (in r139)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r140)
        (f_copy)
        (not 
          (in r139))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r139
    :parameters ()
    :precondition 
      (and
        (in r139)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r138)
        (f_copy)
        (not 
          (in r139))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r140
    :parameters ()
    :precondition 
      (and
        (in r140)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r141)
        (f_copy)
        (not 
          (in r140))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r140
    :parameters ()
    :precondition 
      (and
        (in r140)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r139)
        (f_copy)
        (not 
          (in r140))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r141
    :parameters ()
    :precondition 
      (and
        (in r141)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r142)
        (f_copy)
        (not 
          (in r141))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r141
    :parameters ()
    :precondition 
      (and
        (in r141)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r140)
        (f_copy)
        (not 
          (in r141))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r142
    :parameters ()
    :precondition 
      (and
        (in r142)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r143)
        (f_copy)
        (not 
          (in r142))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r142
    :parameters ()
    :precondition 
      (and
        (in r142)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r141)
        (f_copy)
        (not 
          (in r142))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r143
    :parameters ()
    :precondition 
      (and
        (in r143)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r144)
        (f_copy)
        (not 
          (in r143))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r143
    :parameters ()
    :precondition 
      (and
        (in r143)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r142)
        (f_copy)
        (not 
          (in r143))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r144
    :parameters ()
    :precondition 
      (and
        (in r144)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r145)
        (f_copy)
        (not 
          (in r144))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r144
    :parameters ()
    :precondition 
      (and
        (in r144)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r143)
        (f_copy)
        (not 
          (in r144))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r145
    :parameters ()
    :precondition 
      (and
        (in r145)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r146)
        (f_copy)
        (not 
          (in r145))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r145
    :parameters ()
    :precondition 
      (and
        (in r145)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r144)
        (f_copy)
        (not 
          (in r145))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r146
    :parameters ()
    :precondition 
      (and
        (in r146)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r147)
        (f_copy)
        (not 
          (in r146))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r146
    :parameters ()
    :precondition 
      (and
        (in r146)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r145)
        (f_copy)
        (not 
          (in r146))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r147
    :parameters ()
    :precondition 
      (and
        (in r147)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r148)
        (f_copy)
        (not 
          (in r147))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r147
    :parameters ()
    :precondition 
      (and
        (in r147)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r146)
        (f_copy)
        (not 
          (in r147))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r148
    :parameters ()
    :precondition 
      (and
        (in r148)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r149)
        (f_copy)
        (not 
          (in r148))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r148
    :parameters ()
    :precondition 
      (and
        (in r148)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r147)
        (f_copy)
        (not 
          (in r148))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r149
    :parameters ()
    :precondition 
      (and
        (in r149)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r150)
        (f_copy)
        (not 
          (in r149))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r149
    :parameters ()
    :precondition 
      (and
        (in r149)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r148)
        (f_copy)
        (not 
          (in r149))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r150
    :parameters ()
    :precondition 
      (and
        (in r150)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r151)
        (f_copy)
        (not 
          (in r150))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r150
    :parameters ()
    :precondition 
      (and
        (in r150)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r149)
        (f_copy)
        (not 
          (in r150))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r151
    :parameters ()
    :precondition 
      (and
        (in r151)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r152)
        (f_copy)
        (not 
          (in r151))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r151
    :parameters ()
    :precondition 
      (and
        (in r151)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r150)
        (f_copy)
        (not 
          (in r151))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r152
    :parameters ()
    :precondition 
      (and
        (in r152)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r153)
        (f_copy)
        (not 
          (in r152))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r152
    :parameters ()
    :precondition 
      (and
        (in r152)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r151)
        (f_copy)
        (not 
          (in r152))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r153
    :parameters ()
    :precondition 
      (and
        (in r153)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r154)
        (f_copy)
        (not 
          (in r153))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r153
    :parameters ()
    :precondition 
      (and
        (in r153)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r152)
        (f_copy)
        (not 
          (in r153))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r154
    :parameters ()
    :precondition 
      (and
        (in r154)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r155)
        (f_copy)
        (not 
          (in r154))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r154
    :parameters ()
    :precondition 
      (and
        (in r154)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r153)
        (f_copy)
        (not 
          (in r154))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r155
    :parameters ()
    :precondition 
      (and
        (in r155)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r156)
        (f_copy)
        (not 
          (in r155))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r155
    :parameters ()
    :precondition 
      (and
        (in r155)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r154)
        (f_copy)
        (not 
          (in r155))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r156
    :parameters ()
    :precondition 
      (and
        (in r156)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r157)
        (f_copy)
        (not 
          (in r156))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r156
    :parameters ()
    :precondition 
      (and
        (in r156)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r155)
        (f_copy)
        (not 
          (in r156))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r157
    :parameters ()
    :precondition 
      (and
        (in r157)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r158)
        (f_copy)
        (not 
          (in r157))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r157
    :parameters ()
    :precondition 
      (and
        (in r157)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r156)
        (f_copy)
        (not 
          (in r157))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r158
    :parameters ()
    :precondition 
      (and
        (in r158)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r159)
        (f_copy)
        (not 
          (in r158))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r158
    :parameters ()
    :precondition 
      (and
        (in r158)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r157)
        (f_copy)
        (not 
          (in r158))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r159
    :parameters ()
    :precondition 
      (and
        (in r159)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r160)
        (f_copy)
        (not 
          (in r159))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r159
    :parameters ()
    :precondition 
      (and
        (in r159)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r158)
        (f_copy)
        (not 
          (in r159))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r160
    :parameters ()
    :precondition 
      (and
        (in r160)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r161)
        (f_copy)
        (not 
          (in r160))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r160
    :parameters ()
    :precondition 
      (and
        (in r160)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r159)
        (f_copy)
        (not 
          (in r160))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r161
    :parameters ()
    :precondition 
      (and
        (in r161)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r162)
        (f_copy)
        (not 
          (in r161))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r161
    :parameters ()
    :precondition 
      (and
        (in r161)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r160)
        (f_copy)
        (not 
          (in r161))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r162
    :parameters ()
    :precondition 
      (and
        (in r162)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r163)
        (f_copy)
        (not 
          (in r162))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r162
    :parameters ()
    :precondition 
      (and
        (in r162)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r161)
        (f_copy)
        (not 
          (in r162))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r163
    :parameters ()
    :precondition 
      (and
        (in r163)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r164)
        (f_copy)
        (not 
          (in r163))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r163
    :parameters ()
    :precondition 
      (and
        (in r163)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r162)
        (f_copy)
        (not 
          (in r163))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r164
    :parameters ()
    :precondition 
      (and
        (in r164)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r165)
        (f_copy)
        (not 
          (in r164))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r164
    :parameters ()
    :precondition 
      (and
        (in r164)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r163)
        (f_copy)
        (not 
          (in r164))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r165
    :parameters ()
    :precondition 
      (and
        (in r165)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r166)
        (f_copy)
        (not 
          (in r165))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r165
    :parameters ()
    :precondition 
      (and
        (in r165)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r164)
        (f_copy)
        (not 
          (in r165))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r166
    :parameters ()
    :precondition 
      (and
        (in r166)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r167)
        (f_copy)
        (not 
          (in r166))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r166
    :parameters ()
    :precondition 
      (and
        (in r166)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r165)
        (f_copy)
        (not 
          (in r166))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r167
    :parameters ()
    :precondition 
      (and
        (in r167)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r168)
        (f_copy)
        (not 
          (in r167))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r167
    :parameters ()
    :precondition 
      (and
        (in r167)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r166)
        (f_copy)
        (not 
          (in r167))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r168
    :parameters ()
    :precondition 
      (and
        (in r168)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r169)
        (f_copy)
        (not 
          (in r168))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r168
    :parameters ()
    :precondition 
      (and
        (in r168)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r167)
        (f_copy)
        (not 
          (in r168))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r169
    :parameters ()
    :precondition 
      (and
        (in r169)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r170)
        (f_copy)
        (not 
          (in r169))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r169
    :parameters ()
    :precondition 
      (and
        (in r169)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r168)
        (f_copy)
        (not 
          (in r169))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r170
    :parameters ()
    :precondition 
      (and
        (in r170)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r171)
        (f_copy)
        (not 
          (in r170))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r170
    :parameters ()
    :precondition 
      (and
        (in r170)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r169)
        (f_copy)
        (not 
          (in r170))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r171
    :parameters ()
    :precondition 
      (and
        (in r171)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r172)
        (f_copy)
        (not 
          (in r171))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r171
    :parameters ()
    :precondition 
      (and
        (in r171)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r170)
        (f_copy)
        (not 
          (in r171))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r172
    :parameters ()
    :precondition 
      (and
        (in r172)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r173)
        (f_copy)
        (not 
          (in r172))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r172
    :parameters ()
    :precondition 
      (and
        (in r172)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r171)
        (f_copy)
        (not 
          (in r172))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r173
    :parameters ()
    :precondition 
      (and
        (in r173)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r174)
        (f_copy)
        (not 
          (in r173))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r173
    :parameters ()
    :precondition 
      (and
        (in r173)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r172)
        (f_copy)
        (not 
          (in r173))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r174
    :parameters ()
    :precondition 
      (and
        (in r174)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r175)
        (f_copy)
        (not 
          (in r174))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r174
    :parameters ()
    :precondition 
      (and
        (in r174)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r173)
        (f_copy)
        (not 
          (in r174))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r175
    :parameters ()
    :precondition 
      (and
        (in r175)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r176)
        (f_copy)
        (not 
          (in r175))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r175
    :parameters ()
    :precondition 
      (and
        (in r175)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r174)
        (f_copy)
        (not 
          (in r175))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r176
    :parameters ()
    :precondition 
      (and
        (in r176)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r177)
        (f_copy)
        (not 
          (in r176))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r176
    :parameters ()
    :precondition 
      (and
        (in r176)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r175)
        (f_copy)
        (not 
          (in r176))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r177
    :parameters ()
    :precondition 
      (and
        (in r177)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r178)
        (f_copy)
        (not 
          (in r177))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r177
    :parameters ()
    :precondition 
      (and
        (in r177)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r176)
        (f_copy)
        (not 
          (in r177))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r178
    :parameters ()
    :precondition 
      (and
        (in r178)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r179)
        (f_copy)
        (not 
          (in r178))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r178
    :parameters ()
    :precondition 
      (and
        (in r178)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r177)
        (f_copy)
        (not 
          (in r178))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r179
    :parameters ()
    :precondition 
      (and
        (in r179)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r180)
        (f_copy)
        (not 
          (in r179))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r179
    :parameters ()
    :precondition 
      (and
        (in r179)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r178)
        (f_copy)
        (not 
          (in r179))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r180
    :parameters ()
    :precondition 
      (and
        (in r180)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r181)
        (f_copy)
        (not 
          (in r180))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r180
    :parameters ()
    :precondition 
      (and
        (in r180)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r179)
        (f_copy)
        (not 
          (in r180))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r181
    :parameters ()
    :precondition 
      (and
        (in r181)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r182)
        (f_copy)
        (not 
          (in r181))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r181
    :parameters ()
    :precondition 
      (and
        (in r181)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r180)
        (f_copy)
        (not 
          (in r181))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r182
    :parameters ()
    :precondition 
      (and
        (in r182)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r183)
        (f_copy)
        (not 
          (in r182))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r182
    :parameters ()
    :precondition 
      (and
        (in r182)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r181)
        (f_copy)
        (not 
          (in r182))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r183
    :parameters ()
    :precondition 
      (and
        (in r183)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r184)
        (f_copy)
        (not 
          (in r183))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r183
    :parameters ()
    :precondition 
      (and
        (in r183)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r182)
        (f_copy)
        (not 
          (in r183))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r184
    :parameters ()
    :precondition 
      (and
        (in r184)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r185)
        (f_copy)
        (not 
          (in r184))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r184
    :parameters ()
    :precondition 
      (and
        (in r184)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r183)
        (f_copy)
        (not 
          (in r184))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r185
    :parameters ()
    :precondition 
      (and
        (in r185)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r186)
        (f_copy)
        (not 
          (in r185))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r185
    :parameters ()
    :precondition 
      (and
        (in r185)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r184)
        (f_copy)
        (not 
          (in r185))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r186
    :parameters ()
    :precondition 
      (and
        (in r186)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r187)
        (f_copy)
        (not 
          (in r186))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r186
    :parameters ()
    :precondition 
      (and
        (in r186)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r185)
        (f_copy)
        (not 
          (in r186))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r187
    :parameters ()
    :precondition 
      (and
        (in r187)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r188)
        (f_copy)
        (not 
          (in r187))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r187
    :parameters ()
    :precondition 
      (and
        (in r187)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r186)
        (f_copy)
        (not 
          (in r187))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r188
    :parameters ()
    :precondition 
      (and
        (in r188)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r189)
        (f_copy)
        (not 
          (in r188))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r188
    :parameters ()
    :precondition 
      (and
        (in r188)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r187)
        (f_copy)
        (not 
          (in r188))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r189
    :parameters ()
    :precondition 
      (and
        (in r189)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r190)
        (f_copy)
        (not 
          (in r189))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r189
    :parameters ()
    :precondition 
      (and
        (in r189)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r188)
        (f_copy)
        (not 
          (in r189))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r190
    :parameters ()
    :precondition 
      (and
        (in r190)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r191)
        (f_copy)
        (not 
          (in r190))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r190
    :parameters ()
    :precondition 
      (and
        (in r190)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r189)
        (f_copy)
        (not 
          (in r190))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r191
    :parameters ()
    :precondition 
      (and
        (in r191)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r192)
        (f_copy)
        (not 
          (in r191))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r191
    :parameters ()
    :precondition 
      (and
        (in r191)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r190)
        (f_copy)
        (not 
          (in r191))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r192
    :parameters ()
    :precondition 
      (and
        (in r192)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r193)
        (f_copy)
        (not 
          (in r192))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r192
    :parameters ()
    :precondition 
      (and
        (in r192)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r191)
        (f_copy)
        (not 
          (in r192))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r193
    :parameters ()
    :precondition 
      (and
        (in r193)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r194)
        (f_copy)
        (not 
          (in r193))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r193
    :parameters ()
    :precondition 
      (and
        (in r193)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r192)
        (f_copy)
        (not 
          (in r193))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r194
    :parameters ()
    :precondition 
      (and
        (in r194)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r195)
        (f_copy)
        (not 
          (in r194))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r194
    :parameters ()
    :precondition 
      (and
        (in r194)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r193)
        (f_copy)
        (not 
          (in r194))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r195
    :parameters ()
    :precondition 
      (and
        (in r195)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r196)
        (f_copy)
        (not 
          (in r195))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r195
    :parameters ()
    :precondition 
      (and
        (in r195)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r194)
        (f_copy)
        (not 
          (in r195))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r196
    :parameters ()
    :precondition 
      (and
        (in r196)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r197)
        (f_copy)
        (not 
          (in r196))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r196
    :parameters ()
    :precondition 
      (and
        (in r196)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r195)
        (f_copy)
        (not 
          (in r196))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r197
    :parameters ()
    :precondition 
      (and
        (in r197)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r198)
        (f_copy)
        (not 
          (in r197))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r197
    :parameters ()
    :precondition 
      (and
        (in r197)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r196)
        (f_copy)
        (not 
          (in r197))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r198
    :parameters ()
    :precondition 
      (and
        (in r198)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r199)
        (f_copy)
        (not 
          (in r198))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r198
    :parameters ()
    :precondition 
      (and
        (in r198)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r197)
        (f_copy)
        (not 
          (in r198))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r199
    :parameters ()
    :precondition 
      (and
        (in r199)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r200)
        (f_copy)
        (not 
          (in r199))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r199
    :parameters ()
    :precondition 
      (and
        (in r199)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r198)
        (f_copy)
        (not 
          (in r199))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r200
    :parameters ()
    :precondition 
      (and
        (in r200)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r201)
        (f_copy)
        (not 
          (in r200))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r200
    :parameters ()
    :precondition 
      (and
        (in r200)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r199)
        (f_copy)
        (not 
          (in r200))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r201
    :parameters ()
    :precondition 
      (and
        (in r201)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r202)
        (f_copy)
        (not 
          (in r201))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r201
    :parameters ()
    :precondition 
      (and
        (in r201)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r200)
        (f_copy)
        (not 
          (in r201))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r202
    :parameters ()
    :precondition 
      (and
        (in r202)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r203)
        (f_copy)
        (not 
          (in r202))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r202
    :parameters ()
    :precondition 
      (and
        (in r202)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r201)
        (f_copy)
        (not 
          (in r202))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r203
    :parameters ()
    :precondition 
      (and
        (in r203)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r204)
        (f_copy)
        (not 
          (in r203))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r203
    :parameters ()
    :precondition 
      (and
        (in r203)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r202)
        (f_copy)
        (not 
          (in r203))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r204
    :parameters ()
    :precondition 
      (and
        (in r204)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r205)
        (f_copy)
        (not 
          (in r204))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r204
    :parameters ()
    :precondition 
      (and
        (in r204)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r203)
        (f_copy)
        (not 
          (in r204))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r205
    :parameters ()
    :precondition 
      (and
        (in r205)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r206)
        (f_copy)
        (not 
          (in r205))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r205
    :parameters ()
    :precondition 
      (and
        (in r205)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r204)
        (f_copy)
        (not 
          (in r205))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r206
    :parameters ()
    :precondition 
      (and
        (in r206)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r207)
        (f_copy)
        (not 
          (in r206))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r206
    :parameters ()
    :precondition 
      (and
        (in r206)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r205)
        (f_copy)
        (not 
          (in r206))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r207
    :parameters ()
    :precondition 
      (and
        (in r207)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r208)
        (f_copy)
        (not 
          (in r207))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r207
    :parameters ()
    :precondition 
      (and
        (in r207)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r206)
        (f_copy)
        (not 
          (in r207))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r208
    :parameters ()
    :precondition 
      (and
        (in r208)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r209)
        (f_copy)
        (not 
          (in r208))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r208
    :parameters ()
    :precondition 
      (and
        (in r208)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r207)
        (f_copy)
        (not 
          (in r208))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r209
    :parameters ()
    :precondition 
      (and
        (in r209)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r210)
        (f_copy)
        (not 
          (in r209))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r209
    :parameters ()
    :precondition 
      (and
        (in r209)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r208)
        (f_copy)
        (not 
          (in r209))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r210
    :parameters ()
    :precondition 
      (and
        (in r210)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r211)
        (f_copy)
        (not 
          (in r210))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r210
    :parameters ()
    :precondition 
      (and
        (in r210)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r209)
        (f_copy)
        (not 
          (in r210))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r211
    :parameters ()
    :precondition 
      (and
        (in r211)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r212)
        (f_copy)
        (not 
          (in r211))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r211
    :parameters ()
    :precondition 
      (and
        (in r211)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r210)
        (f_copy)
        (not 
          (in r211))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r212
    :parameters ()
    :precondition 
      (and
        (in r212)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r213)
        (f_copy)
        (not 
          (in r212))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r212
    :parameters ()
    :precondition 
      (and
        (in r212)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r211)
        (f_copy)
        (not 
          (in r212))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r213
    :parameters ()
    :precondition 
      (and
        (in r213)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r214)
        (f_copy)
        (not 
          (in r213))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r213
    :parameters ()
    :precondition 
      (and
        (in r213)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r212)
        (f_copy)
        (not 
          (in r213))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r214
    :parameters ()
    :precondition 
      (and
        (in r214)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r215)
        (f_copy)
        (not 
          (in r214))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r214
    :parameters ()
    :precondition 
      (and
        (in r214)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r213)
        (f_copy)
        (not 
          (in r214))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r215
    :parameters ()
    :precondition 
      (and
        (in r215)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r216)
        (f_copy)
        (not 
          (in r215))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r215
    :parameters ()
    :precondition 
      (and
        (in r215)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r214)
        (f_copy)
        (not 
          (in r215))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r216
    :parameters ()
    :precondition 
      (and
        (in r216)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r217)
        (f_copy)
        (not 
          (in r216))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r216
    :parameters ()
    :precondition 
      (and
        (in r216)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r215)
        (f_copy)
        (not 
          (in r216))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r217
    :parameters ()
    :precondition 
      (and
        (in r217)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r218)
        (f_copy)
        (not 
          (in r217))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r217
    :parameters ()
    :precondition 
      (and
        (in r217)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r216)
        (f_copy)
        (not 
          (in r217))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r218
    :parameters ()
    :precondition 
      (and
        (in r218)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r219)
        (f_copy)
        (not 
          (in r218))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r218
    :parameters ()
    :precondition 
      (and
        (in r218)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r217)
        (f_copy)
        (not 
          (in r218))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r219
    :parameters ()
    :precondition 
      (and
        (in r219)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r220)
        (f_copy)
        (not 
          (in r219))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r219
    :parameters ()
    :precondition 
      (and
        (in r219)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r218)
        (f_copy)
        (not 
          (in r219))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r220
    :parameters ()
    :precondition 
      (and
        (in r220)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r221)
        (f_copy)
        (not 
          (in r220))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r220
    :parameters ()
    :precondition 
      (and
        (in r220)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r219)
        (f_copy)
        (not 
          (in r220))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r221
    :parameters ()
    :precondition 
      (and
        (in r221)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r222)
        (f_copy)
        (not 
          (in r221))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r221
    :parameters ()
    :precondition 
      (and
        (in r221)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r220)
        (f_copy)
        (not 
          (in r221))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r222
    :parameters ()
    :precondition 
      (and
        (in r222)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r223)
        (f_copy)
        (not 
          (in r222))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r222
    :parameters ()
    :precondition 
      (and
        (in r222)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r221)
        (f_copy)
        (not 
          (in r222))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r223
    :parameters ()
    :precondition 
      (and
        (in r223)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r224)
        (f_copy)
        (not 
          (in r223))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r223
    :parameters ()
    :precondition 
      (and
        (in r223)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r222)
        (f_copy)
        (not 
          (in r223))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r224
    :parameters ()
    :precondition 
      (and
        (in r224)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r225)
        (f_copy)
        (not 
          (in r224))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r224
    :parameters ()
    :precondition 
      (and
        (in r224)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r223)
        (f_copy)
        (not 
          (in r224))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r225
    :parameters ()
    :precondition 
      (and
        (in r225)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r226)
        (f_copy)
        (not 
          (in r225))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r225
    :parameters ()
    :precondition 
      (and
        (in r225)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r224)
        (f_copy)
        (not 
          (in r225))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r226
    :parameters ()
    :precondition 
      (and
        (in r226)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r227)
        (f_copy)
        (not 
          (in r226))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r226
    :parameters ()
    :precondition 
      (and
        (in r226)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r225)
        (f_copy)
        (not 
          (in r226))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r227
    :parameters ()
    :precondition 
      (and
        (in r227)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r228)
        (f_copy)
        (not 
          (in r227))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r227
    :parameters ()
    :precondition 
      (and
        (in r227)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r226)
        (f_copy)
        (not 
          (in r227))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r228
    :parameters ()
    :precondition 
      (and
        (in r228)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r229)
        (f_copy)
        (not 
          (in r228))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r228
    :parameters ()
    :precondition 
      (and
        (in r228)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r227)
        (f_copy)
        (not 
          (in r228))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r229
    :parameters ()
    :precondition 
      (and
        (in r229)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r230)
        (f_copy)
        (not 
          (in r229))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r229
    :parameters ()
    :precondition 
      (and
        (in r229)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r228)
        (f_copy)
        (not 
          (in r229))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r230
    :parameters ()
    :precondition 
      (and
        (in r230)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r231)
        (f_copy)
        (not 
          (in r230))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r230
    :parameters ()
    :precondition 
      (and
        (in r230)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r229)
        (f_copy)
        (not 
          (in r230))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r231
    :parameters ()
    :precondition 
      (and
        (in r231)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r232)
        (f_copy)
        (not 
          (in r231))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r231
    :parameters ()
    :precondition 
      (and
        (in r231)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r230)
        (f_copy)
        (not 
          (in r231))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r232
    :parameters ()
    :precondition 
      (and
        (in r232)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r233)
        (f_copy)
        (not 
          (in r232))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r232
    :parameters ()
    :precondition 
      (and
        (in r232)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r231)
        (f_copy)
        (not 
          (in r232))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r233
    :parameters ()
    :precondition 
      (and
        (in r233)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r234)
        (f_copy)
        (not 
          (in r233))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r233
    :parameters ()
    :precondition 
      (and
        (in r233)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r232)
        (f_copy)
        (not 
          (in r233))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r234
    :parameters ()
    :precondition 
      (and
        (in r234)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r235)
        (f_copy)
        (not 
          (in r234))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r234
    :parameters ()
    :precondition 
      (and
        (in r234)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r233)
        (f_copy)
        (not 
          (in r234))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r235
    :parameters ()
    :precondition 
      (and
        (in r235)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r236)
        (f_copy)
        (not 
          (in r235))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r235
    :parameters ()
    :precondition 
      (and
        (in r235)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r234)
        (f_copy)
        (not 
          (in r235))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r236
    :parameters ()
    :precondition 
      (and
        (in r236)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r237)
        (f_copy)
        (not 
          (in r236))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r236
    :parameters ()
    :precondition 
      (and
        (in r236)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r235)
        (f_copy)
        (not 
          (in r236))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r237
    :parameters ()
    :precondition 
      (and
        (in r237)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r238)
        (f_copy)
        (not 
          (in r237))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r237
    :parameters ()
    :precondition 
      (and
        (in r237)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r236)
        (f_copy)
        (not 
          (in r237))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r238
    :parameters ()
    :precondition 
      (and
        (in r238)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r239)
        (f_copy)
        (not 
          (in r238))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r238
    :parameters ()
    :precondition 
      (and
        (in r238)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r237)
        (f_copy)
        (not 
          (in r238))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r239
    :parameters ()
    :precondition 
      (and
        (in r239)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r240)
        (f_copy)
        (not 
          (in r239))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r239
    :parameters ()
    :precondition 
      (and
        (in r239)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r238)
        (f_copy)
        (not 
          (in r239))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r240
    :parameters ()
    :precondition 
      (and
        (in r240)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r241)
        (f_copy)
        (not 
          (in r240))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r240
    :parameters ()
    :precondition 
      (and
        (in r240)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r239)
        (f_copy)
        (not 
          (in r240))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r241
    :parameters ()
    :precondition 
      (and
        (in r241)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r242)
        (f_copy)
        (not 
          (in r241))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r241
    :parameters ()
    :precondition 
      (and
        (in r241)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r240)
        (f_copy)
        (not 
          (in r241))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r242
    :parameters ()
    :precondition 
      (and
        (in r242)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r243)
        (f_copy)
        (not 
          (in r242))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r242
    :parameters ()
    :precondition 
      (and
        (in r242)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r241)
        (f_copy)
        (not 
          (in r242))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r243
    :parameters ()
    :precondition 
      (and
        (in r243)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r244)
        (f_copy)
        (not 
          (in r243))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r243
    :parameters ()
    :precondition 
      (and
        (in r243)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r242)
        (f_copy)
        (not 
          (in r243))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r244
    :parameters ()
    :precondition 
      (and
        (in r244)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r245)
        (f_copy)
        (not 
          (in r244))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r244
    :parameters ()
    :precondition 
      (and
        (in r244)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r243)
        (f_copy)
        (not 
          (in r244))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r245
    :parameters ()
    :precondition 
      (and
        (in r245)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r246)
        (f_copy)
        (not 
          (in r245))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r245
    :parameters ()
    :precondition 
      (and
        (in r245)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r244)
        (f_copy)
        (not 
          (in r245))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r246
    :parameters ()
    :precondition 
      (and
        (in r246)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r247)
        (f_copy)
        (not 
          (in r246))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r246
    :parameters ()
    :precondition 
      (and
        (in r246)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r245)
        (f_copy)
        (not 
          (in r246))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r247
    :parameters ()
    :precondition 
      (and
        (in r247)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r248)
        (f_copy)
        (not 
          (in r247))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r247
    :parameters ()
    :precondition 
      (and
        (in r247)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r246)
        (f_copy)
        (not 
          (in r247))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r248
    :parameters ()
    :precondition 
      (and
        (in r248)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r249)
        (f_copy)
        (not 
          (in r248))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r248
    :parameters ()
    :precondition 
      (and
        (in r248)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r247)
        (f_copy)
        (not 
          (in r248))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r249
    :parameters ()
    :precondition 
      (and
        (in r249)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r250)
        (f_copy)
        (not 
          (in r249))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r249
    :parameters ()
    :precondition 
      (and
        (in r249)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r248)
        (f_copy)
        (not 
          (in r249))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r250
    :parameters ()
    :precondition 
      (and
        (in r250)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r251)
        (f_copy)
        (not 
          (in r250))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r250
    :parameters ()
    :precondition 
      (and
        (in r250)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r249)
        (f_copy)
        (not 
          (in r250))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r251
    :parameters ()
    :precondition 
      (and
        (in r251)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r252)
        (f_copy)
        (not 
          (in r251))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r251
    :parameters ()
    :precondition 
      (and
        (in r251)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r250)
        (f_copy)
        (not 
          (in r251))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r252
    :parameters ()
    :precondition 
      (and
        (in r252)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r253)
        (f_copy)
        (not 
          (in r252))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r252
    :parameters ()
    :precondition 
      (and
        (in r252)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r251)
        (f_copy)
        (not 
          (in r252))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r253
    :parameters ()
    :precondition 
      (and
        (in r253)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r254)
        (f_copy)
        (not 
          (in r253))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r253
    :parameters ()
    :precondition 
      (and
        (in r253)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r252)
        (f_copy)
        (not 
          (in r253))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r254
    :parameters ()
    :precondition 
      (and
        (in r254)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r255)
        (f_copy)
        (not 
          (in r254))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r254
    :parameters ()
    :precondition 
      (and
        (in r254)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r253)
        (f_copy)
        (not 
          (in r254))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r255
    :parameters ()
    :precondition 
      (and
        (in r255)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r256)
        (f_copy)
        (not 
          (in r255))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r255
    :parameters ()
    :precondition 
      (and
        (in r255)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r254)
        (f_copy)
        (not 
          (in r255))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r256
    :parameters ()
    :precondition 
      (and
        (in r256)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r257)
        (f_copy)
        (not 
          (in r256))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r256
    :parameters ()
    :precondition 
      (and
        (in r256)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r255)
        (f_copy)
        (not 
          (in r256))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r257
    :parameters ()
    :precondition 
      (and
        (in r257)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r258)
        (f_copy)
        (not 
          (in r257))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r257
    :parameters ()
    :precondition 
      (and
        (in r257)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r256)
        (f_copy)
        (not 
          (in r257))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r258
    :parameters ()
    :precondition 
      (and
        (in r258)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r259)
        (f_copy)
        (not 
          (in r258))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r258
    :parameters ()
    :precondition 
      (and
        (in r258)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r257)
        (f_copy)
        (not 
          (in r258))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r259
    :parameters ()
    :precondition 
      (and
        (in r259)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r260)
        (f_copy)
        (not 
          (in r259))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r259
    :parameters ()
    :precondition 
      (and
        (in r259)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r258)
        (f_copy)
        (not 
          (in r259))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r260
    :parameters ()
    :precondition 
      (and
        (in r260)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r261)
        (f_copy)
        (not 
          (in r260))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r260
    :parameters ()
    :precondition 
      (and
        (in r260)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r259)
        (f_copy)
        (not 
          (in r260))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r261
    :parameters ()
    :precondition 
      (and
        (in r261)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r262)
        (f_copy)
        (not 
          (in r261))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r261
    :parameters ()
    :precondition 
      (and
        (in r261)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r260)
        (f_copy)
        (not 
          (in r261))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r262
    :parameters ()
    :precondition 
      (and
        (in r262)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r263)
        (f_copy)
        (not 
          (in r262))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r262
    :parameters ()
    :precondition 
      (and
        (in r262)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r261)
        (f_copy)
        (not 
          (in r262))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r263
    :parameters ()
    :precondition 
      (and
        (in r263)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r264)
        (f_copy)
        (not 
          (in r263))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r263
    :parameters ()
    :precondition 
      (and
        (in r263)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r262)
        (f_copy)
        (not 
          (in r263))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r264
    :parameters ()
    :precondition 
      (and
        (in r264)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r265)
        (f_copy)
        (not 
          (in r264))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r264
    :parameters ()
    :precondition 
      (and
        (in r264)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r263)
        (f_copy)
        (not 
          (in r264))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r265
    :parameters ()
    :precondition 
      (and
        (in r265)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r266)
        (f_copy)
        (not 
          (in r265))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r265
    :parameters ()
    :precondition 
      (and
        (in r265)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r264)
        (f_copy)
        (not 
          (in r265))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r266
    :parameters ()
    :precondition 
      (and
        (in r266)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r267)
        (f_copy)
        (not 
          (in r266))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r266
    :parameters ()
    :precondition 
      (and
        (in r266)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r265)
        (f_copy)
        (not 
          (in r266))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r267
    :parameters ()
    :precondition 
      (and
        (in r267)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r268)
        (f_copy)
        (not 
          (in r267))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r267
    :parameters ()
    :precondition 
      (and
        (in r267)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r266)
        (f_copy)
        (not 
          (in r267))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r268
    :parameters ()
    :precondition 
      (and
        (in r268)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r269)
        (f_copy)
        (not 
          (in r268))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r268
    :parameters ()
    :precondition 
      (and
        (in r268)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r267)
        (f_copy)
        (not 
          (in r268))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r269
    :parameters ()
    :precondition 
      (and
        (in r269)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r270)
        (f_copy)
        (not 
          (in r269))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r269
    :parameters ()
    :precondition 
      (and
        (in r269)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r268)
        (f_copy)
        (not 
          (in r269))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r270
    :parameters ()
    :precondition 
      (and
        (in r270)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r271)
        (f_copy)
        (not 
          (in r270))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r270
    :parameters ()
    :precondition 
      (and
        (in r270)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r269)
        (f_copy)
        (not 
          (in r270))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r271
    :parameters ()
    :precondition 
      (and
        (in r271)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r272)
        (f_copy)
        (not 
          (in r271))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r271
    :parameters ()
    :precondition 
      (and
        (in r271)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r270)
        (f_copy)
        (not 
          (in r271))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r272
    :parameters ()
    :precondition 
      (and
        (in r272)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r273)
        (f_copy)
        (not 
          (in r272))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r272
    :parameters ()
    :precondition 
      (and
        (in r272)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r271)
        (f_copy)
        (not 
          (in r272))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r273
    :parameters ()
    :precondition 
      (and
        (in r273)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r274)
        (f_copy)
        (not 
          (in r273))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r273
    :parameters ()
    :precondition 
      (and
        (in r273)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r272)
        (f_copy)
        (not 
          (in r273))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r274
    :parameters ()
    :precondition 
      (and
        (in r274)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r275)
        (f_copy)
        (not 
          (in r274))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r274
    :parameters ()
    :precondition 
      (and
        (in r274)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r273)
        (f_copy)
        (not 
          (in r274))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r275
    :parameters ()
    :precondition 
      (and
        (in r275)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r276)
        (f_copy)
        (not 
          (in r275))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r275
    :parameters ()
    :precondition 
      (and
        (in r275)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r274)
        (f_copy)
        (not 
          (in r275))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r276
    :parameters ()
    :precondition 
      (and
        (in r276)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r277)
        (f_copy)
        (not 
          (in r276))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r276
    :parameters ()
    :precondition 
      (and
        (in r276)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r275)
        (f_copy)
        (not 
          (in r276))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r277
    :parameters ()
    :precondition 
      (and
        (in r277)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r278)
        (f_copy)
        (not 
          (in r277))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r277
    :parameters ()
    :precondition 
      (and
        (in r277)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r276)
        (f_copy)
        (not 
          (in r277))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r278
    :parameters ()
    :precondition 
      (and
        (in r278)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r279)
        (f_copy)
        (not 
          (in r278))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r278
    :parameters ()
    :precondition 
      (and
        (in r278)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r277)
        (f_copy)
        (not 
          (in r278))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r279
    :parameters ()
    :precondition 
      (and
        (in r279)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r280)
        (f_copy)
        (not 
          (in r279))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r279
    :parameters ()
    :precondition 
      (and
        (in r279)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r278)
        (f_copy)
        (not 
          (in r279))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r280
    :parameters ()
    :precondition 
      (and
        (in r280)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r281)
        (f_copy)
        (not 
          (in r280))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r280
    :parameters ()
    :precondition 
      (and
        (in r280)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r279)
        (f_copy)
        (not 
          (in r280))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r281
    :parameters ()
    :precondition 
      (and
        (in r281)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r282)
        (f_copy)
        (not 
          (in r281))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r281
    :parameters ()
    :precondition 
      (and
        (in r281)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r280)
        (f_copy)
        (not 
          (in r281))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r282
    :parameters ()
    :precondition 
      (and
        (in r282)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r283)
        (f_copy)
        (not 
          (in r282))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r282
    :parameters ()
    :precondition 
      (and
        (in r282)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r281)
        (f_copy)
        (not 
          (in r282))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r283
    :parameters ()
    :precondition 
      (and
        (in r283)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r284)
        (f_copy)
        (not 
          (in r283))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r283
    :parameters ()
    :precondition 
      (and
        (in r283)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r282)
        (f_copy)
        (not 
          (in r283))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r284
    :parameters ()
    :precondition 
      (and
        (in r284)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r285)
        (f_copy)
        (not 
          (in r284))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r284
    :parameters ()
    :precondition 
      (and
        (in r284)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r283)
        (f_copy)
        (not 
          (in r284))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r285
    :parameters ()
    :precondition 
      (and
        (in r285)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r286)
        (f_copy)
        (not 
          (in r285))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r285
    :parameters ()
    :precondition 
      (and
        (in r285)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r284)
        (f_copy)
        (not 
          (in r285))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r286
    :parameters ()
    :precondition 
      (and
        (in r286)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r287)
        (f_copy)
        (not 
          (in r286))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r286
    :parameters ()
    :precondition 
      (and
        (in r286)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r285)
        (f_copy)
        (not 
          (in r286))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r287
    :parameters ()
    :precondition 
      (and
        (in r287)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r288)
        (f_copy)
        (not 
          (in r287))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r287
    :parameters ()
    :precondition 
      (and
        (in r287)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r286)
        (f_copy)
        (not 
          (in r287))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r288
    :parameters ()
    :precondition 
      (and
        (in r288)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r289)
        (f_copy)
        (not 
          (in r288))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r288
    :parameters ()
    :precondition 
      (and
        (in r288)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r287)
        (f_copy)
        (not 
          (in r288))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r289
    :parameters ()
    :precondition 
      (and
        (in r289)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r290)
        (f_copy)
        (not 
          (in r289))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r289
    :parameters ()
    :precondition 
      (and
        (in r289)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r288)
        (f_copy)
        (not 
          (in r289))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r290
    :parameters ()
    :precondition 
      (and
        (in r290)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r291)
        (f_copy)
        (not 
          (in r290))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r290
    :parameters ()
    :precondition 
      (and
        (in r290)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r289)
        (f_copy)
        (not 
          (in r290))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r291
    :parameters ()
    :precondition 
      (and
        (in r291)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r292)
        (f_copy)
        (not 
          (in r291))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r291
    :parameters ()
    :precondition 
      (and
        (in r291)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r290)
        (f_copy)
        (not 
          (in r291))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r292
    :parameters ()
    :precondition 
      (and
        (in r292)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r293)
        (f_copy)
        (not 
          (in r292))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r292
    :parameters ()
    :precondition 
      (and
        (in r292)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r291)
        (f_copy)
        (not 
          (in r292))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r293
    :parameters ()
    :precondition 
      (and
        (in r293)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r294)
        (f_copy)
        (not 
          (in r293))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r293
    :parameters ()
    :precondition 
      (and
        (in r293)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r292)
        (f_copy)
        (not 
          (in r293))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r294
    :parameters ()
    :precondition 
      (and
        (in r294)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r295)
        (f_copy)
        (not 
          (in r294))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r294
    :parameters ()
    :precondition 
      (and
        (in r294)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r293)
        (f_copy)
        (not 
          (in r294))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r295
    :parameters ()
    :precondition 
      (and
        (in r295)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r296)
        (f_copy)
        (not 
          (in r295))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r295
    :parameters ()
    :precondition 
      (and
        (in r295)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r294)
        (f_copy)
        (not 
          (in r295))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r296
    :parameters ()
    :precondition 
      (and
        (in r296)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r297)
        (f_copy)
        (not 
          (in r296))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r296
    :parameters ()
    :precondition 
      (and
        (in r296)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r295)
        (f_copy)
        (not 
          (in r296))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r297
    :parameters ()
    :precondition 
      (and
        (in r297)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r298)
        (f_copy)
        (not 
          (in r297))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r297
    :parameters ()
    :precondition 
      (and
        (in r297)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r296)
        (f_copy)
        (not 
          (in r297))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r298
    :parameters ()
    :precondition 
      (and
        (in r298)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r299)
        (f_copy)
        (not 
          (in r298))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r298
    :parameters ()
    :precondition 
      (and
        (in r298)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r297)
        (f_copy)
        (not 
          (in r298))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r299
    :parameters ()
    :precondition 
      (and
        (in r299)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r300)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r299))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r299
    :parameters ()
    :precondition 
      (and
        (in r299)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r298)
        (f_copy)
        (not 
          (in r299))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r300
    :parameters ()
    :precondition 
      (and
        (in r300)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r301)
        (f_copy)
        (not 
          (in r300))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r300
    :parameters ()
    :precondition 
      (and
        (in r300)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r299)
        (f_copy)
        (not 
          (in r300))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r301
    :parameters ()
    :precondition 
      (and
        (in r301)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r302)
        (f_copy)
        (not 
          (in r301))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r301
    :parameters ()
    :precondition 
      (and
        (in r301)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r300)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r301))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r302
    :parameters ()
    :precondition 
      (and
        (in r302)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r303)
        (f_copy)
        (not 
          (in r302))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r302
    :parameters ()
    :precondition 
      (and
        (in r302)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r301)
        (f_copy)
        (not 
          (in r302))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r303
    :parameters ()
    :precondition 
      (and
        (in r303)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r304)
        (f_copy)
        (not 
          (in r303))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r303
    :parameters ()
    :precondition 
      (and
        (in r303)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r302)
        (f_copy)
        (not 
          (in r303))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r304
    :parameters ()
    :precondition 
      (and
        (in r304)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r305)
        (f_copy)
        (not 
          (in r304))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r304
    :parameters ()
    :precondition 
      (and
        (in r304)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r303)
        (f_copy)
        (not 
          (in r304))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r305
    :parameters ()
    :precondition 
      (and
        (in r305)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r306)
        (f_copy)
        (not 
          (in r305))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r305
    :parameters ()
    :precondition 
      (and
        (in r305)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r304)
        (f_copy)
        (not 
          (in r305))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r306
    :parameters ()
    :precondition 
      (and
        (in r306)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r307)
        (f_copy)
        (not 
          (in r306))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r306
    :parameters ()
    :precondition 
      (and
        (in r306)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r305)
        (f_copy)
        (not 
          (in r306))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r307
    :parameters ()
    :precondition 
      (and
        (in r307)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r308)
        (f_copy)
        (not 
          (in r307))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r307
    :parameters ()
    :precondition 
      (and
        (in r307)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r306)
        (f_copy)
        (not 
          (in r307))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r308
    :parameters ()
    :precondition 
      (and
        (in r308)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r309)
        (f_copy)
        (not 
          (in r308))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r308
    :parameters ()
    :precondition 
      (and
        (in r308)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r307)
        (f_copy)
        (not 
          (in r308))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r309
    :parameters ()
    :precondition 
      (and
        (in r309)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r310)
        (f_copy)
        (not 
          (in r309))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r309
    :parameters ()
    :precondition 
      (and
        (in r309)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r308)
        (f_copy)
        (not 
          (in r309))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r310
    :parameters ()
    :precondition 
      (and
        (in r310)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r311)
        (f_copy)
        (not 
          (in r310))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r310
    :parameters ()
    :precondition 
      (and
        (in r310)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r309)
        (f_copy)
        (not 
          (in r310))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r311
    :parameters ()
    :precondition 
      (and
        (in r311)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r312)
        (f_copy)
        (not 
          (in r311))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r311
    :parameters ()
    :precondition 
      (and
        (in r311)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r310)
        (f_copy)
        (not 
          (in r311))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r312
    :parameters ()
    :precondition 
      (and
        (in r312)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r313)
        (f_copy)
        (not 
          (in r312))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r312
    :parameters ()
    :precondition 
      (and
        (in r312)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r311)
        (f_copy)
        (not 
          (in r312))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r313
    :parameters ()
    :precondition 
      (and
        (in r313)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r314)
        (f_copy)
        (not 
          (in r313))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r313
    :parameters ()
    :precondition 
      (and
        (in r313)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r312)
        (f_copy)
        (not 
          (in r313))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r314
    :parameters ()
    :precondition 
      (and
        (in r314)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r315)
        (f_copy)
        (not 
          (in r314))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r314
    :parameters ()
    :precondition 
      (and
        (in r314)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r313)
        (f_copy)
        (not 
          (in r314))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r315
    :parameters ()
    :precondition 
      (and
        (in r315)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r316)
        (f_copy)
        (not 
          (in r315))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r315
    :parameters ()
    :precondition 
      (and
        (in r315)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r314)
        (f_copy)
        (not 
          (in r315))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r316
    :parameters ()
    :precondition 
      (and
        (in r316)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r317)
        (f_copy)
        (not 
          (in r316))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r316
    :parameters ()
    :precondition 
      (and
        (in r316)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r315)
        (f_copy)
        (not 
          (in r316))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r317
    :parameters ()
    :precondition 
      (and
        (in r317)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r318)
        (f_copy)
        (not 
          (in r317))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r317
    :parameters ()
    :precondition 
      (and
        (in r317)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r316)
        (f_copy)
        (not 
          (in r317))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r318
    :parameters ()
    :precondition 
      (and
        (in r318)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r319)
        (f_copy)
        (not 
          (in r318))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r318
    :parameters ()
    :precondition 
      (and
        (in r318)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r317)
        (f_copy)
        (not 
          (in r318))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r319
    :parameters ()
    :precondition 
      (and
        (in r319)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r320)
        (f_copy)
        (not 
          (in r319))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r319
    :parameters ()
    :precondition 
      (and
        (in r319)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r318)
        (f_copy)
        (not 
          (in r319))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r320
    :parameters ()
    :precondition 
      (and
        (in r320)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r321)
        (f_copy)
        (not 
          (in r320))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r320
    :parameters ()
    :precondition 
      (and
        (in r320)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r319)
        (f_copy)
        (not 
          (in r320))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r321
    :parameters ()
    :precondition 
      (and
        (in r321)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r322)
        (f_copy)
        (not 
          (in r321))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r321
    :parameters ()
    :precondition 
      (and
        (in r321)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r320)
        (f_copy)
        (not 
          (in r321))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r322
    :parameters ()
    :precondition 
      (and
        (in r322)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r323)
        (f_copy)
        (not 
          (in r322))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r322
    :parameters ()
    :precondition 
      (and
        (in r322)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r321)
        (f_copy)
        (not 
          (in r322))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r323
    :parameters ()
    :precondition 
      (and
        (in r323)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r324)
        (f_copy)
        (not 
          (in r323))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r323
    :parameters ()
    :precondition 
      (and
        (in r323)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r322)
        (f_copy)
        (not 
          (in r323))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r324
    :parameters ()
    :precondition 
      (and
        (in r324)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r325)
        (f_copy)
        (not 
          (in r324))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r324
    :parameters ()
    :precondition 
      (and
        (in r324)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r323)
        (f_copy)
        (not 
          (in r324))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r325
    :parameters ()
    :precondition 
      (and
        (in r325)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r326)
        (f_copy)
        (not 
          (in r325))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r325
    :parameters ()
    :precondition 
      (and
        (in r325)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r324)
        (f_copy)
        (not 
          (in r325))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r326
    :parameters ()
    :precondition 
      (and
        (in r326)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r327)
        (f_copy)
        (not 
          (in r326))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r326
    :parameters ()
    :precondition 
      (and
        (in r326)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r325)
        (f_copy)
        (not 
          (in r326))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r327
    :parameters ()
    :precondition 
      (and
        (in r327)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r328)
        (f_copy)
        (not 
          (in r327))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r327
    :parameters ()
    :precondition 
      (and
        (in r327)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r326)
        (f_copy)
        (not 
          (in r327))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r328
    :parameters ()
    :precondition 
      (and
        (in r328)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r329)
        (f_copy)
        (not 
          (in r328))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r328
    :parameters ()
    :precondition 
      (and
        (in r328)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r327)
        (f_copy)
        (not 
          (in r328))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r329
    :parameters ()
    :precondition 
      (and
        (in r329)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r330)
        (f_copy)
        (not 
          (in r329))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r329
    :parameters ()
    :precondition 
      (and
        (in r329)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r328)
        (f_copy)
        (not 
          (in r329))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r330
    :parameters ()
    :precondition 
      (and
        (in r330)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r331)
        (f_copy)
        (not 
          (in r330))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r330
    :parameters ()
    :precondition 
      (and
        (in r330)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r329)
        (f_copy)
        (not 
          (in r330))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r331
    :parameters ()
    :precondition 
      (and
        (in r331)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r332)
        (f_copy)
        (not 
          (in r331))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r331
    :parameters ()
    :precondition 
      (and
        (in r331)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r330)
        (f_copy)
        (not 
          (in r331))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r332
    :parameters ()
    :precondition 
      (and
        (in r332)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r333)
        (f_copy)
        (not 
          (in r332))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r332
    :parameters ()
    :precondition 
      (and
        (in r332)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r331)
        (f_copy)
        (not 
          (in r332))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r333
    :parameters ()
    :precondition 
      (and
        (in r333)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r334)
        (f_copy)
        (not 
          (in r333))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r333
    :parameters ()
    :precondition 
      (and
        (in r333)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r332)
        (f_copy)
        (not 
          (in r333))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r334
    :parameters ()
    :precondition 
      (and
        (in r334)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r335)
        (f_copy)
        (not 
          (in r334))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r334
    :parameters ()
    :precondition 
      (and
        (in r334)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r333)
        (f_copy)
        (not 
          (in r334))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r335
    :parameters ()
    :precondition 
      (and
        (in r335)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r336)
        (f_copy)
        (not 
          (in r335))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r335
    :parameters ()
    :precondition 
      (and
        (in r335)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r334)
        (f_copy)
        (not 
          (in r335))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r336
    :parameters ()
    :precondition 
      (and
        (in r336)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r337)
        (f_copy)
        (not 
          (in r336))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r336
    :parameters ()
    :precondition 
      (and
        (in r336)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r335)
        (f_copy)
        (not 
          (in r336))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r337
    :parameters ()
    :precondition 
      (and
        (in r337)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r338)
        (f_copy)
        (not 
          (in r337))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r337
    :parameters ()
    :precondition 
      (and
        (in r337)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r336)
        (f_copy)
        (not 
          (in r337))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r338
    :parameters ()
    :precondition 
      (and
        (in r338)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r339)
        (f_copy)
        (not 
          (in r338))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r338
    :parameters ()
    :precondition 
      (and
        (in r338)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r337)
        (f_copy)
        (not 
          (in r338))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r339
    :parameters ()
    :precondition 
      (and
        (in r339)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r340)
        (f_copy)
        (not 
          (in r339))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r339
    :parameters ()
    :precondition 
      (and
        (in r339)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r338)
        (f_copy)
        (not 
          (in r339))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r340
    :parameters ()
    :precondition 
      (and
        (in r340)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r341)
        (f_copy)
        (not 
          (in r340))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r340
    :parameters ()
    :precondition 
      (and
        (in r340)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r339)
        (f_copy)
        (not 
          (in r340))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r341
    :parameters ()
    :precondition 
      (and
        (in r341)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r342)
        (f_copy)
        (not 
          (in r341))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r341
    :parameters ()
    :precondition 
      (and
        (in r341)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r340)
        (f_copy)
        (not 
          (in r341))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r342
    :parameters ()
    :precondition 
      (and
        (in r342)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r343)
        (f_copy)
        (not 
          (in r342))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r342
    :parameters ()
    :precondition 
      (and
        (in r342)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r341)
        (f_copy)
        (not 
          (in r342))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r343
    :parameters ()
    :precondition 
      (and
        (in r343)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r344)
        (f_copy)
        (not 
          (in r343))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r343
    :parameters ()
    :precondition 
      (and
        (in r343)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r342)
        (f_copy)
        (not 
          (in r343))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r344
    :parameters ()
    :precondition 
      (and
        (in r344)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r345)
        (f_copy)
        (not 
          (in r344))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r344
    :parameters ()
    :precondition 
      (and
        (in r344)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r343)
        (f_copy)
        (not 
          (in r344))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r345
    :parameters ()
    :precondition 
      (and
        (in r345)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r346)
        (f_copy)
        (not 
          (in r345))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r345
    :parameters ()
    :precondition 
      (and
        (in r345)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r344)
        (f_copy)
        (not 
          (in r345))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r346
    :parameters ()
    :precondition 
      (and
        (in r346)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r347)
        (f_copy)
        (not 
          (in r346))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r346
    :parameters ()
    :precondition 
      (and
        (in r346)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r345)
        (f_copy)
        (not 
          (in r346))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r347
    :parameters ()
    :precondition 
      (and
        (in r347)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r348)
        (f_copy)
        (not 
          (in r347))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r347
    :parameters ()
    :precondition 
      (and
        (in r347)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r346)
        (f_copy)
        (not 
          (in r347))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r348
    :parameters ()
    :precondition 
      (and
        (in r348)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r349)
        (f_copy)
        (not 
          (in r348))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r348
    :parameters ()
    :precondition 
      (and
        (in r348)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r347)
        (f_copy)
        (not 
          (in r348))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r349
    :parameters ()
    :precondition 
      (and
        (in r349)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r350)
        (f_copy)
        (not 
          (in r349))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r349
    :parameters ()
    :precondition 
      (and
        (in r349)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r348)
        (f_copy)
        (not 
          (in r349))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r350
    :parameters ()
    :precondition 
      (and
        (in r350)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r351)
        (f_copy)
        (not 
          (in r350))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r350
    :parameters ()
    :precondition 
      (and
        (in r350)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r349)
        (f_copy)
        (not 
          (in r350))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r351
    :parameters ()
    :precondition 
      (and
        (in r351)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r352)
        (f_copy)
        (not 
          (in r351))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r351
    :parameters ()
    :precondition 
      (and
        (in r351)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r350)
        (f_copy)
        (not 
          (in r351))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r352
    :parameters ()
    :precondition 
      (and
        (in r352)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r353)
        (f_copy)
        (not 
          (in r352))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r352
    :parameters ()
    :precondition 
      (and
        (in r352)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r351)
        (f_copy)
        (not 
          (in r352))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r353
    :parameters ()
    :precondition 
      (and
        (in r353)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r354)
        (f_copy)
        (not 
          (in r353))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r353
    :parameters ()
    :precondition 
      (and
        (in r353)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r352)
        (f_copy)
        (not 
          (in r353))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r354
    :parameters ()
    :precondition 
      (and
        (in r354)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r355)
        (f_copy)
        (not 
          (in r354))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r354
    :parameters ()
    :precondition 
      (and
        (in r354)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r353)
        (f_copy)
        (not 
          (in r354))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r355
    :parameters ()
    :precondition 
      (and
        (in r355)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r356)
        (f_copy)
        (not 
          (in r355))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r355
    :parameters ()
    :precondition 
      (and
        (in r355)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r354)
        (f_copy)
        (not 
          (in r355))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r356
    :parameters ()
    :precondition 
      (and
        (in r356)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r357)
        (f_copy)
        (not 
          (in r356))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r356
    :parameters ()
    :precondition 
      (and
        (in r356)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r355)
        (f_copy)
        (not 
          (in r356))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r357
    :parameters ()
    :precondition 
      (and
        (in r357)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r358)
        (f_copy)
        (not 
          (in r357))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r357
    :parameters ()
    :precondition 
      (and
        (in r357)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r356)
        (f_copy)
        (not 
          (in r357))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r358
    :parameters ()
    :precondition 
      (and
        (in r358)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r359)
        (f_copy)
        (not 
          (in r358))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r358
    :parameters ()
    :precondition 
      (and
        (in r358)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r357)
        (f_copy)
        (not 
          (in r358))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r359
    :parameters ()
    :precondition 
      (and
        (in r359)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r360)
        (f_copy)
        (not 
          (in r359))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r359
    :parameters ()
    :precondition 
      (and
        (in r359)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r358)
        (f_copy)
        (not 
          (in r359))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r360
    :parameters ()
    :precondition 
      (and
        (in r360)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r361)
        (f_copy)
        (not 
          (in r360))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r360
    :parameters ()
    :precondition 
      (and
        (in r360)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r359)
        (f_copy)
        (not 
          (in r360))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r361
    :parameters ()
    :precondition 
      (and
        (in r361)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r362)
        (f_copy)
        (not 
          (in r361))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r361
    :parameters ()
    :precondition 
      (and
        (in r361)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r360)
        (f_copy)
        (not 
          (in r361))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r362
    :parameters ()
    :precondition 
      (and
        (in r362)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r363)
        (f_copy)
        (not 
          (in r362))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r362
    :parameters ()
    :precondition 
      (and
        (in r362)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r361)
        (f_copy)
        (not 
          (in r362))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r363
    :parameters ()
    :precondition 
      (and
        (in r363)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r364)
        (f_copy)
        (not 
          (in r363))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r363
    :parameters ()
    :precondition 
      (and
        (in r363)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r362)
        (f_copy)
        (not 
          (in r363))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r364
    :parameters ()
    :precondition 
      (and
        (in r364)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r365)
        (f_copy)
        (not 
          (in r364))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r364
    :parameters ()
    :precondition 
      (and
        (in r364)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r363)
        (f_copy)
        (not 
          (in r364))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r365
    :parameters ()
    :precondition 
      (and
        (in r365)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r366)
        (f_copy)
        (not 
          (in r365))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r365
    :parameters ()
    :precondition 
      (and
        (in r365)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r364)
        (f_copy)
        (not 
          (in r365))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r366
    :parameters ()
    :precondition 
      (and
        (in r366)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r367)
        (f_copy)
        (not 
          (in r366))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r366
    :parameters ()
    :precondition 
      (and
        (in r366)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r365)
        (f_copy)
        (not 
          (in r366))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r367
    :parameters ()
    :precondition 
      (and
        (in r367)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r368)
        (f_copy)
        (not 
          (in r367))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r367
    :parameters ()
    :precondition 
      (and
        (in r367)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r366)
        (f_copy)
        (not 
          (in r367))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r368
    :parameters ()
    :precondition 
      (and
        (in r368)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r369)
        (f_copy)
        (not 
          (in r368))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r368
    :parameters ()
    :precondition 
      (and
        (in r368)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r367)
        (f_copy)
        (not 
          (in r368))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r369
    :parameters ()
    :precondition 
      (and
        (in r369)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r370)
        (f_copy)
        (not 
          (in r369))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r369
    :parameters ()
    :precondition 
      (and
        (in r369)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r368)
        (f_copy)
        (not 
          (in r369))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r370
    :parameters ()
    :precondition 
      (and
        (in r370)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r371)
        (f_copy)
        (not 
          (in r370))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r370
    :parameters ()
    :precondition 
      (and
        (in r370)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r369)
        (f_copy)
        (not 
          (in r370))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r371
    :parameters ()
    :precondition 
      (and
        (in r371)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r372)
        (f_copy)
        (not 
          (in r371))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r371
    :parameters ()
    :precondition 
      (and
        (in r371)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r370)
        (f_copy)
        (not 
          (in r371))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r372
    :parameters ()
    :precondition 
      (and
        (in r372)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r373)
        (f_copy)
        (not 
          (in r372))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r372
    :parameters ()
    :precondition 
      (and
        (in r372)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r371)
        (f_copy)
        (not 
          (in r372))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r373
    :parameters ()
    :precondition 
      (and
        (in r373)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r374)
        (f_copy)
        (not 
          (in r373))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r373
    :parameters ()
    :precondition 
      (and
        (in r373)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r372)
        (f_copy)
        (not 
          (in r373))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r374
    :parameters ()
    :precondition 
      (and
        (in r374)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r375)
        (f_copy)
        (not 
          (in r374))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r374
    :parameters ()
    :precondition 
      (and
        (in r374)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r373)
        (f_copy)
        (not 
          (in r374))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r375
    :parameters ()
    :precondition 
      (and
        (in r375)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r376)
        (f_copy)
        (not 
          (in r375))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r375
    :parameters ()
    :precondition 
      (and
        (in r375)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r374)
        (f_copy)
        (not 
          (in r375))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r376
    :parameters ()
    :precondition 
      (and
        (in r376)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r377)
        (f_copy)
        (not 
          (in r376))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r376
    :parameters ()
    :precondition 
      (and
        (in r376)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r375)
        (f_copy)
        (not 
          (in r376))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r377
    :parameters ()
    :precondition 
      (and
        (in r377)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r378)
        (f_copy)
        (not 
          (in r377))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r377
    :parameters ()
    :precondition 
      (and
        (in r377)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r376)
        (f_copy)
        (not 
          (in r377))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r378
    :parameters ()
    :precondition 
      (and
        (in r378)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r379)
        (f_copy)
        (not 
          (in r378))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r378
    :parameters ()
    :precondition 
      (and
        (in r378)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r377)
        (f_copy)
        (not 
          (in r378))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r379
    :parameters ()
    :precondition 
      (and
        (in r379)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r380)
        (f_copy)
        (not 
          (in r379))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r379
    :parameters ()
    :precondition 
      (and
        (in r379)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r378)
        (f_copy)
        (not 
          (in r379))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r380
    :parameters ()
    :precondition 
      (and
        (in r380)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r381)
        (f_copy)
        (not 
          (in r380))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r380
    :parameters ()
    :precondition 
      (and
        (in r380)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r379)
        (f_copy)
        (not 
          (in r380))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r381
    :parameters ()
    :precondition 
      (and
        (in r381)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r382)
        (f_copy)
        (not 
          (in r381))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r381
    :parameters ()
    :precondition 
      (and
        (in r381)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r380)
        (f_copy)
        (not 
          (in r381))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r382
    :parameters ()
    :precondition 
      (and
        (in r382)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r383)
        (f_copy)
        (not 
          (in r382))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r382
    :parameters ()
    :precondition 
      (and
        (in r382)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r381)
        (f_copy)
        (not 
          (in r382))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r383
    :parameters ()
    :precondition 
      (and
        (in r383)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r384)
        (f_copy)
        (not 
          (in r383))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r383
    :parameters ()
    :precondition 
      (and
        (in r383)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r382)
        (f_copy)
        (not 
          (in r383))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r384
    :parameters ()
    :precondition 
      (and
        (in r384)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r385)
        (f_copy)
        (not 
          (in r384))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r384
    :parameters ()
    :precondition 
      (and
        (in r384)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r383)
        (f_copy)
        (not 
          (in r384))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r385
    :parameters ()
    :precondition 
      (and
        (in r385)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r386)
        (f_copy)
        (not 
          (in r385))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r385
    :parameters ()
    :precondition 
      (and
        (in r385)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r384)
        (f_copy)
        (not 
          (in r385))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r386
    :parameters ()
    :precondition 
      (and
        (in r386)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r387)
        (f_copy)
        (not 
          (in r386))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r386
    :parameters ()
    :precondition 
      (and
        (in r386)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r385)
        (f_copy)
        (not 
          (in r386))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r387
    :parameters ()
    :precondition 
      (and
        (in r387)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r388)
        (f_copy)
        (not 
          (in r387))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r387
    :parameters ()
    :precondition 
      (and
        (in r387)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r386)
        (f_copy)
        (not 
          (in r387))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r388
    :parameters ()
    :precondition 
      (and
        (in r388)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r389)
        (f_copy)
        (not 
          (in r388))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r388
    :parameters ()
    :precondition 
      (and
        (in r388)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r387)
        (f_copy)
        (not 
          (in r388))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r389
    :parameters ()
    :precondition 
      (and
        (in r389)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r390)
        (f_copy)
        (not 
          (in r389))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r389
    :parameters ()
    :precondition 
      (and
        (in r389)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r388)
        (f_copy)
        (not 
          (in r389))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r390
    :parameters ()
    :precondition 
      (and
        (in r390)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r391)
        (f_copy)
        (not 
          (in r390))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r390
    :parameters ()
    :precondition 
      (and
        (in r390)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r389)
        (f_copy)
        (not 
          (in r390))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r391
    :parameters ()
    :precondition 
      (and
        (in r391)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r392)
        (f_copy)
        (not 
          (in r391))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r391
    :parameters ()
    :precondition 
      (and
        (in r391)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r390)
        (f_copy)
        (not 
          (in r391))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r392
    :parameters ()
    :precondition 
      (and
        (in r392)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r393)
        (f_copy)
        (not 
          (in r392))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r392
    :parameters ()
    :precondition 
      (and
        (in r392)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r391)
        (f_copy)
        (not 
          (in r392))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r393
    :parameters ()
    :precondition 
      (and
        (in r393)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r394)
        (f_copy)
        (not 
          (in r393))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r393
    :parameters ()
    :precondition 
      (and
        (in r393)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r392)
        (f_copy)
        (not 
          (in r393))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r394
    :parameters ()
    :precondition 
      (and
        (in r394)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r395)
        (f_copy)
        (not 
          (in r394))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r394
    :parameters ()
    :precondition 
      (and
        (in r394)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r393)
        (f_copy)
        (not 
          (in r394))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r395
    :parameters ()
    :precondition 
      (and
        (in r395)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r396)
        (f_copy)
        (not 
          (in r395))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r395
    :parameters ()
    :precondition 
      (and
        (in r395)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r394)
        (f_copy)
        (not 
          (in r395))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r396
    :parameters ()
    :precondition 
      (and
        (in r396)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r397)
        (f_copy)
        (not 
          (in r396))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r396
    :parameters ()
    :precondition 
      (and
        (in r396)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r395)
        (f_copy)
        (not 
          (in r396))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r397
    :parameters ()
    :precondition 
      (and
        (in r397)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r398)
        (f_copy)
        (not 
          (in r397))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r397
    :parameters ()
    :precondition 
      (and
        (in r397)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r396)
        (f_copy)
        (not 
          (in r397))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r398
    :parameters ()
    :precondition 
      (and
        (in r398)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r399)
        (f_copy)
        (not 
          (in r398))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r398
    :parameters ()
    :precondition 
      (and
        (in r398)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r397)
        (f_copy)
        (not 
          (in r398))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r399
    :parameters ()
    :precondition 
      (and
        (in r399)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r400)
        (f_copy)
        (not 
          (in r399))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r399
    :parameters ()
    :precondition 
      (and
        (in r399)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r398)
        (f_copy)
        (not 
          (in r399))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r400
    :parameters ()
    :precondition 
      (and
        (in r400)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r401)
        (f_copy)
        (not 
          (in r400))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r400
    :parameters ()
    :precondition 
      (and
        (in r400)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r399)
        (f_copy)
        (not 
          (in r400))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r401
    :parameters ()
    :precondition 
      (and
        (in r401)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r402)
        (f_copy)
        (not 
          (in r401))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r401
    :parameters ()
    :precondition 
      (and
        (in r401)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r400)
        (f_copy)
        (not 
          (in r401))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r402
    :parameters ()
    :precondition 
      (and
        (in r402)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r403)
        (f_copy)
        (not 
          (in r402))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r402
    :parameters ()
    :precondition 
      (and
        (in r402)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r401)
        (f_copy)
        (not 
          (in r402))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r403
    :parameters ()
    :precondition 
      (and
        (in r403)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r404)
        (f_copy)
        (not 
          (in r403))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r403
    :parameters ()
    :precondition 
      (and
        (in r403)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r402)
        (f_copy)
        (not 
          (in r403))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r404
    :parameters ()
    :precondition 
      (and
        (in r404)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r405)
        (f_copy)
        (not 
          (in r404))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r404
    :parameters ()
    :precondition 
      (and
        (in r404)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r403)
        (f_copy)
        (not 
          (in r404))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r405
    :parameters ()
    :precondition 
      (and
        (in r405)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r406)
        (f_copy)
        (not 
          (in r405))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r405
    :parameters ()
    :precondition 
      (and
        (in r405)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r404)
        (f_copy)
        (not 
          (in r405))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r406
    :parameters ()
    :precondition 
      (and
        (in r406)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r407)
        (f_copy)
        (not 
          (in r406))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r406
    :parameters ()
    :precondition 
      (and
        (in r406)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r405)
        (f_copy)
        (not 
          (in r406))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r407
    :parameters ()
    :precondition 
      (and
        (in r407)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r408)
        (f_copy)
        (not 
          (in r407))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r407
    :parameters ()
    :precondition 
      (and
        (in r407)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r406)
        (f_copy)
        (not 
          (in r407))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r408
    :parameters ()
    :precondition 
      (and
        (in r408)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r409)
        (f_copy)
        (not 
          (in r408))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r408
    :parameters ()
    :precondition 
      (and
        (in r408)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r407)
        (f_copy)
        (not 
          (in r408))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r409
    :parameters ()
    :precondition 
      (and
        (in r409)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r410)
        (f_copy)
        (not 
          (in r409))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r409
    :parameters ()
    :precondition 
      (and
        (in r409)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r408)
        (f_copy)
        (not 
          (in r409))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r410
    :parameters ()
    :precondition 
      (and
        (in r410)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r411)
        (f_copy)
        (not 
          (in r410))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r410
    :parameters ()
    :precondition 
      (and
        (in r410)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r409)
        (f_copy)
        (not 
          (in r410))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r411
    :parameters ()
    :precondition 
      (and
        (in r411)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r412)
        (f_copy)
        (not 
          (in r411))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r411
    :parameters ()
    :precondition 
      (and
        (in r411)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r410)
        (f_copy)
        (not 
          (in r411))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r412
    :parameters ()
    :precondition 
      (and
        (in r412)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r413)
        (f_copy)
        (not 
          (in r412))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r412
    :parameters ()
    :precondition 
      (and
        (in r412)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r411)
        (f_copy)
        (not 
          (in r412))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r413
    :parameters ()
    :precondition 
      (and
        (in r413)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r414)
        (f_copy)
        (not 
          (in r413))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r413
    :parameters ()
    :precondition 
      (and
        (in r413)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r412)
        (f_copy)
        (not 
          (in r413))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r414
    :parameters ()
    :precondition 
      (and
        (in r414)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r415)
        (f_copy)
        (not 
          (in r414))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r414
    :parameters ()
    :precondition 
      (and
        (in r414)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r413)
        (f_copy)
        (not 
          (in r414))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r415
    :parameters ()
    :precondition 
      (and
        (in r415)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r416)
        (f_copy)
        (not 
          (in r415))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r415
    :parameters ()
    :precondition 
      (and
        (in r415)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r414)
        (f_copy)
        (not 
          (in r415))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r416
    :parameters ()
    :precondition 
      (and
        (in r416)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r417)
        (f_copy)
        (not 
          (in r416))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r416
    :parameters ()
    :precondition 
      (and
        (in r416)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r415)
        (f_copy)
        (not 
          (in r416))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r417
    :parameters ()
    :precondition 
      (and
        (in r417)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r418)
        (f_copy)
        (not 
          (in r417))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r417
    :parameters ()
    :precondition 
      (and
        (in r417)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r416)
        (f_copy)
        (not 
          (in r417))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r418
    :parameters ()
    :precondition 
      (and
        (in r418)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r419)
        (f_copy)
        (not 
          (in r418))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r418
    :parameters ()
    :precondition 
      (and
        (in r418)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r417)
        (f_copy)
        (not 
          (in r418))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r419
    :parameters ()
    :precondition 
      (and
        (in r419)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r420)
        (f_copy)
        (not 
          (in r419))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r419
    :parameters ()
    :precondition 
      (and
        (in r419)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r418)
        (f_copy)
        (not 
          (in r419))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r420
    :parameters ()
    :precondition 
      (and
        (in r420)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r421)
        (f_copy)
        (not 
          (in r420))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r420
    :parameters ()
    :precondition 
      (and
        (in r420)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r419)
        (f_copy)
        (not 
          (in r420))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r421
    :parameters ()
    :precondition 
      (and
        (in r421)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r422)
        (f_copy)
        (not 
          (in r421))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r421
    :parameters ()
    :precondition 
      (and
        (in r421)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r420)
        (f_copy)
        (not 
          (in r421))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r422
    :parameters ()
    :precondition 
      (and
        (in r422)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r423)
        (f_copy)
        (not 
          (in r422))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r422
    :parameters ()
    :precondition 
      (and
        (in r422)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r421)
        (f_copy)
        (not 
          (in r422))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r423
    :parameters ()
    :precondition 
      (and
        (in r423)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r424)
        (f_copy)
        (not 
          (in r423))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r423
    :parameters ()
    :precondition 
      (and
        (in r423)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r422)
        (f_copy)
        (not 
          (in r423))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r424
    :parameters ()
    :precondition 
      (and
        (in r424)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r425)
        (f_copy)
        (not 
          (in r424))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r424
    :parameters ()
    :precondition 
      (and
        (in r424)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r423)
        (f_copy)
        (not 
          (in r424))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r425
    :parameters ()
    :precondition 
      (and
        (in r425)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r426)
        (f_copy)
        (not 
          (in r425))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r425
    :parameters ()
    :precondition 
      (and
        (in r425)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r424)
        (f_copy)
        (not 
          (in r425))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r426
    :parameters ()
    :precondition 
      (and
        (in r426)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r427)
        (f_copy)
        (not 
          (in r426))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r426
    :parameters ()
    :precondition 
      (and
        (in r426)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r425)
        (f_copy)
        (not 
          (in r426))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r427
    :parameters ()
    :precondition 
      (and
        (in r427)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r428)
        (f_copy)
        (not 
          (in r427))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r427
    :parameters ()
    :precondition 
      (and
        (in r427)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r426)
        (f_copy)
        (not 
          (in r427))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r428
    :parameters ()
    :precondition 
      (and
        (in r428)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r429)
        (f_copy)
        (not 
          (in r428))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r428
    :parameters ()
    :precondition 
      (and
        (in r428)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r427)
        (f_copy)
        (not 
          (in r428))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r429
    :parameters ()
    :precondition 
      (and
        (in r429)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r430)
        (f_copy)
        (not 
          (in r429))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r429
    :parameters ()
    :precondition 
      (and
        (in r429)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r428)
        (f_copy)
        (not 
          (in r429))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r430
    :parameters ()
    :precondition 
      (and
        (in r430)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r431)
        (f_copy)
        (not 
          (in r430))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r430
    :parameters ()
    :precondition 
      (and
        (in r430)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r429)
        (f_copy)
        (not 
          (in r430))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r431
    :parameters ()
    :precondition 
      (and
        (in r431)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r432)
        (f_copy)
        (not 
          (in r431))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r431
    :parameters ()
    :precondition 
      (and
        (in r431)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r430)
        (f_copy)
        (not 
          (in r431))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r432
    :parameters ()
    :precondition 
      (and
        (in r432)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r433)
        (f_copy)
        (not 
          (in r432))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r432
    :parameters ()
    :precondition 
      (and
        (in r432)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r431)
        (f_copy)
        (not 
          (in r432))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r433
    :parameters ()
    :precondition 
      (and
        (in r433)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r434)
        (f_copy)
        (not 
          (in r433))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r433
    :parameters ()
    :precondition 
      (and
        (in r433)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r432)
        (f_copy)
        (not 
          (in r433))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r434
    :parameters ()
    :precondition 
      (and
        (in r434)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r435)
        (f_copy)
        (not 
          (in r434))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r434
    :parameters ()
    :precondition 
      (and
        (in r434)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r433)
        (f_copy)
        (not 
          (in r434))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r435
    :parameters ()
    :precondition 
      (and
        (in r435)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r436)
        (f_copy)
        (not 
          (in r435))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r435
    :parameters ()
    :precondition 
      (and
        (in r435)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r434)
        (f_copy)
        (not 
          (in r435))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r436
    :parameters ()
    :precondition 
      (and
        (in r436)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r437)
        (f_copy)
        (not 
          (in r436))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r436
    :parameters ()
    :precondition 
      (and
        (in r436)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r435)
        (f_copy)
        (not 
          (in r436))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r437
    :parameters ()
    :precondition 
      (and
        (in r437)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r438)
        (f_copy)
        (not 
          (in r437))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r437
    :parameters ()
    :precondition 
      (and
        (in r437)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r436)
        (f_copy)
        (not 
          (in r437))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r438
    :parameters ()
    :precondition 
      (and
        (in r438)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r439)
        (f_copy)
        (not 
          (in r438))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r438
    :parameters ()
    :precondition 
      (and
        (in r438)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r437)
        (f_copy)
        (not 
          (in r438))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r439
    :parameters ()
    :precondition 
      (and
        (in r439)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r440)
        (f_copy)
        (not 
          (in r439))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r439
    :parameters ()
    :precondition 
      (and
        (in r439)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r438)
        (f_copy)
        (not 
          (in r439))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r440
    :parameters ()
    :precondition 
      (and
        (in r440)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r441)
        (f_copy)
        (not 
          (in r440))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r440
    :parameters ()
    :precondition 
      (and
        (in r440)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r439)
        (f_copy)
        (not 
          (in r440))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r441
    :parameters ()
    :precondition 
      (and
        (in r441)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r442)
        (f_copy)
        (not 
          (in r441))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r441
    :parameters ()
    :precondition 
      (and
        (in r441)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r440)
        (f_copy)
        (not 
          (in r441))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r442
    :parameters ()
    :precondition 
      (and
        (in r442)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r443)
        (f_copy)
        (not 
          (in r442))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r442
    :parameters ()
    :precondition 
      (and
        (in r442)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r441)
        (f_copy)
        (not 
          (in r442))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r443
    :parameters ()
    :precondition 
      (and
        (in r443)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r444)
        (f_copy)
        (not 
          (in r443))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r443
    :parameters ()
    :precondition 
      (and
        (in r443)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r442)
        (f_copy)
        (not 
          (in r443))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r444
    :parameters ()
    :precondition 
      (and
        (in r444)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r445)
        (f_copy)
        (not 
          (in r444))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r444
    :parameters ()
    :precondition 
      (and
        (in r444)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r443)
        (f_copy)
        (not 
          (in r444))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r445
    :parameters ()
    :precondition 
      (and
        (in r445)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r446)
        (f_copy)
        (not 
          (in r445))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r445
    :parameters ()
    :precondition 
      (and
        (in r445)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r444)
        (f_copy)
        (not 
          (in r445))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r446
    :parameters ()
    :precondition 
      (and
        (in r446)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r447)
        (f_copy)
        (not 
          (in r446))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r446
    :parameters ()
    :precondition 
      (and
        (in r446)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r445)
        (f_copy)
        (not 
          (in r446))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r447
    :parameters ()
    :precondition 
      (and
        (in r447)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r448)
        (f_copy)
        (not 
          (in r447))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r447
    :parameters ()
    :precondition 
      (and
        (in r447)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r446)
        (f_copy)
        (not 
          (in r447))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r448
    :parameters ()
    :precondition 
      (and
        (in r448)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r449)
        (f_copy)
        (not 
          (in r448))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r448
    :parameters ()
    :precondition 
      (and
        (in r448)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r447)
        (f_copy)
        (not 
          (in r448))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r449
    :parameters ()
    :precondition 
      (and
        (in r449)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r450)
        (f_copy)
        (not 
          (in r449))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r449
    :parameters ()
    :precondition 
      (and
        (in r449)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r448)
        (f_copy)
        (not 
          (in r449))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r450
    :parameters ()
    :precondition 
      (and
        (in r450)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r451)
        (f_copy)
        (not 
          (in r450))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r450
    :parameters ()
    :precondition 
      (and
        (in r450)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r449)
        (f_copy)
        (not 
          (in r450))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r451
    :parameters ()
    :precondition 
      (and
        (in r451)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r452)
        (f_copy)
        (not 
          (in r451))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r451
    :parameters ()
    :precondition 
      (and
        (in r451)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r450)
        (f_copy)
        (not 
          (in r451))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r452
    :parameters ()
    :precondition 
      (and
        (in r452)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r453)
        (f_copy)
        (not 
          (in r452))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r452
    :parameters ()
    :precondition 
      (and
        (in r452)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r451)
        (f_copy)
        (not 
          (in r452))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r453
    :parameters ()
    :precondition 
      (and
        (in r453)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r454)
        (f_copy)
        (not 
          (in r453))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r453
    :parameters ()
    :precondition 
      (and
        (in r453)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r452)
        (f_copy)
        (not 
          (in r453))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r454
    :parameters ()
    :precondition 
      (and
        (in r454)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r455)
        (f_copy)
        (not 
          (in r454))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r454
    :parameters ()
    :precondition 
      (and
        (in r454)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r453)
        (f_copy)
        (not 
          (in r454))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r455
    :parameters ()
    :precondition 
      (and
        (in r455)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r456)
        (f_copy)
        (not 
          (in r455))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r455
    :parameters ()
    :precondition 
      (and
        (in r455)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r454)
        (f_copy)
        (not 
          (in r455))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r456
    :parameters ()
    :precondition 
      (and
        (in r456)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r457)
        (f_copy)
        (not 
          (in r456))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r456
    :parameters ()
    :precondition 
      (and
        (in r456)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r455)
        (f_copy)
        (not 
          (in r456))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r457
    :parameters ()
    :precondition 
      (and
        (in r457)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r458)
        (f_copy)
        (not 
          (in r457))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r457
    :parameters ()
    :precondition 
      (and
        (in r457)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r456)
        (f_copy)
        (not 
          (in r457))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r458
    :parameters ()
    :precondition 
      (and
        (in r458)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r459)
        (f_copy)
        (not 
          (in r458))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r458
    :parameters ()
    :precondition 
      (and
        (in r458)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r457)
        (f_copy)
        (not 
          (in r458))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r459
    :parameters ()
    :precondition 
      (and
        (in r459)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r460)
        (f_copy)
        (not 
          (in r459))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r459
    :parameters ()
    :precondition 
      (and
        (in r459)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r458)
        (f_copy)
        (not 
          (in r459))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r460
    :parameters ()
    :precondition 
      (and
        (in r460)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r461)
        (f_copy)
        (not 
          (in r460))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r460
    :parameters ()
    :precondition 
      (and
        (in r460)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r459)
        (f_copy)
        (not 
          (in r460))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r461
    :parameters ()
    :precondition 
      (and
        (in r461)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r462)
        (f_copy)
        (not 
          (in r461))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r461
    :parameters ()
    :precondition 
      (and
        (in r461)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r460)
        (f_copy)
        (not 
          (in r461))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r462
    :parameters ()
    :precondition 
      (and
        (in r462)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r463)
        (f_copy)
        (not 
          (in r462))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r462
    :parameters ()
    :precondition 
      (and
        (in r462)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r461)
        (f_copy)
        (not 
          (in r462))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r463
    :parameters ()
    :precondition 
      (and
        (in r463)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r464)
        (f_copy)
        (not 
          (in r463))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r463
    :parameters ()
    :precondition 
      (and
        (in r463)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r462)
        (f_copy)
        (not 
          (in r463))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r464
    :parameters ()
    :precondition 
      (and
        (in r464)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r465)
        (f_copy)
        (not 
          (in r464))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r464
    :parameters ()
    :precondition 
      (and
        (in r464)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r463)
        (f_copy)
        (not 
          (in r464))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r465
    :parameters ()
    :precondition 
      (and
        (in r465)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r466)
        (f_copy)
        (not 
          (in r465))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r465
    :parameters ()
    :precondition 
      (and
        (in r465)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r464)
        (f_copy)
        (not 
          (in r465))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r466
    :parameters ()
    :precondition 
      (and
        (in r466)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r467)
        (f_copy)
        (not 
          (in r466))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r466
    :parameters ()
    :precondition 
      (and
        (in r466)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r465)
        (f_copy)
        (not 
          (in r466))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r467
    :parameters ()
    :precondition 
      (and
        (in r467)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r468)
        (f_copy)
        (not 
          (in r467))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r467
    :parameters ()
    :precondition 
      (and
        (in r467)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r466)
        (f_copy)
        (not 
          (in r467))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r468
    :parameters ()
    :precondition 
      (and
        (in r468)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r469)
        (f_copy)
        (not 
          (in r468))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r468
    :parameters ()
    :precondition 
      (and
        (in r468)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r467)
        (f_copy)
        (not 
          (in r468))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r469
    :parameters ()
    :precondition 
      (and
        (in r469)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r470)
        (f_copy)
        (not 
          (in r469))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r469
    :parameters ()
    :precondition 
      (and
        (in r469)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r468)
        (f_copy)
        (not 
          (in r469))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r470
    :parameters ()
    :precondition 
      (and
        (in r470)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r471)
        (f_copy)
        (not 
          (in r470))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r470
    :parameters ()
    :precondition 
      (and
        (in r470)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r469)
        (f_copy)
        (not 
          (in r470))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r471
    :parameters ()
    :precondition 
      (and
        (in r471)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r472)
        (f_copy)
        (not 
          (in r471))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r471
    :parameters ()
    :precondition 
      (and
        (in r471)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r470)
        (f_copy)
        (not 
          (in r471))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r472
    :parameters ()
    :precondition 
      (and
        (in r472)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r473)
        (f_copy)
        (not 
          (in r472))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r472
    :parameters ()
    :precondition 
      (and
        (in r472)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r471)
        (f_copy)
        (not 
          (in r472))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r473
    :parameters ()
    :precondition 
      (and
        (in r473)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r474)
        (f_copy)
        (not 
          (in r473))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r473
    :parameters ()
    :precondition 
      (and
        (in r473)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r472)
        (f_copy)
        (not 
          (in r473))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r474
    :parameters ()
    :precondition 
      (and
        (in r474)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r475)
        (f_copy)
        (not 
          (in r474))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r474
    :parameters ()
    :precondition 
      (and
        (in r474)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r473)
        (f_copy)
        (not 
          (in r474))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r475
    :parameters ()
    :precondition 
      (and
        (in r475)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r476)
        (f_copy)
        (not 
          (in r475))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r475
    :parameters ()
    :precondition 
      (and
        (in r475)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r474)
        (f_copy)
        (not 
          (in r475))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r476
    :parameters ()
    :precondition 
      (and
        (in r476)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r477)
        (f_copy)
        (not 
          (in r476))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r476
    :parameters ()
    :precondition 
      (and
        (in r476)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r475)
        (f_copy)
        (not 
          (in r476))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r477
    :parameters ()
    :precondition 
      (and
        (in r477)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r478)
        (f_copy)
        (not 
          (in r477))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r477
    :parameters ()
    :precondition 
      (and
        (in r477)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r476)
        (f_copy)
        (not 
          (in r477))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r478
    :parameters ()
    :precondition 
      (and
        (in r478)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r479)
        (f_copy)
        (not 
          (in r478))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r478
    :parameters ()
    :precondition 
      (and
        (in r478)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r477)
        (f_copy)
        (not 
          (in r478))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r479
    :parameters ()
    :precondition 
      (and
        (in r479)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r480)
        (f_copy)
        (not 
          (in r479))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r479
    :parameters ()
    :precondition 
      (and
        (in r479)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r478)
        (f_copy)
        (not 
          (in r479))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r480
    :parameters ()
    :precondition 
      (and
        (in r480)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r481)
        (f_copy)
        (not 
          (in r480))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r480
    :parameters ()
    :precondition 
      (and
        (in r480)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r479)
        (f_copy)
        (not 
          (in r480))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r481
    :parameters ()
    :precondition 
      (and
        (in r481)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r482)
        (f_copy)
        (not 
          (in r481))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r481
    :parameters ()
    :precondition 
      (and
        (in r481)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r480)
        (f_copy)
        (not 
          (in r481))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r482
    :parameters ()
    :precondition 
      (and
        (in r482)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r483)
        (f_copy)
        (not 
          (in r482))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r482
    :parameters ()
    :precondition 
      (and
        (in r482)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r481)
        (f_copy)
        (not 
          (in r482))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r483
    :parameters ()
    :precondition 
      (and
        (in r483)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r484)
        (f_copy)
        (not 
          (in r483))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r483
    :parameters ()
    :precondition 
      (and
        (in r483)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r482)
        (f_copy)
        (not 
          (in r483))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r484
    :parameters ()
    :precondition 
      (and
        (in r484)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r485)
        (f_copy)
        (not 
          (in r484))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r484
    :parameters ()
    :precondition 
      (and
        (in r484)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r483)
        (f_copy)
        (not 
          (in r484))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r485
    :parameters ()
    :precondition 
      (and
        (in r485)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r486)
        (f_copy)
        (not 
          (in r485))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r485
    :parameters ()
    :precondition 
      (and
        (in r485)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r484)
        (f_copy)
        (not 
          (in r485))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r486
    :parameters ()
    :precondition 
      (and
        (in r486)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r487)
        (f_copy)
        (not 
          (in r486))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r486
    :parameters ()
    :precondition 
      (and
        (in r486)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r485)
        (f_copy)
        (not 
          (in r486))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r487
    :parameters ()
    :precondition 
      (and
        (in r487)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r488)
        (f_copy)
        (not 
          (in r487))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r487
    :parameters ()
    :precondition 
      (and
        (in r487)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r486)
        (f_copy)
        (not 
          (in r487))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r488
    :parameters ()
    :precondition 
      (and
        (in r488)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r489)
        (f_copy)
        (not 
          (in r488))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r488
    :parameters ()
    :precondition 
      (and
        (in r488)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r487)
        (f_copy)
        (not 
          (in r488))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r489
    :parameters ()
    :precondition 
      (and
        (in r489)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r490)
        (f_copy)
        (not 
          (in r489))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r489
    :parameters ()
    :precondition 
      (and
        (in r489)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r488)
        (f_copy)
        (not 
          (in r489))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r490
    :parameters ()
    :precondition 
      (and
        (in r490)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r491)
        (f_copy)
        (not 
          (in r490))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r490
    :parameters ()
    :precondition 
      (and
        (in r490)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r489)
        (f_copy)
        (not 
          (in r490))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r491
    :parameters ()
    :precondition 
      (and
        (in r491)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r492)
        (f_copy)
        (not 
          (in r491))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r491
    :parameters ()
    :precondition 
      (and
        (in r491)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r490)
        (f_copy)
        (not 
          (in r491))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r492
    :parameters ()
    :precondition 
      (and
        (in r492)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r493)
        (f_copy)
        (not 
          (in r492))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r492
    :parameters ()
    :precondition 
      (and
        (in r492)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r491)
        (f_copy)
        (not 
          (in r492))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r493
    :parameters ()
    :precondition 
      (and
        (in r493)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r494)
        (f_copy)
        (not 
          (in r493))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r493
    :parameters ()
    :precondition 
      (and
        (in r493)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r492)
        (f_copy)
        (not 
          (in r493))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r494
    :parameters ()
    :precondition 
      (and
        (in r494)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r495)
        (f_copy)
        (not 
          (in r494))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r494
    :parameters ()
    :precondition 
      (and
        (in r494)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r493)
        (f_copy)
        (not 
          (in r494))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r495
    :parameters ()
    :precondition 
      (and
        (in r495)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r496)
        (f_copy)
        (not 
          (in r495))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r495
    :parameters ()
    :precondition 
      (and
        (in r495)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r494)
        (f_copy)
        (not 
          (in r495))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r496
    :parameters ()
    :precondition 
      (and
        (in r496)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r497)
        (f_copy)
        (not 
          (in r496))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r496
    :parameters ()
    :precondition 
      (and
        (in r496)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r495)
        (f_copy)
        (not 
          (in r496))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r497
    :parameters ()
    :precondition 
      (and
        (in r497)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r498)
        (f_copy)
        (not 
          (in r497))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r497
    :parameters ()
    :precondition 
      (and
        (in r497)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r496)
        (f_copy)
        (not 
          (in r497))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r498
    :parameters ()
    :precondition 
      (and
        (in r498)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r499)
        (f_copy)
        (not 
          (in r498))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r498
    :parameters ()
    :precondition 
      (and
        (in r498)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r497)
        (f_copy)
        (not 
          (in r498))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r499
    :parameters ()
    :precondition 
      (and
        (in r499)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r500)
        (f_copy)
        (not 
          (in r499))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r499
    :parameters ()
    :precondition 
      (and
        (in r499)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r498)
        (f_copy)
        (not 
          (in r499))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r500
    :parameters ()
    :precondition 
      (and
        (in r500)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r501)
        (f_copy)
        (not 
          (in r500))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r500
    :parameters ()
    :precondition 
      (and
        (in r500)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r499)
        (f_copy)
        (not 
          (in r500))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r501
    :parameters ()
    :precondition 
      (and
        (in r501)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r502)
        (f_copy)
        (not 
          (in r501))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r501
    :parameters ()
    :precondition 
      (and
        (in r501)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r500)
        (f_copy)
        (not 
          (in r501))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r502
    :parameters ()
    :precondition 
      (and
        (in r502)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r503)
        (f_copy)
        (not 
          (in r502))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r502
    :parameters ()
    :precondition 
      (and
        (in r502)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r501)
        (f_copy)
        (not 
          (in r502))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r503
    :parameters ()
    :precondition 
      (and
        (in r503)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r504)
        (f_copy)
        (not 
          (in r503))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r503
    :parameters ()
    :precondition 
      (and
        (in r503)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r502)
        (f_copy)
        (not 
          (in r503))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r504
    :parameters ()
    :precondition 
      (and
        (in r504)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r505)
        (f_copy)
        (not 
          (in r504))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r504
    :parameters ()
    :precondition 
      (and
        (in r504)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r503)
        (f_copy)
        (not 
          (in r504))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r505
    :parameters ()
    :precondition 
      (and
        (in r505)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r506)
        (f_copy)
        (not 
          (in r505))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r505
    :parameters ()
    :precondition 
      (and
        (in r505)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r504)
        (f_copy)
        (not 
          (in r505))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r506
    :parameters ()
    :precondition 
      (and
        (in r506)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r507)
        (f_copy)
        (not 
          (in r506))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r506
    :parameters ()
    :precondition 
      (and
        (in r506)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r505)
        (f_copy)
        (not 
          (in r506))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r507
    :parameters ()
    :precondition 
      (and
        (in r507)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r508)
        (f_copy)
        (not 
          (in r507))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r507
    :parameters ()
    :precondition 
      (and
        (in r507)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r506)
        (f_copy)
        (not 
          (in r507))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r508
    :parameters ()
    :precondition 
      (and
        (in r508)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r509)
        (f_copy)
        (not 
          (in r508))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r508
    :parameters ()
    :precondition 
      (and
        (in r508)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r507)
        (f_copy)
        (not 
          (in r508))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r509
    :parameters ()
    :precondition 
      (and
        (in r509)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r510)
        (f_copy)
        (not 
          (in r509))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r509
    :parameters ()
    :precondition 
      (and
        (in r509)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r508)
        (f_copy)
        (not 
          (in r509))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r510
    :parameters ()
    :precondition 
      (and
        (in r510)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r511)
        (f_copy)
        (not 
          (in r510))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r510
    :parameters ()
    :precondition 
      (and
        (in r510)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r509)
        (f_copy)
        (not 
          (in r510))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r511
    :parameters ()
    :precondition 
      (and
        (in r511)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r512)
        (f_copy)
        (not 
          (in r511))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r511
    :parameters ()
    :precondition 
      (and
        (in r511)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r510)
        (f_copy)
        (not 
          (in r511))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r512
    :parameters ()
    :precondition 
      (and
        (in r512)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r513)
        (f_copy)
        (not 
          (in r512))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r512
    :parameters ()
    :precondition 
      (and
        (in r512)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r511)
        (f_copy)
        (not 
          (in r512))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r513
    :parameters ()
    :precondition 
      (and
        (in r513)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r514)
        (f_copy)
        (not 
          (in r513))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r513
    :parameters ()
    :precondition 
      (and
        (in r513)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r512)
        (f_copy)
        (not 
          (in r513))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r514
    :parameters ()
    :precondition 
      (and
        (in r514)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r515)
        (f_copy)
        (not 
          (in r514))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r514
    :parameters ()
    :precondition 
      (and
        (in r514)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r513)
        (f_copy)
        (not 
          (in r514))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r515
    :parameters ()
    :precondition 
      (and
        (in r515)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r516)
        (f_copy)
        (not 
          (in r515))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r515
    :parameters ()
    :precondition 
      (and
        (in r515)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r514)
        (f_copy)
        (not 
          (in r515))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r516
    :parameters ()
    :precondition 
      (and
        (in r516)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r517)
        (f_copy)
        (not 
          (in r516))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r516
    :parameters ()
    :precondition 
      (and
        (in r516)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r515)
        (f_copy)
        (not 
          (in r516))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r517
    :parameters ()
    :precondition 
      (and
        (in r517)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r518)
        (f_copy)
        (not 
          (in r517))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r517
    :parameters ()
    :precondition 
      (and
        (in r517)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r516)
        (f_copy)
        (not 
          (in r517))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r518
    :parameters ()
    :precondition 
      (and
        (in r518)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r519)
        (f_copy)
        (not 
          (in r518))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r518
    :parameters ()
    :precondition 
      (and
        (in r518)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r517)
        (f_copy)
        (not 
          (in r518))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r519
    :parameters ()
    :precondition 
      (and
        (in r519)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r520)
        (f_copy)
        (not 
          (in r519))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r519
    :parameters ()
    :precondition 
      (and
        (in r519)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r518)
        (f_copy)
        (not 
          (in r519))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r520
    :parameters ()
    :precondition 
      (and
        (in r520)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r521)
        (f_copy)
        (not 
          (in r520))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r520
    :parameters ()
    :precondition 
      (and
        (in r520)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r519)
        (f_copy)
        (not 
          (in r520))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r521
    :parameters ()
    :precondition 
      (and
        (in r521)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r522)
        (f_copy)
        (not 
          (in r521))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r521
    :parameters ()
    :precondition 
      (and
        (in r521)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r520)
        (f_copy)
        (not 
          (in r521))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r522
    :parameters ()
    :precondition 
      (and
        (in r522)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r523)
        (f_copy)
        (not 
          (in r522))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r522
    :parameters ()
    :precondition 
      (and
        (in r522)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r521)
        (f_copy)
        (not 
          (in r522))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r523
    :parameters ()
    :precondition 
      (and
        (in r523)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r524)
        (f_copy)
        (not 
          (in r523))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r523
    :parameters ()
    :precondition 
      (and
        (in r523)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r522)
        (f_copy)
        (not 
          (in r523))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r524
    :parameters ()
    :precondition 
      (and
        (in r524)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r525)
        (f_copy)
        (not 
          (in r524))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r524
    :parameters ()
    :precondition 
      (and
        (in r524)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r523)
        (f_copy)
        (not 
          (in r524))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r525
    :parameters ()
    :precondition 
      (and
        (in r525)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r526)
        (f_copy)
        (not 
          (in r525))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r525
    :parameters ()
    :precondition 
      (and
        (in r525)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r524)
        (f_copy)
        (not 
          (in r525))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r526
    :parameters ()
    :precondition 
      (and
        (in r526)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r527)
        (f_copy)
        (not 
          (in r526))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r526
    :parameters ()
    :precondition 
      (and
        (in r526)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r525)
        (f_copy)
        (not 
          (in r526))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r527
    :parameters ()
    :precondition 
      (and
        (in r527)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r528)
        (f_copy)
        (not 
          (in r527))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r527
    :parameters ()
    :precondition 
      (and
        (in r527)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r526)
        (f_copy)
        (not 
          (in r527))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r528
    :parameters ()
    :precondition 
      (and
        (in r528)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r529)
        (f_copy)
        (not 
          (in r528))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r528
    :parameters ()
    :precondition 
      (and
        (in r528)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r527)
        (f_copy)
        (not 
          (in r528))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r529
    :parameters ()
    :precondition 
      (and
        (in r529)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r530)
        (f_copy)
        (not 
          (in r529))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r529
    :parameters ()
    :precondition 
      (and
        (in r529)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r528)
        (f_copy)
        (not 
          (in r529))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r530
    :parameters ()
    :precondition 
      (and
        (in r530)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r531)
        (f_copy)
        (not 
          (in r530))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r530
    :parameters ()
    :precondition 
      (and
        (in r530)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r529)
        (f_copy)
        (not 
          (in r530))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r531
    :parameters ()
    :precondition 
      (and
        (in r531)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r532)
        (f_copy)
        (not 
          (in r531))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r531
    :parameters ()
    :precondition 
      (and
        (in r531)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r530)
        (f_copy)
        (not 
          (in r531))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r532
    :parameters ()
    :precondition 
      (and
        (in r532)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r533)
        (f_copy)
        (not 
          (in r532))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r532
    :parameters ()
    :precondition 
      (and
        (in r532)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r531)
        (f_copy)
        (not 
          (in r532))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r533
    :parameters ()
    :precondition 
      (and
        (in r533)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r534)
        (f_copy)
        (not 
          (in r533))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r533
    :parameters ()
    :precondition 
      (and
        (in r533)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r532)
        (f_copy)
        (not 
          (in r533))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r534
    :parameters ()
    :precondition 
      (and
        (in r534)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r535)
        (f_copy)
        (not 
          (in r534))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r534
    :parameters ()
    :precondition 
      (and
        (in r534)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r533)
        (f_copy)
        (not 
          (in r534))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r535
    :parameters ()
    :precondition 
      (and
        (in r535)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r536)
        (f_copy)
        (not 
          (in r535))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r535
    :parameters ()
    :precondition 
      (and
        (in r535)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r534)
        (f_copy)
        (not 
          (in r535))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r536
    :parameters ()
    :precondition 
      (and
        (in r536)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r537)
        (f_copy)
        (not 
          (in r536))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r536
    :parameters ()
    :precondition 
      (and
        (in r536)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r535)
        (f_copy)
        (not 
          (in r536))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r537
    :parameters ()
    :precondition 
      (and
        (in r537)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r538)
        (f_copy)
        (not 
          (in r537))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r537
    :parameters ()
    :precondition 
      (and
        (in r537)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r536)
        (f_copy)
        (not 
          (in r537))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r538
    :parameters ()
    :precondition 
      (and
        (in r538)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r539)
        (f_copy)
        (not 
          (in r538))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r538
    :parameters ()
    :precondition 
      (and
        (in r538)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r537)
        (f_copy)
        (not 
          (in r538))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r539
    :parameters ()
    :precondition 
      (and
        (in r539)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r540)
        (f_copy)
        (not 
          (in r539))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r539
    :parameters ()
    :precondition 
      (and
        (in r539)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r538)
        (f_copy)
        (not 
          (in r539))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r540
    :parameters ()
    :precondition 
      (and
        (in r540)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r541)
        (f_copy)
        (not 
          (in r540))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r540
    :parameters ()
    :precondition 
      (and
        (in r540)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r539)
        (f_copy)
        (not 
          (in r540))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r541
    :parameters ()
    :precondition 
      (and
        (in r541)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r542)
        (f_copy)
        (not 
          (in r541))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r541
    :parameters ()
    :precondition 
      (and
        (in r541)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r540)
        (f_copy)
        (not 
          (in r541))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r542
    :parameters ()
    :precondition 
      (and
        (in r542)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r543)
        (f_copy)
        (not 
          (in r542))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r542
    :parameters ()
    :precondition 
      (and
        (in r542)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r541)
        (f_copy)
        (not 
          (in r542))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r543
    :parameters ()
    :precondition 
      (and
        (in r543)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r544)
        (f_copy)
        (not 
          (in r543))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r543
    :parameters ()
    :precondition 
      (and
        (in r543)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r542)
        (f_copy)
        (not 
          (in r543))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r544
    :parameters ()
    :precondition 
      (and
        (in r544)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r545)
        (f_copy)
        (not 
          (in r544))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r544
    :parameters ()
    :precondition 
      (and
        (in r544)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r543)
        (f_copy)
        (not 
          (in r544))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r545
    :parameters ()
    :precondition 
      (and
        (in r545)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r546)
        (f_copy)
        (not 
          (in r545))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r545
    :parameters ()
    :precondition 
      (and
        (in r545)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r544)
        (f_copy)
        (not 
          (in r545))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r546
    :parameters ()
    :precondition 
      (and
        (in r546)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r547)
        (f_copy)
        (not 
          (in r546))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r546
    :parameters ()
    :precondition 
      (and
        (in r546)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r545)
        (f_copy)
        (not 
          (in r546))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r547
    :parameters ()
    :precondition 
      (and
        (in r547)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r548)
        (f_copy)
        (not 
          (in r547))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r547
    :parameters ()
    :precondition 
      (and
        (in r547)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r546)
        (f_copy)
        (not 
          (in r547))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r548
    :parameters ()
    :precondition 
      (and
        (in r548)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r549)
        (f_copy)
        (not 
          (in r548))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r548
    :parameters ()
    :precondition 
      (and
        (in r548)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r547)
        (f_copy)
        (not 
          (in r548))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r549
    :parameters ()
    :precondition 
      (and
        (in r549)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r550)
        (f_copy)
        (not 
          (in r549))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r549
    :parameters ()
    :precondition 
      (and
        (in r549)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r548)
        (f_copy)
        (not 
          (in r549))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r550
    :parameters ()
    :precondition 
      (and
        (in r550)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r551)
        (f_copy)
        (not 
          (in r550))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r550
    :parameters ()
    :precondition 
      (and
        (in r550)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r549)
        (f_copy)
        (not 
          (in r550))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r551
    :parameters ()
    :precondition 
      (and
        (in r551)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r552)
        (f_copy)
        (not 
          (in r551))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r551
    :parameters ()
    :precondition 
      (and
        (in r551)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r550)
        (f_copy)
        (not 
          (in r551))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r552
    :parameters ()
    :precondition 
      (and
        (in r552)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r553)
        (f_copy)
        (not 
          (in r552))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r552
    :parameters ()
    :precondition 
      (and
        (in r552)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r551)
        (f_copy)
        (not 
          (in r552))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r553
    :parameters ()
    :precondition 
      (and
        (in r553)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r554)
        (f_copy)
        (not 
          (in r553))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r553
    :parameters ()
    :precondition 
      (and
        (in r553)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r552)
        (f_copy)
        (not 
          (in r553))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r554
    :parameters ()
    :precondition 
      (and
        (in r554)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r555)
        (f_copy)
        (not 
          (in r554))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r554
    :parameters ()
    :precondition 
      (and
        (in r554)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r553)
        (f_copy)
        (not 
          (in r554))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r555
    :parameters ()
    :precondition 
      (and
        (in r555)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r556)
        (f_copy)
        (not 
          (in r555))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r555
    :parameters ()
    :precondition 
      (and
        (in r555)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r554)
        (f_copy)
        (not 
          (in r555))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r556
    :parameters ()
    :precondition 
      (and
        (in r556)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r557)
        (f_copy)
        (not 
          (in r556))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r556
    :parameters ()
    :precondition 
      (and
        (in r556)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r555)
        (f_copy)
        (not 
          (in r556))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r557
    :parameters ()
    :precondition 
      (and
        (in r557)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r558)
        (f_copy)
        (not 
          (in r557))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r557
    :parameters ()
    :precondition 
      (and
        (in r557)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r556)
        (f_copy)
        (not 
          (in r557))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r558
    :parameters ()
    :precondition 
      (and
        (in r558)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r559)
        (f_copy)
        (not 
          (in r558))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r558
    :parameters ()
    :precondition 
      (and
        (in r558)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r557)
        (f_copy)
        (not 
          (in r558))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r559
    :parameters ()
    :precondition 
      (and
        (in r559)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r560)
        (f_copy)
        (not 
          (in r559))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r559
    :parameters ()
    :precondition 
      (and
        (in r559)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r558)
        (f_copy)
        (not 
          (in r559))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r560
    :parameters ()
    :precondition 
      (and
        (in r560)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r561)
        (f_copy)
        (not 
          (in r560))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r560
    :parameters ()
    :precondition 
      (and
        (in r560)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r559)
        (f_copy)
        (not 
          (in r560))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r561
    :parameters ()
    :precondition 
      (and
        (in r561)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r562)
        (f_copy)
        (not 
          (in r561))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r561
    :parameters ()
    :precondition 
      (and
        (in r561)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r560)
        (f_copy)
        (not 
          (in r561))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r562
    :parameters ()
    :precondition 
      (and
        (in r562)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r563)
        (f_copy)
        (not 
          (in r562))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r562
    :parameters ()
    :precondition 
      (and
        (in r562)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r561)
        (f_copy)
        (not 
          (in r562))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r563
    :parameters ()
    :precondition 
      (and
        (in r563)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r564)
        (f_copy)
        (not 
          (in r563))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r563
    :parameters ()
    :precondition 
      (and
        (in r563)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r562)
        (f_copy)
        (not 
          (in r563))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r564
    :parameters ()
    :precondition 
      (and
        (in r564)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r565)
        (f_copy)
        (not 
          (in r564))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r564
    :parameters ()
    :precondition 
      (and
        (in r564)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r563)
        (f_copy)
        (not 
          (in r564))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r565
    :parameters ()
    :precondition 
      (and
        (in r565)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r566)
        (f_copy)
        (not 
          (in r565))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r565
    :parameters ()
    :precondition 
      (and
        (in r565)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r564)
        (f_copy)
        (not 
          (in r565))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r566
    :parameters ()
    :precondition 
      (and
        (in r566)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r567)
        (f_copy)
        (not 
          (in r566))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r566
    :parameters ()
    :precondition 
      (and
        (in r566)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r565)
        (f_copy)
        (not 
          (in r566))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r567
    :parameters ()
    :precondition 
      (and
        (in r567)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r568)
        (f_copy)
        (not 
          (in r567))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r567
    :parameters ()
    :precondition 
      (and
        (in r567)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r566)
        (f_copy)
        (not 
          (in r567))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r568
    :parameters ()
    :precondition 
      (and
        (in r568)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r569)
        (f_copy)
        (not 
          (in r568))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r568
    :parameters ()
    :precondition 
      (and
        (in r568)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r567)
        (f_copy)
        (not 
          (in r568))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r569
    :parameters ()
    :precondition 
      (and
        (in r569)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r570)
        (f_copy)
        (not 
          (in r569))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r569
    :parameters ()
    :precondition 
      (and
        (in r569)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r568)
        (f_copy)
        (not 
          (in r569))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r570
    :parameters ()
    :precondition 
      (and
        (in r570)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r571)
        (f_copy)
        (not 
          (in r570))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r570
    :parameters ()
    :precondition 
      (and
        (in r570)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r569)
        (f_copy)
        (not 
          (in r570))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r571
    :parameters ()
    :precondition 
      (and
        (in r571)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r572)
        (f_copy)
        (not 
          (in r571))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r571
    :parameters ()
    :precondition 
      (and
        (in r571)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r570)
        (f_copy)
        (not 
          (in r571))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r572
    :parameters ()
    :precondition 
      (and
        (in r572)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r573)
        (f_copy)
        (not 
          (in r572))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r572
    :parameters ()
    :precondition 
      (and
        (in r572)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r571)
        (f_copy)
        (not 
          (in r572))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r573
    :parameters ()
    :precondition 
      (and
        (in r573)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r574)
        (f_copy)
        (not 
          (in r573))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r573
    :parameters ()
    :precondition 
      (and
        (in r573)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r572)
        (f_copy)
        (not 
          (in r573))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r574
    :parameters ()
    :precondition 
      (and
        (in r574)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r575)
        (f_copy)
        (not 
          (in r574))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r574
    :parameters ()
    :precondition 
      (and
        (in r574)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r573)
        (f_copy)
        (not 
          (in r574))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r575
    :parameters ()
    :precondition 
      (and
        (in r575)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r576)
        (f_copy)
        (not 
          (in r575))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r575
    :parameters ()
    :precondition 
      (and
        (in r575)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r574)
        (f_copy)
        (not 
          (in r575))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r576
    :parameters ()
    :precondition 
      (and
        (in r576)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r577)
        (f_copy)
        (not 
          (in r576))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r576
    :parameters ()
    :precondition 
      (and
        (in r576)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r575)
        (f_copy)
        (not 
          (in r576))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r577
    :parameters ()
    :precondition 
      (and
        (in r577)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r578)
        (f_copy)
        (not 
          (in r577))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r577
    :parameters ()
    :precondition 
      (and
        (in r577)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r576)
        (f_copy)
        (not 
          (in r577))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r578
    :parameters ()
    :precondition 
      (and
        (in r578)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r579)
        (f_copy)
        (not 
          (in r578))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r578
    :parameters ()
    :precondition 
      (and
        (in r578)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r577)
        (f_copy)
        (not 
          (in r578))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r579
    :parameters ()
    :precondition 
      (and
        (in r579)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r580)
        (f_copy)
        (not 
          (in r579))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r579
    :parameters ()
    :precondition 
      (and
        (in r579)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r578)
        (f_copy)
        (not 
          (in r579))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r580
    :parameters ()
    :precondition 
      (and
        (in r580)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r581)
        (f_copy)
        (not 
          (in r580))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r580
    :parameters ()
    :precondition 
      (and
        (in r580)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r579)
        (f_copy)
        (not 
          (in r580))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r581
    :parameters ()
    :precondition 
      (and
        (in r581)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r582)
        (f_copy)
        (not 
          (in r581))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r581
    :parameters ()
    :precondition 
      (and
        (in r581)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r580)
        (f_copy)
        (not 
          (in r581))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r582
    :parameters ()
    :precondition 
      (and
        (in r582)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r583)
        (f_copy)
        (not 
          (in r582))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r582
    :parameters ()
    :precondition 
      (and
        (in r582)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r581)
        (f_copy)
        (not 
          (in r582))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r583
    :parameters ()
    :precondition 
      (and
        (in r583)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r584)
        (f_copy)
        (not 
          (in r583))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r583
    :parameters ()
    :precondition 
      (and
        (in r583)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r582)
        (f_copy)
        (not 
          (in r583))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r584
    :parameters ()
    :precondition 
      (and
        (in r584)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r585)
        (f_copy)
        (not 
          (in r584))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r584
    :parameters ()
    :precondition 
      (and
        (in r584)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r583)
        (f_copy)
        (not 
          (in r584))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r585
    :parameters ()
    :precondition 
      (and
        (in r585)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r586)
        (f_copy)
        (not 
          (in r585))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r585
    :parameters ()
    :precondition 
      (and
        (in r585)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r584)
        (f_copy)
        (not 
          (in r585))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r586
    :parameters ()
    :precondition 
      (and
        (in r586)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r587)
        (f_copy)
        (not 
          (in r586))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r586
    :parameters ()
    :precondition 
      (and
        (in r586)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r585)
        (f_copy)
        (not 
          (in r586))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r587
    :parameters ()
    :precondition 
      (and
        (in r587)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r588)
        (f_copy)
        (not 
          (in r587))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r587
    :parameters ()
    :precondition 
      (and
        (in r587)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r586)
        (f_copy)
        (not 
          (in r587))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r588
    :parameters ()
    :precondition 
      (and
        (in r588)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r589)
        (f_copy)
        (not 
          (in r588))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r588
    :parameters ()
    :precondition 
      (and
        (in r588)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r587)
        (f_copy)
        (not 
          (in r588))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r589
    :parameters ()
    :precondition 
      (and
        (in r589)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r590)
        (f_copy)
        (not 
          (in r589))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r589
    :parameters ()
    :precondition 
      (and
        (in r589)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r588)
        (f_copy)
        (not 
          (in r589))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r590
    :parameters ()
    :precondition 
      (and
        (in r590)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r591)
        (f_copy)
        (not 
          (in r590))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r590
    :parameters ()
    :precondition 
      (and
        (in r590)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r589)
        (f_copy)
        (not 
          (in r590))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r591
    :parameters ()
    :precondition 
      (and
        (in r591)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r592)
        (f_copy)
        (not 
          (in r591))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r591
    :parameters ()
    :precondition 
      (and
        (in r591)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r590)
        (f_copy)
        (not 
          (in r591))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r592
    :parameters ()
    :precondition 
      (and
        (in r592)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r593)
        (f_copy)
        (not 
          (in r592))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r592
    :parameters ()
    :precondition 
      (and
        (in r592)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r591)
        (f_copy)
        (not 
          (in r592))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r593
    :parameters ()
    :precondition 
      (and
        (in r593)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r594)
        (f_copy)
        (not 
          (in r593))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r593
    :parameters ()
    :precondition 
      (and
        (in r593)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r592)
        (f_copy)
        (not 
          (in r593))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r594
    :parameters ()
    :precondition 
      (and
        (in r594)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r595)
        (f_copy)
        (not 
          (in r594))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r594
    :parameters ()
    :precondition 
      (and
        (in r594)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r593)
        (f_copy)
        (not 
          (in r594))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r595
    :parameters ()
    :precondition 
      (and
        (in r595)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r596)
        (f_copy)
        (not 
          (in r595))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r595
    :parameters ()
    :precondition 
      (and
        (in r595)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r594)
        (f_copy)
        (not 
          (in r595))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r596
    :parameters ()
    :precondition 
      (and
        (in r596)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r597)
        (f_copy)
        (not 
          (in r596))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r596
    :parameters ()
    :precondition 
      (and
        (in r596)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r595)
        (f_copy)
        (not 
          (in r596))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r597
    :parameters ()
    :precondition 
      (and
        (in r597)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r598)
        (f_copy)
        (not 
          (in r597))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r597
    :parameters ()
    :precondition 
      (and
        (in r597)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r596)
        (f_copy)
        (not 
          (in r597))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r598
    :parameters ()
    :precondition 
      (and
        (in r598)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r599)
        (f_copy)
        (not 
          (in r598))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r598
    :parameters ()
    :precondition 
      (and
        (in r598)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r597)
        (f_copy)
        (not 
          (in r598))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r599
    :parameters ()
    :precondition 
      (and
        (in r599)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r600)
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (in r599))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r599
    :parameters ()
    :precondition 
      (and
        (in r599)
        (f_ok)
        (f_world))
    :effect
      (and
        (in r598)
        (f_copy)
        (not 
          (in r599))
        (not 
          (f_world))
      )
    )
  (:action move-right-from-r600
    :parameters ()
    :precondition 
      (and
        (in r600)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r1)
        (f_copy)
        (not 
          (in r600))
        (not 
          (f_world))
      )
    )
  (:action move-left-from-r600
    :parameters ()
    :precondition 
      (and
        (in r600)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (in r599)
        (f_copy)
        (not 
          (in r600))
        (not 
          (f_world))
      )
    )
  (:action stay
    :parameters ()
    :precondition 
      (and
        (not 
          (in r300))
        (not 
          (in r600))
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r300-seen
    :parameters ()
    :precondition 
      (and
        (in r300)
        (seen)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r300-not-seen
    :parameters ()
    :precondition 
      (and
        (in r300)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r600-seen
    :parameters ()
    :precondition 
      (and
        (in r600)
        (seen)
        (f_ok)
        (f_world))
    :effect
      (and
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action stay-in-r600-not-seen
    :parameters ()
    :precondition 
      (and
        (in r600)
        (not 
          (seen))
        (f_ok)
        (f_world))
    :effect
      (and
        (oneof
          (not 
            (seen))
          (seen))
        (f_copy)
        (not 
          (f_world))
      )
    )
  (:action o_copy
    :parameters ()
    :precondition 
      (and
        (f_ok)
        (f_copy))
    :effect
      (and
        (f_sync)
        (q_1t)
        (when
          (q_1)
          (q_1s))
        (when
          (q_2)
          (q_2s))
        (when
          (q_3)
          (q_3s))
        (when
          (q_4)
          (q_4s))
        (not 
          (f_copy))
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3))
        (not 
          (q_4))
      )
    )
  (:action o_goal
    :parameters ()
    :precondition 
      (and
        (f_world)
        (f_ok)
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3)))
    :effect
(f_goal)    )
  (:action o_sync_q_1s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_1t))
    :effect
      (and
        (when
          (q_1s)
          (q_2s))
        (q_2t)
        (when
          (q_1s)
          (not 
            (q_1s)))
        (not 
          (q_1t))
      )
    )
  (:action o_sync_q_1s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_1t))
    :effect
      (and
        (when
          (q_1s)
          (q_3))
        (when
          (q_1s)
          (q_1))
        (q_2t)
        (when
          (q_1s)
          (not 
            (q_1s)))
        (not 
          (q_1t))
      )
    )
  (:action o_sync_q_2s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_2t))
    :effect
      (and
        (q_3t)
        (when
          (q_2s)
          (not 
            (q_2s)))
        (when
          (and
            (q_2s)
            (not 
              (seen)))
          (not 
            (f_ok)))
        (not 
          (q_2t))
      )
    )
  (:action o_sync_q_3s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_3t))
    :effect
      (and
        (q_4t)
        (when
          (q_3s)
          (not 
            (q_3s)))
        (not 
          (q_3t))
      )
    )
  (:action o_sync_q_4s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_4t))
    :effect
      (and
        (f_world)
        (when
          (q_1s)
          (q_1))
        (when
          (q_2s)
          (q_2))
        (when
          (q_3s)
          (q_3))
        (when
          (q_4s)
          (q_4))
        (when
          (q_4s)
          (not 
            (f_ok)))
        (not 
          (q_4t))
        (not 
          (f_sync))
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
      )
    )
)