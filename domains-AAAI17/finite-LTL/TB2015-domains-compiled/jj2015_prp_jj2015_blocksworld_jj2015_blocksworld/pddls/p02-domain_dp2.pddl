(define (problem 'p02-domain_dp2')

  (:domain domain-p02_dp2)
  (:objects a b c d table)
  (:init 
    (block a)
    (block b)
    (block c)
    (block table)
    (block d)
    (on c a)
    (on a table)
    (on b table)
    (on d c)
    (clear d)
    (clear b)
    (clear table)
    (q_1)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)