(define (problem 'p10-domain-strips_dp2')

  (:domain domain-strips-p10_dp2)
  (:objects city1 city1_1 city1_2 city2 city2_1 city2_2 city3 city3_1 city3_2 city4 city4_1 city4_2 city5 city5_1 city5_2 city6 city6_1 city6_2 package1 package2 package3 package4 package5 package6 plane1 plane2 truck1 truck2 truck3 truck4 truck5 truck6)
  (:init 
    (obj package6)
    (obj package5)
    (obj package4)
    (obj package3)
    (obj package2)
    (obj package1)
    (city city6)
    (city city5)
    (city city4)
    (city city3)
    (city city2)
    (city city1)
    (truck truck6)
    (truck truck5)
    (truck truck4)
    (truck truck3)
    (truck truck2)
    (truck truck1)
    (airplane plane1)
    (airplane plane2)
    (location city6_1)
    (location city5_1)
    (location city4_1)
    (location city3_1)
    (location city2_1)
    (location city1_1)
    (location city6_2)
    (location city5_2)
    (location city4_2)
    (location city3_2)
    (location city2_2)
    (location city1_2)
    (airport city6_2)
    (airport city5_2)
    (airport city4_2)
    (airport city3_2)
    (airport city2_2)
    (airport city1_2)
    (in_city city6_2 city6)
    (in_city city6_1 city6)
    (in_city city5_2 city5)
    (in_city city5_1 city5)
    (in_city city4_2 city4)
    (in_city city4_1 city4)
    (in_city city3_2 city3)
    (in_city city3_1 city3)
    (in_city city2_2 city2)
    (in_city city2_1 city2)
    (in_city city1_2 city1)
    (in_city city1_1 city1)
    (at plane2 city4_2)
    (at plane1 city4_2)
    (at truck6 city6_1)
    (at truck5 city5_1)
    (at truck4 city4_1)
    (at truck3 city3_1)
    (at truck2 city2_1)
    (at truck1 city1_1)
    (at package6 city3_1)
    (at package5 city4_2)
    (at package4 city1_1)
    (at package3 city1_1)
    (at package2 city1_2)
    (at package1 city2_1)
    (q_1)
    (f_copy)
    (f_ok)
  )
  (:goal (f_goal))

)