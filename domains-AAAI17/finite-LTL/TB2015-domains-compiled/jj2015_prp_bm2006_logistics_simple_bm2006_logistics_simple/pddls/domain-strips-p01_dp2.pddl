(define (domain domain-strips-p01_dp2)
  (:predicates
    (package ?x0)
    (truck ?x0)
    (airplane ?x0)
    (airport ?x0)
    (location ?x0)
    (in-city ?x0 ?x1)
    (city ?x0)
    (at ?x0 ?x1)
    (in ?x0 ?x1)
    (f_copy)
    (f_sync)
    (f_world)
    (f_ok)
    (f_goal)
    (q_1)
    (q_1s)
    (q_1t)
    (q_2)
    (q_2s)
    (q_2t)
    (q_3)
    (q_3s)
    (q_3t)
    (q_4)
    (q_4s)
    (q_4t)
    (q_5)
    (q_5s)
    (q_5t)
  )
  (:action load-truck
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (truck ?x1)
        (location ?x2)
        (at ?x1 ?x2)
        (at ?x0 ?x2)
        (f_ok)
        (f_world))
    :effect
      (and
        (in ?x0 ?x1)
        (f_copy)
        (not 
          (at ?x0 ?x2))
        (not 
          (f_world))
      )
    )
  (:action load-airplane
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (airplane ?x1)
        (location ?x2)
        (at ?x0 ?x2)
        (at ?x1 ?x2)
        (f_ok)
        (f_world))
    :effect
      (and
        (in ?x0 ?x1)
        (f_copy)
        (not 
          (at ?x0 ?x2))
        (not 
          (f_world))
      )
    )
  (:action unload-truck
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (truck ?x1)
        (location ?x2)
        (at ?x1 ?x2)
        (in ?x0 ?x1)
        (f_ok)
        (f_world))
    :effect
      (and
        (at ?x0 ?x2)
        (f_copy)
        (not 
          (in ?x0 ?x1))
        (not 
          (f_world))
      )
    )
  (:action unload-airplane
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (airplane ?x1)
        (location ?x2)
        (in ?x0 ?x1)
        (at ?x1 ?x2)
        (f_ok)
        (f_world))
    :effect
      (and
        (at ?x0 ?x2)
        (f_copy)
        (not 
          (in ?x0 ?x1))
        (not 
          (f_world))
      )
    )
  (:action drive-truck
    :parameters (?x0 ?x1 ?x2 ?x3)
    :precondition 
      (and
        (truck ?x0)
        (location ?x1)
        (location ?x2)
        (city ?x3)
        (at ?x0 ?x1)
        (in-city ?x1 ?x3)
        (in-city ?x2 ?x3)
        (f_ok)
        (f_world))
    :effect
      (and
        (at ?x0 ?x2)
        (f_copy)
        (not 
          (at ?x0 ?x1))
        (not 
          (f_world))
      )
    )
  (:action fly-airplane
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (airplane ?x0)
        (airport ?x1)
        (airport ?x2)
        (at ?x0 ?x1)
        (f_ok)
        (f_world))
    :effect
      (and
        (at ?x0 ?x2)
        (f_copy)
        (not 
          (at ?x0 ?x1))
        (not 
          (f_world))
      )
    )
  (:action o_copy
    :parameters ()
    :precondition 
      (and
        (f_ok)
        (f_copy))
    :effect
      (and
        (f_sync)
        (q_1t)
        (when
          (q_1)
          (q_1s))
        (when
          (q_2)
          (q_2s))
        (when
          (q_3)
          (q_3s))
        (when
          (q_4)
          (q_4s))
        (when
          (q_5)
          (q_5s))
        (not 
          (f_copy))
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3))
        (not 
          (q_4))
        (not 
          (q_5))
      )
    )
  (:action o_goal
    :parameters ()
    :precondition 
      (and
        (f_world)
        (f_ok)
        (not 
          (q_1))
        (not 
          (q_2))
        (not 
          (q_3))
        (not 
          (q_4)))
    :effect
(f_goal)    )
  (:action o_sync_q_1s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_1t))
    :effect
      (and
        (when
          (q_1s)
          (q_2s))
        (q_2t)
        (when
          (q_1s)
          (not 
            (q_1s)))
        (not 
          (q_1t))
      )
    )
  (:action o_sync_q_1s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_1t))
    :effect
      (and
        (when
          (q_1s)
          (q_4))
        (when
          (q_1s)
          (q_1))
        (q_2t)
        (when
          (q_1s)
          (not 
            (q_1s)))
        (not 
          (q_1t))
      )
    )
  (:action o_sync_q_2s_1
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_2t))
    :effect
      (and
        (when
          (q_2s)
          (q_3s))
        (when
          (q_2s)
          (q_5))
        (q_3t)
        (when
          (q_2s)
          (not 
            (q_2s)))
        (not 
          (q_2t))
      )
    )
  (:action o_sync_q_2s_2
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_2t))
    :effect
      (and
        (when
          (q_2s)
          (q_3s))
        (when
          (q_2s)
          (q_2))
        (when
          (q_2s)
          (q_4))
        (q_3t)
        (when
          (q_2s)
          (not 
            (q_2s)))
        (not 
          (q_2t))
      )
    )
  (:action o_sync_q_3s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_3t))
    :effect
      (and
        (q_4t)
        (when
          (q_3s)
          (not 
            (q_3s)))
        (when
          (and
            (q_3s)
            (not 
              (at package6 city2_2)))
          (not 
            (f_ok)))
        (not 
          (q_3t))
      )
    )
  (:action o_sync_q_4s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_4t))
    :effect
      (and
        (q_5t)
        (when
          (q_4s)
          (not 
            (q_4s)))
        (not 
          (q_4t))
      )
    )
  (:action o_sync_q_5s
    :parameters ()
    :precondition 
      (and
        (f_sync)
        (f_ok)
        (q_5t))
    :effect
      (and
        (f_world)
        (when
          (q_1s)
          (q_1))
        (when
          (q_2s)
          (q_2))
        (when
          (q_3s)
          (q_3))
        (when
          (q_4s)
          (q_4))
        (when
          (q_5s)
          (q_5))
        (when
          (q_5s)
          (not 
            (f_ok)))
        (not 
          (q_5t))
        (not 
          (f_sync))
        (not 
          (q_1s))
        (not 
          (q_2s))
        (not 
          (q_3s))
        (not 
          (q_4s))
        (not 
          (q_5s))
      )
    )
)