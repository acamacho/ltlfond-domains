(define (domain logistics)
  (:requirements :strips :adl)


  (:predicates
    (obj ?x0)
    (truck ?x0)
    (location ?x0)
    (airplane ?x0)
    (city ?x0)
    (airport ?x0)
    (at ?x0 ?x1)
    (in ?x0 ?x1)
    (in_city ?x0 ?x1)
  )
  (:action load
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (obj ?x0)
        (or
          (airplane ?x1)
          (truck ?x1))
        (at ?x0 ?x2)
        (at ?x1 ?x2))
    :effect
      (and
        (in ?x0 ?x1)
        (not 
          (at ?x0 ?x2))
      )
    )
  (:action unload
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (obj ?x0)
        (or
          (airplane ?x1)
          (truck ?x1))
        (location ?x2)
        (in ?x0 ?x1)
        (at ?x1 ?x2))
    :effect
      (and
        (at ?x0 ?x2)
        (not 
          (in ?x0 ?x1))
      )
    )
  (:action drive_truck
    :parameters (?x0 ?x1 ?x2 ?x3)
    :precondition 
      (and
        (truck ?x0)
        (location ?x1)
        (location ?x2)
        (city ?x3)
        (at ?x0 ?x1)
        (in_city ?x1 ?x3)
        (in_city ?x2 ?x3))
    :effect
      (and
        (at ?x0 ?x2)
        (not 
          (at ?x0 ?x1))
      )
    )
  (:action fly_airplane
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (airplane ?x0)
        (airport ?x1)
        (airport ?x2)
        (at ?x0 ?x1))
    :effect
      (and
        (at ?x0 ?x2)
        (not 
          (at ?x0 ?x1))
      )
    )
)
