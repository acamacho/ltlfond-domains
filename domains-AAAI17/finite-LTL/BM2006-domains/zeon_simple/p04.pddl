(define (problem p04)

  (:domain zeon)
   (:objects plane1 plane2 person1 person2 person3 city0 city1 city2 city3 city4 fl0 fl6)
  (:init 
    (aircraft plane1)
    (aircraft plane2)
    ;(aircraft plane3)
    (person person1)
    (person person2)
    (person person3)

    ;(person person4)
    ;(person person5)
    ;(person person6)

    (city city0)
    (city city1)
    (city city2)
    (city city3)
    (city city4)

    (flevel fl0)
    ;(flevel fl1)
    ;(flevel fl2)
    ;(flevel fl3)
    ;(flevel fl4)
    ;(flevel fl5)
    (flevel fl6)

    (at plane1 city0)
    (at plane2 city3)
    ;(at plane3 city0)

    (at person1 city1)
    (at person2 city0)
    (at person3 city2)

    ;(at person4 city0)
    ;(at person5 city3)
    ;(at person6 city4)

    (fuel_level plane1 fl6)
    ;(fuel_level plane3 fl3)
    (fuel_level plane2 fl0)

    ;(next_to fl0 fl1)
    ;(next_to fl1 fl2)
    ;(next_to fl2 fl3)
    ;(next_to fl3 fl4)
    ;(next_to fl4 fl5)
    ;(next_to fl5 fl6)

    (next_to fl0 fl6)

  )


  (:goal (eventually (and (at person1 city2) (next (eventually (and (at person1 city3) (next (eventually (and (at person1 city4) (next (eventually (at person1 city0))))))))))))

  
)