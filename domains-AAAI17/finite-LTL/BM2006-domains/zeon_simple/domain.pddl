(define (domain zeon)
  (:predicates
    (aircraft ?x0)
    (person ?x0)
    (city ?x0)
    (flevel ?x0)
    (at ?x0 ?x1)
    (in ?x0 ?x1)
    (fuel_level ?x0 ?x1)
    (next_to ?x0 ?x1)
  )
  (:action board
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (person ?x0)
        (aircraft ?x1)
        (city ?x2)
        (at ?x0 ?x2)
        (at ?x1 ?x2))
    :effect
      (and
        (in ?x0 ?x1)
        (not 
          (at ?x0 ?x2))
      )
    )
  (:action debark
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (person ?x0)
        (aircraft ?x1)
        (city ?x2)
        (in ?x0 ?x1)
        (at ?x1 ?x2))
    :effect
      (and
        (at ?x0 ?x2)
        (not 
          (in ?x0 ?x1))
      )
    )
  (:action fly
    :parameters (?x0 ?x1 ?x2 ?x3 ?x4)
    :precondition 
      (and
        (aircraft ?x0)
        (city ?x2)
        (at ?x0 ?x1)
        (fuel_level ?x0 ?x3)
        (next_to ?x4 ?x3))
    :effect
      (and
        (at ?x0 ?x2)
        (fuel_level ?x0 ?x4)
        (not 
          (fuel_level ?x0 ?x3))
        (not 
          (at ?x0 ?x1))
      )
    )
  (:action refuel
    :parameters (?x0 ?x1 ?x2 ?x3)
    :precondition 
      (and
        (aircraft ?x0)
        (city ?x1)
        (fuel_level ?x0 ?x2)
        (next_to ?x2 ?x3))
    :effect
      (and
        (fuel_level ?x0 ?x3)
        (not 
          (fuel_level ?x0 ?x2))
      )
    )
)
