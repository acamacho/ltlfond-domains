(define (domain dom)
  (:types
    rover - no_type
    lander - no_type
    camera - no_type
    waypoint - no_type
    objective - no_type
    store - no_type
    mode - no_type
  )

  (:predicates
    (at ?x0 - rover ?x1 - waypoint)
    (at_lander ?x0 - lander ?x1 - waypoint)
    (can_traverse ?x0 - rover ?x1 - waypoint ?x2 - waypoint)
    (equipped_for_soil_analysis ?x0 - rover)
    (equipped_for_rock_analysis ?x0 - rover)
    (equipped_for_imaging ?x0 - rover)
    (empty ?x0 - store)
    (have_rock_analysis ?x0 - rover ?x1 - waypoint)
    (have_soil_analysis ?x0 - rover ?x1 - waypoint)
    (full ?x0 - store)
    (calibrated ?x0 - camera ?x1 - rover)
    (supports ?x0 - camera ?x1 - mode)
    (available ?x0 - rover)
    (visible ?x0 - waypoint ?x1 - waypoint)
    (have_image ?x0 - rover ?x1 - objective ?x2 - mode)
    (communicated_soil_data ?x0 - waypoint)
    (communicated_rock_data ?x0 - waypoint)
    (communicated_image_data ?x0 - objective ?x1 - mode)
    (at_soil_sample ?x0 - waypoint)
    (at_rock_sample ?x0 - waypoint)
    (visible_from ?x0 - objective ?x1 - waypoint)
    (store_of ?x0 - store ?x1 - rover)
    (calibration_target ?x0 - camera ?x1 - objective)
    (on_board ?x0 - camera ?x1 - rover)
    (channel_free ?x0 - lander)
    (autstate_1_2)
    (autstate_1_3)
    (autstate_1_4)
    (autstate_1_5)
    (autstate_1_6)
    (autstate_1_7)
    (autstate_1_8)
    (autstate_1_9)
    (autstate_1_10)
    (autstate_1_11)
    (autstate_1_12)
    (autstate_1_13)
    (autstate_1_14)
    (autstate_1_15)
    (autstate_1_16)
    (autstate_1_1)
  )
  (:action navigate
    :parameters (?x0 - rover ?x1 - waypoint ?x2 - waypoint)
    :precondition 
      (and
        (can_traverse ?x0 ?x1 ?x2)
        (available ?x0)
        (at ?x0 ?x1)
        (visible ?x1 ?x2))
    :effect
      (and
        (at ?x0 ?x2)
        (when
          (and
            (autstate_1_2)
            (communicated_image_data objective1 high_res))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (communicated_rock_data waypoint3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_rock_data waypoint3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_7)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint0))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (communicated_soil_data waypoint2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (communicated_rock_data waypoint3)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_15)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_13)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (communicated_image_data objective1 high_res))
                (and
                  (autstate_1_7)
                  (communicated_rock_data waypoint3))
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (communicated_image_data objective1 high_res))
                    (and
                      (communicated_rock_data waypoint3)
                      (or
                        (autstate_1_3)
                        (and
                          (autstate_1_2)
                          (communicated_image_data objective1 high_res)))))))))
          (autstate_1_1))
        (not 
          (at ?x0 ?x1))
      )
    )
  (:action sample_soil
    :parameters (?x0 - rover ?x1 - store ?x2 - waypoint)
    :precondition 
      (and
        (at ?x0 ?x2)
        (at_soil_sample ?x2)
        (equipped_for_soil_analysis ?x0)
        (store_of ?x1 ?x0)
        (empty ?x1))
    :effect
      (and
        (full ?x1)
        (have_soil_analysis ?x0 ?x2)
        (when
          (and
            (autstate_1_2)
            (communicated_image_data objective1 high_res))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (communicated_rock_data waypoint3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_rock_data waypoint3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_7)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint0))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (communicated_soil_data waypoint2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (communicated_rock_data waypoint3)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_15)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_13)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (communicated_image_data objective1 high_res))
                (and
                  (autstate_1_7)
                  (communicated_rock_data waypoint3))
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (communicated_image_data objective1 high_res))
                    (and
                      (communicated_rock_data waypoint3)
                      (or
                        (autstate_1_3)
                        (and
                          (autstate_1_2)
                          (communicated_image_data objective1 high_res)))))))))
          (autstate_1_1))
        (not 
          (empty ?x1))
        (not 
          (at_soil_sample ?x2))
      )
    )
  (:action sample_rock
    :parameters (?x0 - rover ?x1 - store ?x2 - waypoint)
    :precondition 
      (and
        (at ?x0 ?x2)
        (at_rock_sample ?x2)
        (equipped_for_rock_analysis ?x0)
        (store_of ?x1 ?x0)
        (empty ?x1))
    :effect
      (and
        (full ?x1)
        (have_rock_analysis ?x0 ?x2)
        (when
          (and
            (autstate_1_2)
            (communicated_image_data objective1 high_res))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (communicated_rock_data waypoint3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_rock_data waypoint3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_7)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint0))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (communicated_soil_data waypoint2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (communicated_rock_data waypoint3)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_15)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_13)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (communicated_image_data objective1 high_res))
                (and
                  (autstate_1_7)
                  (communicated_rock_data waypoint3))
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (communicated_image_data objective1 high_res))
                    (and
                      (communicated_rock_data waypoint3)
                      (or
                        (autstate_1_3)
                        (and
                          (autstate_1_2)
                          (communicated_image_data objective1 high_res)))))))))
          (autstate_1_1))
        (not 
          (empty ?x1))
        (not 
          (at_rock_sample ?x2))
      )
    )
  (:action drop
    :parameters (?x0 - rover ?x1 - store)
    :precondition 
      (and
        (store_of ?x1 ?x0)
        (full ?x1))
    :effect
      (and
        (empty ?x1)
        (when
          (and
            (autstate_1_2)
            (communicated_image_data objective1 high_res))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (communicated_rock_data waypoint3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_rock_data waypoint3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_7)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint0))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (communicated_soil_data waypoint2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (communicated_rock_data waypoint3)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_15)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_13)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (communicated_image_data objective1 high_res))
                (and
                  (autstate_1_7)
                  (communicated_rock_data waypoint3))
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (communicated_image_data objective1 high_res))
                    (and
                      (communicated_rock_data waypoint3)
                      (or
                        (autstate_1_3)
                        (and
                          (autstate_1_2)
                          (communicated_image_data objective1 high_res)))))))))
          (autstate_1_1))
        (not 
          (full ?x1))
      )
    )
  (:action calibrate
    :parameters (?x0 - rover ?x1 - camera ?x2 - objective ?x3 - waypoint)
    :precondition 
      (and
        (equipped_for_imaging ?x0)
        (calibration_target ?x1 ?x2)
        (at ?x0 ?x3)
        (visible_from ?x2 ?x3)
        (on_board ?x1 ?x0))
    :effect
      (and
        (calibrated ?x1 ?x0)
        (when
          (and
            (autstate_1_2)
            (communicated_image_data objective1 high_res))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (communicated_rock_data waypoint3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_rock_data waypoint3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_7)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint0))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (communicated_soil_data waypoint2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (communicated_rock_data waypoint3)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_15)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_13)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (communicated_image_data objective1 high_res))
                (and
                  (autstate_1_7)
                  (communicated_rock_data waypoint3))
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (communicated_image_data objective1 high_res))
                    (and
                      (communicated_rock_data waypoint3)
                      (or
                        (autstate_1_3)
                        (and
                          (autstate_1_2)
                          (communicated_image_data objective1 high_res)))))))))
          (autstate_1_1))
      )
    )
  (:action take_image
    :parameters (?x0 - rover ?x1 - waypoint ?x2 - objective ?x3 - camera ?x4 - mode)
    :precondition 
      (and
        (calibrated ?x3 ?x0)
        (on_board ?x3 ?x0)
        (equipped_for_imaging ?x0)
        (supports ?x3 ?x4)
        (visible_from ?x2 ?x1)
        (at ?x0 ?x1))
    :effect
      (and
        (have_image ?x0 ?x2 ?x4)
        (when
          (and
            (autstate_1_2)
            (communicated_image_data objective1 high_res))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (communicated_rock_data waypoint3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_rock_data waypoint3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_7)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint0))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (communicated_soil_data waypoint2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (communicated_rock_data waypoint3)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_15)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_13)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (communicated_image_data objective1 high_res))
                (and
                  (autstate_1_7)
                  (communicated_rock_data waypoint3))
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (communicated_image_data objective1 high_res))
                    (and
                      (communicated_rock_data waypoint3)
                      (or
                        (autstate_1_3)
                        (and
                          (autstate_1_2)
                          (communicated_image_data objective1 high_res)))))))))
          (autstate_1_1))
        (not 
          (calibrated ?x3 ?x0))
      )
    )
  (:action communicate_soil_data
    :parameters (?x0 - rover ?x1 - lander ?x2 - waypoint ?x3 - waypoint ?x4 - waypoint)
    :precondition 
      (and
        (at ?x0 ?x3)
        (at_lander ?x1 ?x4)
        (have_soil_analysis ?x0 ?x2)
        (visible ?x3 ?x4)
        (available ?x0)
        (channel_free ?x1))
    :effect
      (and
        (channel_free ?x1)
        (communicated_soil_data ?x2)
        (available ?x0)
        (when
          (and
            (autstate_1_2)
            (communicated_image_data objective1 high_res))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (communicated_rock_data waypoint3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_rock_data waypoint3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x2 waypoint2)
              (communicated_soil_data waypoint2)))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_3)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_2)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_4)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_2)
              (communicated_rock_data waypoint3)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_7)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_5)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_4)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (communicated_rock_data waypoint3)
              (or
                (and
                  (autstate_1_3)
                  (or
                    (= ?x2 waypoint2)
                    (communicated_soil_data waypoint2)))
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)
                  (or
                    (= ?x2 waypoint2)
                    (communicated_soil_data waypoint2))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x2 waypoint0)
              (communicated_soil_data waypoint0)))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_3)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_2)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_4)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_2)
              (communicated_rock_data waypoint3)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_5)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_4)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (communicated_rock_data waypoint3)
              (or
                (and
                  (autstate_1_3)
                  (or
                    (= ?x2 waypoint0)
                    (communicated_soil_data waypoint0)))
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)
                  (or
                    (= ?x2 waypoint0)
                    (communicated_soil_data waypoint0))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_6)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_2)
              (or
                (and
                  (communicated_soil_data waypoint0)
                  (= ?x2 waypoint2))
                (and
                  (communicated_soil_data waypoint2)
                  (= ?x2 waypoint0))
                (and
                  (communicated_soil_data waypoint0)
                  (communicated_soil_data waypoint2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_10)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_7)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_6)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_3)
              (or
                (and
                  (communicated_soil_data waypoint0)
                  (= ?x2 waypoint2))
                (and
                  (communicated_soil_data waypoint2)
                  (= ?x2 waypoint0))
                (and
                  (communicated_soil_data waypoint0)
                  (communicated_soil_data waypoint2))))
            (and
              (autstate_1_2)
              (communicated_image_data objective1 high_res)
              (or
                (and
                  (communicated_soil_data waypoint0)
                  (= ?x2 waypoint2))
                (and
                  (communicated_soil_data waypoint2)
                  (= ?x2 waypoint0))
                (and
                  (communicated_soil_data waypoint0)
                  (communicated_soil_data waypoint2)))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_12)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_8)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_4)
              (or
                (and
                  (communicated_soil_data waypoint0)
                  (= ?x2 waypoint2))
                (and
                  (communicated_soil_data waypoint2)
                  (= ?x2 waypoint0))
                (and
                  (communicated_soil_data waypoint0)
                  (communicated_soil_data waypoint2))))
            (and
              (autstate_1_2)
              (communicated_rock_data waypoint3)
              (or
                (and
                  (communicated_soil_data waypoint0)
                  (= ?x2 waypoint2))
                (and
                  (communicated_soil_data waypoint2)
                  (= ?x2 waypoint0))
                (and
                  (communicated_soil_data waypoint0)
                  (communicated_soil_data waypoint2)))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_15)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_13)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_12)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_11)
              (communicated_rock_data waypoint3)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint2)
                (communicated_soil_data waypoint2)))
            (and
              (autstate_1_9)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_8)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_7)
              (communicated_rock_data waypoint3)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint0)
                (communicated_soil_data waypoint0)))
            (and
              (autstate_1_5)
              (or
                (and
                  (communicated_soil_data waypoint0)
                  (= ?x2 waypoint2))
                (and
                  (communicated_soil_data waypoint2)
                  (= ?x2 waypoint0))
                (and
                  (communicated_soil_data waypoint0)
                  (communicated_soil_data waypoint2))))
            (and
              (autstate_1_4)
              (communicated_image_data objective1 high_res)
              (or
                (and
                  (communicated_soil_data waypoint0)
                  (= ?x2 waypoint2))
                (and
                  (communicated_soil_data waypoint2)
                  (= ?x2 waypoint0))
                (and
                  (communicated_soil_data waypoint0)
                  (communicated_soil_data waypoint2))))
            (and
              (communicated_rock_data waypoint3)
              (or
                (and
                  (autstate_1_3)
                  (or
                    (and
                      (communicated_soil_data waypoint0)
                      (= ?x2 waypoint2))
                    (and
                      (communicated_soil_data waypoint2)
                      (= ?x2 waypoint0))
                    (and
                      (communicated_soil_data waypoint0)
                      (communicated_soil_data waypoint2))))
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)
                  (or
                    (and
                      (communicated_soil_data waypoint0)
                      (= ?x2 waypoint2))
                    (and
                      (communicated_soil_data waypoint2)
                      (= ?x2 waypoint0))
                    (and
                      (communicated_soil_data waypoint0)
                      (communicated_soil_data waypoint2)))))))
          (autstate_1_1))
        (not 
          (available ?x0))
        (not 
          (channel_free ?x1))
      )
    )
  (:action communicate_rock_data
    :parameters (?x0 - rover ?x1 - lander ?x2 - waypoint ?x3 - waypoint ?x4 - waypoint)
    :precondition 
      (and
        (at ?x0 ?x3)
        (at_lander ?x1 ?x4)
        (have_rock_analysis ?x0 ?x2)
        (visible ?x3 ?x4)
        (available ?x0)
        (channel_free ?x1))
    :effect
      (and
        (channel_free ?x1)
        (communicated_rock_data ?x2)
        (available ?x0)
        (when
          (and
            (autstate_1_2)
            (communicated_image_data objective1 high_res))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x2 waypoint3)
              (communicated_rock_data waypoint3)))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_3)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (autstate_1_2)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (or
                    (= ?x2 waypoint3)
                    (communicated_rock_data waypoint3))))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_7)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (autstate_1_6)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (autstate_1_3)
                  (or
                    (= ?x2 waypoint3)
                    (communicated_rock_data waypoint3)))
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)
                  (or
                    (= ?x2 waypoint3)
                    (communicated_rock_data waypoint3))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint0))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (or
                    (= ?x2 waypoint3)
                    (communicated_rock_data waypoint3))))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (autstate_1_10)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (communicated_image_data objective1 high_res))
                (and
                  (autstate_1_3)
                  (or
                    (= ?x2 waypoint3)
                    (communicated_rock_data waypoint3)))
                (and
                  (autstate_1_2)
                  (communicated_image_data objective1 high_res)
                  (or
                    (= ?x2 waypoint3)
                    (communicated_rock_data waypoint3))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (communicated_soil_data waypoint2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (communicated_image_data objective1 high_res))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (or
                    (= ?x2 waypoint3)
                    (communicated_rock_data waypoint3)))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (or
                        (= ?x2 waypoint3)
                        (communicated_rock_data waypoint3))))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_15)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (autstate_1_14)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (autstate_1_13)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_image_data objective1 high_res)
              (or
                (= ?x2 waypoint3)
                (communicated_rock_data waypoint3)))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (communicated_image_data objective1 high_res))
                (and
                  (autstate_1_7)
                  (or
                    (= ?x2 waypoint3)
                    (communicated_rock_data waypoint3)))
                (and
                  (autstate_1_6)
                  (communicated_image_data objective1 high_res)
                  (or
                    (= ?x2 waypoint3)
                    (communicated_rock_data waypoint3)))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (communicated_image_data objective1 high_res))
                    (and
                      (autstate_1_3)
                      (or
                        (= ?x2 waypoint3)
                        (communicated_rock_data waypoint3)))
                    (and
                      (autstate_1_2)
                      (communicated_image_data objective1 high_res)
                      (or
                        (= ?x2 waypoint3)
                        (communicated_rock_data waypoint3))))))))
          (autstate_1_1))
        (not 
          (available ?x0))
        (not 
          (channel_free ?x1))
      )
    )
  (:action communicate_image_data
    :parameters (?x0 - rover ?x1 - lander ?x2 - objective ?x3 - mode ?x4 - waypoint ?x5 - waypoint)
    :precondition 
      (and
        (at ?x0 ?x4)
        (at_lander ?x1 ?x5)
        (have_image ?x0 ?x2 ?x3)
        (visible ?x4 ?x5)
        (available ?x0)
        (channel_free ?x1))
    :effect
      (and
        (channel_free ?x1)
        (communicated_image_data ?x2 ?x3)
        (available ?x0)
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x2 objective1)
                (= ?x3 high_res))
              (communicated_image_data objective1 high_res)))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (communicated_rock_data waypoint3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (communicated_rock_data waypoint3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (or
                    (and
                      (= ?x2 objective1)
                      (= ?x3 high_res))
                    (communicated_image_data objective1 high_res))))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (or
                    (and
                      (= ?x2 objective1)
                      (= ?x3 high_res))
                    (communicated_image_data objective1 high_res))))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (autstate_1_7)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_6)
              (communicated_rock_data waypoint3)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (communicated_soil_data waypoint2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (or
                    (and
                      (= ?x2 objective1)
                      (= ?x3 high_res))
                    (communicated_image_data objective1 high_res)))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (or
                        (and
                          (= ?x2 objective1)
                          (= ?x3 high_res))
                        (communicated_image_data objective1 high_res))))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (communicated_soil_data waypoint0))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (or
                    (and
                      (= ?x2 objective1)
                      (= ?x3 high_res))
                    (communicated_image_data objective1 high_res))))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (communicated_rock_data waypoint3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (autstate_1_11)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_rock_data waypoint3)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (or
                    (and
                      (= ?x2 objective1)
                      (= ?x3 high_res))
                    (communicated_image_data objective1 high_res)))
                (and
                  (communicated_rock_data waypoint3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (or
                        (and
                          (= ?x2 objective1)
                          (= ?x3 high_res))
                        (communicated_image_data objective1 high_res))))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (communicated_soil_data waypoint2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (or
                    (and
                      (= ?x2 objective1)
                      (= ?x3 high_res))
                    (communicated_image_data objective1 high_res)))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (or
                        (and
                          (= ?x2 objective1)
                          (= ?x3 high_res))
                        (communicated_image_data objective1 high_res))))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (communicated_rock_data waypoint3)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (autstate_1_15)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_14)
              (communicated_rock_data waypoint3)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (autstate_1_13)
              (communicated_soil_data waypoint2))
            (and
              (autstate_1_12)
              (communicated_soil_data waypoint2)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (autstate_1_11)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3))
            (and
              (autstate_1_10)
              (communicated_soil_data waypoint2)
              (communicated_rock_data waypoint3)
              (or
                (and
                  (= ?x2 objective1)
                  (= ?x3 high_res))
                (communicated_image_data objective1 high_res)))
            (and
              (communicated_soil_data waypoint0)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (or
                    (and
                      (= ?x2 objective1)
                      (= ?x3 high_res))
                    (communicated_image_data objective1 high_res)))
                (and
                  (autstate_1_7)
                  (communicated_rock_data waypoint3))
                (and
                  (autstate_1_6)
                  (communicated_rock_data waypoint3)
                  (or
                    (and
                      (= ?x2 objective1)
                      (= ?x3 high_res))
                    (communicated_image_data objective1 high_res)))
                (and
                  (communicated_soil_data waypoint2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (or
                        (and
                          (= ?x2 objective1)
                          (= ?x3 high_res))
                        (communicated_image_data objective1 high_res)))
                    (and
                      (communicated_rock_data waypoint3)
                      (or
                        (autstate_1_3)
                        (and
                          (autstate_1_2)
                          (or
                            (and
                              (= ?x2 objective1)
                              (= ?x3 high_res))
                            (communicated_image_data objective1 high_res))))))))))
          (autstate_1_1))
        (not 
          (available ?x0))
        (not 
          (channel_free ?x1))
      )
    )
)