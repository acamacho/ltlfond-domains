(define (domain logistics)
  (:types
    default_object - none
  )

  (:predicates
    (package ?x0)
    (truck ?x0)
    (airplane ?x0)
    (airport ?x0)
    (location ?x0)
    (in-city ?x0 ?x1)
    (city ?x0)
    (at ?x0 ?x1)
    (in ?x0 ?x1)
    (autstate_1_5)
    (autstate_1_2)
    (autstate_1_3)
    (autstate_1_1)
    (autstate_1_4)
    (autstate_1_6)
  )
  (:action load-truck
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (truck ?x1)
        (location ?x2)
        (at ?x1 ?x2)
        (at ?x0 ?x2))
    :effect
      (and
        (in ?x0 ?x1)
        (when
          (autstate_1_6)
          (autstate_1_2))
        (when
          (and
            (autstate_1_2)
            (at package6 city3_2))
          (autstate_1_3))
        (when
          (or
            (and
              (autstate_1_4)
              (at package6 city3_2))
            (and
              (at package6 city1_2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at package6 city3_2)))))
          (autstate_1_1))
        (when
          (and
            (at package6 city1_2)
            (or
              (autstate_1_6)
              (autstate_1_2)))
          (autstate_1_4))
        (when
          (and
            (autstate_1_5)
            (at package6 city2_2))
          (autstate_1_6))
        (not 
          (at ?x0 ?x2))
        (when
          (or
            (not 
              (autstate_1_5))
            (not 
              (at package6 city2_2)))
          (not 
            (autstate_1_6)))
      )
    )
  (:action load-airplane
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (airplane ?x1)
        (location ?x2)
        (at ?x0 ?x2)
        (at ?x1 ?x2))
    :effect
      (and
        (in ?x0 ?x1)
        (when
          (autstate_1_6)
          (autstate_1_2))
        (when
          (and
            (autstate_1_2)
            (at package6 city3_2))
          (autstate_1_3))
        (when
          (or
            (and
              (autstate_1_4)
              (at package6 city3_2))
            (and
              (at package6 city1_2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at package6 city3_2)))))
          (autstate_1_1))
        (when
          (and
            (at package6 city1_2)
            (or
              (autstate_1_6)
              (autstate_1_2)))
          (autstate_1_4))
        (when
          (and
            (autstate_1_5)
            (at package6 city2_2))
          (autstate_1_6))
        (not 
          (at ?x0 ?x2))
        (when
          (or
            (not 
              (autstate_1_5))
            (not 
              (at package6 city2_2)))
          (not 
            (autstate_1_6)))
      )
    )
  (:action unload-truck
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (truck ?x1)
        (location ?x2)
        (at ?x1 ?x2)
        (in ?x0 ?x1))
    :effect
      (and
        (at ?x0 ?x2)
        (when
          (autstate_1_6)
          (autstate_1_2))
        (when
          (and
            (autstate_1_2)
            (at package6 city3_2))
          (autstate_1_3))
        (when
          (or
            (and
              (autstate_1_4)
              (at package6 city3_2))
            (and
              (at package6 city1_2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at package6 city3_2)))))
          (autstate_1_1))
        (when
          (and
            (at package6 city1_2)
            (or
              (autstate_1_6)
              (autstate_1_2)))
          (autstate_1_4))
        (when
          (and
            (autstate_1_5)
            (at package6 city2_2))
          (autstate_1_6))
        (not 
          (in ?x0 ?x1))
        (when
          (or
            (not 
              (autstate_1_5))
            (not 
              (at package6 city2_2)))
          (not 
            (autstate_1_6)))
      )
    )
  (:action unload-airplane
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (airplane ?x1)
        (location ?x2)
        (in ?x0 ?x1)
        (at ?x1 ?x2))
    :effect
      (and
        (at ?x0 ?x2)
        (when
          (autstate_1_6)
          (autstate_1_2))
        (when
          (and
            (autstate_1_2)
            (at package6 city3_2))
          (autstate_1_3))
        (when
          (or
            (and
              (autstate_1_4)
              (at package6 city3_2))
            (and
              (at package6 city1_2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at package6 city3_2)))))
          (autstate_1_1))
        (when
          (and
            (at package6 city1_2)
            (or
              (autstate_1_6)
              (autstate_1_2)))
          (autstate_1_4))
        (when
          (and
            (autstate_1_5)
            (at package6 city2_2))
          (autstate_1_6))
        (not 
          (in ?x0 ?x1))
        (when
          (or
            (not 
              (autstate_1_5))
            (not 
              (at package6 city2_2)))
          (not 
            (autstate_1_6)))
      )
    )
  (:action drive-truck
    :parameters (?x0 ?x1 ?x2 ?x3)
    :precondition 
      (and
        (truck ?x0)
        (location ?x1)
        (location ?x2)
        (city ?x3)
        (at ?x0 ?x1)
        (in-city ?x1 ?x3)
        (in-city ?x2 ?x3))
    :effect
      (and
        (at ?x0 ?x2)
        (when
          (autstate_1_6)
          (autstate_1_2))
        (when
          (and
            (autstate_1_2)
            (at package6 city3_2))
          (autstate_1_3))
        (when
          (or
            (and
              (autstate_1_4)
              (at package6 city3_2))
            (and
              (at package6 city1_2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at package6 city3_2)))))
          (autstate_1_1))
        (when
          (and
            (at package6 city1_2)
            (or
              (autstate_1_6)
              (autstate_1_2)))
          (autstate_1_4))
        (when
          (and
            (autstate_1_5)
            (at package6 city2_2))
          (autstate_1_6))
        (not 
          (at ?x0 ?x1))
        (when
          (or
            (not 
              (autstate_1_5))
            (not 
              (at package6 city2_2)))
          (not 
            (autstate_1_6)))
      )
    )
  (:action fly-airplane
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (airplane ?x0)
        (airport ?x1)
        (airport ?x2)
        (at ?x0 ?x1))
    :effect
      (and
        (at ?x0 ?x2)
        (when
          (autstate_1_6)
          (autstate_1_2))
        (when
          (and
            (autstate_1_2)
            (at package6 city3_2))
          (autstate_1_3))
        (when
          (or
            (and
              (autstate_1_4)
              (at package6 city3_2))
            (and
              (at package6 city1_2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at package6 city3_2)))))
          (autstate_1_1))
        (when
          (and
            (at package6 city1_2)
            (or
              (autstate_1_6)
              (autstate_1_2)))
          (autstate_1_4))
        (when
          (and
            (autstate_1_5)
            (at package6 city2_2))
          (autstate_1_6))
        (not 
          (at ?x0 ?x1))
        (when
          (or
            (not 
              (autstate_1_5))
            (not 
              (at package6 city2_2)))
          (not 
            (autstate_1_6)))
      )
    )
)