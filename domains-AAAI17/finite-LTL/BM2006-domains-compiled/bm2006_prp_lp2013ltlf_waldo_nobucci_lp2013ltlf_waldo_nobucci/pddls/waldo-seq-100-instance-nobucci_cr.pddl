(define (domain waldo)
  (:types
    room - none
  )

  (:predicates
    (seen)
    (in ?x0 - room)
    (autstate_1_2)
    (autstate_1_1)
  )
  (:action move-right-from-r1
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r2)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r1))
      )
    )
  (:action move-left-from-r1_detdup_0
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r100)
        (not 
          (in r1))
        (not 
          (seen))
      )
    )
  (:action move-left-from-r1_detdup_1
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r100)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r1))
      )
    )
  (:action move-right-from-r2
    :parameters ()
    :precondition 
      (in r2)
    :effect
      (and
        (in r3)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r2))
      )
    )
  (:action move-left-from-r2
    :parameters ()
    :precondition 
      (in r2)
    :effect
      (and
        (in r1)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r2))
      )
    )
  (:action move-right-from-r3
    :parameters ()
    :precondition 
      (in r3)
    :effect
      (and
        (in r4)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r3))
      )
    )
  (:action move-left-from-r3
    :parameters ()
    :precondition 
      (in r3)
    :effect
      (and
        (in r2)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r3))
      )
    )
  (:action move-right-from-r4
    :parameters ()
    :precondition 
      (in r4)
    :effect
      (and
        (in r5)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r4))
      )
    )
  (:action move-left-from-r4
    :parameters ()
    :precondition 
      (in r4)
    :effect
      (and
        (in r3)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r4))
      )
    )
  (:action move-right-from-r5
    :parameters ()
    :precondition 
      (in r5)
    :effect
      (and
        (in r6)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r5))
      )
    )
  (:action move-left-from-r5
    :parameters ()
    :precondition 
      (in r5)
    :effect
      (and
        (in r4)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r5))
      )
    )
  (:action move-right-from-r6
    :parameters ()
    :precondition 
      (in r6)
    :effect
      (and
        (in r7)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r6))
      )
    )
  (:action move-left-from-r6
    :parameters ()
    :precondition 
      (in r6)
    :effect
      (and
        (in r5)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r6))
      )
    )
  (:action move-right-from-r7
    :parameters ()
    :precondition 
      (in r7)
    :effect
      (and
        (in r8)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r7))
      )
    )
  (:action move-left-from-r7
    :parameters ()
    :precondition 
      (in r7)
    :effect
      (and
        (in r6)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r7))
      )
    )
  (:action move-right-from-r8
    :parameters ()
    :precondition 
      (in r8)
    :effect
      (and
        (in r9)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r8))
      )
    )
  (:action move-left-from-r8
    :parameters ()
    :precondition 
      (in r8)
    :effect
      (and
        (in r7)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r8))
      )
    )
  (:action move-right-from-r9
    :parameters ()
    :precondition 
      (in r9)
    :effect
      (and
        (in r10)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r9))
      )
    )
  (:action move-left-from-r9
    :parameters ()
    :precondition 
      (in r9)
    :effect
      (and
        (in r8)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r9))
      )
    )
  (:action move-right-from-r10
    :parameters ()
    :precondition 
      (in r10)
    :effect
      (and
        (in r11)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r10))
      )
    )
  (:action move-left-from-r10
    :parameters ()
    :precondition 
      (in r10)
    :effect
      (and
        (in r9)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r10))
      )
    )
  (:action move-right-from-r11
    :parameters ()
    :precondition 
      (in r11)
    :effect
      (and
        (in r12)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r11))
      )
    )
  (:action move-left-from-r11
    :parameters ()
    :precondition 
      (in r11)
    :effect
      (and
        (in r10)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r11))
      )
    )
  (:action move-right-from-r12
    :parameters ()
    :precondition 
      (in r12)
    :effect
      (and
        (in r13)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r12))
      )
    )
  (:action move-left-from-r12
    :parameters ()
    :precondition 
      (in r12)
    :effect
      (and
        (in r11)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r12))
      )
    )
  (:action move-right-from-r13
    :parameters ()
    :precondition 
      (in r13)
    :effect
      (and
        (in r14)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r13))
      )
    )
  (:action move-left-from-r13
    :parameters ()
    :precondition 
      (in r13)
    :effect
      (and
        (in r12)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r13))
      )
    )
  (:action move-right-from-r14
    :parameters ()
    :precondition 
      (in r14)
    :effect
      (and
        (in r15)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r14))
      )
    )
  (:action move-left-from-r14
    :parameters ()
    :precondition 
      (in r14)
    :effect
      (and
        (in r13)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r14))
      )
    )
  (:action move-right-from-r15
    :parameters ()
    :precondition 
      (in r15)
    :effect
      (and
        (in r16)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r15))
      )
    )
  (:action move-left-from-r15
    :parameters ()
    :precondition 
      (in r15)
    :effect
      (and
        (in r14)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r15))
      )
    )
  (:action move-right-from-r16
    :parameters ()
    :precondition 
      (in r16)
    :effect
      (and
        (in r17)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r16))
      )
    )
  (:action move-left-from-r16
    :parameters ()
    :precondition 
      (in r16)
    :effect
      (and
        (in r15)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r16))
      )
    )
  (:action move-right-from-r17
    :parameters ()
    :precondition 
      (in r17)
    :effect
      (and
        (in r18)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r17))
      )
    )
  (:action move-left-from-r17
    :parameters ()
    :precondition 
      (in r17)
    :effect
      (and
        (in r16)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r17))
      )
    )
  (:action move-right-from-r18
    :parameters ()
    :precondition 
      (in r18)
    :effect
      (and
        (in r19)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r18))
      )
    )
  (:action move-left-from-r18
    :parameters ()
    :precondition 
      (in r18)
    :effect
      (and
        (in r17)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r18))
      )
    )
  (:action move-right-from-r19
    :parameters ()
    :precondition 
      (in r19)
    :effect
      (and
        (in r20)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r19))
      )
    )
  (:action move-left-from-r19
    :parameters ()
    :precondition 
      (in r19)
    :effect
      (and
        (in r18)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r19))
      )
    )
  (:action move-right-from-r20
    :parameters ()
    :precondition 
      (in r20)
    :effect
      (and
        (in r21)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r20))
      )
    )
  (:action move-left-from-r20
    :parameters ()
    :precondition 
      (in r20)
    :effect
      (and
        (in r19)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r20))
      )
    )
  (:action move-right-from-r21
    :parameters ()
    :precondition 
      (in r21)
    :effect
      (and
        (in r22)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r21))
      )
    )
  (:action move-left-from-r21
    :parameters ()
    :precondition 
      (in r21)
    :effect
      (and
        (in r20)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r21))
      )
    )
  (:action move-right-from-r22
    :parameters ()
    :precondition 
      (in r22)
    :effect
      (and
        (in r23)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r22))
      )
    )
  (:action move-left-from-r22
    :parameters ()
    :precondition 
      (in r22)
    :effect
      (and
        (in r21)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r22))
      )
    )
  (:action move-right-from-r23
    :parameters ()
    :precondition 
      (in r23)
    :effect
      (and
        (in r24)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r23))
      )
    )
  (:action move-left-from-r23
    :parameters ()
    :precondition 
      (in r23)
    :effect
      (and
        (in r22)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r23))
      )
    )
  (:action move-right-from-r24
    :parameters ()
    :precondition 
      (in r24)
    :effect
      (and
        (in r25)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r24))
      )
    )
  (:action move-left-from-r24
    :parameters ()
    :precondition 
      (in r24)
    :effect
      (and
        (in r23)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r24))
      )
    )
  (:action move-right-from-r25
    :parameters ()
    :precondition 
      (in r25)
    :effect
      (and
        (in r26)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r25))
      )
    )
  (:action move-left-from-r25
    :parameters ()
    :precondition 
      (in r25)
    :effect
      (and
        (in r24)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r25))
      )
    )
  (:action move-right-from-r26
    :parameters ()
    :precondition 
      (in r26)
    :effect
      (and
        (in r27)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r26))
      )
    )
  (:action move-left-from-r26
    :parameters ()
    :precondition 
      (in r26)
    :effect
      (and
        (in r25)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r26))
      )
    )
  (:action move-right-from-r27
    :parameters ()
    :precondition 
      (in r27)
    :effect
      (and
        (in r28)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r27))
      )
    )
  (:action move-left-from-r27
    :parameters ()
    :precondition 
      (in r27)
    :effect
      (and
        (in r26)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r27))
      )
    )
  (:action move-right-from-r28
    :parameters ()
    :precondition 
      (in r28)
    :effect
      (and
        (in r29)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r28))
      )
    )
  (:action move-left-from-r28
    :parameters ()
    :precondition 
      (in r28)
    :effect
      (and
        (in r27)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r28))
      )
    )
  (:action move-right-from-r29
    :parameters ()
    :precondition 
      (in r29)
    :effect
      (and
        (in r30)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r29))
      )
    )
  (:action move-left-from-r29
    :parameters ()
    :precondition 
      (in r29)
    :effect
      (and
        (in r28)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r29))
      )
    )
  (:action move-right-from-r30
    :parameters ()
    :precondition 
      (in r30)
    :effect
      (and
        (in r31)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r30))
      )
    )
  (:action move-left-from-r30
    :parameters ()
    :precondition 
      (in r30)
    :effect
      (and
        (in r29)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r30))
      )
    )
  (:action move-right-from-r31
    :parameters ()
    :precondition 
      (in r31)
    :effect
      (and
        (in r32)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r31))
      )
    )
  (:action move-left-from-r31
    :parameters ()
    :precondition 
      (in r31)
    :effect
      (and
        (in r30)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r31))
      )
    )
  (:action move-right-from-r32
    :parameters ()
    :precondition 
      (in r32)
    :effect
      (and
        (in r33)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r32))
      )
    )
  (:action move-left-from-r32
    :parameters ()
    :precondition 
      (in r32)
    :effect
      (and
        (in r31)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r32))
      )
    )
  (:action move-right-from-r33
    :parameters ()
    :precondition 
      (in r33)
    :effect
      (and
        (in r34)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r33))
      )
    )
  (:action move-left-from-r33
    :parameters ()
    :precondition 
      (in r33)
    :effect
      (and
        (in r32)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r33))
      )
    )
  (:action move-right-from-r34
    :parameters ()
    :precondition 
      (in r34)
    :effect
      (and
        (in r35)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r34))
      )
    )
  (:action move-left-from-r34
    :parameters ()
    :precondition 
      (in r34)
    :effect
      (and
        (in r33)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r34))
      )
    )
  (:action move-right-from-r35
    :parameters ()
    :precondition 
      (in r35)
    :effect
      (and
        (in r36)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r35))
      )
    )
  (:action move-left-from-r35
    :parameters ()
    :precondition 
      (in r35)
    :effect
      (and
        (in r34)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r35))
      )
    )
  (:action move-right-from-r36
    :parameters ()
    :precondition 
      (in r36)
    :effect
      (and
        (in r37)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r36))
      )
    )
  (:action move-left-from-r36
    :parameters ()
    :precondition 
      (in r36)
    :effect
      (and
        (in r35)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r36))
      )
    )
  (:action move-right-from-r37
    :parameters ()
    :precondition 
      (in r37)
    :effect
      (and
        (in r38)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r37))
      )
    )
  (:action move-left-from-r37
    :parameters ()
    :precondition 
      (in r37)
    :effect
      (and
        (in r36)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r37))
      )
    )
  (:action move-right-from-r38
    :parameters ()
    :precondition 
      (in r38)
    :effect
      (and
        (in r39)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r38))
      )
    )
  (:action move-left-from-r38
    :parameters ()
    :precondition 
      (in r38)
    :effect
      (and
        (in r37)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r38))
      )
    )
  (:action move-right-from-r39
    :parameters ()
    :precondition 
      (in r39)
    :effect
      (and
        (in r40)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r39))
      )
    )
  (:action move-left-from-r39
    :parameters ()
    :precondition 
      (in r39)
    :effect
      (and
        (in r38)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r39))
      )
    )
  (:action move-right-from-r40
    :parameters ()
    :precondition 
      (in r40)
    :effect
      (and
        (in r41)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r40))
      )
    )
  (:action move-left-from-r40
    :parameters ()
    :precondition 
      (in r40)
    :effect
      (and
        (in r39)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r40))
      )
    )
  (:action move-right-from-r41
    :parameters ()
    :precondition 
      (in r41)
    :effect
      (and
        (in r42)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r41))
      )
    )
  (:action move-left-from-r41
    :parameters ()
    :precondition 
      (in r41)
    :effect
      (and
        (in r40)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r41))
      )
    )
  (:action move-right-from-r42
    :parameters ()
    :precondition 
      (in r42)
    :effect
      (and
        (in r43)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r42))
      )
    )
  (:action move-left-from-r42
    :parameters ()
    :precondition 
      (in r42)
    :effect
      (and
        (in r41)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r42))
      )
    )
  (:action move-right-from-r43
    :parameters ()
    :precondition 
      (in r43)
    :effect
      (and
        (in r44)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r43))
      )
    )
  (:action move-left-from-r43
    :parameters ()
    :precondition 
      (in r43)
    :effect
      (and
        (in r42)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r43))
      )
    )
  (:action move-right-from-r44
    :parameters ()
    :precondition 
      (in r44)
    :effect
      (and
        (in r45)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r44))
      )
    )
  (:action move-left-from-r44
    :parameters ()
    :precondition 
      (in r44)
    :effect
      (and
        (in r43)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r44))
      )
    )
  (:action move-right-from-r45
    :parameters ()
    :precondition 
      (in r45)
    :effect
      (and
        (in r46)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r45))
      )
    )
  (:action move-left-from-r45
    :parameters ()
    :precondition 
      (in r45)
    :effect
      (and
        (in r44)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r45))
      )
    )
  (:action move-right-from-r46
    :parameters ()
    :precondition 
      (in r46)
    :effect
      (and
        (in r47)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r46))
      )
    )
  (:action move-left-from-r46
    :parameters ()
    :precondition 
      (in r46)
    :effect
      (and
        (in r45)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r46))
      )
    )
  (:action move-right-from-r47
    :parameters ()
    :precondition 
      (in r47)
    :effect
      (and
        (in r48)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r47))
      )
    )
  (:action move-left-from-r47
    :parameters ()
    :precondition 
      (in r47)
    :effect
      (and
        (in r46)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r47))
      )
    )
  (:action move-right-from-r48
    :parameters ()
    :precondition 
      (in r48)
    :effect
      (and
        (in r49)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r48))
      )
    )
  (:action move-left-from-r48
    :parameters ()
    :precondition 
      (in r48)
    :effect
      (and
        (in r47)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r48))
      )
    )
  (:action move-right-from-r49_detdup_0
    :parameters ()
    :precondition 
      (in r49)
    :effect
      (and
        (in r50)
        (not 
          (in r49))
        (not 
          (seen))
      )
    )
  (:action move-right-from-r49_detdup_1
    :parameters ()
    :precondition 
      (in r49)
    :effect
      (and
        (in r50)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r49))
      )
    )
  (:action move-left-from-r49
    :parameters ()
    :precondition 
      (in r49)
    :effect
      (and
        (in r48)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r49))
      )
    )
  (:action move-right-from-r50
    :parameters ()
    :precondition 
      (and
        (in r50)
        (not 
          (seen)))
    :effect
      (and
        (in r51)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r50))
      )
    )
  (:action move-left-from-r50
    :parameters ()
    :precondition 
      (and
        (in r50)
        (not 
          (seen)))
    :effect
      (and
        (in r49)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r50))
      )
    )
  (:action move-right-from-r51
    :parameters ()
    :precondition 
      (in r51)
    :effect
      (and
        (in r52)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r51))
      )
    )
  (:action move-left-from-r51_detdup_0
    :parameters ()
    :precondition 
      (in r51)
    :effect
      (and
        (in r50)
        (not 
          (in r51))
        (not 
          (seen))
      )
    )
  (:action move-left-from-r51_detdup_1
    :parameters ()
    :precondition 
      (in r51)
    :effect
      (and
        (in r50)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r51))
      )
    )
  (:action move-right-from-r52
    :parameters ()
    :precondition 
      (in r52)
    :effect
      (and
        (in r53)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r52))
      )
    )
  (:action move-left-from-r52
    :parameters ()
    :precondition 
      (in r52)
    :effect
      (and
        (in r51)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r52))
      )
    )
  (:action move-right-from-r53
    :parameters ()
    :precondition 
      (in r53)
    :effect
      (and
        (in r54)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r53))
      )
    )
  (:action move-left-from-r53
    :parameters ()
    :precondition 
      (in r53)
    :effect
      (and
        (in r52)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r53))
      )
    )
  (:action move-right-from-r54
    :parameters ()
    :precondition 
      (in r54)
    :effect
      (and
        (in r55)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r54))
      )
    )
  (:action move-left-from-r54
    :parameters ()
    :precondition 
      (in r54)
    :effect
      (and
        (in r53)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r54))
      )
    )
  (:action move-right-from-r55
    :parameters ()
    :precondition 
      (in r55)
    :effect
      (and
        (in r56)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r55))
      )
    )
  (:action move-left-from-r55
    :parameters ()
    :precondition 
      (in r55)
    :effect
      (and
        (in r54)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r55))
      )
    )
  (:action move-right-from-r56
    :parameters ()
    :precondition 
      (in r56)
    :effect
      (and
        (in r57)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r56))
      )
    )
  (:action move-left-from-r56
    :parameters ()
    :precondition 
      (in r56)
    :effect
      (and
        (in r55)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r56))
      )
    )
  (:action move-right-from-r57
    :parameters ()
    :precondition 
      (in r57)
    :effect
      (and
        (in r58)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r57))
      )
    )
  (:action move-left-from-r57
    :parameters ()
    :precondition 
      (in r57)
    :effect
      (and
        (in r56)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r57))
      )
    )
  (:action move-right-from-r58
    :parameters ()
    :precondition 
      (in r58)
    :effect
      (and
        (in r59)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r58))
      )
    )
  (:action move-left-from-r58
    :parameters ()
    :precondition 
      (in r58)
    :effect
      (and
        (in r57)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r58))
      )
    )
  (:action move-right-from-r59
    :parameters ()
    :precondition 
      (in r59)
    :effect
      (and
        (in r60)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r59))
      )
    )
  (:action move-left-from-r59
    :parameters ()
    :precondition 
      (in r59)
    :effect
      (and
        (in r58)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r59))
      )
    )
  (:action move-right-from-r60
    :parameters ()
    :precondition 
      (in r60)
    :effect
      (and
        (in r61)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r60))
      )
    )
  (:action move-left-from-r60
    :parameters ()
    :precondition 
      (in r60)
    :effect
      (and
        (in r59)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r60))
      )
    )
  (:action move-right-from-r61
    :parameters ()
    :precondition 
      (in r61)
    :effect
      (and
        (in r62)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r61))
      )
    )
  (:action move-left-from-r61
    :parameters ()
    :precondition 
      (in r61)
    :effect
      (and
        (in r60)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r61))
      )
    )
  (:action move-right-from-r62
    :parameters ()
    :precondition 
      (in r62)
    :effect
      (and
        (in r63)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r62))
      )
    )
  (:action move-left-from-r62
    :parameters ()
    :precondition 
      (in r62)
    :effect
      (and
        (in r61)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r62))
      )
    )
  (:action move-right-from-r63
    :parameters ()
    :precondition 
      (in r63)
    :effect
      (and
        (in r64)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r63))
      )
    )
  (:action move-left-from-r63
    :parameters ()
    :precondition 
      (in r63)
    :effect
      (and
        (in r62)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r63))
      )
    )
  (:action move-right-from-r64
    :parameters ()
    :precondition 
      (in r64)
    :effect
      (and
        (in r65)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r64))
      )
    )
  (:action move-left-from-r64
    :parameters ()
    :precondition 
      (in r64)
    :effect
      (and
        (in r63)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r64))
      )
    )
  (:action move-right-from-r65
    :parameters ()
    :precondition 
      (in r65)
    :effect
      (and
        (in r66)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r65))
      )
    )
  (:action move-left-from-r65
    :parameters ()
    :precondition 
      (in r65)
    :effect
      (and
        (in r64)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r65))
      )
    )
  (:action move-right-from-r66
    :parameters ()
    :precondition 
      (in r66)
    :effect
      (and
        (in r67)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r66))
      )
    )
  (:action move-left-from-r66
    :parameters ()
    :precondition 
      (in r66)
    :effect
      (and
        (in r65)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r66))
      )
    )
  (:action move-right-from-r67
    :parameters ()
    :precondition 
      (in r67)
    :effect
      (and
        (in r68)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r67))
      )
    )
  (:action move-left-from-r67
    :parameters ()
    :precondition 
      (in r67)
    :effect
      (and
        (in r66)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r67))
      )
    )
  (:action move-right-from-r68
    :parameters ()
    :precondition 
      (in r68)
    :effect
      (and
        (in r69)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r68))
      )
    )
  (:action move-left-from-r68
    :parameters ()
    :precondition 
      (in r68)
    :effect
      (and
        (in r67)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r68))
      )
    )
  (:action move-right-from-r69
    :parameters ()
    :precondition 
      (in r69)
    :effect
      (and
        (in r70)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r69))
      )
    )
  (:action move-left-from-r69
    :parameters ()
    :precondition 
      (in r69)
    :effect
      (and
        (in r68)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r69))
      )
    )
  (:action move-right-from-r70
    :parameters ()
    :precondition 
      (in r70)
    :effect
      (and
        (in r71)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r70))
      )
    )
  (:action move-left-from-r70
    :parameters ()
    :precondition 
      (in r70)
    :effect
      (and
        (in r69)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r70))
      )
    )
  (:action move-right-from-r71
    :parameters ()
    :precondition 
      (in r71)
    :effect
      (and
        (in r72)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r71))
      )
    )
  (:action move-left-from-r71
    :parameters ()
    :precondition 
      (in r71)
    :effect
      (and
        (in r70)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r71))
      )
    )
  (:action move-right-from-r72
    :parameters ()
    :precondition 
      (in r72)
    :effect
      (and
        (in r73)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r72))
      )
    )
  (:action move-left-from-r72
    :parameters ()
    :precondition 
      (in r72)
    :effect
      (and
        (in r71)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r72))
      )
    )
  (:action move-right-from-r73
    :parameters ()
    :precondition 
      (in r73)
    :effect
      (and
        (in r74)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r73))
      )
    )
  (:action move-left-from-r73
    :parameters ()
    :precondition 
      (in r73)
    :effect
      (and
        (in r72)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r73))
      )
    )
  (:action move-right-from-r74
    :parameters ()
    :precondition 
      (in r74)
    :effect
      (and
        (in r75)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r74))
      )
    )
  (:action move-left-from-r74
    :parameters ()
    :precondition 
      (in r74)
    :effect
      (and
        (in r73)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r74))
      )
    )
  (:action move-right-from-r75
    :parameters ()
    :precondition 
      (in r75)
    :effect
      (and
        (in r76)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r75))
      )
    )
  (:action move-left-from-r75
    :parameters ()
    :precondition 
      (in r75)
    :effect
      (and
        (in r74)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r75))
      )
    )
  (:action move-right-from-r76
    :parameters ()
    :precondition 
      (in r76)
    :effect
      (and
        (in r77)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r76))
      )
    )
  (:action move-left-from-r76
    :parameters ()
    :precondition 
      (in r76)
    :effect
      (and
        (in r75)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r76))
      )
    )
  (:action move-right-from-r77
    :parameters ()
    :precondition 
      (in r77)
    :effect
      (and
        (in r78)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r77))
      )
    )
  (:action move-left-from-r77
    :parameters ()
    :precondition 
      (in r77)
    :effect
      (and
        (in r76)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r77))
      )
    )
  (:action move-right-from-r78
    :parameters ()
    :precondition 
      (in r78)
    :effect
      (and
        (in r79)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r78))
      )
    )
  (:action move-left-from-r78
    :parameters ()
    :precondition 
      (in r78)
    :effect
      (and
        (in r77)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r78))
      )
    )
  (:action move-right-from-r79
    :parameters ()
    :precondition 
      (in r79)
    :effect
      (and
        (in r80)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r79))
      )
    )
  (:action move-left-from-r79
    :parameters ()
    :precondition 
      (in r79)
    :effect
      (and
        (in r78)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r79))
      )
    )
  (:action move-right-from-r80
    :parameters ()
    :precondition 
      (in r80)
    :effect
      (and
        (in r81)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r80))
      )
    )
  (:action move-left-from-r80
    :parameters ()
    :precondition 
      (in r80)
    :effect
      (and
        (in r79)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r80))
      )
    )
  (:action move-right-from-r81
    :parameters ()
    :precondition 
      (in r81)
    :effect
      (and
        (in r82)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r81))
      )
    )
  (:action move-left-from-r81
    :parameters ()
    :precondition 
      (in r81)
    :effect
      (and
        (in r80)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r81))
      )
    )
  (:action move-right-from-r82
    :parameters ()
    :precondition 
      (in r82)
    :effect
      (and
        (in r83)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r82))
      )
    )
  (:action move-left-from-r82
    :parameters ()
    :precondition 
      (in r82)
    :effect
      (and
        (in r81)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r82))
      )
    )
  (:action move-right-from-r83
    :parameters ()
    :precondition 
      (in r83)
    :effect
      (and
        (in r84)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r83))
      )
    )
  (:action move-left-from-r83
    :parameters ()
    :precondition 
      (in r83)
    :effect
      (and
        (in r82)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r83))
      )
    )
  (:action move-right-from-r84
    :parameters ()
    :precondition 
      (in r84)
    :effect
      (and
        (in r85)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r84))
      )
    )
  (:action move-left-from-r84
    :parameters ()
    :precondition 
      (in r84)
    :effect
      (and
        (in r83)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r84))
      )
    )
  (:action move-right-from-r85
    :parameters ()
    :precondition 
      (in r85)
    :effect
      (and
        (in r86)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r85))
      )
    )
  (:action move-left-from-r85
    :parameters ()
    :precondition 
      (in r85)
    :effect
      (and
        (in r84)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r85))
      )
    )
  (:action move-right-from-r86
    :parameters ()
    :precondition 
      (in r86)
    :effect
      (and
        (in r87)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r86))
      )
    )
  (:action move-left-from-r86
    :parameters ()
    :precondition 
      (in r86)
    :effect
      (and
        (in r85)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r86))
      )
    )
  (:action move-right-from-r87
    :parameters ()
    :precondition 
      (in r87)
    :effect
      (and
        (in r88)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r87))
      )
    )
  (:action move-left-from-r87
    :parameters ()
    :precondition 
      (in r87)
    :effect
      (and
        (in r86)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r87))
      )
    )
  (:action move-right-from-r88
    :parameters ()
    :precondition 
      (in r88)
    :effect
      (and
        (in r89)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r88))
      )
    )
  (:action move-left-from-r88
    :parameters ()
    :precondition 
      (in r88)
    :effect
      (and
        (in r87)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r88))
      )
    )
  (:action move-right-from-r89
    :parameters ()
    :precondition 
      (in r89)
    :effect
      (and
        (in r90)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r89))
      )
    )
  (:action move-left-from-r89
    :parameters ()
    :precondition 
      (in r89)
    :effect
      (and
        (in r88)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r89))
      )
    )
  (:action move-right-from-r90
    :parameters ()
    :precondition 
      (in r90)
    :effect
      (and
        (in r91)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r90))
      )
    )
  (:action move-left-from-r90
    :parameters ()
    :precondition 
      (in r90)
    :effect
      (and
        (in r89)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r90))
      )
    )
  (:action move-right-from-r91
    :parameters ()
    :precondition 
      (in r91)
    :effect
      (and
        (in r92)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r91))
      )
    )
  (:action move-left-from-r91
    :parameters ()
    :precondition 
      (in r91)
    :effect
      (and
        (in r90)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r91))
      )
    )
  (:action move-right-from-r92
    :parameters ()
    :precondition 
      (in r92)
    :effect
      (and
        (in r93)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r92))
      )
    )
  (:action move-left-from-r92
    :parameters ()
    :precondition 
      (in r92)
    :effect
      (and
        (in r91)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r92))
      )
    )
  (:action move-right-from-r93
    :parameters ()
    :precondition 
      (in r93)
    :effect
      (and
        (in r94)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r93))
      )
    )
  (:action move-left-from-r93
    :parameters ()
    :precondition 
      (in r93)
    :effect
      (and
        (in r92)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r93))
      )
    )
  (:action move-right-from-r94
    :parameters ()
    :precondition 
      (in r94)
    :effect
      (and
        (in r95)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r94))
      )
    )
  (:action move-left-from-r94
    :parameters ()
    :precondition 
      (in r94)
    :effect
      (and
        (in r93)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r94))
      )
    )
  (:action move-right-from-r95
    :parameters ()
    :precondition 
      (in r95)
    :effect
      (and
        (in r96)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r95))
      )
    )
  (:action move-left-from-r95
    :parameters ()
    :precondition 
      (in r95)
    :effect
      (and
        (in r94)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r95))
      )
    )
  (:action move-right-from-r96
    :parameters ()
    :precondition 
      (in r96)
    :effect
      (and
        (in r97)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r96))
      )
    )
  (:action move-left-from-r96
    :parameters ()
    :precondition 
      (in r96)
    :effect
      (and
        (in r95)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r96))
      )
    )
  (:action move-right-from-r97
    :parameters ()
    :precondition 
      (in r97)
    :effect
      (and
        (in r98)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r97))
      )
    )
  (:action move-left-from-r97
    :parameters ()
    :precondition 
      (in r97)
    :effect
      (and
        (in r96)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r97))
      )
    )
  (:action move-right-from-r98
    :parameters ()
    :precondition 
      (in r98)
    :effect
      (and
        (in r99)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r98))
      )
    )
  (:action move-left-from-r98
    :parameters ()
    :precondition 
      (in r98)
    :effect
      (and
        (in r97)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r98))
      )
    )
  (:action move-right-from-r99_detdup_0
    :parameters ()
    :precondition 
      (in r99)
    :effect
      (and
        (in r100)
        (not 
          (in r99))
        (not 
          (seen))
      )
    )
  (:action move-right-from-r99_detdup_1
    :parameters ()
    :precondition 
      (in r99)
    :effect
      (and
        (in r100)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r99))
      )
    )
  (:action move-left-from-r99
    :parameters ()
    :precondition 
      (in r99)
    :effect
      (and
        (in r98)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r99))
      )
    )
  (:action move-right-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen)))
    :effect
      (and
        (in r1)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r100))
      )
    )
  (:action move-left-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen)))
    :effect
      (and
        (in r99)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r100))
      )
    )
  (:action stay
    :parameters ()
    :precondition 
      (and
        (not 
          (in r50))
        (not 
          (in r100)))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r50-seen
    :parameters ()
    :precondition 
      (and
        (in r50)
        (seen))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r50-not-seen_detdup_0
    :parameters ()
    :precondition 
      (and
        (in r50)
        (not 
          (seen)))
    :effect
(not 
        (seen))    )
  (:action stay-in-r50-not-seen_detdup_1
    :parameters ()
    :precondition 
      (and
        (in r50)
        (not 
          (seen)))
    :effect
      (and
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
      )
    )
  (:action stay-in-r100-seen
    :parameters ()
    :precondition 
      (and
        (in r100)
        (seen))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r100-not-seen_detdup_0
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen)))
    :effect
(not 
        (seen))    )
  (:action stay-in-r100-not-seen_detdup_1
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen)))
    :effect
      (and
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
      )
    )
)