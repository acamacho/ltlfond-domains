(define (domain waldo)
  (:types
    room - none
  )

  (:predicates
    (seen)
    (in ?x0 - room)
    (autstate_1_2)
    (autstate_1_1)
  )
  (:action move-right-from-r1
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r2)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r1))
      )
    )
  (:action move-left-from-r1_detdup_0
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r200)
        (not 
          (in r1))
        (not 
          (seen))
      )
    )
  (:action move-left-from-r1_detdup_1
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r200)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r1))
      )
    )
  (:action move-right-from-r2
    :parameters ()
    :precondition 
      (in r2)
    :effect
      (and
        (in r3)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r2))
      )
    )
  (:action move-left-from-r2
    :parameters ()
    :precondition 
      (in r2)
    :effect
      (and
        (in r1)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r2))
      )
    )
  (:action move-right-from-r3
    :parameters ()
    :precondition 
      (in r3)
    :effect
      (and
        (in r4)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r3))
      )
    )
  (:action move-left-from-r3
    :parameters ()
    :precondition 
      (in r3)
    :effect
      (and
        (in r2)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r3))
      )
    )
  (:action move-right-from-r4
    :parameters ()
    :precondition 
      (in r4)
    :effect
      (and
        (in r5)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r4))
      )
    )
  (:action move-left-from-r4
    :parameters ()
    :precondition 
      (in r4)
    :effect
      (and
        (in r3)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r4))
      )
    )
  (:action move-right-from-r5
    :parameters ()
    :precondition 
      (in r5)
    :effect
      (and
        (in r6)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r5))
      )
    )
  (:action move-left-from-r5
    :parameters ()
    :precondition 
      (in r5)
    :effect
      (and
        (in r4)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r5))
      )
    )
  (:action move-right-from-r6
    :parameters ()
    :precondition 
      (in r6)
    :effect
      (and
        (in r7)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r6))
      )
    )
  (:action move-left-from-r6
    :parameters ()
    :precondition 
      (in r6)
    :effect
      (and
        (in r5)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r6))
      )
    )
  (:action move-right-from-r7
    :parameters ()
    :precondition 
      (in r7)
    :effect
      (and
        (in r8)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r7))
      )
    )
  (:action move-left-from-r7
    :parameters ()
    :precondition 
      (in r7)
    :effect
      (and
        (in r6)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r7))
      )
    )
  (:action move-right-from-r8
    :parameters ()
    :precondition 
      (in r8)
    :effect
      (and
        (in r9)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r8))
      )
    )
  (:action move-left-from-r8
    :parameters ()
    :precondition 
      (in r8)
    :effect
      (and
        (in r7)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r8))
      )
    )
  (:action move-right-from-r9
    :parameters ()
    :precondition 
      (in r9)
    :effect
      (and
        (in r10)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r9))
      )
    )
  (:action move-left-from-r9
    :parameters ()
    :precondition 
      (in r9)
    :effect
      (and
        (in r8)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r9))
      )
    )
  (:action move-right-from-r10
    :parameters ()
    :precondition 
      (in r10)
    :effect
      (and
        (in r11)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r10))
      )
    )
  (:action move-left-from-r10
    :parameters ()
    :precondition 
      (in r10)
    :effect
      (and
        (in r9)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r10))
      )
    )
  (:action move-right-from-r11
    :parameters ()
    :precondition 
      (in r11)
    :effect
      (and
        (in r12)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r11))
      )
    )
  (:action move-left-from-r11
    :parameters ()
    :precondition 
      (in r11)
    :effect
      (and
        (in r10)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r11))
      )
    )
  (:action move-right-from-r12
    :parameters ()
    :precondition 
      (in r12)
    :effect
      (and
        (in r13)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r12))
      )
    )
  (:action move-left-from-r12
    :parameters ()
    :precondition 
      (in r12)
    :effect
      (and
        (in r11)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r12))
      )
    )
  (:action move-right-from-r13
    :parameters ()
    :precondition 
      (in r13)
    :effect
      (and
        (in r14)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r13))
      )
    )
  (:action move-left-from-r13
    :parameters ()
    :precondition 
      (in r13)
    :effect
      (and
        (in r12)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r13))
      )
    )
  (:action move-right-from-r14
    :parameters ()
    :precondition 
      (in r14)
    :effect
      (and
        (in r15)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r14))
      )
    )
  (:action move-left-from-r14
    :parameters ()
    :precondition 
      (in r14)
    :effect
      (and
        (in r13)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r14))
      )
    )
  (:action move-right-from-r15
    :parameters ()
    :precondition 
      (in r15)
    :effect
      (and
        (in r16)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r15))
      )
    )
  (:action move-left-from-r15
    :parameters ()
    :precondition 
      (in r15)
    :effect
      (and
        (in r14)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r15))
      )
    )
  (:action move-right-from-r16
    :parameters ()
    :precondition 
      (in r16)
    :effect
      (and
        (in r17)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r16))
      )
    )
  (:action move-left-from-r16
    :parameters ()
    :precondition 
      (in r16)
    :effect
      (and
        (in r15)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r16))
      )
    )
  (:action move-right-from-r17
    :parameters ()
    :precondition 
      (in r17)
    :effect
      (and
        (in r18)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r17))
      )
    )
  (:action move-left-from-r17
    :parameters ()
    :precondition 
      (in r17)
    :effect
      (and
        (in r16)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r17))
      )
    )
  (:action move-right-from-r18
    :parameters ()
    :precondition 
      (in r18)
    :effect
      (and
        (in r19)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r18))
      )
    )
  (:action move-left-from-r18
    :parameters ()
    :precondition 
      (in r18)
    :effect
      (and
        (in r17)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r18))
      )
    )
  (:action move-right-from-r19
    :parameters ()
    :precondition 
      (in r19)
    :effect
      (and
        (in r20)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r19))
      )
    )
  (:action move-left-from-r19
    :parameters ()
    :precondition 
      (in r19)
    :effect
      (and
        (in r18)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r19))
      )
    )
  (:action move-right-from-r20
    :parameters ()
    :precondition 
      (in r20)
    :effect
      (and
        (in r21)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r20))
      )
    )
  (:action move-left-from-r20
    :parameters ()
    :precondition 
      (in r20)
    :effect
      (and
        (in r19)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r20))
      )
    )
  (:action move-right-from-r21
    :parameters ()
    :precondition 
      (in r21)
    :effect
      (and
        (in r22)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r21))
      )
    )
  (:action move-left-from-r21
    :parameters ()
    :precondition 
      (in r21)
    :effect
      (and
        (in r20)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r21))
      )
    )
  (:action move-right-from-r22
    :parameters ()
    :precondition 
      (in r22)
    :effect
      (and
        (in r23)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r22))
      )
    )
  (:action move-left-from-r22
    :parameters ()
    :precondition 
      (in r22)
    :effect
      (and
        (in r21)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r22))
      )
    )
  (:action move-right-from-r23
    :parameters ()
    :precondition 
      (in r23)
    :effect
      (and
        (in r24)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r23))
      )
    )
  (:action move-left-from-r23
    :parameters ()
    :precondition 
      (in r23)
    :effect
      (and
        (in r22)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r23))
      )
    )
  (:action move-right-from-r24
    :parameters ()
    :precondition 
      (in r24)
    :effect
      (and
        (in r25)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r24))
      )
    )
  (:action move-left-from-r24
    :parameters ()
    :precondition 
      (in r24)
    :effect
      (and
        (in r23)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r24))
      )
    )
  (:action move-right-from-r25
    :parameters ()
    :precondition 
      (in r25)
    :effect
      (and
        (in r26)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r25))
      )
    )
  (:action move-left-from-r25
    :parameters ()
    :precondition 
      (in r25)
    :effect
      (and
        (in r24)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r25))
      )
    )
  (:action move-right-from-r26
    :parameters ()
    :precondition 
      (in r26)
    :effect
      (and
        (in r27)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r26))
      )
    )
  (:action move-left-from-r26
    :parameters ()
    :precondition 
      (in r26)
    :effect
      (and
        (in r25)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r26))
      )
    )
  (:action move-right-from-r27
    :parameters ()
    :precondition 
      (in r27)
    :effect
      (and
        (in r28)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r27))
      )
    )
  (:action move-left-from-r27
    :parameters ()
    :precondition 
      (in r27)
    :effect
      (and
        (in r26)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r27))
      )
    )
  (:action move-right-from-r28
    :parameters ()
    :precondition 
      (in r28)
    :effect
      (and
        (in r29)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r28))
      )
    )
  (:action move-left-from-r28
    :parameters ()
    :precondition 
      (in r28)
    :effect
      (and
        (in r27)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r28))
      )
    )
  (:action move-right-from-r29
    :parameters ()
    :precondition 
      (in r29)
    :effect
      (and
        (in r30)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r29))
      )
    )
  (:action move-left-from-r29
    :parameters ()
    :precondition 
      (in r29)
    :effect
      (and
        (in r28)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r29))
      )
    )
  (:action move-right-from-r30
    :parameters ()
    :precondition 
      (in r30)
    :effect
      (and
        (in r31)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r30))
      )
    )
  (:action move-left-from-r30
    :parameters ()
    :precondition 
      (in r30)
    :effect
      (and
        (in r29)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r30))
      )
    )
  (:action move-right-from-r31
    :parameters ()
    :precondition 
      (in r31)
    :effect
      (and
        (in r32)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r31))
      )
    )
  (:action move-left-from-r31
    :parameters ()
    :precondition 
      (in r31)
    :effect
      (and
        (in r30)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r31))
      )
    )
  (:action move-right-from-r32
    :parameters ()
    :precondition 
      (in r32)
    :effect
      (and
        (in r33)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r32))
      )
    )
  (:action move-left-from-r32
    :parameters ()
    :precondition 
      (in r32)
    :effect
      (and
        (in r31)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r32))
      )
    )
  (:action move-right-from-r33
    :parameters ()
    :precondition 
      (in r33)
    :effect
      (and
        (in r34)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r33))
      )
    )
  (:action move-left-from-r33
    :parameters ()
    :precondition 
      (in r33)
    :effect
      (and
        (in r32)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r33))
      )
    )
  (:action move-right-from-r34
    :parameters ()
    :precondition 
      (in r34)
    :effect
      (and
        (in r35)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r34))
      )
    )
  (:action move-left-from-r34
    :parameters ()
    :precondition 
      (in r34)
    :effect
      (and
        (in r33)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r34))
      )
    )
  (:action move-right-from-r35
    :parameters ()
    :precondition 
      (in r35)
    :effect
      (and
        (in r36)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r35))
      )
    )
  (:action move-left-from-r35
    :parameters ()
    :precondition 
      (in r35)
    :effect
      (and
        (in r34)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r35))
      )
    )
  (:action move-right-from-r36
    :parameters ()
    :precondition 
      (in r36)
    :effect
      (and
        (in r37)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r36))
      )
    )
  (:action move-left-from-r36
    :parameters ()
    :precondition 
      (in r36)
    :effect
      (and
        (in r35)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r36))
      )
    )
  (:action move-right-from-r37
    :parameters ()
    :precondition 
      (in r37)
    :effect
      (and
        (in r38)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r37))
      )
    )
  (:action move-left-from-r37
    :parameters ()
    :precondition 
      (in r37)
    :effect
      (and
        (in r36)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r37))
      )
    )
  (:action move-right-from-r38
    :parameters ()
    :precondition 
      (in r38)
    :effect
      (and
        (in r39)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r38))
      )
    )
  (:action move-left-from-r38
    :parameters ()
    :precondition 
      (in r38)
    :effect
      (and
        (in r37)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r38))
      )
    )
  (:action move-right-from-r39
    :parameters ()
    :precondition 
      (in r39)
    :effect
      (and
        (in r40)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r39))
      )
    )
  (:action move-left-from-r39
    :parameters ()
    :precondition 
      (in r39)
    :effect
      (and
        (in r38)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r39))
      )
    )
  (:action move-right-from-r40
    :parameters ()
    :precondition 
      (in r40)
    :effect
      (and
        (in r41)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r40))
      )
    )
  (:action move-left-from-r40
    :parameters ()
    :precondition 
      (in r40)
    :effect
      (and
        (in r39)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r40))
      )
    )
  (:action move-right-from-r41
    :parameters ()
    :precondition 
      (in r41)
    :effect
      (and
        (in r42)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r41))
      )
    )
  (:action move-left-from-r41
    :parameters ()
    :precondition 
      (in r41)
    :effect
      (and
        (in r40)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r41))
      )
    )
  (:action move-right-from-r42
    :parameters ()
    :precondition 
      (in r42)
    :effect
      (and
        (in r43)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r42))
      )
    )
  (:action move-left-from-r42
    :parameters ()
    :precondition 
      (in r42)
    :effect
      (and
        (in r41)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r42))
      )
    )
  (:action move-right-from-r43
    :parameters ()
    :precondition 
      (in r43)
    :effect
      (and
        (in r44)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r43))
      )
    )
  (:action move-left-from-r43
    :parameters ()
    :precondition 
      (in r43)
    :effect
      (and
        (in r42)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r43))
      )
    )
  (:action move-right-from-r44
    :parameters ()
    :precondition 
      (in r44)
    :effect
      (and
        (in r45)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r44))
      )
    )
  (:action move-left-from-r44
    :parameters ()
    :precondition 
      (in r44)
    :effect
      (and
        (in r43)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r44))
      )
    )
  (:action move-right-from-r45
    :parameters ()
    :precondition 
      (in r45)
    :effect
      (and
        (in r46)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r45))
      )
    )
  (:action move-left-from-r45
    :parameters ()
    :precondition 
      (in r45)
    :effect
      (and
        (in r44)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r45))
      )
    )
  (:action move-right-from-r46
    :parameters ()
    :precondition 
      (in r46)
    :effect
      (and
        (in r47)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r46))
      )
    )
  (:action move-left-from-r46
    :parameters ()
    :precondition 
      (in r46)
    :effect
      (and
        (in r45)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r46))
      )
    )
  (:action move-right-from-r47
    :parameters ()
    :precondition 
      (in r47)
    :effect
      (and
        (in r48)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r47))
      )
    )
  (:action move-left-from-r47
    :parameters ()
    :precondition 
      (in r47)
    :effect
      (and
        (in r46)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r47))
      )
    )
  (:action move-right-from-r48
    :parameters ()
    :precondition 
      (in r48)
    :effect
      (and
        (in r49)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r48))
      )
    )
  (:action move-left-from-r48
    :parameters ()
    :precondition 
      (in r48)
    :effect
      (and
        (in r47)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r48))
      )
    )
  (:action move-right-from-r49
    :parameters ()
    :precondition 
      (in r49)
    :effect
      (and
        (in r50)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r49))
      )
    )
  (:action move-left-from-r49
    :parameters ()
    :precondition 
      (in r49)
    :effect
      (and
        (in r48)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r49))
      )
    )
  (:action move-right-from-r50
    :parameters ()
    :precondition 
      (in r50)
    :effect
      (and
        (in r51)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r50))
      )
    )
  (:action move-left-from-r50
    :parameters ()
    :precondition 
      (in r50)
    :effect
      (and
        (in r49)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r50))
      )
    )
  (:action move-right-from-r51
    :parameters ()
    :precondition 
      (in r51)
    :effect
      (and
        (in r52)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r51))
      )
    )
  (:action move-left-from-r51
    :parameters ()
    :precondition 
      (in r51)
    :effect
      (and
        (in r50)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r51))
      )
    )
  (:action move-right-from-r52
    :parameters ()
    :precondition 
      (in r52)
    :effect
      (and
        (in r53)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r52))
      )
    )
  (:action move-left-from-r52
    :parameters ()
    :precondition 
      (in r52)
    :effect
      (and
        (in r51)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r52))
      )
    )
  (:action move-right-from-r53
    :parameters ()
    :precondition 
      (in r53)
    :effect
      (and
        (in r54)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r53))
      )
    )
  (:action move-left-from-r53
    :parameters ()
    :precondition 
      (in r53)
    :effect
      (and
        (in r52)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r53))
      )
    )
  (:action move-right-from-r54
    :parameters ()
    :precondition 
      (in r54)
    :effect
      (and
        (in r55)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r54))
      )
    )
  (:action move-left-from-r54
    :parameters ()
    :precondition 
      (in r54)
    :effect
      (and
        (in r53)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r54))
      )
    )
  (:action move-right-from-r55
    :parameters ()
    :precondition 
      (in r55)
    :effect
      (and
        (in r56)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r55))
      )
    )
  (:action move-left-from-r55
    :parameters ()
    :precondition 
      (in r55)
    :effect
      (and
        (in r54)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r55))
      )
    )
  (:action move-right-from-r56
    :parameters ()
    :precondition 
      (in r56)
    :effect
      (and
        (in r57)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r56))
      )
    )
  (:action move-left-from-r56
    :parameters ()
    :precondition 
      (in r56)
    :effect
      (and
        (in r55)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r56))
      )
    )
  (:action move-right-from-r57
    :parameters ()
    :precondition 
      (in r57)
    :effect
      (and
        (in r58)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r57))
      )
    )
  (:action move-left-from-r57
    :parameters ()
    :precondition 
      (in r57)
    :effect
      (and
        (in r56)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r57))
      )
    )
  (:action move-right-from-r58
    :parameters ()
    :precondition 
      (in r58)
    :effect
      (and
        (in r59)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r58))
      )
    )
  (:action move-left-from-r58
    :parameters ()
    :precondition 
      (in r58)
    :effect
      (and
        (in r57)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r58))
      )
    )
  (:action move-right-from-r59
    :parameters ()
    :precondition 
      (in r59)
    :effect
      (and
        (in r60)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r59))
      )
    )
  (:action move-left-from-r59
    :parameters ()
    :precondition 
      (in r59)
    :effect
      (and
        (in r58)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r59))
      )
    )
  (:action move-right-from-r60
    :parameters ()
    :precondition 
      (in r60)
    :effect
      (and
        (in r61)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r60))
      )
    )
  (:action move-left-from-r60
    :parameters ()
    :precondition 
      (in r60)
    :effect
      (and
        (in r59)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r60))
      )
    )
  (:action move-right-from-r61
    :parameters ()
    :precondition 
      (in r61)
    :effect
      (and
        (in r62)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r61))
      )
    )
  (:action move-left-from-r61
    :parameters ()
    :precondition 
      (in r61)
    :effect
      (and
        (in r60)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r61))
      )
    )
  (:action move-right-from-r62
    :parameters ()
    :precondition 
      (in r62)
    :effect
      (and
        (in r63)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r62))
      )
    )
  (:action move-left-from-r62
    :parameters ()
    :precondition 
      (in r62)
    :effect
      (and
        (in r61)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r62))
      )
    )
  (:action move-right-from-r63
    :parameters ()
    :precondition 
      (in r63)
    :effect
      (and
        (in r64)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r63))
      )
    )
  (:action move-left-from-r63
    :parameters ()
    :precondition 
      (in r63)
    :effect
      (and
        (in r62)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r63))
      )
    )
  (:action move-right-from-r64
    :parameters ()
    :precondition 
      (in r64)
    :effect
      (and
        (in r65)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r64))
      )
    )
  (:action move-left-from-r64
    :parameters ()
    :precondition 
      (in r64)
    :effect
      (and
        (in r63)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r64))
      )
    )
  (:action move-right-from-r65
    :parameters ()
    :precondition 
      (in r65)
    :effect
      (and
        (in r66)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r65))
      )
    )
  (:action move-left-from-r65
    :parameters ()
    :precondition 
      (in r65)
    :effect
      (and
        (in r64)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r65))
      )
    )
  (:action move-right-from-r66
    :parameters ()
    :precondition 
      (in r66)
    :effect
      (and
        (in r67)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r66))
      )
    )
  (:action move-left-from-r66
    :parameters ()
    :precondition 
      (in r66)
    :effect
      (and
        (in r65)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r66))
      )
    )
  (:action move-right-from-r67
    :parameters ()
    :precondition 
      (in r67)
    :effect
      (and
        (in r68)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r67))
      )
    )
  (:action move-left-from-r67
    :parameters ()
    :precondition 
      (in r67)
    :effect
      (and
        (in r66)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r67))
      )
    )
  (:action move-right-from-r68
    :parameters ()
    :precondition 
      (in r68)
    :effect
      (and
        (in r69)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r68))
      )
    )
  (:action move-left-from-r68
    :parameters ()
    :precondition 
      (in r68)
    :effect
      (and
        (in r67)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r68))
      )
    )
  (:action move-right-from-r69
    :parameters ()
    :precondition 
      (in r69)
    :effect
      (and
        (in r70)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r69))
      )
    )
  (:action move-left-from-r69
    :parameters ()
    :precondition 
      (in r69)
    :effect
      (and
        (in r68)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r69))
      )
    )
  (:action move-right-from-r70
    :parameters ()
    :precondition 
      (in r70)
    :effect
      (and
        (in r71)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r70))
      )
    )
  (:action move-left-from-r70
    :parameters ()
    :precondition 
      (in r70)
    :effect
      (and
        (in r69)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r70))
      )
    )
  (:action move-right-from-r71
    :parameters ()
    :precondition 
      (in r71)
    :effect
      (and
        (in r72)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r71))
      )
    )
  (:action move-left-from-r71
    :parameters ()
    :precondition 
      (in r71)
    :effect
      (and
        (in r70)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r71))
      )
    )
  (:action move-right-from-r72
    :parameters ()
    :precondition 
      (in r72)
    :effect
      (and
        (in r73)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r72))
      )
    )
  (:action move-left-from-r72
    :parameters ()
    :precondition 
      (in r72)
    :effect
      (and
        (in r71)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r72))
      )
    )
  (:action move-right-from-r73
    :parameters ()
    :precondition 
      (in r73)
    :effect
      (and
        (in r74)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r73))
      )
    )
  (:action move-left-from-r73
    :parameters ()
    :precondition 
      (in r73)
    :effect
      (and
        (in r72)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r73))
      )
    )
  (:action move-right-from-r74
    :parameters ()
    :precondition 
      (in r74)
    :effect
      (and
        (in r75)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r74))
      )
    )
  (:action move-left-from-r74
    :parameters ()
    :precondition 
      (in r74)
    :effect
      (and
        (in r73)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r74))
      )
    )
  (:action move-right-from-r75
    :parameters ()
    :precondition 
      (in r75)
    :effect
      (and
        (in r76)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r75))
      )
    )
  (:action move-left-from-r75
    :parameters ()
    :precondition 
      (in r75)
    :effect
      (and
        (in r74)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r75))
      )
    )
  (:action move-right-from-r76
    :parameters ()
    :precondition 
      (in r76)
    :effect
      (and
        (in r77)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r76))
      )
    )
  (:action move-left-from-r76
    :parameters ()
    :precondition 
      (in r76)
    :effect
      (and
        (in r75)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r76))
      )
    )
  (:action move-right-from-r77
    :parameters ()
    :precondition 
      (in r77)
    :effect
      (and
        (in r78)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r77))
      )
    )
  (:action move-left-from-r77
    :parameters ()
    :precondition 
      (in r77)
    :effect
      (and
        (in r76)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r77))
      )
    )
  (:action move-right-from-r78
    :parameters ()
    :precondition 
      (in r78)
    :effect
      (and
        (in r79)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r78))
      )
    )
  (:action move-left-from-r78
    :parameters ()
    :precondition 
      (in r78)
    :effect
      (and
        (in r77)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r78))
      )
    )
  (:action move-right-from-r79
    :parameters ()
    :precondition 
      (in r79)
    :effect
      (and
        (in r80)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r79))
      )
    )
  (:action move-left-from-r79
    :parameters ()
    :precondition 
      (in r79)
    :effect
      (and
        (in r78)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r79))
      )
    )
  (:action move-right-from-r80
    :parameters ()
    :precondition 
      (in r80)
    :effect
      (and
        (in r81)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r80))
      )
    )
  (:action move-left-from-r80
    :parameters ()
    :precondition 
      (in r80)
    :effect
      (and
        (in r79)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r80))
      )
    )
  (:action move-right-from-r81
    :parameters ()
    :precondition 
      (in r81)
    :effect
      (and
        (in r82)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r81))
      )
    )
  (:action move-left-from-r81
    :parameters ()
    :precondition 
      (in r81)
    :effect
      (and
        (in r80)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r81))
      )
    )
  (:action move-right-from-r82
    :parameters ()
    :precondition 
      (in r82)
    :effect
      (and
        (in r83)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r82))
      )
    )
  (:action move-left-from-r82
    :parameters ()
    :precondition 
      (in r82)
    :effect
      (and
        (in r81)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r82))
      )
    )
  (:action move-right-from-r83
    :parameters ()
    :precondition 
      (in r83)
    :effect
      (and
        (in r84)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r83))
      )
    )
  (:action move-left-from-r83
    :parameters ()
    :precondition 
      (in r83)
    :effect
      (and
        (in r82)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r83))
      )
    )
  (:action move-right-from-r84
    :parameters ()
    :precondition 
      (in r84)
    :effect
      (and
        (in r85)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r84))
      )
    )
  (:action move-left-from-r84
    :parameters ()
    :precondition 
      (in r84)
    :effect
      (and
        (in r83)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r84))
      )
    )
  (:action move-right-from-r85
    :parameters ()
    :precondition 
      (in r85)
    :effect
      (and
        (in r86)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r85))
      )
    )
  (:action move-left-from-r85
    :parameters ()
    :precondition 
      (in r85)
    :effect
      (and
        (in r84)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r85))
      )
    )
  (:action move-right-from-r86
    :parameters ()
    :precondition 
      (in r86)
    :effect
      (and
        (in r87)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r86))
      )
    )
  (:action move-left-from-r86
    :parameters ()
    :precondition 
      (in r86)
    :effect
      (and
        (in r85)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r86))
      )
    )
  (:action move-right-from-r87
    :parameters ()
    :precondition 
      (in r87)
    :effect
      (and
        (in r88)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r87))
      )
    )
  (:action move-left-from-r87
    :parameters ()
    :precondition 
      (in r87)
    :effect
      (and
        (in r86)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r87))
      )
    )
  (:action move-right-from-r88
    :parameters ()
    :precondition 
      (in r88)
    :effect
      (and
        (in r89)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r88))
      )
    )
  (:action move-left-from-r88
    :parameters ()
    :precondition 
      (in r88)
    :effect
      (and
        (in r87)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r88))
      )
    )
  (:action move-right-from-r89
    :parameters ()
    :precondition 
      (in r89)
    :effect
      (and
        (in r90)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r89))
      )
    )
  (:action move-left-from-r89
    :parameters ()
    :precondition 
      (in r89)
    :effect
      (and
        (in r88)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r89))
      )
    )
  (:action move-right-from-r90
    :parameters ()
    :precondition 
      (in r90)
    :effect
      (and
        (in r91)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r90))
      )
    )
  (:action move-left-from-r90
    :parameters ()
    :precondition 
      (in r90)
    :effect
      (and
        (in r89)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r90))
      )
    )
  (:action move-right-from-r91
    :parameters ()
    :precondition 
      (in r91)
    :effect
      (and
        (in r92)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r91))
      )
    )
  (:action move-left-from-r91
    :parameters ()
    :precondition 
      (in r91)
    :effect
      (and
        (in r90)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r91))
      )
    )
  (:action move-right-from-r92
    :parameters ()
    :precondition 
      (in r92)
    :effect
      (and
        (in r93)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r92))
      )
    )
  (:action move-left-from-r92
    :parameters ()
    :precondition 
      (in r92)
    :effect
      (and
        (in r91)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r92))
      )
    )
  (:action move-right-from-r93
    :parameters ()
    :precondition 
      (in r93)
    :effect
      (and
        (in r94)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r93))
      )
    )
  (:action move-left-from-r93
    :parameters ()
    :precondition 
      (in r93)
    :effect
      (and
        (in r92)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r93))
      )
    )
  (:action move-right-from-r94
    :parameters ()
    :precondition 
      (in r94)
    :effect
      (and
        (in r95)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r94))
      )
    )
  (:action move-left-from-r94
    :parameters ()
    :precondition 
      (in r94)
    :effect
      (and
        (in r93)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r94))
      )
    )
  (:action move-right-from-r95
    :parameters ()
    :precondition 
      (in r95)
    :effect
      (and
        (in r96)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r95))
      )
    )
  (:action move-left-from-r95
    :parameters ()
    :precondition 
      (in r95)
    :effect
      (and
        (in r94)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r95))
      )
    )
  (:action move-right-from-r96
    :parameters ()
    :precondition 
      (in r96)
    :effect
      (and
        (in r97)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r96))
      )
    )
  (:action move-left-from-r96
    :parameters ()
    :precondition 
      (in r96)
    :effect
      (and
        (in r95)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r96))
      )
    )
  (:action move-right-from-r97
    :parameters ()
    :precondition 
      (in r97)
    :effect
      (and
        (in r98)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r97))
      )
    )
  (:action move-left-from-r97
    :parameters ()
    :precondition 
      (in r97)
    :effect
      (and
        (in r96)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r97))
      )
    )
  (:action move-right-from-r98
    :parameters ()
    :precondition 
      (in r98)
    :effect
      (and
        (in r99)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r98))
      )
    )
  (:action move-left-from-r98
    :parameters ()
    :precondition 
      (in r98)
    :effect
      (and
        (in r97)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r98))
      )
    )
  (:action move-right-from-r99_detdup_0
    :parameters ()
    :precondition 
      (in r99)
    :effect
      (and
        (in r100)
        (not 
          (in r99))
        (not 
          (seen))
      )
    )
  (:action move-right-from-r99_detdup_1
    :parameters ()
    :precondition 
      (in r99)
    :effect
      (and
        (in r100)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r99))
      )
    )
  (:action move-left-from-r99
    :parameters ()
    :precondition 
      (in r99)
    :effect
      (and
        (in r98)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r99))
      )
    )
  (:action move-right-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen)))
    :effect
      (and
        (in r101)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r100))
      )
    )
  (:action move-left-from-r100
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen)))
    :effect
      (and
        (in r99)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r100))
      )
    )
  (:action move-right-from-r101
    :parameters ()
    :precondition 
      (in r101)
    :effect
      (and
        (in r102)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r101))
      )
    )
  (:action move-left-from-r101_detdup_0
    :parameters ()
    :precondition 
      (in r101)
    :effect
      (and
        (in r100)
        (not 
          (in r101))
        (not 
          (seen))
      )
    )
  (:action move-left-from-r101_detdup_1
    :parameters ()
    :precondition 
      (in r101)
    :effect
      (and
        (in r100)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r101))
      )
    )
  (:action move-right-from-r102
    :parameters ()
    :precondition 
      (in r102)
    :effect
      (and
        (in r103)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r102))
      )
    )
  (:action move-left-from-r102
    :parameters ()
    :precondition 
      (in r102)
    :effect
      (and
        (in r101)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r102))
      )
    )
  (:action move-right-from-r103
    :parameters ()
    :precondition 
      (in r103)
    :effect
      (and
        (in r104)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r103))
      )
    )
  (:action move-left-from-r103
    :parameters ()
    :precondition 
      (in r103)
    :effect
      (and
        (in r102)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r103))
      )
    )
  (:action move-right-from-r104
    :parameters ()
    :precondition 
      (in r104)
    :effect
      (and
        (in r105)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r104))
      )
    )
  (:action move-left-from-r104
    :parameters ()
    :precondition 
      (in r104)
    :effect
      (and
        (in r103)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r104))
      )
    )
  (:action move-right-from-r105
    :parameters ()
    :precondition 
      (in r105)
    :effect
      (and
        (in r106)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r105))
      )
    )
  (:action move-left-from-r105
    :parameters ()
    :precondition 
      (in r105)
    :effect
      (and
        (in r104)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r105))
      )
    )
  (:action move-right-from-r106
    :parameters ()
    :precondition 
      (in r106)
    :effect
      (and
        (in r107)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r106))
      )
    )
  (:action move-left-from-r106
    :parameters ()
    :precondition 
      (in r106)
    :effect
      (and
        (in r105)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r106))
      )
    )
  (:action move-right-from-r107
    :parameters ()
    :precondition 
      (in r107)
    :effect
      (and
        (in r108)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r107))
      )
    )
  (:action move-left-from-r107
    :parameters ()
    :precondition 
      (in r107)
    :effect
      (and
        (in r106)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r107))
      )
    )
  (:action move-right-from-r108
    :parameters ()
    :precondition 
      (in r108)
    :effect
      (and
        (in r109)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r108))
      )
    )
  (:action move-left-from-r108
    :parameters ()
    :precondition 
      (in r108)
    :effect
      (and
        (in r107)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r108))
      )
    )
  (:action move-right-from-r109
    :parameters ()
    :precondition 
      (in r109)
    :effect
      (and
        (in r110)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r109))
      )
    )
  (:action move-left-from-r109
    :parameters ()
    :precondition 
      (in r109)
    :effect
      (and
        (in r108)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r109))
      )
    )
  (:action move-right-from-r110
    :parameters ()
    :precondition 
      (in r110)
    :effect
      (and
        (in r111)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r110))
      )
    )
  (:action move-left-from-r110
    :parameters ()
    :precondition 
      (in r110)
    :effect
      (and
        (in r109)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r110))
      )
    )
  (:action move-right-from-r111
    :parameters ()
    :precondition 
      (in r111)
    :effect
      (and
        (in r112)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r111))
      )
    )
  (:action move-left-from-r111
    :parameters ()
    :precondition 
      (in r111)
    :effect
      (and
        (in r110)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r111))
      )
    )
  (:action move-right-from-r112
    :parameters ()
    :precondition 
      (in r112)
    :effect
      (and
        (in r113)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r112))
      )
    )
  (:action move-left-from-r112
    :parameters ()
    :precondition 
      (in r112)
    :effect
      (and
        (in r111)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r112))
      )
    )
  (:action move-right-from-r113
    :parameters ()
    :precondition 
      (in r113)
    :effect
      (and
        (in r114)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r113))
      )
    )
  (:action move-left-from-r113
    :parameters ()
    :precondition 
      (in r113)
    :effect
      (and
        (in r112)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r113))
      )
    )
  (:action move-right-from-r114
    :parameters ()
    :precondition 
      (in r114)
    :effect
      (and
        (in r115)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r114))
      )
    )
  (:action move-left-from-r114
    :parameters ()
    :precondition 
      (in r114)
    :effect
      (and
        (in r113)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r114))
      )
    )
  (:action move-right-from-r115
    :parameters ()
    :precondition 
      (in r115)
    :effect
      (and
        (in r116)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r115))
      )
    )
  (:action move-left-from-r115
    :parameters ()
    :precondition 
      (in r115)
    :effect
      (and
        (in r114)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r115))
      )
    )
  (:action move-right-from-r116
    :parameters ()
    :precondition 
      (in r116)
    :effect
      (and
        (in r117)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r116))
      )
    )
  (:action move-left-from-r116
    :parameters ()
    :precondition 
      (in r116)
    :effect
      (and
        (in r115)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r116))
      )
    )
  (:action move-right-from-r117
    :parameters ()
    :precondition 
      (in r117)
    :effect
      (and
        (in r118)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r117))
      )
    )
  (:action move-left-from-r117
    :parameters ()
    :precondition 
      (in r117)
    :effect
      (and
        (in r116)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r117))
      )
    )
  (:action move-right-from-r118
    :parameters ()
    :precondition 
      (in r118)
    :effect
      (and
        (in r119)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r118))
      )
    )
  (:action move-left-from-r118
    :parameters ()
    :precondition 
      (in r118)
    :effect
      (and
        (in r117)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r118))
      )
    )
  (:action move-right-from-r119
    :parameters ()
    :precondition 
      (in r119)
    :effect
      (and
        (in r120)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r119))
      )
    )
  (:action move-left-from-r119
    :parameters ()
    :precondition 
      (in r119)
    :effect
      (and
        (in r118)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r119))
      )
    )
  (:action move-right-from-r120
    :parameters ()
    :precondition 
      (in r120)
    :effect
      (and
        (in r121)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r120))
      )
    )
  (:action move-left-from-r120
    :parameters ()
    :precondition 
      (in r120)
    :effect
      (and
        (in r119)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r120))
      )
    )
  (:action move-right-from-r121
    :parameters ()
    :precondition 
      (in r121)
    :effect
      (and
        (in r122)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r121))
      )
    )
  (:action move-left-from-r121
    :parameters ()
    :precondition 
      (in r121)
    :effect
      (and
        (in r120)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r121))
      )
    )
  (:action move-right-from-r122
    :parameters ()
    :precondition 
      (in r122)
    :effect
      (and
        (in r123)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r122))
      )
    )
  (:action move-left-from-r122
    :parameters ()
    :precondition 
      (in r122)
    :effect
      (and
        (in r121)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r122))
      )
    )
  (:action move-right-from-r123
    :parameters ()
    :precondition 
      (in r123)
    :effect
      (and
        (in r124)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r123))
      )
    )
  (:action move-left-from-r123
    :parameters ()
    :precondition 
      (in r123)
    :effect
      (and
        (in r122)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r123))
      )
    )
  (:action move-right-from-r124
    :parameters ()
    :precondition 
      (in r124)
    :effect
      (and
        (in r125)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r124))
      )
    )
  (:action move-left-from-r124
    :parameters ()
    :precondition 
      (in r124)
    :effect
      (and
        (in r123)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r124))
      )
    )
  (:action move-right-from-r125
    :parameters ()
    :precondition 
      (in r125)
    :effect
      (and
        (in r126)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r125))
      )
    )
  (:action move-left-from-r125
    :parameters ()
    :precondition 
      (in r125)
    :effect
      (and
        (in r124)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r125))
      )
    )
  (:action move-right-from-r126
    :parameters ()
    :precondition 
      (in r126)
    :effect
      (and
        (in r127)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r126))
      )
    )
  (:action move-left-from-r126
    :parameters ()
    :precondition 
      (in r126)
    :effect
      (and
        (in r125)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r126))
      )
    )
  (:action move-right-from-r127
    :parameters ()
    :precondition 
      (in r127)
    :effect
      (and
        (in r128)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r127))
      )
    )
  (:action move-left-from-r127
    :parameters ()
    :precondition 
      (in r127)
    :effect
      (and
        (in r126)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r127))
      )
    )
  (:action move-right-from-r128
    :parameters ()
    :precondition 
      (in r128)
    :effect
      (and
        (in r129)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r128))
      )
    )
  (:action move-left-from-r128
    :parameters ()
    :precondition 
      (in r128)
    :effect
      (and
        (in r127)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r128))
      )
    )
  (:action move-right-from-r129
    :parameters ()
    :precondition 
      (in r129)
    :effect
      (and
        (in r130)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r129))
      )
    )
  (:action move-left-from-r129
    :parameters ()
    :precondition 
      (in r129)
    :effect
      (and
        (in r128)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r129))
      )
    )
  (:action move-right-from-r130
    :parameters ()
    :precondition 
      (in r130)
    :effect
      (and
        (in r131)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r130))
      )
    )
  (:action move-left-from-r130
    :parameters ()
    :precondition 
      (in r130)
    :effect
      (and
        (in r129)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r130))
      )
    )
  (:action move-right-from-r131
    :parameters ()
    :precondition 
      (in r131)
    :effect
      (and
        (in r132)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r131))
      )
    )
  (:action move-left-from-r131
    :parameters ()
    :precondition 
      (in r131)
    :effect
      (and
        (in r130)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r131))
      )
    )
  (:action move-right-from-r132
    :parameters ()
    :precondition 
      (in r132)
    :effect
      (and
        (in r133)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r132))
      )
    )
  (:action move-left-from-r132
    :parameters ()
    :precondition 
      (in r132)
    :effect
      (and
        (in r131)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r132))
      )
    )
  (:action move-right-from-r133
    :parameters ()
    :precondition 
      (in r133)
    :effect
      (and
        (in r134)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r133))
      )
    )
  (:action move-left-from-r133
    :parameters ()
    :precondition 
      (in r133)
    :effect
      (and
        (in r132)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r133))
      )
    )
  (:action move-right-from-r134
    :parameters ()
    :precondition 
      (in r134)
    :effect
      (and
        (in r135)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r134))
      )
    )
  (:action move-left-from-r134
    :parameters ()
    :precondition 
      (in r134)
    :effect
      (and
        (in r133)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r134))
      )
    )
  (:action move-right-from-r135
    :parameters ()
    :precondition 
      (in r135)
    :effect
      (and
        (in r136)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r135))
      )
    )
  (:action move-left-from-r135
    :parameters ()
    :precondition 
      (in r135)
    :effect
      (and
        (in r134)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r135))
      )
    )
  (:action move-right-from-r136
    :parameters ()
    :precondition 
      (in r136)
    :effect
      (and
        (in r137)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r136))
      )
    )
  (:action move-left-from-r136
    :parameters ()
    :precondition 
      (in r136)
    :effect
      (and
        (in r135)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r136))
      )
    )
  (:action move-right-from-r137
    :parameters ()
    :precondition 
      (in r137)
    :effect
      (and
        (in r138)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r137))
      )
    )
  (:action move-left-from-r137
    :parameters ()
    :precondition 
      (in r137)
    :effect
      (and
        (in r136)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r137))
      )
    )
  (:action move-right-from-r138
    :parameters ()
    :precondition 
      (in r138)
    :effect
      (and
        (in r139)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r138))
      )
    )
  (:action move-left-from-r138
    :parameters ()
    :precondition 
      (in r138)
    :effect
      (and
        (in r137)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r138))
      )
    )
  (:action move-right-from-r139
    :parameters ()
    :precondition 
      (in r139)
    :effect
      (and
        (in r140)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r139))
      )
    )
  (:action move-left-from-r139
    :parameters ()
    :precondition 
      (in r139)
    :effect
      (and
        (in r138)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r139))
      )
    )
  (:action move-right-from-r140
    :parameters ()
    :precondition 
      (in r140)
    :effect
      (and
        (in r141)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r140))
      )
    )
  (:action move-left-from-r140
    :parameters ()
    :precondition 
      (in r140)
    :effect
      (and
        (in r139)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r140))
      )
    )
  (:action move-right-from-r141
    :parameters ()
    :precondition 
      (in r141)
    :effect
      (and
        (in r142)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r141))
      )
    )
  (:action move-left-from-r141
    :parameters ()
    :precondition 
      (in r141)
    :effect
      (and
        (in r140)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r141))
      )
    )
  (:action move-right-from-r142
    :parameters ()
    :precondition 
      (in r142)
    :effect
      (and
        (in r143)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r142))
      )
    )
  (:action move-left-from-r142
    :parameters ()
    :precondition 
      (in r142)
    :effect
      (and
        (in r141)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r142))
      )
    )
  (:action move-right-from-r143
    :parameters ()
    :precondition 
      (in r143)
    :effect
      (and
        (in r144)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r143))
      )
    )
  (:action move-left-from-r143
    :parameters ()
    :precondition 
      (in r143)
    :effect
      (and
        (in r142)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r143))
      )
    )
  (:action move-right-from-r144
    :parameters ()
    :precondition 
      (in r144)
    :effect
      (and
        (in r145)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r144))
      )
    )
  (:action move-left-from-r144
    :parameters ()
    :precondition 
      (in r144)
    :effect
      (and
        (in r143)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r144))
      )
    )
  (:action move-right-from-r145
    :parameters ()
    :precondition 
      (in r145)
    :effect
      (and
        (in r146)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r145))
      )
    )
  (:action move-left-from-r145
    :parameters ()
    :precondition 
      (in r145)
    :effect
      (and
        (in r144)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r145))
      )
    )
  (:action move-right-from-r146
    :parameters ()
    :precondition 
      (in r146)
    :effect
      (and
        (in r147)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r146))
      )
    )
  (:action move-left-from-r146
    :parameters ()
    :precondition 
      (in r146)
    :effect
      (and
        (in r145)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r146))
      )
    )
  (:action move-right-from-r147
    :parameters ()
    :precondition 
      (in r147)
    :effect
      (and
        (in r148)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r147))
      )
    )
  (:action move-left-from-r147
    :parameters ()
    :precondition 
      (in r147)
    :effect
      (and
        (in r146)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r147))
      )
    )
  (:action move-right-from-r148
    :parameters ()
    :precondition 
      (in r148)
    :effect
      (and
        (in r149)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r148))
      )
    )
  (:action move-left-from-r148
    :parameters ()
    :precondition 
      (in r148)
    :effect
      (and
        (in r147)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r148))
      )
    )
  (:action move-right-from-r149
    :parameters ()
    :precondition 
      (in r149)
    :effect
      (and
        (in r150)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r149))
      )
    )
  (:action move-left-from-r149
    :parameters ()
    :precondition 
      (in r149)
    :effect
      (and
        (in r148)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r149))
      )
    )
  (:action move-right-from-r150
    :parameters ()
    :precondition 
      (in r150)
    :effect
      (and
        (in r151)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r150))
      )
    )
  (:action move-left-from-r150
    :parameters ()
    :precondition 
      (in r150)
    :effect
      (and
        (in r149)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r150))
      )
    )
  (:action move-right-from-r151
    :parameters ()
    :precondition 
      (in r151)
    :effect
      (and
        (in r152)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r151))
      )
    )
  (:action move-left-from-r151
    :parameters ()
    :precondition 
      (in r151)
    :effect
      (and
        (in r150)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r151))
      )
    )
  (:action move-right-from-r152
    :parameters ()
    :precondition 
      (in r152)
    :effect
      (and
        (in r153)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r152))
      )
    )
  (:action move-left-from-r152
    :parameters ()
    :precondition 
      (in r152)
    :effect
      (and
        (in r151)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r152))
      )
    )
  (:action move-right-from-r153
    :parameters ()
    :precondition 
      (in r153)
    :effect
      (and
        (in r154)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r153))
      )
    )
  (:action move-left-from-r153
    :parameters ()
    :precondition 
      (in r153)
    :effect
      (and
        (in r152)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r153))
      )
    )
  (:action move-right-from-r154
    :parameters ()
    :precondition 
      (in r154)
    :effect
      (and
        (in r155)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r154))
      )
    )
  (:action move-left-from-r154
    :parameters ()
    :precondition 
      (in r154)
    :effect
      (and
        (in r153)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r154))
      )
    )
  (:action move-right-from-r155
    :parameters ()
    :precondition 
      (in r155)
    :effect
      (and
        (in r156)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r155))
      )
    )
  (:action move-left-from-r155
    :parameters ()
    :precondition 
      (in r155)
    :effect
      (and
        (in r154)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r155))
      )
    )
  (:action move-right-from-r156
    :parameters ()
    :precondition 
      (in r156)
    :effect
      (and
        (in r157)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r156))
      )
    )
  (:action move-left-from-r156
    :parameters ()
    :precondition 
      (in r156)
    :effect
      (and
        (in r155)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r156))
      )
    )
  (:action move-right-from-r157
    :parameters ()
    :precondition 
      (in r157)
    :effect
      (and
        (in r158)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r157))
      )
    )
  (:action move-left-from-r157
    :parameters ()
    :precondition 
      (in r157)
    :effect
      (and
        (in r156)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r157))
      )
    )
  (:action move-right-from-r158
    :parameters ()
    :precondition 
      (in r158)
    :effect
      (and
        (in r159)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r158))
      )
    )
  (:action move-left-from-r158
    :parameters ()
    :precondition 
      (in r158)
    :effect
      (and
        (in r157)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r158))
      )
    )
  (:action move-right-from-r159
    :parameters ()
    :precondition 
      (in r159)
    :effect
      (and
        (in r160)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r159))
      )
    )
  (:action move-left-from-r159
    :parameters ()
    :precondition 
      (in r159)
    :effect
      (and
        (in r158)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r159))
      )
    )
  (:action move-right-from-r160
    :parameters ()
    :precondition 
      (in r160)
    :effect
      (and
        (in r161)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r160))
      )
    )
  (:action move-left-from-r160
    :parameters ()
    :precondition 
      (in r160)
    :effect
      (and
        (in r159)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r160))
      )
    )
  (:action move-right-from-r161
    :parameters ()
    :precondition 
      (in r161)
    :effect
      (and
        (in r162)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r161))
      )
    )
  (:action move-left-from-r161
    :parameters ()
    :precondition 
      (in r161)
    :effect
      (and
        (in r160)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r161))
      )
    )
  (:action move-right-from-r162
    :parameters ()
    :precondition 
      (in r162)
    :effect
      (and
        (in r163)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r162))
      )
    )
  (:action move-left-from-r162
    :parameters ()
    :precondition 
      (in r162)
    :effect
      (and
        (in r161)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r162))
      )
    )
  (:action move-right-from-r163
    :parameters ()
    :precondition 
      (in r163)
    :effect
      (and
        (in r164)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r163))
      )
    )
  (:action move-left-from-r163
    :parameters ()
    :precondition 
      (in r163)
    :effect
      (and
        (in r162)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r163))
      )
    )
  (:action move-right-from-r164
    :parameters ()
    :precondition 
      (in r164)
    :effect
      (and
        (in r165)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r164))
      )
    )
  (:action move-left-from-r164
    :parameters ()
    :precondition 
      (in r164)
    :effect
      (and
        (in r163)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r164))
      )
    )
  (:action move-right-from-r165
    :parameters ()
    :precondition 
      (in r165)
    :effect
      (and
        (in r166)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r165))
      )
    )
  (:action move-left-from-r165
    :parameters ()
    :precondition 
      (in r165)
    :effect
      (and
        (in r164)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r165))
      )
    )
  (:action move-right-from-r166
    :parameters ()
    :precondition 
      (in r166)
    :effect
      (and
        (in r167)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r166))
      )
    )
  (:action move-left-from-r166
    :parameters ()
    :precondition 
      (in r166)
    :effect
      (and
        (in r165)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r166))
      )
    )
  (:action move-right-from-r167
    :parameters ()
    :precondition 
      (in r167)
    :effect
      (and
        (in r168)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r167))
      )
    )
  (:action move-left-from-r167
    :parameters ()
    :precondition 
      (in r167)
    :effect
      (and
        (in r166)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r167))
      )
    )
  (:action move-right-from-r168
    :parameters ()
    :precondition 
      (in r168)
    :effect
      (and
        (in r169)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r168))
      )
    )
  (:action move-left-from-r168
    :parameters ()
    :precondition 
      (in r168)
    :effect
      (and
        (in r167)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r168))
      )
    )
  (:action move-right-from-r169
    :parameters ()
    :precondition 
      (in r169)
    :effect
      (and
        (in r170)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r169))
      )
    )
  (:action move-left-from-r169
    :parameters ()
    :precondition 
      (in r169)
    :effect
      (and
        (in r168)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r169))
      )
    )
  (:action move-right-from-r170
    :parameters ()
    :precondition 
      (in r170)
    :effect
      (and
        (in r171)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r170))
      )
    )
  (:action move-left-from-r170
    :parameters ()
    :precondition 
      (in r170)
    :effect
      (and
        (in r169)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r170))
      )
    )
  (:action move-right-from-r171
    :parameters ()
    :precondition 
      (in r171)
    :effect
      (and
        (in r172)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r171))
      )
    )
  (:action move-left-from-r171
    :parameters ()
    :precondition 
      (in r171)
    :effect
      (and
        (in r170)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r171))
      )
    )
  (:action move-right-from-r172
    :parameters ()
    :precondition 
      (in r172)
    :effect
      (and
        (in r173)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r172))
      )
    )
  (:action move-left-from-r172
    :parameters ()
    :precondition 
      (in r172)
    :effect
      (and
        (in r171)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r172))
      )
    )
  (:action move-right-from-r173
    :parameters ()
    :precondition 
      (in r173)
    :effect
      (and
        (in r174)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r173))
      )
    )
  (:action move-left-from-r173
    :parameters ()
    :precondition 
      (in r173)
    :effect
      (and
        (in r172)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r173))
      )
    )
  (:action move-right-from-r174
    :parameters ()
    :precondition 
      (in r174)
    :effect
      (and
        (in r175)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r174))
      )
    )
  (:action move-left-from-r174
    :parameters ()
    :precondition 
      (in r174)
    :effect
      (and
        (in r173)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r174))
      )
    )
  (:action move-right-from-r175
    :parameters ()
    :precondition 
      (in r175)
    :effect
      (and
        (in r176)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r175))
      )
    )
  (:action move-left-from-r175
    :parameters ()
    :precondition 
      (in r175)
    :effect
      (and
        (in r174)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r175))
      )
    )
  (:action move-right-from-r176
    :parameters ()
    :precondition 
      (in r176)
    :effect
      (and
        (in r177)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r176))
      )
    )
  (:action move-left-from-r176
    :parameters ()
    :precondition 
      (in r176)
    :effect
      (and
        (in r175)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r176))
      )
    )
  (:action move-right-from-r177
    :parameters ()
    :precondition 
      (in r177)
    :effect
      (and
        (in r178)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r177))
      )
    )
  (:action move-left-from-r177
    :parameters ()
    :precondition 
      (in r177)
    :effect
      (and
        (in r176)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r177))
      )
    )
  (:action move-right-from-r178
    :parameters ()
    :precondition 
      (in r178)
    :effect
      (and
        (in r179)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r178))
      )
    )
  (:action move-left-from-r178
    :parameters ()
    :precondition 
      (in r178)
    :effect
      (and
        (in r177)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r178))
      )
    )
  (:action move-right-from-r179
    :parameters ()
    :precondition 
      (in r179)
    :effect
      (and
        (in r180)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r179))
      )
    )
  (:action move-left-from-r179
    :parameters ()
    :precondition 
      (in r179)
    :effect
      (and
        (in r178)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r179))
      )
    )
  (:action move-right-from-r180
    :parameters ()
    :precondition 
      (in r180)
    :effect
      (and
        (in r181)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r180))
      )
    )
  (:action move-left-from-r180
    :parameters ()
    :precondition 
      (in r180)
    :effect
      (and
        (in r179)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r180))
      )
    )
  (:action move-right-from-r181
    :parameters ()
    :precondition 
      (in r181)
    :effect
      (and
        (in r182)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r181))
      )
    )
  (:action move-left-from-r181
    :parameters ()
    :precondition 
      (in r181)
    :effect
      (and
        (in r180)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r181))
      )
    )
  (:action move-right-from-r182
    :parameters ()
    :precondition 
      (in r182)
    :effect
      (and
        (in r183)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r182))
      )
    )
  (:action move-left-from-r182
    :parameters ()
    :precondition 
      (in r182)
    :effect
      (and
        (in r181)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r182))
      )
    )
  (:action move-right-from-r183
    :parameters ()
    :precondition 
      (in r183)
    :effect
      (and
        (in r184)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r183))
      )
    )
  (:action move-left-from-r183
    :parameters ()
    :precondition 
      (in r183)
    :effect
      (and
        (in r182)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r183))
      )
    )
  (:action move-right-from-r184
    :parameters ()
    :precondition 
      (in r184)
    :effect
      (and
        (in r185)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r184))
      )
    )
  (:action move-left-from-r184
    :parameters ()
    :precondition 
      (in r184)
    :effect
      (and
        (in r183)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r184))
      )
    )
  (:action move-right-from-r185
    :parameters ()
    :precondition 
      (in r185)
    :effect
      (and
        (in r186)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r185))
      )
    )
  (:action move-left-from-r185
    :parameters ()
    :precondition 
      (in r185)
    :effect
      (and
        (in r184)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r185))
      )
    )
  (:action move-right-from-r186
    :parameters ()
    :precondition 
      (in r186)
    :effect
      (and
        (in r187)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r186))
      )
    )
  (:action move-left-from-r186
    :parameters ()
    :precondition 
      (in r186)
    :effect
      (and
        (in r185)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r186))
      )
    )
  (:action move-right-from-r187
    :parameters ()
    :precondition 
      (in r187)
    :effect
      (and
        (in r188)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r187))
      )
    )
  (:action move-left-from-r187
    :parameters ()
    :precondition 
      (in r187)
    :effect
      (and
        (in r186)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r187))
      )
    )
  (:action move-right-from-r188
    :parameters ()
    :precondition 
      (in r188)
    :effect
      (and
        (in r189)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r188))
      )
    )
  (:action move-left-from-r188
    :parameters ()
    :precondition 
      (in r188)
    :effect
      (and
        (in r187)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r188))
      )
    )
  (:action move-right-from-r189
    :parameters ()
    :precondition 
      (in r189)
    :effect
      (and
        (in r190)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r189))
      )
    )
  (:action move-left-from-r189
    :parameters ()
    :precondition 
      (in r189)
    :effect
      (and
        (in r188)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r189))
      )
    )
  (:action move-right-from-r190
    :parameters ()
    :precondition 
      (in r190)
    :effect
      (and
        (in r191)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r190))
      )
    )
  (:action move-left-from-r190
    :parameters ()
    :precondition 
      (in r190)
    :effect
      (and
        (in r189)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r190))
      )
    )
  (:action move-right-from-r191
    :parameters ()
    :precondition 
      (in r191)
    :effect
      (and
        (in r192)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r191))
      )
    )
  (:action move-left-from-r191
    :parameters ()
    :precondition 
      (in r191)
    :effect
      (and
        (in r190)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r191))
      )
    )
  (:action move-right-from-r192
    :parameters ()
    :precondition 
      (in r192)
    :effect
      (and
        (in r193)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r192))
      )
    )
  (:action move-left-from-r192
    :parameters ()
    :precondition 
      (in r192)
    :effect
      (and
        (in r191)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r192))
      )
    )
  (:action move-right-from-r193
    :parameters ()
    :precondition 
      (in r193)
    :effect
      (and
        (in r194)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r193))
      )
    )
  (:action move-left-from-r193
    :parameters ()
    :precondition 
      (in r193)
    :effect
      (and
        (in r192)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r193))
      )
    )
  (:action move-right-from-r194
    :parameters ()
    :precondition 
      (in r194)
    :effect
      (and
        (in r195)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r194))
      )
    )
  (:action move-left-from-r194
    :parameters ()
    :precondition 
      (in r194)
    :effect
      (and
        (in r193)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r194))
      )
    )
  (:action move-right-from-r195
    :parameters ()
    :precondition 
      (in r195)
    :effect
      (and
        (in r196)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r195))
      )
    )
  (:action move-left-from-r195
    :parameters ()
    :precondition 
      (in r195)
    :effect
      (and
        (in r194)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r195))
      )
    )
  (:action move-right-from-r196
    :parameters ()
    :precondition 
      (in r196)
    :effect
      (and
        (in r197)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r196))
      )
    )
  (:action move-left-from-r196
    :parameters ()
    :precondition 
      (in r196)
    :effect
      (and
        (in r195)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r196))
      )
    )
  (:action move-right-from-r197
    :parameters ()
    :precondition 
      (in r197)
    :effect
      (and
        (in r198)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r197))
      )
    )
  (:action move-left-from-r197
    :parameters ()
    :precondition 
      (in r197)
    :effect
      (and
        (in r196)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r197))
      )
    )
  (:action move-right-from-r198
    :parameters ()
    :precondition 
      (in r198)
    :effect
      (and
        (in r199)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r198))
      )
    )
  (:action move-left-from-r198
    :parameters ()
    :precondition 
      (in r198)
    :effect
      (and
        (in r197)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r198))
      )
    )
  (:action move-right-from-r199_detdup_0
    :parameters ()
    :precondition 
      (in r199)
    :effect
      (and
        (in r200)
        (not 
          (in r199))
        (not 
          (seen))
      )
    )
  (:action move-right-from-r199_detdup_1
    :parameters ()
    :precondition 
      (in r199)
    :effect
      (and
        (in r200)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r199))
      )
    )
  (:action move-left-from-r199
    :parameters ()
    :precondition 
      (in r199)
    :effect
      (and
        (in r198)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r199))
      )
    )
  (:action move-right-from-r200
    :parameters ()
    :precondition 
      (and
        (in r200)
        (not 
          (seen)))
    :effect
      (and
        (in r1)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r200))
      )
    )
  (:action move-left-from-r200
    :parameters ()
    :precondition 
      (and
        (in r200)
        (not 
          (seen)))
    :effect
      (and
        (in r199)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r200))
      )
    )
  (:action stay
    :parameters ()
    :precondition 
      (and
        (not 
          (in r100))
        (not 
          (in r200)))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r100-seen
    :parameters ()
    :precondition 
      (and
        (in r100)
        (seen))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r100-not-seen_detdup_0
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen)))
    :effect
(not 
        (seen))    )
  (:action stay-in-r100-not-seen_detdup_1
    :parameters ()
    :precondition 
      (and
        (in r100)
        (not 
          (seen)))
    :effect
      (and
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
      )
    )
  (:action stay-in-r200-seen
    :parameters ()
    :precondition 
      (and
        (in r200)
        (seen))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r200-not-seen_detdup_0
    :parameters ()
    :precondition 
      (and
        (in r200)
        (not 
          (seen)))
    :effect
(not 
        (seen))    )
  (:action stay-in-r200-not-seen_detdup_1
    :parameters ()
    :precondition 
      (and
        (in r200)
        (not 
          (seen)))
    :effect
      (and
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
      )
    )
)