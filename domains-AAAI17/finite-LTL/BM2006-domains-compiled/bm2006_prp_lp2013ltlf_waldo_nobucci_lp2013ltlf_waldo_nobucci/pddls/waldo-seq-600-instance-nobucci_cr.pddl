(define (domain waldo)
  (:types
    room - none
  )

  (:predicates
    (seen)
    (in ?x0 - room)
    (autstate_1_2)
    (autstate_1_1)
  )
  (:action move-right-from-r1
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r2)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r1))
      )
    )
  (:action move-left-from-r1_detdup_0
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r600)
        (not 
          (in r1))
        (not 
          (seen))
      )
    )
  (:action move-left-from-r1_detdup_1
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r600)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r1))
      )
    )
  (:action move-right-from-r2
    :parameters ()
    :precondition 
      (in r2)
    :effect
      (and
        (in r3)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r2))
      )
    )
  (:action move-left-from-r2
    :parameters ()
    :precondition 
      (in r2)
    :effect
      (and
        (in r1)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r2))
      )
    )
  (:action move-right-from-r3
    :parameters ()
    :precondition 
      (in r3)
    :effect
      (and
        (in r4)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r3))
      )
    )
  (:action move-left-from-r3
    :parameters ()
    :precondition 
      (in r3)
    :effect
      (and
        (in r2)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r3))
      )
    )
  (:action move-right-from-r4
    :parameters ()
    :precondition 
      (in r4)
    :effect
      (and
        (in r5)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r4))
      )
    )
  (:action move-left-from-r4
    :parameters ()
    :precondition 
      (in r4)
    :effect
      (and
        (in r3)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r4))
      )
    )
  (:action move-right-from-r5
    :parameters ()
    :precondition 
      (in r5)
    :effect
      (and
        (in r6)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r5))
      )
    )
  (:action move-left-from-r5
    :parameters ()
    :precondition 
      (in r5)
    :effect
      (and
        (in r4)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r5))
      )
    )
  (:action move-right-from-r6
    :parameters ()
    :precondition 
      (in r6)
    :effect
      (and
        (in r7)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r6))
      )
    )
  (:action move-left-from-r6
    :parameters ()
    :precondition 
      (in r6)
    :effect
      (and
        (in r5)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r6))
      )
    )
  (:action move-right-from-r7
    :parameters ()
    :precondition 
      (in r7)
    :effect
      (and
        (in r8)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r7))
      )
    )
  (:action move-left-from-r7
    :parameters ()
    :precondition 
      (in r7)
    :effect
      (and
        (in r6)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r7))
      )
    )
  (:action move-right-from-r8
    :parameters ()
    :precondition 
      (in r8)
    :effect
      (and
        (in r9)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r8))
      )
    )
  (:action move-left-from-r8
    :parameters ()
    :precondition 
      (in r8)
    :effect
      (and
        (in r7)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r8))
      )
    )
  (:action move-right-from-r9
    :parameters ()
    :precondition 
      (in r9)
    :effect
      (and
        (in r10)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r9))
      )
    )
  (:action move-left-from-r9
    :parameters ()
    :precondition 
      (in r9)
    :effect
      (and
        (in r8)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r9))
      )
    )
  (:action move-right-from-r10
    :parameters ()
    :precondition 
      (in r10)
    :effect
      (and
        (in r11)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r10))
      )
    )
  (:action move-left-from-r10
    :parameters ()
    :precondition 
      (in r10)
    :effect
      (and
        (in r9)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r10))
      )
    )
  (:action move-right-from-r11
    :parameters ()
    :precondition 
      (in r11)
    :effect
      (and
        (in r12)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r11))
      )
    )
  (:action move-left-from-r11
    :parameters ()
    :precondition 
      (in r11)
    :effect
      (and
        (in r10)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r11))
      )
    )
  (:action move-right-from-r12
    :parameters ()
    :precondition 
      (in r12)
    :effect
      (and
        (in r13)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r12))
      )
    )
  (:action move-left-from-r12
    :parameters ()
    :precondition 
      (in r12)
    :effect
      (and
        (in r11)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r12))
      )
    )
  (:action move-right-from-r13
    :parameters ()
    :precondition 
      (in r13)
    :effect
      (and
        (in r14)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r13))
      )
    )
  (:action move-left-from-r13
    :parameters ()
    :precondition 
      (in r13)
    :effect
      (and
        (in r12)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r13))
      )
    )
  (:action move-right-from-r14
    :parameters ()
    :precondition 
      (in r14)
    :effect
      (and
        (in r15)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r14))
      )
    )
  (:action move-left-from-r14
    :parameters ()
    :precondition 
      (in r14)
    :effect
      (and
        (in r13)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r14))
      )
    )
  (:action move-right-from-r15
    :parameters ()
    :precondition 
      (in r15)
    :effect
      (and
        (in r16)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r15))
      )
    )
  (:action move-left-from-r15
    :parameters ()
    :precondition 
      (in r15)
    :effect
      (and
        (in r14)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r15))
      )
    )
  (:action move-right-from-r16
    :parameters ()
    :precondition 
      (in r16)
    :effect
      (and
        (in r17)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r16))
      )
    )
  (:action move-left-from-r16
    :parameters ()
    :precondition 
      (in r16)
    :effect
      (and
        (in r15)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r16))
      )
    )
  (:action move-right-from-r17
    :parameters ()
    :precondition 
      (in r17)
    :effect
      (and
        (in r18)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r17))
      )
    )
  (:action move-left-from-r17
    :parameters ()
    :precondition 
      (in r17)
    :effect
      (and
        (in r16)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r17))
      )
    )
  (:action move-right-from-r18
    :parameters ()
    :precondition 
      (in r18)
    :effect
      (and
        (in r19)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r18))
      )
    )
  (:action move-left-from-r18
    :parameters ()
    :precondition 
      (in r18)
    :effect
      (and
        (in r17)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r18))
      )
    )
  (:action move-right-from-r19
    :parameters ()
    :precondition 
      (in r19)
    :effect
      (and
        (in r20)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r19))
      )
    )
  (:action move-left-from-r19
    :parameters ()
    :precondition 
      (in r19)
    :effect
      (and
        (in r18)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r19))
      )
    )
  (:action move-right-from-r20
    :parameters ()
    :precondition 
      (in r20)
    :effect
      (and
        (in r21)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r20))
      )
    )
  (:action move-left-from-r20
    :parameters ()
    :precondition 
      (in r20)
    :effect
      (and
        (in r19)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r20))
      )
    )
  (:action move-right-from-r21
    :parameters ()
    :precondition 
      (in r21)
    :effect
      (and
        (in r22)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r21))
      )
    )
  (:action move-left-from-r21
    :parameters ()
    :precondition 
      (in r21)
    :effect
      (and
        (in r20)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r21))
      )
    )
  (:action move-right-from-r22
    :parameters ()
    :precondition 
      (in r22)
    :effect
      (and
        (in r23)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r22))
      )
    )
  (:action move-left-from-r22
    :parameters ()
    :precondition 
      (in r22)
    :effect
      (and
        (in r21)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r22))
      )
    )
  (:action move-right-from-r23
    :parameters ()
    :precondition 
      (in r23)
    :effect
      (and
        (in r24)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r23))
      )
    )
  (:action move-left-from-r23
    :parameters ()
    :precondition 
      (in r23)
    :effect
      (and
        (in r22)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r23))
      )
    )
  (:action move-right-from-r24
    :parameters ()
    :precondition 
      (in r24)
    :effect
      (and
        (in r25)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r24))
      )
    )
  (:action move-left-from-r24
    :parameters ()
    :precondition 
      (in r24)
    :effect
      (and
        (in r23)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r24))
      )
    )
  (:action move-right-from-r25
    :parameters ()
    :precondition 
      (in r25)
    :effect
      (and
        (in r26)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r25))
      )
    )
  (:action move-left-from-r25
    :parameters ()
    :precondition 
      (in r25)
    :effect
      (and
        (in r24)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r25))
      )
    )
  (:action move-right-from-r26
    :parameters ()
    :precondition 
      (in r26)
    :effect
      (and
        (in r27)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r26))
      )
    )
  (:action move-left-from-r26
    :parameters ()
    :precondition 
      (in r26)
    :effect
      (and
        (in r25)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r26))
      )
    )
  (:action move-right-from-r27
    :parameters ()
    :precondition 
      (in r27)
    :effect
      (and
        (in r28)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r27))
      )
    )
  (:action move-left-from-r27
    :parameters ()
    :precondition 
      (in r27)
    :effect
      (and
        (in r26)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r27))
      )
    )
  (:action move-right-from-r28
    :parameters ()
    :precondition 
      (in r28)
    :effect
      (and
        (in r29)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r28))
      )
    )
  (:action move-left-from-r28
    :parameters ()
    :precondition 
      (in r28)
    :effect
      (and
        (in r27)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r28))
      )
    )
  (:action move-right-from-r29
    :parameters ()
    :precondition 
      (in r29)
    :effect
      (and
        (in r30)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r29))
      )
    )
  (:action move-left-from-r29
    :parameters ()
    :precondition 
      (in r29)
    :effect
      (and
        (in r28)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r29))
      )
    )
  (:action move-right-from-r30
    :parameters ()
    :precondition 
      (in r30)
    :effect
      (and
        (in r31)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r30))
      )
    )
  (:action move-left-from-r30
    :parameters ()
    :precondition 
      (in r30)
    :effect
      (and
        (in r29)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r30))
      )
    )
  (:action move-right-from-r31
    :parameters ()
    :precondition 
      (in r31)
    :effect
      (and
        (in r32)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r31))
      )
    )
  (:action move-left-from-r31
    :parameters ()
    :precondition 
      (in r31)
    :effect
      (and
        (in r30)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r31))
      )
    )
  (:action move-right-from-r32
    :parameters ()
    :precondition 
      (in r32)
    :effect
      (and
        (in r33)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r32))
      )
    )
  (:action move-left-from-r32
    :parameters ()
    :precondition 
      (in r32)
    :effect
      (and
        (in r31)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r32))
      )
    )
  (:action move-right-from-r33
    :parameters ()
    :precondition 
      (in r33)
    :effect
      (and
        (in r34)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r33))
      )
    )
  (:action move-left-from-r33
    :parameters ()
    :precondition 
      (in r33)
    :effect
      (and
        (in r32)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r33))
      )
    )
  (:action move-right-from-r34
    :parameters ()
    :precondition 
      (in r34)
    :effect
      (and
        (in r35)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r34))
      )
    )
  (:action move-left-from-r34
    :parameters ()
    :precondition 
      (in r34)
    :effect
      (and
        (in r33)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r34))
      )
    )
  (:action move-right-from-r35
    :parameters ()
    :precondition 
      (in r35)
    :effect
      (and
        (in r36)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r35))
      )
    )
  (:action move-left-from-r35
    :parameters ()
    :precondition 
      (in r35)
    :effect
      (and
        (in r34)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r35))
      )
    )
  (:action move-right-from-r36
    :parameters ()
    :precondition 
      (in r36)
    :effect
      (and
        (in r37)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r36))
      )
    )
  (:action move-left-from-r36
    :parameters ()
    :precondition 
      (in r36)
    :effect
      (and
        (in r35)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r36))
      )
    )
  (:action move-right-from-r37
    :parameters ()
    :precondition 
      (in r37)
    :effect
      (and
        (in r38)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r37))
      )
    )
  (:action move-left-from-r37
    :parameters ()
    :precondition 
      (in r37)
    :effect
      (and
        (in r36)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r37))
      )
    )
  (:action move-right-from-r38
    :parameters ()
    :precondition 
      (in r38)
    :effect
      (and
        (in r39)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r38))
      )
    )
  (:action move-left-from-r38
    :parameters ()
    :precondition 
      (in r38)
    :effect
      (and
        (in r37)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r38))
      )
    )
  (:action move-right-from-r39
    :parameters ()
    :precondition 
      (in r39)
    :effect
      (and
        (in r40)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r39))
      )
    )
  (:action move-left-from-r39
    :parameters ()
    :precondition 
      (in r39)
    :effect
      (and
        (in r38)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r39))
      )
    )
  (:action move-right-from-r40
    :parameters ()
    :precondition 
      (in r40)
    :effect
      (and
        (in r41)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r40))
      )
    )
  (:action move-left-from-r40
    :parameters ()
    :precondition 
      (in r40)
    :effect
      (and
        (in r39)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r40))
      )
    )
  (:action move-right-from-r41
    :parameters ()
    :precondition 
      (in r41)
    :effect
      (and
        (in r42)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r41))
      )
    )
  (:action move-left-from-r41
    :parameters ()
    :precondition 
      (in r41)
    :effect
      (and
        (in r40)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r41))
      )
    )
  (:action move-right-from-r42
    :parameters ()
    :precondition 
      (in r42)
    :effect
      (and
        (in r43)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r42))
      )
    )
  (:action move-left-from-r42
    :parameters ()
    :precondition 
      (in r42)
    :effect
      (and
        (in r41)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r42))
      )
    )
  (:action move-right-from-r43
    :parameters ()
    :precondition 
      (in r43)
    :effect
      (and
        (in r44)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r43))
      )
    )
  (:action move-left-from-r43
    :parameters ()
    :precondition 
      (in r43)
    :effect
      (and
        (in r42)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r43))
      )
    )
  (:action move-right-from-r44
    :parameters ()
    :precondition 
      (in r44)
    :effect
      (and
        (in r45)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r44))
      )
    )
  (:action move-left-from-r44
    :parameters ()
    :precondition 
      (in r44)
    :effect
      (and
        (in r43)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r44))
      )
    )
  (:action move-right-from-r45
    :parameters ()
    :precondition 
      (in r45)
    :effect
      (and
        (in r46)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r45))
      )
    )
  (:action move-left-from-r45
    :parameters ()
    :precondition 
      (in r45)
    :effect
      (and
        (in r44)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r45))
      )
    )
  (:action move-right-from-r46
    :parameters ()
    :precondition 
      (in r46)
    :effect
      (and
        (in r47)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r46))
      )
    )
  (:action move-left-from-r46
    :parameters ()
    :precondition 
      (in r46)
    :effect
      (and
        (in r45)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r46))
      )
    )
  (:action move-right-from-r47
    :parameters ()
    :precondition 
      (in r47)
    :effect
      (and
        (in r48)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r47))
      )
    )
  (:action move-left-from-r47
    :parameters ()
    :precondition 
      (in r47)
    :effect
      (and
        (in r46)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r47))
      )
    )
  (:action move-right-from-r48
    :parameters ()
    :precondition 
      (in r48)
    :effect
      (and
        (in r49)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r48))
      )
    )
  (:action move-left-from-r48
    :parameters ()
    :precondition 
      (in r48)
    :effect
      (and
        (in r47)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r48))
      )
    )
  (:action move-right-from-r49
    :parameters ()
    :precondition 
      (in r49)
    :effect
      (and
        (in r50)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r49))
      )
    )
  (:action move-left-from-r49
    :parameters ()
    :precondition 
      (in r49)
    :effect
      (and
        (in r48)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r49))
      )
    )
  (:action move-right-from-r50
    :parameters ()
    :precondition 
      (in r50)
    :effect
      (and
        (in r51)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r50))
      )
    )
  (:action move-left-from-r50
    :parameters ()
    :precondition 
      (in r50)
    :effect
      (and
        (in r49)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r50))
      )
    )
  (:action move-right-from-r51
    :parameters ()
    :precondition 
      (in r51)
    :effect
      (and
        (in r52)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r51))
      )
    )
  (:action move-left-from-r51
    :parameters ()
    :precondition 
      (in r51)
    :effect
      (and
        (in r50)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r51))
      )
    )
  (:action move-right-from-r52
    :parameters ()
    :precondition 
      (in r52)
    :effect
      (and
        (in r53)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r52))
      )
    )
  (:action move-left-from-r52
    :parameters ()
    :precondition 
      (in r52)
    :effect
      (and
        (in r51)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r52))
      )
    )
  (:action move-right-from-r53
    :parameters ()
    :precondition 
      (in r53)
    :effect
      (and
        (in r54)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r53))
      )
    )
  (:action move-left-from-r53
    :parameters ()
    :precondition 
      (in r53)
    :effect
      (and
        (in r52)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r53))
      )
    )
  (:action move-right-from-r54
    :parameters ()
    :precondition 
      (in r54)
    :effect
      (and
        (in r55)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r54))
      )
    )
  (:action move-left-from-r54
    :parameters ()
    :precondition 
      (in r54)
    :effect
      (and
        (in r53)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r54))
      )
    )
  (:action move-right-from-r55
    :parameters ()
    :precondition 
      (in r55)
    :effect
      (and
        (in r56)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r55))
      )
    )
  (:action move-left-from-r55
    :parameters ()
    :precondition 
      (in r55)
    :effect
      (and
        (in r54)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r55))
      )
    )
  (:action move-right-from-r56
    :parameters ()
    :precondition 
      (in r56)
    :effect
      (and
        (in r57)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r56))
      )
    )
  (:action move-left-from-r56
    :parameters ()
    :precondition 
      (in r56)
    :effect
      (and
        (in r55)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r56))
      )
    )
  (:action move-right-from-r57
    :parameters ()
    :precondition 
      (in r57)
    :effect
      (and
        (in r58)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r57))
      )
    )
  (:action move-left-from-r57
    :parameters ()
    :precondition 
      (in r57)
    :effect
      (and
        (in r56)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r57))
      )
    )
  (:action move-right-from-r58
    :parameters ()
    :precondition 
      (in r58)
    :effect
      (and
        (in r59)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r58))
      )
    )
  (:action move-left-from-r58
    :parameters ()
    :precondition 
      (in r58)
    :effect
      (and
        (in r57)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r58))
      )
    )
  (:action move-right-from-r59
    :parameters ()
    :precondition 
      (in r59)
    :effect
      (and
        (in r60)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r59))
      )
    )
  (:action move-left-from-r59
    :parameters ()
    :precondition 
      (in r59)
    :effect
      (and
        (in r58)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r59))
      )
    )
  (:action move-right-from-r60
    :parameters ()
    :precondition 
      (in r60)
    :effect
      (and
        (in r61)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r60))
      )
    )
  (:action move-left-from-r60
    :parameters ()
    :precondition 
      (in r60)
    :effect
      (and
        (in r59)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r60))
      )
    )
  (:action move-right-from-r61
    :parameters ()
    :precondition 
      (in r61)
    :effect
      (and
        (in r62)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r61))
      )
    )
  (:action move-left-from-r61
    :parameters ()
    :precondition 
      (in r61)
    :effect
      (and
        (in r60)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r61))
      )
    )
  (:action move-right-from-r62
    :parameters ()
    :precondition 
      (in r62)
    :effect
      (and
        (in r63)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r62))
      )
    )
  (:action move-left-from-r62
    :parameters ()
    :precondition 
      (in r62)
    :effect
      (and
        (in r61)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r62))
      )
    )
  (:action move-right-from-r63
    :parameters ()
    :precondition 
      (in r63)
    :effect
      (and
        (in r64)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r63))
      )
    )
  (:action move-left-from-r63
    :parameters ()
    :precondition 
      (in r63)
    :effect
      (and
        (in r62)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r63))
      )
    )
  (:action move-right-from-r64
    :parameters ()
    :precondition 
      (in r64)
    :effect
      (and
        (in r65)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r64))
      )
    )
  (:action move-left-from-r64
    :parameters ()
    :precondition 
      (in r64)
    :effect
      (and
        (in r63)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r64))
      )
    )
  (:action move-right-from-r65
    :parameters ()
    :precondition 
      (in r65)
    :effect
      (and
        (in r66)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r65))
      )
    )
  (:action move-left-from-r65
    :parameters ()
    :precondition 
      (in r65)
    :effect
      (and
        (in r64)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r65))
      )
    )
  (:action move-right-from-r66
    :parameters ()
    :precondition 
      (in r66)
    :effect
      (and
        (in r67)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r66))
      )
    )
  (:action move-left-from-r66
    :parameters ()
    :precondition 
      (in r66)
    :effect
      (and
        (in r65)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r66))
      )
    )
  (:action move-right-from-r67
    :parameters ()
    :precondition 
      (in r67)
    :effect
      (and
        (in r68)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r67))
      )
    )
  (:action move-left-from-r67
    :parameters ()
    :precondition 
      (in r67)
    :effect
      (and
        (in r66)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r67))
      )
    )
  (:action move-right-from-r68
    :parameters ()
    :precondition 
      (in r68)
    :effect
      (and
        (in r69)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r68))
      )
    )
  (:action move-left-from-r68
    :parameters ()
    :precondition 
      (in r68)
    :effect
      (and
        (in r67)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r68))
      )
    )
  (:action move-right-from-r69
    :parameters ()
    :precondition 
      (in r69)
    :effect
      (and
        (in r70)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r69))
      )
    )
  (:action move-left-from-r69
    :parameters ()
    :precondition 
      (in r69)
    :effect
      (and
        (in r68)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r69))
      )
    )
  (:action move-right-from-r70
    :parameters ()
    :precondition 
      (in r70)
    :effect
      (and
        (in r71)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r70))
      )
    )
  (:action move-left-from-r70
    :parameters ()
    :precondition 
      (in r70)
    :effect
      (and
        (in r69)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r70))
      )
    )
  (:action move-right-from-r71
    :parameters ()
    :precondition 
      (in r71)
    :effect
      (and
        (in r72)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r71))
      )
    )
  (:action move-left-from-r71
    :parameters ()
    :precondition 
      (in r71)
    :effect
      (and
        (in r70)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r71))
      )
    )
  (:action move-right-from-r72
    :parameters ()
    :precondition 
      (in r72)
    :effect
      (and
        (in r73)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r72))
      )
    )
  (:action move-left-from-r72
    :parameters ()
    :precondition 
      (in r72)
    :effect
      (and
        (in r71)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r72))
      )
    )
  (:action move-right-from-r73
    :parameters ()
    :precondition 
      (in r73)
    :effect
      (and
        (in r74)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r73))
      )
    )
  (:action move-left-from-r73
    :parameters ()
    :precondition 
      (in r73)
    :effect
      (and
        (in r72)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r73))
      )
    )
  (:action move-right-from-r74
    :parameters ()
    :precondition 
      (in r74)
    :effect
      (and
        (in r75)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r74))
      )
    )
  (:action move-left-from-r74
    :parameters ()
    :precondition 
      (in r74)
    :effect
      (and
        (in r73)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r74))
      )
    )
  (:action move-right-from-r75
    :parameters ()
    :precondition 
      (in r75)
    :effect
      (and
        (in r76)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r75))
      )
    )
  (:action move-left-from-r75
    :parameters ()
    :precondition 
      (in r75)
    :effect
      (and
        (in r74)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r75))
      )
    )
  (:action move-right-from-r76
    :parameters ()
    :precondition 
      (in r76)
    :effect
      (and
        (in r77)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r76))
      )
    )
  (:action move-left-from-r76
    :parameters ()
    :precondition 
      (in r76)
    :effect
      (and
        (in r75)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r76))
      )
    )
  (:action move-right-from-r77
    :parameters ()
    :precondition 
      (in r77)
    :effect
      (and
        (in r78)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r77))
      )
    )
  (:action move-left-from-r77
    :parameters ()
    :precondition 
      (in r77)
    :effect
      (and
        (in r76)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r77))
      )
    )
  (:action move-right-from-r78
    :parameters ()
    :precondition 
      (in r78)
    :effect
      (and
        (in r79)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r78))
      )
    )
  (:action move-left-from-r78
    :parameters ()
    :precondition 
      (in r78)
    :effect
      (and
        (in r77)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r78))
      )
    )
  (:action move-right-from-r79
    :parameters ()
    :precondition 
      (in r79)
    :effect
      (and
        (in r80)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r79))
      )
    )
  (:action move-left-from-r79
    :parameters ()
    :precondition 
      (in r79)
    :effect
      (and
        (in r78)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r79))
      )
    )
  (:action move-right-from-r80
    :parameters ()
    :precondition 
      (in r80)
    :effect
      (and
        (in r81)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r80))
      )
    )
  (:action move-left-from-r80
    :parameters ()
    :precondition 
      (in r80)
    :effect
      (and
        (in r79)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r80))
      )
    )
  (:action move-right-from-r81
    :parameters ()
    :precondition 
      (in r81)
    :effect
      (and
        (in r82)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r81))
      )
    )
  (:action move-left-from-r81
    :parameters ()
    :precondition 
      (in r81)
    :effect
      (and
        (in r80)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r81))
      )
    )
  (:action move-right-from-r82
    :parameters ()
    :precondition 
      (in r82)
    :effect
      (and
        (in r83)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r82))
      )
    )
  (:action move-left-from-r82
    :parameters ()
    :precondition 
      (in r82)
    :effect
      (and
        (in r81)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r82))
      )
    )
  (:action move-right-from-r83
    :parameters ()
    :precondition 
      (in r83)
    :effect
      (and
        (in r84)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r83))
      )
    )
  (:action move-left-from-r83
    :parameters ()
    :precondition 
      (in r83)
    :effect
      (and
        (in r82)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r83))
      )
    )
  (:action move-right-from-r84
    :parameters ()
    :precondition 
      (in r84)
    :effect
      (and
        (in r85)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r84))
      )
    )
  (:action move-left-from-r84
    :parameters ()
    :precondition 
      (in r84)
    :effect
      (and
        (in r83)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r84))
      )
    )
  (:action move-right-from-r85
    :parameters ()
    :precondition 
      (in r85)
    :effect
      (and
        (in r86)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r85))
      )
    )
  (:action move-left-from-r85
    :parameters ()
    :precondition 
      (in r85)
    :effect
      (and
        (in r84)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r85))
      )
    )
  (:action move-right-from-r86
    :parameters ()
    :precondition 
      (in r86)
    :effect
      (and
        (in r87)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r86))
      )
    )
  (:action move-left-from-r86
    :parameters ()
    :precondition 
      (in r86)
    :effect
      (and
        (in r85)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r86))
      )
    )
  (:action move-right-from-r87
    :parameters ()
    :precondition 
      (in r87)
    :effect
      (and
        (in r88)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r87))
      )
    )
  (:action move-left-from-r87
    :parameters ()
    :precondition 
      (in r87)
    :effect
      (and
        (in r86)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r87))
      )
    )
  (:action move-right-from-r88
    :parameters ()
    :precondition 
      (in r88)
    :effect
      (and
        (in r89)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r88))
      )
    )
  (:action move-left-from-r88
    :parameters ()
    :precondition 
      (in r88)
    :effect
      (and
        (in r87)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r88))
      )
    )
  (:action move-right-from-r89
    :parameters ()
    :precondition 
      (in r89)
    :effect
      (and
        (in r90)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r89))
      )
    )
  (:action move-left-from-r89
    :parameters ()
    :precondition 
      (in r89)
    :effect
      (and
        (in r88)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r89))
      )
    )
  (:action move-right-from-r90
    :parameters ()
    :precondition 
      (in r90)
    :effect
      (and
        (in r91)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r90))
      )
    )
  (:action move-left-from-r90
    :parameters ()
    :precondition 
      (in r90)
    :effect
      (and
        (in r89)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r90))
      )
    )
  (:action move-right-from-r91
    :parameters ()
    :precondition 
      (in r91)
    :effect
      (and
        (in r92)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r91))
      )
    )
  (:action move-left-from-r91
    :parameters ()
    :precondition 
      (in r91)
    :effect
      (and
        (in r90)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r91))
      )
    )
  (:action move-right-from-r92
    :parameters ()
    :precondition 
      (in r92)
    :effect
      (and
        (in r93)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r92))
      )
    )
  (:action move-left-from-r92
    :parameters ()
    :precondition 
      (in r92)
    :effect
      (and
        (in r91)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r92))
      )
    )
  (:action move-right-from-r93
    :parameters ()
    :precondition 
      (in r93)
    :effect
      (and
        (in r94)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r93))
      )
    )
  (:action move-left-from-r93
    :parameters ()
    :precondition 
      (in r93)
    :effect
      (and
        (in r92)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r93))
      )
    )
  (:action move-right-from-r94
    :parameters ()
    :precondition 
      (in r94)
    :effect
      (and
        (in r95)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r94))
      )
    )
  (:action move-left-from-r94
    :parameters ()
    :precondition 
      (in r94)
    :effect
      (and
        (in r93)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r94))
      )
    )
  (:action move-right-from-r95
    :parameters ()
    :precondition 
      (in r95)
    :effect
      (and
        (in r96)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r95))
      )
    )
  (:action move-left-from-r95
    :parameters ()
    :precondition 
      (in r95)
    :effect
      (and
        (in r94)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r95))
      )
    )
  (:action move-right-from-r96
    :parameters ()
    :precondition 
      (in r96)
    :effect
      (and
        (in r97)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r96))
      )
    )
  (:action move-left-from-r96
    :parameters ()
    :precondition 
      (in r96)
    :effect
      (and
        (in r95)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r96))
      )
    )
  (:action move-right-from-r97
    :parameters ()
    :precondition 
      (in r97)
    :effect
      (and
        (in r98)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r97))
      )
    )
  (:action move-left-from-r97
    :parameters ()
    :precondition 
      (in r97)
    :effect
      (and
        (in r96)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r97))
      )
    )
  (:action move-right-from-r98
    :parameters ()
    :precondition 
      (in r98)
    :effect
      (and
        (in r99)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r98))
      )
    )
  (:action move-left-from-r98
    :parameters ()
    :precondition 
      (in r98)
    :effect
      (and
        (in r97)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r98))
      )
    )
  (:action move-right-from-r99
    :parameters ()
    :precondition 
      (in r99)
    :effect
      (and
        (in r100)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r99))
      )
    )
  (:action move-left-from-r99
    :parameters ()
    :precondition 
      (in r99)
    :effect
      (and
        (in r98)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r99))
      )
    )
  (:action move-right-from-r100
    :parameters ()
    :precondition 
      (in r100)
    :effect
      (and
        (in r101)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r100))
      )
    )
  (:action move-left-from-r100
    :parameters ()
    :precondition 
      (in r100)
    :effect
      (and
        (in r99)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r100))
      )
    )
  (:action move-right-from-r101
    :parameters ()
    :precondition 
      (in r101)
    :effect
      (and
        (in r102)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r101))
      )
    )
  (:action move-left-from-r101
    :parameters ()
    :precondition 
      (in r101)
    :effect
      (and
        (in r100)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r101))
      )
    )
  (:action move-right-from-r102
    :parameters ()
    :precondition 
      (in r102)
    :effect
      (and
        (in r103)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r102))
      )
    )
  (:action move-left-from-r102
    :parameters ()
    :precondition 
      (in r102)
    :effect
      (and
        (in r101)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r102))
      )
    )
  (:action move-right-from-r103
    :parameters ()
    :precondition 
      (in r103)
    :effect
      (and
        (in r104)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r103))
      )
    )
  (:action move-left-from-r103
    :parameters ()
    :precondition 
      (in r103)
    :effect
      (and
        (in r102)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r103))
      )
    )
  (:action move-right-from-r104
    :parameters ()
    :precondition 
      (in r104)
    :effect
      (and
        (in r105)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r104))
      )
    )
  (:action move-left-from-r104
    :parameters ()
    :precondition 
      (in r104)
    :effect
      (and
        (in r103)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r104))
      )
    )
  (:action move-right-from-r105
    :parameters ()
    :precondition 
      (in r105)
    :effect
      (and
        (in r106)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r105))
      )
    )
  (:action move-left-from-r105
    :parameters ()
    :precondition 
      (in r105)
    :effect
      (and
        (in r104)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r105))
      )
    )
  (:action move-right-from-r106
    :parameters ()
    :precondition 
      (in r106)
    :effect
      (and
        (in r107)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r106))
      )
    )
  (:action move-left-from-r106
    :parameters ()
    :precondition 
      (in r106)
    :effect
      (and
        (in r105)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r106))
      )
    )
  (:action move-right-from-r107
    :parameters ()
    :precondition 
      (in r107)
    :effect
      (and
        (in r108)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r107))
      )
    )
  (:action move-left-from-r107
    :parameters ()
    :precondition 
      (in r107)
    :effect
      (and
        (in r106)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r107))
      )
    )
  (:action move-right-from-r108
    :parameters ()
    :precondition 
      (in r108)
    :effect
      (and
        (in r109)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r108))
      )
    )
  (:action move-left-from-r108
    :parameters ()
    :precondition 
      (in r108)
    :effect
      (and
        (in r107)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r108))
      )
    )
  (:action move-right-from-r109
    :parameters ()
    :precondition 
      (in r109)
    :effect
      (and
        (in r110)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r109))
      )
    )
  (:action move-left-from-r109
    :parameters ()
    :precondition 
      (in r109)
    :effect
      (and
        (in r108)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r109))
      )
    )
  (:action move-right-from-r110
    :parameters ()
    :precondition 
      (in r110)
    :effect
      (and
        (in r111)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r110))
      )
    )
  (:action move-left-from-r110
    :parameters ()
    :precondition 
      (in r110)
    :effect
      (and
        (in r109)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r110))
      )
    )
  (:action move-right-from-r111
    :parameters ()
    :precondition 
      (in r111)
    :effect
      (and
        (in r112)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r111))
      )
    )
  (:action move-left-from-r111
    :parameters ()
    :precondition 
      (in r111)
    :effect
      (and
        (in r110)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r111))
      )
    )
  (:action move-right-from-r112
    :parameters ()
    :precondition 
      (in r112)
    :effect
      (and
        (in r113)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r112))
      )
    )
  (:action move-left-from-r112
    :parameters ()
    :precondition 
      (in r112)
    :effect
      (and
        (in r111)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r112))
      )
    )
  (:action move-right-from-r113
    :parameters ()
    :precondition 
      (in r113)
    :effect
      (and
        (in r114)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r113))
      )
    )
  (:action move-left-from-r113
    :parameters ()
    :precondition 
      (in r113)
    :effect
      (and
        (in r112)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r113))
      )
    )
  (:action move-right-from-r114
    :parameters ()
    :precondition 
      (in r114)
    :effect
      (and
        (in r115)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r114))
      )
    )
  (:action move-left-from-r114
    :parameters ()
    :precondition 
      (in r114)
    :effect
      (and
        (in r113)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r114))
      )
    )
  (:action move-right-from-r115
    :parameters ()
    :precondition 
      (in r115)
    :effect
      (and
        (in r116)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r115))
      )
    )
  (:action move-left-from-r115
    :parameters ()
    :precondition 
      (in r115)
    :effect
      (and
        (in r114)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r115))
      )
    )
  (:action move-right-from-r116
    :parameters ()
    :precondition 
      (in r116)
    :effect
      (and
        (in r117)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r116))
      )
    )
  (:action move-left-from-r116
    :parameters ()
    :precondition 
      (in r116)
    :effect
      (and
        (in r115)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r116))
      )
    )
  (:action move-right-from-r117
    :parameters ()
    :precondition 
      (in r117)
    :effect
      (and
        (in r118)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r117))
      )
    )
  (:action move-left-from-r117
    :parameters ()
    :precondition 
      (in r117)
    :effect
      (and
        (in r116)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r117))
      )
    )
  (:action move-right-from-r118
    :parameters ()
    :precondition 
      (in r118)
    :effect
      (and
        (in r119)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r118))
      )
    )
  (:action move-left-from-r118
    :parameters ()
    :precondition 
      (in r118)
    :effect
      (and
        (in r117)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r118))
      )
    )
  (:action move-right-from-r119
    :parameters ()
    :precondition 
      (in r119)
    :effect
      (and
        (in r120)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r119))
      )
    )
  (:action move-left-from-r119
    :parameters ()
    :precondition 
      (in r119)
    :effect
      (and
        (in r118)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r119))
      )
    )
  (:action move-right-from-r120
    :parameters ()
    :precondition 
      (in r120)
    :effect
      (and
        (in r121)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r120))
      )
    )
  (:action move-left-from-r120
    :parameters ()
    :precondition 
      (in r120)
    :effect
      (and
        (in r119)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r120))
      )
    )
  (:action move-right-from-r121
    :parameters ()
    :precondition 
      (in r121)
    :effect
      (and
        (in r122)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r121))
      )
    )
  (:action move-left-from-r121
    :parameters ()
    :precondition 
      (in r121)
    :effect
      (and
        (in r120)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r121))
      )
    )
  (:action move-right-from-r122
    :parameters ()
    :precondition 
      (in r122)
    :effect
      (and
        (in r123)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r122))
      )
    )
  (:action move-left-from-r122
    :parameters ()
    :precondition 
      (in r122)
    :effect
      (and
        (in r121)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r122))
      )
    )
  (:action move-right-from-r123
    :parameters ()
    :precondition 
      (in r123)
    :effect
      (and
        (in r124)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r123))
      )
    )
  (:action move-left-from-r123
    :parameters ()
    :precondition 
      (in r123)
    :effect
      (and
        (in r122)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r123))
      )
    )
  (:action move-right-from-r124
    :parameters ()
    :precondition 
      (in r124)
    :effect
      (and
        (in r125)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r124))
      )
    )
  (:action move-left-from-r124
    :parameters ()
    :precondition 
      (in r124)
    :effect
      (and
        (in r123)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r124))
      )
    )
  (:action move-right-from-r125
    :parameters ()
    :precondition 
      (in r125)
    :effect
      (and
        (in r126)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r125))
      )
    )
  (:action move-left-from-r125
    :parameters ()
    :precondition 
      (in r125)
    :effect
      (and
        (in r124)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r125))
      )
    )
  (:action move-right-from-r126
    :parameters ()
    :precondition 
      (in r126)
    :effect
      (and
        (in r127)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r126))
      )
    )
  (:action move-left-from-r126
    :parameters ()
    :precondition 
      (in r126)
    :effect
      (and
        (in r125)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r126))
      )
    )
  (:action move-right-from-r127
    :parameters ()
    :precondition 
      (in r127)
    :effect
      (and
        (in r128)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r127))
      )
    )
  (:action move-left-from-r127
    :parameters ()
    :precondition 
      (in r127)
    :effect
      (and
        (in r126)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r127))
      )
    )
  (:action move-right-from-r128
    :parameters ()
    :precondition 
      (in r128)
    :effect
      (and
        (in r129)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r128))
      )
    )
  (:action move-left-from-r128
    :parameters ()
    :precondition 
      (in r128)
    :effect
      (and
        (in r127)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r128))
      )
    )
  (:action move-right-from-r129
    :parameters ()
    :precondition 
      (in r129)
    :effect
      (and
        (in r130)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r129))
      )
    )
  (:action move-left-from-r129
    :parameters ()
    :precondition 
      (in r129)
    :effect
      (and
        (in r128)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r129))
      )
    )
  (:action move-right-from-r130
    :parameters ()
    :precondition 
      (in r130)
    :effect
      (and
        (in r131)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r130))
      )
    )
  (:action move-left-from-r130
    :parameters ()
    :precondition 
      (in r130)
    :effect
      (and
        (in r129)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r130))
      )
    )
  (:action move-right-from-r131
    :parameters ()
    :precondition 
      (in r131)
    :effect
      (and
        (in r132)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r131))
      )
    )
  (:action move-left-from-r131
    :parameters ()
    :precondition 
      (in r131)
    :effect
      (and
        (in r130)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r131))
      )
    )
  (:action move-right-from-r132
    :parameters ()
    :precondition 
      (in r132)
    :effect
      (and
        (in r133)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r132))
      )
    )
  (:action move-left-from-r132
    :parameters ()
    :precondition 
      (in r132)
    :effect
      (and
        (in r131)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r132))
      )
    )
  (:action move-right-from-r133
    :parameters ()
    :precondition 
      (in r133)
    :effect
      (and
        (in r134)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r133))
      )
    )
  (:action move-left-from-r133
    :parameters ()
    :precondition 
      (in r133)
    :effect
      (and
        (in r132)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r133))
      )
    )
  (:action move-right-from-r134
    :parameters ()
    :precondition 
      (in r134)
    :effect
      (and
        (in r135)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r134))
      )
    )
  (:action move-left-from-r134
    :parameters ()
    :precondition 
      (in r134)
    :effect
      (and
        (in r133)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r134))
      )
    )
  (:action move-right-from-r135
    :parameters ()
    :precondition 
      (in r135)
    :effect
      (and
        (in r136)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r135))
      )
    )
  (:action move-left-from-r135
    :parameters ()
    :precondition 
      (in r135)
    :effect
      (and
        (in r134)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r135))
      )
    )
  (:action move-right-from-r136
    :parameters ()
    :precondition 
      (in r136)
    :effect
      (and
        (in r137)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r136))
      )
    )
  (:action move-left-from-r136
    :parameters ()
    :precondition 
      (in r136)
    :effect
      (and
        (in r135)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r136))
      )
    )
  (:action move-right-from-r137
    :parameters ()
    :precondition 
      (in r137)
    :effect
      (and
        (in r138)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r137))
      )
    )
  (:action move-left-from-r137
    :parameters ()
    :precondition 
      (in r137)
    :effect
      (and
        (in r136)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r137))
      )
    )
  (:action move-right-from-r138
    :parameters ()
    :precondition 
      (in r138)
    :effect
      (and
        (in r139)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r138))
      )
    )
  (:action move-left-from-r138
    :parameters ()
    :precondition 
      (in r138)
    :effect
      (and
        (in r137)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r138))
      )
    )
  (:action move-right-from-r139
    :parameters ()
    :precondition 
      (in r139)
    :effect
      (and
        (in r140)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r139))
      )
    )
  (:action move-left-from-r139
    :parameters ()
    :precondition 
      (in r139)
    :effect
      (and
        (in r138)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r139))
      )
    )
  (:action move-right-from-r140
    :parameters ()
    :precondition 
      (in r140)
    :effect
      (and
        (in r141)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r140))
      )
    )
  (:action move-left-from-r140
    :parameters ()
    :precondition 
      (in r140)
    :effect
      (and
        (in r139)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r140))
      )
    )
  (:action move-right-from-r141
    :parameters ()
    :precondition 
      (in r141)
    :effect
      (and
        (in r142)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r141))
      )
    )
  (:action move-left-from-r141
    :parameters ()
    :precondition 
      (in r141)
    :effect
      (and
        (in r140)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r141))
      )
    )
  (:action move-right-from-r142
    :parameters ()
    :precondition 
      (in r142)
    :effect
      (and
        (in r143)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r142))
      )
    )
  (:action move-left-from-r142
    :parameters ()
    :precondition 
      (in r142)
    :effect
      (and
        (in r141)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r142))
      )
    )
  (:action move-right-from-r143
    :parameters ()
    :precondition 
      (in r143)
    :effect
      (and
        (in r144)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r143))
      )
    )
  (:action move-left-from-r143
    :parameters ()
    :precondition 
      (in r143)
    :effect
      (and
        (in r142)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r143))
      )
    )
  (:action move-right-from-r144
    :parameters ()
    :precondition 
      (in r144)
    :effect
      (and
        (in r145)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r144))
      )
    )
  (:action move-left-from-r144
    :parameters ()
    :precondition 
      (in r144)
    :effect
      (and
        (in r143)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r144))
      )
    )
  (:action move-right-from-r145
    :parameters ()
    :precondition 
      (in r145)
    :effect
      (and
        (in r146)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r145))
      )
    )
  (:action move-left-from-r145
    :parameters ()
    :precondition 
      (in r145)
    :effect
      (and
        (in r144)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r145))
      )
    )
  (:action move-right-from-r146
    :parameters ()
    :precondition 
      (in r146)
    :effect
      (and
        (in r147)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r146))
      )
    )
  (:action move-left-from-r146
    :parameters ()
    :precondition 
      (in r146)
    :effect
      (and
        (in r145)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r146))
      )
    )
  (:action move-right-from-r147
    :parameters ()
    :precondition 
      (in r147)
    :effect
      (and
        (in r148)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r147))
      )
    )
  (:action move-left-from-r147
    :parameters ()
    :precondition 
      (in r147)
    :effect
      (and
        (in r146)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r147))
      )
    )
  (:action move-right-from-r148
    :parameters ()
    :precondition 
      (in r148)
    :effect
      (and
        (in r149)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r148))
      )
    )
  (:action move-left-from-r148
    :parameters ()
    :precondition 
      (in r148)
    :effect
      (and
        (in r147)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r148))
      )
    )
  (:action move-right-from-r149
    :parameters ()
    :precondition 
      (in r149)
    :effect
      (and
        (in r150)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r149))
      )
    )
  (:action move-left-from-r149
    :parameters ()
    :precondition 
      (in r149)
    :effect
      (and
        (in r148)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r149))
      )
    )
  (:action move-right-from-r150
    :parameters ()
    :precondition 
      (in r150)
    :effect
      (and
        (in r151)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r150))
      )
    )
  (:action move-left-from-r150
    :parameters ()
    :precondition 
      (in r150)
    :effect
      (and
        (in r149)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r150))
      )
    )
  (:action move-right-from-r151
    :parameters ()
    :precondition 
      (in r151)
    :effect
      (and
        (in r152)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r151))
      )
    )
  (:action move-left-from-r151
    :parameters ()
    :precondition 
      (in r151)
    :effect
      (and
        (in r150)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r151))
      )
    )
  (:action move-right-from-r152
    :parameters ()
    :precondition 
      (in r152)
    :effect
      (and
        (in r153)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r152))
      )
    )
  (:action move-left-from-r152
    :parameters ()
    :precondition 
      (in r152)
    :effect
      (and
        (in r151)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r152))
      )
    )
  (:action move-right-from-r153
    :parameters ()
    :precondition 
      (in r153)
    :effect
      (and
        (in r154)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r153))
      )
    )
  (:action move-left-from-r153
    :parameters ()
    :precondition 
      (in r153)
    :effect
      (and
        (in r152)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r153))
      )
    )
  (:action move-right-from-r154
    :parameters ()
    :precondition 
      (in r154)
    :effect
      (and
        (in r155)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r154))
      )
    )
  (:action move-left-from-r154
    :parameters ()
    :precondition 
      (in r154)
    :effect
      (and
        (in r153)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r154))
      )
    )
  (:action move-right-from-r155
    :parameters ()
    :precondition 
      (in r155)
    :effect
      (and
        (in r156)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r155))
      )
    )
  (:action move-left-from-r155
    :parameters ()
    :precondition 
      (in r155)
    :effect
      (and
        (in r154)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r155))
      )
    )
  (:action move-right-from-r156
    :parameters ()
    :precondition 
      (in r156)
    :effect
      (and
        (in r157)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r156))
      )
    )
  (:action move-left-from-r156
    :parameters ()
    :precondition 
      (in r156)
    :effect
      (and
        (in r155)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r156))
      )
    )
  (:action move-right-from-r157
    :parameters ()
    :precondition 
      (in r157)
    :effect
      (and
        (in r158)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r157))
      )
    )
  (:action move-left-from-r157
    :parameters ()
    :precondition 
      (in r157)
    :effect
      (and
        (in r156)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r157))
      )
    )
  (:action move-right-from-r158
    :parameters ()
    :precondition 
      (in r158)
    :effect
      (and
        (in r159)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r158))
      )
    )
  (:action move-left-from-r158
    :parameters ()
    :precondition 
      (in r158)
    :effect
      (and
        (in r157)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r158))
      )
    )
  (:action move-right-from-r159
    :parameters ()
    :precondition 
      (in r159)
    :effect
      (and
        (in r160)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r159))
      )
    )
  (:action move-left-from-r159
    :parameters ()
    :precondition 
      (in r159)
    :effect
      (and
        (in r158)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r159))
      )
    )
  (:action move-right-from-r160
    :parameters ()
    :precondition 
      (in r160)
    :effect
      (and
        (in r161)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r160))
      )
    )
  (:action move-left-from-r160
    :parameters ()
    :precondition 
      (in r160)
    :effect
      (and
        (in r159)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r160))
      )
    )
  (:action move-right-from-r161
    :parameters ()
    :precondition 
      (in r161)
    :effect
      (and
        (in r162)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r161))
      )
    )
  (:action move-left-from-r161
    :parameters ()
    :precondition 
      (in r161)
    :effect
      (and
        (in r160)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r161))
      )
    )
  (:action move-right-from-r162
    :parameters ()
    :precondition 
      (in r162)
    :effect
      (and
        (in r163)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r162))
      )
    )
  (:action move-left-from-r162
    :parameters ()
    :precondition 
      (in r162)
    :effect
      (and
        (in r161)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r162))
      )
    )
  (:action move-right-from-r163
    :parameters ()
    :precondition 
      (in r163)
    :effect
      (and
        (in r164)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r163))
      )
    )
  (:action move-left-from-r163
    :parameters ()
    :precondition 
      (in r163)
    :effect
      (and
        (in r162)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r163))
      )
    )
  (:action move-right-from-r164
    :parameters ()
    :precondition 
      (in r164)
    :effect
      (and
        (in r165)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r164))
      )
    )
  (:action move-left-from-r164
    :parameters ()
    :precondition 
      (in r164)
    :effect
      (and
        (in r163)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r164))
      )
    )
  (:action move-right-from-r165
    :parameters ()
    :precondition 
      (in r165)
    :effect
      (and
        (in r166)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r165))
      )
    )
  (:action move-left-from-r165
    :parameters ()
    :precondition 
      (in r165)
    :effect
      (and
        (in r164)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r165))
      )
    )
  (:action move-right-from-r166
    :parameters ()
    :precondition 
      (in r166)
    :effect
      (and
        (in r167)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r166))
      )
    )
  (:action move-left-from-r166
    :parameters ()
    :precondition 
      (in r166)
    :effect
      (and
        (in r165)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r166))
      )
    )
  (:action move-right-from-r167
    :parameters ()
    :precondition 
      (in r167)
    :effect
      (and
        (in r168)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r167))
      )
    )
  (:action move-left-from-r167
    :parameters ()
    :precondition 
      (in r167)
    :effect
      (and
        (in r166)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r167))
      )
    )
  (:action move-right-from-r168
    :parameters ()
    :precondition 
      (in r168)
    :effect
      (and
        (in r169)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r168))
      )
    )
  (:action move-left-from-r168
    :parameters ()
    :precondition 
      (in r168)
    :effect
      (and
        (in r167)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r168))
      )
    )
  (:action move-right-from-r169
    :parameters ()
    :precondition 
      (in r169)
    :effect
      (and
        (in r170)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r169))
      )
    )
  (:action move-left-from-r169
    :parameters ()
    :precondition 
      (in r169)
    :effect
      (and
        (in r168)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r169))
      )
    )
  (:action move-right-from-r170
    :parameters ()
    :precondition 
      (in r170)
    :effect
      (and
        (in r171)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r170))
      )
    )
  (:action move-left-from-r170
    :parameters ()
    :precondition 
      (in r170)
    :effect
      (and
        (in r169)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r170))
      )
    )
  (:action move-right-from-r171
    :parameters ()
    :precondition 
      (in r171)
    :effect
      (and
        (in r172)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r171))
      )
    )
  (:action move-left-from-r171
    :parameters ()
    :precondition 
      (in r171)
    :effect
      (and
        (in r170)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r171))
      )
    )
  (:action move-right-from-r172
    :parameters ()
    :precondition 
      (in r172)
    :effect
      (and
        (in r173)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r172))
      )
    )
  (:action move-left-from-r172
    :parameters ()
    :precondition 
      (in r172)
    :effect
      (and
        (in r171)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r172))
      )
    )
  (:action move-right-from-r173
    :parameters ()
    :precondition 
      (in r173)
    :effect
      (and
        (in r174)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r173))
      )
    )
  (:action move-left-from-r173
    :parameters ()
    :precondition 
      (in r173)
    :effect
      (and
        (in r172)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r173))
      )
    )
  (:action move-right-from-r174
    :parameters ()
    :precondition 
      (in r174)
    :effect
      (and
        (in r175)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r174))
      )
    )
  (:action move-left-from-r174
    :parameters ()
    :precondition 
      (in r174)
    :effect
      (and
        (in r173)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r174))
      )
    )
  (:action move-right-from-r175
    :parameters ()
    :precondition 
      (in r175)
    :effect
      (and
        (in r176)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r175))
      )
    )
  (:action move-left-from-r175
    :parameters ()
    :precondition 
      (in r175)
    :effect
      (and
        (in r174)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r175))
      )
    )
  (:action move-right-from-r176
    :parameters ()
    :precondition 
      (in r176)
    :effect
      (and
        (in r177)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r176))
      )
    )
  (:action move-left-from-r176
    :parameters ()
    :precondition 
      (in r176)
    :effect
      (and
        (in r175)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r176))
      )
    )
  (:action move-right-from-r177
    :parameters ()
    :precondition 
      (in r177)
    :effect
      (and
        (in r178)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r177))
      )
    )
  (:action move-left-from-r177
    :parameters ()
    :precondition 
      (in r177)
    :effect
      (and
        (in r176)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r177))
      )
    )
  (:action move-right-from-r178
    :parameters ()
    :precondition 
      (in r178)
    :effect
      (and
        (in r179)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r178))
      )
    )
  (:action move-left-from-r178
    :parameters ()
    :precondition 
      (in r178)
    :effect
      (and
        (in r177)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r178))
      )
    )
  (:action move-right-from-r179
    :parameters ()
    :precondition 
      (in r179)
    :effect
      (and
        (in r180)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r179))
      )
    )
  (:action move-left-from-r179
    :parameters ()
    :precondition 
      (in r179)
    :effect
      (and
        (in r178)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r179))
      )
    )
  (:action move-right-from-r180
    :parameters ()
    :precondition 
      (in r180)
    :effect
      (and
        (in r181)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r180))
      )
    )
  (:action move-left-from-r180
    :parameters ()
    :precondition 
      (in r180)
    :effect
      (and
        (in r179)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r180))
      )
    )
  (:action move-right-from-r181
    :parameters ()
    :precondition 
      (in r181)
    :effect
      (and
        (in r182)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r181))
      )
    )
  (:action move-left-from-r181
    :parameters ()
    :precondition 
      (in r181)
    :effect
      (and
        (in r180)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r181))
      )
    )
  (:action move-right-from-r182
    :parameters ()
    :precondition 
      (in r182)
    :effect
      (and
        (in r183)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r182))
      )
    )
  (:action move-left-from-r182
    :parameters ()
    :precondition 
      (in r182)
    :effect
      (and
        (in r181)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r182))
      )
    )
  (:action move-right-from-r183
    :parameters ()
    :precondition 
      (in r183)
    :effect
      (and
        (in r184)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r183))
      )
    )
  (:action move-left-from-r183
    :parameters ()
    :precondition 
      (in r183)
    :effect
      (and
        (in r182)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r183))
      )
    )
  (:action move-right-from-r184
    :parameters ()
    :precondition 
      (in r184)
    :effect
      (and
        (in r185)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r184))
      )
    )
  (:action move-left-from-r184
    :parameters ()
    :precondition 
      (in r184)
    :effect
      (and
        (in r183)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r184))
      )
    )
  (:action move-right-from-r185
    :parameters ()
    :precondition 
      (in r185)
    :effect
      (and
        (in r186)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r185))
      )
    )
  (:action move-left-from-r185
    :parameters ()
    :precondition 
      (in r185)
    :effect
      (and
        (in r184)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r185))
      )
    )
  (:action move-right-from-r186
    :parameters ()
    :precondition 
      (in r186)
    :effect
      (and
        (in r187)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r186))
      )
    )
  (:action move-left-from-r186
    :parameters ()
    :precondition 
      (in r186)
    :effect
      (and
        (in r185)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r186))
      )
    )
  (:action move-right-from-r187
    :parameters ()
    :precondition 
      (in r187)
    :effect
      (and
        (in r188)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r187))
      )
    )
  (:action move-left-from-r187
    :parameters ()
    :precondition 
      (in r187)
    :effect
      (and
        (in r186)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r187))
      )
    )
  (:action move-right-from-r188
    :parameters ()
    :precondition 
      (in r188)
    :effect
      (and
        (in r189)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r188))
      )
    )
  (:action move-left-from-r188
    :parameters ()
    :precondition 
      (in r188)
    :effect
      (and
        (in r187)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r188))
      )
    )
  (:action move-right-from-r189
    :parameters ()
    :precondition 
      (in r189)
    :effect
      (and
        (in r190)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r189))
      )
    )
  (:action move-left-from-r189
    :parameters ()
    :precondition 
      (in r189)
    :effect
      (and
        (in r188)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r189))
      )
    )
  (:action move-right-from-r190
    :parameters ()
    :precondition 
      (in r190)
    :effect
      (and
        (in r191)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r190))
      )
    )
  (:action move-left-from-r190
    :parameters ()
    :precondition 
      (in r190)
    :effect
      (and
        (in r189)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r190))
      )
    )
  (:action move-right-from-r191
    :parameters ()
    :precondition 
      (in r191)
    :effect
      (and
        (in r192)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r191))
      )
    )
  (:action move-left-from-r191
    :parameters ()
    :precondition 
      (in r191)
    :effect
      (and
        (in r190)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r191))
      )
    )
  (:action move-right-from-r192
    :parameters ()
    :precondition 
      (in r192)
    :effect
      (and
        (in r193)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r192))
      )
    )
  (:action move-left-from-r192
    :parameters ()
    :precondition 
      (in r192)
    :effect
      (and
        (in r191)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r192))
      )
    )
  (:action move-right-from-r193
    :parameters ()
    :precondition 
      (in r193)
    :effect
      (and
        (in r194)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r193))
      )
    )
  (:action move-left-from-r193
    :parameters ()
    :precondition 
      (in r193)
    :effect
      (and
        (in r192)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r193))
      )
    )
  (:action move-right-from-r194
    :parameters ()
    :precondition 
      (in r194)
    :effect
      (and
        (in r195)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r194))
      )
    )
  (:action move-left-from-r194
    :parameters ()
    :precondition 
      (in r194)
    :effect
      (and
        (in r193)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r194))
      )
    )
  (:action move-right-from-r195
    :parameters ()
    :precondition 
      (in r195)
    :effect
      (and
        (in r196)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r195))
      )
    )
  (:action move-left-from-r195
    :parameters ()
    :precondition 
      (in r195)
    :effect
      (and
        (in r194)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r195))
      )
    )
  (:action move-right-from-r196
    :parameters ()
    :precondition 
      (in r196)
    :effect
      (and
        (in r197)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r196))
      )
    )
  (:action move-left-from-r196
    :parameters ()
    :precondition 
      (in r196)
    :effect
      (and
        (in r195)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r196))
      )
    )
  (:action move-right-from-r197
    :parameters ()
    :precondition 
      (in r197)
    :effect
      (and
        (in r198)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r197))
      )
    )
  (:action move-left-from-r197
    :parameters ()
    :precondition 
      (in r197)
    :effect
      (and
        (in r196)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r197))
      )
    )
  (:action move-right-from-r198
    :parameters ()
    :precondition 
      (in r198)
    :effect
      (and
        (in r199)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r198))
      )
    )
  (:action move-left-from-r198
    :parameters ()
    :precondition 
      (in r198)
    :effect
      (and
        (in r197)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r198))
      )
    )
  (:action move-right-from-r199
    :parameters ()
    :precondition 
      (in r199)
    :effect
      (and
        (in r200)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r199))
      )
    )
  (:action move-left-from-r199
    :parameters ()
    :precondition 
      (in r199)
    :effect
      (and
        (in r198)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r199))
      )
    )
  (:action move-right-from-r200
    :parameters ()
    :precondition 
      (in r200)
    :effect
      (and
        (in r201)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r200))
      )
    )
  (:action move-left-from-r200
    :parameters ()
    :precondition 
      (in r200)
    :effect
      (and
        (in r199)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r200))
      )
    )
  (:action move-right-from-r201
    :parameters ()
    :precondition 
      (in r201)
    :effect
      (and
        (in r202)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r201))
      )
    )
  (:action move-left-from-r201
    :parameters ()
    :precondition 
      (in r201)
    :effect
      (and
        (in r200)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r201))
      )
    )
  (:action move-right-from-r202
    :parameters ()
    :precondition 
      (in r202)
    :effect
      (and
        (in r203)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r202))
      )
    )
  (:action move-left-from-r202
    :parameters ()
    :precondition 
      (in r202)
    :effect
      (and
        (in r201)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r202))
      )
    )
  (:action move-right-from-r203
    :parameters ()
    :precondition 
      (in r203)
    :effect
      (and
        (in r204)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r203))
      )
    )
  (:action move-left-from-r203
    :parameters ()
    :precondition 
      (in r203)
    :effect
      (and
        (in r202)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r203))
      )
    )
  (:action move-right-from-r204
    :parameters ()
    :precondition 
      (in r204)
    :effect
      (and
        (in r205)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r204))
      )
    )
  (:action move-left-from-r204
    :parameters ()
    :precondition 
      (in r204)
    :effect
      (and
        (in r203)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r204))
      )
    )
  (:action move-right-from-r205
    :parameters ()
    :precondition 
      (in r205)
    :effect
      (and
        (in r206)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r205))
      )
    )
  (:action move-left-from-r205
    :parameters ()
    :precondition 
      (in r205)
    :effect
      (and
        (in r204)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r205))
      )
    )
  (:action move-right-from-r206
    :parameters ()
    :precondition 
      (in r206)
    :effect
      (and
        (in r207)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r206))
      )
    )
  (:action move-left-from-r206
    :parameters ()
    :precondition 
      (in r206)
    :effect
      (and
        (in r205)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r206))
      )
    )
  (:action move-right-from-r207
    :parameters ()
    :precondition 
      (in r207)
    :effect
      (and
        (in r208)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r207))
      )
    )
  (:action move-left-from-r207
    :parameters ()
    :precondition 
      (in r207)
    :effect
      (and
        (in r206)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r207))
      )
    )
  (:action move-right-from-r208
    :parameters ()
    :precondition 
      (in r208)
    :effect
      (and
        (in r209)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r208))
      )
    )
  (:action move-left-from-r208
    :parameters ()
    :precondition 
      (in r208)
    :effect
      (and
        (in r207)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r208))
      )
    )
  (:action move-right-from-r209
    :parameters ()
    :precondition 
      (in r209)
    :effect
      (and
        (in r210)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r209))
      )
    )
  (:action move-left-from-r209
    :parameters ()
    :precondition 
      (in r209)
    :effect
      (and
        (in r208)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r209))
      )
    )
  (:action move-right-from-r210
    :parameters ()
    :precondition 
      (in r210)
    :effect
      (and
        (in r211)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r210))
      )
    )
  (:action move-left-from-r210
    :parameters ()
    :precondition 
      (in r210)
    :effect
      (and
        (in r209)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r210))
      )
    )
  (:action move-right-from-r211
    :parameters ()
    :precondition 
      (in r211)
    :effect
      (and
        (in r212)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r211))
      )
    )
  (:action move-left-from-r211
    :parameters ()
    :precondition 
      (in r211)
    :effect
      (and
        (in r210)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r211))
      )
    )
  (:action move-right-from-r212
    :parameters ()
    :precondition 
      (in r212)
    :effect
      (and
        (in r213)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r212))
      )
    )
  (:action move-left-from-r212
    :parameters ()
    :precondition 
      (in r212)
    :effect
      (and
        (in r211)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r212))
      )
    )
  (:action move-right-from-r213
    :parameters ()
    :precondition 
      (in r213)
    :effect
      (and
        (in r214)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r213))
      )
    )
  (:action move-left-from-r213
    :parameters ()
    :precondition 
      (in r213)
    :effect
      (and
        (in r212)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r213))
      )
    )
  (:action move-right-from-r214
    :parameters ()
    :precondition 
      (in r214)
    :effect
      (and
        (in r215)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r214))
      )
    )
  (:action move-left-from-r214
    :parameters ()
    :precondition 
      (in r214)
    :effect
      (and
        (in r213)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r214))
      )
    )
  (:action move-right-from-r215
    :parameters ()
    :precondition 
      (in r215)
    :effect
      (and
        (in r216)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r215))
      )
    )
  (:action move-left-from-r215
    :parameters ()
    :precondition 
      (in r215)
    :effect
      (and
        (in r214)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r215))
      )
    )
  (:action move-right-from-r216
    :parameters ()
    :precondition 
      (in r216)
    :effect
      (and
        (in r217)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r216))
      )
    )
  (:action move-left-from-r216
    :parameters ()
    :precondition 
      (in r216)
    :effect
      (and
        (in r215)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r216))
      )
    )
  (:action move-right-from-r217
    :parameters ()
    :precondition 
      (in r217)
    :effect
      (and
        (in r218)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r217))
      )
    )
  (:action move-left-from-r217
    :parameters ()
    :precondition 
      (in r217)
    :effect
      (and
        (in r216)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r217))
      )
    )
  (:action move-right-from-r218
    :parameters ()
    :precondition 
      (in r218)
    :effect
      (and
        (in r219)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r218))
      )
    )
  (:action move-left-from-r218
    :parameters ()
    :precondition 
      (in r218)
    :effect
      (and
        (in r217)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r218))
      )
    )
  (:action move-right-from-r219
    :parameters ()
    :precondition 
      (in r219)
    :effect
      (and
        (in r220)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r219))
      )
    )
  (:action move-left-from-r219
    :parameters ()
    :precondition 
      (in r219)
    :effect
      (and
        (in r218)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r219))
      )
    )
  (:action move-right-from-r220
    :parameters ()
    :precondition 
      (in r220)
    :effect
      (and
        (in r221)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r220))
      )
    )
  (:action move-left-from-r220
    :parameters ()
    :precondition 
      (in r220)
    :effect
      (and
        (in r219)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r220))
      )
    )
  (:action move-right-from-r221
    :parameters ()
    :precondition 
      (in r221)
    :effect
      (and
        (in r222)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r221))
      )
    )
  (:action move-left-from-r221
    :parameters ()
    :precondition 
      (in r221)
    :effect
      (and
        (in r220)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r221))
      )
    )
  (:action move-right-from-r222
    :parameters ()
    :precondition 
      (in r222)
    :effect
      (and
        (in r223)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r222))
      )
    )
  (:action move-left-from-r222
    :parameters ()
    :precondition 
      (in r222)
    :effect
      (and
        (in r221)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r222))
      )
    )
  (:action move-right-from-r223
    :parameters ()
    :precondition 
      (in r223)
    :effect
      (and
        (in r224)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r223))
      )
    )
  (:action move-left-from-r223
    :parameters ()
    :precondition 
      (in r223)
    :effect
      (and
        (in r222)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r223))
      )
    )
  (:action move-right-from-r224
    :parameters ()
    :precondition 
      (in r224)
    :effect
      (and
        (in r225)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r224))
      )
    )
  (:action move-left-from-r224
    :parameters ()
    :precondition 
      (in r224)
    :effect
      (and
        (in r223)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r224))
      )
    )
  (:action move-right-from-r225
    :parameters ()
    :precondition 
      (in r225)
    :effect
      (and
        (in r226)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r225))
      )
    )
  (:action move-left-from-r225
    :parameters ()
    :precondition 
      (in r225)
    :effect
      (and
        (in r224)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r225))
      )
    )
  (:action move-right-from-r226
    :parameters ()
    :precondition 
      (in r226)
    :effect
      (and
        (in r227)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r226))
      )
    )
  (:action move-left-from-r226
    :parameters ()
    :precondition 
      (in r226)
    :effect
      (and
        (in r225)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r226))
      )
    )
  (:action move-right-from-r227
    :parameters ()
    :precondition 
      (in r227)
    :effect
      (and
        (in r228)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r227))
      )
    )
  (:action move-left-from-r227
    :parameters ()
    :precondition 
      (in r227)
    :effect
      (and
        (in r226)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r227))
      )
    )
  (:action move-right-from-r228
    :parameters ()
    :precondition 
      (in r228)
    :effect
      (and
        (in r229)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r228))
      )
    )
  (:action move-left-from-r228
    :parameters ()
    :precondition 
      (in r228)
    :effect
      (and
        (in r227)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r228))
      )
    )
  (:action move-right-from-r229
    :parameters ()
    :precondition 
      (in r229)
    :effect
      (and
        (in r230)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r229))
      )
    )
  (:action move-left-from-r229
    :parameters ()
    :precondition 
      (in r229)
    :effect
      (and
        (in r228)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r229))
      )
    )
  (:action move-right-from-r230
    :parameters ()
    :precondition 
      (in r230)
    :effect
      (and
        (in r231)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r230))
      )
    )
  (:action move-left-from-r230
    :parameters ()
    :precondition 
      (in r230)
    :effect
      (and
        (in r229)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r230))
      )
    )
  (:action move-right-from-r231
    :parameters ()
    :precondition 
      (in r231)
    :effect
      (and
        (in r232)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r231))
      )
    )
  (:action move-left-from-r231
    :parameters ()
    :precondition 
      (in r231)
    :effect
      (and
        (in r230)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r231))
      )
    )
  (:action move-right-from-r232
    :parameters ()
    :precondition 
      (in r232)
    :effect
      (and
        (in r233)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r232))
      )
    )
  (:action move-left-from-r232
    :parameters ()
    :precondition 
      (in r232)
    :effect
      (and
        (in r231)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r232))
      )
    )
  (:action move-right-from-r233
    :parameters ()
    :precondition 
      (in r233)
    :effect
      (and
        (in r234)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r233))
      )
    )
  (:action move-left-from-r233
    :parameters ()
    :precondition 
      (in r233)
    :effect
      (and
        (in r232)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r233))
      )
    )
  (:action move-right-from-r234
    :parameters ()
    :precondition 
      (in r234)
    :effect
      (and
        (in r235)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r234))
      )
    )
  (:action move-left-from-r234
    :parameters ()
    :precondition 
      (in r234)
    :effect
      (and
        (in r233)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r234))
      )
    )
  (:action move-right-from-r235
    :parameters ()
    :precondition 
      (in r235)
    :effect
      (and
        (in r236)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r235))
      )
    )
  (:action move-left-from-r235
    :parameters ()
    :precondition 
      (in r235)
    :effect
      (and
        (in r234)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r235))
      )
    )
  (:action move-right-from-r236
    :parameters ()
    :precondition 
      (in r236)
    :effect
      (and
        (in r237)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r236))
      )
    )
  (:action move-left-from-r236
    :parameters ()
    :precondition 
      (in r236)
    :effect
      (and
        (in r235)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r236))
      )
    )
  (:action move-right-from-r237
    :parameters ()
    :precondition 
      (in r237)
    :effect
      (and
        (in r238)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r237))
      )
    )
  (:action move-left-from-r237
    :parameters ()
    :precondition 
      (in r237)
    :effect
      (and
        (in r236)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r237))
      )
    )
  (:action move-right-from-r238
    :parameters ()
    :precondition 
      (in r238)
    :effect
      (and
        (in r239)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r238))
      )
    )
  (:action move-left-from-r238
    :parameters ()
    :precondition 
      (in r238)
    :effect
      (and
        (in r237)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r238))
      )
    )
  (:action move-right-from-r239
    :parameters ()
    :precondition 
      (in r239)
    :effect
      (and
        (in r240)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r239))
      )
    )
  (:action move-left-from-r239
    :parameters ()
    :precondition 
      (in r239)
    :effect
      (and
        (in r238)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r239))
      )
    )
  (:action move-right-from-r240
    :parameters ()
    :precondition 
      (in r240)
    :effect
      (and
        (in r241)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r240))
      )
    )
  (:action move-left-from-r240
    :parameters ()
    :precondition 
      (in r240)
    :effect
      (and
        (in r239)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r240))
      )
    )
  (:action move-right-from-r241
    :parameters ()
    :precondition 
      (in r241)
    :effect
      (and
        (in r242)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r241))
      )
    )
  (:action move-left-from-r241
    :parameters ()
    :precondition 
      (in r241)
    :effect
      (and
        (in r240)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r241))
      )
    )
  (:action move-right-from-r242
    :parameters ()
    :precondition 
      (in r242)
    :effect
      (and
        (in r243)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r242))
      )
    )
  (:action move-left-from-r242
    :parameters ()
    :precondition 
      (in r242)
    :effect
      (and
        (in r241)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r242))
      )
    )
  (:action move-right-from-r243
    :parameters ()
    :precondition 
      (in r243)
    :effect
      (and
        (in r244)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r243))
      )
    )
  (:action move-left-from-r243
    :parameters ()
    :precondition 
      (in r243)
    :effect
      (and
        (in r242)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r243))
      )
    )
  (:action move-right-from-r244
    :parameters ()
    :precondition 
      (in r244)
    :effect
      (and
        (in r245)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r244))
      )
    )
  (:action move-left-from-r244
    :parameters ()
    :precondition 
      (in r244)
    :effect
      (and
        (in r243)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r244))
      )
    )
  (:action move-right-from-r245
    :parameters ()
    :precondition 
      (in r245)
    :effect
      (and
        (in r246)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r245))
      )
    )
  (:action move-left-from-r245
    :parameters ()
    :precondition 
      (in r245)
    :effect
      (and
        (in r244)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r245))
      )
    )
  (:action move-right-from-r246
    :parameters ()
    :precondition 
      (in r246)
    :effect
      (and
        (in r247)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r246))
      )
    )
  (:action move-left-from-r246
    :parameters ()
    :precondition 
      (in r246)
    :effect
      (and
        (in r245)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r246))
      )
    )
  (:action move-right-from-r247
    :parameters ()
    :precondition 
      (in r247)
    :effect
      (and
        (in r248)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r247))
      )
    )
  (:action move-left-from-r247
    :parameters ()
    :precondition 
      (in r247)
    :effect
      (and
        (in r246)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r247))
      )
    )
  (:action move-right-from-r248
    :parameters ()
    :precondition 
      (in r248)
    :effect
      (and
        (in r249)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r248))
      )
    )
  (:action move-left-from-r248
    :parameters ()
    :precondition 
      (in r248)
    :effect
      (and
        (in r247)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r248))
      )
    )
  (:action move-right-from-r249
    :parameters ()
    :precondition 
      (in r249)
    :effect
      (and
        (in r250)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r249))
      )
    )
  (:action move-left-from-r249
    :parameters ()
    :precondition 
      (in r249)
    :effect
      (and
        (in r248)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r249))
      )
    )
  (:action move-right-from-r250
    :parameters ()
    :precondition 
      (in r250)
    :effect
      (and
        (in r251)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r250))
      )
    )
  (:action move-left-from-r250
    :parameters ()
    :precondition 
      (in r250)
    :effect
      (and
        (in r249)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r250))
      )
    )
  (:action move-right-from-r251
    :parameters ()
    :precondition 
      (in r251)
    :effect
      (and
        (in r252)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r251))
      )
    )
  (:action move-left-from-r251
    :parameters ()
    :precondition 
      (in r251)
    :effect
      (and
        (in r250)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r251))
      )
    )
  (:action move-right-from-r252
    :parameters ()
    :precondition 
      (in r252)
    :effect
      (and
        (in r253)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r252))
      )
    )
  (:action move-left-from-r252
    :parameters ()
    :precondition 
      (in r252)
    :effect
      (and
        (in r251)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r252))
      )
    )
  (:action move-right-from-r253
    :parameters ()
    :precondition 
      (in r253)
    :effect
      (and
        (in r254)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r253))
      )
    )
  (:action move-left-from-r253
    :parameters ()
    :precondition 
      (in r253)
    :effect
      (and
        (in r252)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r253))
      )
    )
  (:action move-right-from-r254
    :parameters ()
    :precondition 
      (in r254)
    :effect
      (and
        (in r255)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r254))
      )
    )
  (:action move-left-from-r254
    :parameters ()
    :precondition 
      (in r254)
    :effect
      (and
        (in r253)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r254))
      )
    )
  (:action move-right-from-r255
    :parameters ()
    :precondition 
      (in r255)
    :effect
      (and
        (in r256)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r255))
      )
    )
  (:action move-left-from-r255
    :parameters ()
    :precondition 
      (in r255)
    :effect
      (and
        (in r254)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r255))
      )
    )
  (:action move-right-from-r256
    :parameters ()
    :precondition 
      (in r256)
    :effect
      (and
        (in r257)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r256))
      )
    )
  (:action move-left-from-r256
    :parameters ()
    :precondition 
      (in r256)
    :effect
      (and
        (in r255)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r256))
      )
    )
  (:action move-right-from-r257
    :parameters ()
    :precondition 
      (in r257)
    :effect
      (and
        (in r258)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r257))
      )
    )
  (:action move-left-from-r257
    :parameters ()
    :precondition 
      (in r257)
    :effect
      (and
        (in r256)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r257))
      )
    )
  (:action move-right-from-r258
    :parameters ()
    :precondition 
      (in r258)
    :effect
      (and
        (in r259)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r258))
      )
    )
  (:action move-left-from-r258
    :parameters ()
    :precondition 
      (in r258)
    :effect
      (and
        (in r257)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r258))
      )
    )
  (:action move-right-from-r259
    :parameters ()
    :precondition 
      (in r259)
    :effect
      (and
        (in r260)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r259))
      )
    )
  (:action move-left-from-r259
    :parameters ()
    :precondition 
      (in r259)
    :effect
      (and
        (in r258)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r259))
      )
    )
  (:action move-right-from-r260
    :parameters ()
    :precondition 
      (in r260)
    :effect
      (and
        (in r261)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r260))
      )
    )
  (:action move-left-from-r260
    :parameters ()
    :precondition 
      (in r260)
    :effect
      (and
        (in r259)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r260))
      )
    )
  (:action move-right-from-r261
    :parameters ()
    :precondition 
      (in r261)
    :effect
      (and
        (in r262)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r261))
      )
    )
  (:action move-left-from-r261
    :parameters ()
    :precondition 
      (in r261)
    :effect
      (and
        (in r260)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r261))
      )
    )
  (:action move-right-from-r262
    :parameters ()
    :precondition 
      (in r262)
    :effect
      (and
        (in r263)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r262))
      )
    )
  (:action move-left-from-r262
    :parameters ()
    :precondition 
      (in r262)
    :effect
      (and
        (in r261)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r262))
      )
    )
  (:action move-right-from-r263
    :parameters ()
    :precondition 
      (in r263)
    :effect
      (and
        (in r264)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r263))
      )
    )
  (:action move-left-from-r263
    :parameters ()
    :precondition 
      (in r263)
    :effect
      (and
        (in r262)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r263))
      )
    )
  (:action move-right-from-r264
    :parameters ()
    :precondition 
      (in r264)
    :effect
      (and
        (in r265)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r264))
      )
    )
  (:action move-left-from-r264
    :parameters ()
    :precondition 
      (in r264)
    :effect
      (and
        (in r263)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r264))
      )
    )
  (:action move-right-from-r265
    :parameters ()
    :precondition 
      (in r265)
    :effect
      (and
        (in r266)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r265))
      )
    )
  (:action move-left-from-r265
    :parameters ()
    :precondition 
      (in r265)
    :effect
      (and
        (in r264)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r265))
      )
    )
  (:action move-right-from-r266
    :parameters ()
    :precondition 
      (in r266)
    :effect
      (and
        (in r267)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r266))
      )
    )
  (:action move-left-from-r266
    :parameters ()
    :precondition 
      (in r266)
    :effect
      (and
        (in r265)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r266))
      )
    )
  (:action move-right-from-r267
    :parameters ()
    :precondition 
      (in r267)
    :effect
      (and
        (in r268)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r267))
      )
    )
  (:action move-left-from-r267
    :parameters ()
    :precondition 
      (in r267)
    :effect
      (and
        (in r266)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r267))
      )
    )
  (:action move-right-from-r268
    :parameters ()
    :precondition 
      (in r268)
    :effect
      (and
        (in r269)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r268))
      )
    )
  (:action move-left-from-r268
    :parameters ()
    :precondition 
      (in r268)
    :effect
      (and
        (in r267)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r268))
      )
    )
  (:action move-right-from-r269
    :parameters ()
    :precondition 
      (in r269)
    :effect
      (and
        (in r270)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r269))
      )
    )
  (:action move-left-from-r269
    :parameters ()
    :precondition 
      (in r269)
    :effect
      (and
        (in r268)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r269))
      )
    )
  (:action move-right-from-r270
    :parameters ()
    :precondition 
      (in r270)
    :effect
      (and
        (in r271)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r270))
      )
    )
  (:action move-left-from-r270
    :parameters ()
    :precondition 
      (in r270)
    :effect
      (and
        (in r269)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r270))
      )
    )
  (:action move-right-from-r271
    :parameters ()
    :precondition 
      (in r271)
    :effect
      (and
        (in r272)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r271))
      )
    )
  (:action move-left-from-r271
    :parameters ()
    :precondition 
      (in r271)
    :effect
      (and
        (in r270)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r271))
      )
    )
  (:action move-right-from-r272
    :parameters ()
    :precondition 
      (in r272)
    :effect
      (and
        (in r273)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r272))
      )
    )
  (:action move-left-from-r272
    :parameters ()
    :precondition 
      (in r272)
    :effect
      (and
        (in r271)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r272))
      )
    )
  (:action move-right-from-r273
    :parameters ()
    :precondition 
      (in r273)
    :effect
      (and
        (in r274)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r273))
      )
    )
  (:action move-left-from-r273
    :parameters ()
    :precondition 
      (in r273)
    :effect
      (and
        (in r272)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r273))
      )
    )
  (:action move-right-from-r274
    :parameters ()
    :precondition 
      (in r274)
    :effect
      (and
        (in r275)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r274))
      )
    )
  (:action move-left-from-r274
    :parameters ()
    :precondition 
      (in r274)
    :effect
      (and
        (in r273)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r274))
      )
    )
  (:action move-right-from-r275
    :parameters ()
    :precondition 
      (in r275)
    :effect
      (and
        (in r276)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r275))
      )
    )
  (:action move-left-from-r275
    :parameters ()
    :precondition 
      (in r275)
    :effect
      (and
        (in r274)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r275))
      )
    )
  (:action move-right-from-r276
    :parameters ()
    :precondition 
      (in r276)
    :effect
      (and
        (in r277)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r276))
      )
    )
  (:action move-left-from-r276
    :parameters ()
    :precondition 
      (in r276)
    :effect
      (and
        (in r275)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r276))
      )
    )
  (:action move-right-from-r277
    :parameters ()
    :precondition 
      (in r277)
    :effect
      (and
        (in r278)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r277))
      )
    )
  (:action move-left-from-r277
    :parameters ()
    :precondition 
      (in r277)
    :effect
      (and
        (in r276)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r277))
      )
    )
  (:action move-right-from-r278
    :parameters ()
    :precondition 
      (in r278)
    :effect
      (and
        (in r279)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r278))
      )
    )
  (:action move-left-from-r278
    :parameters ()
    :precondition 
      (in r278)
    :effect
      (and
        (in r277)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r278))
      )
    )
  (:action move-right-from-r279
    :parameters ()
    :precondition 
      (in r279)
    :effect
      (and
        (in r280)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r279))
      )
    )
  (:action move-left-from-r279
    :parameters ()
    :precondition 
      (in r279)
    :effect
      (and
        (in r278)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r279))
      )
    )
  (:action move-right-from-r280
    :parameters ()
    :precondition 
      (in r280)
    :effect
      (and
        (in r281)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r280))
      )
    )
  (:action move-left-from-r280
    :parameters ()
    :precondition 
      (in r280)
    :effect
      (and
        (in r279)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r280))
      )
    )
  (:action move-right-from-r281
    :parameters ()
    :precondition 
      (in r281)
    :effect
      (and
        (in r282)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r281))
      )
    )
  (:action move-left-from-r281
    :parameters ()
    :precondition 
      (in r281)
    :effect
      (and
        (in r280)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r281))
      )
    )
  (:action move-right-from-r282
    :parameters ()
    :precondition 
      (in r282)
    :effect
      (and
        (in r283)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r282))
      )
    )
  (:action move-left-from-r282
    :parameters ()
    :precondition 
      (in r282)
    :effect
      (and
        (in r281)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r282))
      )
    )
  (:action move-right-from-r283
    :parameters ()
    :precondition 
      (in r283)
    :effect
      (and
        (in r284)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r283))
      )
    )
  (:action move-left-from-r283
    :parameters ()
    :precondition 
      (in r283)
    :effect
      (and
        (in r282)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r283))
      )
    )
  (:action move-right-from-r284
    :parameters ()
    :precondition 
      (in r284)
    :effect
      (and
        (in r285)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r284))
      )
    )
  (:action move-left-from-r284
    :parameters ()
    :precondition 
      (in r284)
    :effect
      (and
        (in r283)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r284))
      )
    )
  (:action move-right-from-r285
    :parameters ()
    :precondition 
      (in r285)
    :effect
      (and
        (in r286)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r285))
      )
    )
  (:action move-left-from-r285
    :parameters ()
    :precondition 
      (in r285)
    :effect
      (and
        (in r284)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r285))
      )
    )
  (:action move-right-from-r286
    :parameters ()
    :precondition 
      (in r286)
    :effect
      (and
        (in r287)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r286))
      )
    )
  (:action move-left-from-r286
    :parameters ()
    :precondition 
      (in r286)
    :effect
      (and
        (in r285)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r286))
      )
    )
  (:action move-right-from-r287
    :parameters ()
    :precondition 
      (in r287)
    :effect
      (and
        (in r288)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r287))
      )
    )
  (:action move-left-from-r287
    :parameters ()
    :precondition 
      (in r287)
    :effect
      (and
        (in r286)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r287))
      )
    )
  (:action move-right-from-r288
    :parameters ()
    :precondition 
      (in r288)
    :effect
      (and
        (in r289)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r288))
      )
    )
  (:action move-left-from-r288
    :parameters ()
    :precondition 
      (in r288)
    :effect
      (and
        (in r287)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r288))
      )
    )
  (:action move-right-from-r289
    :parameters ()
    :precondition 
      (in r289)
    :effect
      (and
        (in r290)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r289))
      )
    )
  (:action move-left-from-r289
    :parameters ()
    :precondition 
      (in r289)
    :effect
      (and
        (in r288)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r289))
      )
    )
  (:action move-right-from-r290
    :parameters ()
    :precondition 
      (in r290)
    :effect
      (and
        (in r291)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r290))
      )
    )
  (:action move-left-from-r290
    :parameters ()
    :precondition 
      (in r290)
    :effect
      (and
        (in r289)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r290))
      )
    )
  (:action move-right-from-r291
    :parameters ()
    :precondition 
      (in r291)
    :effect
      (and
        (in r292)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r291))
      )
    )
  (:action move-left-from-r291
    :parameters ()
    :precondition 
      (in r291)
    :effect
      (and
        (in r290)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r291))
      )
    )
  (:action move-right-from-r292
    :parameters ()
    :precondition 
      (in r292)
    :effect
      (and
        (in r293)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r292))
      )
    )
  (:action move-left-from-r292
    :parameters ()
    :precondition 
      (in r292)
    :effect
      (and
        (in r291)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r292))
      )
    )
  (:action move-right-from-r293
    :parameters ()
    :precondition 
      (in r293)
    :effect
      (and
        (in r294)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r293))
      )
    )
  (:action move-left-from-r293
    :parameters ()
    :precondition 
      (in r293)
    :effect
      (and
        (in r292)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r293))
      )
    )
  (:action move-right-from-r294
    :parameters ()
    :precondition 
      (in r294)
    :effect
      (and
        (in r295)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r294))
      )
    )
  (:action move-left-from-r294
    :parameters ()
    :precondition 
      (in r294)
    :effect
      (and
        (in r293)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r294))
      )
    )
  (:action move-right-from-r295
    :parameters ()
    :precondition 
      (in r295)
    :effect
      (and
        (in r296)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r295))
      )
    )
  (:action move-left-from-r295
    :parameters ()
    :precondition 
      (in r295)
    :effect
      (and
        (in r294)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r295))
      )
    )
  (:action move-right-from-r296
    :parameters ()
    :precondition 
      (in r296)
    :effect
      (and
        (in r297)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r296))
      )
    )
  (:action move-left-from-r296
    :parameters ()
    :precondition 
      (in r296)
    :effect
      (and
        (in r295)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r296))
      )
    )
  (:action move-right-from-r297
    :parameters ()
    :precondition 
      (in r297)
    :effect
      (and
        (in r298)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r297))
      )
    )
  (:action move-left-from-r297
    :parameters ()
    :precondition 
      (in r297)
    :effect
      (and
        (in r296)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r297))
      )
    )
  (:action move-right-from-r298
    :parameters ()
    :precondition 
      (in r298)
    :effect
      (and
        (in r299)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r298))
      )
    )
  (:action move-left-from-r298
    :parameters ()
    :precondition 
      (in r298)
    :effect
      (and
        (in r297)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r298))
      )
    )
  (:action move-right-from-r299_detdup_0
    :parameters ()
    :precondition 
      (in r299)
    :effect
      (and
        (in r300)
        (not 
          (in r299))
        (not 
          (seen))
      )
    )
  (:action move-right-from-r299_detdup_1
    :parameters ()
    :precondition 
      (in r299)
    :effect
      (and
        (in r300)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r299))
      )
    )
  (:action move-left-from-r299
    :parameters ()
    :precondition 
      (in r299)
    :effect
      (and
        (in r298)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r299))
      )
    )
  (:action move-right-from-r300
    :parameters ()
    :precondition 
      (and
        (in r300)
        (not 
          (seen)))
    :effect
      (and
        (in r301)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r300))
      )
    )
  (:action move-left-from-r300
    :parameters ()
    :precondition 
      (and
        (in r300)
        (not 
          (seen)))
    :effect
      (and
        (in r299)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r300))
      )
    )
  (:action move-right-from-r301
    :parameters ()
    :precondition 
      (in r301)
    :effect
      (and
        (in r302)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r301))
      )
    )
  (:action move-left-from-r301_detdup_0
    :parameters ()
    :precondition 
      (in r301)
    :effect
      (and
        (in r300)
        (not 
          (in r301))
        (not 
          (seen))
      )
    )
  (:action move-left-from-r301_detdup_1
    :parameters ()
    :precondition 
      (in r301)
    :effect
      (and
        (in r300)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r301))
      )
    )
  (:action move-right-from-r302
    :parameters ()
    :precondition 
      (in r302)
    :effect
      (and
        (in r303)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r302))
      )
    )
  (:action move-left-from-r302
    :parameters ()
    :precondition 
      (in r302)
    :effect
      (and
        (in r301)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r302))
      )
    )
  (:action move-right-from-r303
    :parameters ()
    :precondition 
      (in r303)
    :effect
      (and
        (in r304)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r303))
      )
    )
  (:action move-left-from-r303
    :parameters ()
    :precondition 
      (in r303)
    :effect
      (and
        (in r302)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r303))
      )
    )
  (:action move-right-from-r304
    :parameters ()
    :precondition 
      (in r304)
    :effect
      (and
        (in r305)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r304))
      )
    )
  (:action move-left-from-r304
    :parameters ()
    :precondition 
      (in r304)
    :effect
      (and
        (in r303)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r304))
      )
    )
  (:action move-right-from-r305
    :parameters ()
    :precondition 
      (in r305)
    :effect
      (and
        (in r306)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r305))
      )
    )
  (:action move-left-from-r305
    :parameters ()
    :precondition 
      (in r305)
    :effect
      (and
        (in r304)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r305))
      )
    )
  (:action move-right-from-r306
    :parameters ()
    :precondition 
      (in r306)
    :effect
      (and
        (in r307)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r306))
      )
    )
  (:action move-left-from-r306
    :parameters ()
    :precondition 
      (in r306)
    :effect
      (and
        (in r305)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r306))
      )
    )
  (:action move-right-from-r307
    :parameters ()
    :precondition 
      (in r307)
    :effect
      (and
        (in r308)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r307))
      )
    )
  (:action move-left-from-r307
    :parameters ()
    :precondition 
      (in r307)
    :effect
      (and
        (in r306)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r307))
      )
    )
  (:action move-right-from-r308
    :parameters ()
    :precondition 
      (in r308)
    :effect
      (and
        (in r309)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r308))
      )
    )
  (:action move-left-from-r308
    :parameters ()
    :precondition 
      (in r308)
    :effect
      (and
        (in r307)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r308))
      )
    )
  (:action move-right-from-r309
    :parameters ()
    :precondition 
      (in r309)
    :effect
      (and
        (in r310)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r309))
      )
    )
  (:action move-left-from-r309
    :parameters ()
    :precondition 
      (in r309)
    :effect
      (and
        (in r308)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r309))
      )
    )
  (:action move-right-from-r310
    :parameters ()
    :precondition 
      (in r310)
    :effect
      (and
        (in r311)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r310))
      )
    )
  (:action move-left-from-r310
    :parameters ()
    :precondition 
      (in r310)
    :effect
      (and
        (in r309)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r310))
      )
    )
  (:action move-right-from-r311
    :parameters ()
    :precondition 
      (in r311)
    :effect
      (and
        (in r312)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r311))
      )
    )
  (:action move-left-from-r311
    :parameters ()
    :precondition 
      (in r311)
    :effect
      (and
        (in r310)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r311))
      )
    )
  (:action move-right-from-r312
    :parameters ()
    :precondition 
      (in r312)
    :effect
      (and
        (in r313)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r312))
      )
    )
  (:action move-left-from-r312
    :parameters ()
    :precondition 
      (in r312)
    :effect
      (and
        (in r311)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r312))
      )
    )
  (:action move-right-from-r313
    :parameters ()
    :precondition 
      (in r313)
    :effect
      (and
        (in r314)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r313))
      )
    )
  (:action move-left-from-r313
    :parameters ()
    :precondition 
      (in r313)
    :effect
      (and
        (in r312)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r313))
      )
    )
  (:action move-right-from-r314
    :parameters ()
    :precondition 
      (in r314)
    :effect
      (and
        (in r315)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r314))
      )
    )
  (:action move-left-from-r314
    :parameters ()
    :precondition 
      (in r314)
    :effect
      (and
        (in r313)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r314))
      )
    )
  (:action move-right-from-r315
    :parameters ()
    :precondition 
      (in r315)
    :effect
      (and
        (in r316)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r315))
      )
    )
  (:action move-left-from-r315
    :parameters ()
    :precondition 
      (in r315)
    :effect
      (and
        (in r314)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r315))
      )
    )
  (:action move-right-from-r316
    :parameters ()
    :precondition 
      (in r316)
    :effect
      (and
        (in r317)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r316))
      )
    )
  (:action move-left-from-r316
    :parameters ()
    :precondition 
      (in r316)
    :effect
      (and
        (in r315)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r316))
      )
    )
  (:action move-right-from-r317
    :parameters ()
    :precondition 
      (in r317)
    :effect
      (and
        (in r318)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r317))
      )
    )
  (:action move-left-from-r317
    :parameters ()
    :precondition 
      (in r317)
    :effect
      (and
        (in r316)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r317))
      )
    )
  (:action move-right-from-r318
    :parameters ()
    :precondition 
      (in r318)
    :effect
      (and
        (in r319)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r318))
      )
    )
  (:action move-left-from-r318
    :parameters ()
    :precondition 
      (in r318)
    :effect
      (and
        (in r317)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r318))
      )
    )
  (:action move-right-from-r319
    :parameters ()
    :precondition 
      (in r319)
    :effect
      (and
        (in r320)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r319))
      )
    )
  (:action move-left-from-r319
    :parameters ()
    :precondition 
      (in r319)
    :effect
      (and
        (in r318)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r319))
      )
    )
  (:action move-right-from-r320
    :parameters ()
    :precondition 
      (in r320)
    :effect
      (and
        (in r321)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r320))
      )
    )
  (:action move-left-from-r320
    :parameters ()
    :precondition 
      (in r320)
    :effect
      (and
        (in r319)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r320))
      )
    )
  (:action move-right-from-r321
    :parameters ()
    :precondition 
      (in r321)
    :effect
      (and
        (in r322)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r321))
      )
    )
  (:action move-left-from-r321
    :parameters ()
    :precondition 
      (in r321)
    :effect
      (and
        (in r320)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r321))
      )
    )
  (:action move-right-from-r322
    :parameters ()
    :precondition 
      (in r322)
    :effect
      (and
        (in r323)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r322))
      )
    )
  (:action move-left-from-r322
    :parameters ()
    :precondition 
      (in r322)
    :effect
      (and
        (in r321)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r322))
      )
    )
  (:action move-right-from-r323
    :parameters ()
    :precondition 
      (in r323)
    :effect
      (and
        (in r324)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r323))
      )
    )
  (:action move-left-from-r323
    :parameters ()
    :precondition 
      (in r323)
    :effect
      (and
        (in r322)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r323))
      )
    )
  (:action move-right-from-r324
    :parameters ()
    :precondition 
      (in r324)
    :effect
      (and
        (in r325)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r324))
      )
    )
  (:action move-left-from-r324
    :parameters ()
    :precondition 
      (in r324)
    :effect
      (and
        (in r323)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r324))
      )
    )
  (:action move-right-from-r325
    :parameters ()
    :precondition 
      (in r325)
    :effect
      (and
        (in r326)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r325))
      )
    )
  (:action move-left-from-r325
    :parameters ()
    :precondition 
      (in r325)
    :effect
      (and
        (in r324)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r325))
      )
    )
  (:action move-right-from-r326
    :parameters ()
    :precondition 
      (in r326)
    :effect
      (and
        (in r327)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r326))
      )
    )
  (:action move-left-from-r326
    :parameters ()
    :precondition 
      (in r326)
    :effect
      (and
        (in r325)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r326))
      )
    )
  (:action move-right-from-r327
    :parameters ()
    :precondition 
      (in r327)
    :effect
      (and
        (in r328)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r327))
      )
    )
  (:action move-left-from-r327
    :parameters ()
    :precondition 
      (in r327)
    :effect
      (and
        (in r326)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r327))
      )
    )
  (:action move-right-from-r328
    :parameters ()
    :precondition 
      (in r328)
    :effect
      (and
        (in r329)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r328))
      )
    )
  (:action move-left-from-r328
    :parameters ()
    :precondition 
      (in r328)
    :effect
      (and
        (in r327)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r328))
      )
    )
  (:action move-right-from-r329
    :parameters ()
    :precondition 
      (in r329)
    :effect
      (and
        (in r330)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r329))
      )
    )
  (:action move-left-from-r329
    :parameters ()
    :precondition 
      (in r329)
    :effect
      (and
        (in r328)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r329))
      )
    )
  (:action move-right-from-r330
    :parameters ()
    :precondition 
      (in r330)
    :effect
      (and
        (in r331)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r330))
      )
    )
  (:action move-left-from-r330
    :parameters ()
    :precondition 
      (in r330)
    :effect
      (and
        (in r329)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r330))
      )
    )
  (:action move-right-from-r331
    :parameters ()
    :precondition 
      (in r331)
    :effect
      (and
        (in r332)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r331))
      )
    )
  (:action move-left-from-r331
    :parameters ()
    :precondition 
      (in r331)
    :effect
      (and
        (in r330)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r331))
      )
    )
  (:action move-right-from-r332
    :parameters ()
    :precondition 
      (in r332)
    :effect
      (and
        (in r333)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r332))
      )
    )
  (:action move-left-from-r332
    :parameters ()
    :precondition 
      (in r332)
    :effect
      (and
        (in r331)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r332))
      )
    )
  (:action move-right-from-r333
    :parameters ()
    :precondition 
      (in r333)
    :effect
      (and
        (in r334)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r333))
      )
    )
  (:action move-left-from-r333
    :parameters ()
    :precondition 
      (in r333)
    :effect
      (and
        (in r332)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r333))
      )
    )
  (:action move-right-from-r334
    :parameters ()
    :precondition 
      (in r334)
    :effect
      (and
        (in r335)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r334))
      )
    )
  (:action move-left-from-r334
    :parameters ()
    :precondition 
      (in r334)
    :effect
      (and
        (in r333)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r334))
      )
    )
  (:action move-right-from-r335
    :parameters ()
    :precondition 
      (in r335)
    :effect
      (and
        (in r336)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r335))
      )
    )
  (:action move-left-from-r335
    :parameters ()
    :precondition 
      (in r335)
    :effect
      (and
        (in r334)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r335))
      )
    )
  (:action move-right-from-r336
    :parameters ()
    :precondition 
      (in r336)
    :effect
      (and
        (in r337)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r336))
      )
    )
  (:action move-left-from-r336
    :parameters ()
    :precondition 
      (in r336)
    :effect
      (and
        (in r335)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r336))
      )
    )
  (:action move-right-from-r337
    :parameters ()
    :precondition 
      (in r337)
    :effect
      (and
        (in r338)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r337))
      )
    )
  (:action move-left-from-r337
    :parameters ()
    :precondition 
      (in r337)
    :effect
      (and
        (in r336)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r337))
      )
    )
  (:action move-right-from-r338
    :parameters ()
    :precondition 
      (in r338)
    :effect
      (and
        (in r339)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r338))
      )
    )
  (:action move-left-from-r338
    :parameters ()
    :precondition 
      (in r338)
    :effect
      (and
        (in r337)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r338))
      )
    )
  (:action move-right-from-r339
    :parameters ()
    :precondition 
      (in r339)
    :effect
      (and
        (in r340)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r339))
      )
    )
  (:action move-left-from-r339
    :parameters ()
    :precondition 
      (in r339)
    :effect
      (and
        (in r338)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r339))
      )
    )
  (:action move-right-from-r340
    :parameters ()
    :precondition 
      (in r340)
    :effect
      (and
        (in r341)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r340))
      )
    )
  (:action move-left-from-r340
    :parameters ()
    :precondition 
      (in r340)
    :effect
      (and
        (in r339)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r340))
      )
    )
  (:action move-right-from-r341
    :parameters ()
    :precondition 
      (in r341)
    :effect
      (and
        (in r342)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r341))
      )
    )
  (:action move-left-from-r341
    :parameters ()
    :precondition 
      (in r341)
    :effect
      (and
        (in r340)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r341))
      )
    )
  (:action move-right-from-r342
    :parameters ()
    :precondition 
      (in r342)
    :effect
      (and
        (in r343)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r342))
      )
    )
  (:action move-left-from-r342
    :parameters ()
    :precondition 
      (in r342)
    :effect
      (and
        (in r341)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r342))
      )
    )
  (:action move-right-from-r343
    :parameters ()
    :precondition 
      (in r343)
    :effect
      (and
        (in r344)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r343))
      )
    )
  (:action move-left-from-r343
    :parameters ()
    :precondition 
      (in r343)
    :effect
      (and
        (in r342)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r343))
      )
    )
  (:action move-right-from-r344
    :parameters ()
    :precondition 
      (in r344)
    :effect
      (and
        (in r345)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r344))
      )
    )
  (:action move-left-from-r344
    :parameters ()
    :precondition 
      (in r344)
    :effect
      (and
        (in r343)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r344))
      )
    )
  (:action move-right-from-r345
    :parameters ()
    :precondition 
      (in r345)
    :effect
      (and
        (in r346)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r345))
      )
    )
  (:action move-left-from-r345
    :parameters ()
    :precondition 
      (in r345)
    :effect
      (and
        (in r344)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r345))
      )
    )
  (:action move-right-from-r346
    :parameters ()
    :precondition 
      (in r346)
    :effect
      (and
        (in r347)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r346))
      )
    )
  (:action move-left-from-r346
    :parameters ()
    :precondition 
      (in r346)
    :effect
      (and
        (in r345)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r346))
      )
    )
  (:action move-right-from-r347
    :parameters ()
    :precondition 
      (in r347)
    :effect
      (and
        (in r348)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r347))
      )
    )
  (:action move-left-from-r347
    :parameters ()
    :precondition 
      (in r347)
    :effect
      (and
        (in r346)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r347))
      )
    )
  (:action move-right-from-r348
    :parameters ()
    :precondition 
      (in r348)
    :effect
      (and
        (in r349)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r348))
      )
    )
  (:action move-left-from-r348
    :parameters ()
    :precondition 
      (in r348)
    :effect
      (and
        (in r347)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r348))
      )
    )
  (:action move-right-from-r349
    :parameters ()
    :precondition 
      (in r349)
    :effect
      (and
        (in r350)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r349))
      )
    )
  (:action move-left-from-r349
    :parameters ()
    :precondition 
      (in r349)
    :effect
      (and
        (in r348)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r349))
      )
    )
  (:action move-right-from-r350
    :parameters ()
    :precondition 
      (in r350)
    :effect
      (and
        (in r351)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r350))
      )
    )
  (:action move-left-from-r350
    :parameters ()
    :precondition 
      (in r350)
    :effect
      (and
        (in r349)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r350))
      )
    )
  (:action move-right-from-r351
    :parameters ()
    :precondition 
      (in r351)
    :effect
      (and
        (in r352)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r351))
      )
    )
  (:action move-left-from-r351
    :parameters ()
    :precondition 
      (in r351)
    :effect
      (and
        (in r350)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r351))
      )
    )
  (:action move-right-from-r352
    :parameters ()
    :precondition 
      (in r352)
    :effect
      (and
        (in r353)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r352))
      )
    )
  (:action move-left-from-r352
    :parameters ()
    :precondition 
      (in r352)
    :effect
      (and
        (in r351)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r352))
      )
    )
  (:action move-right-from-r353
    :parameters ()
    :precondition 
      (in r353)
    :effect
      (and
        (in r354)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r353))
      )
    )
  (:action move-left-from-r353
    :parameters ()
    :precondition 
      (in r353)
    :effect
      (and
        (in r352)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r353))
      )
    )
  (:action move-right-from-r354
    :parameters ()
    :precondition 
      (in r354)
    :effect
      (and
        (in r355)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r354))
      )
    )
  (:action move-left-from-r354
    :parameters ()
    :precondition 
      (in r354)
    :effect
      (and
        (in r353)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r354))
      )
    )
  (:action move-right-from-r355
    :parameters ()
    :precondition 
      (in r355)
    :effect
      (and
        (in r356)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r355))
      )
    )
  (:action move-left-from-r355
    :parameters ()
    :precondition 
      (in r355)
    :effect
      (and
        (in r354)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r355))
      )
    )
  (:action move-right-from-r356
    :parameters ()
    :precondition 
      (in r356)
    :effect
      (and
        (in r357)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r356))
      )
    )
  (:action move-left-from-r356
    :parameters ()
    :precondition 
      (in r356)
    :effect
      (and
        (in r355)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r356))
      )
    )
  (:action move-right-from-r357
    :parameters ()
    :precondition 
      (in r357)
    :effect
      (and
        (in r358)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r357))
      )
    )
  (:action move-left-from-r357
    :parameters ()
    :precondition 
      (in r357)
    :effect
      (and
        (in r356)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r357))
      )
    )
  (:action move-right-from-r358
    :parameters ()
    :precondition 
      (in r358)
    :effect
      (and
        (in r359)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r358))
      )
    )
  (:action move-left-from-r358
    :parameters ()
    :precondition 
      (in r358)
    :effect
      (and
        (in r357)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r358))
      )
    )
  (:action move-right-from-r359
    :parameters ()
    :precondition 
      (in r359)
    :effect
      (and
        (in r360)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r359))
      )
    )
  (:action move-left-from-r359
    :parameters ()
    :precondition 
      (in r359)
    :effect
      (and
        (in r358)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r359))
      )
    )
  (:action move-right-from-r360
    :parameters ()
    :precondition 
      (in r360)
    :effect
      (and
        (in r361)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r360))
      )
    )
  (:action move-left-from-r360
    :parameters ()
    :precondition 
      (in r360)
    :effect
      (and
        (in r359)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r360))
      )
    )
  (:action move-right-from-r361
    :parameters ()
    :precondition 
      (in r361)
    :effect
      (and
        (in r362)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r361))
      )
    )
  (:action move-left-from-r361
    :parameters ()
    :precondition 
      (in r361)
    :effect
      (and
        (in r360)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r361))
      )
    )
  (:action move-right-from-r362
    :parameters ()
    :precondition 
      (in r362)
    :effect
      (and
        (in r363)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r362))
      )
    )
  (:action move-left-from-r362
    :parameters ()
    :precondition 
      (in r362)
    :effect
      (and
        (in r361)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r362))
      )
    )
  (:action move-right-from-r363
    :parameters ()
    :precondition 
      (in r363)
    :effect
      (and
        (in r364)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r363))
      )
    )
  (:action move-left-from-r363
    :parameters ()
    :precondition 
      (in r363)
    :effect
      (and
        (in r362)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r363))
      )
    )
  (:action move-right-from-r364
    :parameters ()
    :precondition 
      (in r364)
    :effect
      (and
        (in r365)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r364))
      )
    )
  (:action move-left-from-r364
    :parameters ()
    :precondition 
      (in r364)
    :effect
      (and
        (in r363)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r364))
      )
    )
  (:action move-right-from-r365
    :parameters ()
    :precondition 
      (in r365)
    :effect
      (and
        (in r366)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r365))
      )
    )
  (:action move-left-from-r365
    :parameters ()
    :precondition 
      (in r365)
    :effect
      (and
        (in r364)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r365))
      )
    )
  (:action move-right-from-r366
    :parameters ()
    :precondition 
      (in r366)
    :effect
      (and
        (in r367)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r366))
      )
    )
  (:action move-left-from-r366
    :parameters ()
    :precondition 
      (in r366)
    :effect
      (and
        (in r365)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r366))
      )
    )
  (:action move-right-from-r367
    :parameters ()
    :precondition 
      (in r367)
    :effect
      (and
        (in r368)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r367))
      )
    )
  (:action move-left-from-r367
    :parameters ()
    :precondition 
      (in r367)
    :effect
      (and
        (in r366)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r367))
      )
    )
  (:action move-right-from-r368
    :parameters ()
    :precondition 
      (in r368)
    :effect
      (and
        (in r369)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r368))
      )
    )
  (:action move-left-from-r368
    :parameters ()
    :precondition 
      (in r368)
    :effect
      (and
        (in r367)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r368))
      )
    )
  (:action move-right-from-r369
    :parameters ()
    :precondition 
      (in r369)
    :effect
      (and
        (in r370)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r369))
      )
    )
  (:action move-left-from-r369
    :parameters ()
    :precondition 
      (in r369)
    :effect
      (and
        (in r368)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r369))
      )
    )
  (:action move-right-from-r370
    :parameters ()
    :precondition 
      (in r370)
    :effect
      (and
        (in r371)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r370))
      )
    )
  (:action move-left-from-r370
    :parameters ()
    :precondition 
      (in r370)
    :effect
      (and
        (in r369)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r370))
      )
    )
  (:action move-right-from-r371
    :parameters ()
    :precondition 
      (in r371)
    :effect
      (and
        (in r372)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r371))
      )
    )
  (:action move-left-from-r371
    :parameters ()
    :precondition 
      (in r371)
    :effect
      (and
        (in r370)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r371))
      )
    )
  (:action move-right-from-r372
    :parameters ()
    :precondition 
      (in r372)
    :effect
      (and
        (in r373)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r372))
      )
    )
  (:action move-left-from-r372
    :parameters ()
    :precondition 
      (in r372)
    :effect
      (and
        (in r371)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r372))
      )
    )
  (:action move-right-from-r373
    :parameters ()
    :precondition 
      (in r373)
    :effect
      (and
        (in r374)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r373))
      )
    )
  (:action move-left-from-r373
    :parameters ()
    :precondition 
      (in r373)
    :effect
      (and
        (in r372)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r373))
      )
    )
  (:action move-right-from-r374
    :parameters ()
    :precondition 
      (in r374)
    :effect
      (and
        (in r375)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r374))
      )
    )
  (:action move-left-from-r374
    :parameters ()
    :precondition 
      (in r374)
    :effect
      (and
        (in r373)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r374))
      )
    )
  (:action move-right-from-r375
    :parameters ()
    :precondition 
      (in r375)
    :effect
      (and
        (in r376)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r375))
      )
    )
  (:action move-left-from-r375
    :parameters ()
    :precondition 
      (in r375)
    :effect
      (and
        (in r374)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r375))
      )
    )
  (:action move-right-from-r376
    :parameters ()
    :precondition 
      (in r376)
    :effect
      (and
        (in r377)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r376))
      )
    )
  (:action move-left-from-r376
    :parameters ()
    :precondition 
      (in r376)
    :effect
      (and
        (in r375)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r376))
      )
    )
  (:action move-right-from-r377
    :parameters ()
    :precondition 
      (in r377)
    :effect
      (and
        (in r378)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r377))
      )
    )
  (:action move-left-from-r377
    :parameters ()
    :precondition 
      (in r377)
    :effect
      (and
        (in r376)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r377))
      )
    )
  (:action move-right-from-r378
    :parameters ()
    :precondition 
      (in r378)
    :effect
      (and
        (in r379)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r378))
      )
    )
  (:action move-left-from-r378
    :parameters ()
    :precondition 
      (in r378)
    :effect
      (and
        (in r377)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r378))
      )
    )
  (:action move-right-from-r379
    :parameters ()
    :precondition 
      (in r379)
    :effect
      (and
        (in r380)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r379))
      )
    )
  (:action move-left-from-r379
    :parameters ()
    :precondition 
      (in r379)
    :effect
      (and
        (in r378)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r379))
      )
    )
  (:action move-right-from-r380
    :parameters ()
    :precondition 
      (in r380)
    :effect
      (and
        (in r381)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r380))
      )
    )
  (:action move-left-from-r380
    :parameters ()
    :precondition 
      (in r380)
    :effect
      (and
        (in r379)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r380))
      )
    )
  (:action move-right-from-r381
    :parameters ()
    :precondition 
      (in r381)
    :effect
      (and
        (in r382)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r381))
      )
    )
  (:action move-left-from-r381
    :parameters ()
    :precondition 
      (in r381)
    :effect
      (and
        (in r380)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r381))
      )
    )
  (:action move-right-from-r382
    :parameters ()
    :precondition 
      (in r382)
    :effect
      (and
        (in r383)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r382))
      )
    )
  (:action move-left-from-r382
    :parameters ()
    :precondition 
      (in r382)
    :effect
      (and
        (in r381)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r382))
      )
    )
  (:action move-right-from-r383
    :parameters ()
    :precondition 
      (in r383)
    :effect
      (and
        (in r384)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r383))
      )
    )
  (:action move-left-from-r383
    :parameters ()
    :precondition 
      (in r383)
    :effect
      (and
        (in r382)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r383))
      )
    )
  (:action move-right-from-r384
    :parameters ()
    :precondition 
      (in r384)
    :effect
      (and
        (in r385)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r384))
      )
    )
  (:action move-left-from-r384
    :parameters ()
    :precondition 
      (in r384)
    :effect
      (and
        (in r383)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r384))
      )
    )
  (:action move-right-from-r385
    :parameters ()
    :precondition 
      (in r385)
    :effect
      (and
        (in r386)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r385))
      )
    )
  (:action move-left-from-r385
    :parameters ()
    :precondition 
      (in r385)
    :effect
      (and
        (in r384)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r385))
      )
    )
  (:action move-right-from-r386
    :parameters ()
    :precondition 
      (in r386)
    :effect
      (and
        (in r387)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r386))
      )
    )
  (:action move-left-from-r386
    :parameters ()
    :precondition 
      (in r386)
    :effect
      (and
        (in r385)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r386))
      )
    )
  (:action move-right-from-r387
    :parameters ()
    :precondition 
      (in r387)
    :effect
      (and
        (in r388)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r387))
      )
    )
  (:action move-left-from-r387
    :parameters ()
    :precondition 
      (in r387)
    :effect
      (and
        (in r386)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r387))
      )
    )
  (:action move-right-from-r388
    :parameters ()
    :precondition 
      (in r388)
    :effect
      (and
        (in r389)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r388))
      )
    )
  (:action move-left-from-r388
    :parameters ()
    :precondition 
      (in r388)
    :effect
      (and
        (in r387)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r388))
      )
    )
  (:action move-right-from-r389
    :parameters ()
    :precondition 
      (in r389)
    :effect
      (and
        (in r390)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r389))
      )
    )
  (:action move-left-from-r389
    :parameters ()
    :precondition 
      (in r389)
    :effect
      (and
        (in r388)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r389))
      )
    )
  (:action move-right-from-r390
    :parameters ()
    :precondition 
      (in r390)
    :effect
      (and
        (in r391)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r390))
      )
    )
  (:action move-left-from-r390
    :parameters ()
    :precondition 
      (in r390)
    :effect
      (and
        (in r389)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r390))
      )
    )
  (:action move-right-from-r391
    :parameters ()
    :precondition 
      (in r391)
    :effect
      (and
        (in r392)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r391))
      )
    )
  (:action move-left-from-r391
    :parameters ()
    :precondition 
      (in r391)
    :effect
      (and
        (in r390)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r391))
      )
    )
  (:action move-right-from-r392
    :parameters ()
    :precondition 
      (in r392)
    :effect
      (and
        (in r393)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r392))
      )
    )
  (:action move-left-from-r392
    :parameters ()
    :precondition 
      (in r392)
    :effect
      (and
        (in r391)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r392))
      )
    )
  (:action move-right-from-r393
    :parameters ()
    :precondition 
      (in r393)
    :effect
      (and
        (in r394)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r393))
      )
    )
  (:action move-left-from-r393
    :parameters ()
    :precondition 
      (in r393)
    :effect
      (and
        (in r392)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r393))
      )
    )
  (:action move-right-from-r394
    :parameters ()
    :precondition 
      (in r394)
    :effect
      (and
        (in r395)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r394))
      )
    )
  (:action move-left-from-r394
    :parameters ()
    :precondition 
      (in r394)
    :effect
      (and
        (in r393)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r394))
      )
    )
  (:action move-right-from-r395
    :parameters ()
    :precondition 
      (in r395)
    :effect
      (and
        (in r396)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r395))
      )
    )
  (:action move-left-from-r395
    :parameters ()
    :precondition 
      (in r395)
    :effect
      (and
        (in r394)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r395))
      )
    )
  (:action move-right-from-r396
    :parameters ()
    :precondition 
      (in r396)
    :effect
      (and
        (in r397)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r396))
      )
    )
  (:action move-left-from-r396
    :parameters ()
    :precondition 
      (in r396)
    :effect
      (and
        (in r395)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r396))
      )
    )
  (:action move-right-from-r397
    :parameters ()
    :precondition 
      (in r397)
    :effect
      (and
        (in r398)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r397))
      )
    )
  (:action move-left-from-r397
    :parameters ()
    :precondition 
      (in r397)
    :effect
      (and
        (in r396)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r397))
      )
    )
  (:action move-right-from-r398
    :parameters ()
    :precondition 
      (in r398)
    :effect
      (and
        (in r399)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r398))
      )
    )
  (:action move-left-from-r398
    :parameters ()
    :precondition 
      (in r398)
    :effect
      (and
        (in r397)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r398))
      )
    )
  (:action move-right-from-r399
    :parameters ()
    :precondition 
      (in r399)
    :effect
      (and
        (in r400)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r399))
      )
    )
  (:action move-left-from-r399
    :parameters ()
    :precondition 
      (in r399)
    :effect
      (and
        (in r398)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r399))
      )
    )
  (:action move-right-from-r400
    :parameters ()
    :precondition 
      (in r400)
    :effect
      (and
        (in r401)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r400))
      )
    )
  (:action move-left-from-r400
    :parameters ()
    :precondition 
      (in r400)
    :effect
      (and
        (in r399)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r400))
      )
    )
  (:action move-right-from-r401
    :parameters ()
    :precondition 
      (in r401)
    :effect
      (and
        (in r402)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r401))
      )
    )
  (:action move-left-from-r401
    :parameters ()
    :precondition 
      (in r401)
    :effect
      (and
        (in r400)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r401))
      )
    )
  (:action move-right-from-r402
    :parameters ()
    :precondition 
      (in r402)
    :effect
      (and
        (in r403)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r402))
      )
    )
  (:action move-left-from-r402
    :parameters ()
    :precondition 
      (in r402)
    :effect
      (and
        (in r401)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r402))
      )
    )
  (:action move-right-from-r403
    :parameters ()
    :precondition 
      (in r403)
    :effect
      (and
        (in r404)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r403))
      )
    )
  (:action move-left-from-r403
    :parameters ()
    :precondition 
      (in r403)
    :effect
      (and
        (in r402)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r403))
      )
    )
  (:action move-right-from-r404
    :parameters ()
    :precondition 
      (in r404)
    :effect
      (and
        (in r405)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r404))
      )
    )
  (:action move-left-from-r404
    :parameters ()
    :precondition 
      (in r404)
    :effect
      (and
        (in r403)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r404))
      )
    )
  (:action move-right-from-r405
    :parameters ()
    :precondition 
      (in r405)
    :effect
      (and
        (in r406)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r405))
      )
    )
  (:action move-left-from-r405
    :parameters ()
    :precondition 
      (in r405)
    :effect
      (and
        (in r404)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r405))
      )
    )
  (:action move-right-from-r406
    :parameters ()
    :precondition 
      (in r406)
    :effect
      (and
        (in r407)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r406))
      )
    )
  (:action move-left-from-r406
    :parameters ()
    :precondition 
      (in r406)
    :effect
      (and
        (in r405)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r406))
      )
    )
  (:action move-right-from-r407
    :parameters ()
    :precondition 
      (in r407)
    :effect
      (and
        (in r408)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r407))
      )
    )
  (:action move-left-from-r407
    :parameters ()
    :precondition 
      (in r407)
    :effect
      (and
        (in r406)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r407))
      )
    )
  (:action move-right-from-r408
    :parameters ()
    :precondition 
      (in r408)
    :effect
      (and
        (in r409)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r408))
      )
    )
  (:action move-left-from-r408
    :parameters ()
    :precondition 
      (in r408)
    :effect
      (and
        (in r407)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r408))
      )
    )
  (:action move-right-from-r409
    :parameters ()
    :precondition 
      (in r409)
    :effect
      (and
        (in r410)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r409))
      )
    )
  (:action move-left-from-r409
    :parameters ()
    :precondition 
      (in r409)
    :effect
      (and
        (in r408)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r409))
      )
    )
  (:action move-right-from-r410
    :parameters ()
    :precondition 
      (in r410)
    :effect
      (and
        (in r411)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r410))
      )
    )
  (:action move-left-from-r410
    :parameters ()
    :precondition 
      (in r410)
    :effect
      (and
        (in r409)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r410))
      )
    )
  (:action move-right-from-r411
    :parameters ()
    :precondition 
      (in r411)
    :effect
      (and
        (in r412)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r411))
      )
    )
  (:action move-left-from-r411
    :parameters ()
    :precondition 
      (in r411)
    :effect
      (and
        (in r410)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r411))
      )
    )
  (:action move-right-from-r412
    :parameters ()
    :precondition 
      (in r412)
    :effect
      (and
        (in r413)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r412))
      )
    )
  (:action move-left-from-r412
    :parameters ()
    :precondition 
      (in r412)
    :effect
      (and
        (in r411)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r412))
      )
    )
  (:action move-right-from-r413
    :parameters ()
    :precondition 
      (in r413)
    :effect
      (and
        (in r414)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r413))
      )
    )
  (:action move-left-from-r413
    :parameters ()
    :precondition 
      (in r413)
    :effect
      (and
        (in r412)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r413))
      )
    )
  (:action move-right-from-r414
    :parameters ()
    :precondition 
      (in r414)
    :effect
      (and
        (in r415)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r414))
      )
    )
  (:action move-left-from-r414
    :parameters ()
    :precondition 
      (in r414)
    :effect
      (and
        (in r413)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r414))
      )
    )
  (:action move-right-from-r415
    :parameters ()
    :precondition 
      (in r415)
    :effect
      (and
        (in r416)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r415))
      )
    )
  (:action move-left-from-r415
    :parameters ()
    :precondition 
      (in r415)
    :effect
      (and
        (in r414)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r415))
      )
    )
  (:action move-right-from-r416
    :parameters ()
    :precondition 
      (in r416)
    :effect
      (and
        (in r417)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r416))
      )
    )
  (:action move-left-from-r416
    :parameters ()
    :precondition 
      (in r416)
    :effect
      (and
        (in r415)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r416))
      )
    )
  (:action move-right-from-r417
    :parameters ()
    :precondition 
      (in r417)
    :effect
      (and
        (in r418)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r417))
      )
    )
  (:action move-left-from-r417
    :parameters ()
    :precondition 
      (in r417)
    :effect
      (and
        (in r416)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r417))
      )
    )
  (:action move-right-from-r418
    :parameters ()
    :precondition 
      (in r418)
    :effect
      (and
        (in r419)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r418))
      )
    )
  (:action move-left-from-r418
    :parameters ()
    :precondition 
      (in r418)
    :effect
      (and
        (in r417)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r418))
      )
    )
  (:action move-right-from-r419
    :parameters ()
    :precondition 
      (in r419)
    :effect
      (and
        (in r420)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r419))
      )
    )
  (:action move-left-from-r419
    :parameters ()
    :precondition 
      (in r419)
    :effect
      (and
        (in r418)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r419))
      )
    )
  (:action move-right-from-r420
    :parameters ()
    :precondition 
      (in r420)
    :effect
      (and
        (in r421)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r420))
      )
    )
  (:action move-left-from-r420
    :parameters ()
    :precondition 
      (in r420)
    :effect
      (and
        (in r419)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r420))
      )
    )
  (:action move-right-from-r421
    :parameters ()
    :precondition 
      (in r421)
    :effect
      (and
        (in r422)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r421))
      )
    )
  (:action move-left-from-r421
    :parameters ()
    :precondition 
      (in r421)
    :effect
      (and
        (in r420)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r421))
      )
    )
  (:action move-right-from-r422
    :parameters ()
    :precondition 
      (in r422)
    :effect
      (and
        (in r423)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r422))
      )
    )
  (:action move-left-from-r422
    :parameters ()
    :precondition 
      (in r422)
    :effect
      (and
        (in r421)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r422))
      )
    )
  (:action move-right-from-r423
    :parameters ()
    :precondition 
      (in r423)
    :effect
      (and
        (in r424)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r423))
      )
    )
  (:action move-left-from-r423
    :parameters ()
    :precondition 
      (in r423)
    :effect
      (and
        (in r422)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r423))
      )
    )
  (:action move-right-from-r424
    :parameters ()
    :precondition 
      (in r424)
    :effect
      (and
        (in r425)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r424))
      )
    )
  (:action move-left-from-r424
    :parameters ()
    :precondition 
      (in r424)
    :effect
      (and
        (in r423)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r424))
      )
    )
  (:action move-right-from-r425
    :parameters ()
    :precondition 
      (in r425)
    :effect
      (and
        (in r426)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r425))
      )
    )
  (:action move-left-from-r425
    :parameters ()
    :precondition 
      (in r425)
    :effect
      (and
        (in r424)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r425))
      )
    )
  (:action move-right-from-r426
    :parameters ()
    :precondition 
      (in r426)
    :effect
      (and
        (in r427)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r426))
      )
    )
  (:action move-left-from-r426
    :parameters ()
    :precondition 
      (in r426)
    :effect
      (and
        (in r425)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r426))
      )
    )
  (:action move-right-from-r427
    :parameters ()
    :precondition 
      (in r427)
    :effect
      (and
        (in r428)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r427))
      )
    )
  (:action move-left-from-r427
    :parameters ()
    :precondition 
      (in r427)
    :effect
      (and
        (in r426)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r427))
      )
    )
  (:action move-right-from-r428
    :parameters ()
    :precondition 
      (in r428)
    :effect
      (and
        (in r429)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r428))
      )
    )
  (:action move-left-from-r428
    :parameters ()
    :precondition 
      (in r428)
    :effect
      (and
        (in r427)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r428))
      )
    )
  (:action move-right-from-r429
    :parameters ()
    :precondition 
      (in r429)
    :effect
      (and
        (in r430)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r429))
      )
    )
  (:action move-left-from-r429
    :parameters ()
    :precondition 
      (in r429)
    :effect
      (and
        (in r428)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r429))
      )
    )
  (:action move-right-from-r430
    :parameters ()
    :precondition 
      (in r430)
    :effect
      (and
        (in r431)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r430))
      )
    )
  (:action move-left-from-r430
    :parameters ()
    :precondition 
      (in r430)
    :effect
      (and
        (in r429)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r430))
      )
    )
  (:action move-right-from-r431
    :parameters ()
    :precondition 
      (in r431)
    :effect
      (and
        (in r432)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r431))
      )
    )
  (:action move-left-from-r431
    :parameters ()
    :precondition 
      (in r431)
    :effect
      (and
        (in r430)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r431))
      )
    )
  (:action move-right-from-r432
    :parameters ()
    :precondition 
      (in r432)
    :effect
      (and
        (in r433)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r432))
      )
    )
  (:action move-left-from-r432
    :parameters ()
    :precondition 
      (in r432)
    :effect
      (and
        (in r431)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r432))
      )
    )
  (:action move-right-from-r433
    :parameters ()
    :precondition 
      (in r433)
    :effect
      (and
        (in r434)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r433))
      )
    )
  (:action move-left-from-r433
    :parameters ()
    :precondition 
      (in r433)
    :effect
      (and
        (in r432)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r433))
      )
    )
  (:action move-right-from-r434
    :parameters ()
    :precondition 
      (in r434)
    :effect
      (and
        (in r435)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r434))
      )
    )
  (:action move-left-from-r434
    :parameters ()
    :precondition 
      (in r434)
    :effect
      (and
        (in r433)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r434))
      )
    )
  (:action move-right-from-r435
    :parameters ()
    :precondition 
      (in r435)
    :effect
      (and
        (in r436)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r435))
      )
    )
  (:action move-left-from-r435
    :parameters ()
    :precondition 
      (in r435)
    :effect
      (and
        (in r434)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r435))
      )
    )
  (:action move-right-from-r436
    :parameters ()
    :precondition 
      (in r436)
    :effect
      (and
        (in r437)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r436))
      )
    )
  (:action move-left-from-r436
    :parameters ()
    :precondition 
      (in r436)
    :effect
      (and
        (in r435)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r436))
      )
    )
  (:action move-right-from-r437
    :parameters ()
    :precondition 
      (in r437)
    :effect
      (and
        (in r438)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r437))
      )
    )
  (:action move-left-from-r437
    :parameters ()
    :precondition 
      (in r437)
    :effect
      (and
        (in r436)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r437))
      )
    )
  (:action move-right-from-r438
    :parameters ()
    :precondition 
      (in r438)
    :effect
      (and
        (in r439)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r438))
      )
    )
  (:action move-left-from-r438
    :parameters ()
    :precondition 
      (in r438)
    :effect
      (and
        (in r437)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r438))
      )
    )
  (:action move-right-from-r439
    :parameters ()
    :precondition 
      (in r439)
    :effect
      (and
        (in r440)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r439))
      )
    )
  (:action move-left-from-r439
    :parameters ()
    :precondition 
      (in r439)
    :effect
      (and
        (in r438)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r439))
      )
    )
  (:action move-right-from-r440
    :parameters ()
    :precondition 
      (in r440)
    :effect
      (and
        (in r441)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r440))
      )
    )
  (:action move-left-from-r440
    :parameters ()
    :precondition 
      (in r440)
    :effect
      (and
        (in r439)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r440))
      )
    )
  (:action move-right-from-r441
    :parameters ()
    :precondition 
      (in r441)
    :effect
      (and
        (in r442)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r441))
      )
    )
  (:action move-left-from-r441
    :parameters ()
    :precondition 
      (in r441)
    :effect
      (and
        (in r440)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r441))
      )
    )
  (:action move-right-from-r442
    :parameters ()
    :precondition 
      (in r442)
    :effect
      (and
        (in r443)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r442))
      )
    )
  (:action move-left-from-r442
    :parameters ()
    :precondition 
      (in r442)
    :effect
      (and
        (in r441)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r442))
      )
    )
  (:action move-right-from-r443
    :parameters ()
    :precondition 
      (in r443)
    :effect
      (and
        (in r444)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r443))
      )
    )
  (:action move-left-from-r443
    :parameters ()
    :precondition 
      (in r443)
    :effect
      (and
        (in r442)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r443))
      )
    )
  (:action move-right-from-r444
    :parameters ()
    :precondition 
      (in r444)
    :effect
      (and
        (in r445)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r444))
      )
    )
  (:action move-left-from-r444
    :parameters ()
    :precondition 
      (in r444)
    :effect
      (and
        (in r443)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r444))
      )
    )
  (:action move-right-from-r445
    :parameters ()
    :precondition 
      (in r445)
    :effect
      (and
        (in r446)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r445))
      )
    )
  (:action move-left-from-r445
    :parameters ()
    :precondition 
      (in r445)
    :effect
      (and
        (in r444)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r445))
      )
    )
  (:action move-right-from-r446
    :parameters ()
    :precondition 
      (in r446)
    :effect
      (and
        (in r447)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r446))
      )
    )
  (:action move-left-from-r446
    :parameters ()
    :precondition 
      (in r446)
    :effect
      (and
        (in r445)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r446))
      )
    )
  (:action move-right-from-r447
    :parameters ()
    :precondition 
      (in r447)
    :effect
      (and
        (in r448)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r447))
      )
    )
  (:action move-left-from-r447
    :parameters ()
    :precondition 
      (in r447)
    :effect
      (and
        (in r446)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r447))
      )
    )
  (:action move-right-from-r448
    :parameters ()
    :precondition 
      (in r448)
    :effect
      (and
        (in r449)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r448))
      )
    )
  (:action move-left-from-r448
    :parameters ()
    :precondition 
      (in r448)
    :effect
      (and
        (in r447)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r448))
      )
    )
  (:action move-right-from-r449
    :parameters ()
    :precondition 
      (in r449)
    :effect
      (and
        (in r450)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r449))
      )
    )
  (:action move-left-from-r449
    :parameters ()
    :precondition 
      (in r449)
    :effect
      (and
        (in r448)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r449))
      )
    )
  (:action move-right-from-r450
    :parameters ()
    :precondition 
      (in r450)
    :effect
      (and
        (in r451)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r450))
      )
    )
  (:action move-left-from-r450
    :parameters ()
    :precondition 
      (in r450)
    :effect
      (and
        (in r449)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r450))
      )
    )
  (:action move-right-from-r451
    :parameters ()
    :precondition 
      (in r451)
    :effect
      (and
        (in r452)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r451))
      )
    )
  (:action move-left-from-r451
    :parameters ()
    :precondition 
      (in r451)
    :effect
      (and
        (in r450)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r451))
      )
    )
  (:action move-right-from-r452
    :parameters ()
    :precondition 
      (in r452)
    :effect
      (and
        (in r453)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r452))
      )
    )
  (:action move-left-from-r452
    :parameters ()
    :precondition 
      (in r452)
    :effect
      (and
        (in r451)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r452))
      )
    )
  (:action move-right-from-r453
    :parameters ()
    :precondition 
      (in r453)
    :effect
      (and
        (in r454)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r453))
      )
    )
  (:action move-left-from-r453
    :parameters ()
    :precondition 
      (in r453)
    :effect
      (and
        (in r452)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r453))
      )
    )
  (:action move-right-from-r454
    :parameters ()
    :precondition 
      (in r454)
    :effect
      (and
        (in r455)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r454))
      )
    )
  (:action move-left-from-r454
    :parameters ()
    :precondition 
      (in r454)
    :effect
      (and
        (in r453)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r454))
      )
    )
  (:action move-right-from-r455
    :parameters ()
    :precondition 
      (in r455)
    :effect
      (and
        (in r456)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r455))
      )
    )
  (:action move-left-from-r455
    :parameters ()
    :precondition 
      (in r455)
    :effect
      (and
        (in r454)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r455))
      )
    )
  (:action move-right-from-r456
    :parameters ()
    :precondition 
      (in r456)
    :effect
      (and
        (in r457)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r456))
      )
    )
  (:action move-left-from-r456
    :parameters ()
    :precondition 
      (in r456)
    :effect
      (and
        (in r455)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r456))
      )
    )
  (:action move-right-from-r457
    :parameters ()
    :precondition 
      (in r457)
    :effect
      (and
        (in r458)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r457))
      )
    )
  (:action move-left-from-r457
    :parameters ()
    :precondition 
      (in r457)
    :effect
      (and
        (in r456)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r457))
      )
    )
  (:action move-right-from-r458
    :parameters ()
    :precondition 
      (in r458)
    :effect
      (and
        (in r459)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r458))
      )
    )
  (:action move-left-from-r458
    :parameters ()
    :precondition 
      (in r458)
    :effect
      (and
        (in r457)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r458))
      )
    )
  (:action move-right-from-r459
    :parameters ()
    :precondition 
      (in r459)
    :effect
      (and
        (in r460)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r459))
      )
    )
  (:action move-left-from-r459
    :parameters ()
    :precondition 
      (in r459)
    :effect
      (and
        (in r458)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r459))
      )
    )
  (:action move-right-from-r460
    :parameters ()
    :precondition 
      (in r460)
    :effect
      (and
        (in r461)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r460))
      )
    )
  (:action move-left-from-r460
    :parameters ()
    :precondition 
      (in r460)
    :effect
      (and
        (in r459)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r460))
      )
    )
  (:action move-right-from-r461
    :parameters ()
    :precondition 
      (in r461)
    :effect
      (and
        (in r462)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r461))
      )
    )
  (:action move-left-from-r461
    :parameters ()
    :precondition 
      (in r461)
    :effect
      (and
        (in r460)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r461))
      )
    )
  (:action move-right-from-r462
    :parameters ()
    :precondition 
      (in r462)
    :effect
      (and
        (in r463)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r462))
      )
    )
  (:action move-left-from-r462
    :parameters ()
    :precondition 
      (in r462)
    :effect
      (and
        (in r461)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r462))
      )
    )
  (:action move-right-from-r463
    :parameters ()
    :precondition 
      (in r463)
    :effect
      (and
        (in r464)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r463))
      )
    )
  (:action move-left-from-r463
    :parameters ()
    :precondition 
      (in r463)
    :effect
      (and
        (in r462)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r463))
      )
    )
  (:action move-right-from-r464
    :parameters ()
    :precondition 
      (in r464)
    :effect
      (and
        (in r465)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r464))
      )
    )
  (:action move-left-from-r464
    :parameters ()
    :precondition 
      (in r464)
    :effect
      (and
        (in r463)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r464))
      )
    )
  (:action move-right-from-r465
    :parameters ()
    :precondition 
      (in r465)
    :effect
      (and
        (in r466)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r465))
      )
    )
  (:action move-left-from-r465
    :parameters ()
    :precondition 
      (in r465)
    :effect
      (and
        (in r464)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r465))
      )
    )
  (:action move-right-from-r466
    :parameters ()
    :precondition 
      (in r466)
    :effect
      (and
        (in r467)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r466))
      )
    )
  (:action move-left-from-r466
    :parameters ()
    :precondition 
      (in r466)
    :effect
      (and
        (in r465)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r466))
      )
    )
  (:action move-right-from-r467
    :parameters ()
    :precondition 
      (in r467)
    :effect
      (and
        (in r468)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r467))
      )
    )
  (:action move-left-from-r467
    :parameters ()
    :precondition 
      (in r467)
    :effect
      (and
        (in r466)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r467))
      )
    )
  (:action move-right-from-r468
    :parameters ()
    :precondition 
      (in r468)
    :effect
      (and
        (in r469)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r468))
      )
    )
  (:action move-left-from-r468
    :parameters ()
    :precondition 
      (in r468)
    :effect
      (and
        (in r467)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r468))
      )
    )
  (:action move-right-from-r469
    :parameters ()
    :precondition 
      (in r469)
    :effect
      (and
        (in r470)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r469))
      )
    )
  (:action move-left-from-r469
    :parameters ()
    :precondition 
      (in r469)
    :effect
      (and
        (in r468)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r469))
      )
    )
  (:action move-right-from-r470
    :parameters ()
    :precondition 
      (in r470)
    :effect
      (and
        (in r471)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r470))
      )
    )
  (:action move-left-from-r470
    :parameters ()
    :precondition 
      (in r470)
    :effect
      (and
        (in r469)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r470))
      )
    )
  (:action move-right-from-r471
    :parameters ()
    :precondition 
      (in r471)
    :effect
      (and
        (in r472)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r471))
      )
    )
  (:action move-left-from-r471
    :parameters ()
    :precondition 
      (in r471)
    :effect
      (and
        (in r470)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r471))
      )
    )
  (:action move-right-from-r472
    :parameters ()
    :precondition 
      (in r472)
    :effect
      (and
        (in r473)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r472))
      )
    )
  (:action move-left-from-r472
    :parameters ()
    :precondition 
      (in r472)
    :effect
      (and
        (in r471)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r472))
      )
    )
  (:action move-right-from-r473
    :parameters ()
    :precondition 
      (in r473)
    :effect
      (and
        (in r474)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r473))
      )
    )
  (:action move-left-from-r473
    :parameters ()
    :precondition 
      (in r473)
    :effect
      (and
        (in r472)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r473))
      )
    )
  (:action move-right-from-r474
    :parameters ()
    :precondition 
      (in r474)
    :effect
      (and
        (in r475)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r474))
      )
    )
  (:action move-left-from-r474
    :parameters ()
    :precondition 
      (in r474)
    :effect
      (and
        (in r473)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r474))
      )
    )
  (:action move-right-from-r475
    :parameters ()
    :precondition 
      (in r475)
    :effect
      (and
        (in r476)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r475))
      )
    )
  (:action move-left-from-r475
    :parameters ()
    :precondition 
      (in r475)
    :effect
      (and
        (in r474)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r475))
      )
    )
  (:action move-right-from-r476
    :parameters ()
    :precondition 
      (in r476)
    :effect
      (and
        (in r477)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r476))
      )
    )
  (:action move-left-from-r476
    :parameters ()
    :precondition 
      (in r476)
    :effect
      (and
        (in r475)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r476))
      )
    )
  (:action move-right-from-r477
    :parameters ()
    :precondition 
      (in r477)
    :effect
      (and
        (in r478)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r477))
      )
    )
  (:action move-left-from-r477
    :parameters ()
    :precondition 
      (in r477)
    :effect
      (and
        (in r476)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r477))
      )
    )
  (:action move-right-from-r478
    :parameters ()
    :precondition 
      (in r478)
    :effect
      (and
        (in r479)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r478))
      )
    )
  (:action move-left-from-r478
    :parameters ()
    :precondition 
      (in r478)
    :effect
      (and
        (in r477)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r478))
      )
    )
  (:action move-right-from-r479
    :parameters ()
    :precondition 
      (in r479)
    :effect
      (and
        (in r480)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r479))
      )
    )
  (:action move-left-from-r479
    :parameters ()
    :precondition 
      (in r479)
    :effect
      (and
        (in r478)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r479))
      )
    )
  (:action move-right-from-r480
    :parameters ()
    :precondition 
      (in r480)
    :effect
      (and
        (in r481)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r480))
      )
    )
  (:action move-left-from-r480
    :parameters ()
    :precondition 
      (in r480)
    :effect
      (and
        (in r479)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r480))
      )
    )
  (:action move-right-from-r481
    :parameters ()
    :precondition 
      (in r481)
    :effect
      (and
        (in r482)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r481))
      )
    )
  (:action move-left-from-r481
    :parameters ()
    :precondition 
      (in r481)
    :effect
      (and
        (in r480)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r481))
      )
    )
  (:action move-right-from-r482
    :parameters ()
    :precondition 
      (in r482)
    :effect
      (and
        (in r483)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r482))
      )
    )
  (:action move-left-from-r482
    :parameters ()
    :precondition 
      (in r482)
    :effect
      (and
        (in r481)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r482))
      )
    )
  (:action move-right-from-r483
    :parameters ()
    :precondition 
      (in r483)
    :effect
      (and
        (in r484)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r483))
      )
    )
  (:action move-left-from-r483
    :parameters ()
    :precondition 
      (in r483)
    :effect
      (and
        (in r482)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r483))
      )
    )
  (:action move-right-from-r484
    :parameters ()
    :precondition 
      (in r484)
    :effect
      (and
        (in r485)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r484))
      )
    )
  (:action move-left-from-r484
    :parameters ()
    :precondition 
      (in r484)
    :effect
      (and
        (in r483)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r484))
      )
    )
  (:action move-right-from-r485
    :parameters ()
    :precondition 
      (in r485)
    :effect
      (and
        (in r486)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r485))
      )
    )
  (:action move-left-from-r485
    :parameters ()
    :precondition 
      (in r485)
    :effect
      (and
        (in r484)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r485))
      )
    )
  (:action move-right-from-r486
    :parameters ()
    :precondition 
      (in r486)
    :effect
      (and
        (in r487)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r486))
      )
    )
  (:action move-left-from-r486
    :parameters ()
    :precondition 
      (in r486)
    :effect
      (and
        (in r485)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r486))
      )
    )
  (:action move-right-from-r487
    :parameters ()
    :precondition 
      (in r487)
    :effect
      (and
        (in r488)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r487))
      )
    )
  (:action move-left-from-r487
    :parameters ()
    :precondition 
      (in r487)
    :effect
      (and
        (in r486)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r487))
      )
    )
  (:action move-right-from-r488
    :parameters ()
    :precondition 
      (in r488)
    :effect
      (and
        (in r489)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r488))
      )
    )
  (:action move-left-from-r488
    :parameters ()
    :precondition 
      (in r488)
    :effect
      (and
        (in r487)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r488))
      )
    )
  (:action move-right-from-r489
    :parameters ()
    :precondition 
      (in r489)
    :effect
      (and
        (in r490)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r489))
      )
    )
  (:action move-left-from-r489
    :parameters ()
    :precondition 
      (in r489)
    :effect
      (and
        (in r488)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r489))
      )
    )
  (:action move-right-from-r490
    :parameters ()
    :precondition 
      (in r490)
    :effect
      (and
        (in r491)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r490))
      )
    )
  (:action move-left-from-r490
    :parameters ()
    :precondition 
      (in r490)
    :effect
      (and
        (in r489)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r490))
      )
    )
  (:action move-right-from-r491
    :parameters ()
    :precondition 
      (in r491)
    :effect
      (and
        (in r492)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r491))
      )
    )
  (:action move-left-from-r491
    :parameters ()
    :precondition 
      (in r491)
    :effect
      (and
        (in r490)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r491))
      )
    )
  (:action move-right-from-r492
    :parameters ()
    :precondition 
      (in r492)
    :effect
      (and
        (in r493)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r492))
      )
    )
  (:action move-left-from-r492
    :parameters ()
    :precondition 
      (in r492)
    :effect
      (and
        (in r491)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r492))
      )
    )
  (:action move-right-from-r493
    :parameters ()
    :precondition 
      (in r493)
    :effect
      (and
        (in r494)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r493))
      )
    )
  (:action move-left-from-r493
    :parameters ()
    :precondition 
      (in r493)
    :effect
      (and
        (in r492)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r493))
      )
    )
  (:action move-right-from-r494
    :parameters ()
    :precondition 
      (in r494)
    :effect
      (and
        (in r495)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r494))
      )
    )
  (:action move-left-from-r494
    :parameters ()
    :precondition 
      (in r494)
    :effect
      (and
        (in r493)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r494))
      )
    )
  (:action move-right-from-r495
    :parameters ()
    :precondition 
      (in r495)
    :effect
      (and
        (in r496)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r495))
      )
    )
  (:action move-left-from-r495
    :parameters ()
    :precondition 
      (in r495)
    :effect
      (and
        (in r494)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r495))
      )
    )
  (:action move-right-from-r496
    :parameters ()
    :precondition 
      (in r496)
    :effect
      (and
        (in r497)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r496))
      )
    )
  (:action move-left-from-r496
    :parameters ()
    :precondition 
      (in r496)
    :effect
      (and
        (in r495)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r496))
      )
    )
  (:action move-right-from-r497
    :parameters ()
    :precondition 
      (in r497)
    :effect
      (and
        (in r498)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r497))
      )
    )
  (:action move-left-from-r497
    :parameters ()
    :precondition 
      (in r497)
    :effect
      (and
        (in r496)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r497))
      )
    )
  (:action move-right-from-r498
    :parameters ()
    :precondition 
      (in r498)
    :effect
      (and
        (in r499)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r498))
      )
    )
  (:action move-left-from-r498
    :parameters ()
    :precondition 
      (in r498)
    :effect
      (and
        (in r497)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r498))
      )
    )
  (:action move-right-from-r499
    :parameters ()
    :precondition 
      (in r499)
    :effect
      (and
        (in r500)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r499))
      )
    )
  (:action move-left-from-r499
    :parameters ()
    :precondition 
      (in r499)
    :effect
      (and
        (in r498)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r499))
      )
    )
  (:action move-right-from-r500
    :parameters ()
    :precondition 
      (in r500)
    :effect
      (and
        (in r501)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r500))
      )
    )
  (:action move-left-from-r500
    :parameters ()
    :precondition 
      (in r500)
    :effect
      (and
        (in r499)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r500))
      )
    )
  (:action move-right-from-r501
    :parameters ()
    :precondition 
      (in r501)
    :effect
      (and
        (in r502)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r501))
      )
    )
  (:action move-left-from-r501
    :parameters ()
    :precondition 
      (in r501)
    :effect
      (and
        (in r500)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r501))
      )
    )
  (:action move-right-from-r502
    :parameters ()
    :precondition 
      (in r502)
    :effect
      (and
        (in r503)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r502))
      )
    )
  (:action move-left-from-r502
    :parameters ()
    :precondition 
      (in r502)
    :effect
      (and
        (in r501)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r502))
      )
    )
  (:action move-right-from-r503
    :parameters ()
    :precondition 
      (in r503)
    :effect
      (and
        (in r504)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r503))
      )
    )
  (:action move-left-from-r503
    :parameters ()
    :precondition 
      (in r503)
    :effect
      (and
        (in r502)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r503))
      )
    )
  (:action move-right-from-r504
    :parameters ()
    :precondition 
      (in r504)
    :effect
      (and
        (in r505)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r504))
      )
    )
  (:action move-left-from-r504
    :parameters ()
    :precondition 
      (in r504)
    :effect
      (and
        (in r503)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r504))
      )
    )
  (:action move-right-from-r505
    :parameters ()
    :precondition 
      (in r505)
    :effect
      (and
        (in r506)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r505))
      )
    )
  (:action move-left-from-r505
    :parameters ()
    :precondition 
      (in r505)
    :effect
      (and
        (in r504)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r505))
      )
    )
  (:action move-right-from-r506
    :parameters ()
    :precondition 
      (in r506)
    :effect
      (and
        (in r507)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r506))
      )
    )
  (:action move-left-from-r506
    :parameters ()
    :precondition 
      (in r506)
    :effect
      (and
        (in r505)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r506))
      )
    )
  (:action move-right-from-r507
    :parameters ()
    :precondition 
      (in r507)
    :effect
      (and
        (in r508)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r507))
      )
    )
  (:action move-left-from-r507
    :parameters ()
    :precondition 
      (in r507)
    :effect
      (and
        (in r506)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r507))
      )
    )
  (:action move-right-from-r508
    :parameters ()
    :precondition 
      (in r508)
    :effect
      (and
        (in r509)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r508))
      )
    )
  (:action move-left-from-r508
    :parameters ()
    :precondition 
      (in r508)
    :effect
      (and
        (in r507)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r508))
      )
    )
  (:action move-right-from-r509
    :parameters ()
    :precondition 
      (in r509)
    :effect
      (and
        (in r510)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r509))
      )
    )
  (:action move-left-from-r509
    :parameters ()
    :precondition 
      (in r509)
    :effect
      (and
        (in r508)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r509))
      )
    )
  (:action move-right-from-r510
    :parameters ()
    :precondition 
      (in r510)
    :effect
      (and
        (in r511)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r510))
      )
    )
  (:action move-left-from-r510
    :parameters ()
    :precondition 
      (in r510)
    :effect
      (and
        (in r509)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r510))
      )
    )
  (:action move-right-from-r511
    :parameters ()
    :precondition 
      (in r511)
    :effect
      (and
        (in r512)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r511))
      )
    )
  (:action move-left-from-r511
    :parameters ()
    :precondition 
      (in r511)
    :effect
      (and
        (in r510)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r511))
      )
    )
  (:action move-right-from-r512
    :parameters ()
    :precondition 
      (in r512)
    :effect
      (and
        (in r513)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r512))
      )
    )
  (:action move-left-from-r512
    :parameters ()
    :precondition 
      (in r512)
    :effect
      (and
        (in r511)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r512))
      )
    )
  (:action move-right-from-r513
    :parameters ()
    :precondition 
      (in r513)
    :effect
      (and
        (in r514)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r513))
      )
    )
  (:action move-left-from-r513
    :parameters ()
    :precondition 
      (in r513)
    :effect
      (and
        (in r512)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r513))
      )
    )
  (:action move-right-from-r514
    :parameters ()
    :precondition 
      (in r514)
    :effect
      (and
        (in r515)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r514))
      )
    )
  (:action move-left-from-r514
    :parameters ()
    :precondition 
      (in r514)
    :effect
      (and
        (in r513)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r514))
      )
    )
  (:action move-right-from-r515
    :parameters ()
    :precondition 
      (in r515)
    :effect
      (and
        (in r516)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r515))
      )
    )
  (:action move-left-from-r515
    :parameters ()
    :precondition 
      (in r515)
    :effect
      (and
        (in r514)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r515))
      )
    )
  (:action move-right-from-r516
    :parameters ()
    :precondition 
      (in r516)
    :effect
      (and
        (in r517)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r516))
      )
    )
  (:action move-left-from-r516
    :parameters ()
    :precondition 
      (in r516)
    :effect
      (and
        (in r515)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r516))
      )
    )
  (:action move-right-from-r517
    :parameters ()
    :precondition 
      (in r517)
    :effect
      (and
        (in r518)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r517))
      )
    )
  (:action move-left-from-r517
    :parameters ()
    :precondition 
      (in r517)
    :effect
      (and
        (in r516)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r517))
      )
    )
  (:action move-right-from-r518
    :parameters ()
    :precondition 
      (in r518)
    :effect
      (and
        (in r519)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r518))
      )
    )
  (:action move-left-from-r518
    :parameters ()
    :precondition 
      (in r518)
    :effect
      (and
        (in r517)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r518))
      )
    )
  (:action move-right-from-r519
    :parameters ()
    :precondition 
      (in r519)
    :effect
      (and
        (in r520)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r519))
      )
    )
  (:action move-left-from-r519
    :parameters ()
    :precondition 
      (in r519)
    :effect
      (and
        (in r518)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r519))
      )
    )
  (:action move-right-from-r520
    :parameters ()
    :precondition 
      (in r520)
    :effect
      (and
        (in r521)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r520))
      )
    )
  (:action move-left-from-r520
    :parameters ()
    :precondition 
      (in r520)
    :effect
      (and
        (in r519)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r520))
      )
    )
  (:action move-right-from-r521
    :parameters ()
    :precondition 
      (in r521)
    :effect
      (and
        (in r522)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r521))
      )
    )
  (:action move-left-from-r521
    :parameters ()
    :precondition 
      (in r521)
    :effect
      (and
        (in r520)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r521))
      )
    )
  (:action move-right-from-r522
    :parameters ()
    :precondition 
      (in r522)
    :effect
      (and
        (in r523)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r522))
      )
    )
  (:action move-left-from-r522
    :parameters ()
    :precondition 
      (in r522)
    :effect
      (and
        (in r521)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r522))
      )
    )
  (:action move-right-from-r523
    :parameters ()
    :precondition 
      (in r523)
    :effect
      (and
        (in r524)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r523))
      )
    )
  (:action move-left-from-r523
    :parameters ()
    :precondition 
      (in r523)
    :effect
      (and
        (in r522)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r523))
      )
    )
  (:action move-right-from-r524
    :parameters ()
    :precondition 
      (in r524)
    :effect
      (and
        (in r525)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r524))
      )
    )
  (:action move-left-from-r524
    :parameters ()
    :precondition 
      (in r524)
    :effect
      (and
        (in r523)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r524))
      )
    )
  (:action move-right-from-r525
    :parameters ()
    :precondition 
      (in r525)
    :effect
      (and
        (in r526)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r525))
      )
    )
  (:action move-left-from-r525
    :parameters ()
    :precondition 
      (in r525)
    :effect
      (and
        (in r524)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r525))
      )
    )
  (:action move-right-from-r526
    :parameters ()
    :precondition 
      (in r526)
    :effect
      (and
        (in r527)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r526))
      )
    )
  (:action move-left-from-r526
    :parameters ()
    :precondition 
      (in r526)
    :effect
      (and
        (in r525)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r526))
      )
    )
  (:action move-right-from-r527
    :parameters ()
    :precondition 
      (in r527)
    :effect
      (and
        (in r528)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r527))
      )
    )
  (:action move-left-from-r527
    :parameters ()
    :precondition 
      (in r527)
    :effect
      (and
        (in r526)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r527))
      )
    )
  (:action move-right-from-r528
    :parameters ()
    :precondition 
      (in r528)
    :effect
      (and
        (in r529)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r528))
      )
    )
  (:action move-left-from-r528
    :parameters ()
    :precondition 
      (in r528)
    :effect
      (and
        (in r527)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r528))
      )
    )
  (:action move-right-from-r529
    :parameters ()
    :precondition 
      (in r529)
    :effect
      (and
        (in r530)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r529))
      )
    )
  (:action move-left-from-r529
    :parameters ()
    :precondition 
      (in r529)
    :effect
      (and
        (in r528)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r529))
      )
    )
  (:action move-right-from-r530
    :parameters ()
    :precondition 
      (in r530)
    :effect
      (and
        (in r531)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r530))
      )
    )
  (:action move-left-from-r530
    :parameters ()
    :precondition 
      (in r530)
    :effect
      (and
        (in r529)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r530))
      )
    )
  (:action move-right-from-r531
    :parameters ()
    :precondition 
      (in r531)
    :effect
      (and
        (in r532)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r531))
      )
    )
  (:action move-left-from-r531
    :parameters ()
    :precondition 
      (in r531)
    :effect
      (and
        (in r530)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r531))
      )
    )
  (:action move-right-from-r532
    :parameters ()
    :precondition 
      (in r532)
    :effect
      (and
        (in r533)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r532))
      )
    )
  (:action move-left-from-r532
    :parameters ()
    :precondition 
      (in r532)
    :effect
      (and
        (in r531)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r532))
      )
    )
  (:action move-right-from-r533
    :parameters ()
    :precondition 
      (in r533)
    :effect
      (and
        (in r534)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r533))
      )
    )
  (:action move-left-from-r533
    :parameters ()
    :precondition 
      (in r533)
    :effect
      (and
        (in r532)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r533))
      )
    )
  (:action move-right-from-r534
    :parameters ()
    :precondition 
      (in r534)
    :effect
      (and
        (in r535)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r534))
      )
    )
  (:action move-left-from-r534
    :parameters ()
    :precondition 
      (in r534)
    :effect
      (and
        (in r533)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r534))
      )
    )
  (:action move-right-from-r535
    :parameters ()
    :precondition 
      (in r535)
    :effect
      (and
        (in r536)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r535))
      )
    )
  (:action move-left-from-r535
    :parameters ()
    :precondition 
      (in r535)
    :effect
      (and
        (in r534)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r535))
      )
    )
  (:action move-right-from-r536
    :parameters ()
    :precondition 
      (in r536)
    :effect
      (and
        (in r537)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r536))
      )
    )
  (:action move-left-from-r536
    :parameters ()
    :precondition 
      (in r536)
    :effect
      (and
        (in r535)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r536))
      )
    )
  (:action move-right-from-r537
    :parameters ()
    :precondition 
      (in r537)
    :effect
      (and
        (in r538)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r537))
      )
    )
  (:action move-left-from-r537
    :parameters ()
    :precondition 
      (in r537)
    :effect
      (and
        (in r536)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r537))
      )
    )
  (:action move-right-from-r538
    :parameters ()
    :precondition 
      (in r538)
    :effect
      (and
        (in r539)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r538))
      )
    )
  (:action move-left-from-r538
    :parameters ()
    :precondition 
      (in r538)
    :effect
      (and
        (in r537)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r538))
      )
    )
  (:action move-right-from-r539
    :parameters ()
    :precondition 
      (in r539)
    :effect
      (and
        (in r540)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r539))
      )
    )
  (:action move-left-from-r539
    :parameters ()
    :precondition 
      (in r539)
    :effect
      (and
        (in r538)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r539))
      )
    )
  (:action move-right-from-r540
    :parameters ()
    :precondition 
      (in r540)
    :effect
      (and
        (in r541)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r540))
      )
    )
  (:action move-left-from-r540
    :parameters ()
    :precondition 
      (in r540)
    :effect
      (and
        (in r539)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r540))
      )
    )
  (:action move-right-from-r541
    :parameters ()
    :precondition 
      (in r541)
    :effect
      (and
        (in r542)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r541))
      )
    )
  (:action move-left-from-r541
    :parameters ()
    :precondition 
      (in r541)
    :effect
      (and
        (in r540)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r541))
      )
    )
  (:action move-right-from-r542
    :parameters ()
    :precondition 
      (in r542)
    :effect
      (and
        (in r543)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r542))
      )
    )
  (:action move-left-from-r542
    :parameters ()
    :precondition 
      (in r542)
    :effect
      (and
        (in r541)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r542))
      )
    )
  (:action move-right-from-r543
    :parameters ()
    :precondition 
      (in r543)
    :effect
      (and
        (in r544)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r543))
      )
    )
  (:action move-left-from-r543
    :parameters ()
    :precondition 
      (in r543)
    :effect
      (and
        (in r542)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r543))
      )
    )
  (:action move-right-from-r544
    :parameters ()
    :precondition 
      (in r544)
    :effect
      (and
        (in r545)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r544))
      )
    )
  (:action move-left-from-r544
    :parameters ()
    :precondition 
      (in r544)
    :effect
      (and
        (in r543)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r544))
      )
    )
  (:action move-right-from-r545
    :parameters ()
    :precondition 
      (in r545)
    :effect
      (and
        (in r546)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r545))
      )
    )
  (:action move-left-from-r545
    :parameters ()
    :precondition 
      (in r545)
    :effect
      (and
        (in r544)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r545))
      )
    )
  (:action move-right-from-r546
    :parameters ()
    :precondition 
      (in r546)
    :effect
      (and
        (in r547)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r546))
      )
    )
  (:action move-left-from-r546
    :parameters ()
    :precondition 
      (in r546)
    :effect
      (and
        (in r545)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r546))
      )
    )
  (:action move-right-from-r547
    :parameters ()
    :precondition 
      (in r547)
    :effect
      (and
        (in r548)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r547))
      )
    )
  (:action move-left-from-r547
    :parameters ()
    :precondition 
      (in r547)
    :effect
      (and
        (in r546)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r547))
      )
    )
  (:action move-right-from-r548
    :parameters ()
    :precondition 
      (in r548)
    :effect
      (and
        (in r549)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r548))
      )
    )
  (:action move-left-from-r548
    :parameters ()
    :precondition 
      (in r548)
    :effect
      (and
        (in r547)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r548))
      )
    )
  (:action move-right-from-r549
    :parameters ()
    :precondition 
      (in r549)
    :effect
      (and
        (in r550)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r549))
      )
    )
  (:action move-left-from-r549
    :parameters ()
    :precondition 
      (in r549)
    :effect
      (and
        (in r548)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r549))
      )
    )
  (:action move-right-from-r550
    :parameters ()
    :precondition 
      (in r550)
    :effect
      (and
        (in r551)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r550))
      )
    )
  (:action move-left-from-r550
    :parameters ()
    :precondition 
      (in r550)
    :effect
      (and
        (in r549)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r550))
      )
    )
  (:action move-right-from-r551
    :parameters ()
    :precondition 
      (in r551)
    :effect
      (and
        (in r552)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r551))
      )
    )
  (:action move-left-from-r551
    :parameters ()
    :precondition 
      (in r551)
    :effect
      (and
        (in r550)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r551))
      )
    )
  (:action move-right-from-r552
    :parameters ()
    :precondition 
      (in r552)
    :effect
      (and
        (in r553)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r552))
      )
    )
  (:action move-left-from-r552
    :parameters ()
    :precondition 
      (in r552)
    :effect
      (and
        (in r551)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r552))
      )
    )
  (:action move-right-from-r553
    :parameters ()
    :precondition 
      (in r553)
    :effect
      (and
        (in r554)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r553))
      )
    )
  (:action move-left-from-r553
    :parameters ()
    :precondition 
      (in r553)
    :effect
      (and
        (in r552)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r553))
      )
    )
  (:action move-right-from-r554
    :parameters ()
    :precondition 
      (in r554)
    :effect
      (and
        (in r555)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r554))
      )
    )
  (:action move-left-from-r554
    :parameters ()
    :precondition 
      (in r554)
    :effect
      (and
        (in r553)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r554))
      )
    )
  (:action move-right-from-r555
    :parameters ()
    :precondition 
      (in r555)
    :effect
      (and
        (in r556)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r555))
      )
    )
  (:action move-left-from-r555
    :parameters ()
    :precondition 
      (in r555)
    :effect
      (and
        (in r554)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r555))
      )
    )
  (:action move-right-from-r556
    :parameters ()
    :precondition 
      (in r556)
    :effect
      (and
        (in r557)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r556))
      )
    )
  (:action move-left-from-r556
    :parameters ()
    :precondition 
      (in r556)
    :effect
      (and
        (in r555)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r556))
      )
    )
  (:action move-right-from-r557
    :parameters ()
    :precondition 
      (in r557)
    :effect
      (and
        (in r558)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r557))
      )
    )
  (:action move-left-from-r557
    :parameters ()
    :precondition 
      (in r557)
    :effect
      (and
        (in r556)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r557))
      )
    )
  (:action move-right-from-r558
    :parameters ()
    :precondition 
      (in r558)
    :effect
      (and
        (in r559)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r558))
      )
    )
  (:action move-left-from-r558
    :parameters ()
    :precondition 
      (in r558)
    :effect
      (and
        (in r557)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r558))
      )
    )
  (:action move-right-from-r559
    :parameters ()
    :precondition 
      (in r559)
    :effect
      (and
        (in r560)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r559))
      )
    )
  (:action move-left-from-r559
    :parameters ()
    :precondition 
      (in r559)
    :effect
      (and
        (in r558)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r559))
      )
    )
  (:action move-right-from-r560
    :parameters ()
    :precondition 
      (in r560)
    :effect
      (and
        (in r561)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r560))
      )
    )
  (:action move-left-from-r560
    :parameters ()
    :precondition 
      (in r560)
    :effect
      (and
        (in r559)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r560))
      )
    )
  (:action move-right-from-r561
    :parameters ()
    :precondition 
      (in r561)
    :effect
      (and
        (in r562)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r561))
      )
    )
  (:action move-left-from-r561
    :parameters ()
    :precondition 
      (in r561)
    :effect
      (and
        (in r560)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r561))
      )
    )
  (:action move-right-from-r562
    :parameters ()
    :precondition 
      (in r562)
    :effect
      (and
        (in r563)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r562))
      )
    )
  (:action move-left-from-r562
    :parameters ()
    :precondition 
      (in r562)
    :effect
      (and
        (in r561)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r562))
      )
    )
  (:action move-right-from-r563
    :parameters ()
    :precondition 
      (in r563)
    :effect
      (and
        (in r564)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r563))
      )
    )
  (:action move-left-from-r563
    :parameters ()
    :precondition 
      (in r563)
    :effect
      (and
        (in r562)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r563))
      )
    )
  (:action move-right-from-r564
    :parameters ()
    :precondition 
      (in r564)
    :effect
      (and
        (in r565)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r564))
      )
    )
  (:action move-left-from-r564
    :parameters ()
    :precondition 
      (in r564)
    :effect
      (and
        (in r563)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r564))
      )
    )
  (:action move-right-from-r565
    :parameters ()
    :precondition 
      (in r565)
    :effect
      (and
        (in r566)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r565))
      )
    )
  (:action move-left-from-r565
    :parameters ()
    :precondition 
      (in r565)
    :effect
      (and
        (in r564)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r565))
      )
    )
  (:action move-right-from-r566
    :parameters ()
    :precondition 
      (in r566)
    :effect
      (and
        (in r567)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r566))
      )
    )
  (:action move-left-from-r566
    :parameters ()
    :precondition 
      (in r566)
    :effect
      (and
        (in r565)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r566))
      )
    )
  (:action move-right-from-r567
    :parameters ()
    :precondition 
      (in r567)
    :effect
      (and
        (in r568)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r567))
      )
    )
  (:action move-left-from-r567
    :parameters ()
    :precondition 
      (in r567)
    :effect
      (and
        (in r566)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r567))
      )
    )
  (:action move-right-from-r568
    :parameters ()
    :precondition 
      (in r568)
    :effect
      (and
        (in r569)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r568))
      )
    )
  (:action move-left-from-r568
    :parameters ()
    :precondition 
      (in r568)
    :effect
      (and
        (in r567)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r568))
      )
    )
  (:action move-right-from-r569
    :parameters ()
    :precondition 
      (in r569)
    :effect
      (and
        (in r570)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r569))
      )
    )
  (:action move-left-from-r569
    :parameters ()
    :precondition 
      (in r569)
    :effect
      (and
        (in r568)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r569))
      )
    )
  (:action move-right-from-r570
    :parameters ()
    :precondition 
      (in r570)
    :effect
      (and
        (in r571)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r570))
      )
    )
  (:action move-left-from-r570
    :parameters ()
    :precondition 
      (in r570)
    :effect
      (and
        (in r569)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r570))
      )
    )
  (:action move-right-from-r571
    :parameters ()
    :precondition 
      (in r571)
    :effect
      (and
        (in r572)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r571))
      )
    )
  (:action move-left-from-r571
    :parameters ()
    :precondition 
      (in r571)
    :effect
      (and
        (in r570)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r571))
      )
    )
  (:action move-right-from-r572
    :parameters ()
    :precondition 
      (in r572)
    :effect
      (and
        (in r573)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r572))
      )
    )
  (:action move-left-from-r572
    :parameters ()
    :precondition 
      (in r572)
    :effect
      (and
        (in r571)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r572))
      )
    )
  (:action move-right-from-r573
    :parameters ()
    :precondition 
      (in r573)
    :effect
      (and
        (in r574)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r573))
      )
    )
  (:action move-left-from-r573
    :parameters ()
    :precondition 
      (in r573)
    :effect
      (and
        (in r572)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r573))
      )
    )
  (:action move-right-from-r574
    :parameters ()
    :precondition 
      (in r574)
    :effect
      (and
        (in r575)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r574))
      )
    )
  (:action move-left-from-r574
    :parameters ()
    :precondition 
      (in r574)
    :effect
      (and
        (in r573)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r574))
      )
    )
  (:action move-right-from-r575
    :parameters ()
    :precondition 
      (in r575)
    :effect
      (and
        (in r576)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r575))
      )
    )
  (:action move-left-from-r575
    :parameters ()
    :precondition 
      (in r575)
    :effect
      (and
        (in r574)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r575))
      )
    )
  (:action move-right-from-r576
    :parameters ()
    :precondition 
      (in r576)
    :effect
      (and
        (in r577)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r576))
      )
    )
  (:action move-left-from-r576
    :parameters ()
    :precondition 
      (in r576)
    :effect
      (and
        (in r575)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r576))
      )
    )
  (:action move-right-from-r577
    :parameters ()
    :precondition 
      (in r577)
    :effect
      (and
        (in r578)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r577))
      )
    )
  (:action move-left-from-r577
    :parameters ()
    :precondition 
      (in r577)
    :effect
      (and
        (in r576)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r577))
      )
    )
  (:action move-right-from-r578
    :parameters ()
    :precondition 
      (in r578)
    :effect
      (and
        (in r579)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r578))
      )
    )
  (:action move-left-from-r578
    :parameters ()
    :precondition 
      (in r578)
    :effect
      (and
        (in r577)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r578))
      )
    )
  (:action move-right-from-r579
    :parameters ()
    :precondition 
      (in r579)
    :effect
      (and
        (in r580)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r579))
      )
    )
  (:action move-left-from-r579
    :parameters ()
    :precondition 
      (in r579)
    :effect
      (and
        (in r578)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r579))
      )
    )
  (:action move-right-from-r580
    :parameters ()
    :precondition 
      (in r580)
    :effect
      (and
        (in r581)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r580))
      )
    )
  (:action move-left-from-r580
    :parameters ()
    :precondition 
      (in r580)
    :effect
      (and
        (in r579)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r580))
      )
    )
  (:action move-right-from-r581
    :parameters ()
    :precondition 
      (in r581)
    :effect
      (and
        (in r582)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r581))
      )
    )
  (:action move-left-from-r581
    :parameters ()
    :precondition 
      (in r581)
    :effect
      (and
        (in r580)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r581))
      )
    )
  (:action move-right-from-r582
    :parameters ()
    :precondition 
      (in r582)
    :effect
      (and
        (in r583)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r582))
      )
    )
  (:action move-left-from-r582
    :parameters ()
    :precondition 
      (in r582)
    :effect
      (and
        (in r581)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r582))
      )
    )
  (:action move-right-from-r583
    :parameters ()
    :precondition 
      (in r583)
    :effect
      (and
        (in r584)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r583))
      )
    )
  (:action move-left-from-r583
    :parameters ()
    :precondition 
      (in r583)
    :effect
      (and
        (in r582)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r583))
      )
    )
  (:action move-right-from-r584
    :parameters ()
    :precondition 
      (in r584)
    :effect
      (and
        (in r585)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r584))
      )
    )
  (:action move-left-from-r584
    :parameters ()
    :precondition 
      (in r584)
    :effect
      (and
        (in r583)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r584))
      )
    )
  (:action move-right-from-r585
    :parameters ()
    :precondition 
      (in r585)
    :effect
      (and
        (in r586)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r585))
      )
    )
  (:action move-left-from-r585
    :parameters ()
    :precondition 
      (in r585)
    :effect
      (and
        (in r584)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r585))
      )
    )
  (:action move-right-from-r586
    :parameters ()
    :precondition 
      (in r586)
    :effect
      (and
        (in r587)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r586))
      )
    )
  (:action move-left-from-r586
    :parameters ()
    :precondition 
      (in r586)
    :effect
      (and
        (in r585)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r586))
      )
    )
  (:action move-right-from-r587
    :parameters ()
    :precondition 
      (in r587)
    :effect
      (and
        (in r588)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r587))
      )
    )
  (:action move-left-from-r587
    :parameters ()
    :precondition 
      (in r587)
    :effect
      (and
        (in r586)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r587))
      )
    )
  (:action move-right-from-r588
    :parameters ()
    :precondition 
      (in r588)
    :effect
      (and
        (in r589)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r588))
      )
    )
  (:action move-left-from-r588
    :parameters ()
    :precondition 
      (in r588)
    :effect
      (and
        (in r587)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r588))
      )
    )
  (:action move-right-from-r589
    :parameters ()
    :precondition 
      (in r589)
    :effect
      (and
        (in r590)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r589))
      )
    )
  (:action move-left-from-r589
    :parameters ()
    :precondition 
      (in r589)
    :effect
      (and
        (in r588)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r589))
      )
    )
  (:action move-right-from-r590
    :parameters ()
    :precondition 
      (in r590)
    :effect
      (and
        (in r591)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r590))
      )
    )
  (:action move-left-from-r590
    :parameters ()
    :precondition 
      (in r590)
    :effect
      (and
        (in r589)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r590))
      )
    )
  (:action move-right-from-r591
    :parameters ()
    :precondition 
      (in r591)
    :effect
      (and
        (in r592)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r591))
      )
    )
  (:action move-left-from-r591
    :parameters ()
    :precondition 
      (in r591)
    :effect
      (and
        (in r590)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r591))
      )
    )
  (:action move-right-from-r592
    :parameters ()
    :precondition 
      (in r592)
    :effect
      (and
        (in r593)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r592))
      )
    )
  (:action move-left-from-r592
    :parameters ()
    :precondition 
      (in r592)
    :effect
      (and
        (in r591)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r592))
      )
    )
  (:action move-right-from-r593
    :parameters ()
    :precondition 
      (in r593)
    :effect
      (and
        (in r594)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r593))
      )
    )
  (:action move-left-from-r593
    :parameters ()
    :precondition 
      (in r593)
    :effect
      (and
        (in r592)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r593))
      )
    )
  (:action move-right-from-r594
    :parameters ()
    :precondition 
      (in r594)
    :effect
      (and
        (in r595)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r594))
      )
    )
  (:action move-left-from-r594
    :parameters ()
    :precondition 
      (in r594)
    :effect
      (and
        (in r593)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r594))
      )
    )
  (:action move-right-from-r595
    :parameters ()
    :precondition 
      (in r595)
    :effect
      (and
        (in r596)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r595))
      )
    )
  (:action move-left-from-r595
    :parameters ()
    :precondition 
      (in r595)
    :effect
      (and
        (in r594)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r595))
      )
    )
  (:action move-right-from-r596
    :parameters ()
    :precondition 
      (in r596)
    :effect
      (and
        (in r597)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r596))
      )
    )
  (:action move-left-from-r596
    :parameters ()
    :precondition 
      (in r596)
    :effect
      (and
        (in r595)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r596))
      )
    )
  (:action move-right-from-r597
    :parameters ()
    :precondition 
      (in r597)
    :effect
      (and
        (in r598)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r597))
      )
    )
  (:action move-left-from-r597
    :parameters ()
    :precondition 
      (in r597)
    :effect
      (and
        (in r596)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r597))
      )
    )
  (:action move-right-from-r598
    :parameters ()
    :precondition 
      (in r598)
    :effect
      (and
        (in r599)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r598))
      )
    )
  (:action move-left-from-r598
    :parameters ()
    :precondition 
      (in r598)
    :effect
      (and
        (in r597)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r598))
      )
    )
  (:action move-right-from-r599_detdup_0
    :parameters ()
    :precondition 
      (in r599)
    :effect
      (and
        (in r600)
        (not 
          (in r599))
        (not 
          (seen))
      )
    )
  (:action move-right-from-r599_detdup_1
    :parameters ()
    :precondition 
      (in r599)
    :effect
      (and
        (in r600)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r599))
      )
    )
  (:action move-left-from-r599
    :parameters ()
    :precondition 
      (in r599)
    :effect
      (and
        (in r598)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r599))
      )
    )
  (:action move-right-from-r600
    :parameters ()
    :precondition 
      (and
        (in r600)
        (not 
          (seen)))
    :effect
      (and
        (in r1)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r600))
      )
    )
  (:action move-left-from-r600
    :parameters ()
    :precondition 
      (and
        (in r600)
        (not 
          (seen)))
    :effect
      (and
        (in r599)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r600))
      )
    )
  (:action stay
    :parameters ()
    :precondition 
      (and
        (not 
          (in r300))
        (not 
          (in r600)))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r300-seen
    :parameters ()
    :precondition 
      (and
        (in r300)
        (seen))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r300-not-seen_detdup_0
    :parameters ()
    :precondition 
      (and
        (in r300)
        (not 
          (seen)))
    :effect
(not 
        (seen))    )
  (:action stay-in-r300-not-seen_detdup_1
    :parameters ()
    :precondition 
      (and
        (in r300)
        (not 
          (seen)))
    :effect
      (and
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
      )
    )
  (:action stay-in-r600-seen
    :parameters ()
    :precondition 
      (and
        (in r600)
        (seen))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r600-not-seen_detdup_0
    :parameters ()
    :precondition 
      (and
        (in r600)
        (not 
          (seen)))
    :effect
(not 
        (seen))    )
  (:action stay-in-r600-not-seen_detdup_1
    :parameters ()
    :precondition 
      (and
        (in r600)
        (not 
          (seen)))
    :effect
      (and
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
      )
    )
)