(define (domain waldo)
  (:types
    room - none
  )

  (:predicates
    (seen)
    (in ?x0 - room)
    (autstate_1_2)
    (autstate_1_1)
  )
  (:action move-right-from-r1
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r2)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r1))
      )
    )
  (:action move-left-from-r1_detdup_0
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r1000)
        (not 
          (in r1))
        (not 
          (seen))
      )
    )
  (:action move-left-from-r1_detdup_1
    :parameters ()
    :precondition 
      (in r1)
    :effect
      (and
        (in r1000)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r1))
      )
    )
  (:action move-right-from-r2
    :parameters ()
    :precondition 
      (in r2)
    :effect
      (and
        (in r3)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r2))
      )
    )
  (:action move-left-from-r2
    :parameters ()
    :precondition 
      (in r2)
    :effect
      (and
        (in r1)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r2))
      )
    )
  (:action move-right-from-r3
    :parameters ()
    :precondition 
      (in r3)
    :effect
      (and
        (in r4)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r3))
      )
    )
  (:action move-left-from-r3
    :parameters ()
    :precondition 
      (in r3)
    :effect
      (and
        (in r2)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r3))
      )
    )
  (:action move-right-from-r4
    :parameters ()
    :precondition 
      (in r4)
    :effect
      (and
        (in r5)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r4))
      )
    )
  (:action move-left-from-r4
    :parameters ()
    :precondition 
      (in r4)
    :effect
      (and
        (in r3)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r4))
      )
    )
  (:action move-right-from-r5
    :parameters ()
    :precondition 
      (in r5)
    :effect
      (and
        (in r6)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r5))
      )
    )
  (:action move-left-from-r5
    :parameters ()
    :precondition 
      (in r5)
    :effect
      (and
        (in r4)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r5))
      )
    )
  (:action move-right-from-r6
    :parameters ()
    :precondition 
      (in r6)
    :effect
      (and
        (in r7)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r6))
      )
    )
  (:action move-left-from-r6
    :parameters ()
    :precondition 
      (in r6)
    :effect
      (and
        (in r5)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r6))
      )
    )
  (:action move-right-from-r7
    :parameters ()
    :precondition 
      (in r7)
    :effect
      (and
        (in r8)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r7))
      )
    )
  (:action move-left-from-r7
    :parameters ()
    :precondition 
      (in r7)
    :effect
      (and
        (in r6)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r7))
      )
    )
  (:action move-right-from-r8
    :parameters ()
    :precondition 
      (in r8)
    :effect
      (and
        (in r9)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r8))
      )
    )
  (:action move-left-from-r8
    :parameters ()
    :precondition 
      (in r8)
    :effect
      (and
        (in r7)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r8))
      )
    )
  (:action move-right-from-r9
    :parameters ()
    :precondition 
      (in r9)
    :effect
      (and
        (in r10)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r9))
      )
    )
  (:action move-left-from-r9
    :parameters ()
    :precondition 
      (in r9)
    :effect
      (and
        (in r8)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r9))
      )
    )
  (:action move-right-from-r10
    :parameters ()
    :precondition 
      (in r10)
    :effect
      (and
        (in r11)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r10))
      )
    )
  (:action move-left-from-r10
    :parameters ()
    :precondition 
      (in r10)
    :effect
      (and
        (in r9)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r10))
      )
    )
  (:action move-right-from-r11
    :parameters ()
    :precondition 
      (in r11)
    :effect
      (and
        (in r12)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r11))
      )
    )
  (:action move-left-from-r11
    :parameters ()
    :precondition 
      (in r11)
    :effect
      (and
        (in r10)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r11))
      )
    )
  (:action move-right-from-r12
    :parameters ()
    :precondition 
      (in r12)
    :effect
      (and
        (in r13)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r12))
      )
    )
  (:action move-left-from-r12
    :parameters ()
    :precondition 
      (in r12)
    :effect
      (and
        (in r11)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r12))
      )
    )
  (:action move-right-from-r13
    :parameters ()
    :precondition 
      (in r13)
    :effect
      (and
        (in r14)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r13))
      )
    )
  (:action move-left-from-r13
    :parameters ()
    :precondition 
      (in r13)
    :effect
      (and
        (in r12)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r13))
      )
    )
  (:action move-right-from-r14
    :parameters ()
    :precondition 
      (in r14)
    :effect
      (and
        (in r15)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r14))
      )
    )
  (:action move-left-from-r14
    :parameters ()
    :precondition 
      (in r14)
    :effect
      (and
        (in r13)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r14))
      )
    )
  (:action move-right-from-r15
    :parameters ()
    :precondition 
      (in r15)
    :effect
      (and
        (in r16)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r15))
      )
    )
  (:action move-left-from-r15
    :parameters ()
    :precondition 
      (in r15)
    :effect
      (and
        (in r14)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r15))
      )
    )
  (:action move-right-from-r16
    :parameters ()
    :precondition 
      (in r16)
    :effect
      (and
        (in r17)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r16))
      )
    )
  (:action move-left-from-r16
    :parameters ()
    :precondition 
      (in r16)
    :effect
      (and
        (in r15)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r16))
      )
    )
  (:action move-right-from-r17
    :parameters ()
    :precondition 
      (in r17)
    :effect
      (and
        (in r18)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r17))
      )
    )
  (:action move-left-from-r17
    :parameters ()
    :precondition 
      (in r17)
    :effect
      (and
        (in r16)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r17))
      )
    )
  (:action move-right-from-r18
    :parameters ()
    :precondition 
      (in r18)
    :effect
      (and
        (in r19)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r18))
      )
    )
  (:action move-left-from-r18
    :parameters ()
    :precondition 
      (in r18)
    :effect
      (and
        (in r17)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r18))
      )
    )
  (:action move-right-from-r19
    :parameters ()
    :precondition 
      (in r19)
    :effect
      (and
        (in r20)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r19))
      )
    )
  (:action move-left-from-r19
    :parameters ()
    :precondition 
      (in r19)
    :effect
      (and
        (in r18)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r19))
      )
    )
  (:action move-right-from-r20
    :parameters ()
    :precondition 
      (in r20)
    :effect
      (and
        (in r21)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r20))
      )
    )
  (:action move-left-from-r20
    :parameters ()
    :precondition 
      (in r20)
    :effect
      (and
        (in r19)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r20))
      )
    )
  (:action move-right-from-r21
    :parameters ()
    :precondition 
      (in r21)
    :effect
      (and
        (in r22)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r21))
      )
    )
  (:action move-left-from-r21
    :parameters ()
    :precondition 
      (in r21)
    :effect
      (and
        (in r20)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r21))
      )
    )
  (:action move-right-from-r22
    :parameters ()
    :precondition 
      (in r22)
    :effect
      (and
        (in r23)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r22))
      )
    )
  (:action move-left-from-r22
    :parameters ()
    :precondition 
      (in r22)
    :effect
      (and
        (in r21)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r22))
      )
    )
  (:action move-right-from-r23
    :parameters ()
    :precondition 
      (in r23)
    :effect
      (and
        (in r24)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r23))
      )
    )
  (:action move-left-from-r23
    :parameters ()
    :precondition 
      (in r23)
    :effect
      (and
        (in r22)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r23))
      )
    )
  (:action move-right-from-r24
    :parameters ()
    :precondition 
      (in r24)
    :effect
      (and
        (in r25)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r24))
      )
    )
  (:action move-left-from-r24
    :parameters ()
    :precondition 
      (in r24)
    :effect
      (and
        (in r23)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r24))
      )
    )
  (:action move-right-from-r25
    :parameters ()
    :precondition 
      (in r25)
    :effect
      (and
        (in r26)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r25))
      )
    )
  (:action move-left-from-r25
    :parameters ()
    :precondition 
      (in r25)
    :effect
      (and
        (in r24)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r25))
      )
    )
  (:action move-right-from-r26
    :parameters ()
    :precondition 
      (in r26)
    :effect
      (and
        (in r27)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r26))
      )
    )
  (:action move-left-from-r26
    :parameters ()
    :precondition 
      (in r26)
    :effect
      (and
        (in r25)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r26))
      )
    )
  (:action move-right-from-r27
    :parameters ()
    :precondition 
      (in r27)
    :effect
      (and
        (in r28)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r27))
      )
    )
  (:action move-left-from-r27
    :parameters ()
    :precondition 
      (in r27)
    :effect
      (and
        (in r26)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r27))
      )
    )
  (:action move-right-from-r28
    :parameters ()
    :precondition 
      (in r28)
    :effect
      (and
        (in r29)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r28))
      )
    )
  (:action move-left-from-r28
    :parameters ()
    :precondition 
      (in r28)
    :effect
      (and
        (in r27)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r28))
      )
    )
  (:action move-right-from-r29
    :parameters ()
    :precondition 
      (in r29)
    :effect
      (and
        (in r30)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r29))
      )
    )
  (:action move-left-from-r29
    :parameters ()
    :precondition 
      (in r29)
    :effect
      (and
        (in r28)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r29))
      )
    )
  (:action move-right-from-r30
    :parameters ()
    :precondition 
      (in r30)
    :effect
      (and
        (in r31)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r30))
      )
    )
  (:action move-left-from-r30
    :parameters ()
    :precondition 
      (in r30)
    :effect
      (and
        (in r29)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r30))
      )
    )
  (:action move-right-from-r31
    :parameters ()
    :precondition 
      (in r31)
    :effect
      (and
        (in r32)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r31))
      )
    )
  (:action move-left-from-r31
    :parameters ()
    :precondition 
      (in r31)
    :effect
      (and
        (in r30)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r31))
      )
    )
  (:action move-right-from-r32
    :parameters ()
    :precondition 
      (in r32)
    :effect
      (and
        (in r33)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r32))
      )
    )
  (:action move-left-from-r32
    :parameters ()
    :precondition 
      (in r32)
    :effect
      (and
        (in r31)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r32))
      )
    )
  (:action move-right-from-r33
    :parameters ()
    :precondition 
      (in r33)
    :effect
      (and
        (in r34)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r33))
      )
    )
  (:action move-left-from-r33
    :parameters ()
    :precondition 
      (in r33)
    :effect
      (and
        (in r32)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r33))
      )
    )
  (:action move-right-from-r34
    :parameters ()
    :precondition 
      (in r34)
    :effect
      (and
        (in r35)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r34))
      )
    )
  (:action move-left-from-r34
    :parameters ()
    :precondition 
      (in r34)
    :effect
      (and
        (in r33)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r34))
      )
    )
  (:action move-right-from-r35
    :parameters ()
    :precondition 
      (in r35)
    :effect
      (and
        (in r36)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r35))
      )
    )
  (:action move-left-from-r35
    :parameters ()
    :precondition 
      (in r35)
    :effect
      (and
        (in r34)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r35))
      )
    )
  (:action move-right-from-r36
    :parameters ()
    :precondition 
      (in r36)
    :effect
      (and
        (in r37)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r36))
      )
    )
  (:action move-left-from-r36
    :parameters ()
    :precondition 
      (in r36)
    :effect
      (and
        (in r35)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r36))
      )
    )
  (:action move-right-from-r37
    :parameters ()
    :precondition 
      (in r37)
    :effect
      (and
        (in r38)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r37))
      )
    )
  (:action move-left-from-r37
    :parameters ()
    :precondition 
      (in r37)
    :effect
      (and
        (in r36)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r37))
      )
    )
  (:action move-right-from-r38
    :parameters ()
    :precondition 
      (in r38)
    :effect
      (and
        (in r39)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r38))
      )
    )
  (:action move-left-from-r38
    :parameters ()
    :precondition 
      (in r38)
    :effect
      (and
        (in r37)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r38))
      )
    )
  (:action move-right-from-r39
    :parameters ()
    :precondition 
      (in r39)
    :effect
      (and
        (in r40)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r39))
      )
    )
  (:action move-left-from-r39
    :parameters ()
    :precondition 
      (in r39)
    :effect
      (and
        (in r38)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r39))
      )
    )
  (:action move-right-from-r40
    :parameters ()
    :precondition 
      (in r40)
    :effect
      (and
        (in r41)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r40))
      )
    )
  (:action move-left-from-r40
    :parameters ()
    :precondition 
      (in r40)
    :effect
      (and
        (in r39)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r40))
      )
    )
  (:action move-right-from-r41
    :parameters ()
    :precondition 
      (in r41)
    :effect
      (and
        (in r42)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r41))
      )
    )
  (:action move-left-from-r41
    :parameters ()
    :precondition 
      (in r41)
    :effect
      (and
        (in r40)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r41))
      )
    )
  (:action move-right-from-r42
    :parameters ()
    :precondition 
      (in r42)
    :effect
      (and
        (in r43)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r42))
      )
    )
  (:action move-left-from-r42
    :parameters ()
    :precondition 
      (in r42)
    :effect
      (and
        (in r41)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r42))
      )
    )
  (:action move-right-from-r43
    :parameters ()
    :precondition 
      (in r43)
    :effect
      (and
        (in r44)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r43))
      )
    )
  (:action move-left-from-r43
    :parameters ()
    :precondition 
      (in r43)
    :effect
      (and
        (in r42)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r43))
      )
    )
  (:action move-right-from-r44
    :parameters ()
    :precondition 
      (in r44)
    :effect
      (and
        (in r45)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r44))
      )
    )
  (:action move-left-from-r44
    :parameters ()
    :precondition 
      (in r44)
    :effect
      (and
        (in r43)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r44))
      )
    )
  (:action move-right-from-r45
    :parameters ()
    :precondition 
      (in r45)
    :effect
      (and
        (in r46)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r45))
      )
    )
  (:action move-left-from-r45
    :parameters ()
    :precondition 
      (in r45)
    :effect
      (and
        (in r44)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r45))
      )
    )
  (:action move-right-from-r46
    :parameters ()
    :precondition 
      (in r46)
    :effect
      (and
        (in r47)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r46))
      )
    )
  (:action move-left-from-r46
    :parameters ()
    :precondition 
      (in r46)
    :effect
      (and
        (in r45)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r46))
      )
    )
  (:action move-right-from-r47
    :parameters ()
    :precondition 
      (in r47)
    :effect
      (and
        (in r48)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r47))
      )
    )
  (:action move-left-from-r47
    :parameters ()
    :precondition 
      (in r47)
    :effect
      (and
        (in r46)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r47))
      )
    )
  (:action move-right-from-r48
    :parameters ()
    :precondition 
      (in r48)
    :effect
      (and
        (in r49)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r48))
      )
    )
  (:action move-left-from-r48
    :parameters ()
    :precondition 
      (in r48)
    :effect
      (and
        (in r47)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r48))
      )
    )
  (:action move-right-from-r49
    :parameters ()
    :precondition 
      (in r49)
    :effect
      (and
        (in r50)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r49))
      )
    )
  (:action move-left-from-r49
    :parameters ()
    :precondition 
      (in r49)
    :effect
      (and
        (in r48)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r49))
      )
    )
  (:action move-right-from-r50
    :parameters ()
    :precondition 
      (in r50)
    :effect
      (and
        (in r51)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r50))
      )
    )
  (:action move-left-from-r50
    :parameters ()
    :precondition 
      (in r50)
    :effect
      (and
        (in r49)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r50))
      )
    )
  (:action move-right-from-r51
    :parameters ()
    :precondition 
      (in r51)
    :effect
      (and
        (in r52)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r51))
      )
    )
  (:action move-left-from-r51
    :parameters ()
    :precondition 
      (in r51)
    :effect
      (and
        (in r50)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r51))
      )
    )
  (:action move-right-from-r52
    :parameters ()
    :precondition 
      (in r52)
    :effect
      (and
        (in r53)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r52))
      )
    )
  (:action move-left-from-r52
    :parameters ()
    :precondition 
      (in r52)
    :effect
      (and
        (in r51)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r52))
      )
    )
  (:action move-right-from-r53
    :parameters ()
    :precondition 
      (in r53)
    :effect
      (and
        (in r54)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r53))
      )
    )
  (:action move-left-from-r53
    :parameters ()
    :precondition 
      (in r53)
    :effect
      (and
        (in r52)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r53))
      )
    )
  (:action move-right-from-r54
    :parameters ()
    :precondition 
      (in r54)
    :effect
      (and
        (in r55)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r54))
      )
    )
  (:action move-left-from-r54
    :parameters ()
    :precondition 
      (in r54)
    :effect
      (and
        (in r53)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r54))
      )
    )
  (:action move-right-from-r55
    :parameters ()
    :precondition 
      (in r55)
    :effect
      (and
        (in r56)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r55))
      )
    )
  (:action move-left-from-r55
    :parameters ()
    :precondition 
      (in r55)
    :effect
      (and
        (in r54)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r55))
      )
    )
  (:action move-right-from-r56
    :parameters ()
    :precondition 
      (in r56)
    :effect
      (and
        (in r57)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r56))
      )
    )
  (:action move-left-from-r56
    :parameters ()
    :precondition 
      (in r56)
    :effect
      (and
        (in r55)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r56))
      )
    )
  (:action move-right-from-r57
    :parameters ()
    :precondition 
      (in r57)
    :effect
      (and
        (in r58)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r57))
      )
    )
  (:action move-left-from-r57
    :parameters ()
    :precondition 
      (in r57)
    :effect
      (and
        (in r56)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r57))
      )
    )
  (:action move-right-from-r58
    :parameters ()
    :precondition 
      (in r58)
    :effect
      (and
        (in r59)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r58))
      )
    )
  (:action move-left-from-r58
    :parameters ()
    :precondition 
      (in r58)
    :effect
      (and
        (in r57)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r58))
      )
    )
  (:action move-right-from-r59
    :parameters ()
    :precondition 
      (in r59)
    :effect
      (and
        (in r60)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r59))
      )
    )
  (:action move-left-from-r59
    :parameters ()
    :precondition 
      (in r59)
    :effect
      (and
        (in r58)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r59))
      )
    )
  (:action move-right-from-r60
    :parameters ()
    :precondition 
      (in r60)
    :effect
      (and
        (in r61)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r60))
      )
    )
  (:action move-left-from-r60
    :parameters ()
    :precondition 
      (in r60)
    :effect
      (and
        (in r59)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r60))
      )
    )
  (:action move-right-from-r61
    :parameters ()
    :precondition 
      (in r61)
    :effect
      (and
        (in r62)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r61))
      )
    )
  (:action move-left-from-r61
    :parameters ()
    :precondition 
      (in r61)
    :effect
      (and
        (in r60)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r61))
      )
    )
  (:action move-right-from-r62
    :parameters ()
    :precondition 
      (in r62)
    :effect
      (and
        (in r63)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r62))
      )
    )
  (:action move-left-from-r62
    :parameters ()
    :precondition 
      (in r62)
    :effect
      (and
        (in r61)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r62))
      )
    )
  (:action move-right-from-r63
    :parameters ()
    :precondition 
      (in r63)
    :effect
      (and
        (in r64)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r63))
      )
    )
  (:action move-left-from-r63
    :parameters ()
    :precondition 
      (in r63)
    :effect
      (and
        (in r62)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r63))
      )
    )
  (:action move-right-from-r64
    :parameters ()
    :precondition 
      (in r64)
    :effect
      (and
        (in r65)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r64))
      )
    )
  (:action move-left-from-r64
    :parameters ()
    :precondition 
      (in r64)
    :effect
      (and
        (in r63)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r64))
      )
    )
  (:action move-right-from-r65
    :parameters ()
    :precondition 
      (in r65)
    :effect
      (and
        (in r66)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r65))
      )
    )
  (:action move-left-from-r65
    :parameters ()
    :precondition 
      (in r65)
    :effect
      (and
        (in r64)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r65))
      )
    )
  (:action move-right-from-r66
    :parameters ()
    :precondition 
      (in r66)
    :effect
      (and
        (in r67)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r66))
      )
    )
  (:action move-left-from-r66
    :parameters ()
    :precondition 
      (in r66)
    :effect
      (and
        (in r65)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r66))
      )
    )
  (:action move-right-from-r67
    :parameters ()
    :precondition 
      (in r67)
    :effect
      (and
        (in r68)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r67))
      )
    )
  (:action move-left-from-r67
    :parameters ()
    :precondition 
      (in r67)
    :effect
      (and
        (in r66)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r67))
      )
    )
  (:action move-right-from-r68
    :parameters ()
    :precondition 
      (in r68)
    :effect
      (and
        (in r69)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r68))
      )
    )
  (:action move-left-from-r68
    :parameters ()
    :precondition 
      (in r68)
    :effect
      (and
        (in r67)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r68))
      )
    )
  (:action move-right-from-r69
    :parameters ()
    :precondition 
      (in r69)
    :effect
      (and
        (in r70)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r69))
      )
    )
  (:action move-left-from-r69
    :parameters ()
    :precondition 
      (in r69)
    :effect
      (and
        (in r68)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r69))
      )
    )
  (:action move-right-from-r70
    :parameters ()
    :precondition 
      (in r70)
    :effect
      (and
        (in r71)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r70))
      )
    )
  (:action move-left-from-r70
    :parameters ()
    :precondition 
      (in r70)
    :effect
      (and
        (in r69)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r70))
      )
    )
  (:action move-right-from-r71
    :parameters ()
    :precondition 
      (in r71)
    :effect
      (and
        (in r72)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r71))
      )
    )
  (:action move-left-from-r71
    :parameters ()
    :precondition 
      (in r71)
    :effect
      (and
        (in r70)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r71))
      )
    )
  (:action move-right-from-r72
    :parameters ()
    :precondition 
      (in r72)
    :effect
      (and
        (in r73)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r72))
      )
    )
  (:action move-left-from-r72
    :parameters ()
    :precondition 
      (in r72)
    :effect
      (and
        (in r71)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r72))
      )
    )
  (:action move-right-from-r73
    :parameters ()
    :precondition 
      (in r73)
    :effect
      (and
        (in r74)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r73))
      )
    )
  (:action move-left-from-r73
    :parameters ()
    :precondition 
      (in r73)
    :effect
      (and
        (in r72)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r73))
      )
    )
  (:action move-right-from-r74
    :parameters ()
    :precondition 
      (in r74)
    :effect
      (and
        (in r75)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r74))
      )
    )
  (:action move-left-from-r74
    :parameters ()
    :precondition 
      (in r74)
    :effect
      (and
        (in r73)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r74))
      )
    )
  (:action move-right-from-r75
    :parameters ()
    :precondition 
      (in r75)
    :effect
      (and
        (in r76)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r75))
      )
    )
  (:action move-left-from-r75
    :parameters ()
    :precondition 
      (in r75)
    :effect
      (and
        (in r74)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r75))
      )
    )
  (:action move-right-from-r76
    :parameters ()
    :precondition 
      (in r76)
    :effect
      (and
        (in r77)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r76))
      )
    )
  (:action move-left-from-r76
    :parameters ()
    :precondition 
      (in r76)
    :effect
      (and
        (in r75)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r76))
      )
    )
  (:action move-right-from-r77
    :parameters ()
    :precondition 
      (in r77)
    :effect
      (and
        (in r78)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r77))
      )
    )
  (:action move-left-from-r77
    :parameters ()
    :precondition 
      (in r77)
    :effect
      (and
        (in r76)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r77))
      )
    )
  (:action move-right-from-r78
    :parameters ()
    :precondition 
      (in r78)
    :effect
      (and
        (in r79)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r78))
      )
    )
  (:action move-left-from-r78
    :parameters ()
    :precondition 
      (in r78)
    :effect
      (and
        (in r77)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r78))
      )
    )
  (:action move-right-from-r79
    :parameters ()
    :precondition 
      (in r79)
    :effect
      (and
        (in r80)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r79))
      )
    )
  (:action move-left-from-r79
    :parameters ()
    :precondition 
      (in r79)
    :effect
      (and
        (in r78)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r79))
      )
    )
  (:action move-right-from-r80
    :parameters ()
    :precondition 
      (in r80)
    :effect
      (and
        (in r81)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r80))
      )
    )
  (:action move-left-from-r80
    :parameters ()
    :precondition 
      (in r80)
    :effect
      (and
        (in r79)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r80))
      )
    )
  (:action move-right-from-r81
    :parameters ()
    :precondition 
      (in r81)
    :effect
      (and
        (in r82)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r81))
      )
    )
  (:action move-left-from-r81
    :parameters ()
    :precondition 
      (in r81)
    :effect
      (and
        (in r80)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r81))
      )
    )
  (:action move-right-from-r82
    :parameters ()
    :precondition 
      (in r82)
    :effect
      (and
        (in r83)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r82))
      )
    )
  (:action move-left-from-r82
    :parameters ()
    :precondition 
      (in r82)
    :effect
      (and
        (in r81)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r82))
      )
    )
  (:action move-right-from-r83
    :parameters ()
    :precondition 
      (in r83)
    :effect
      (and
        (in r84)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r83))
      )
    )
  (:action move-left-from-r83
    :parameters ()
    :precondition 
      (in r83)
    :effect
      (and
        (in r82)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r83))
      )
    )
  (:action move-right-from-r84
    :parameters ()
    :precondition 
      (in r84)
    :effect
      (and
        (in r85)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r84))
      )
    )
  (:action move-left-from-r84
    :parameters ()
    :precondition 
      (in r84)
    :effect
      (and
        (in r83)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r84))
      )
    )
  (:action move-right-from-r85
    :parameters ()
    :precondition 
      (in r85)
    :effect
      (and
        (in r86)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r85))
      )
    )
  (:action move-left-from-r85
    :parameters ()
    :precondition 
      (in r85)
    :effect
      (and
        (in r84)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r85))
      )
    )
  (:action move-right-from-r86
    :parameters ()
    :precondition 
      (in r86)
    :effect
      (and
        (in r87)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r86))
      )
    )
  (:action move-left-from-r86
    :parameters ()
    :precondition 
      (in r86)
    :effect
      (and
        (in r85)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r86))
      )
    )
  (:action move-right-from-r87
    :parameters ()
    :precondition 
      (in r87)
    :effect
      (and
        (in r88)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r87))
      )
    )
  (:action move-left-from-r87
    :parameters ()
    :precondition 
      (in r87)
    :effect
      (and
        (in r86)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r87))
      )
    )
  (:action move-right-from-r88
    :parameters ()
    :precondition 
      (in r88)
    :effect
      (and
        (in r89)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r88))
      )
    )
  (:action move-left-from-r88
    :parameters ()
    :precondition 
      (in r88)
    :effect
      (and
        (in r87)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r88))
      )
    )
  (:action move-right-from-r89
    :parameters ()
    :precondition 
      (in r89)
    :effect
      (and
        (in r90)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r89))
      )
    )
  (:action move-left-from-r89
    :parameters ()
    :precondition 
      (in r89)
    :effect
      (and
        (in r88)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r89))
      )
    )
  (:action move-right-from-r90
    :parameters ()
    :precondition 
      (in r90)
    :effect
      (and
        (in r91)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r90))
      )
    )
  (:action move-left-from-r90
    :parameters ()
    :precondition 
      (in r90)
    :effect
      (and
        (in r89)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r90))
      )
    )
  (:action move-right-from-r91
    :parameters ()
    :precondition 
      (in r91)
    :effect
      (and
        (in r92)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r91))
      )
    )
  (:action move-left-from-r91
    :parameters ()
    :precondition 
      (in r91)
    :effect
      (and
        (in r90)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r91))
      )
    )
  (:action move-right-from-r92
    :parameters ()
    :precondition 
      (in r92)
    :effect
      (and
        (in r93)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r92))
      )
    )
  (:action move-left-from-r92
    :parameters ()
    :precondition 
      (in r92)
    :effect
      (and
        (in r91)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r92))
      )
    )
  (:action move-right-from-r93
    :parameters ()
    :precondition 
      (in r93)
    :effect
      (and
        (in r94)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r93))
      )
    )
  (:action move-left-from-r93
    :parameters ()
    :precondition 
      (in r93)
    :effect
      (and
        (in r92)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r93))
      )
    )
  (:action move-right-from-r94
    :parameters ()
    :precondition 
      (in r94)
    :effect
      (and
        (in r95)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r94))
      )
    )
  (:action move-left-from-r94
    :parameters ()
    :precondition 
      (in r94)
    :effect
      (and
        (in r93)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r94))
      )
    )
  (:action move-right-from-r95
    :parameters ()
    :precondition 
      (in r95)
    :effect
      (and
        (in r96)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r95))
      )
    )
  (:action move-left-from-r95
    :parameters ()
    :precondition 
      (in r95)
    :effect
      (and
        (in r94)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r95))
      )
    )
  (:action move-right-from-r96
    :parameters ()
    :precondition 
      (in r96)
    :effect
      (and
        (in r97)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r96))
      )
    )
  (:action move-left-from-r96
    :parameters ()
    :precondition 
      (in r96)
    :effect
      (and
        (in r95)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r96))
      )
    )
  (:action move-right-from-r97
    :parameters ()
    :precondition 
      (in r97)
    :effect
      (and
        (in r98)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r97))
      )
    )
  (:action move-left-from-r97
    :parameters ()
    :precondition 
      (in r97)
    :effect
      (and
        (in r96)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r97))
      )
    )
  (:action move-right-from-r98
    :parameters ()
    :precondition 
      (in r98)
    :effect
      (and
        (in r99)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r98))
      )
    )
  (:action move-left-from-r98
    :parameters ()
    :precondition 
      (in r98)
    :effect
      (and
        (in r97)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r98))
      )
    )
  (:action move-right-from-r99
    :parameters ()
    :precondition 
      (in r99)
    :effect
      (and
        (in r100)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r99))
      )
    )
  (:action move-left-from-r99
    :parameters ()
    :precondition 
      (in r99)
    :effect
      (and
        (in r98)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r99))
      )
    )
  (:action move-right-from-r100
    :parameters ()
    :precondition 
      (in r100)
    :effect
      (and
        (in r101)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r100))
      )
    )
  (:action move-left-from-r100
    :parameters ()
    :precondition 
      (in r100)
    :effect
      (and
        (in r99)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r100))
      )
    )
  (:action move-right-from-r101
    :parameters ()
    :precondition 
      (in r101)
    :effect
      (and
        (in r102)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r101))
      )
    )
  (:action move-left-from-r101
    :parameters ()
    :precondition 
      (in r101)
    :effect
      (and
        (in r100)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r101))
      )
    )
  (:action move-right-from-r102
    :parameters ()
    :precondition 
      (in r102)
    :effect
      (and
        (in r103)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r102))
      )
    )
  (:action move-left-from-r102
    :parameters ()
    :precondition 
      (in r102)
    :effect
      (and
        (in r101)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r102))
      )
    )
  (:action move-right-from-r103
    :parameters ()
    :precondition 
      (in r103)
    :effect
      (and
        (in r104)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r103))
      )
    )
  (:action move-left-from-r103
    :parameters ()
    :precondition 
      (in r103)
    :effect
      (and
        (in r102)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r103))
      )
    )
  (:action move-right-from-r104
    :parameters ()
    :precondition 
      (in r104)
    :effect
      (and
        (in r105)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r104))
      )
    )
  (:action move-left-from-r104
    :parameters ()
    :precondition 
      (in r104)
    :effect
      (and
        (in r103)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r104))
      )
    )
  (:action move-right-from-r105
    :parameters ()
    :precondition 
      (in r105)
    :effect
      (and
        (in r106)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r105))
      )
    )
  (:action move-left-from-r105
    :parameters ()
    :precondition 
      (in r105)
    :effect
      (and
        (in r104)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r105))
      )
    )
  (:action move-right-from-r106
    :parameters ()
    :precondition 
      (in r106)
    :effect
      (and
        (in r107)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r106))
      )
    )
  (:action move-left-from-r106
    :parameters ()
    :precondition 
      (in r106)
    :effect
      (and
        (in r105)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r106))
      )
    )
  (:action move-right-from-r107
    :parameters ()
    :precondition 
      (in r107)
    :effect
      (and
        (in r108)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r107))
      )
    )
  (:action move-left-from-r107
    :parameters ()
    :precondition 
      (in r107)
    :effect
      (and
        (in r106)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r107))
      )
    )
  (:action move-right-from-r108
    :parameters ()
    :precondition 
      (in r108)
    :effect
      (and
        (in r109)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r108))
      )
    )
  (:action move-left-from-r108
    :parameters ()
    :precondition 
      (in r108)
    :effect
      (and
        (in r107)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r108))
      )
    )
  (:action move-right-from-r109
    :parameters ()
    :precondition 
      (in r109)
    :effect
      (and
        (in r110)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r109))
      )
    )
  (:action move-left-from-r109
    :parameters ()
    :precondition 
      (in r109)
    :effect
      (and
        (in r108)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r109))
      )
    )
  (:action move-right-from-r110
    :parameters ()
    :precondition 
      (in r110)
    :effect
      (and
        (in r111)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r110))
      )
    )
  (:action move-left-from-r110
    :parameters ()
    :precondition 
      (in r110)
    :effect
      (and
        (in r109)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r110))
      )
    )
  (:action move-right-from-r111
    :parameters ()
    :precondition 
      (in r111)
    :effect
      (and
        (in r112)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r111))
      )
    )
  (:action move-left-from-r111
    :parameters ()
    :precondition 
      (in r111)
    :effect
      (and
        (in r110)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r111))
      )
    )
  (:action move-right-from-r112
    :parameters ()
    :precondition 
      (in r112)
    :effect
      (and
        (in r113)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r112))
      )
    )
  (:action move-left-from-r112
    :parameters ()
    :precondition 
      (in r112)
    :effect
      (and
        (in r111)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r112))
      )
    )
  (:action move-right-from-r113
    :parameters ()
    :precondition 
      (in r113)
    :effect
      (and
        (in r114)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r113))
      )
    )
  (:action move-left-from-r113
    :parameters ()
    :precondition 
      (in r113)
    :effect
      (and
        (in r112)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r113))
      )
    )
  (:action move-right-from-r114
    :parameters ()
    :precondition 
      (in r114)
    :effect
      (and
        (in r115)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r114))
      )
    )
  (:action move-left-from-r114
    :parameters ()
    :precondition 
      (in r114)
    :effect
      (and
        (in r113)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r114))
      )
    )
  (:action move-right-from-r115
    :parameters ()
    :precondition 
      (in r115)
    :effect
      (and
        (in r116)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r115))
      )
    )
  (:action move-left-from-r115
    :parameters ()
    :precondition 
      (in r115)
    :effect
      (and
        (in r114)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r115))
      )
    )
  (:action move-right-from-r116
    :parameters ()
    :precondition 
      (in r116)
    :effect
      (and
        (in r117)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r116))
      )
    )
  (:action move-left-from-r116
    :parameters ()
    :precondition 
      (in r116)
    :effect
      (and
        (in r115)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r116))
      )
    )
  (:action move-right-from-r117
    :parameters ()
    :precondition 
      (in r117)
    :effect
      (and
        (in r118)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r117))
      )
    )
  (:action move-left-from-r117
    :parameters ()
    :precondition 
      (in r117)
    :effect
      (and
        (in r116)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r117))
      )
    )
  (:action move-right-from-r118
    :parameters ()
    :precondition 
      (in r118)
    :effect
      (and
        (in r119)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r118))
      )
    )
  (:action move-left-from-r118
    :parameters ()
    :precondition 
      (in r118)
    :effect
      (and
        (in r117)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r118))
      )
    )
  (:action move-right-from-r119
    :parameters ()
    :precondition 
      (in r119)
    :effect
      (and
        (in r120)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r119))
      )
    )
  (:action move-left-from-r119
    :parameters ()
    :precondition 
      (in r119)
    :effect
      (and
        (in r118)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r119))
      )
    )
  (:action move-right-from-r120
    :parameters ()
    :precondition 
      (in r120)
    :effect
      (and
        (in r121)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r120))
      )
    )
  (:action move-left-from-r120
    :parameters ()
    :precondition 
      (in r120)
    :effect
      (and
        (in r119)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r120))
      )
    )
  (:action move-right-from-r121
    :parameters ()
    :precondition 
      (in r121)
    :effect
      (and
        (in r122)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r121))
      )
    )
  (:action move-left-from-r121
    :parameters ()
    :precondition 
      (in r121)
    :effect
      (and
        (in r120)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r121))
      )
    )
  (:action move-right-from-r122
    :parameters ()
    :precondition 
      (in r122)
    :effect
      (and
        (in r123)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r122))
      )
    )
  (:action move-left-from-r122
    :parameters ()
    :precondition 
      (in r122)
    :effect
      (and
        (in r121)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r122))
      )
    )
  (:action move-right-from-r123
    :parameters ()
    :precondition 
      (in r123)
    :effect
      (and
        (in r124)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r123))
      )
    )
  (:action move-left-from-r123
    :parameters ()
    :precondition 
      (in r123)
    :effect
      (and
        (in r122)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r123))
      )
    )
  (:action move-right-from-r124
    :parameters ()
    :precondition 
      (in r124)
    :effect
      (and
        (in r125)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r124))
      )
    )
  (:action move-left-from-r124
    :parameters ()
    :precondition 
      (in r124)
    :effect
      (and
        (in r123)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r124))
      )
    )
  (:action move-right-from-r125
    :parameters ()
    :precondition 
      (in r125)
    :effect
      (and
        (in r126)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r125))
      )
    )
  (:action move-left-from-r125
    :parameters ()
    :precondition 
      (in r125)
    :effect
      (and
        (in r124)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r125))
      )
    )
  (:action move-right-from-r126
    :parameters ()
    :precondition 
      (in r126)
    :effect
      (and
        (in r127)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r126))
      )
    )
  (:action move-left-from-r126
    :parameters ()
    :precondition 
      (in r126)
    :effect
      (and
        (in r125)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r126))
      )
    )
  (:action move-right-from-r127
    :parameters ()
    :precondition 
      (in r127)
    :effect
      (and
        (in r128)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r127))
      )
    )
  (:action move-left-from-r127
    :parameters ()
    :precondition 
      (in r127)
    :effect
      (and
        (in r126)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r127))
      )
    )
  (:action move-right-from-r128
    :parameters ()
    :precondition 
      (in r128)
    :effect
      (and
        (in r129)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r128))
      )
    )
  (:action move-left-from-r128
    :parameters ()
    :precondition 
      (in r128)
    :effect
      (and
        (in r127)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r128))
      )
    )
  (:action move-right-from-r129
    :parameters ()
    :precondition 
      (in r129)
    :effect
      (and
        (in r130)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r129))
      )
    )
  (:action move-left-from-r129
    :parameters ()
    :precondition 
      (in r129)
    :effect
      (and
        (in r128)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r129))
      )
    )
  (:action move-right-from-r130
    :parameters ()
    :precondition 
      (in r130)
    :effect
      (and
        (in r131)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r130))
      )
    )
  (:action move-left-from-r130
    :parameters ()
    :precondition 
      (in r130)
    :effect
      (and
        (in r129)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r130))
      )
    )
  (:action move-right-from-r131
    :parameters ()
    :precondition 
      (in r131)
    :effect
      (and
        (in r132)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r131))
      )
    )
  (:action move-left-from-r131
    :parameters ()
    :precondition 
      (in r131)
    :effect
      (and
        (in r130)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r131))
      )
    )
  (:action move-right-from-r132
    :parameters ()
    :precondition 
      (in r132)
    :effect
      (and
        (in r133)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r132))
      )
    )
  (:action move-left-from-r132
    :parameters ()
    :precondition 
      (in r132)
    :effect
      (and
        (in r131)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r132))
      )
    )
  (:action move-right-from-r133
    :parameters ()
    :precondition 
      (in r133)
    :effect
      (and
        (in r134)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r133))
      )
    )
  (:action move-left-from-r133
    :parameters ()
    :precondition 
      (in r133)
    :effect
      (and
        (in r132)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r133))
      )
    )
  (:action move-right-from-r134
    :parameters ()
    :precondition 
      (in r134)
    :effect
      (and
        (in r135)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r134))
      )
    )
  (:action move-left-from-r134
    :parameters ()
    :precondition 
      (in r134)
    :effect
      (and
        (in r133)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r134))
      )
    )
  (:action move-right-from-r135
    :parameters ()
    :precondition 
      (in r135)
    :effect
      (and
        (in r136)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r135))
      )
    )
  (:action move-left-from-r135
    :parameters ()
    :precondition 
      (in r135)
    :effect
      (and
        (in r134)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r135))
      )
    )
  (:action move-right-from-r136
    :parameters ()
    :precondition 
      (in r136)
    :effect
      (and
        (in r137)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r136))
      )
    )
  (:action move-left-from-r136
    :parameters ()
    :precondition 
      (in r136)
    :effect
      (and
        (in r135)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r136))
      )
    )
  (:action move-right-from-r137
    :parameters ()
    :precondition 
      (in r137)
    :effect
      (and
        (in r138)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r137))
      )
    )
  (:action move-left-from-r137
    :parameters ()
    :precondition 
      (in r137)
    :effect
      (and
        (in r136)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r137))
      )
    )
  (:action move-right-from-r138
    :parameters ()
    :precondition 
      (in r138)
    :effect
      (and
        (in r139)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r138))
      )
    )
  (:action move-left-from-r138
    :parameters ()
    :precondition 
      (in r138)
    :effect
      (and
        (in r137)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r138))
      )
    )
  (:action move-right-from-r139
    :parameters ()
    :precondition 
      (in r139)
    :effect
      (and
        (in r140)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r139))
      )
    )
  (:action move-left-from-r139
    :parameters ()
    :precondition 
      (in r139)
    :effect
      (and
        (in r138)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r139))
      )
    )
  (:action move-right-from-r140
    :parameters ()
    :precondition 
      (in r140)
    :effect
      (and
        (in r141)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r140))
      )
    )
  (:action move-left-from-r140
    :parameters ()
    :precondition 
      (in r140)
    :effect
      (and
        (in r139)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r140))
      )
    )
  (:action move-right-from-r141
    :parameters ()
    :precondition 
      (in r141)
    :effect
      (and
        (in r142)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r141))
      )
    )
  (:action move-left-from-r141
    :parameters ()
    :precondition 
      (in r141)
    :effect
      (and
        (in r140)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r141))
      )
    )
  (:action move-right-from-r142
    :parameters ()
    :precondition 
      (in r142)
    :effect
      (and
        (in r143)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r142))
      )
    )
  (:action move-left-from-r142
    :parameters ()
    :precondition 
      (in r142)
    :effect
      (and
        (in r141)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r142))
      )
    )
  (:action move-right-from-r143
    :parameters ()
    :precondition 
      (in r143)
    :effect
      (and
        (in r144)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r143))
      )
    )
  (:action move-left-from-r143
    :parameters ()
    :precondition 
      (in r143)
    :effect
      (and
        (in r142)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r143))
      )
    )
  (:action move-right-from-r144
    :parameters ()
    :precondition 
      (in r144)
    :effect
      (and
        (in r145)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r144))
      )
    )
  (:action move-left-from-r144
    :parameters ()
    :precondition 
      (in r144)
    :effect
      (and
        (in r143)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r144))
      )
    )
  (:action move-right-from-r145
    :parameters ()
    :precondition 
      (in r145)
    :effect
      (and
        (in r146)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r145))
      )
    )
  (:action move-left-from-r145
    :parameters ()
    :precondition 
      (in r145)
    :effect
      (and
        (in r144)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r145))
      )
    )
  (:action move-right-from-r146
    :parameters ()
    :precondition 
      (in r146)
    :effect
      (and
        (in r147)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r146))
      )
    )
  (:action move-left-from-r146
    :parameters ()
    :precondition 
      (in r146)
    :effect
      (and
        (in r145)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r146))
      )
    )
  (:action move-right-from-r147
    :parameters ()
    :precondition 
      (in r147)
    :effect
      (and
        (in r148)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r147))
      )
    )
  (:action move-left-from-r147
    :parameters ()
    :precondition 
      (in r147)
    :effect
      (and
        (in r146)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r147))
      )
    )
  (:action move-right-from-r148
    :parameters ()
    :precondition 
      (in r148)
    :effect
      (and
        (in r149)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r148))
      )
    )
  (:action move-left-from-r148
    :parameters ()
    :precondition 
      (in r148)
    :effect
      (and
        (in r147)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r148))
      )
    )
  (:action move-right-from-r149
    :parameters ()
    :precondition 
      (in r149)
    :effect
      (and
        (in r150)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r149))
      )
    )
  (:action move-left-from-r149
    :parameters ()
    :precondition 
      (in r149)
    :effect
      (and
        (in r148)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r149))
      )
    )
  (:action move-right-from-r150
    :parameters ()
    :precondition 
      (in r150)
    :effect
      (and
        (in r151)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r150))
      )
    )
  (:action move-left-from-r150
    :parameters ()
    :precondition 
      (in r150)
    :effect
      (and
        (in r149)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r150))
      )
    )
  (:action move-right-from-r151
    :parameters ()
    :precondition 
      (in r151)
    :effect
      (and
        (in r152)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r151))
      )
    )
  (:action move-left-from-r151
    :parameters ()
    :precondition 
      (in r151)
    :effect
      (and
        (in r150)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r151))
      )
    )
  (:action move-right-from-r152
    :parameters ()
    :precondition 
      (in r152)
    :effect
      (and
        (in r153)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r152))
      )
    )
  (:action move-left-from-r152
    :parameters ()
    :precondition 
      (in r152)
    :effect
      (and
        (in r151)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r152))
      )
    )
  (:action move-right-from-r153
    :parameters ()
    :precondition 
      (in r153)
    :effect
      (and
        (in r154)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r153))
      )
    )
  (:action move-left-from-r153
    :parameters ()
    :precondition 
      (in r153)
    :effect
      (and
        (in r152)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r153))
      )
    )
  (:action move-right-from-r154
    :parameters ()
    :precondition 
      (in r154)
    :effect
      (and
        (in r155)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r154))
      )
    )
  (:action move-left-from-r154
    :parameters ()
    :precondition 
      (in r154)
    :effect
      (and
        (in r153)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r154))
      )
    )
  (:action move-right-from-r155
    :parameters ()
    :precondition 
      (in r155)
    :effect
      (and
        (in r156)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r155))
      )
    )
  (:action move-left-from-r155
    :parameters ()
    :precondition 
      (in r155)
    :effect
      (and
        (in r154)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r155))
      )
    )
  (:action move-right-from-r156
    :parameters ()
    :precondition 
      (in r156)
    :effect
      (and
        (in r157)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r156))
      )
    )
  (:action move-left-from-r156
    :parameters ()
    :precondition 
      (in r156)
    :effect
      (and
        (in r155)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r156))
      )
    )
  (:action move-right-from-r157
    :parameters ()
    :precondition 
      (in r157)
    :effect
      (and
        (in r158)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r157))
      )
    )
  (:action move-left-from-r157
    :parameters ()
    :precondition 
      (in r157)
    :effect
      (and
        (in r156)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r157))
      )
    )
  (:action move-right-from-r158
    :parameters ()
    :precondition 
      (in r158)
    :effect
      (and
        (in r159)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r158))
      )
    )
  (:action move-left-from-r158
    :parameters ()
    :precondition 
      (in r158)
    :effect
      (and
        (in r157)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r158))
      )
    )
  (:action move-right-from-r159
    :parameters ()
    :precondition 
      (in r159)
    :effect
      (and
        (in r160)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r159))
      )
    )
  (:action move-left-from-r159
    :parameters ()
    :precondition 
      (in r159)
    :effect
      (and
        (in r158)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r159))
      )
    )
  (:action move-right-from-r160
    :parameters ()
    :precondition 
      (in r160)
    :effect
      (and
        (in r161)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r160))
      )
    )
  (:action move-left-from-r160
    :parameters ()
    :precondition 
      (in r160)
    :effect
      (and
        (in r159)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r160))
      )
    )
  (:action move-right-from-r161
    :parameters ()
    :precondition 
      (in r161)
    :effect
      (and
        (in r162)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r161))
      )
    )
  (:action move-left-from-r161
    :parameters ()
    :precondition 
      (in r161)
    :effect
      (and
        (in r160)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r161))
      )
    )
  (:action move-right-from-r162
    :parameters ()
    :precondition 
      (in r162)
    :effect
      (and
        (in r163)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r162))
      )
    )
  (:action move-left-from-r162
    :parameters ()
    :precondition 
      (in r162)
    :effect
      (and
        (in r161)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r162))
      )
    )
  (:action move-right-from-r163
    :parameters ()
    :precondition 
      (in r163)
    :effect
      (and
        (in r164)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r163))
      )
    )
  (:action move-left-from-r163
    :parameters ()
    :precondition 
      (in r163)
    :effect
      (and
        (in r162)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r163))
      )
    )
  (:action move-right-from-r164
    :parameters ()
    :precondition 
      (in r164)
    :effect
      (and
        (in r165)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r164))
      )
    )
  (:action move-left-from-r164
    :parameters ()
    :precondition 
      (in r164)
    :effect
      (and
        (in r163)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r164))
      )
    )
  (:action move-right-from-r165
    :parameters ()
    :precondition 
      (in r165)
    :effect
      (and
        (in r166)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r165))
      )
    )
  (:action move-left-from-r165
    :parameters ()
    :precondition 
      (in r165)
    :effect
      (and
        (in r164)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r165))
      )
    )
  (:action move-right-from-r166
    :parameters ()
    :precondition 
      (in r166)
    :effect
      (and
        (in r167)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r166))
      )
    )
  (:action move-left-from-r166
    :parameters ()
    :precondition 
      (in r166)
    :effect
      (and
        (in r165)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r166))
      )
    )
  (:action move-right-from-r167
    :parameters ()
    :precondition 
      (in r167)
    :effect
      (and
        (in r168)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r167))
      )
    )
  (:action move-left-from-r167
    :parameters ()
    :precondition 
      (in r167)
    :effect
      (and
        (in r166)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r167))
      )
    )
  (:action move-right-from-r168
    :parameters ()
    :precondition 
      (in r168)
    :effect
      (and
        (in r169)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r168))
      )
    )
  (:action move-left-from-r168
    :parameters ()
    :precondition 
      (in r168)
    :effect
      (and
        (in r167)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r168))
      )
    )
  (:action move-right-from-r169
    :parameters ()
    :precondition 
      (in r169)
    :effect
      (and
        (in r170)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r169))
      )
    )
  (:action move-left-from-r169
    :parameters ()
    :precondition 
      (in r169)
    :effect
      (and
        (in r168)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r169))
      )
    )
  (:action move-right-from-r170
    :parameters ()
    :precondition 
      (in r170)
    :effect
      (and
        (in r171)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r170))
      )
    )
  (:action move-left-from-r170
    :parameters ()
    :precondition 
      (in r170)
    :effect
      (and
        (in r169)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r170))
      )
    )
  (:action move-right-from-r171
    :parameters ()
    :precondition 
      (in r171)
    :effect
      (and
        (in r172)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r171))
      )
    )
  (:action move-left-from-r171
    :parameters ()
    :precondition 
      (in r171)
    :effect
      (and
        (in r170)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r171))
      )
    )
  (:action move-right-from-r172
    :parameters ()
    :precondition 
      (in r172)
    :effect
      (and
        (in r173)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r172))
      )
    )
  (:action move-left-from-r172
    :parameters ()
    :precondition 
      (in r172)
    :effect
      (and
        (in r171)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r172))
      )
    )
  (:action move-right-from-r173
    :parameters ()
    :precondition 
      (in r173)
    :effect
      (and
        (in r174)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r173))
      )
    )
  (:action move-left-from-r173
    :parameters ()
    :precondition 
      (in r173)
    :effect
      (and
        (in r172)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r173))
      )
    )
  (:action move-right-from-r174
    :parameters ()
    :precondition 
      (in r174)
    :effect
      (and
        (in r175)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r174))
      )
    )
  (:action move-left-from-r174
    :parameters ()
    :precondition 
      (in r174)
    :effect
      (and
        (in r173)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r174))
      )
    )
  (:action move-right-from-r175
    :parameters ()
    :precondition 
      (in r175)
    :effect
      (and
        (in r176)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r175))
      )
    )
  (:action move-left-from-r175
    :parameters ()
    :precondition 
      (in r175)
    :effect
      (and
        (in r174)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r175))
      )
    )
  (:action move-right-from-r176
    :parameters ()
    :precondition 
      (in r176)
    :effect
      (and
        (in r177)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r176))
      )
    )
  (:action move-left-from-r176
    :parameters ()
    :precondition 
      (in r176)
    :effect
      (and
        (in r175)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r176))
      )
    )
  (:action move-right-from-r177
    :parameters ()
    :precondition 
      (in r177)
    :effect
      (and
        (in r178)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r177))
      )
    )
  (:action move-left-from-r177
    :parameters ()
    :precondition 
      (in r177)
    :effect
      (and
        (in r176)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r177))
      )
    )
  (:action move-right-from-r178
    :parameters ()
    :precondition 
      (in r178)
    :effect
      (and
        (in r179)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r178))
      )
    )
  (:action move-left-from-r178
    :parameters ()
    :precondition 
      (in r178)
    :effect
      (and
        (in r177)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r178))
      )
    )
  (:action move-right-from-r179
    :parameters ()
    :precondition 
      (in r179)
    :effect
      (and
        (in r180)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r179))
      )
    )
  (:action move-left-from-r179
    :parameters ()
    :precondition 
      (in r179)
    :effect
      (and
        (in r178)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r179))
      )
    )
  (:action move-right-from-r180
    :parameters ()
    :precondition 
      (in r180)
    :effect
      (and
        (in r181)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r180))
      )
    )
  (:action move-left-from-r180
    :parameters ()
    :precondition 
      (in r180)
    :effect
      (and
        (in r179)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r180))
      )
    )
  (:action move-right-from-r181
    :parameters ()
    :precondition 
      (in r181)
    :effect
      (and
        (in r182)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r181))
      )
    )
  (:action move-left-from-r181
    :parameters ()
    :precondition 
      (in r181)
    :effect
      (and
        (in r180)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r181))
      )
    )
  (:action move-right-from-r182
    :parameters ()
    :precondition 
      (in r182)
    :effect
      (and
        (in r183)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r182))
      )
    )
  (:action move-left-from-r182
    :parameters ()
    :precondition 
      (in r182)
    :effect
      (and
        (in r181)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r182))
      )
    )
  (:action move-right-from-r183
    :parameters ()
    :precondition 
      (in r183)
    :effect
      (and
        (in r184)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r183))
      )
    )
  (:action move-left-from-r183
    :parameters ()
    :precondition 
      (in r183)
    :effect
      (and
        (in r182)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r183))
      )
    )
  (:action move-right-from-r184
    :parameters ()
    :precondition 
      (in r184)
    :effect
      (and
        (in r185)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r184))
      )
    )
  (:action move-left-from-r184
    :parameters ()
    :precondition 
      (in r184)
    :effect
      (and
        (in r183)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r184))
      )
    )
  (:action move-right-from-r185
    :parameters ()
    :precondition 
      (in r185)
    :effect
      (and
        (in r186)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r185))
      )
    )
  (:action move-left-from-r185
    :parameters ()
    :precondition 
      (in r185)
    :effect
      (and
        (in r184)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r185))
      )
    )
  (:action move-right-from-r186
    :parameters ()
    :precondition 
      (in r186)
    :effect
      (and
        (in r187)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r186))
      )
    )
  (:action move-left-from-r186
    :parameters ()
    :precondition 
      (in r186)
    :effect
      (and
        (in r185)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r186))
      )
    )
  (:action move-right-from-r187
    :parameters ()
    :precondition 
      (in r187)
    :effect
      (and
        (in r188)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r187))
      )
    )
  (:action move-left-from-r187
    :parameters ()
    :precondition 
      (in r187)
    :effect
      (and
        (in r186)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r187))
      )
    )
  (:action move-right-from-r188
    :parameters ()
    :precondition 
      (in r188)
    :effect
      (and
        (in r189)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r188))
      )
    )
  (:action move-left-from-r188
    :parameters ()
    :precondition 
      (in r188)
    :effect
      (and
        (in r187)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r188))
      )
    )
  (:action move-right-from-r189
    :parameters ()
    :precondition 
      (in r189)
    :effect
      (and
        (in r190)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r189))
      )
    )
  (:action move-left-from-r189
    :parameters ()
    :precondition 
      (in r189)
    :effect
      (and
        (in r188)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r189))
      )
    )
  (:action move-right-from-r190
    :parameters ()
    :precondition 
      (in r190)
    :effect
      (and
        (in r191)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r190))
      )
    )
  (:action move-left-from-r190
    :parameters ()
    :precondition 
      (in r190)
    :effect
      (and
        (in r189)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r190))
      )
    )
  (:action move-right-from-r191
    :parameters ()
    :precondition 
      (in r191)
    :effect
      (and
        (in r192)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r191))
      )
    )
  (:action move-left-from-r191
    :parameters ()
    :precondition 
      (in r191)
    :effect
      (and
        (in r190)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r191))
      )
    )
  (:action move-right-from-r192
    :parameters ()
    :precondition 
      (in r192)
    :effect
      (and
        (in r193)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r192))
      )
    )
  (:action move-left-from-r192
    :parameters ()
    :precondition 
      (in r192)
    :effect
      (and
        (in r191)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r192))
      )
    )
  (:action move-right-from-r193
    :parameters ()
    :precondition 
      (in r193)
    :effect
      (and
        (in r194)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r193))
      )
    )
  (:action move-left-from-r193
    :parameters ()
    :precondition 
      (in r193)
    :effect
      (and
        (in r192)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r193))
      )
    )
  (:action move-right-from-r194
    :parameters ()
    :precondition 
      (in r194)
    :effect
      (and
        (in r195)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r194))
      )
    )
  (:action move-left-from-r194
    :parameters ()
    :precondition 
      (in r194)
    :effect
      (and
        (in r193)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r194))
      )
    )
  (:action move-right-from-r195
    :parameters ()
    :precondition 
      (in r195)
    :effect
      (and
        (in r196)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r195))
      )
    )
  (:action move-left-from-r195
    :parameters ()
    :precondition 
      (in r195)
    :effect
      (and
        (in r194)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r195))
      )
    )
  (:action move-right-from-r196
    :parameters ()
    :precondition 
      (in r196)
    :effect
      (and
        (in r197)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r196))
      )
    )
  (:action move-left-from-r196
    :parameters ()
    :precondition 
      (in r196)
    :effect
      (and
        (in r195)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r196))
      )
    )
  (:action move-right-from-r197
    :parameters ()
    :precondition 
      (in r197)
    :effect
      (and
        (in r198)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r197))
      )
    )
  (:action move-left-from-r197
    :parameters ()
    :precondition 
      (in r197)
    :effect
      (and
        (in r196)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r197))
      )
    )
  (:action move-right-from-r198
    :parameters ()
    :precondition 
      (in r198)
    :effect
      (and
        (in r199)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r198))
      )
    )
  (:action move-left-from-r198
    :parameters ()
    :precondition 
      (in r198)
    :effect
      (and
        (in r197)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r198))
      )
    )
  (:action move-right-from-r199
    :parameters ()
    :precondition 
      (in r199)
    :effect
      (and
        (in r200)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r199))
      )
    )
  (:action move-left-from-r199
    :parameters ()
    :precondition 
      (in r199)
    :effect
      (and
        (in r198)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r199))
      )
    )
  (:action move-right-from-r200
    :parameters ()
    :precondition 
      (in r200)
    :effect
      (and
        (in r201)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r200))
      )
    )
  (:action move-left-from-r200
    :parameters ()
    :precondition 
      (in r200)
    :effect
      (and
        (in r199)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r200))
      )
    )
  (:action move-right-from-r201
    :parameters ()
    :precondition 
      (in r201)
    :effect
      (and
        (in r202)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r201))
      )
    )
  (:action move-left-from-r201
    :parameters ()
    :precondition 
      (in r201)
    :effect
      (and
        (in r200)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r201))
      )
    )
  (:action move-right-from-r202
    :parameters ()
    :precondition 
      (in r202)
    :effect
      (and
        (in r203)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r202))
      )
    )
  (:action move-left-from-r202
    :parameters ()
    :precondition 
      (in r202)
    :effect
      (and
        (in r201)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r202))
      )
    )
  (:action move-right-from-r203
    :parameters ()
    :precondition 
      (in r203)
    :effect
      (and
        (in r204)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r203))
      )
    )
  (:action move-left-from-r203
    :parameters ()
    :precondition 
      (in r203)
    :effect
      (and
        (in r202)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r203))
      )
    )
  (:action move-right-from-r204
    :parameters ()
    :precondition 
      (in r204)
    :effect
      (and
        (in r205)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r204))
      )
    )
  (:action move-left-from-r204
    :parameters ()
    :precondition 
      (in r204)
    :effect
      (and
        (in r203)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r204))
      )
    )
  (:action move-right-from-r205
    :parameters ()
    :precondition 
      (in r205)
    :effect
      (and
        (in r206)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r205))
      )
    )
  (:action move-left-from-r205
    :parameters ()
    :precondition 
      (in r205)
    :effect
      (and
        (in r204)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r205))
      )
    )
  (:action move-right-from-r206
    :parameters ()
    :precondition 
      (in r206)
    :effect
      (and
        (in r207)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r206))
      )
    )
  (:action move-left-from-r206
    :parameters ()
    :precondition 
      (in r206)
    :effect
      (and
        (in r205)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r206))
      )
    )
  (:action move-right-from-r207
    :parameters ()
    :precondition 
      (in r207)
    :effect
      (and
        (in r208)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r207))
      )
    )
  (:action move-left-from-r207
    :parameters ()
    :precondition 
      (in r207)
    :effect
      (and
        (in r206)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r207))
      )
    )
  (:action move-right-from-r208
    :parameters ()
    :precondition 
      (in r208)
    :effect
      (and
        (in r209)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r208))
      )
    )
  (:action move-left-from-r208
    :parameters ()
    :precondition 
      (in r208)
    :effect
      (and
        (in r207)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r208))
      )
    )
  (:action move-right-from-r209
    :parameters ()
    :precondition 
      (in r209)
    :effect
      (and
        (in r210)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r209))
      )
    )
  (:action move-left-from-r209
    :parameters ()
    :precondition 
      (in r209)
    :effect
      (and
        (in r208)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r209))
      )
    )
  (:action move-right-from-r210
    :parameters ()
    :precondition 
      (in r210)
    :effect
      (and
        (in r211)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r210))
      )
    )
  (:action move-left-from-r210
    :parameters ()
    :precondition 
      (in r210)
    :effect
      (and
        (in r209)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r210))
      )
    )
  (:action move-right-from-r211
    :parameters ()
    :precondition 
      (in r211)
    :effect
      (and
        (in r212)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r211))
      )
    )
  (:action move-left-from-r211
    :parameters ()
    :precondition 
      (in r211)
    :effect
      (and
        (in r210)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r211))
      )
    )
  (:action move-right-from-r212
    :parameters ()
    :precondition 
      (in r212)
    :effect
      (and
        (in r213)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r212))
      )
    )
  (:action move-left-from-r212
    :parameters ()
    :precondition 
      (in r212)
    :effect
      (and
        (in r211)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r212))
      )
    )
  (:action move-right-from-r213
    :parameters ()
    :precondition 
      (in r213)
    :effect
      (and
        (in r214)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r213))
      )
    )
  (:action move-left-from-r213
    :parameters ()
    :precondition 
      (in r213)
    :effect
      (and
        (in r212)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r213))
      )
    )
  (:action move-right-from-r214
    :parameters ()
    :precondition 
      (in r214)
    :effect
      (and
        (in r215)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r214))
      )
    )
  (:action move-left-from-r214
    :parameters ()
    :precondition 
      (in r214)
    :effect
      (and
        (in r213)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r214))
      )
    )
  (:action move-right-from-r215
    :parameters ()
    :precondition 
      (in r215)
    :effect
      (and
        (in r216)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r215))
      )
    )
  (:action move-left-from-r215
    :parameters ()
    :precondition 
      (in r215)
    :effect
      (and
        (in r214)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r215))
      )
    )
  (:action move-right-from-r216
    :parameters ()
    :precondition 
      (in r216)
    :effect
      (and
        (in r217)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r216))
      )
    )
  (:action move-left-from-r216
    :parameters ()
    :precondition 
      (in r216)
    :effect
      (and
        (in r215)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r216))
      )
    )
  (:action move-right-from-r217
    :parameters ()
    :precondition 
      (in r217)
    :effect
      (and
        (in r218)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r217))
      )
    )
  (:action move-left-from-r217
    :parameters ()
    :precondition 
      (in r217)
    :effect
      (and
        (in r216)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r217))
      )
    )
  (:action move-right-from-r218
    :parameters ()
    :precondition 
      (in r218)
    :effect
      (and
        (in r219)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r218))
      )
    )
  (:action move-left-from-r218
    :parameters ()
    :precondition 
      (in r218)
    :effect
      (and
        (in r217)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r218))
      )
    )
  (:action move-right-from-r219
    :parameters ()
    :precondition 
      (in r219)
    :effect
      (and
        (in r220)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r219))
      )
    )
  (:action move-left-from-r219
    :parameters ()
    :precondition 
      (in r219)
    :effect
      (and
        (in r218)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r219))
      )
    )
  (:action move-right-from-r220
    :parameters ()
    :precondition 
      (in r220)
    :effect
      (and
        (in r221)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r220))
      )
    )
  (:action move-left-from-r220
    :parameters ()
    :precondition 
      (in r220)
    :effect
      (and
        (in r219)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r220))
      )
    )
  (:action move-right-from-r221
    :parameters ()
    :precondition 
      (in r221)
    :effect
      (and
        (in r222)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r221))
      )
    )
  (:action move-left-from-r221
    :parameters ()
    :precondition 
      (in r221)
    :effect
      (and
        (in r220)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r221))
      )
    )
  (:action move-right-from-r222
    :parameters ()
    :precondition 
      (in r222)
    :effect
      (and
        (in r223)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r222))
      )
    )
  (:action move-left-from-r222
    :parameters ()
    :precondition 
      (in r222)
    :effect
      (and
        (in r221)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r222))
      )
    )
  (:action move-right-from-r223
    :parameters ()
    :precondition 
      (in r223)
    :effect
      (and
        (in r224)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r223))
      )
    )
  (:action move-left-from-r223
    :parameters ()
    :precondition 
      (in r223)
    :effect
      (and
        (in r222)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r223))
      )
    )
  (:action move-right-from-r224
    :parameters ()
    :precondition 
      (in r224)
    :effect
      (and
        (in r225)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r224))
      )
    )
  (:action move-left-from-r224
    :parameters ()
    :precondition 
      (in r224)
    :effect
      (and
        (in r223)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r224))
      )
    )
  (:action move-right-from-r225
    :parameters ()
    :precondition 
      (in r225)
    :effect
      (and
        (in r226)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r225))
      )
    )
  (:action move-left-from-r225
    :parameters ()
    :precondition 
      (in r225)
    :effect
      (and
        (in r224)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r225))
      )
    )
  (:action move-right-from-r226
    :parameters ()
    :precondition 
      (in r226)
    :effect
      (and
        (in r227)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r226))
      )
    )
  (:action move-left-from-r226
    :parameters ()
    :precondition 
      (in r226)
    :effect
      (and
        (in r225)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r226))
      )
    )
  (:action move-right-from-r227
    :parameters ()
    :precondition 
      (in r227)
    :effect
      (and
        (in r228)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r227))
      )
    )
  (:action move-left-from-r227
    :parameters ()
    :precondition 
      (in r227)
    :effect
      (and
        (in r226)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r227))
      )
    )
  (:action move-right-from-r228
    :parameters ()
    :precondition 
      (in r228)
    :effect
      (and
        (in r229)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r228))
      )
    )
  (:action move-left-from-r228
    :parameters ()
    :precondition 
      (in r228)
    :effect
      (and
        (in r227)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r228))
      )
    )
  (:action move-right-from-r229
    :parameters ()
    :precondition 
      (in r229)
    :effect
      (and
        (in r230)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r229))
      )
    )
  (:action move-left-from-r229
    :parameters ()
    :precondition 
      (in r229)
    :effect
      (and
        (in r228)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r229))
      )
    )
  (:action move-right-from-r230
    :parameters ()
    :precondition 
      (in r230)
    :effect
      (and
        (in r231)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r230))
      )
    )
  (:action move-left-from-r230
    :parameters ()
    :precondition 
      (in r230)
    :effect
      (and
        (in r229)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r230))
      )
    )
  (:action move-right-from-r231
    :parameters ()
    :precondition 
      (in r231)
    :effect
      (and
        (in r232)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r231))
      )
    )
  (:action move-left-from-r231
    :parameters ()
    :precondition 
      (in r231)
    :effect
      (and
        (in r230)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r231))
      )
    )
  (:action move-right-from-r232
    :parameters ()
    :precondition 
      (in r232)
    :effect
      (and
        (in r233)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r232))
      )
    )
  (:action move-left-from-r232
    :parameters ()
    :precondition 
      (in r232)
    :effect
      (and
        (in r231)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r232))
      )
    )
  (:action move-right-from-r233
    :parameters ()
    :precondition 
      (in r233)
    :effect
      (and
        (in r234)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r233))
      )
    )
  (:action move-left-from-r233
    :parameters ()
    :precondition 
      (in r233)
    :effect
      (and
        (in r232)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r233))
      )
    )
  (:action move-right-from-r234
    :parameters ()
    :precondition 
      (in r234)
    :effect
      (and
        (in r235)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r234))
      )
    )
  (:action move-left-from-r234
    :parameters ()
    :precondition 
      (in r234)
    :effect
      (and
        (in r233)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r234))
      )
    )
  (:action move-right-from-r235
    :parameters ()
    :precondition 
      (in r235)
    :effect
      (and
        (in r236)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r235))
      )
    )
  (:action move-left-from-r235
    :parameters ()
    :precondition 
      (in r235)
    :effect
      (and
        (in r234)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r235))
      )
    )
  (:action move-right-from-r236
    :parameters ()
    :precondition 
      (in r236)
    :effect
      (and
        (in r237)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r236))
      )
    )
  (:action move-left-from-r236
    :parameters ()
    :precondition 
      (in r236)
    :effect
      (and
        (in r235)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r236))
      )
    )
  (:action move-right-from-r237
    :parameters ()
    :precondition 
      (in r237)
    :effect
      (and
        (in r238)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r237))
      )
    )
  (:action move-left-from-r237
    :parameters ()
    :precondition 
      (in r237)
    :effect
      (and
        (in r236)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r237))
      )
    )
  (:action move-right-from-r238
    :parameters ()
    :precondition 
      (in r238)
    :effect
      (and
        (in r239)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r238))
      )
    )
  (:action move-left-from-r238
    :parameters ()
    :precondition 
      (in r238)
    :effect
      (and
        (in r237)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r238))
      )
    )
  (:action move-right-from-r239
    :parameters ()
    :precondition 
      (in r239)
    :effect
      (and
        (in r240)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r239))
      )
    )
  (:action move-left-from-r239
    :parameters ()
    :precondition 
      (in r239)
    :effect
      (and
        (in r238)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r239))
      )
    )
  (:action move-right-from-r240
    :parameters ()
    :precondition 
      (in r240)
    :effect
      (and
        (in r241)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r240))
      )
    )
  (:action move-left-from-r240
    :parameters ()
    :precondition 
      (in r240)
    :effect
      (and
        (in r239)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r240))
      )
    )
  (:action move-right-from-r241
    :parameters ()
    :precondition 
      (in r241)
    :effect
      (and
        (in r242)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r241))
      )
    )
  (:action move-left-from-r241
    :parameters ()
    :precondition 
      (in r241)
    :effect
      (and
        (in r240)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r241))
      )
    )
  (:action move-right-from-r242
    :parameters ()
    :precondition 
      (in r242)
    :effect
      (and
        (in r243)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r242))
      )
    )
  (:action move-left-from-r242
    :parameters ()
    :precondition 
      (in r242)
    :effect
      (and
        (in r241)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r242))
      )
    )
  (:action move-right-from-r243
    :parameters ()
    :precondition 
      (in r243)
    :effect
      (and
        (in r244)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r243))
      )
    )
  (:action move-left-from-r243
    :parameters ()
    :precondition 
      (in r243)
    :effect
      (and
        (in r242)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r243))
      )
    )
  (:action move-right-from-r244
    :parameters ()
    :precondition 
      (in r244)
    :effect
      (and
        (in r245)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r244))
      )
    )
  (:action move-left-from-r244
    :parameters ()
    :precondition 
      (in r244)
    :effect
      (and
        (in r243)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r244))
      )
    )
  (:action move-right-from-r245
    :parameters ()
    :precondition 
      (in r245)
    :effect
      (and
        (in r246)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r245))
      )
    )
  (:action move-left-from-r245
    :parameters ()
    :precondition 
      (in r245)
    :effect
      (and
        (in r244)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r245))
      )
    )
  (:action move-right-from-r246
    :parameters ()
    :precondition 
      (in r246)
    :effect
      (and
        (in r247)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r246))
      )
    )
  (:action move-left-from-r246
    :parameters ()
    :precondition 
      (in r246)
    :effect
      (and
        (in r245)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r246))
      )
    )
  (:action move-right-from-r247
    :parameters ()
    :precondition 
      (in r247)
    :effect
      (and
        (in r248)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r247))
      )
    )
  (:action move-left-from-r247
    :parameters ()
    :precondition 
      (in r247)
    :effect
      (and
        (in r246)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r247))
      )
    )
  (:action move-right-from-r248
    :parameters ()
    :precondition 
      (in r248)
    :effect
      (and
        (in r249)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r248))
      )
    )
  (:action move-left-from-r248
    :parameters ()
    :precondition 
      (in r248)
    :effect
      (and
        (in r247)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r248))
      )
    )
  (:action move-right-from-r249
    :parameters ()
    :precondition 
      (in r249)
    :effect
      (and
        (in r250)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r249))
      )
    )
  (:action move-left-from-r249
    :parameters ()
    :precondition 
      (in r249)
    :effect
      (and
        (in r248)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r249))
      )
    )
  (:action move-right-from-r250
    :parameters ()
    :precondition 
      (in r250)
    :effect
      (and
        (in r251)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r250))
      )
    )
  (:action move-left-from-r250
    :parameters ()
    :precondition 
      (in r250)
    :effect
      (and
        (in r249)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r250))
      )
    )
  (:action move-right-from-r251
    :parameters ()
    :precondition 
      (in r251)
    :effect
      (and
        (in r252)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r251))
      )
    )
  (:action move-left-from-r251
    :parameters ()
    :precondition 
      (in r251)
    :effect
      (and
        (in r250)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r251))
      )
    )
  (:action move-right-from-r252
    :parameters ()
    :precondition 
      (in r252)
    :effect
      (and
        (in r253)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r252))
      )
    )
  (:action move-left-from-r252
    :parameters ()
    :precondition 
      (in r252)
    :effect
      (and
        (in r251)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r252))
      )
    )
  (:action move-right-from-r253
    :parameters ()
    :precondition 
      (in r253)
    :effect
      (and
        (in r254)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r253))
      )
    )
  (:action move-left-from-r253
    :parameters ()
    :precondition 
      (in r253)
    :effect
      (and
        (in r252)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r253))
      )
    )
  (:action move-right-from-r254
    :parameters ()
    :precondition 
      (in r254)
    :effect
      (and
        (in r255)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r254))
      )
    )
  (:action move-left-from-r254
    :parameters ()
    :precondition 
      (in r254)
    :effect
      (and
        (in r253)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r254))
      )
    )
  (:action move-right-from-r255
    :parameters ()
    :precondition 
      (in r255)
    :effect
      (and
        (in r256)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r255))
      )
    )
  (:action move-left-from-r255
    :parameters ()
    :precondition 
      (in r255)
    :effect
      (and
        (in r254)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r255))
      )
    )
  (:action move-right-from-r256
    :parameters ()
    :precondition 
      (in r256)
    :effect
      (and
        (in r257)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r256))
      )
    )
  (:action move-left-from-r256
    :parameters ()
    :precondition 
      (in r256)
    :effect
      (and
        (in r255)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r256))
      )
    )
  (:action move-right-from-r257
    :parameters ()
    :precondition 
      (in r257)
    :effect
      (and
        (in r258)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r257))
      )
    )
  (:action move-left-from-r257
    :parameters ()
    :precondition 
      (in r257)
    :effect
      (and
        (in r256)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r257))
      )
    )
  (:action move-right-from-r258
    :parameters ()
    :precondition 
      (in r258)
    :effect
      (and
        (in r259)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r258))
      )
    )
  (:action move-left-from-r258
    :parameters ()
    :precondition 
      (in r258)
    :effect
      (and
        (in r257)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r258))
      )
    )
  (:action move-right-from-r259
    :parameters ()
    :precondition 
      (in r259)
    :effect
      (and
        (in r260)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r259))
      )
    )
  (:action move-left-from-r259
    :parameters ()
    :precondition 
      (in r259)
    :effect
      (and
        (in r258)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r259))
      )
    )
  (:action move-right-from-r260
    :parameters ()
    :precondition 
      (in r260)
    :effect
      (and
        (in r261)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r260))
      )
    )
  (:action move-left-from-r260
    :parameters ()
    :precondition 
      (in r260)
    :effect
      (and
        (in r259)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r260))
      )
    )
  (:action move-right-from-r261
    :parameters ()
    :precondition 
      (in r261)
    :effect
      (and
        (in r262)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r261))
      )
    )
  (:action move-left-from-r261
    :parameters ()
    :precondition 
      (in r261)
    :effect
      (and
        (in r260)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r261))
      )
    )
  (:action move-right-from-r262
    :parameters ()
    :precondition 
      (in r262)
    :effect
      (and
        (in r263)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r262))
      )
    )
  (:action move-left-from-r262
    :parameters ()
    :precondition 
      (in r262)
    :effect
      (and
        (in r261)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r262))
      )
    )
  (:action move-right-from-r263
    :parameters ()
    :precondition 
      (in r263)
    :effect
      (and
        (in r264)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r263))
      )
    )
  (:action move-left-from-r263
    :parameters ()
    :precondition 
      (in r263)
    :effect
      (and
        (in r262)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r263))
      )
    )
  (:action move-right-from-r264
    :parameters ()
    :precondition 
      (in r264)
    :effect
      (and
        (in r265)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r264))
      )
    )
  (:action move-left-from-r264
    :parameters ()
    :precondition 
      (in r264)
    :effect
      (and
        (in r263)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r264))
      )
    )
  (:action move-right-from-r265
    :parameters ()
    :precondition 
      (in r265)
    :effect
      (and
        (in r266)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r265))
      )
    )
  (:action move-left-from-r265
    :parameters ()
    :precondition 
      (in r265)
    :effect
      (and
        (in r264)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r265))
      )
    )
  (:action move-right-from-r266
    :parameters ()
    :precondition 
      (in r266)
    :effect
      (and
        (in r267)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r266))
      )
    )
  (:action move-left-from-r266
    :parameters ()
    :precondition 
      (in r266)
    :effect
      (and
        (in r265)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r266))
      )
    )
  (:action move-right-from-r267
    :parameters ()
    :precondition 
      (in r267)
    :effect
      (and
        (in r268)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r267))
      )
    )
  (:action move-left-from-r267
    :parameters ()
    :precondition 
      (in r267)
    :effect
      (and
        (in r266)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r267))
      )
    )
  (:action move-right-from-r268
    :parameters ()
    :precondition 
      (in r268)
    :effect
      (and
        (in r269)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r268))
      )
    )
  (:action move-left-from-r268
    :parameters ()
    :precondition 
      (in r268)
    :effect
      (and
        (in r267)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r268))
      )
    )
  (:action move-right-from-r269
    :parameters ()
    :precondition 
      (in r269)
    :effect
      (and
        (in r270)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r269))
      )
    )
  (:action move-left-from-r269
    :parameters ()
    :precondition 
      (in r269)
    :effect
      (and
        (in r268)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r269))
      )
    )
  (:action move-right-from-r270
    :parameters ()
    :precondition 
      (in r270)
    :effect
      (and
        (in r271)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r270))
      )
    )
  (:action move-left-from-r270
    :parameters ()
    :precondition 
      (in r270)
    :effect
      (and
        (in r269)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r270))
      )
    )
  (:action move-right-from-r271
    :parameters ()
    :precondition 
      (in r271)
    :effect
      (and
        (in r272)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r271))
      )
    )
  (:action move-left-from-r271
    :parameters ()
    :precondition 
      (in r271)
    :effect
      (and
        (in r270)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r271))
      )
    )
  (:action move-right-from-r272
    :parameters ()
    :precondition 
      (in r272)
    :effect
      (and
        (in r273)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r272))
      )
    )
  (:action move-left-from-r272
    :parameters ()
    :precondition 
      (in r272)
    :effect
      (and
        (in r271)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r272))
      )
    )
  (:action move-right-from-r273
    :parameters ()
    :precondition 
      (in r273)
    :effect
      (and
        (in r274)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r273))
      )
    )
  (:action move-left-from-r273
    :parameters ()
    :precondition 
      (in r273)
    :effect
      (and
        (in r272)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r273))
      )
    )
  (:action move-right-from-r274
    :parameters ()
    :precondition 
      (in r274)
    :effect
      (and
        (in r275)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r274))
      )
    )
  (:action move-left-from-r274
    :parameters ()
    :precondition 
      (in r274)
    :effect
      (and
        (in r273)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r274))
      )
    )
  (:action move-right-from-r275
    :parameters ()
    :precondition 
      (in r275)
    :effect
      (and
        (in r276)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r275))
      )
    )
  (:action move-left-from-r275
    :parameters ()
    :precondition 
      (in r275)
    :effect
      (and
        (in r274)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r275))
      )
    )
  (:action move-right-from-r276
    :parameters ()
    :precondition 
      (in r276)
    :effect
      (and
        (in r277)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r276))
      )
    )
  (:action move-left-from-r276
    :parameters ()
    :precondition 
      (in r276)
    :effect
      (and
        (in r275)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r276))
      )
    )
  (:action move-right-from-r277
    :parameters ()
    :precondition 
      (in r277)
    :effect
      (and
        (in r278)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r277))
      )
    )
  (:action move-left-from-r277
    :parameters ()
    :precondition 
      (in r277)
    :effect
      (and
        (in r276)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r277))
      )
    )
  (:action move-right-from-r278
    :parameters ()
    :precondition 
      (in r278)
    :effect
      (and
        (in r279)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r278))
      )
    )
  (:action move-left-from-r278
    :parameters ()
    :precondition 
      (in r278)
    :effect
      (and
        (in r277)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r278))
      )
    )
  (:action move-right-from-r279
    :parameters ()
    :precondition 
      (in r279)
    :effect
      (and
        (in r280)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r279))
      )
    )
  (:action move-left-from-r279
    :parameters ()
    :precondition 
      (in r279)
    :effect
      (and
        (in r278)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r279))
      )
    )
  (:action move-right-from-r280
    :parameters ()
    :precondition 
      (in r280)
    :effect
      (and
        (in r281)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r280))
      )
    )
  (:action move-left-from-r280
    :parameters ()
    :precondition 
      (in r280)
    :effect
      (and
        (in r279)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r280))
      )
    )
  (:action move-right-from-r281
    :parameters ()
    :precondition 
      (in r281)
    :effect
      (and
        (in r282)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r281))
      )
    )
  (:action move-left-from-r281
    :parameters ()
    :precondition 
      (in r281)
    :effect
      (and
        (in r280)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r281))
      )
    )
  (:action move-right-from-r282
    :parameters ()
    :precondition 
      (in r282)
    :effect
      (and
        (in r283)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r282))
      )
    )
  (:action move-left-from-r282
    :parameters ()
    :precondition 
      (in r282)
    :effect
      (and
        (in r281)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r282))
      )
    )
  (:action move-right-from-r283
    :parameters ()
    :precondition 
      (in r283)
    :effect
      (and
        (in r284)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r283))
      )
    )
  (:action move-left-from-r283
    :parameters ()
    :precondition 
      (in r283)
    :effect
      (and
        (in r282)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r283))
      )
    )
  (:action move-right-from-r284
    :parameters ()
    :precondition 
      (in r284)
    :effect
      (and
        (in r285)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r284))
      )
    )
  (:action move-left-from-r284
    :parameters ()
    :precondition 
      (in r284)
    :effect
      (and
        (in r283)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r284))
      )
    )
  (:action move-right-from-r285
    :parameters ()
    :precondition 
      (in r285)
    :effect
      (and
        (in r286)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r285))
      )
    )
  (:action move-left-from-r285
    :parameters ()
    :precondition 
      (in r285)
    :effect
      (and
        (in r284)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r285))
      )
    )
  (:action move-right-from-r286
    :parameters ()
    :precondition 
      (in r286)
    :effect
      (and
        (in r287)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r286))
      )
    )
  (:action move-left-from-r286
    :parameters ()
    :precondition 
      (in r286)
    :effect
      (and
        (in r285)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r286))
      )
    )
  (:action move-right-from-r287
    :parameters ()
    :precondition 
      (in r287)
    :effect
      (and
        (in r288)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r287))
      )
    )
  (:action move-left-from-r287
    :parameters ()
    :precondition 
      (in r287)
    :effect
      (and
        (in r286)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r287))
      )
    )
  (:action move-right-from-r288
    :parameters ()
    :precondition 
      (in r288)
    :effect
      (and
        (in r289)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r288))
      )
    )
  (:action move-left-from-r288
    :parameters ()
    :precondition 
      (in r288)
    :effect
      (and
        (in r287)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r288))
      )
    )
  (:action move-right-from-r289
    :parameters ()
    :precondition 
      (in r289)
    :effect
      (and
        (in r290)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r289))
      )
    )
  (:action move-left-from-r289
    :parameters ()
    :precondition 
      (in r289)
    :effect
      (and
        (in r288)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r289))
      )
    )
  (:action move-right-from-r290
    :parameters ()
    :precondition 
      (in r290)
    :effect
      (and
        (in r291)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r290))
      )
    )
  (:action move-left-from-r290
    :parameters ()
    :precondition 
      (in r290)
    :effect
      (and
        (in r289)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r290))
      )
    )
  (:action move-right-from-r291
    :parameters ()
    :precondition 
      (in r291)
    :effect
      (and
        (in r292)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r291))
      )
    )
  (:action move-left-from-r291
    :parameters ()
    :precondition 
      (in r291)
    :effect
      (and
        (in r290)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r291))
      )
    )
  (:action move-right-from-r292
    :parameters ()
    :precondition 
      (in r292)
    :effect
      (and
        (in r293)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r292))
      )
    )
  (:action move-left-from-r292
    :parameters ()
    :precondition 
      (in r292)
    :effect
      (and
        (in r291)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r292))
      )
    )
  (:action move-right-from-r293
    :parameters ()
    :precondition 
      (in r293)
    :effect
      (and
        (in r294)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r293))
      )
    )
  (:action move-left-from-r293
    :parameters ()
    :precondition 
      (in r293)
    :effect
      (and
        (in r292)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r293))
      )
    )
  (:action move-right-from-r294
    :parameters ()
    :precondition 
      (in r294)
    :effect
      (and
        (in r295)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r294))
      )
    )
  (:action move-left-from-r294
    :parameters ()
    :precondition 
      (in r294)
    :effect
      (and
        (in r293)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r294))
      )
    )
  (:action move-right-from-r295
    :parameters ()
    :precondition 
      (in r295)
    :effect
      (and
        (in r296)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r295))
      )
    )
  (:action move-left-from-r295
    :parameters ()
    :precondition 
      (in r295)
    :effect
      (and
        (in r294)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r295))
      )
    )
  (:action move-right-from-r296
    :parameters ()
    :precondition 
      (in r296)
    :effect
      (and
        (in r297)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r296))
      )
    )
  (:action move-left-from-r296
    :parameters ()
    :precondition 
      (in r296)
    :effect
      (and
        (in r295)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r296))
      )
    )
  (:action move-right-from-r297
    :parameters ()
    :precondition 
      (in r297)
    :effect
      (and
        (in r298)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r297))
      )
    )
  (:action move-left-from-r297
    :parameters ()
    :precondition 
      (in r297)
    :effect
      (and
        (in r296)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r297))
      )
    )
  (:action move-right-from-r298
    :parameters ()
    :precondition 
      (in r298)
    :effect
      (and
        (in r299)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r298))
      )
    )
  (:action move-left-from-r298
    :parameters ()
    :precondition 
      (in r298)
    :effect
      (and
        (in r297)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r298))
      )
    )
  (:action move-right-from-r299
    :parameters ()
    :precondition 
      (in r299)
    :effect
      (and
        (in r300)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r299))
      )
    )
  (:action move-left-from-r299
    :parameters ()
    :precondition 
      (in r299)
    :effect
      (and
        (in r298)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r299))
      )
    )
  (:action move-right-from-r300
    :parameters ()
    :precondition 
      (in r300)
    :effect
      (and
        (in r301)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r300))
      )
    )
  (:action move-left-from-r300
    :parameters ()
    :precondition 
      (in r300)
    :effect
      (and
        (in r299)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r300))
      )
    )
  (:action move-right-from-r301
    :parameters ()
    :precondition 
      (in r301)
    :effect
      (and
        (in r302)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r301))
      )
    )
  (:action move-left-from-r301
    :parameters ()
    :precondition 
      (in r301)
    :effect
      (and
        (in r300)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r301))
      )
    )
  (:action move-right-from-r302
    :parameters ()
    :precondition 
      (in r302)
    :effect
      (and
        (in r303)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r302))
      )
    )
  (:action move-left-from-r302
    :parameters ()
    :precondition 
      (in r302)
    :effect
      (and
        (in r301)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r302))
      )
    )
  (:action move-right-from-r303
    :parameters ()
    :precondition 
      (in r303)
    :effect
      (and
        (in r304)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r303))
      )
    )
  (:action move-left-from-r303
    :parameters ()
    :precondition 
      (in r303)
    :effect
      (and
        (in r302)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r303))
      )
    )
  (:action move-right-from-r304
    :parameters ()
    :precondition 
      (in r304)
    :effect
      (and
        (in r305)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r304))
      )
    )
  (:action move-left-from-r304
    :parameters ()
    :precondition 
      (in r304)
    :effect
      (and
        (in r303)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r304))
      )
    )
  (:action move-right-from-r305
    :parameters ()
    :precondition 
      (in r305)
    :effect
      (and
        (in r306)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r305))
      )
    )
  (:action move-left-from-r305
    :parameters ()
    :precondition 
      (in r305)
    :effect
      (and
        (in r304)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r305))
      )
    )
  (:action move-right-from-r306
    :parameters ()
    :precondition 
      (in r306)
    :effect
      (and
        (in r307)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r306))
      )
    )
  (:action move-left-from-r306
    :parameters ()
    :precondition 
      (in r306)
    :effect
      (and
        (in r305)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r306))
      )
    )
  (:action move-right-from-r307
    :parameters ()
    :precondition 
      (in r307)
    :effect
      (and
        (in r308)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r307))
      )
    )
  (:action move-left-from-r307
    :parameters ()
    :precondition 
      (in r307)
    :effect
      (and
        (in r306)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r307))
      )
    )
  (:action move-right-from-r308
    :parameters ()
    :precondition 
      (in r308)
    :effect
      (and
        (in r309)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r308))
      )
    )
  (:action move-left-from-r308
    :parameters ()
    :precondition 
      (in r308)
    :effect
      (and
        (in r307)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r308))
      )
    )
  (:action move-right-from-r309
    :parameters ()
    :precondition 
      (in r309)
    :effect
      (and
        (in r310)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r309))
      )
    )
  (:action move-left-from-r309
    :parameters ()
    :precondition 
      (in r309)
    :effect
      (and
        (in r308)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r309))
      )
    )
  (:action move-right-from-r310
    :parameters ()
    :precondition 
      (in r310)
    :effect
      (and
        (in r311)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r310))
      )
    )
  (:action move-left-from-r310
    :parameters ()
    :precondition 
      (in r310)
    :effect
      (and
        (in r309)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r310))
      )
    )
  (:action move-right-from-r311
    :parameters ()
    :precondition 
      (in r311)
    :effect
      (and
        (in r312)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r311))
      )
    )
  (:action move-left-from-r311
    :parameters ()
    :precondition 
      (in r311)
    :effect
      (and
        (in r310)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r311))
      )
    )
  (:action move-right-from-r312
    :parameters ()
    :precondition 
      (in r312)
    :effect
      (and
        (in r313)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r312))
      )
    )
  (:action move-left-from-r312
    :parameters ()
    :precondition 
      (in r312)
    :effect
      (and
        (in r311)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r312))
      )
    )
  (:action move-right-from-r313
    :parameters ()
    :precondition 
      (in r313)
    :effect
      (and
        (in r314)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r313))
      )
    )
  (:action move-left-from-r313
    :parameters ()
    :precondition 
      (in r313)
    :effect
      (and
        (in r312)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r313))
      )
    )
  (:action move-right-from-r314
    :parameters ()
    :precondition 
      (in r314)
    :effect
      (and
        (in r315)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r314))
      )
    )
  (:action move-left-from-r314
    :parameters ()
    :precondition 
      (in r314)
    :effect
      (and
        (in r313)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r314))
      )
    )
  (:action move-right-from-r315
    :parameters ()
    :precondition 
      (in r315)
    :effect
      (and
        (in r316)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r315))
      )
    )
  (:action move-left-from-r315
    :parameters ()
    :precondition 
      (in r315)
    :effect
      (and
        (in r314)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r315))
      )
    )
  (:action move-right-from-r316
    :parameters ()
    :precondition 
      (in r316)
    :effect
      (and
        (in r317)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r316))
      )
    )
  (:action move-left-from-r316
    :parameters ()
    :precondition 
      (in r316)
    :effect
      (and
        (in r315)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r316))
      )
    )
  (:action move-right-from-r317
    :parameters ()
    :precondition 
      (in r317)
    :effect
      (and
        (in r318)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r317))
      )
    )
  (:action move-left-from-r317
    :parameters ()
    :precondition 
      (in r317)
    :effect
      (and
        (in r316)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r317))
      )
    )
  (:action move-right-from-r318
    :parameters ()
    :precondition 
      (in r318)
    :effect
      (and
        (in r319)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r318))
      )
    )
  (:action move-left-from-r318
    :parameters ()
    :precondition 
      (in r318)
    :effect
      (and
        (in r317)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r318))
      )
    )
  (:action move-right-from-r319
    :parameters ()
    :precondition 
      (in r319)
    :effect
      (and
        (in r320)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r319))
      )
    )
  (:action move-left-from-r319
    :parameters ()
    :precondition 
      (in r319)
    :effect
      (and
        (in r318)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r319))
      )
    )
  (:action move-right-from-r320
    :parameters ()
    :precondition 
      (in r320)
    :effect
      (and
        (in r321)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r320))
      )
    )
  (:action move-left-from-r320
    :parameters ()
    :precondition 
      (in r320)
    :effect
      (and
        (in r319)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r320))
      )
    )
  (:action move-right-from-r321
    :parameters ()
    :precondition 
      (in r321)
    :effect
      (and
        (in r322)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r321))
      )
    )
  (:action move-left-from-r321
    :parameters ()
    :precondition 
      (in r321)
    :effect
      (and
        (in r320)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r321))
      )
    )
  (:action move-right-from-r322
    :parameters ()
    :precondition 
      (in r322)
    :effect
      (and
        (in r323)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r322))
      )
    )
  (:action move-left-from-r322
    :parameters ()
    :precondition 
      (in r322)
    :effect
      (and
        (in r321)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r322))
      )
    )
  (:action move-right-from-r323
    :parameters ()
    :precondition 
      (in r323)
    :effect
      (and
        (in r324)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r323))
      )
    )
  (:action move-left-from-r323
    :parameters ()
    :precondition 
      (in r323)
    :effect
      (and
        (in r322)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r323))
      )
    )
  (:action move-right-from-r324
    :parameters ()
    :precondition 
      (in r324)
    :effect
      (and
        (in r325)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r324))
      )
    )
  (:action move-left-from-r324
    :parameters ()
    :precondition 
      (in r324)
    :effect
      (and
        (in r323)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r324))
      )
    )
  (:action move-right-from-r325
    :parameters ()
    :precondition 
      (in r325)
    :effect
      (and
        (in r326)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r325))
      )
    )
  (:action move-left-from-r325
    :parameters ()
    :precondition 
      (in r325)
    :effect
      (and
        (in r324)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r325))
      )
    )
  (:action move-right-from-r326
    :parameters ()
    :precondition 
      (in r326)
    :effect
      (and
        (in r327)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r326))
      )
    )
  (:action move-left-from-r326
    :parameters ()
    :precondition 
      (in r326)
    :effect
      (and
        (in r325)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r326))
      )
    )
  (:action move-right-from-r327
    :parameters ()
    :precondition 
      (in r327)
    :effect
      (and
        (in r328)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r327))
      )
    )
  (:action move-left-from-r327
    :parameters ()
    :precondition 
      (in r327)
    :effect
      (and
        (in r326)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r327))
      )
    )
  (:action move-right-from-r328
    :parameters ()
    :precondition 
      (in r328)
    :effect
      (and
        (in r329)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r328))
      )
    )
  (:action move-left-from-r328
    :parameters ()
    :precondition 
      (in r328)
    :effect
      (and
        (in r327)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r328))
      )
    )
  (:action move-right-from-r329
    :parameters ()
    :precondition 
      (in r329)
    :effect
      (and
        (in r330)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r329))
      )
    )
  (:action move-left-from-r329
    :parameters ()
    :precondition 
      (in r329)
    :effect
      (and
        (in r328)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r329))
      )
    )
  (:action move-right-from-r330
    :parameters ()
    :precondition 
      (in r330)
    :effect
      (and
        (in r331)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r330))
      )
    )
  (:action move-left-from-r330
    :parameters ()
    :precondition 
      (in r330)
    :effect
      (and
        (in r329)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r330))
      )
    )
  (:action move-right-from-r331
    :parameters ()
    :precondition 
      (in r331)
    :effect
      (and
        (in r332)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r331))
      )
    )
  (:action move-left-from-r331
    :parameters ()
    :precondition 
      (in r331)
    :effect
      (and
        (in r330)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r331))
      )
    )
  (:action move-right-from-r332
    :parameters ()
    :precondition 
      (in r332)
    :effect
      (and
        (in r333)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r332))
      )
    )
  (:action move-left-from-r332
    :parameters ()
    :precondition 
      (in r332)
    :effect
      (and
        (in r331)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r332))
      )
    )
  (:action move-right-from-r333
    :parameters ()
    :precondition 
      (in r333)
    :effect
      (and
        (in r334)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r333))
      )
    )
  (:action move-left-from-r333
    :parameters ()
    :precondition 
      (in r333)
    :effect
      (and
        (in r332)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r333))
      )
    )
  (:action move-right-from-r334
    :parameters ()
    :precondition 
      (in r334)
    :effect
      (and
        (in r335)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r334))
      )
    )
  (:action move-left-from-r334
    :parameters ()
    :precondition 
      (in r334)
    :effect
      (and
        (in r333)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r334))
      )
    )
  (:action move-right-from-r335
    :parameters ()
    :precondition 
      (in r335)
    :effect
      (and
        (in r336)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r335))
      )
    )
  (:action move-left-from-r335
    :parameters ()
    :precondition 
      (in r335)
    :effect
      (and
        (in r334)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r335))
      )
    )
  (:action move-right-from-r336
    :parameters ()
    :precondition 
      (in r336)
    :effect
      (and
        (in r337)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r336))
      )
    )
  (:action move-left-from-r336
    :parameters ()
    :precondition 
      (in r336)
    :effect
      (and
        (in r335)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r336))
      )
    )
  (:action move-right-from-r337
    :parameters ()
    :precondition 
      (in r337)
    :effect
      (and
        (in r338)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r337))
      )
    )
  (:action move-left-from-r337
    :parameters ()
    :precondition 
      (in r337)
    :effect
      (and
        (in r336)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r337))
      )
    )
  (:action move-right-from-r338
    :parameters ()
    :precondition 
      (in r338)
    :effect
      (and
        (in r339)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r338))
      )
    )
  (:action move-left-from-r338
    :parameters ()
    :precondition 
      (in r338)
    :effect
      (and
        (in r337)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r338))
      )
    )
  (:action move-right-from-r339
    :parameters ()
    :precondition 
      (in r339)
    :effect
      (and
        (in r340)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r339))
      )
    )
  (:action move-left-from-r339
    :parameters ()
    :precondition 
      (in r339)
    :effect
      (and
        (in r338)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r339))
      )
    )
  (:action move-right-from-r340
    :parameters ()
    :precondition 
      (in r340)
    :effect
      (and
        (in r341)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r340))
      )
    )
  (:action move-left-from-r340
    :parameters ()
    :precondition 
      (in r340)
    :effect
      (and
        (in r339)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r340))
      )
    )
  (:action move-right-from-r341
    :parameters ()
    :precondition 
      (in r341)
    :effect
      (and
        (in r342)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r341))
      )
    )
  (:action move-left-from-r341
    :parameters ()
    :precondition 
      (in r341)
    :effect
      (and
        (in r340)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r341))
      )
    )
  (:action move-right-from-r342
    :parameters ()
    :precondition 
      (in r342)
    :effect
      (and
        (in r343)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r342))
      )
    )
  (:action move-left-from-r342
    :parameters ()
    :precondition 
      (in r342)
    :effect
      (and
        (in r341)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r342))
      )
    )
  (:action move-right-from-r343
    :parameters ()
    :precondition 
      (in r343)
    :effect
      (and
        (in r344)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r343))
      )
    )
  (:action move-left-from-r343
    :parameters ()
    :precondition 
      (in r343)
    :effect
      (and
        (in r342)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r343))
      )
    )
  (:action move-right-from-r344
    :parameters ()
    :precondition 
      (in r344)
    :effect
      (and
        (in r345)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r344))
      )
    )
  (:action move-left-from-r344
    :parameters ()
    :precondition 
      (in r344)
    :effect
      (and
        (in r343)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r344))
      )
    )
  (:action move-right-from-r345
    :parameters ()
    :precondition 
      (in r345)
    :effect
      (and
        (in r346)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r345))
      )
    )
  (:action move-left-from-r345
    :parameters ()
    :precondition 
      (in r345)
    :effect
      (and
        (in r344)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r345))
      )
    )
  (:action move-right-from-r346
    :parameters ()
    :precondition 
      (in r346)
    :effect
      (and
        (in r347)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r346))
      )
    )
  (:action move-left-from-r346
    :parameters ()
    :precondition 
      (in r346)
    :effect
      (and
        (in r345)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r346))
      )
    )
  (:action move-right-from-r347
    :parameters ()
    :precondition 
      (in r347)
    :effect
      (and
        (in r348)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r347))
      )
    )
  (:action move-left-from-r347
    :parameters ()
    :precondition 
      (in r347)
    :effect
      (and
        (in r346)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r347))
      )
    )
  (:action move-right-from-r348
    :parameters ()
    :precondition 
      (in r348)
    :effect
      (and
        (in r349)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r348))
      )
    )
  (:action move-left-from-r348
    :parameters ()
    :precondition 
      (in r348)
    :effect
      (and
        (in r347)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r348))
      )
    )
  (:action move-right-from-r349
    :parameters ()
    :precondition 
      (in r349)
    :effect
      (and
        (in r350)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r349))
      )
    )
  (:action move-left-from-r349
    :parameters ()
    :precondition 
      (in r349)
    :effect
      (and
        (in r348)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r349))
      )
    )
  (:action move-right-from-r350
    :parameters ()
    :precondition 
      (in r350)
    :effect
      (and
        (in r351)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r350))
      )
    )
  (:action move-left-from-r350
    :parameters ()
    :precondition 
      (in r350)
    :effect
      (and
        (in r349)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r350))
      )
    )
  (:action move-right-from-r351
    :parameters ()
    :precondition 
      (in r351)
    :effect
      (and
        (in r352)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r351))
      )
    )
  (:action move-left-from-r351
    :parameters ()
    :precondition 
      (in r351)
    :effect
      (and
        (in r350)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r351))
      )
    )
  (:action move-right-from-r352
    :parameters ()
    :precondition 
      (in r352)
    :effect
      (and
        (in r353)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r352))
      )
    )
  (:action move-left-from-r352
    :parameters ()
    :precondition 
      (in r352)
    :effect
      (and
        (in r351)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r352))
      )
    )
  (:action move-right-from-r353
    :parameters ()
    :precondition 
      (in r353)
    :effect
      (and
        (in r354)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r353))
      )
    )
  (:action move-left-from-r353
    :parameters ()
    :precondition 
      (in r353)
    :effect
      (and
        (in r352)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r353))
      )
    )
  (:action move-right-from-r354
    :parameters ()
    :precondition 
      (in r354)
    :effect
      (and
        (in r355)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r354))
      )
    )
  (:action move-left-from-r354
    :parameters ()
    :precondition 
      (in r354)
    :effect
      (and
        (in r353)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r354))
      )
    )
  (:action move-right-from-r355
    :parameters ()
    :precondition 
      (in r355)
    :effect
      (and
        (in r356)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r355))
      )
    )
  (:action move-left-from-r355
    :parameters ()
    :precondition 
      (in r355)
    :effect
      (and
        (in r354)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r355))
      )
    )
  (:action move-right-from-r356
    :parameters ()
    :precondition 
      (in r356)
    :effect
      (and
        (in r357)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r356))
      )
    )
  (:action move-left-from-r356
    :parameters ()
    :precondition 
      (in r356)
    :effect
      (and
        (in r355)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r356))
      )
    )
  (:action move-right-from-r357
    :parameters ()
    :precondition 
      (in r357)
    :effect
      (and
        (in r358)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r357))
      )
    )
  (:action move-left-from-r357
    :parameters ()
    :precondition 
      (in r357)
    :effect
      (and
        (in r356)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r357))
      )
    )
  (:action move-right-from-r358
    :parameters ()
    :precondition 
      (in r358)
    :effect
      (and
        (in r359)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r358))
      )
    )
  (:action move-left-from-r358
    :parameters ()
    :precondition 
      (in r358)
    :effect
      (and
        (in r357)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r358))
      )
    )
  (:action move-right-from-r359
    :parameters ()
    :precondition 
      (in r359)
    :effect
      (and
        (in r360)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r359))
      )
    )
  (:action move-left-from-r359
    :parameters ()
    :precondition 
      (in r359)
    :effect
      (and
        (in r358)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r359))
      )
    )
  (:action move-right-from-r360
    :parameters ()
    :precondition 
      (in r360)
    :effect
      (and
        (in r361)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r360))
      )
    )
  (:action move-left-from-r360
    :parameters ()
    :precondition 
      (in r360)
    :effect
      (and
        (in r359)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r360))
      )
    )
  (:action move-right-from-r361
    :parameters ()
    :precondition 
      (in r361)
    :effect
      (and
        (in r362)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r361))
      )
    )
  (:action move-left-from-r361
    :parameters ()
    :precondition 
      (in r361)
    :effect
      (and
        (in r360)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r361))
      )
    )
  (:action move-right-from-r362
    :parameters ()
    :precondition 
      (in r362)
    :effect
      (and
        (in r363)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r362))
      )
    )
  (:action move-left-from-r362
    :parameters ()
    :precondition 
      (in r362)
    :effect
      (and
        (in r361)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r362))
      )
    )
  (:action move-right-from-r363
    :parameters ()
    :precondition 
      (in r363)
    :effect
      (and
        (in r364)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r363))
      )
    )
  (:action move-left-from-r363
    :parameters ()
    :precondition 
      (in r363)
    :effect
      (and
        (in r362)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r363))
      )
    )
  (:action move-right-from-r364
    :parameters ()
    :precondition 
      (in r364)
    :effect
      (and
        (in r365)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r364))
      )
    )
  (:action move-left-from-r364
    :parameters ()
    :precondition 
      (in r364)
    :effect
      (and
        (in r363)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r364))
      )
    )
  (:action move-right-from-r365
    :parameters ()
    :precondition 
      (in r365)
    :effect
      (and
        (in r366)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r365))
      )
    )
  (:action move-left-from-r365
    :parameters ()
    :precondition 
      (in r365)
    :effect
      (and
        (in r364)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r365))
      )
    )
  (:action move-right-from-r366
    :parameters ()
    :precondition 
      (in r366)
    :effect
      (and
        (in r367)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r366))
      )
    )
  (:action move-left-from-r366
    :parameters ()
    :precondition 
      (in r366)
    :effect
      (and
        (in r365)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r366))
      )
    )
  (:action move-right-from-r367
    :parameters ()
    :precondition 
      (in r367)
    :effect
      (and
        (in r368)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r367))
      )
    )
  (:action move-left-from-r367
    :parameters ()
    :precondition 
      (in r367)
    :effect
      (and
        (in r366)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r367))
      )
    )
  (:action move-right-from-r368
    :parameters ()
    :precondition 
      (in r368)
    :effect
      (and
        (in r369)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r368))
      )
    )
  (:action move-left-from-r368
    :parameters ()
    :precondition 
      (in r368)
    :effect
      (and
        (in r367)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r368))
      )
    )
  (:action move-right-from-r369
    :parameters ()
    :precondition 
      (in r369)
    :effect
      (and
        (in r370)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r369))
      )
    )
  (:action move-left-from-r369
    :parameters ()
    :precondition 
      (in r369)
    :effect
      (and
        (in r368)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r369))
      )
    )
  (:action move-right-from-r370
    :parameters ()
    :precondition 
      (in r370)
    :effect
      (and
        (in r371)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r370))
      )
    )
  (:action move-left-from-r370
    :parameters ()
    :precondition 
      (in r370)
    :effect
      (and
        (in r369)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r370))
      )
    )
  (:action move-right-from-r371
    :parameters ()
    :precondition 
      (in r371)
    :effect
      (and
        (in r372)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r371))
      )
    )
  (:action move-left-from-r371
    :parameters ()
    :precondition 
      (in r371)
    :effect
      (and
        (in r370)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r371))
      )
    )
  (:action move-right-from-r372
    :parameters ()
    :precondition 
      (in r372)
    :effect
      (and
        (in r373)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r372))
      )
    )
  (:action move-left-from-r372
    :parameters ()
    :precondition 
      (in r372)
    :effect
      (and
        (in r371)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r372))
      )
    )
  (:action move-right-from-r373
    :parameters ()
    :precondition 
      (in r373)
    :effect
      (and
        (in r374)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r373))
      )
    )
  (:action move-left-from-r373
    :parameters ()
    :precondition 
      (in r373)
    :effect
      (and
        (in r372)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r373))
      )
    )
  (:action move-right-from-r374
    :parameters ()
    :precondition 
      (in r374)
    :effect
      (and
        (in r375)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r374))
      )
    )
  (:action move-left-from-r374
    :parameters ()
    :precondition 
      (in r374)
    :effect
      (and
        (in r373)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r374))
      )
    )
  (:action move-right-from-r375
    :parameters ()
    :precondition 
      (in r375)
    :effect
      (and
        (in r376)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r375))
      )
    )
  (:action move-left-from-r375
    :parameters ()
    :precondition 
      (in r375)
    :effect
      (and
        (in r374)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r375))
      )
    )
  (:action move-right-from-r376
    :parameters ()
    :precondition 
      (in r376)
    :effect
      (and
        (in r377)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r376))
      )
    )
  (:action move-left-from-r376
    :parameters ()
    :precondition 
      (in r376)
    :effect
      (and
        (in r375)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r376))
      )
    )
  (:action move-right-from-r377
    :parameters ()
    :precondition 
      (in r377)
    :effect
      (and
        (in r378)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r377))
      )
    )
  (:action move-left-from-r377
    :parameters ()
    :precondition 
      (in r377)
    :effect
      (and
        (in r376)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r377))
      )
    )
  (:action move-right-from-r378
    :parameters ()
    :precondition 
      (in r378)
    :effect
      (and
        (in r379)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r378))
      )
    )
  (:action move-left-from-r378
    :parameters ()
    :precondition 
      (in r378)
    :effect
      (and
        (in r377)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r378))
      )
    )
  (:action move-right-from-r379
    :parameters ()
    :precondition 
      (in r379)
    :effect
      (and
        (in r380)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r379))
      )
    )
  (:action move-left-from-r379
    :parameters ()
    :precondition 
      (in r379)
    :effect
      (and
        (in r378)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r379))
      )
    )
  (:action move-right-from-r380
    :parameters ()
    :precondition 
      (in r380)
    :effect
      (and
        (in r381)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r380))
      )
    )
  (:action move-left-from-r380
    :parameters ()
    :precondition 
      (in r380)
    :effect
      (and
        (in r379)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r380))
      )
    )
  (:action move-right-from-r381
    :parameters ()
    :precondition 
      (in r381)
    :effect
      (and
        (in r382)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r381))
      )
    )
  (:action move-left-from-r381
    :parameters ()
    :precondition 
      (in r381)
    :effect
      (and
        (in r380)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r381))
      )
    )
  (:action move-right-from-r382
    :parameters ()
    :precondition 
      (in r382)
    :effect
      (and
        (in r383)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r382))
      )
    )
  (:action move-left-from-r382
    :parameters ()
    :precondition 
      (in r382)
    :effect
      (and
        (in r381)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r382))
      )
    )
  (:action move-right-from-r383
    :parameters ()
    :precondition 
      (in r383)
    :effect
      (and
        (in r384)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r383))
      )
    )
  (:action move-left-from-r383
    :parameters ()
    :precondition 
      (in r383)
    :effect
      (and
        (in r382)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r383))
      )
    )
  (:action move-right-from-r384
    :parameters ()
    :precondition 
      (in r384)
    :effect
      (and
        (in r385)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r384))
      )
    )
  (:action move-left-from-r384
    :parameters ()
    :precondition 
      (in r384)
    :effect
      (and
        (in r383)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r384))
      )
    )
  (:action move-right-from-r385
    :parameters ()
    :precondition 
      (in r385)
    :effect
      (and
        (in r386)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r385))
      )
    )
  (:action move-left-from-r385
    :parameters ()
    :precondition 
      (in r385)
    :effect
      (and
        (in r384)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r385))
      )
    )
  (:action move-right-from-r386
    :parameters ()
    :precondition 
      (in r386)
    :effect
      (and
        (in r387)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r386))
      )
    )
  (:action move-left-from-r386
    :parameters ()
    :precondition 
      (in r386)
    :effect
      (and
        (in r385)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r386))
      )
    )
  (:action move-right-from-r387
    :parameters ()
    :precondition 
      (in r387)
    :effect
      (and
        (in r388)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r387))
      )
    )
  (:action move-left-from-r387
    :parameters ()
    :precondition 
      (in r387)
    :effect
      (and
        (in r386)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r387))
      )
    )
  (:action move-right-from-r388
    :parameters ()
    :precondition 
      (in r388)
    :effect
      (and
        (in r389)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r388))
      )
    )
  (:action move-left-from-r388
    :parameters ()
    :precondition 
      (in r388)
    :effect
      (and
        (in r387)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r388))
      )
    )
  (:action move-right-from-r389
    :parameters ()
    :precondition 
      (in r389)
    :effect
      (and
        (in r390)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r389))
      )
    )
  (:action move-left-from-r389
    :parameters ()
    :precondition 
      (in r389)
    :effect
      (and
        (in r388)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r389))
      )
    )
  (:action move-right-from-r390
    :parameters ()
    :precondition 
      (in r390)
    :effect
      (and
        (in r391)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r390))
      )
    )
  (:action move-left-from-r390
    :parameters ()
    :precondition 
      (in r390)
    :effect
      (and
        (in r389)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r390))
      )
    )
  (:action move-right-from-r391
    :parameters ()
    :precondition 
      (in r391)
    :effect
      (and
        (in r392)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r391))
      )
    )
  (:action move-left-from-r391
    :parameters ()
    :precondition 
      (in r391)
    :effect
      (and
        (in r390)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r391))
      )
    )
  (:action move-right-from-r392
    :parameters ()
    :precondition 
      (in r392)
    :effect
      (and
        (in r393)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r392))
      )
    )
  (:action move-left-from-r392
    :parameters ()
    :precondition 
      (in r392)
    :effect
      (and
        (in r391)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r392))
      )
    )
  (:action move-right-from-r393
    :parameters ()
    :precondition 
      (in r393)
    :effect
      (and
        (in r394)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r393))
      )
    )
  (:action move-left-from-r393
    :parameters ()
    :precondition 
      (in r393)
    :effect
      (and
        (in r392)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r393))
      )
    )
  (:action move-right-from-r394
    :parameters ()
    :precondition 
      (in r394)
    :effect
      (and
        (in r395)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r394))
      )
    )
  (:action move-left-from-r394
    :parameters ()
    :precondition 
      (in r394)
    :effect
      (and
        (in r393)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r394))
      )
    )
  (:action move-right-from-r395
    :parameters ()
    :precondition 
      (in r395)
    :effect
      (and
        (in r396)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r395))
      )
    )
  (:action move-left-from-r395
    :parameters ()
    :precondition 
      (in r395)
    :effect
      (and
        (in r394)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r395))
      )
    )
  (:action move-right-from-r396
    :parameters ()
    :precondition 
      (in r396)
    :effect
      (and
        (in r397)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r396))
      )
    )
  (:action move-left-from-r396
    :parameters ()
    :precondition 
      (in r396)
    :effect
      (and
        (in r395)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r396))
      )
    )
  (:action move-right-from-r397
    :parameters ()
    :precondition 
      (in r397)
    :effect
      (and
        (in r398)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r397))
      )
    )
  (:action move-left-from-r397
    :parameters ()
    :precondition 
      (in r397)
    :effect
      (and
        (in r396)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r397))
      )
    )
  (:action move-right-from-r398
    :parameters ()
    :precondition 
      (in r398)
    :effect
      (and
        (in r399)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r398))
      )
    )
  (:action move-left-from-r398
    :parameters ()
    :precondition 
      (in r398)
    :effect
      (and
        (in r397)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r398))
      )
    )
  (:action move-right-from-r399
    :parameters ()
    :precondition 
      (in r399)
    :effect
      (and
        (in r400)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r399))
      )
    )
  (:action move-left-from-r399
    :parameters ()
    :precondition 
      (in r399)
    :effect
      (and
        (in r398)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r399))
      )
    )
  (:action move-right-from-r400
    :parameters ()
    :precondition 
      (in r400)
    :effect
      (and
        (in r401)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r400))
      )
    )
  (:action move-left-from-r400
    :parameters ()
    :precondition 
      (in r400)
    :effect
      (and
        (in r399)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r400))
      )
    )
  (:action move-right-from-r401
    :parameters ()
    :precondition 
      (in r401)
    :effect
      (and
        (in r402)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r401))
      )
    )
  (:action move-left-from-r401
    :parameters ()
    :precondition 
      (in r401)
    :effect
      (and
        (in r400)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r401))
      )
    )
  (:action move-right-from-r402
    :parameters ()
    :precondition 
      (in r402)
    :effect
      (and
        (in r403)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r402))
      )
    )
  (:action move-left-from-r402
    :parameters ()
    :precondition 
      (in r402)
    :effect
      (and
        (in r401)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r402))
      )
    )
  (:action move-right-from-r403
    :parameters ()
    :precondition 
      (in r403)
    :effect
      (and
        (in r404)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r403))
      )
    )
  (:action move-left-from-r403
    :parameters ()
    :precondition 
      (in r403)
    :effect
      (and
        (in r402)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r403))
      )
    )
  (:action move-right-from-r404
    :parameters ()
    :precondition 
      (in r404)
    :effect
      (and
        (in r405)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r404))
      )
    )
  (:action move-left-from-r404
    :parameters ()
    :precondition 
      (in r404)
    :effect
      (and
        (in r403)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r404))
      )
    )
  (:action move-right-from-r405
    :parameters ()
    :precondition 
      (in r405)
    :effect
      (and
        (in r406)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r405))
      )
    )
  (:action move-left-from-r405
    :parameters ()
    :precondition 
      (in r405)
    :effect
      (and
        (in r404)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r405))
      )
    )
  (:action move-right-from-r406
    :parameters ()
    :precondition 
      (in r406)
    :effect
      (and
        (in r407)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r406))
      )
    )
  (:action move-left-from-r406
    :parameters ()
    :precondition 
      (in r406)
    :effect
      (and
        (in r405)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r406))
      )
    )
  (:action move-right-from-r407
    :parameters ()
    :precondition 
      (in r407)
    :effect
      (and
        (in r408)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r407))
      )
    )
  (:action move-left-from-r407
    :parameters ()
    :precondition 
      (in r407)
    :effect
      (and
        (in r406)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r407))
      )
    )
  (:action move-right-from-r408
    :parameters ()
    :precondition 
      (in r408)
    :effect
      (and
        (in r409)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r408))
      )
    )
  (:action move-left-from-r408
    :parameters ()
    :precondition 
      (in r408)
    :effect
      (and
        (in r407)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r408))
      )
    )
  (:action move-right-from-r409
    :parameters ()
    :precondition 
      (in r409)
    :effect
      (and
        (in r410)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r409))
      )
    )
  (:action move-left-from-r409
    :parameters ()
    :precondition 
      (in r409)
    :effect
      (and
        (in r408)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r409))
      )
    )
  (:action move-right-from-r410
    :parameters ()
    :precondition 
      (in r410)
    :effect
      (and
        (in r411)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r410))
      )
    )
  (:action move-left-from-r410
    :parameters ()
    :precondition 
      (in r410)
    :effect
      (and
        (in r409)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r410))
      )
    )
  (:action move-right-from-r411
    :parameters ()
    :precondition 
      (in r411)
    :effect
      (and
        (in r412)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r411))
      )
    )
  (:action move-left-from-r411
    :parameters ()
    :precondition 
      (in r411)
    :effect
      (and
        (in r410)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r411))
      )
    )
  (:action move-right-from-r412
    :parameters ()
    :precondition 
      (in r412)
    :effect
      (and
        (in r413)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r412))
      )
    )
  (:action move-left-from-r412
    :parameters ()
    :precondition 
      (in r412)
    :effect
      (and
        (in r411)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r412))
      )
    )
  (:action move-right-from-r413
    :parameters ()
    :precondition 
      (in r413)
    :effect
      (and
        (in r414)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r413))
      )
    )
  (:action move-left-from-r413
    :parameters ()
    :precondition 
      (in r413)
    :effect
      (and
        (in r412)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r413))
      )
    )
  (:action move-right-from-r414
    :parameters ()
    :precondition 
      (in r414)
    :effect
      (and
        (in r415)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r414))
      )
    )
  (:action move-left-from-r414
    :parameters ()
    :precondition 
      (in r414)
    :effect
      (and
        (in r413)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r414))
      )
    )
  (:action move-right-from-r415
    :parameters ()
    :precondition 
      (in r415)
    :effect
      (and
        (in r416)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r415))
      )
    )
  (:action move-left-from-r415
    :parameters ()
    :precondition 
      (in r415)
    :effect
      (and
        (in r414)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r415))
      )
    )
  (:action move-right-from-r416
    :parameters ()
    :precondition 
      (in r416)
    :effect
      (and
        (in r417)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r416))
      )
    )
  (:action move-left-from-r416
    :parameters ()
    :precondition 
      (in r416)
    :effect
      (and
        (in r415)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r416))
      )
    )
  (:action move-right-from-r417
    :parameters ()
    :precondition 
      (in r417)
    :effect
      (and
        (in r418)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r417))
      )
    )
  (:action move-left-from-r417
    :parameters ()
    :precondition 
      (in r417)
    :effect
      (and
        (in r416)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r417))
      )
    )
  (:action move-right-from-r418
    :parameters ()
    :precondition 
      (in r418)
    :effect
      (and
        (in r419)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r418))
      )
    )
  (:action move-left-from-r418
    :parameters ()
    :precondition 
      (in r418)
    :effect
      (and
        (in r417)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r418))
      )
    )
  (:action move-right-from-r419
    :parameters ()
    :precondition 
      (in r419)
    :effect
      (and
        (in r420)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r419))
      )
    )
  (:action move-left-from-r419
    :parameters ()
    :precondition 
      (in r419)
    :effect
      (and
        (in r418)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r419))
      )
    )
  (:action move-right-from-r420
    :parameters ()
    :precondition 
      (in r420)
    :effect
      (and
        (in r421)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r420))
      )
    )
  (:action move-left-from-r420
    :parameters ()
    :precondition 
      (in r420)
    :effect
      (and
        (in r419)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r420))
      )
    )
  (:action move-right-from-r421
    :parameters ()
    :precondition 
      (in r421)
    :effect
      (and
        (in r422)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r421))
      )
    )
  (:action move-left-from-r421
    :parameters ()
    :precondition 
      (in r421)
    :effect
      (and
        (in r420)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r421))
      )
    )
  (:action move-right-from-r422
    :parameters ()
    :precondition 
      (in r422)
    :effect
      (and
        (in r423)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r422))
      )
    )
  (:action move-left-from-r422
    :parameters ()
    :precondition 
      (in r422)
    :effect
      (and
        (in r421)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r422))
      )
    )
  (:action move-right-from-r423
    :parameters ()
    :precondition 
      (in r423)
    :effect
      (and
        (in r424)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r423))
      )
    )
  (:action move-left-from-r423
    :parameters ()
    :precondition 
      (in r423)
    :effect
      (and
        (in r422)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r423))
      )
    )
  (:action move-right-from-r424
    :parameters ()
    :precondition 
      (in r424)
    :effect
      (and
        (in r425)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r424))
      )
    )
  (:action move-left-from-r424
    :parameters ()
    :precondition 
      (in r424)
    :effect
      (and
        (in r423)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r424))
      )
    )
  (:action move-right-from-r425
    :parameters ()
    :precondition 
      (in r425)
    :effect
      (and
        (in r426)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r425))
      )
    )
  (:action move-left-from-r425
    :parameters ()
    :precondition 
      (in r425)
    :effect
      (and
        (in r424)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r425))
      )
    )
  (:action move-right-from-r426
    :parameters ()
    :precondition 
      (in r426)
    :effect
      (and
        (in r427)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r426))
      )
    )
  (:action move-left-from-r426
    :parameters ()
    :precondition 
      (in r426)
    :effect
      (and
        (in r425)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r426))
      )
    )
  (:action move-right-from-r427
    :parameters ()
    :precondition 
      (in r427)
    :effect
      (and
        (in r428)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r427))
      )
    )
  (:action move-left-from-r427
    :parameters ()
    :precondition 
      (in r427)
    :effect
      (and
        (in r426)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r427))
      )
    )
  (:action move-right-from-r428
    :parameters ()
    :precondition 
      (in r428)
    :effect
      (and
        (in r429)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r428))
      )
    )
  (:action move-left-from-r428
    :parameters ()
    :precondition 
      (in r428)
    :effect
      (and
        (in r427)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r428))
      )
    )
  (:action move-right-from-r429
    :parameters ()
    :precondition 
      (in r429)
    :effect
      (and
        (in r430)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r429))
      )
    )
  (:action move-left-from-r429
    :parameters ()
    :precondition 
      (in r429)
    :effect
      (and
        (in r428)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r429))
      )
    )
  (:action move-right-from-r430
    :parameters ()
    :precondition 
      (in r430)
    :effect
      (and
        (in r431)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r430))
      )
    )
  (:action move-left-from-r430
    :parameters ()
    :precondition 
      (in r430)
    :effect
      (and
        (in r429)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r430))
      )
    )
  (:action move-right-from-r431
    :parameters ()
    :precondition 
      (in r431)
    :effect
      (and
        (in r432)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r431))
      )
    )
  (:action move-left-from-r431
    :parameters ()
    :precondition 
      (in r431)
    :effect
      (and
        (in r430)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r431))
      )
    )
  (:action move-right-from-r432
    :parameters ()
    :precondition 
      (in r432)
    :effect
      (and
        (in r433)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r432))
      )
    )
  (:action move-left-from-r432
    :parameters ()
    :precondition 
      (in r432)
    :effect
      (and
        (in r431)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r432))
      )
    )
  (:action move-right-from-r433
    :parameters ()
    :precondition 
      (in r433)
    :effect
      (and
        (in r434)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r433))
      )
    )
  (:action move-left-from-r433
    :parameters ()
    :precondition 
      (in r433)
    :effect
      (and
        (in r432)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r433))
      )
    )
  (:action move-right-from-r434
    :parameters ()
    :precondition 
      (in r434)
    :effect
      (and
        (in r435)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r434))
      )
    )
  (:action move-left-from-r434
    :parameters ()
    :precondition 
      (in r434)
    :effect
      (and
        (in r433)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r434))
      )
    )
  (:action move-right-from-r435
    :parameters ()
    :precondition 
      (in r435)
    :effect
      (and
        (in r436)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r435))
      )
    )
  (:action move-left-from-r435
    :parameters ()
    :precondition 
      (in r435)
    :effect
      (and
        (in r434)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r435))
      )
    )
  (:action move-right-from-r436
    :parameters ()
    :precondition 
      (in r436)
    :effect
      (and
        (in r437)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r436))
      )
    )
  (:action move-left-from-r436
    :parameters ()
    :precondition 
      (in r436)
    :effect
      (and
        (in r435)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r436))
      )
    )
  (:action move-right-from-r437
    :parameters ()
    :precondition 
      (in r437)
    :effect
      (and
        (in r438)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r437))
      )
    )
  (:action move-left-from-r437
    :parameters ()
    :precondition 
      (in r437)
    :effect
      (and
        (in r436)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r437))
      )
    )
  (:action move-right-from-r438
    :parameters ()
    :precondition 
      (in r438)
    :effect
      (and
        (in r439)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r438))
      )
    )
  (:action move-left-from-r438
    :parameters ()
    :precondition 
      (in r438)
    :effect
      (and
        (in r437)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r438))
      )
    )
  (:action move-right-from-r439
    :parameters ()
    :precondition 
      (in r439)
    :effect
      (and
        (in r440)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r439))
      )
    )
  (:action move-left-from-r439
    :parameters ()
    :precondition 
      (in r439)
    :effect
      (and
        (in r438)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r439))
      )
    )
  (:action move-right-from-r440
    :parameters ()
    :precondition 
      (in r440)
    :effect
      (and
        (in r441)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r440))
      )
    )
  (:action move-left-from-r440
    :parameters ()
    :precondition 
      (in r440)
    :effect
      (and
        (in r439)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r440))
      )
    )
  (:action move-right-from-r441
    :parameters ()
    :precondition 
      (in r441)
    :effect
      (and
        (in r442)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r441))
      )
    )
  (:action move-left-from-r441
    :parameters ()
    :precondition 
      (in r441)
    :effect
      (and
        (in r440)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r441))
      )
    )
  (:action move-right-from-r442
    :parameters ()
    :precondition 
      (in r442)
    :effect
      (and
        (in r443)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r442))
      )
    )
  (:action move-left-from-r442
    :parameters ()
    :precondition 
      (in r442)
    :effect
      (and
        (in r441)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r442))
      )
    )
  (:action move-right-from-r443
    :parameters ()
    :precondition 
      (in r443)
    :effect
      (and
        (in r444)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r443))
      )
    )
  (:action move-left-from-r443
    :parameters ()
    :precondition 
      (in r443)
    :effect
      (and
        (in r442)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r443))
      )
    )
  (:action move-right-from-r444
    :parameters ()
    :precondition 
      (in r444)
    :effect
      (and
        (in r445)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r444))
      )
    )
  (:action move-left-from-r444
    :parameters ()
    :precondition 
      (in r444)
    :effect
      (and
        (in r443)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r444))
      )
    )
  (:action move-right-from-r445
    :parameters ()
    :precondition 
      (in r445)
    :effect
      (and
        (in r446)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r445))
      )
    )
  (:action move-left-from-r445
    :parameters ()
    :precondition 
      (in r445)
    :effect
      (and
        (in r444)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r445))
      )
    )
  (:action move-right-from-r446
    :parameters ()
    :precondition 
      (in r446)
    :effect
      (and
        (in r447)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r446))
      )
    )
  (:action move-left-from-r446
    :parameters ()
    :precondition 
      (in r446)
    :effect
      (and
        (in r445)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r446))
      )
    )
  (:action move-right-from-r447
    :parameters ()
    :precondition 
      (in r447)
    :effect
      (and
        (in r448)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r447))
      )
    )
  (:action move-left-from-r447
    :parameters ()
    :precondition 
      (in r447)
    :effect
      (and
        (in r446)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r447))
      )
    )
  (:action move-right-from-r448
    :parameters ()
    :precondition 
      (in r448)
    :effect
      (and
        (in r449)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r448))
      )
    )
  (:action move-left-from-r448
    :parameters ()
    :precondition 
      (in r448)
    :effect
      (and
        (in r447)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r448))
      )
    )
  (:action move-right-from-r449
    :parameters ()
    :precondition 
      (in r449)
    :effect
      (and
        (in r450)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r449))
      )
    )
  (:action move-left-from-r449
    :parameters ()
    :precondition 
      (in r449)
    :effect
      (and
        (in r448)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r449))
      )
    )
  (:action move-right-from-r450
    :parameters ()
    :precondition 
      (in r450)
    :effect
      (and
        (in r451)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r450))
      )
    )
  (:action move-left-from-r450
    :parameters ()
    :precondition 
      (in r450)
    :effect
      (and
        (in r449)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r450))
      )
    )
  (:action move-right-from-r451
    :parameters ()
    :precondition 
      (in r451)
    :effect
      (and
        (in r452)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r451))
      )
    )
  (:action move-left-from-r451
    :parameters ()
    :precondition 
      (in r451)
    :effect
      (and
        (in r450)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r451))
      )
    )
  (:action move-right-from-r452
    :parameters ()
    :precondition 
      (in r452)
    :effect
      (and
        (in r453)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r452))
      )
    )
  (:action move-left-from-r452
    :parameters ()
    :precondition 
      (in r452)
    :effect
      (and
        (in r451)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r452))
      )
    )
  (:action move-right-from-r453
    :parameters ()
    :precondition 
      (in r453)
    :effect
      (and
        (in r454)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r453))
      )
    )
  (:action move-left-from-r453
    :parameters ()
    :precondition 
      (in r453)
    :effect
      (and
        (in r452)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r453))
      )
    )
  (:action move-right-from-r454
    :parameters ()
    :precondition 
      (in r454)
    :effect
      (and
        (in r455)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r454))
      )
    )
  (:action move-left-from-r454
    :parameters ()
    :precondition 
      (in r454)
    :effect
      (and
        (in r453)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r454))
      )
    )
  (:action move-right-from-r455
    :parameters ()
    :precondition 
      (in r455)
    :effect
      (and
        (in r456)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r455))
      )
    )
  (:action move-left-from-r455
    :parameters ()
    :precondition 
      (in r455)
    :effect
      (and
        (in r454)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r455))
      )
    )
  (:action move-right-from-r456
    :parameters ()
    :precondition 
      (in r456)
    :effect
      (and
        (in r457)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r456))
      )
    )
  (:action move-left-from-r456
    :parameters ()
    :precondition 
      (in r456)
    :effect
      (and
        (in r455)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r456))
      )
    )
  (:action move-right-from-r457
    :parameters ()
    :precondition 
      (in r457)
    :effect
      (and
        (in r458)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r457))
      )
    )
  (:action move-left-from-r457
    :parameters ()
    :precondition 
      (in r457)
    :effect
      (and
        (in r456)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r457))
      )
    )
  (:action move-right-from-r458
    :parameters ()
    :precondition 
      (in r458)
    :effect
      (and
        (in r459)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r458))
      )
    )
  (:action move-left-from-r458
    :parameters ()
    :precondition 
      (in r458)
    :effect
      (and
        (in r457)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r458))
      )
    )
  (:action move-right-from-r459
    :parameters ()
    :precondition 
      (in r459)
    :effect
      (and
        (in r460)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r459))
      )
    )
  (:action move-left-from-r459
    :parameters ()
    :precondition 
      (in r459)
    :effect
      (and
        (in r458)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r459))
      )
    )
  (:action move-right-from-r460
    :parameters ()
    :precondition 
      (in r460)
    :effect
      (and
        (in r461)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r460))
      )
    )
  (:action move-left-from-r460
    :parameters ()
    :precondition 
      (in r460)
    :effect
      (and
        (in r459)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r460))
      )
    )
  (:action move-right-from-r461
    :parameters ()
    :precondition 
      (in r461)
    :effect
      (and
        (in r462)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r461))
      )
    )
  (:action move-left-from-r461
    :parameters ()
    :precondition 
      (in r461)
    :effect
      (and
        (in r460)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r461))
      )
    )
  (:action move-right-from-r462
    :parameters ()
    :precondition 
      (in r462)
    :effect
      (and
        (in r463)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r462))
      )
    )
  (:action move-left-from-r462
    :parameters ()
    :precondition 
      (in r462)
    :effect
      (and
        (in r461)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r462))
      )
    )
  (:action move-right-from-r463
    :parameters ()
    :precondition 
      (in r463)
    :effect
      (and
        (in r464)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r463))
      )
    )
  (:action move-left-from-r463
    :parameters ()
    :precondition 
      (in r463)
    :effect
      (and
        (in r462)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r463))
      )
    )
  (:action move-right-from-r464
    :parameters ()
    :precondition 
      (in r464)
    :effect
      (and
        (in r465)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r464))
      )
    )
  (:action move-left-from-r464
    :parameters ()
    :precondition 
      (in r464)
    :effect
      (and
        (in r463)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r464))
      )
    )
  (:action move-right-from-r465
    :parameters ()
    :precondition 
      (in r465)
    :effect
      (and
        (in r466)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r465))
      )
    )
  (:action move-left-from-r465
    :parameters ()
    :precondition 
      (in r465)
    :effect
      (and
        (in r464)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r465))
      )
    )
  (:action move-right-from-r466
    :parameters ()
    :precondition 
      (in r466)
    :effect
      (and
        (in r467)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r466))
      )
    )
  (:action move-left-from-r466
    :parameters ()
    :precondition 
      (in r466)
    :effect
      (and
        (in r465)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r466))
      )
    )
  (:action move-right-from-r467
    :parameters ()
    :precondition 
      (in r467)
    :effect
      (and
        (in r468)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r467))
      )
    )
  (:action move-left-from-r467
    :parameters ()
    :precondition 
      (in r467)
    :effect
      (and
        (in r466)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r467))
      )
    )
  (:action move-right-from-r468
    :parameters ()
    :precondition 
      (in r468)
    :effect
      (and
        (in r469)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r468))
      )
    )
  (:action move-left-from-r468
    :parameters ()
    :precondition 
      (in r468)
    :effect
      (and
        (in r467)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r468))
      )
    )
  (:action move-right-from-r469
    :parameters ()
    :precondition 
      (in r469)
    :effect
      (and
        (in r470)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r469))
      )
    )
  (:action move-left-from-r469
    :parameters ()
    :precondition 
      (in r469)
    :effect
      (and
        (in r468)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r469))
      )
    )
  (:action move-right-from-r470
    :parameters ()
    :precondition 
      (in r470)
    :effect
      (and
        (in r471)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r470))
      )
    )
  (:action move-left-from-r470
    :parameters ()
    :precondition 
      (in r470)
    :effect
      (and
        (in r469)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r470))
      )
    )
  (:action move-right-from-r471
    :parameters ()
    :precondition 
      (in r471)
    :effect
      (and
        (in r472)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r471))
      )
    )
  (:action move-left-from-r471
    :parameters ()
    :precondition 
      (in r471)
    :effect
      (and
        (in r470)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r471))
      )
    )
  (:action move-right-from-r472
    :parameters ()
    :precondition 
      (in r472)
    :effect
      (and
        (in r473)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r472))
      )
    )
  (:action move-left-from-r472
    :parameters ()
    :precondition 
      (in r472)
    :effect
      (and
        (in r471)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r472))
      )
    )
  (:action move-right-from-r473
    :parameters ()
    :precondition 
      (in r473)
    :effect
      (and
        (in r474)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r473))
      )
    )
  (:action move-left-from-r473
    :parameters ()
    :precondition 
      (in r473)
    :effect
      (and
        (in r472)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r473))
      )
    )
  (:action move-right-from-r474
    :parameters ()
    :precondition 
      (in r474)
    :effect
      (and
        (in r475)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r474))
      )
    )
  (:action move-left-from-r474
    :parameters ()
    :precondition 
      (in r474)
    :effect
      (and
        (in r473)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r474))
      )
    )
  (:action move-right-from-r475
    :parameters ()
    :precondition 
      (in r475)
    :effect
      (and
        (in r476)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r475))
      )
    )
  (:action move-left-from-r475
    :parameters ()
    :precondition 
      (in r475)
    :effect
      (and
        (in r474)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r475))
      )
    )
  (:action move-right-from-r476
    :parameters ()
    :precondition 
      (in r476)
    :effect
      (and
        (in r477)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r476))
      )
    )
  (:action move-left-from-r476
    :parameters ()
    :precondition 
      (in r476)
    :effect
      (and
        (in r475)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r476))
      )
    )
  (:action move-right-from-r477
    :parameters ()
    :precondition 
      (in r477)
    :effect
      (and
        (in r478)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r477))
      )
    )
  (:action move-left-from-r477
    :parameters ()
    :precondition 
      (in r477)
    :effect
      (and
        (in r476)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r477))
      )
    )
  (:action move-right-from-r478
    :parameters ()
    :precondition 
      (in r478)
    :effect
      (and
        (in r479)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r478))
      )
    )
  (:action move-left-from-r478
    :parameters ()
    :precondition 
      (in r478)
    :effect
      (and
        (in r477)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r478))
      )
    )
  (:action move-right-from-r479
    :parameters ()
    :precondition 
      (in r479)
    :effect
      (and
        (in r480)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r479))
      )
    )
  (:action move-left-from-r479
    :parameters ()
    :precondition 
      (in r479)
    :effect
      (and
        (in r478)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r479))
      )
    )
  (:action move-right-from-r480
    :parameters ()
    :precondition 
      (in r480)
    :effect
      (and
        (in r481)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r480))
      )
    )
  (:action move-left-from-r480
    :parameters ()
    :precondition 
      (in r480)
    :effect
      (and
        (in r479)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r480))
      )
    )
  (:action move-right-from-r481
    :parameters ()
    :precondition 
      (in r481)
    :effect
      (and
        (in r482)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r481))
      )
    )
  (:action move-left-from-r481
    :parameters ()
    :precondition 
      (in r481)
    :effect
      (and
        (in r480)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r481))
      )
    )
  (:action move-right-from-r482
    :parameters ()
    :precondition 
      (in r482)
    :effect
      (and
        (in r483)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r482))
      )
    )
  (:action move-left-from-r482
    :parameters ()
    :precondition 
      (in r482)
    :effect
      (and
        (in r481)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r482))
      )
    )
  (:action move-right-from-r483
    :parameters ()
    :precondition 
      (in r483)
    :effect
      (and
        (in r484)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r483))
      )
    )
  (:action move-left-from-r483
    :parameters ()
    :precondition 
      (in r483)
    :effect
      (and
        (in r482)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r483))
      )
    )
  (:action move-right-from-r484
    :parameters ()
    :precondition 
      (in r484)
    :effect
      (and
        (in r485)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r484))
      )
    )
  (:action move-left-from-r484
    :parameters ()
    :precondition 
      (in r484)
    :effect
      (and
        (in r483)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r484))
      )
    )
  (:action move-right-from-r485
    :parameters ()
    :precondition 
      (in r485)
    :effect
      (and
        (in r486)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r485))
      )
    )
  (:action move-left-from-r485
    :parameters ()
    :precondition 
      (in r485)
    :effect
      (and
        (in r484)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r485))
      )
    )
  (:action move-right-from-r486
    :parameters ()
    :precondition 
      (in r486)
    :effect
      (and
        (in r487)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r486))
      )
    )
  (:action move-left-from-r486
    :parameters ()
    :precondition 
      (in r486)
    :effect
      (and
        (in r485)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r486))
      )
    )
  (:action move-right-from-r487
    :parameters ()
    :precondition 
      (in r487)
    :effect
      (and
        (in r488)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r487))
      )
    )
  (:action move-left-from-r487
    :parameters ()
    :precondition 
      (in r487)
    :effect
      (and
        (in r486)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r487))
      )
    )
  (:action move-right-from-r488
    :parameters ()
    :precondition 
      (in r488)
    :effect
      (and
        (in r489)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r488))
      )
    )
  (:action move-left-from-r488
    :parameters ()
    :precondition 
      (in r488)
    :effect
      (and
        (in r487)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r488))
      )
    )
  (:action move-right-from-r489
    :parameters ()
    :precondition 
      (in r489)
    :effect
      (and
        (in r490)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r489))
      )
    )
  (:action move-left-from-r489
    :parameters ()
    :precondition 
      (in r489)
    :effect
      (and
        (in r488)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r489))
      )
    )
  (:action move-right-from-r490
    :parameters ()
    :precondition 
      (in r490)
    :effect
      (and
        (in r491)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r490))
      )
    )
  (:action move-left-from-r490
    :parameters ()
    :precondition 
      (in r490)
    :effect
      (and
        (in r489)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r490))
      )
    )
  (:action move-right-from-r491
    :parameters ()
    :precondition 
      (in r491)
    :effect
      (and
        (in r492)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r491))
      )
    )
  (:action move-left-from-r491
    :parameters ()
    :precondition 
      (in r491)
    :effect
      (and
        (in r490)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r491))
      )
    )
  (:action move-right-from-r492
    :parameters ()
    :precondition 
      (in r492)
    :effect
      (and
        (in r493)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r492))
      )
    )
  (:action move-left-from-r492
    :parameters ()
    :precondition 
      (in r492)
    :effect
      (and
        (in r491)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r492))
      )
    )
  (:action move-right-from-r493
    :parameters ()
    :precondition 
      (in r493)
    :effect
      (and
        (in r494)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r493))
      )
    )
  (:action move-left-from-r493
    :parameters ()
    :precondition 
      (in r493)
    :effect
      (and
        (in r492)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r493))
      )
    )
  (:action move-right-from-r494
    :parameters ()
    :precondition 
      (in r494)
    :effect
      (and
        (in r495)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r494))
      )
    )
  (:action move-left-from-r494
    :parameters ()
    :precondition 
      (in r494)
    :effect
      (and
        (in r493)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r494))
      )
    )
  (:action move-right-from-r495
    :parameters ()
    :precondition 
      (in r495)
    :effect
      (and
        (in r496)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r495))
      )
    )
  (:action move-left-from-r495
    :parameters ()
    :precondition 
      (in r495)
    :effect
      (and
        (in r494)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r495))
      )
    )
  (:action move-right-from-r496
    :parameters ()
    :precondition 
      (in r496)
    :effect
      (and
        (in r497)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r496))
      )
    )
  (:action move-left-from-r496
    :parameters ()
    :precondition 
      (in r496)
    :effect
      (and
        (in r495)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r496))
      )
    )
  (:action move-right-from-r497
    :parameters ()
    :precondition 
      (in r497)
    :effect
      (and
        (in r498)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r497))
      )
    )
  (:action move-left-from-r497
    :parameters ()
    :precondition 
      (in r497)
    :effect
      (and
        (in r496)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r497))
      )
    )
  (:action move-right-from-r498
    :parameters ()
    :precondition 
      (in r498)
    :effect
      (and
        (in r499)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r498))
      )
    )
  (:action move-left-from-r498
    :parameters ()
    :precondition 
      (in r498)
    :effect
      (and
        (in r497)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r498))
      )
    )
  (:action move-right-from-r499_detdup_0
    :parameters ()
    :precondition 
      (in r499)
    :effect
      (and
        (in r500)
        (not 
          (in r499))
        (not 
          (seen))
      )
    )
  (:action move-right-from-r499_detdup_1
    :parameters ()
    :precondition 
      (in r499)
    :effect
      (and
        (in r500)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r499))
      )
    )
  (:action move-left-from-r499
    :parameters ()
    :precondition 
      (in r499)
    :effect
      (and
        (in r498)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r499))
      )
    )
  (:action move-right-from-r500
    :parameters ()
    :precondition 
      (and
        (in r500)
        (not 
          (seen)))
    :effect
      (and
        (in r501)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r500))
      )
    )
  (:action move-left-from-r500
    :parameters ()
    :precondition 
      (and
        (in r500)
        (not 
          (seen)))
    :effect
      (and
        (in r499)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r500))
      )
    )
  (:action move-right-from-r501
    :parameters ()
    :precondition 
      (in r501)
    :effect
      (and
        (in r502)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r501))
      )
    )
  (:action move-left-from-r501_detdup_0
    :parameters ()
    :precondition 
      (in r501)
    :effect
      (and
        (in r500)
        (not 
          (in r501))
        (not 
          (seen))
      )
    )
  (:action move-left-from-r501_detdup_1
    :parameters ()
    :precondition 
      (in r501)
    :effect
      (and
        (in r500)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r501))
      )
    )
  (:action move-right-from-r502
    :parameters ()
    :precondition 
      (in r502)
    :effect
      (and
        (in r503)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r502))
      )
    )
  (:action move-left-from-r502
    :parameters ()
    :precondition 
      (in r502)
    :effect
      (and
        (in r501)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r502))
      )
    )
  (:action move-right-from-r503
    :parameters ()
    :precondition 
      (in r503)
    :effect
      (and
        (in r504)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r503))
      )
    )
  (:action move-left-from-r503
    :parameters ()
    :precondition 
      (in r503)
    :effect
      (and
        (in r502)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r503))
      )
    )
  (:action move-right-from-r504
    :parameters ()
    :precondition 
      (in r504)
    :effect
      (and
        (in r505)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r504))
      )
    )
  (:action move-left-from-r504
    :parameters ()
    :precondition 
      (in r504)
    :effect
      (and
        (in r503)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r504))
      )
    )
  (:action move-right-from-r505
    :parameters ()
    :precondition 
      (in r505)
    :effect
      (and
        (in r506)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r505))
      )
    )
  (:action move-left-from-r505
    :parameters ()
    :precondition 
      (in r505)
    :effect
      (and
        (in r504)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r505))
      )
    )
  (:action move-right-from-r506
    :parameters ()
    :precondition 
      (in r506)
    :effect
      (and
        (in r507)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r506))
      )
    )
  (:action move-left-from-r506
    :parameters ()
    :precondition 
      (in r506)
    :effect
      (and
        (in r505)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r506))
      )
    )
  (:action move-right-from-r507
    :parameters ()
    :precondition 
      (in r507)
    :effect
      (and
        (in r508)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r507))
      )
    )
  (:action move-left-from-r507
    :parameters ()
    :precondition 
      (in r507)
    :effect
      (and
        (in r506)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r507))
      )
    )
  (:action move-right-from-r508
    :parameters ()
    :precondition 
      (in r508)
    :effect
      (and
        (in r509)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r508))
      )
    )
  (:action move-left-from-r508
    :parameters ()
    :precondition 
      (in r508)
    :effect
      (and
        (in r507)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r508))
      )
    )
  (:action move-right-from-r509
    :parameters ()
    :precondition 
      (in r509)
    :effect
      (and
        (in r510)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r509))
      )
    )
  (:action move-left-from-r509
    :parameters ()
    :precondition 
      (in r509)
    :effect
      (and
        (in r508)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r509))
      )
    )
  (:action move-right-from-r510
    :parameters ()
    :precondition 
      (in r510)
    :effect
      (and
        (in r511)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r510))
      )
    )
  (:action move-left-from-r510
    :parameters ()
    :precondition 
      (in r510)
    :effect
      (and
        (in r509)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r510))
      )
    )
  (:action move-right-from-r511
    :parameters ()
    :precondition 
      (in r511)
    :effect
      (and
        (in r512)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r511))
      )
    )
  (:action move-left-from-r511
    :parameters ()
    :precondition 
      (in r511)
    :effect
      (and
        (in r510)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r511))
      )
    )
  (:action move-right-from-r512
    :parameters ()
    :precondition 
      (in r512)
    :effect
      (and
        (in r513)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r512))
      )
    )
  (:action move-left-from-r512
    :parameters ()
    :precondition 
      (in r512)
    :effect
      (and
        (in r511)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r512))
      )
    )
  (:action move-right-from-r513
    :parameters ()
    :precondition 
      (in r513)
    :effect
      (and
        (in r514)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r513))
      )
    )
  (:action move-left-from-r513
    :parameters ()
    :precondition 
      (in r513)
    :effect
      (and
        (in r512)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r513))
      )
    )
  (:action move-right-from-r514
    :parameters ()
    :precondition 
      (in r514)
    :effect
      (and
        (in r515)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r514))
      )
    )
  (:action move-left-from-r514
    :parameters ()
    :precondition 
      (in r514)
    :effect
      (and
        (in r513)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r514))
      )
    )
  (:action move-right-from-r515
    :parameters ()
    :precondition 
      (in r515)
    :effect
      (and
        (in r516)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r515))
      )
    )
  (:action move-left-from-r515
    :parameters ()
    :precondition 
      (in r515)
    :effect
      (and
        (in r514)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r515))
      )
    )
  (:action move-right-from-r516
    :parameters ()
    :precondition 
      (in r516)
    :effect
      (and
        (in r517)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r516))
      )
    )
  (:action move-left-from-r516
    :parameters ()
    :precondition 
      (in r516)
    :effect
      (and
        (in r515)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r516))
      )
    )
  (:action move-right-from-r517
    :parameters ()
    :precondition 
      (in r517)
    :effect
      (and
        (in r518)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r517))
      )
    )
  (:action move-left-from-r517
    :parameters ()
    :precondition 
      (in r517)
    :effect
      (and
        (in r516)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r517))
      )
    )
  (:action move-right-from-r518
    :parameters ()
    :precondition 
      (in r518)
    :effect
      (and
        (in r519)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r518))
      )
    )
  (:action move-left-from-r518
    :parameters ()
    :precondition 
      (in r518)
    :effect
      (and
        (in r517)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r518))
      )
    )
  (:action move-right-from-r519
    :parameters ()
    :precondition 
      (in r519)
    :effect
      (and
        (in r520)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r519))
      )
    )
  (:action move-left-from-r519
    :parameters ()
    :precondition 
      (in r519)
    :effect
      (and
        (in r518)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r519))
      )
    )
  (:action move-right-from-r520
    :parameters ()
    :precondition 
      (in r520)
    :effect
      (and
        (in r521)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r520))
      )
    )
  (:action move-left-from-r520
    :parameters ()
    :precondition 
      (in r520)
    :effect
      (and
        (in r519)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r520))
      )
    )
  (:action move-right-from-r521
    :parameters ()
    :precondition 
      (in r521)
    :effect
      (and
        (in r522)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r521))
      )
    )
  (:action move-left-from-r521
    :parameters ()
    :precondition 
      (in r521)
    :effect
      (and
        (in r520)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r521))
      )
    )
  (:action move-right-from-r522
    :parameters ()
    :precondition 
      (in r522)
    :effect
      (and
        (in r523)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r522))
      )
    )
  (:action move-left-from-r522
    :parameters ()
    :precondition 
      (in r522)
    :effect
      (and
        (in r521)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r522))
      )
    )
  (:action move-right-from-r523
    :parameters ()
    :precondition 
      (in r523)
    :effect
      (and
        (in r524)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r523))
      )
    )
  (:action move-left-from-r523
    :parameters ()
    :precondition 
      (in r523)
    :effect
      (and
        (in r522)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r523))
      )
    )
  (:action move-right-from-r524
    :parameters ()
    :precondition 
      (in r524)
    :effect
      (and
        (in r525)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r524))
      )
    )
  (:action move-left-from-r524
    :parameters ()
    :precondition 
      (in r524)
    :effect
      (and
        (in r523)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r524))
      )
    )
  (:action move-right-from-r525
    :parameters ()
    :precondition 
      (in r525)
    :effect
      (and
        (in r526)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r525))
      )
    )
  (:action move-left-from-r525
    :parameters ()
    :precondition 
      (in r525)
    :effect
      (and
        (in r524)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r525))
      )
    )
  (:action move-right-from-r526
    :parameters ()
    :precondition 
      (in r526)
    :effect
      (and
        (in r527)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r526))
      )
    )
  (:action move-left-from-r526
    :parameters ()
    :precondition 
      (in r526)
    :effect
      (and
        (in r525)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r526))
      )
    )
  (:action move-right-from-r527
    :parameters ()
    :precondition 
      (in r527)
    :effect
      (and
        (in r528)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r527))
      )
    )
  (:action move-left-from-r527
    :parameters ()
    :precondition 
      (in r527)
    :effect
      (and
        (in r526)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r527))
      )
    )
  (:action move-right-from-r528
    :parameters ()
    :precondition 
      (in r528)
    :effect
      (and
        (in r529)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r528))
      )
    )
  (:action move-left-from-r528
    :parameters ()
    :precondition 
      (in r528)
    :effect
      (and
        (in r527)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r528))
      )
    )
  (:action move-right-from-r529
    :parameters ()
    :precondition 
      (in r529)
    :effect
      (and
        (in r530)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r529))
      )
    )
  (:action move-left-from-r529
    :parameters ()
    :precondition 
      (in r529)
    :effect
      (and
        (in r528)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r529))
      )
    )
  (:action move-right-from-r530
    :parameters ()
    :precondition 
      (in r530)
    :effect
      (and
        (in r531)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r530))
      )
    )
  (:action move-left-from-r530
    :parameters ()
    :precondition 
      (in r530)
    :effect
      (and
        (in r529)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r530))
      )
    )
  (:action move-right-from-r531
    :parameters ()
    :precondition 
      (in r531)
    :effect
      (and
        (in r532)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r531))
      )
    )
  (:action move-left-from-r531
    :parameters ()
    :precondition 
      (in r531)
    :effect
      (and
        (in r530)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r531))
      )
    )
  (:action move-right-from-r532
    :parameters ()
    :precondition 
      (in r532)
    :effect
      (and
        (in r533)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r532))
      )
    )
  (:action move-left-from-r532
    :parameters ()
    :precondition 
      (in r532)
    :effect
      (and
        (in r531)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r532))
      )
    )
  (:action move-right-from-r533
    :parameters ()
    :precondition 
      (in r533)
    :effect
      (and
        (in r534)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r533))
      )
    )
  (:action move-left-from-r533
    :parameters ()
    :precondition 
      (in r533)
    :effect
      (and
        (in r532)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r533))
      )
    )
  (:action move-right-from-r534
    :parameters ()
    :precondition 
      (in r534)
    :effect
      (and
        (in r535)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r534))
      )
    )
  (:action move-left-from-r534
    :parameters ()
    :precondition 
      (in r534)
    :effect
      (and
        (in r533)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r534))
      )
    )
  (:action move-right-from-r535
    :parameters ()
    :precondition 
      (in r535)
    :effect
      (and
        (in r536)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r535))
      )
    )
  (:action move-left-from-r535
    :parameters ()
    :precondition 
      (in r535)
    :effect
      (and
        (in r534)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r535))
      )
    )
  (:action move-right-from-r536
    :parameters ()
    :precondition 
      (in r536)
    :effect
      (and
        (in r537)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r536))
      )
    )
  (:action move-left-from-r536
    :parameters ()
    :precondition 
      (in r536)
    :effect
      (and
        (in r535)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r536))
      )
    )
  (:action move-right-from-r537
    :parameters ()
    :precondition 
      (in r537)
    :effect
      (and
        (in r538)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r537))
      )
    )
  (:action move-left-from-r537
    :parameters ()
    :precondition 
      (in r537)
    :effect
      (and
        (in r536)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r537))
      )
    )
  (:action move-right-from-r538
    :parameters ()
    :precondition 
      (in r538)
    :effect
      (and
        (in r539)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r538))
      )
    )
  (:action move-left-from-r538
    :parameters ()
    :precondition 
      (in r538)
    :effect
      (and
        (in r537)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r538))
      )
    )
  (:action move-right-from-r539
    :parameters ()
    :precondition 
      (in r539)
    :effect
      (and
        (in r540)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r539))
      )
    )
  (:action move-left-from-r539
    :parameters ()
    :precondition 
      (in r539)
    :effect
      (and
        (in r538)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r539))
      )
    )
  (:action move-right-from-r540
    :parameters ()
    :precondition 
      (in r540)
    :effect
      (and
        (in r541)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r540))
      )
    )
  (:action move-left-from-r540
    :parameters ()
    :precondition 
      (in r540)
    :effect
      (and
        (in r539)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r540))
      )
    )
  (:action move-right-from-r541
    :parameters ()
    :precondition 
      (in r541)
    :effect
      (and
        (in r542)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r541))
      )
    )
  (:action move-left-from-r541
    :parameters ()
    :precondition 
      (in r541)
    :effect
      (and
        (in r540)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r541))
      )
    )
  (:action move-right-from-r542
    :parameters ()
    :precondition 
      (in r542)
    :effect
      (and
        (in r543)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r542))
      )
    )
  (:action move-left-from-r542
    :parameters ()
    :precondition 
      (in r542)
    :effect
      (and
        (in r541)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r542))
      )
    )
  (:action move-right-from-r543
    :parameters ()
    :precondition 
      (in r543)
    :effect
      (and
        (in r544)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r543))
      )
    )
  (:action move-left-from-r543
    :parameters ()
    :precondition 
      (in r543)
    :effect
      (and
        (in r542)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r543))
      )
    )
  (:action move-right-from-r544
    :parameters ()
    :precondition 
      (in r544)
    :effect
      (and
        (in r545)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r544))
      )
    )
  (:action move-left-from-r544
    :parameters ()
    :precondition 
      (in r544)
    :effect
      (and
        (in r543)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r544))
      )
    )
  (:action move-right-from-r545
    :parameters ()
    :precondition 
      (in r545)
    :effect
      (and
        (in r546)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r545))
      )
    )
  (:action move-left-from-r545
    :parameters ()
    :precondition 
      (in r545)
    :effect
      (and
        (in r544)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r545))
      )
    )
  (:action move-right-from-r546
    :parameters ()
    :precondition 
      (in r546)
    :effect
      (and
        (in r547)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r546))
      )
    )
  (:action move-left-from-r546
    :parameters ()
    :precondition 
      (in r546)
    :effect
      (and
        (in r545)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r546))
      )
    )
  (:action move-right-from-r547
    :parameters ()
    :precondition 
      (in r547)
    :effect
      (and
        (in r548)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r547))
      )
    )
  (:action move-left-from-r547
    :parameters ()
    :precondition 
      (in r547)
    :effect
      (and
        (in r546)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r547))
      )
    )
  (:action move-right-from-r548
    :parameters ()
    :precondition 
      (in r548)
    :effect
      (and
        (in r549)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r548))
      )
    )
  (:action move-left-from-r548
    :parameters ()
    :precondition 
      (in r548)
    :effect
      (and
        (in r547)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r548))
      )
    )
  (:action move-right-from-r549
    :parameters ()
    :precondition 
      (in r549)
    :effect
      (and
        (in r550)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r549))
      )
    )
  (:action move-left-from-r549
    :parameters ()
    :precondition 
      (in r549)
    :effect
      (and
        (in r548)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r549))
      )
    )
  (:action move-right-from-r550
    :parameters ()
    :precondition 
      (in r550)
    :effect
      (and
        (in r551)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r550))
      )
    )
  (:action move-left-from-r550
    :parameters ()
    :precondition 
      (in r550)
    :effect
      (and
        (in r549)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r550))
      )
    )
  (:action move-right-from-r551
    :parameters ()
    :precondition 
      (in r551)
    :effect
      (and
        (in r552)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r551))
      )
    )
  (:action move-left-from-r551
    :parameters ()
    :precondition 
      (in r551)
    :effect
      (and
        (in r550)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r551))
      )
    )
  (:action move-right-from-r552
    :parameters ()
    :precondition 
      (in r552)
    :effect
      (and
        (in r553)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r552))
      )
    )
  (:action move-left-from-r552
    :parameters ()
    :precondition 
      (in r552)
    :effect
      (and
        (in r551)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r552))
      )
    )
  (:action move-right-from-r553
    :parameters ()
    :precondition 
      (in r553)
    :effect
      (and
        (in r554)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r553))
      )
    )
  (:action move-left-from-r553
    :parameters ()
    :precondition 
      (in r553)
    :effect
      (and
        (in r552)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r553))
      )
    )
  (:action move-right-from-r554
    :parameters ()
    :precondition 
      (in r554)
    :effect
      (and
        (in r555)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r554))
      )
    )
  (:action move-left-from-r554
    :parameters ()
    :precondition 
      (in r554)
    :effect
      (and
        (in r553)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r554))
      )
    )
  (:action move-right-from-r555
    :parameters ()
    :precondition 
      (in r555)
    :effect
      (and
        (in r556)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r555))
      )
    )
  (:action move-left-from-r555
    :parameters ()
    :precondition 
      (in r555)
    :effect
      (and
        (in r554)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r555))
      )
    )
  (:action move-right-from-r556
    :parameters ()
    :precondition 
      (in r556)
    :effect
      (and
        (in r557)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r556))
      )
    )
  (:action move-left-from-r556
    :parameters ()
    :precondition 
      (in r556)
    :effect
      (and
        (in r555)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r556))
      )
    )
  (:action move-right-from-r557
    :parameters ()
    :precondition 
      (in r557)
    :effect
      (and
        (in r558)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r557))
      )
    )
  (:action move-left-from-r557
    :parameters ()
    :precondition 
      (in r557)
    :effect
      (and
        (in r556)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r557))
      )
    )
  (:action move-right-from-r558
    :parameters ()
    :precondition 
      (in r558)
    :effect
      (and
        (in r559)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r558))
      )
    )
  (:action move-left-from-r558
    :parameters ()
    :precondition 
      (in r558)
    :effect
      (and
        (in r557)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r558))
      )
    )
  (:action move-right-from-r559
    :parameters ()
    :precondition 
      (in r559)
    :effect
      (and
        (in r560)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r559))
      )
    )
  (:action move-left-from-r559
    :parameters ()
    :precondition 
      (in r559)
    :effect
      (and
        (in r558)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r559))
      )
    )
  (:action move-right-from-r560
    :parameters ()
    :precondition 
      (in r560)
    :effect
      (and
        (in r561)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r560))
      )
    )
  (:action move-left-from-r560
    :parameters ()
    :precondition 
      (in r560)
    :effect
      (and
        (in r559)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r560))
      )
    )
  (:action move-right-from-r561
    :parameters ()
    :precondition 
      (in r561)
    :effect
      (and
        (in r562)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r561))
      )
    )
  (:action move-left-from-r561
    :parameters ()
    :precondition 
      (in r561)
    :effect
      (and
        (in r560)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r561))
      )
    )
  (:action move-right-from-r562
    :parameters ()
    :precondition 
      (in r562)
    :effect
      (and
        (in r563)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r562))
      )
    )
  (:action move-left-from-r562
    :parameters ()
    :precondition 
      (in r562)
    :effect
      (and
        (in r561)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r562))
      )
    )
  (:action move-right-from-r563
    :parameters ()
    :precondition 
      (in r563)
    :effect
      (and
        (in r564)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r563))
      )
    )
  (:action move-left-from-r563
    :parameters ()
    :precondition 
      (in r563)
    :effect
      (and
        (in r562)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r563))
      )
    )
  (:action move-right-from-r564
    :parameters ()
    :precondition 
      (in r564)
    :effect
      (and
        (in r565)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r564))
      )
    )
  (:action move-left-from-r564
    :parameters ()
    :precondition 
      (in r564)
    :effect
      (and
        (in r563)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r564))
      )
    )
  (:action move-right-from-r565
    :parameters ()
    :precondition 
      (in r565)
    :effect
      (and
        (in r566)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r565))
      )
    )
  (:action move-left-from-r565
    :parameters ()
    :precondition 
      (in r565)
    :effect
      (and
        (in r564)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r565))
      )
    )
  (:action move-right-from-r566
    :parameters ()
    :precondition 
      (in r566)
    :effect
      (and
        (in r567)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r566))
      )
    )
  (:action move-left-from-r566
    :parameters ()
    :precondition 
      (in r566)
    :effect
      (and
        (in r565)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r566))
      )
    )
  (:action move-right-from-r567
    :parameters ()
    :precondition 
      (in r567)
    :effect
      (and
        (in r568)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r567))
      )
    )
  (:action move-left-from-r567
    :parameters ()
    :precondition 
      (in r567)
    :effect
      (and
        (in r566)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r567))
      )
    )
  (:action move-right-from-r568
    :parameters ()
    :precondition 
      (in r568)
    :effect
      (and
        (in r569)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r568))
      )
    )
  (:action move-left-from-r568
    :parameters ()
    :precondition 
      (in r568)
    :effect
      (and
        (in r567)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r568))
      )
    )
  (:action move-right-from-r569
    :parameters ()
    :precondition 
      (in r569)
    :effect
      (and
        (in r570)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r569))
      )
    )
  (:action move-left-from-r569
    :parameters ()
    :precondition 
      (in r569)
    :effect
      (and
        (in r568)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r569))
      )
    )
  (:action move-right-from-r570
    :parameters ()
    :precondition 
      (in r570)
    :effect
      (and
        (in r571)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r570))
      )
    )
  (:action move-left-from-r570
    :parameters ()
    :precondition 
      (in r570)
    :effect
      (and
        (in r569)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r570))
      )
    )
  (:action move-right-from-r571
    :parameters ()
    :precondition 
      (in r571)
    :effect
      (and
        (in r572)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r571))
      )
    )
  (:action move-left-from-r571
    :parameters ()
    :precondition 
      (in r571)
    :effect
      (and
        (in r570)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r571))
      )
    )
  (:action move-right-from-r572
    :parameters ()
    :precondition 
      (in r572)
    :effect
      (and
        (in r573)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r572))
      )
    )
  (:action move-left-from-r572
    :parameters ()
    :precondition 
      (in r572)
    :effect
      (and
        (in r571)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r572))
      )
    )
  (:action move-right-from-r573
    :parameters ()
    :precondition 
      (in r573)
    :effect
      (and
        (in r574)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r573))
      )
    )
  (:action move-left-from-r573
    :parameters ()
    :precondition 
      (in r573)
    :effect
      (and
        (in r572)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r573))
      )
    )
  (:action move-right-from-r574
    :parameters ()
    :precondition 
      (in r574)
    :effect
      (and
        (in r575)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r574))
      )
    )
  (:action move-left-from-r574
    :parameters ()
    :precondition 
      (in r574)
    :effect
      (and
        (in r573)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r574))
      )
    )
  (:action move-right-from-r575
    :parameters ()
    :precondition 
      (in r575)
    :effect
      (and
        (in r576)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r575))
      )
    )
  (:action move-left-from-r575
    :parameters ()
    :precondition 
      (in r575)
    :effect
      (and
        (in r574)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r575))
      )
    )
  (:action move-right-from-r576
    :parameters ()
    :precondition 
      (in r576)
    :effect
      (and
        (in r577)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r576))
      )
    )
  (:action move-left-from-r576
    :parameters ()
    :precondition 
      (in r576)
    :effect
      (and
        (in r575)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r576))
      )
    )
  (:action move-right-from-r577
    :parameters ()
    :precondition 
      (in r577)
    :effect
      (and
        (in r578)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r577))
      )
    )
  (:action move-left-from-r577
    :parameters ()
    :precondition 
      (in r577)
    :effect
      (and
        (in r576)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r577))
      )
    )
  (:action move-right-from-r578
    :parameters ()
    :precondition 
      (in r578)
    :effect
      (and
        (in r579)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r578))
      )
    )
  (:action move-left-from-r578
    :parameters ()
    :precondition 
      (in r578)
    :effect
      (and
        (in r577)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r578))
      )
    )
  (:action move-right-from-r579
    :parameters ()
    :precondition 
      (in r579)
    :effect
      (and
        (in r580)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r579))
      )
    )
  (:action move-left-from-r579
    :parameters ()
    :precondition 
      (in r579)
    :effect
      (and
        (in r578)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r579))
      )
    )
  (:action move-right-from-r580
    :parameters ()
    :precondition 
      (in r580)
    :effect
      (and
        (in r581)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r580))
      )
    )
  (:action move-left-from-r580
    :parameters ()
    :precondition 
      (in r580)
    :effect
      (and
        (in r579)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r580))
      )
    )
  (:action move-right-from-r581
    :parameters ()
    :precondition 
      (in r581)
    :effect
      (and
        (in r582)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r581))
      )
    )
  (:action move-left-from-r581
    :parameters ()
    :precondition 
      (in r581)
    :effect
      (and
        (in r580)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r581))
      )
    )
  (:action move-right-from-r582
    :parameters ()
    :precondition 
      (in r582)
    :effect
      (and
        (in r583)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r582))
      )
    )
  (:action move-left-from-r582
    :parameters ()
    :precondition 
      (in r582)
    :effect
      (and
        (in r581)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r582))
      )
    )
  (:action move-right-from-r583
    :parameters ()
    :precondition 
      (in r583)
    :effect
      (and
        (in r584)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r583))
      )
    )
  (:action move-left-from-r583
    :parameters ()
    :precondition 
      (in r583)
    :effect
      (and
        (in r582)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r583))
      )
    )
  (:action move-right-from-r584
    :parameters ()
    :precondition 
      (in r584)
    :effect
      (and
        (in r585)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r584))
      )
    )
  (:action move-left-from-r584
    :parameters ()
    :precondition 
      (in r584)
    :effect
      (and
        (in r583)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r584))
      )
    )
  (:action move-right-from-r585
    :parameters ()
    :precondition 
      (in r585)
    :effect
      (and
        (in r586)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r585))
      )
    )
  (:action move-left-from-r585
    :parameters ()
    :precondition 
      (in r585)
    :effect
      (and
        (in r584)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r585))
      )
    )
  (:action move-right-from-r586
    :parameters ()
    :precondition 
      (in r586)
    :effect
      (and
        (in r587)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r586))
      )
    )
  (:action move-left-from-r586
    :parameters ()
    :precondition 
      (in r586)
    :effect
      (and
        (in r585)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r586))
      )
    )
  (:action move-right-from-r587
    :parameters ()
    :precondition 
      (in r587)
    :effect
      (and
        (in r588)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r587))
      )
    )
  (:action move-left-from-r587
    :parameters ()
    :precondition 
      (in r587)
    :effect
      (and
        (in r586)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r587))
      )
    )
  (:action move-right-from-r588
    :parameters ()
    :precondition 
      (in r588)
    :effect
      (and
        (in r589)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r588))
      )
    )
  (:action move-left-from-r588
    :parameters ()
    :precondition 
      (in r588)
    :effect
      (and
        (in r587)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r588))
      )
    )
  (:action move-right-from-r589
    :parameters ()
    :precondition 
      (in r589)
    :effect
      (and
        (in r590)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r589))
      )
    )
  (:action move-left-from-r589
    :parameters ()
    :precondition 
      (in r589)
    :effect
      (and
        (in r588)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r589))
      )
    )
  (:action move-right-from-r590
    :parameters ()
    :precondition 
      (in r590)
    :effect
      (and
        (in r591)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r590))
      )
    )
  (:action move-left-from-r590
    :parameters ()
    :precondition 
      (in r590)
    :effect
      (and
        (in r589)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r590))
      )
    )
  (:action move-right-from-r591
    :parameters ()
    :precondition 
      (in r591)
    :effect
      (and
        (in r592)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r591))
      )
    )
  (:action move-left-from-r591
    :parameters ()
    :precondition 
      (in r591)
    :effect
      (and
        (in r590)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r591))
      )
    )
  (:action move-right-from-r592
    :parameters ()
    :precondition 
      (in r592)
    :effect
      (and
        (in r593)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r592))
      )
    )
  (:action move-left-from-r592
    :parameters ()
    :precondition 
      (in r592)
    :effect
      (and
        (in r591)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r592))
      )
    )
  (:action move-right-from-r593
    :parameters ()
    :precondition 
      (in r593)
    :effect
      (and
        (in r594)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r593))
      )
    )
  (:action move-left-from-r593
    :parameters ()
    :precondition 
      (in r593)
    :effect
      (and
        (in r592)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r593))
      )
    )
  (:action move-right-from-r594
    :parameters ()
    :precondition 
      (in r594)
    :effect
      (and
        (in r595)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r594))
      )
    )
  (:action move-left-from-r594
    :parameters ()
    :precondition 
      (in r594)
    :effect
      (and
        (in r593)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r594))
      )
    )
  (:action move-right-from-r595
    :parameters ()
    :precondition 
      (in r595)
    :effect
      (and
        (in r596)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r595))
      )
    )
  (:action move-left-from-r595
    :parameters ()
    :precondition 
      (in r595)
    :effect
      (and
        (in r594)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r595))
      )
    )
  (:action move-right-from-r596
    :parameters ()
    :precondition 
      (in r596)
    :effect
      (and
        (in r597)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r596))
      )
    )
  (:action move-left-from-r596
    :parameters ()
    :precondition 
      (in r596)
    :effect
      (and
        (in r595)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r596))
      )
    )
  (:action move-right-from-r597
    :parameters ()
    :precondition 
      (in r597)
    :effect
      (and
        (in r598)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r597))
      )
    )
  (:action move-left-from-r597
    :parameters ()
    :precondition 
      (in r597)
    :effect
      (and
        (in r596)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r597))
      )
    )
  (:action move-right-from-r598
    :parameters ()
    :precondition 
      (in r598)
    :effect
      (and
        (in r599)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r598))
      )
    )
  (:action move-left-from-r598
    :parameters ()
    :precondition 
      (in r598)
    :effect
      (and
        (in r597)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r598))
      )
    )
  (:action move-right-from-r599
    :parameters ()
    :precondition 
      (in r599)
    :effect
      (and
        (in r600)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r599))
      )
    )
  (:action move-left-from-r599
    :parameters ()
    :precondition 
      (in r599)
    :effect
      (and
        (in r598)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r599))
      )
    )
  (:action move-right-from-r600
    :parameters ()
    :precondition 
      (in r600)
    :effect
      (and
        (in r601)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r600))
      )
    )
  (:action move-left-from-r600
    :parameters ()
    :precondition 
      (in r600)
    :effect
      (and
        (in r599)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r600))
      )
    )
  (:action move-right-from-r601
    :parameters ()
    :precondition 
      (in r601)
    :effect
      (and
        (in r602)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r601))
      )
    )
  (:action move-left-from-r601
    :parameters ()
    :precondition 
      (in r601)
    :effect
      (and
        (in r600)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r601))
      )
    )
  (:action move-right-from-r602
    :parameters ()
    :precondition 
      (in r602)
    :effect
      (and
        (in r603)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r602))
      )
    )
  (:action move-left-from-r602
    :parameters ()
    :precondition 
      (in r602)
    :effect
      (and
        (in r601)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r602))
      )
    )
  (:action move-right-from-r603
    :parameters ()
    :precondition 
      (in r603)
    :effect
      (and
        (in r604)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r603))
      )
    )
  (:action move-left-from-r603
    :parameters ()
    :precondition 
      (in r603)
    :effect
      (and
        (in r602)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r603))
      )
    )
  (:action move-right-from-r604
    :parameters ()
    :precondition 
      (in r604)
    :effect
      (and
        (in r605)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r604))
      )
    )
  (:action move-left-from-r604
    :parameters ()
    :precondition 
      (in r604)
    :effect
      (and
        (in r603)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r604))
      )
    )
  (:action move-right-from-r605
    :parameters ()
    :precondition 
      (in r605)
    :effect
      (and
        (in r606)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r605))
      )
    )
  (:action move-left-from-r605
    :parameters ()
    :precondition 
      (in r605)
    :effect
      (and
        (in r604)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r605))
      )
    )
  (:action move-right-from-r606
    :parameters ()
    :precondition 
      (in r606)
    :effect
      (and
        (in r607)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r606))
      )
    )
  (:action move-left-from-r606
    :parameters ()
    :precondition 
      (in r606)
    :effect
      (and
        (in r605)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r606))
      )
    )
  (:action move-right-from-r607
    :parameters ()
    :precondition 
      (in r607)
    :effect
      (and
        (in r608)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r607))
      )
    )
  (:action move-left-from-r607
    :parameters ()
    :precondition 
      (in r607)
    :effect
      (and
        (in r606)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r607))
      )
    )
  (:action move-right-from-r608
    :parameters ()
    :precondition 
      (in r608)
    :effect
      (and
        (in r609)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r608))
      )
    )
  (:action move-left-from-r608
    :parameters ()
    :precondition 
      (in r608)
    :effect
      (and
        (in r607)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r608))
      )
    )
  (:action move-right-from-r609
    :parameters ()
    :precondition 
      (in r609)
    :effect
      (and
        (in r610)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r609))
      )
    )
  (:action move-left-from-r609
    :parameters ()
    :precondition 
      (in r609)
    :effect
      (and
        (in r608)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r609))
      )
    )
  (:action move-right-from-r610
    :parameters ()
    :precondition 
      (in r610)
    :effect
      (and
        (in r611)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r610))
      )
    )
  (:action move-left-from-r610
    :parameters ()
    :precondition 
      (in r610)
    :effect
      (and
        (in r609)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r610))
      )
    )
  (:action move-right-from-r611
    :parameters ()
    :precondition 
      (in r611)
    :effect
      (and
        (in r612)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r611))
      )
    )
  (:action move-left-from-r611
    :parameters ()
    :precondition 
      (in r611)
    :effect
      (and
        (in r610)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r611))
      )
    )
  (:action move-right-from-r612
    :parameters ()
    :precondition 
      (in r612)
    :effect
      (and
        (in r613)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r612))
      )
    )
  (:action move-left-from-r612
    :parameters ()
    :precondition 
      (in r612)
    :effect
      (and
        (in r611)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r612))
      )
    )
  (:action move-right-from-r613
    :parameters ()
    :precondition 
      (in r613)
    :effect
      (and
        (in r614)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r613))
      )
    )
  (:action move-left-from-r613
    :parameters ()
    :precondition 
      (in r613)
    :effect
      (and
        (in r612)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r613))
      )
    )
  (:action move-right-from-r614
    :parameters ()
    :precondition 
      (in r614)
    :effect
      (and
        (in r615)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r614))
      )
    )
  (:action move-left-from-r614
    :parameters ()
    :precondition 
      (in r614)
    :effect
      (and
        (in r613)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r614))
      )
    )
  (:action move-right-from-r615
    :parameters ()
    :precondition 
      (in r615)
    :effect
      (and
        (in r616)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r615))
      )
    )
  (:action move-left-from-r615
    :parameters ()
    :precondition 
      (in r615)
    :effect
      (and
        (in r614)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r615))
      )
    )
  (:action move-right-from-r616
    :parameters ()
    :precondition 
      (in r616)
    :effect
      (and
        (in r617)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r616))
      )
    )
  (:action move-left-from-r616
    :parameters ()
    :precondition 
      (in r616)
    :effect
      (and
        (in r615)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r616))
      )
    )
  (:action move-right-from-r617
    :parameters ()
    :precondition 
      (in r617)
    :effect
      (and
        (in r618)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r617))
      )
    )
  (:action move-left-from-r617
    :parameters ()
    :precondition 
      (in r617)
    :effect
      (and
        (in r616)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r617))
      )
    )
  (:action move-right-from-r618
    :parameters ()
    :precondition 
      (in r618)
    :effect
      (and
        (in r619)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r618))
      )
    )
  (:action move-left-from-r618
    :parameters ()
    :precondition 
      (in r618)
    :effect
      (and
        (in r617)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r618))
      )
    )
  (:action move-right-from-r619
    :parameters ()
    :precondition 
      (in r619)
    :effect
      (and
        (in r620)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r619))
      )
    )
  (:action move-left-from-r619
    :parameters ()
    :precondition 
      (in r619)
    :effect
      (and
        (in r618)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r619))
      )
    )
  (:action move-right-from-r620
    :parameters ()
    :precondition 
      (in r620)
    :effect
      (and
        (in r621)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r620))
      )
    )
  (:action move-left-from-r620
    :parameters ()
    :precondition 
      (in r620)
    :effect
      (and
        (in r619)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r620))
      )
    )
  (:action move-right-from-r621
    :parameters ()
    :precondition 
      (in r621)
    :effect
      (and
        (in r622)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r621))
      )
    )
  (:action move-left-from-r621
    :parameters ()
    :precondition 
      (in r621)
    :effect
      (and
        (in r620)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r621))
      )
    )
  (:action move-right-from-r622
    :parameters ()
    :precondition 
      (in r622)
    :effect
      (and
        (in r623)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r622))
      )
    )
  (:action move-left-from-r622
    :parameters ()
    :precondition 
      (in r622)
    :effect
      (and
        (in r621)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r622))
      )
    )
  (:action move-right-from-r623
    :parameters ()
    :precondition 
      (in r623)
    :effect
      (and
        (in r624)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r623))
      )
    )
  (:action move-left-from-r623
    :parameters ()
    :precondition 
      (in r623)
    :effect
      (and
        (in r622)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r623))
      )
    )
  (:action move-right-from-r624
    :parameters ()
    :precondition 
      (in r624)
    :effect
      (and
        (in r625)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r624))
      )
    )
  (:action move-left-from-r624
    :parameters ()
    :precondition 
      (in r624)
    :effect
      (and
        (in r623)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r624))
      )
    )
  (:action move-right-from-r625
    :parameters ()
    :precondition 
      (in r625)
    :effect
      (and
        (in r626)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r625))
      )
    )
  (:action move-left-from-r625
    :parameters ()
    :precondition 
      (in r625)
    :effect
      (and
        (in r624)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r625))
      )
    )
  (:action move-right-from-r626
    :parameters ()
    :precondition 
      (in r626)
    :effect
      (and
        (in r627)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r626))
      )
    )
  (:action move-left-from-r626
    :parameters ()
    :precondition 
      (in r626)
    :effect
      (and
        (in r625)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r626))
      )
    )
  (:action move-right-from-r627
    :parameters ()
    :precondition 
      (in r627)
    :effect
      (and
        (in r628)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r627))
      )
    )
  (:action move-left-from-r627
    :parameters ()
    :precondition 
      (in r627)
    :effect
      (and
        (in r626)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r627))
      )
    )
  (:action move-right-from-r628
    :parameters ()
    :precondition 
      (in r628)
    :effect
      (and
        (in r629)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r628))
      )
    )
  (:action move-left-from-r628
    :parameters ()
    :precondition 
      (in r628)
    :effect
      (and
        (in r627)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r628))
      )
    )
  (:action move-right-from-r629
    :parameters ()
    :precondition 
      (in r629)
    :effect
      (and
        (in r630)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r629))
      )
    )
  (:action move-left-from-r629
    :parameters ()
    :precondition 
      (in r629)
    :effect
      (and
        (in r628)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r629))
      )
    )
  (:action move-right-from-r630
    :parameters ()
    :precondition 
      (in r630)
    :effect
      (and
        (in r631)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r630))
      )
    )
  (:action move-left-from-r630
    :parameters ()
    :precondition 
      (in r630)
    :effect
      (and
        (in r629)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r630))
      )
    )
  (:action move-right-from-r631
    :parameters ()
    :precondition 
      (in r631)
    :effect
      (and
        (in r632)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r631))
      )
    )
  (:action move-left-from-r631
    :parameters ()
    :precondition 
      (in r631)
    :effect
      (and
        (in r630)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r631))
      )
    )
  (:action move-right-from-r632
    :parameters ()
    :precondition 
      (in r632)
    :effect
      (and
        (in r633)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r632))
      )
    )
  (:action move-left-from-r632
    :parameters ()
    :precondition 
      (in r632)
    :effect
      (and
        (in r631)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r632))
      )
    )
  (:action move-right-from-r633
    :parameters ()
    :precondition 
      (in r633)
    :effect
      (and
        (in r634)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r633))
      )
    )
  (:action move-left-from-r633
    :parameters ()
    :precondition 
      (in r633)
    :effect
      (and
        (in r632)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r633))
      )
    )
  (:action move-right-from-r634
    :parameters ()
    :precondition 
      (in r634)
    :effect
      (and
        (in r635)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r634))
      )
    )
  (:action move-left-from-r634
    :parameters ()
    :precondition 
      (in r634)
    :effect
      (and
        (in r633)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r634))
      )
    )
  (:action move-right-from-r635
    :parameters ()
    :precondition 
      (in r635)
    :effect
      (and
        (in r636)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r635))
      )
    )
  (:action move-left-from-r635
    :parameters ()
    :precondition 
      (in r635)
    :effect
      (and
        (in r634)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r635))
      )
    )
  (:action move-right-from-r636
    :parameters ()
    :precondition 
      (in r636)
    :effect
      (and
        (in r637)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r636))
      )
    )
  (:action move-left-from-r636
    :parameters ()
    :precondition 
      (in r636)
    :effect
      (and
        (in r635)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r636))
      )
    )
  (:action move-right-from-r637
    :parameters ()
    :precondition 
      (in r637)
    :effect
      (and
        (in r638)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r637))
      )
    )
  (:action move-left-from-r637
    :parameters ()
    :precondition 
      (in r637)
    :effect
      (and
        (in r636)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r637))
      )
    )
  (:action move-right-from-r638
    :parameters ()
    :precondition 
      (in r638)
    :effect
      (and
        (in r639)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r638))
      )
    )
  (:action move-left-from-r638
    :parameters ()
    :precondition 
      (in r638)
    :effect
      (and
        (in r637)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r638))
      )
    )
  (:action move-right-from-r639
    :parameters ()
    :precondition 
      (in r639)
    :effect
      (and
        (in r640)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r639))
      )
    )
  (:action move-left-from-r639
    :parameters ()
    :precondition 
      (in r639)
    :effect
      (and
        (in r638)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r639))
      )
    )
  (:action move-right-from-r640
    :parameters ()
    :precondition 
      (in r640)
    :effect
      (and
        (in r641)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r640))
      )
    )
  (:action move-left-from-r640
    :parameters ()
    :precondition 
      (in r640)
    :effect
      (and
        (in r639)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r640))
      )
    )
  (:action move-right-from-r641
    :parameters ()
    :precondition 
      (in r641)
    :effect
      (and
        (in r642)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r641))
      )
    )
  (:action move-left-from-r641
    :parameters ()
    :precondition 
      (in r641)
    :effect
      (and
        (in r640)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r641))
      )
    )
  (:action move-right-from-r642
    :parameters ()
    :precondition 
      (in r642)
    :effect
      (and
        (in r643)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r642))
      )
    )
  (:action move-left-from-r642
    :parameters ()
    :precondition 
      (in r642)
    :effect
      (and
        (in r641)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r642))
      )
    )
  (:action move-right-from-r643
    :parameters ()
    :precondition 
      (in r643)
    :effect
      (and
        (in r644)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r643))
      )
    )
  (:action move-left-from-r643
    :parameters ()
    :precondition 
      (in r643)
    :effect
      (and
        (in r642)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r643))
      )
    )
  (:action move-right-from-r644
    :parameters ()
    :precondition 
      (in r644)
    :effect
      (and
        (in r645)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r644))
      )
    )
  (:action move-left-from-r644
    :parameters ()
    :precondition 
      (in r644)
    :effect
      (and
        (in r643)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r644))
      )
    )
  (:action move-right-from-r645
    :parameters ()
    :precondition 
      (in r645)
    :effect
      (and
        (in r646)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r645))
      )
    )
  (:action move-left-from-r645
    :parameters ()
    :precondition 
      (in r645)
    :effect
      (and
        (in r644)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r645))
      )
    )
  (:action move-right-from-r646
    :parameters ()
    :precondition 
      (in r646)
    :effect
      (and
        (in r647)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r646))
      )
    )
  (:action move-left-from-r646
    :parameters ()
    :precondition 
      (in r646)
    :effect
      (and
        (in r645)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r646))
      )
    )
  (:action move-right-from-r647
    :parameters ()
    :precondition 
      (in r647)
    :effect
      (and
        (in r648)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r647))
      )
    )
  (:action move-left-from-r647
    :parameters ()
    :precondition 
      (in r647)
    :effect
      (and
        (in r646)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r647))
      )
    )
  (:action move-right-from-r648
    :parameters ()
    :precondition 
      (in r648)
    :effect
      (and
        (in r649)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r648))
      )
    )
  (:action move-left-from-r648
    :parameters ()
    :precondition 
      (in r648)
    :effect
      (and
        (in r647)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r648))
      )
    )
  (:action move-right-from-r649
    :parameters ()
    :precondition 
      (in r649)
    :effect
      (and
        (in r650)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r649))
      )
    )
  (:action move-left-from-r649
    :parameters ()
    :precondition 
      (in r649)
    :effect
      (and
        (in r648)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r649))
      )
    )
  (:action move-right-from-r650
    :parameters ()
    :precondition 
      (in r650)
    :effect
      (and
        (in r651)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r650))
      )
    )
  (:action move-left-from-r650
    :parameters ()
    :precondition 
      (in r650)
    :effect
      (and
        (in r649)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r650))
      )
    )
  (:action move-right-from-r651
    :parameters ()
    :precondition 
      (in r651)
    :effect
      (and
        (in r652)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r651))
      )
    )
  (:action move-left-from-r651
    :parameters ()
    :precondition 
      (in r651)
    :effect
      (and
        (in r650)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r651))
      )
    )
  (:action move-right-from-r652
    :parameters ()
    :precondition 
      (in r652)
    :effect
      (and
        (in r653)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r652))
      )
    )
  (:action move-left-from-r652
    :parameters ()
    :precondition 
      (in r652)
    :effect
      (and
        (in r651)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r652))
      )
    )
  (:action move-right-from-r653
    :parameters ()
    :precondition 
      (in r653)
    :effect
      (and
        (in r654)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r653))
      )
    )
  (:action move-left-from-r653
    :parameters ()
    :precondition 
      (in r653)
    :effect
      (and
        (in r652)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r653))
      )
    )
  (:action move-right-from-r654
    :parameters ()
    :precondition 
      (in r654)
    :effect
      (and
        (in r655)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r654))
      )
    )
  (:action move-left-from-r654
    :parameters ()
    :precondition 
      (in r654)
    :effect
      (and
        (in r653)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r654))
      )
    )
  (:action move-right-from-r655
    :parameters ()
    :precondition 
      (in r655)
    :effect
      (and
        (in r656)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r655))
      )
    )
  (:action move-left-from-r655
    :parameters ()
    :precondition 
      (in r655)
    :effect
      (and
        (in r654)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r655))
      )
    )
  (:action move-right-from-r656
    :parameters ()
    :precondition 
      (in r656)
    :effect
      (and
        (in r657)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r656))
      )
    )
  (:action move-left-from-r656
    :parameters ()
    :precondition 
      (in r656)
    :effect
      (and
        (in r655)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r656))
      )
    )
  (:action move-right-from-r657
    :parameters ()
    :precondition 
      (in r657)
    :effect
      (and
        (in r658)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r657))
      )
    )
  (:action move-left-from-r657
    :parameters ()
    :precondition 
      (in r657)
    :effect
      (and
        (in r656)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r657))
      )
    )
  (:action move-right-from-r658
    :parameters ()
    :precondition 
      (in r658)
    :effect
      (and
        (in r659)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r658))
      )
    )
  (:action move-left-from-r658
    :parameters ()
    :precondition 
      (in r658)
    :effect
      (and
        (in r657)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r658))
      )
    )
  (:action move-right-from-r659
    :parameters ()
    :precondition 
      (in r659)
    :effect
      (and
        (in r660)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r659))
      )
    )
  (:action move-left-from-r659
    :parameters ()
    :precondition 
      (in r659)
    :effect
      (and
        (in r658)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r659))
      )
    )
  (:action move-right-from-r660
    :parameters ()
    :precondition 
      (in r660)
    :effect
      (and
        (in r661)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r660))
      )
    )
  (:action move-left-from-r660
    :parameters ()
    :precondition 
      (in r660)
    :effect
      (and
        (in r659)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r660))
      )
    )
  (:action move-right-from-r661
    :parameters ()
    :precondition 
      (in r661)
    :effect
      (and
        (in r662)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r661))
      )
    )
  (:action move-left-from-r661
    :parameters ()
    :precondition 
      (in r661)
    :effect
      (and
        (in r660)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r661))
      )
    )
  (:action move-right-from-r662
    :parameters ()
    :precondition 
      (in r662)
    :effect
      (and
        (in r663)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r662))
      )
    )
  (:action move-left-from-r662
    :parameters ()
    :precondition 
      (in r662)
    :effect
      (and
        (in r661)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r662))
      )
    )
  (:action move-right-from-r663
    :parameters ()
    :precondition 
      (in r663)
    :effect
      (and
        (in r664)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r663))
      )
    )
  (:action move-left-from-r663
    :parameters ()
    :precondition 
      (in r663)
    :effect
      (and
        (in r662)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r663))
      )
    )
  (:action move-right-from-r664
    :parameters ()
    :precondition 
      (in r664)
    :effect
      (and
        (in r665)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r664))
      )
    )
  (:action move-left-from-r664
    :parameters ()
    :precondition 
      (in r664)
    :effect
      (and
        (in r663)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r664))
      )
    )
  (:action move-right-from-r665
    :parameters ()
    :precondition 
      (in r665)
    :effect
      (and
        (in r666)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r665))
      )
    )
  (:action move-left-from-r665
    :parameters ()
    :precondition 
      (in r665)
    :effect
      (and
        (in r664)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r665))
      )
    )
  (:action move-right-from-r666
    :parameters ()
    :precondition 
      (in r666)
    :effect
      (and
        (in r667)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r666))
      )
    )
  (:action move-left-from-r666
    :parameters ()
    :precondition 
      (in r666)
    :effect
      (and
        (in r665)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r666))
      )
    )
  (:action move-right-from-r667
    :parameters ()
    :precondition 
      (in r667)
    :effect
      (and
        (in r668)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r667))
      )
    )
  (:action move-left-from-r667
    :parameters ()
    :precondition 
      (in r667)
    :effect
      (and
        (in r666)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r667))
      )
    )
  (:action move-right-from-r668
    :parameters ()
    :precondition 
      (in r668)
    :effect
      (and
        (in r669)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r668))
      )
    )
  (:action move-left-from-r668
    :parameters ()
    :precondition 
      (in r668)
    :effect
      (and
        (in r667)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r668))
      )
    )
  (:action move-right-from-r669
    :parameters ()
    :precondition 
      (in r669)
    :effect
      (and
        (in r670)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r669))
      )
    )
  (:action move-left-from-r669
    :parameters ()
    :precondition 
      (in r669)
    :effect
      (and
        (in r668)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r669))
      )
    )
  (:action move-right-from-r670
    :parameters ()
    :precondition 
      (in r670)
    :effect
      (and
        (in r671)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r670))
      )
    )
  (:action move-left-from-r670
    :parameters ()
    :precondition 
      (in r670)
    :effect
      (and
        (in r669)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r670))
      )
    )
  (:action move-right-from-r671
    :parameters ()
    :precondition 
      (in r671)
    :effect
      (and
        (in r672)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r671))
      )
    )
  (:action move-left-from-r671
    :parameters ()
    :precondition 
      (in r671)
    :effect
      (and
        (in r670)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r671))
      )
    )
  (:action move-right-from-r672
    :parameters ()
    :precondition 
      (in r672)
    :effect
      (and
        (in r673)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r672))
      )
    )
  (:action move-left-from-r672
    :parameters ()
    :precondition 
      (in r672)
    :effect
      (and
        (in r671)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r672))
      )
    )
  (:action move-right-from-r673
    :parameters ()
    :precondition 
      (in r673)
    :effect
      (and
        (in r674)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r673))
      )
    )
  (:action move-left-from-r673
    :parameters ()
    :precondition 
      (in r673)
    :effect
      (and
        (in r672)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r673))
      )
    )
  (:action move-right-from-r674
    :parameters ()
    :precondition 
      (in r674)
    :effect
      (and
        (in r675)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r674))
      )
    )
  (:action move-left-from-r674
    :parameters ()
    :precondition 
      (in r674)
    :effect
      (and
        (in r673)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r674))
      )
    )
  (:action move-right-from-r675
    :parameters ()
    :precondition 
      (in r675)
    :effect
      (and
        (in r676)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r675))
      )
    )
  (:action move-left-from-r675
    :parameters ()
    :precondition 
      (in r675)
    :effect
      (and
        (in r674)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r675))
      )
    )
  (:action move-right-from-r676
    :parameters ()
    :precondition 
      (in r676)
    :effect
      (and
        (in r677)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r676))
      )
    )
  (:action move-left-from-r676
    :parameters ()
    :precondition 
      (in r676)
    :effect
      (and
        (in r675)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r676))
      )
    )
  (:action move-right-from-r677
    :parameters ()
    :precondition 
      (in r677)
    :effect
      (and
        (in r678)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r677))
      )
    )
  (:action move-left-from-r677
    :parameters ()
    :precondition 
      (in r677)
    :effect
      (and
        (in r676)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r677))
      )
    )
  (:action move-right-from-r678
    :parameters ()
    :precondition 
      (in r678)
    :effect
      (and
        (in r679)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r678))
      )
    )
  (:action move-left-from-r678
    :parameters ()
    :precondition 
      (in r678)
    :effect
      (and
        (in r677)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r678))
      )
    )
  (:action move-right-from-r679
    :parameters ()
    :precondition 
      (in r679)
    :effect
      (and
        (in r680)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r679))
      )
    )
  (:action move-left-from-r679
    :parameters ()
    :precondition 
      (in r679)
    :effect
      (and
        (in r678)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r679))
      )
    )
  (:action move-right-from-r680
    :parameters ()
    :precondition 
      (in r680)
    :effect
      (and
        (in r681)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r680))
      )
    )
  (:action move-left-from-r680
    :parameters ()
    :precondition 
      (in r680)
    :effect
      (and
        (in r679)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r680))
      )
    )
  (:action move-right-from-r681
    :parameters ()
    :precondition 
      (in r681)
    :effect
      (and
        (in r682)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r681))
      )
    )
  (:action move-left-from-r681
    :parameters ()
    :precondition 
      (in r681)
    :effect
      (and
        (in r680)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r681))
      )
    )
  (:action move-right-from-r682
    :parameters ()
    :precondition 
      (in r682)
    :effect
      (and
        (in r683)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r682))
      )
    )
  (:action move-left-from-r682
    :parameters ()
    :precondition 
      (in r682)
    :effect
      (and
        (in r681)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r682))
      )
    )
  (:action move-right-from-r683
    :parameters ()
    :precondition 
      (in r683)
    :effect
      (and
        (in r684)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r683))
      )
    )
  (:action move-left-from-r683
    :parameters ()
    :precondition 
      (in r683)
    :effect
      (and
        (in r682)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r683))
      )
    )
  (:action move-right-from-r684
    :parameters ()
    :precondition 
      (in r684)
    :effect
      (and
        (in r685)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r684))
      )
    )
  (:action move-left-from-r684
    :parameters ()
    :precondition 
      (in r684)
    :effect
      (and
        (in r683)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r684))
      )
    )
  (:action move-right-from-r685
    :parameters ()
    :precondition 
      (in r685)
    :effect
      (and
        (in r686)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r685))
      )
    )
  (:action move-left-from-r685
    :parameters ()
    :precondition 
      (in r685)
    :effect
      (and
        (in r684)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r685))
      )
    )
  (:action move-right-from-r686
    :parameters ()
    :precondition 
      (in r686)
    :effect
      (and
        (in r687)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r686))
      )
    )
  (:action move-left-from-r686
    :parameters ()
    :precondition 
      (in r686)
    :effect
      (and
        (in r685)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r686))
      )
    )
  (:action move-right-from-r687
    :parameters ()
    :precondition 
      (in r687)
    :effect
      (and
        (in r688)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r687))
      )
    )
  (:action move-left-from-r687
    :parameters ()
    :precondition 
      (in r687)
    :effect
      (and
        (in r686)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r687))
      )
    )
  (:action move-right-from-r688
    :parameters ()
    :precondition 
      (in r688)
    :effect
      (and
        (in r689)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r688))
      )
    )
  (:action move-left-from-r688
    :parameters ()
    :precondition 
      (in r688)
    :effect
      (and
        (in r687)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r688))
      )
    )
  (:action move-right-from-r689
    :parameters ()
    :precondition 
      (in r689)
    :effect
      (and
        (in r690)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r689))
      )
    )
  (:action move-left-from-r689
    :parameters ()
    :precondition 
      (in r689)
    :effect
      (and
        (in r688)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r689))
      )
    )
  (:action move-right-from-r690
    :parameters ()
    :precondition 
      (in r690)
    :effect
      (and
        (in r691)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r690))
      )
    )
  (:action move-left-from-r690
    :parameters ()
    :precondition 
      (in r690)
    :effect
      (and
        (in r689)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r690))
      )
    )
  (:action move-right-from-r691
    :parameters ()
    :precondition 
      (in r691)
    :effect
      (and
        (in r692)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r691))
      )
    )
  (:action move-left-from-r691
    :parameters ()
    :precondition 
      (in r691)
    :effect
      (and
        (in r690)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r691))
      )
    )
  (:action move-right-from-r692
    :parameters ()
    :precondition 
      (in r692)
    :effect
      (and
        (in r693)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r692))
      )
    )
  (:action move-left-from-r692
    :parameters ()
    :precondition 
      (in r692)
    :effect
      (and
        (in r691)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r692))
      )
    )
  (:action move-right-from-r693
    :parameters ()
    :precondition 
      (in r693)
    :effect
      (and
        (in r694)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r693))
      )
    )
  (:action move-left-from-r693
    :parameters ()
    :precondition 
      (in r693)
    :effect
      (and
        (in r692)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r693))
      )
    )
  (:action move-right-from-r694
    :parameters ()
    :precondition 
      (in r694)
    :effect
      (and
        (in r695)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r694))
      )
    )
  (:action move-left-from-r694
    :parameters ()
    :precondition 
      (in r694)
    :effect
      (and
        (in r693)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r694))
      )
    )
  (:action move-right-from-r695
    :parameters ()
    :precondition 
      (in r695)
    :effect
      (and
        (in r696)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r695))
      )
    )
  (:action move-left-from-r695
    :parameters ()
    :precondition 
      (in r695)
    :effect
      (and
        (in r694)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r695))
      )
    )
  (:action move-right-from-r696
    :parameters ()
    :precondition 
      (in r696)
    :effect
      (and
        (in r697)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r696))
      )
    )
  (:action move-left-from-r696
    :parameters ()
    :precondition 
      (in r696)
    :effect
      (and
        (in r695)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r696))
      )
    )
  (:action move-right-from-r697
    :parameters ()
    :precondition 
      (in r697)
    :effect
      (and
        (in r698)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r697))
      )
    )
  (:action move-left-from-r697
    :parameters ()
    :precondition 
      (in r697)
    :effect
      (and
        (in r696)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r697))
      )
    )
  (:action move-right-from-r698
    :parameters ()
    :precondition 
      (in r698)
    :effect
      (and
        (in r699)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r698))
      )
    )
  (:action move-left-from-r698
    :parameters ()
    :precondition 
      (in r698)
    :effect
      (and
        (in r697)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r698))
      )
    )
  (:action move-right-from-r699
    :parameters ()
    :precondition 
      (in r699)
    :effect
      (and
        (in r700)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r699))
      )
    )
  (:action move-left-from-r699
    :parameters ()
    :precondition 
      (in r699)
    :effect
      (and
        (in r698)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r699))
      )
    )
  (:action move-right-from-r700
    :parameters ()
    :precondition 
      (in r700)
    :effect
      (and
        (in r701)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r700))
      )
    )
  (:action move-left-from-r700
    :parameters ()
    :precondition 
      (in r700)
    :effect
      (and
        (in r699)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r700))
      )
    )
  (:action move-right-from-r701
    :parameters ()
    :precondition 
      (in r701)
    :effect
      (and
        (in r702)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r701))
      )
    )
  (:action move-left-from-r701
    :parameters ()
    :precondition 
      (in r701)
    :effect
      (and
        (in r700)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r701))
      )
    )
  (:action move-right-from-r702
    :parameters ()
    :precondition 
      (in r702)
    :effect
      (and
        (in r703)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r702))
      )
    )
  (:action move-left-from-r702
    :parameters ()
    :precondition 
      (in r702)
    :effect
      (and
        (in r701)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r702))
      )
    )
  (:action move-right-from-r703
    :parameters ()
    :precondition 
      (in r703)
    :effect
      (and
        (in r704)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r703))
      )
    )
  (:action move-left-from-r703
    :parameters ()
    :precondition 
      (in r703)
    :effect
      (and
        (in r702)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r703))
      )
    )
  (:action move-right-from-r704
    :parameters ()
    :precondition 
      (in r704)
    :effect
      (and
        (in r705)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r704))
      )
    )
  (:action move-left-from-r704
    :parameters ()
    :precondition 
      (in r704)
    :effect
      (and
        (in r703)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r704))
      )
    )
  (:action move-right-from-r705
    :parameters ()
    :precondition 
      (in r705)
    :effect
      (and
        (in r706)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r705))
      )
    )
  (:action move-left-from-r705
    :parameters ()
    :precondition 
      (in r705)
    :effect
      (and
        (in r704)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r705))
      )
    )
  (:action move-right-from-r706
    :parameters ()
    :precondition 
      (in r706)
    :effect
      (and
        (in r707)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r706))
      )
    )
  (:action move-left-from-r706
    :parameters ()
    :precondition 
      (in r706)
    :effect
      (and
        (in r705)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r706))
      )
    )
  (:action move-right-from-r707
    :parameters ()
    :precondition 
      (in r707)
    :effect
      (and
        (in r708)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r707))
      )
    )
  (:action move-left-from-r707
    :parameters ()
    :precondition 
      (in r707)
    :effect
      (and
        (in r706)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r707))
      )
    )
  (:action move-right-from-r708
    :parameters ()
    :precondition 
      (in r708)
    :effect
      (and
        (in r709)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r708))
      )
    )
  (:action move-left-from-r708
    :parameters ()
    :precondition 
      (in r708)
    :effect
      (and
        (in r707)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r708))
      )
    )
  (:action move-right-from-r709
    :parameters ()
    :precondition 
      (in r709)
    :effect
      (and
        (in r710)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r709))
      )
    )
  (:action move-left-from-r709
    :parameters ()
    :precondition 
      (in r709)
    :effect
      (and
        (in r708)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r709))
      )
    )
  (:action move-right-from-r710
    :parameters ()
    :precondition 
      (in r710)
    :effect
      (and
        (in r711)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r710))
      )
    )
  (:action move-left-from-r710
    :parameters ()
    :precondition 
      (in r710)
    :effect
      (and
        (in r709)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r710))
      )
    )
  (:action move-right-from-r711
    :parameters ()
    :precondition 
      (in r711)
    :effect
      (and
        (in r712)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r711))
      )
    )
  (:action move-left-from-r711
    :parameters ()
    :precondition 
      (in r711)
    :effect
      (and
        (in r710)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r711))
      )
    )
  (:action move-right-from-r712
    :parameters ()
    :precondition 
      (in r712)
    :effect
      (and
        (in r713)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r712))
      )
    )
  (:action move-left-from-r712
    :parameters ()
    :precondition 
      (in r712)
    :effect
      (and
        (in r711)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r712))
      )
    )
  (:action move-right-from-r713
    :parameters ()
    :precondition 
      (in r713)
    :effect
      (and
        (in r714)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r713))
      )
    )
  (:action move-left-from-r713
    :parameters ()
    :precondition 
      (in r713)
    :effect
      (and
        (in r712)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r713))
      )
    )
  (:action move-right-from-r714
    :parameters ()
    :precondition 
      (in r714)
    :effect
      (and
        (in r715)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r714))
      )
    )
  (:action move-left-from-r714
    :parameters ()
    :precondition 
      (in r714)
    :effect
      (and
        (in r713)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r714))
      )
    )
  (:action move-right-from-r715
    :parameters ()
    :precondition 
      (in r715)
    :effect
      (and
        (in r716)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r715))
      )
    )
  (:action move-left-from-r715
    :parameters ()
    :precondition 
      (in r715)
    :effect
      (and
        (in r714)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r715))
      )
    )
  (:action move-right-from-r716
    :parameters ()
    :precondition 
      (in r716)
    :effect
      (and
        (in r717)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r716))
      )
    )
  (:action move-left-from-r716
    :parameters ()
    :precondition 
      (in r716)
    :effect
      (and
        (in r715)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r716))
      )
    )
  (:action move-right-from-r717
    :parameters ()
    :precondition 
      (in r717)
    :effect
      (and
        (in r718)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r717))
      )
    )
  (:action move-left-from-r717
    :parameters ()
    :precondition 
      (in r717)
    :effect
      (and
        (in r716)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r717))
      )
    )
  (:action move-right-from-r718
    :parameters ()
    :precondition 
      (in r718)
    :effect
      (and
        (in r719)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r718))
      )
    )
  (:action move-left-from-r718
    :parameters ()
    :precondition 
      (in r718)
    :effect
      (and
        (in r717)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r718))
      )
    )
  (:action move-right-from-r719
    :parameters ()
    :precondition 
      (in r719)
    :effect
      (and
        (in r720)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r719))
      )
    )
  (:action move-left-from-r719
    :parameters ()
    :precondition 
      (in r719)
    :effect
      (and
        (in r718)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r719))
      )
    )
  (:action move-right-from-r720
    :parameters ()
    :precondition 
      (in r720)
    :effect
      (and
        (in r721)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r720))
      )
    )
  (:action move-left-from-r720
    :parameters ()
    :precondition 
      (in r720)
    :effect
      (and
        (in r719)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r720))
      )
    )
  (:action move-right-from-r721
    :parameters ()
    :precondition 
      (in r721)
    :effect
      (and
        (in r722)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r721))
      )
    )
  (:action move-left-from-r721
    :parameters ()
    :precondition 
      (in r721)
    :effect
      (and
        (in r720)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r721))
      )
    )
  (:action move-right-from-r722
    :parameters ()
    :precondition 
      (in r722)
    :effect
      (and
        (in r723)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r722))
      )
    )
  (:action move-left-from-r722
    :parameters ()
    :precondition 
      (in r722)
    :effect
      (and
        (in r721)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r722))
      )
    )
  (:action move-right-from-r723
    :parameters ()
    :precondition 
      (in r723)
    :effect
      (and
        (in r724)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r723))
      )
    )
  (:action move-left-from-r723
    :parameters ()
    :precondition 
      (in r723)
    :effect
      (and
        (in r722)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r723))
      )
    )
  (:action move-right-from-r724
    :parameters ()
    :precondition 
      (in r724)
    :effect
      (and
        (in r725)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r724))
      )
    )
  (:action move-left-from-r724
    :parameters ()
    :precondition 
      (in r724)
    :effect
      (and
        (in r723)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r724))
      )
    )
  (:action move-right-from-r725
    :parameters ()
    :precondition 
      (in r725)
    :effect
      (and
        (in r726)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r725))
      )
    )
  (:action move-left-from-r725
    :parameters ()
    :precondition 
      (in r725)
    :effect
      (and
        (in r724)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r725))
      )
    )
  (:action move-right-from-r726
    :parameters ()
    :precondition 
      (in r726)
    :effect
      (and
        (in r727)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r726))
      )
    )
  (:action move-left-from-r726
    :parameters ()
    :precondition 
      (in r726)
    :effect
      (and
        (in r725)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r726))
      )
    )
  (:action move-right-from-r727
    :parameters ()
    :precondition 
      (in r727)
    :effect
      (and
        (in r728)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r727))
      )
    )
  (:action move-left-from-r727
    :parameters ()
    :precondition 
      (in r727)
    :effect
      (and
        (in r726)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r727))
      )
    )
  (:action move-right-from-r728
    :parameters ()
    :precondition 
      (in r728)
    :effect
      (and
        (in r729)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r728))
      )
    )
  (:action move-left-from-r728
    :parameters ()
    :precondition 
      (in r728)
    :effect
      (and
        (in r727)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r728))
      )
    )
  (:action move-right-from-r729
    :parameters ()
    :precondition 
      (in r729)
    :effect
      (and
        (in r730)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r729))
      )
    )
  (:action move-left-from-r729
    :parameters ()
    :precondition 
      (in r729)
    :effect
      (and
        (in r728)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r729))
      )
    )
  (:action move-right-from-r730
    :parameters ()
    :precondition 
      (in r730)
    :effect
      (and
        (in r731)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r730))
      )
    )
  (:action move-left-from-r730
    :parameters ()
    :precondition 
      (in r730)
    :effect
      (and
        (in r729)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r730))
      )
    )
  (:action move-right-from-r731
    :parameters ()
    :precondition 
      (in r731)
    :effect
      (and
        (in r732)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r731))
      )
    )
  (:action move-left-from-r731
    :parameters ()
    :precondition 
      (in r731)
    :effect
      (and
        (in r730)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r731))
      )
    )
  (:action move-right-from-r732
    :parameters ()
    :precondition 
      (in r732)
    :effect
      (and
        (in r733)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r732))
      )
    )
  (:action move-left-from-r732
    :parameters ()
    :precondition 
      (in r732)
    :effect
      (and
        (in r731)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r732))
      )
    )
  (:action move-right-from-r733
    :parameters ()
    :precondition 
      (in r733)
    :effect
      (and
        (in r734)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r733))
      )
    )
  (:action move-left-from-r733
    :parameters ()
    :precondition 
      (in r733)
    :effect
      (and
        (in r732)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r733))
      )
    )
  (:action move-right-from-r734
    :parameters ()
    :precondition 
      (in r734)
    :effect
      (and
        (in r735)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r734))
      )
    )
  (:action move-left-from-r734
    :parameters ()
    :precondition 
      (in r734)
    :effect
      (and
        (in r733)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r734))
      )
    )
  (:action move-right-from-r735
    :parameters ()
    :precondition 
      (in r735)
    :effect
      (and
        (in r736)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r735))
      )
    )
  (:action move-left-from-r735
    :parameters ()
    :precondition 
      (in r735)
    :effect
      (and
        (in r734)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r735))
      )
    )
  (:action move-right-from-r736
    :parameters ()
    :precondition 
      (in r736)
    :effect
      (and
        (in r737)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r736))
      )
    )
  (:action move-left-from-r736
    :parameters ()
    :precondition 
      (in r736)
    :effect
      (and
        (in r735)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r736))
      )
    )
  (:action move-right-from-r737
    :parameters ()
    :precondition 
      (in r737)
    :effect
      (and
        (in r738)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r737))
      )
    )
  (:action move-left-from-r737
    :parameters ()
    :precondition 
      (in r737)
    :effect
      (and
        (in r736)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r737))
      )
    )
  (:action move-right-from-r738
    :parameters ()
    :precondition 
      (in r738)
    :effect
      (and
        (in r739)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r738))
      )
    )
  (:action move-left-from-r738
    :parameters ()
    :precondition 
      (in r738)
    :effect
      (and
        (in r737)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r738))
      )
    )
  (:action move-right-from-r739
    :parameters ()
    :precondition 
      (in r739)
    :effect
      (and
        (in r740)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r739))
      )
    )
  (:action move-left-from-r739
    :parameters ()
    :precondition 
      (in r739)
    :effect
      (and
        (in r738)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r739))
      )
    )
  (:action move-right-from-r740
    :parameters ()
    :precondition 
      (in r740)
    :effect
      (and
        (in r741)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r740))
      )
    )
  (:action move-left-from-r740
    :parameters ()
    :precondition 
      (in r740)
    :effect
      (and
        (in r739)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r740))
      )
    )
  (:action move-right-from-r741
    :parameters ()
    :precondition 
      (in r741)
    :effect
      (and
        (in r742)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r741))
      )
    )
  (:action move-left-from-r741
    :parameters ()
    :precondition 
      (in r741)
    :effect
      (and
        (in r740)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r741))
      )
    )
  (:action move-right-from-r742
    :parameters ()
    :precondition 
      (in r742)
    :effect
      (and
        (in r743)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r742))
      )
    )
  (:action move-left-from-r742
    :parameters ()
    :precondition 
      (in r742)
    :effect
      (and
        (in r741)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r742))
      )
    )
  (:action move-right-from-r743
    :parameters ()
    :precondition 
      (in r743)
    :effect
      (and
        (in r744)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r743))
      )
    )
  (:action move-left-from-r743
    :parameters ()
    :precondition 
      (in r743)
    :effect
      (and
        (in r742)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r743))
      )
    )
  (:action move-right-from-r744
    :parameters ()
    :precondition 
      (in r744)
    :effect
      (and
        (in r745)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r744))
      )
    )
  (:action move-left-from-r744
    :parameters ()
    :precondition 
      (in r744)
    :effect
      (and
        (in r743)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r744))
      )
    )
  (:action move-right-from-r745
    :parameters ()
    :precondition 
      (in r745)
    :effect
      (and
        (in r746)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r745))
      )
    )
  (:action move-left-from-r745
    :parameters ()
    :precondition 
      (in r745)
    :effect
      (and
        (in r744)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r745))
      )
    )
  (:action move-right-from-r746
    :parameters ()
    :precondition 
      (in r746)
    :effect
      (and
        (in r747)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r746))
      )
    )
  (:action move-left-from-r746
    :parameters ()
    :precondition 
      (in r746)
    :effect
      (and
        (in r745)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r746))
      )
    )
  (:action move-right-from-r747
    :parameters ()
    :precondition 
      (in r747)
    :effect
      (and
        (in r748)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r747))
      )
    )
  (:action move-left-from-r747
    :parameters ()
    :precondition 
      (in r747)
    :effect
      (and
        (in r746)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r747))
      )
    )
  (:action move-right-from-r748
    :parameters ()
    :precondition 
      (in r748)
    :effect
      (and
        (in r749)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r748))
      )
    )
  (:action move-left-from-r748
    :parameters ()
    :precondition 
      (in r748)
    :effect
      (and
        (in r747)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r748))
      )
    )
  (:action move-right-from-r749
    :parameters ()
    :precondition 
      (in r749)
    :effect
      (and
        (in r750)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r749))
      )
    )
  (:action move-left-from-r749
    :parameters ()
    :precondition 
      (in r749)
    :effect
      (and
        (in r748)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r749))
      )
    )
  (:action move-right-from-r750
    :parameters ()
    :precondition 
      (in r750)
    :effect
      (and
        (in r751)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r750))
      )
    )
  (:action move-left-from-r750
    :parameters ()
    :precondition 
      (in r750)
    :effect
      (and
        (in r749)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r750))
      )
    )
  (:action move-right-from-r751
    :parameters ()
    :precondition 
      (in r751)
    :effect
      (and
        (in r752)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r751))
      )
    )
  (:action move-left-from-r751
    :parameters ()
    :precondition 
      (in r751)
    :effect
      (and
        (in r750)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r751))
      )
    )
  (:action move-right-from-r752
    :parameters ()
    :precondition 
      (in r752)
    :effect
      (and
        (in r753)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r752))
      )
    )
  (:action move-left-from-r752
    :parameters ()
    :precondition 
      (in r752)
    :effect
      (and
        (in r751)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r752))
      )
    )
  (:action move-right-from-r753
    :parameters ()
    :precondition 
      (in r753)
    :effect
      (and
        (in r754)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r753))
      )
    )
  (:action move-left-from-r753
    :parameters ()
    :precondition 
      (in r753)
    :effect
      (and
        (in r752)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r753))
      )
    )
  (:action move-right-from-r754
    :parameters ()
    :precondition 
      (in r754)
    :effect
      (and
        (in r755)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r754))
      )
    )
  (:action move-left-from-r754
    :parameters ()
    :precondition 
      (in r754)
    :effect
      (and
        (in r753)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r754))
      )
    )
  (:action move-right-from-r755
    :parameters ()
    :precondition 
      (in r755)
    :effect
      (and
        (in r756)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r755))
      )
    )
  (:action move-left-from-r755
    :parameters ()
    :precondition 
      (in r755)
    :effect
      (and
        (in r754)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r755))
      )
    )
  (:action move-right-from-r756
    :parameters ()
    :precondition 
      (in r756)
    :effect
      (and
        (in r757)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r756))
      )
    )
  (:action move-left-from-r756
    :parameters ()
    :precondition 
      (in r756)
    :effect
      (and
        (in r755)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r756))
      )
    )
  (:action move-right-from-r757
    :parameters ()
    :precondition 
      (in r757)
    :effect
      (and
        (in r758)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r757))
      )
    )
  (:action move-left-from-r757
    :parameters ()
    :precondition 
      (in r757)
    :effect
      (and
        (in r756)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r757))
      )
    )
  (:action move-right-from-r758
    :parameters ()
    :precondition 
      (in r758)
    :effect
      (and
        (in r759)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r758))
      )
    )
  (:action move-left-from-r758
    :parameters ()
    :precondition 
      (in r758)
    :effect
      (and
        (in r757)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r758))
      )
    )
  (:action move-right-from-r759
    :parameters ()
    :precondition 
      (in r759)
    :effect
      (and
        (in r760)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r759))
      )
    )
  (:action move-left-from-r759
    :parameters ()
    :precondition 
      (in r759)
    :effect
      (and
        (in r758)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r759))
      )
    )
  (:action move-right-from-r760
    :parameters ()
    :precondition 
      (in r760)
    :effect
      (and
        (in r761)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r760))
      )
    )
  (:action move-left-from-r760
    :parameters ()
    :precondition 
      (in r760)
    :effect
      (and
        (in r759)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r760))
      )
    )
  (:action move-right-from-r761
    :parameters ()
    :precondition 
      (in r761)
    :effect
      (and
        (in r762)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r761))
      )
    )
  (:action move-left-from-r761
    :parameters ()
    :precondition 
      (in r761)
    :effect
      (and
        (in r760)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r761))
      )
    )
  (:action move-right-from-r762
    :parameters ()
    :precondition 
      (in r762)
    :effect
      (and
        (in r763)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r762))
      )
    )
  (:action move-left-from-r762
    :parameters ()
    :precondition 
      (in r762)
    :effect
      (and
        (in r761)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r762))
      )
    )
  (:action move-right-from-r763
    :parameters ()
    :precondition 
      (in r763)
    :effect
      (and
        (in r764)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r763))
      )
    )
  (:action move-left-from-r763
    :parameters ()
    :precondition 
      (in r763)
    :effect
      (and
        (in r762)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r763))
      )
    )
  (:action move-right-from-r764
    :parameters ()
    :precondition 
      (in r764)
    :effect
      (and
        (in r765)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r764))
      )
    )
  (:action move-left-from-r764
    :parameters ()
    :precondition 
      (in r764)
    :effect
      (and
        (in r763)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r764))
      )
    )
  (:action move-right-from-r765
    :parameters ()
    :precondition 
      (in r765)
    :effect
      (and
        (in r766)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r765))
      )
    )
  (:action move-left-from-r765
    :parameters ()
    :precondition 
      (in r765)
    :effect
      (and
        (in r764)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r765))
      )
    )
  (:action move-right-from-r766
    :parameters ()
    :precondition 
      (in r766)
    :effect
      (and
        (in r767)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r766))
      )
    )
  (:action move-left-from-r766
    :parameters ()
    :precondition 
      (in r766)
    :effect
      (and
        (in r765)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r766))
      )
    )
  (:action move-right-from-r767
    :parameters ()
    :precondition 
      (in r767)
    :effect
      (and
        (in r768)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r767))
      )
    )
  (:action move-left-from-r767
    :parameters ()
    :precondition 
      (in r767)
    :effect
      (and
        (in r766)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r767))
      )
    )
  (:action move-right-from-r768
    :parameters ()
    :precondition 
      (in r768)
    :effect
      (and
        (in r769)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r768))
      )
    )
  (:action move-left-from-r768
    :parameters ()
    :precondition 
      (in r768)
    :effect
      (and
        (in r767)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r768))
      )
    )
  (:action move-right-from-r769
    :parameters ()
    :precondition 
      (in r769)
    :effect
      (and
        (in r770)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r769))
      )
    )
  (:action move-left-from-r769
    :parameters ()
    :precondition 
      (in r769)
    :effect
      (and
        (in r768)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r769))
      )
    )
  (:action move-right-from-r770
    :parameters ()
    :precondition 
      (in r770)
    :effect
      (and
        (in r771)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r770))
      )
    )
  (:action move-left-from-r770
    :parameters ()
    :precondition 
      (in r770)
    :effect
      (and
        (in r769)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r770))
      )
    )
  (:action move-right-from-r771
    :parameters ()
    :precondition 
      (in r771)
    :effect
      (and
        (in r772)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r771))
      )
    )
  (:action move-left-from-r771
    :parameters ()
    :precondition 
      (in r771)
    :effect
      (and
        (in r770)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r771))
      )
    )
  (:action move-right-from-r772
    :parameters ()
    :precondition 
      (in r772)
    :effect
      (and
        (in r773)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r772))
      )
    )
  (:action move-left-from-r772
    :parameters ()
    :precondition 
      (in r772)
    :effect
      (and
        (in r771)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r772))
      )
    )
  (:action move-right-from-r773
    :parameters ()
    :precondition 
      (in r773)
    :effect
      (and
        (in r774)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r773))
      )
    )
  (:action move-left-from-r773
    :parameters ()
    :precondition 
      (in r773)
    :effect
      (and
        (in r772)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r773))
      )
    )
  (:action move-right-from-r774
    :parameters ()
    :precondition 
      (in r774)
    :effect
      (and
        (in r775)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r774))
      )
    )
  (:action move-left-from-r774
    :parameters ()
    :precondition 
      (in r774)
    :effect
      (and
        (in r773)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r774))
      )
    )
  (:action move-right-from-r775
    :parameters ()
    :precondition 
      (in r775)
    :effect
      (and
        (in r776)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r775))
      )
    )
  (:action move-left-from-r775
    :parameters ()
    :precondition 
      (in r775)
    :effect
      (and
        (in r774)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r775))
      )
    )
  (:action move-right-from-r776
    :parameters ()
    :precondition 
      (in r776)
    :effect
      (and
        (in r777)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r776))
      )
    )
  (:action move-left-from-r776
    :parameters ()
    :precondition 
      (in r776)
    :effect
      (and
        (in r775)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r776))
      )
    )
  (:action move-right-from-r777
    :parameters ()
    :precondition 
      (in r777)
    :effect
      (and
        (in r778)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r777))
      )
    )
  (:action move-left-from-r777
    :parameters ()
    :precondition 
      (in r777)
    :effect
      (and
        (in r776)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r777))
      )
    )
  (:action move-right-from-r778
    :parameters ()
    :precondition 
      (in r778)
    :effect
      (and
        (in r779)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r778))
      )
    )
  (:action move-left-from-r778
    :parameters ()
    :precondition 
      (in r778)
    :effect
      (and
        (in r777)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r778))
      )
    )
  (:action move-right-from-r779
    :parameters ()
    :precondition 
      (in r779)
    :effect
      (and
        (in r780)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r779))
      )
    )
  (:action move-left-from-r779
    :parameters ()
    :precondition 
      (in r779)
    :effect
      (and
        (in r778)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r779))
      )
    )
  (:action move-right-from-r780
    :parameters ()
    :precondition 
      (in r780)
    :effect
      (and
        (in r781)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r780))
      )
    )
  (:action move-left-from-r780
    :parameters ()
    :precondition 
      (in r780)
    :effect
      (and
        (in r779)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r780))
      )
    )
  (:action move-right-from-r781
    :parameters ()
    :precondition 
      (in r781)
    :effect
      (and
        (in r782)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r781))
      )
    )
  (:action move-left-from-r781
    :parameters ()
    :precondition 
      (in r781)
    :effect
      (and
        (in r780)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r781))
      )
    )
  (:action move-right-from-r782
    :parameters ()
    :precondition 
      (in r782)
    :effect
      (and
        (in r783)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r782))
      )
    )
  (:action move-left-from-r782
    :parameters ()
    :precondition 
      (in r782)
    :effect
      (and
        (in r781)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r782))
      )
    )
  (:action move-right-from-r783
    :parameters ()
    :precondition 
      (in r783)
    :effect
      (and
        (in r784)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r783))
      )
    )
  (:action move-left-from-r783
    :parameters ()
    :precondition 
      (in r783)
    :effect
      (and
        (in r782)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r783))
      )
    )
  (:action move-right-from-r784
    :parameters ()
    :precondition 
      (in r784)
    :effect
      (and
        (in r785)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r784))
      )
    )
  (:action move-left-from-r784
    :parameters ()
    :precondition 
      (in r784)
    :effect
      (and
        (in r783)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r784))
      )
    )
  (:action move-right-from-r785
    :parameters ()
    :precondition 
      (in r785)
    :effect
      (and
        (in r786)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r785))
      )
    )
  (:action move-left-from-r785
    :parameters ()
    :precondition 
      (in r785)
    :effect
      (and
        (in r784)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r785))
      )
    )
  (:action move-right-from-r786
    :parameters ()
    :precondition 
      (in r786)
    :effect
      (and
        (in r787)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r786))
      )
    )
  (:action move-left-from-r786
    :parameters ()
    :precondition 
      (in r786)
    :effect
      (and
        (in r785)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r786))
      )
    )
  (:action move-right-from-r787
    :parameters ()
    :precondition 
      (in r787)
    :effect
      (and
        (in r788)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r787))
      )
    )
  (:action move-left-from-r787
    :parameters ()
    :precondition 
      (in r787)
    :effect
      (and
        (in r786)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r787))
      )
    )
  (:action move-right-from-r788
    :parameters ()
    :precondition 
      (in r788)
    :effect
      (and
        (in r789)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r788))
      )
    )
  (:action move-left-from-r788
    :parameters ()
    :precondition 
      (in r788)
    :effect
      (and
        (in r787)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r788))
      )
    )
  (:action move-right-from-r789
    :parameters ()
    :precondition 
      (in r789)
    :effect
      (and
        (in r790)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r789))
      )
    )
  (:action move-left-from-r789
    :parameters ()
    :precondition 
      (in r789)
    :effect
      (and
        (in r788)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r789))
      )
    )
  (:action move-right-from-r790
    :parameters ()
    :precondition 
      (in r790)
    :effect
      (and
        (in r791)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r790))
      )
    )
  (:action move-left-from-r790
    :parameters ()
    :precondition 
      (in r790)
    :effect
      (and
        (in r789)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r790))
      )
    )
  (:action move-right-from-r791
    :parameters ()
    :precondition 
      (in r791)
    :effect
      (and
        (in r792)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r791))
      )
    )
  (:action move-left-from-r791
    :parameters ()
    :precondition 
      (in r791)
    :effect
      (and
        (in r790)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r791))
      )
    )
  (:action move-right-from-r792
    :parameters ()
    :precondition 
      (in r792)
    :effect
      (and
        (in r793)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r792))
      )
    )
  (:action move-left-from-r792
    :parameters ()
    :precondition 
      (in r792)
    :effect
      (and
        (in r791)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r792))
      )
    )
  (:action move-right-from-r793
    :parameters ()
    :precondition 
      (in r793)
    :effect
      (and
        (in r794)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r793))
      )
    )
  (:action move-left-from-r793
    :parameters ()
    :precondition 
      (in r793)
    :effect
      (and
        (in r792)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r793))
      )
    )
  (:action move-right-from-r794
    :parameters ()
    :precondition 
      (in r794)
    :effect
      (and
        (in r795)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r794))
      )
    )
  (:action move-left-from-r794
    :parameters ()
    :precondition 
      (in r794)
    :effect
      (and
        (in r793)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r794))
      )
    )
  (:action move-right-from-r795
    :parameters ()
    :precondition 
      (in r795)
    :effect
      (and
        (in r796)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r795))
      )
    )
  (:action move-left-from-r795
    :parameters ()
    :precondition 
      (in r795)
    :effect
      (and
        (in r794)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r795))
      )
    )
  (:action move-right-from-r796
    :parameters ()
    :precondition 
      (in r796)
    :effect
      (and
        (in r797)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r796))
      )
    )
  (:action move-left-from-r796
    :parameters ()
    :precondition 
      (in r796)
    :effect
      (and
        (in r795)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r796))
      )
    )
  (:action move-right-from-r797
    :parameters ()
    :precondition 
      (in r797)
    :effect
      (and
        (in r798)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r797))
      )
    )
  (:action move-left-from-r797
    :parameters ()
    :precondition 
      (in r797)
    :effect
      (and
        (in r796)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r797))
      )
    )
  (:action move-right-from-r798
    :parameters ()
    :precondition 
      (in r798)
    :effect
      (and
        (in r799)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r798))
      )
    )
  (:action move-left-from-r798
    :parameters ()
    :precondition 
      (in r798)
    :effect
      (and
        (in r797)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r798))
      )
    )
  (:action move-right-from-r799
    :parameters ()
    :precondition 
      (in r799)
    :effect
      (and
        (in r800)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r799))
      )
    )
  (:action move-left-from-r799
    :parameters ()
    :precondition 
      (in r799)
    :effect
      (and
        (in r798)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r799))
      )
    )
  (:action move-right-from-r800
    :parameters ()
    :precondition 
      (in r800)
    :effect
      (and
        (in r801)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r800))
      )
    )
  (:action move-left-from-r800
    :parameters ()
    :precondition 
      (in r800)
    :effect
      (and
        (in r799)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r800))
      )
    )
  (:action move-right-from-r801
    :parameters ()
    :precondition 
      (in r801)
    :effect
      (and
        (in r802)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r801))
      )
    )
  (:action move-left-from-r801
    :parameters ()
    :precondition 
      (in r801)
    :effect
      (and
        (in r800)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r801))
      )
    )
  (:action move-right-from-r802
    :parameters ()
    :precondition 
      (in r802)
    :effect
      (and
        (in r803)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r802))
      )
    )
  (:action move-left-from-r802
    :parameters ()
    :precondition 
      (in r802)
    :effect
      (and
        (in r801)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r802))
      )
    )
  (:action move-right-from-r803
    :parameters ()
    :precondition 
      (in r803)
    :effect
      (and
        (in r804)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r803))
      )
    )
  (:action move-left-from-r803
    :parameters ()
    :precondition 
      (in r803)
    :effect
      (and
        (in r802)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r803))
      )
    )
  (:action move-right-from-r804
    :parameters ()
    :precondition 
      (in r804)
    :effect
      (and
        (in r805)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r804))
      )
    )
  (:action move-left-from-r804
    :parameters ()
    :precondition 
      (in r804)
    :effect
      (and
        (in r803)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r804))
      )
    )
  (:action move-right-from-r805
    :parameters ()
    :precondition 
      (in r805)
    :effect
      (and
        (in r806)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r805))
      )
    )
  (:action move-left-from-r805
    :parameters ()
    :precondition 
      (in r805)
    :effect
      (and
        (in r804)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r805))
      )
    )
  (:action move-right-from-r806
    :parameters ()
    :precondition 
      (in r806)
    :effect
      (and
        (in r807)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r806))
      )
    )
  (:action move-left-from-r806
    :parameters ()
    :precondition 
      (in r806)
    :effect
      (and
        (in r805)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r806))
      )
    )
  (:action move-right-from-r807
    :parameters ()
    :precondition 
      (in r807)
    :effect
      (and
        (in r808)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r807))
      )
    )
  (:action move-left-from-r807
    :parameters ()
    :precondition 
      (in r807)
    :effect
      (and
        (in r806)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r807))
      )
    )
  (:action move-right-from-r808
    :parameters ()
    :precondition 
      (in r808)
    :effect
      (and
        (in r809)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r808))
      )
    )
  (:action move-left-from-r808
    :parameters ()
    :precondition 
      (in r808)
    :effect
      (and
        (in r807)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r808))
      )
    )
  (:action move-right-from-r809
    :parameters ()
    :precondition 
      (in r809)
    :effect
      (and
        (in r810)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r809))
      )
    )
  (:action move-left-from-r809
    :parameters ()
    :precondition 
      (in r809)
    :effect
      (and
        (in r808)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r809))
      )
    )
  (:action move-right-from-r810
    :parameters ()
    :precondition 
      (in r810)
    :effect
      (and
        (in r811)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r810))
      )
    )
  (:action move-left-from-r810
    :parameters ()
    :precondition 
      (in r810)
    :effect
      (and
        (in r809)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r810))
      )
    )
  (:action move-right-from-r811
    :parameters ()
    :precondition 
      (in r811)
    :effect
      (and
        (in r812)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r811))
      )
    )
  (:action move-left-from-r811
    :parameters ()
    :precondition 
      (in r811)
    :effect
      (and
        (in r810)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r811))
      )
    )
  (:action move-right-from-r812
    :parameters ()
    :precondition 
      (in r812)
    :effect
      (and
        (in r813)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r812))
      )
    )
  (:action move-left-from-r812
    :parameters ()
    :precondition 
      (in r812)
    :effect
      (and
        (in r811)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r812))
      )
    )
  (:action move-right-from-r813
    :parameters ()
    :precondition 
      (in r813)
    :effect
      (and
        (in r814)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r813))
      )
    )
  (:action move-left-from-r813
    :parameters ()
    :precondition 
      (in r813)
    :effect
      (and
        (in r812)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r813))
      )
    )
  (:action move-right-from-r814
    :parameters ()
    :precondition 
      (in r814)
    :effect
      (and
        (in r815)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r814))
      )
    )
  (:action move-left-from-r814
    :parameters ()
    :precondition 
      (in r814)
    :effect
      (and
        (in r813)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r814))
      )
    )
  (:action move-right-from-r815
    :parameters ()
    :precondition 
      (in r815)
    :effect
      (and
        (in r816)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r815))
      )
    )
  (:action move-left-from-r815
    :parameters ()
    :precondition 
      (in r815)
    :effect
      (and
        (in r814)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r815))
      )
    )
  (:action move-right-from-r816
    :parameters ()
    :precondition 
      (in r816)
    :effect
      (and
        (in r817)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r816))
      )
    )
  (:action move-left-from-r816
    :parameters ()
    :precondition 
      (in r816)
    :effect
      (and
        (in r815)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r816))
      )
    )
  (:action move-right-from-r817
    :parameters ()
    :precondition 
      (in r817)
    :effect
      (and
        (in r818)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r817))
      )
    )
  (:action move-left-from-r817
    :parameters ()
    :precondition 
      (in r817)
    :effect
      (and
        (in r816)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r817))
      )
    )
  (:action move-right-from-r818
    :parameters ()
    :precondition 
      (in r818)
    :effect
      (and
        (in r819)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r818))
      )
    )
  (:action move-left-from-r818
    :parameters ()
    :precondition 
      (in r818)
    :effect
      (and
        (in r817)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r818))
      )
    )
  (:action move-right-from-r819
    :parameters ()
    :precondition 
      (in r819)
    :effect
      (and
        (in r820)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r819))
      )
    )
  (:action move-left-from-r819
    :parameters ()
    :precondition 
      (in r819)
    :effect
      (and
        (in r818)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r819))
      )
    )
  (:action move-right-from-r820
    :parameters ()
    :precondition 
      (in r820)
    :effect
      (and
        (in r821)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r820))
      )
    )
  (:action move-left-from-r820
    :parameters ()
    :precondition 
      (in r820)
    :effect
      (and
        (in r819)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r820))
      )
    )
  (:action move-right-from-r821
    :parameters ()
    :precondition 
      (in r821)
    :effect
      (and
        (in r822)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r821))
      )
    )
  (:action move-left-from-r821
    :parameters ()
    :precondition 
      (in r821)
    :effect
      (and
        (in r820)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r821))
      )
    )
  (:action move-right-from-r822
    :parameters ()
    :precondition 
      (in r822)
    :effect
      (and
        (in r823)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r822))
      )
    )
  (:action move-left-from-r822
    :parameters ()
    :precondition 
      (in r822)
    :effect
      (and
        (in r821)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r822))
      )
    )
  (:action move-right-from-r823
    :parameters ()
    :precondition 
      (in r823)
    :effect
      (and
        (in r824)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r823))
      )
    )
  (:action move-left-from-r823
    :parameters ()
    :precondition 
      (in r823)
    :effect
      (and
        (in r822)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r823))
      )
    )
  (:action move-right-from-r824
    :parameters ()
    :precondition 
      (in r824)
    :effect
      (and
        (in r825)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r824))
      )
    )
  (:action move-left-from-r824
    :parameters ()
    :precondition 
      (in r824)
    :effect
      (and
        (in r823)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r824))
      )
    )
  (:action move-right-from-r825
    :parameters ()
    :precondition 
      (in r825)
    :effect
      (and
        (in r826)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r825))
      )
    )
  (:action move-left-from-r825
    :parameters ()
    :precondition 
      (in r825)
    :effect
      (and
        (in r824)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r825))
      )
    )
  (:action move-right-from-r826
    :parameters ()
    :precondition 
      (in r826)
    :effect
      (and
        (in r827)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r826))
      )
    )
  (:action move-left-from-r826
    :parameters ()
    :precondition 
      (in r826)
    :effect
      (and
        (in r825)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r826))
      )
    )
  (:action move-right-from-r827
    :parameters ()
    :precondition 
      (in r827)
    :effect
      (and
        (in r828)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r827))
      )
    )
  (:action move-left-from-r827
    :parameters ()
    :precondition 
      (in r827)
    :effect
      (and
        (in r826)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r827))
      )
    )
  (:action move-right-from-r828
    :parameters ()
    :precondition 
      (in r828)
    :effect
      (and
        (in r829)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r828))
      )
    )
  (:action move-left-from-r828
    :parameters ()
    :precondition 
      (in r828)
    :effect
      (and
        (in r827)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r828))
      )
    )
  (:action move-right-from-r829
    :parameters ()
    :precondition 
      (in r829)
    :effect
      (and
        (in r830)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r829))
      )
    )
  (:action move-left-from-r829
    :parameters ()
    :precondition 
      (in r829)
    :effect
      (and
        (in r828)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r829))
      )
    )
  (:action move-right-from-r830
    :parameters ()
    :precondition 
      (in r830)
    :effect
      (and
        (in r831)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r830))
      )
    )
  (:action move-left-from-r830
    :parameters ()
    :precondition 
      (in r830)
    :effect
      (and
        (in r829)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r830))
      )
    )
  (:action move-right-from-r831
    :parameters ()
    :precondition 
      (in r831)
    :effect
      (and
        (in r832)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r831))
      )
    )
  (:action move-left-from-r831
    :parameters ()
    :precondition 
      (in r831)
    :effect
      (and
        (in r830)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r831))
      )
    )
  (:action move-right-from-r832
    :parameters ()
    :precondition 
      (in r832)
    :effect
      (and
        (in r833)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r832))
      )
    )
  (:action move-left-from-r832
    :parameters ()
    :precondition 
      (in r832)
    :effect
      (and
        (in r831)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r832))
      )
    )
  (:action move-right-from-r833
    :parameters ()
    :precondition 
      (in r833)
    :effect
      (and
        (in r834)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r833))
      )
    )
  (:action move-left-from-r833
    :parameters ()
    :precondition 
      (in r833)
    :effect
      (and
        (in r832)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r833))
      )
    )
  (:action move-right-from-r834
    :parameters ()
    :precondition 
      (in r834)
    :effect
      (and
        (in r835)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r834))
      )
    )
  (:action move-left-from-r834
    :parameters ()
    :precondition 
      (in r834)
    :effect
      (and
        (in r833)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r834))
      )
    )
  (:action move-right-from-r835
    :parameters ()
    :precondition 
      (in r835)
    :effect
      (and
        (in r836)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r835))
      )
    )
  (:action move-left-from-r835
    :parameters ()
    :precondition 
      (in r835)
    :effect
      (and
        (in r834)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r835))
      )
    )
  (:action move-right-from-r836
    :parameters ()
    :precondition 
      (in r836)
    :effect
      (and
        (in r837)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r836))
      )
    )
  (:action move-left-from-r836
    :parameters ()
    :precondition 
      (in r836)
    :effect
      (and
        (in r835)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r836))
      )
    )
  (:action move-right-from-r837
    :parameters ()
    :precondition 
      (in r837)
    :effect
      (and
        (in r838)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r837))
      )
    )
  (:action move-left-from-r837
    :parameters ()
    :precondition 
      (in r837)
    :effect
      (and
        (in r836)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r837))
      )
    )
  (:action move-right-from-r838
    :parameters ()
    :precondition 
      (in r838)
    :effect
      (and
        (in r839)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r838))
      )
    )
  (:action move-left-from-r838
    :parameters ()
    :precondition 
      (in r838)
    :effect
      (and
        (in r837)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r838))
      )
    )
  (:action move-right-from-r839
    :parameters ()
    :precondition 
      (in r839)
    :effect
      (and
        (in r840)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r839))
      )
    )
  (:action move-left-from-r839
    :parameters ()
    :precondition 
      (in r839)
    :effect
      (and
        (in r838)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r839))
      )
    )
  (:action move-right-from-r840
    :parameters ()
    :precondition 
      (in r840)
    :effect
      (and
        (in r841)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r840))
      )
    )
  (:action move-left-from-r840
    :parameters ()
    :precondition 
      (in r840)
    :effect
      (and
        (in r839)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r840))
      )
    )
  (:action move-right-from-r841
    :parameters ()
    :precondition 
      (in r841)
    :effect
      (and
        (in r842)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r841))
      )
    )
  (:action move-left-from-r841
    :parameters ()
    :precondition 
      (in r841)
    :effect
      (and
        (in r840)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r841))
      )
    )
  (:action move-right-from-r842
    :parameters ()
    :precondition 
      (in r842)
    :effect
      (and
        (in r843)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r842))
      )
    )
  (:action move-left-from-r842
    :parameters ()
    :precondition 
      (in r842)
    :effect
      (and
        (in r841)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r842))
      )
    )
  (:action move-right-from-r843
    :parameters ()
    :precondition 
      (in r843)
    :effect
      (and
        (in r844)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r843))
      )
    )
  (:action move-left-from-r843
    :parameters ()
    :precondition 
      (in r843)
    :effect
      (and
        (in r842)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r843))
      )
    )
  (:action move-right-from-r844
    :parameters ()
    :precondition 
      (in r844)
    :effect
      (and
        (in r845)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r844))
      )
    )
  (:action move-left-from-r844
    :parameters ()
    :precondition 
      (in r844)
    :effect
      (and
        (in r843)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r844))
      )
    )
  (:action move-right-from-r845
    :parameters ()
    :precondition 
      (in r845)
    :effect
      (and
        (in r846)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r845))
      )
    )
  (:action move-left-from-r845
    :parameters ()
    :precondition 
      (in r845)
    :effect
      (and
        (in r844)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r845))
      )
    )
  (:action move-right-from-r846
    :parameters ()
    :precondition 
      (in r846)
    :effect
      (and
        (in r847)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r846))
      )
    )
  (:action move-left-from-r846
    :parameters ()
    :precondition 
      (in r846)
    :effect
      (and
        (in r845)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r846))
      )
    )
  (:action move-right-from-r847
    :parameters ()
    :precondition 
      (in r847)
    :effect
      (and
        (in r848)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r847))
      )
    )
  (:action move-left-from-r847
    :parameters ()
    :precondition 
      (in r847)
    :effect
      (and
        (in r846)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r847))
      )
    )
  (:action move-right-from-r848
    :parameters ()
    :precondition 
      (in r848)
    :effect
      (and
        (in r849)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r848))
      )
    )
  (:action move-left-from-r848
    :parameters ()
    :precondition 
      (in r848)
    :effect
      (and
        (in r847)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r848))
      )
    )
  (:action move-right-from-r849
    :parameters ()
    :precondition 
      (in r849)
    :effect
      (and
        (in r850)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r849))
      )
    )
  (:action move-left-from-r849
    :parameters ()
    :precondition 
      (in r849)
    :effect
      (and
        (in r848)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r849))
      )
    )
  (:action move-right-from-r850
    :parameters ()
    :precondition 
      (in r850)
    :effect
      (and
        (in r851)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r850))
      )
    )
  (:action move-left-from-r850
    :parameters ()
    :precondition 
      (in r850)
    :effect
      (and
        (in r849)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r850))
      )
    )
  (:action move-right-from-r851
    :parameters ()
    :precondition 
      (in r851)
    :effect
      (and
        (in r852)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r851))
      )
    )
  (:action move-left-from-r851
    :parameters ()
    :precondition 
      (in r851)
    :effect
      (and
        (in r850)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r851))
      )
    )
  (:action move-right-from-r852
    :parameters ()
    :precondition 
      (in r852)
    :effect
      (and
        (in r853)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r852))
      )
    )
  (:action move-left-from-r852
    :parameters ()
    :precondition 
      (in r852)
    :effect
      (and
        (in r851)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r852))
      )
    )
  (:action move-right-from-r853
    :parameters ()
    :precondition 
      (in r853)
    :effect
      (and
        (in r854)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r853))
      )
    )
  (:action move-left-from-r853
    :parameters ()
    :precondition 
      (in r853)
    :effect
      (and
        (in r852)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r853))
      )
    )
  (:action move-right-from-r854
    :parameters ()
    :precondition 
      (in r854)
    :effect
      (and
        (in r855)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r854))
      )
    )
  (:action move-left-from-r854
    :parameters ()
    :precondition 
      (in r854)
    :effect
      (and
        (in r853)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r854))
      )
    )
  (:action move-right-from-r855
    :parameters ()
    :precondition 
      (in r855)
    :effect
      (and
        (in r856)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r855))
      )
    )
  (:action move-left-from-r855
    :parameters ()
    :precondition 
      (in r855)
    :effect
      (and
        (in r854)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r855))
      )
    )
  (:action move-right-from-r856
    :parameters ()
    :precondition 
      (in r856)
    :effect
      (and
        (in r857)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r856))
      )
    )
  (:action move-left-from-r856
    :parameters ()
    :precondition 
      (in r856)
    :effect
      (and
        (in r855)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r856))
      )
    )
  (:action move-right-from-r857
    :parameters ()
    :precondition 
      (in r857)
    :effect
      (and
        (in r858)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r857))
      )
    )
  (:action move-left-from-r857
    :parameters ()
    :precondition 
      (in r857)
    :effect
      (and
        (in r856)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r857))
      )
    )
  (:action move-right-from-r858
    :parameters ()
    :precondition 
      (in r858)
    :effect
      (and
        (in r859)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r858))
      )
    )
  (:action move-left-from-r858
    :parameters ()
    :precondition 
      (in r858)
    :effect
      (and
        (in r857)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r858))
      )
    )
  (:action move-right-from-r859
    :parameters ()
    :precondition 
      (in r859)
    :effect
      (and
        (in r860)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r859))
      )
    )
  (:action move-left-from-r859
    :parameters ()
    :precondition 
      (in r859)
    :effect
      (and
        (in r858)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r859))
      )
    )
  (:action move-right-from-r860
    :parameters ()
    :precondition 
      (in r860)
    :effect
      (and
        (in r861)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r860))
      )
    )
  (:action move-left-from-r860
    :parameters ()
    :precondition 
      (in r860)
    :effect
      (and
        (in r859)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r860))
      )
    )
  (:action move-right-from-r861
    :parameters ()
    :precondition 
      (in r861)
    :effect
      (and
        (in r862)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r861))
      )
    )
  (:action move-left-from-r861
    :parameters ()
    :precondition 
      (in r861)
    :effect
      (and
        (in r860)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r861))
      )
    )
  (:action move-right-from-r862
    :parameters ()
    :precondition 
      (in r862)
    :effect
      (and
        (in r863)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r862))
      )
    )
  (:action move-left-from-r862
    :parameters ()
    :precondition 
      (in r862)
    :effect
      (and
        (in r861)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r862))
      )
    )
  (:action move-right-from-r863
    :parameters ()
    :precondition 
      (in r863)
    :effect
      (and
        (in r864)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r863))
      )
    )
  (:action move-left-from-r863
    :parameters ()
    :precondition 
      (in r863)
    :effect
      (and
        (in r862)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r863))
      )
    )
  (:action move-right-from-r864
    :parameters ()
    :precondition 
      (in r864)
    :effect
      (and
        (in r865)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r864))
      )
    )
  (:action move-left-from-r864
    :parameters ()
    :precondition 
      (in r864)
    :effect
      (and
        (in r863)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r864))
      )
    )
  (:action move-right-from-r865
    :parameters ()
    :precondition 
      (in r865)
    :effect
      (and
        (in r866)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r865))
      )
    )
  (:action move-left-from-r865
    :parameters ()
    :precondition 
      (in r865)
    :effect
      (and
        (in r864)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r865))
      )
    )
  (:action move-right-from-r866
    :parameters ()
    :precondition 
      (in r866)
    :effect
      (and
        (in r867)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r866))
      )
    )
  (:action move-left-from-r866
    :parameters ()
    :precondition 
      (in r866)
    :effect
      (and
        (in r865)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r866))
      )
    )
  (:action move-right-from-r867
    :parameters ()
    :precondition 
      (in r867)
    :effect
      (and
        (in r868)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r867))
      )
    )
  (:action move-left-from-r867
    :parameters ()
    :precondition 
      (in r867)
    :effect
      (and
        (in r866)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r867))
      )
    )
  (:action move-right-from-r868
    :parameters ()
    :precondition 
      (in r868)
    :effect
      (and
        (in r869)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r868))
      )
    )
  (:action move-left-from-r868
    :parameters ()
    :precondition 
      (in r868)
    :effect
      (and
        (in r867)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r868))
      )
    )
  (:action move-right-from-r869
    :parameters ()
    :precondition 
      (in r869)
    :effect
      (and
        (in r870)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r869))
      )
    )
  (:action move-left-from-r869
    :parameters ()
    :precondition 
      (in r869)
    :effect
      (and
        (in r868)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r869))
      )
    )
  (:action move-right-from-r870
    :parameters ()
    :precondition 
      (in r870)
    :effect
      (and
        (in r871)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r870))
      )
    )
  (:action move-left-from-r870
    :parameters ()
    :precondition 
      (in r870)
    :effect
      (and
        (in r869)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r870))
      )
    )
  (:action move-right-from-r871
    :parameters ()
    :precondition 
      (in r871)
    :effect
      (and
        (in r872)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r871))
      )
    )
  (:action move-left-from-r871
    :parameters ()
    :precondition 
      (in r871)
    :effect
      (and
        (in r870)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r871))
      )
    )
  (:action move-right-from-r872
    :parameters ()
    :precondition 
      (in r872)
    :effect
      (and
        (in r873)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r872))
      )
    )
  (:action move-left-from-r872
    :parameters ()
    :precondition 
      (in r872)
    :effect
      (and
        (in r871)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r872))
      )
    )
  (:action move-right-from-r873
    :parameters ()
    :precondition 
      (in r873)
    :effect
      (and
        (in r874)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r873))
      )
    )
  (:action move-left-from-r873
    :parameters ()
    :precondition 
      (in r873)
    :effect
      (and
        (in r872)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r873))
      )
    )
  (:action move-right-from-r874
    :parameters ()
    :precondition 
      (in r874)
    :effect
      (and
        (in r875)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r874))
      )
    )
  (:action move-left-from-r874
    :parameters ()
    :precondition 
      (in r874)
    :effect
      (and
        (in r873)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r874))
      )
    )
  (:action move-right-from-r875
    :parameters ()
    :precondition 
      (in r875)
    :effect
      (and
        (in r876)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r875))
      )
    )
  (:action move-left-from-r875
    :parameters ()
    :precondition 
      (in r875)
    :effect
      (and
        (in r874)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r875))
      )
    )
  (:action move-right-from-r876
    :parameters ()
    :precondition 
      (in r876)
    :effect
      (and
        (in r877)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r876))
      )
    )
  (:action move-left-from-r876
    :parameters ()
    :precondition 
      (in r876)
    :effect
      (and
        (in r875)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r876))
      )
    )
  (:action move-right-from-r877
    :parameters ()
    :precondition 
      (in r877)
    :effect
      (and
        (in r878)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r877))
      )
    )
  (:action move-left-from-r877
    :parameters ()
    :precondition 
      (in r877)
    :effect
      (and
        (in r876)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r877))
      )
    )
  (:action move-right-from-r878
    :parameters ()
    :precondition 
      (in r878)
    :effect
      (and
        (in r879)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r878))
      )
    )
  (:action move-left-from-r878
    :parameters ()
    :precondition 
      (in r878)
    :effect
      (and
        (in r877)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r878))
      )
    )
  (:action move-right-from-r879
    :parameters ()
    :precondition 
      (in r879)
    :effect
      (and
        (in r880)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r879))
      )
    )
  (:action move-left-from-r879
    :parameters ()
    :precondition 
      (in r879)
    :effect
      (and
        (in r878)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r879))
      )
    )
  (:action move-right-from-r880
    :parameters ()
    :precondition 
      (in r880)
    :effect
      (and
        (in r881)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r880))
      )
    )
  (:action move-left-from-r880
    :parameters ()
    :precondition 
      (in r880)
    :effect
      (and
        (in r879)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r880))
      )
    )
  (:action move-right-from-r881
    :parameters ()
    :precondition 
      (in r881)
    :effect
      (and
        (in r882)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r881))
      )
    )
  (:action move-left-from-r881
    :parameters ()
    :precondition 
      (in r881)
    :effect
      (and
        (in r880)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r881))
      )
    )
  (:action move-right-from-r882
    :parameters ()
    :precondition 
      (in r882)
    :effect
      (and
        (in r883)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r882))
      )
    )
  (:action move-left-from-r882
    :parameters ()
    :precondition 
      (in r882)
    :effect
      (and
        (in r881)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r882))
      )
    )
  (:action move-right-from-r883
    :parameters ()
    :precondition 
      (in r883)
    :effect
      (and
        (in r884)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r883))
      )
    )
  (:action move-left-from-r883
    :parameters ()
    :precondition 
      (in r883)
    :effect
      (and
        (in r882)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r883))
      )
    )
  (:action move-right-from-r884
    :parameters ()
    :precondition 
      (in r884)
    :effect
      (and
        (in r885)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r884))
      )
    )
  (:action move-left-from-r884
    :parameters ()
    :precondition 
      (in r884)
    :effect
      (and
        (in r883)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r884))
      )
    )
  (:action move-right-from-r885
    :parameters ()
    :precondition 
      (in r885)
    :effect
      (and
        (in r886)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r885))
      )
    )
  (:action move-left-from-r885
    :parameters ()
    :precondition 
      (in r885)
    :effect
      (and
        (in r884)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r885))
      )
    )
  (:action move-right-from-r886
    :parameters ()
    :precondition 
      (in r886)
    :effect
      (and
        (in r887)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r886))
      )
    )
  (:action move-left-from-r886
    :parameters ()
    :precondition 
      (in r886)
    :effect
      (and
        (in r885)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r886))
      )
    )
  (:action move-right-from-r887
    :parameters ()
    :precondition 
      (in r887)
    :effect
      (and
        (in r888)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r887))
      )
    )
  (:action move-left-from-r887
    :parameters ()
    :precondition 
      (in r887)
    :effect
      (and
        (in r886)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r887))
      )
    )
  (:action move-right-from-r888
    :parameters ()
    :precondition 
      (in r888)
    :effect
      (and
        (in r889)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r888))
      )
    )
  (:action move-left-from-r888
    :parameters ()
    :precondition 
      (in r888)
    :effect
      (and
        (in r887)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r888))
      )
    )
  (:action move-right-from-r889
    :parameters ()
    :precondition 
      (in r889)
    :effect
      (and
        (in r890)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r889))
      )
    )
  (:action move-left-from-r889
    :parameters ()
    :precondition 
      (in r889)
    :effect
      (and
        (in r888)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r889))
      )
    )
  (:action move-right-from-r890
    :parameters ()
    :precondition 
      (in r890)
    :effect
      (and
        (in r891)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r890))
      )
    )
  (:action move-left-from-r890
    :parameters ()
    :precondition 
      (in r890)
    :effect
      (and
        (in r889)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r890))
      )
    )
  (:action move-right-from-r891
    :parameters ()
    :precondition 
      (in r891)
    :effect
      (and
        (in r892)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r891))
      )
    )
  (:action move-left-from-r891
    :parameters ()
    :precondition 
      (in r891)
    :effect
      (and
        (in r890)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r891))
      )
    )
  (:action move-right-from-r892
    :parameters ()
    :precondition 
      (in r892)
    :effect
      (and
        (in r893)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r892))
      )
    )
  (:action move-left-from-r892
    :parameters ()
    :precondition 
      (in r892)
    :effect
      (and
        (in r891)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r892))
      )
    )
  (:action move-right-from-r893
    :parameters ()
    :precondition 
      (in r893)
    :effect
      (and
        (in r894)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r893))
      )
    )
  (:action move-left-from-r893
    :parameters ()
    :precondition 
      (in r893)
    :effect
      (and
        (in r892)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r893))
      )
    )
  (:action move-right-from-r894
    :parameters ()
    :precondition 
      (in r894)
    :effect
      (and
        (in r895)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r894))
      )
    )
  (:action move-left-from-r894
    :parameters ()
    :precondition 
      (in r894)
    :effect
      (and
        (in r893)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r894))
      )
    )
  (:action move-right-from-r895
    :parameters ()
    :precondition 
      (in r895)
    :effect
      (and
        (in r896)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r895))
      )
    )
  (:action move-left-from-r895
    :parameters ()
    :precondition 
      (in r895)
    :effect
      (and
        (in r894)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r895))
      )
    )
  (:action move-right-from-r896
    :parameters ()
    :precondition 
      (in r896)
    :effect
      (and
        (in r897)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r896))
      )
    )
  (:action move-left-from-r896
    :parameters ()
    :precondition 
      (in r896)
    :effect
      (and
        (in r895)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r896))
      )
    )
  (:action move-right-from-r897
    :parameters ()
    :precondition 
      (in r897)
    :effect
      (and
        (in r898)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r897))
      )
    )
  (:action move-left-from-r897
    :parameters ()
    :precondition 
      (in r897)
    :effect
      (and
        (in r896)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r897))
      )
    )
  (:action move-right-from-r898
    :parameters ()
    :precondition 
      (in r898)
    :effect
      (and
        (in r899)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r898))
      )
    )
  (:action move-left-from-r898
    :parameters ()
    :precondition 
      (in r898)
    :effect
      (and
        (in r897)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r898))
      )
    )
  (:action move-right-from-r899
    :parameters ()
    :precondition 
      (in r899)
    :effect
      (and
        (in r900)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r899))
      )
    )
  (:action move-left-from-r899
    :parameters ()
    :precondition 
      (in r899)
    :effect
      (and
        (in r898)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r899))
      )
    )
  (:action move-right-from-r900
    :parameters ()
    :precondition 
      (in r900)
    :effect
      (and
        (in r901)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r900))
      )
    )
  (:action move-left-from-r900
    :parameters ()
    :precondition 
      (in r900)
    :effect
      (and
        (in r899)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r900))
      )
    )
  (:action move-right-from-r901
    :parameters ()
    :precondition 
      (in r901)
    :effect
      (and
        (in r902)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r901))
      )
    )
  (:action move-left-from-r901
    :parameters ()
    :precondition 
      (in r901)
    :effect
      (and
        (in r900)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r901))
      )
    )
  (:action move-right-from-r902
    :parameters ()
    :precondition 
      (in r902)
    :effect
      (and
        (in r903)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r902))
      )
    )
  (:action move-left-from-r902
    :parameters ()
    :precondition 
      (in r902)
    :effect
      (and
        (in r901)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r902))
      )
    )
  (:action move-right-from-r903
    :parameters ()
    :precondition 
      (in r903)
    :effect
      (and
        (in r904)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r903))
      )
    )
  (:action move-left-from-r903
    :parameters ()
    :precondition 
      (in r903)
    :effect
      (and
        (in r902)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r903))
      )
    )
  (:action move-right-from-r904
    :parameters ()
    :precondition 
      (in r904)
    :effect
      (and
        (in r905)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r904))
      )
    )
  (:action move-left-from-r904
    :parameters ()
    :precondition 
      (in r904)
    :effect
      (and
        (in r903)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r904))
      )
    )
  (:action move-right-from-r905
    :parameters ()
    :precondition 
      (in r905)
    :effect
      (and
        (in r906)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r905))
      )
    )
  (:action move-left-from-r905
    :parameters ()
    :precondition 
      (in r905)
    :effect
      (and
        (in r904)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r905))
      )
    )
  (:action move-right-from-r906
    :parameters ()
    :precondition 
      (in r906)
    :effect
      (and
        (in r907)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r906))
      )
    )
  (:action move-left-from-r906
    :parameters ()
    :precondition 
      (in r906)
    :effect
      (and
        (in r905)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r906))
      )
    )
  (:action move-right-from-r907
    :parameters ()
    :precondition 
      (in r907)
    :effect
      (and
        (in r908)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r907))
      )
    )
  (:action move-left-from-r907
    :parameters ()
    :precondition 
      (in r907)
    :effect
      (and
        (in r906)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r907))
      )
    )
  (:action move-right-from-r908
    :parameters ()
    :precondition 
      (in r908)
    :effect
      (and
        (in r909)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r908))
      )
    )
  (:action move-left-from-r908
    :parameters ()
    :precondition 
      (in r908)
    :effect
      (and
        (in r907)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r908))
      )
    )
  (:action move-right-from-r909
    :parameters ()
    :precondition 
      (in r909)
    :effect
      (and
        (in r910)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r909))
      )
    )
  (:action move-left-from-r909
    :parameters ()
    :precondition 
      (in r909)
    :effect
      (and
        (in r908)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r909))
      )
    )
  (:action move-right-from-r910
    :parameters ()
    :precondition 
      (in r910)
    :effect
      (and
        (in r911)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r910))
      )
    )
  (:action move-left-from-r910
    :parameters ()
    :precondition 
      (in r910)
    :effect
      (and
        (in r909)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r910))
      )
    )
  (:action move-right-from-r911
    :parameters ()
    :precondition 
      (in r911)
    :effect
      (and
        (in r912)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r911))
      )
    )
  (:action move-left-from-r911
    :parameters ()
    :precondition 
      (in r911)
    :effect
      (and
        (in r910)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r911))
      )
    )
  (:action move-right-from-r912
    :parameters ()
    :precondition 
      (in r912)
    :effect
      (and
        (in r913)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r912))
      )
    )
  (:action move-left-from-r912
    :parameters ()
    :precondition 
      (in r912)
    :effect
      (and
        (in r911)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r912))
      )
    )
  (:action move-right-from-r913
    :parameters ()
    :precondition 
      (in r913)
    :effect
      (and
        (in r914)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r913))
      )
    )
  (:action move-left-from-r913
    :parameters ()
    :precondition 
      (in r913)
    :effect
      (and
        (in r912)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r913))
      )
    )
  (:action move-right-from-r914
    :parameters ()
    :precondition 
      (in r914)
    :effect
      (and
        (in r915)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r914))
      )
    )
  (:action move-left-from-r914
    :parameters ()
    :precondition 
      (in r914)
    :effect
      (and
        (in r913)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r914))
      )
    )
  (:action move-right-from-r915
    :parameters ()
    :precondition 
      (in r915)
    :effect
      (and
        (in r916)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r915))
      )
    )
  (:action move-left-from-r915
    :parameters ()
    :precondition 
      (in r915)
    :effect
      (and
        (in r914)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r915))
      )
    )
  (:action move-right-from-r916
    :parameters ()
    :precondition 
      (in r916)
    :effect
      (and
        (in r917)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r916))
      )
    )
  (:action move-left-from-r916
    :parameters ()
    :precondition 
      (in r916)
    :effect
      (and
        (in r915)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r916))
      )
    )
  (:action move-right-from-r917
    :parameters ()
    :precondition 
      (in r917)
    :effect
      (and
        (in r918)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r917))
      )
    )
  (:action move-left-from-r917
    :parameters ()
    :precondition 
      (in r917)
    :effect
      (and
        (in r916)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r917))
      )
    )
  (:action move-right-from-r918
    :parameters ()
    :precondition 
      (in r918)
    :effect
      (and
        (in r919)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r918))
      )
    )
  (:action move-left-from-r918
    :parameters ()
    :precondition 
      (in r918)
    :effect
      (and
        (in r917)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r918))
      )
    )
  (:action move-right-from-r919
    :parameters ()
    :precondition 
      (in r919)
    :effect
      (and
        (in r920)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r919))
      )
    )
  (:action move-left-from-r919
    :parameters ()
    :precondition 
      (in r919)
    :effect
      (and
        (in r918)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r919))
      )
    )
  (:action move-right-from-r920
    :parameters ()
    :precondition 
      (in r920)
    :effect
      (and
        (in r921)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r920))
      )
    )
  (:action move-left-from-r920
    :parameters ()
    :precondition 
      (in r920)
    :effect
      (and
        (in r919)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r920))
      )
    )
  (:action move-right-from-r921
    :parameters ()
    :precondition 
      (in r921)
    :effect
      (and
        (in r922)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r921))
      )
    )
  (:action move-left-from-r921
    :parameters ()
    :precondition 
      (in r921)
    :effect
      (and
        (in r920)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r921))
      )
    )
  (:action move-right-from-r922
    :parameters ()
    :precondition 
      (in r922)
    :effect
      (and
        (in r923)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r922))
      )
    )
  (:action move-left-from-r922
    :parameters ()
    :precondition 
      (in r922)
    :effect
      (and
        (in r921)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r922))
      )
    )
  (:action move-right-from-r923
    :parameters ()
    :precondition 
      (in r923)
    :effect
      (and
        (in r924)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r923))
      )
    )
  (:action move-left-from-r923
    :parameters ()
    :precondition 
      (in r923)
    :effect
      (and
        (in r922)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r923))
      )
    )
  (:action move-right-from-r924
    :parameters ()
    :precondition 
      (in r924)
    :effect
      (and
        (in r925)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r924))
      )
    )
  (:action move-left-from-r924
    :parameters ()
    :precondition 
      (in r924)
    :effect
      (and
        (in r923)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r924))
      )
    )
  (:action move-right-from-r925
    :parameters ()
    :precondition 
      (in r925)
    :effect
      (and
        (in r926)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r925))
      )
    )
  (:action move-left-from-r925
    :parameters ()
    :precondition 
      (in r925)
    :effect
      (and
        (in r924)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r925))
      )
    )
  (:action move-right-from-r926
    :parameters ()
    :precondition 
      (in r926)
    :effect
      (and
        (in r927)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r926))
      )
    )
  (:action move-left-from-r926
    :parameters ()
    :precondition 
      (in r926)
    :effect
      (and
        (in r925)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r926))
      )
    )
  (:action move-right-from-r927
    :parameters ()
    :precondition 
      (in r927)
    :effect
      (and
        (in r928)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r927))
      )
    )
  (:action move-left-from-r927
    :parameters ()
    :precondition 
      (in r927)
    :effect
      (and
        (in r926)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r927))
      )
    )
  (:action move-right-from-r928
    :parameters ()
    :precondition 
      (in r928)
    :effect
      (and
        (in r929)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r928))
      )
    )
  (:action move-left-from-r928
    :parameters ()
    :precondition 
      (in r928)
    :effect
      (and
        (in r927)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r928))
      )
    )
  (:action move-right-from-r929
    :parameters ()
    :precondition 
      (in r929)
    :effect
      (and
        (in r930)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r929))
      )
    )
  (:action move-left-from-r929
    :parameters ()
    :precondition 
      (in r929)
    :effect
      (and
        (in r928)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r929))
      )
    )
  (:action move-right-from-r930
    :parameters ()
    :precondition 
      (in r930)
    :effect
      (and
        (in r931)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r930))
      )
    )
  (:action move-left-from-r930
    :parameters ()
    :precondition 
      (in r930)
    :effect
      (and
        (in r929)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r930))
      )
    )
  (:action move-right-from-r931
    :parameters ()
    :precondition 
      (in r931)
    :effect
      (and
        (in r932)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r931))
      )
    )
  (:action move-left-from-r931
    :parameters ()
    :precondition 
      (in r931)
    :effect
      (and
        (in r930)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r931))
      )
    )
  (:action move-right-from-r932
    :parameters ()
    :precondition 
      (in r932)
    :effect
      (and
        (in r933)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r932))
      )
    )
  (:action move-left-from-r932
    :parameters ()
    :precondition 
      (in r932)
    :effect
      (and
        (in r931)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r932))
      )
    )
  (:action move-right-from-r933
    :parameters ()
    :precondition 
      (in r933)
    :effect
      (and
        (in r934)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r933))
      )
    )
  (:action move-left-from-r933
    :parameters ()
    :precondition 
      (in r933)
    :effect
      (and
        (in r932)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r933))
      )
    )
  (:action move-right-from-r934
    :parameters ()
    :precondition 
      (in r934)
    :effect
      (and
        (in r935)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r934))
      )
    )
  (:action move-left-from-r934
    :parameters ()
    :precondition 
      (in r934)
    :effect
      (and
        (in r933)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r934))
      )
    )
  (:action move-right-from-r935
    :parameters ()
    :precondition 
      (in r935)
    :effect
      (and
        (in r936)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r935))
      )
    )
  (:action move-left-from-r935
    :parameters ()
    :precondition 
      (in r935)
    :effect
      (and
        (in r934)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r935))
      )
    )
  (:action move-right-from-r936
    :parameters ()
    :precondition 
      (in r936)
    :effect
      (and
        (in r937)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r936))
      )
    )
  (:action move-left-from-r936
    :parameters ()
    :precondition 
      (in r936)
    :effect
      (and
        (in r935)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r936))
      )
    )
  (:action move-right-from-r937
    :parameters ()
    :precondition 
      (in r937)
    :effect
      (and
        (in r938)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r937))
      )
    )
  (:action move-left-from-r937
    :parameters ()
    :precondition 
      (in r937)
    :effect
      (and
        (in r936)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r937))
      )
    )
  (:action move-right-from-r938
    :parameters ()
    :precondition 
      (in r938)
    :effect
      (and
        (in r939)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r938))
      )
    )
  (:action move-left-from-r938
    :parameters ()
    :precondition 
      (in r938)
    :effect
      (and
        (in r937)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r938))
      )
    )
  (:action move-right-from-r939
    :parameters ()
    :precondition 
      (in r939)
    :effect
      (and
        (in r940)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r939))
      )
    )
  (:action move-left-from-r939
    :parameters ()
    :precondition 
      (in r939)
    :effect
      (and
        (in r938)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r939))
      )
    )
  (:action move-right-from-r940
    :parameters ()
    :precondition 
      (in r940)
    :effect
      (and
        (in r941)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r940))
      )
    )
  (:action move-left-from-r940
    :parameters ()
    :precondition 
      (in r940)
    :effect
      (and
        (in r939)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r940))
      )
    )
  (:action move-right-from-r941
    :parameters ()
    :precondition 
      (in r941)
    :effect
      (and
        (in r942)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r941))
      )
    )
  (:action move-left-from-r941
    :parameters ()
    :precondition 
      (in r941)
    :effect
      (and
        (in r940)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r941))
      )
    )
  (:action move-right-from-r942
    :parameters ()
    :precondition 
      (in r942)
    :effect
      (and
        (in r943)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r942))
      )
    )
  (:action move-left-from-r942
    :parameters ()
    :precondition 
      (in r942)
    :effect
      (and
        (in r941)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r942))
      )
    )
  (:action move-right-from-r943
    :parameters ()
    :precondition 
      (in r943)
    :effect
      (and
        (in r944)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r943))
      )
    )
  (:action move-left-from-r943
    :parameters ()
    :precondition 
      (in r943)
    :effect
      (and
        (in r942)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r943))
      )
    )
  (:action move-right-from-r944
    :parameters ()
    :precondition 
      (in r944)
    :effect
      (and
        (in r945)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r944))
      )
    )
  (:action move-left-from-r944
    :parameters ()
    :precondition 
      (in r944)
    :effect
      (and
        (in r943)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r944))
      )
    )
  (:action move-right-from-r945
    :parameters ()
    :precondition 
      (in r945)
    :effect
      (and
        (in r946)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r945))
      )
    )
  (:action move-left-from-r945
    :parameters ()
    :precondition 
      (in r945)
    :effect
      (and
        (in r944)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r945))
      )
    )
  (:action move-right-from-r946
    :parameters ()
    :precondition 
      (in r946)
    :effect
      (and
        (in r947)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r946))
      )
    )
  (:action move-left-from-r946
    :parameters ()
    :precondition 
      (in r946)
    :effect
      (and
        (in r945)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r946))
      )
    )
  (:action move-right-from-r947
    :parameters ()
    :precondition 
      (in r947)
    :effect
      (and
        (in r948)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r947))
      )
    )
  (:action move-left-from-r947
    :parameters ()
    :precondition 
      (in r947)
    :effect
      (and
        (in r946)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r947))
      )
    )
  (:action move-right-from-r948
    :parameters ()
    :precondition 
      (in r948)
    :effect
      (and
        (in r949)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r948))
      )
    )
  (:action move-left-from-r948
    :parameters ()
    :precondition 
      (in r948)
    :effect
      (and
        (in r947)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r948))
      )
    )
  (:action move-right-from-r949
    :parameters ()
    :precondition 
      (in r949)
    :effect
      (and
        (in r950)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r949))
      )
    )
  (:action move-left-from-r949
    :parameters ()
    :precondition 
      (in r949)
    :effect
      (and
        (in r948)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r949))
      )
    )
  (:action move-right-from-r950
    :parameters ()
    :precondition 
      (in r950)
    :effect
      (and
        (in r951)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r950))
      )
    )
  (:action move-left-from-r950
    :parameters ()
    :precondition 
      (in r950)
    :effect
      (and
        (in r949)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r950))
      )
    )
  (:action move-right-from-r951
    :parameters ()
    :precondition 
      (in r951)
    :effect
      (and
        (in r952)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r951))
      )
    )
  (:action move-left-from-r951
    :parameters ()
    :precondition 
      (in r951)
    :effect
      (and
        (in r950)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r951))
      )
    )
  (:action move-right-from-r952
    :parameters ()
    :precondition 
      (in r952)
    :effect
      (and
        (in r953)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r952))
      )
    )
  (:action move-left-from-r952
    :parameters ()
    :precondition 
      (in r952)
    :effect
      (and
        (in r951)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r952))
      )
    )
  (:action move-right-from-r953
    :parameters ()
    :precondition 
      (in r953)
    :effect
      (and
        (in r954)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r953))
      )
    )
  (:action move-left-from-r953
    :parameters ()
    :precondition 
      (in r953)
    :effect
      (and
        (in r952)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r953))
      )
    )
  (:action move-right-from-r954
    :parameters ()
    :precondition 
      (in r954)
    :effect
      (and
        (in r955)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r954))
      )
    )
  (:action move-left-from-r954
    :parameters ()
    :precondition 
      (in r954)
    :effect
      (and
        (in r953)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r954))
      )
    )
  (:action move-right-from-r955
    :parameters ()
    :precondition 
      (in r955)
    :effect
      (and
        (in r956)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r955))
      )
    )
  (:action move-left-from-r955
    :parameters ()
    :precondition 
      (in r955)
    :effect
      (and
        (in r954)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r955))
      )
    )
  (:action move-right-from-r956
    :parameters ()
    :precondition 
      (in r956)
    :effect
      (and
        (in r957)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r956))
      )
    )
  (:action move-left-from-r956
    :parameters ()
    :precondition 
      (in r956)
    :effect
      (and
        (in r955)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r956))
      )
    )
  (:action move-right-from-r957
    :parameters ()
    :precondition 
      (in r957)
    :effect
      (and
        (in r958)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r957))
      )
    )
  (:action move-left-from-r957
    :parameters ()
    :precondition 
      (in r957)
    :effect
      (and
        (in r956)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r957))
      )
    )
  (:action move-right-from-r958
    :parameters ()
    :precondition 
      (in r958)
    :effect
      (and
        (in r959)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r958))
      )
    )
  (:action move-left-from-r958
    :parameters ()
    :precondition 
      (in r958)
    :effect
      (and
        (in r957)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r958))
      )
    )
  (:action move-right-from-r959
    :parameters ()
    :precondition 
      (in r959)
    :effect
      (and
        (in r960)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r959))
      )
    )
  (:action move-left-from-r959
    :parameters ()
    :precondition 
      (in r959)
    :effect
      (and
        (in r958)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r959))
      )
    )
  (:action move-right-from-r960
    :parameters ()
    :precondition 
      (in r960)
    :effect
      (and
        (in r961)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r960))
      )
    )
  (:action move-left-from-r960
    :parameters ()
    :precondition 
      (in r960)
    :effect
      (and
        (in r959)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r960))
      )
    )
  (:action move-right-from-r961
    :parameters ()
    :precondition 
      (in r961)
    :effect
      (and
        (in r962)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r961))
      )
    )
  (:action move-left-from-r961
    :parameters ()
    :precondition 
      (in r961)
    :effect
      (and
        (in r960)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r961))
      )
    )
  (:action move-right-from-r962
    :parameters ()
    :precondition 
      (in r962)
    :effect
      (and
        (in r963)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r962))
      )
    )
  (:action move-left-from-r962
    :parameters ()
    :precondition 
      (in r962)
    :effect
      (and
        (in r961)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r962))
      )
    )
  (:action move-right-from-r963
    :parameters ()
    :precondition 
      (in r963)
    :effect
      (and
        (in r964)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r963))
      )
    )
  (:action move-left-from-r963
    :parameters ()
    :precondition 
      (in r963)
    :effect
      (and
        (in r962)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r963))
      )
    )
  (:action move-right-from-r964
    :parameters ()
    :precondition 
      (in r964)
    :effect
      (and
        (in r965)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r964))
      )
    )
  (:action move-left-from-r964
    :parameters ()
    :precondition 
      (in r964)
    :effect
      (and
        (in r963)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r964))
      )
    )
  (:action move-right-from-r965
    :parameters ()
    :precondition 
      (in r965)
    :effect
      (and
        (in r966)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r965))
      )
    )
  (:action move-left-from-r965
    :parameters ()
    :precondition 
      (in r965)
    :effect
      (and
        (in r964)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r965))
      )
    )
  (:action move-right-from-r966
    :parameters ()
    :precondition 
      (in r966)
    :effect
      (and
        (in r967)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r966))
      )
    )
  (:action move-left-from-r966
    :parameters ()
    :precondition 
      (in r966)
    :effect
      (and
        (in r965)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r966))
      )
    )
  (:action move-right-from-r967
    :parameters ()
    :precondition 
      (in r967)
    :effect
      (and
        (in r968)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r967))
      )
    )
  (:action move-left-from-r967
    :parameters ()
    :precondition 
      (in r967)
    :effect
      (and
        (in r966)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r967))
      )
    )
  (:action move-right-from-r968
    :parameters ()
    :precondition 
      (in r968)
    :effect
      (and
        (in r969)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r968))
      )
    )
  (:action move-left-from-r968
    :parameters ()
    :precondition 
      (in r968)
    :effect
      (and
        (in r967)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r968))
      )
    )
  (:action move-right-from-r969
    :parameters ()
    :precondition 
      (in r969)
    :effect
      (and
        (in r970)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r969))
      )
    )
  (:action move-left-from-r969
    :parameters ()
    :precondition 
      (in r969)
    :effect
      (and
        (in r968)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r969))
      )
    )
  (:action move-right-from-r970
    :parameters ()
    :precondition 
      (in r970)
    :effect
      (and
        (in r971)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r970))
      )
    )
  (:action move-left-from-r970
    :parameters ()
    :precondition 
      (in r970)
    :effect
      (and
        (in r969)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r970))
      )
    )
  (:action move-right-from-r971
    :parameters ()
    :precondition 
      (in r971)
    :effect
      (and
        (in r972)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r971))
      )
    )
  (:action move-left-from-r971
    :parameters ()
    :precondition 
      (in r971)
    :effect
      (and
        (in r970)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r971))
      )
    )
  (:action move-right-from-r972
    :parameters ()
    :precondition 
      (in r972)
    :effect
      (and
        (in r973)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r972))
      )
    )
  (:action move-left-from-r972
    :parameters ()
    :precondition 
      (in r972)
    :effect
      (and
        (in r971)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r972))
      )
    )
  (:action move-right-from-r973
    :parameters ()
    :precondition 
      (in r973)
    :effect
      (and
        (in r974)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r973))
      )
    )
  (:action move-left-from-r973
    :parameters ()
    :precondition 
      (in r973)
    :effect
      (and
        (in r972)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r973))
      )
    )
  (:action move-right-from-r974
    :parameters ()
    :precondition 
      (in r974)
    :effect
      (and
        (in r975)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r974))
      )
    )
  (:action move-left-from-r974
    :parameters ()
    :precondition 
      (in r974)
    :effect
      (and
        (in r973)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r974))
      )
    )
  (:action move-right-from-r975
    :parameters ()
    :precondition 
      (in r975)
    :effect
      (and
        (in r976)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r975))
      )
    )
  (:action move-left-from-r975
    :parameters ()
    :precondition 
      (in r975)
    :effect
      (and
        (in r974)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r975))
      )
    )
  (:action move-right-from-r976
    :parameters ()
    :precondition 
      (in r976)
    :effect
      (and
        (in r977)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r976))
      )
    )
  (:action move-left-from-r976
    :parameters ()
    :precondition 
      (in r976)
    :effect
      (and
        (in r975)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r976))
      )
    )
  (:action move-right-from-r977
    :parameters ()
    :precondition 
      (in r977)
    :effect
      (and
        (in r978)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r977))
      )
    )
  (:action move-left-from-r977
    :parameters ()
    :precondition 
      (in r977)
    :effect
      (and
        (in r976)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r977))
      )
    )
  (:action move-right-from-r978
    :parameters ()
    :precondition 
      (in r978)
    :effect
      (and
        (in r979)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r978))
      )
    )
  (:action move-left-from-r978
    :parameters ()
    :precondition 
      (in r978)
    :effect
      (and
        (in r977)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r978))
      )
    )
  (:action move-right-from-r979
    :parameters ()
    :precondition 
      (in r979)
    :effect
      (and
        (in r980)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r979))
      )
    )
  (:action move-left-from-r979
    :parameters ()
    :precondition 
      (in r979)
    :effect
      (and
        (in r978)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r979))
      )
    )
  (:action move-right-from-r980
    :parameters ()
    :precondition 
      (in r980)
    :effect
      (and
        (in r981)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r980))
      )
    )
  (:action move-left-from-r980
    :parameters ()
    :precondition 
      (in r980)
    :effect
      (and
        (in r979)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r980))
      )
    )
  (:action move-right-from-r981
    :parameters ()
    :precondition 
      (in r981)
    :effect
      (and
        (in r982)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r981))
      )
    )
  (:action move-left-from-r981
    :parameters ()
    :precondition 
      (in r981)
    :effect
      (and
        (in r980)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r981))
      )
    )
  (:action move-right-from-r982
    :parameters ()
    :precondition 
      (in r982)
    :effect
      (and
        (in r983)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r982))
      )
    )
  (:action move-left-from-r982
    :parameters ()
    :precondition 
      (in r982)
    :effect
      (and
        (in r981)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r982))
      )
    )
  (:action move-right-from-r983
    :parameters ()
    :precondition 
      (in r983)
    :effect
      (and
        (in r984)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r983))
      )
    )
  (:action move-left-from-r983
    :parameters ()
    :precondition 
      (in r983)
    :effect
      (and
        (in r982)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r983))
      )
    )
  (:action move-right-from-r984
    :parameters ()
    :precondition 
      (in r984)
    :effect
      (and
        (in r985)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r984))
      )
    )
  (:action move-left-from-r984
    :parameters ()
    :precondition 
      (in r984)
    :effect
      (and
        (in r983)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r984))
      )
    )
  (:action move-right-from-r985
    :parameters ()
    :precondition 
      (in r985)
    :effect
      (and
        (in r986)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r985))
      )
    )
  (:action move-left-from-r985
    :parameters ()
    :precondition 
      (in r985)
    :effect
      (and
        (in r984)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r985))
      )
    )
  (:action move-right-from-r986
    :parameters ()
    :precondition 
      (in r986)
    :effect
      (and
        (in r987)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r986))
      )
    )
  (:action move-left-from-r986
    :parameters ()
    :precondition 
      (in r986)
    :effect
      (and
        (in r985)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r986))
      )
    )
  (:action move-right-from-r987
    :parameters ()
    :precondition 
      (in r987)
    :effect
      (and
        (in r988)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r987))
      )
    )
  (:action move-left-from-r987
    :parameters ()
    :precondition 
      (in r987)
    :effect
      (and
        (in r986)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r987))
      )
    )
  (:action move-right-from-r988
    :parameters ()
    :precondition 
      (in r988)
    :effect
      (and
        (in r989)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r988))
      )
    )
  (:action move-left-from-r988
    :parameters ()
    :precondition 
      (in r988)
    :effect
      (and
        (in r987)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r988))
      )
    )
  (:action move-right-from-r989
    :parameters ()
    :precondition 
      (in r989)
    :effect
      (and
        (in r990)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r989))
      )
    )
  (:action move-left-from-r989
    :parameters ()
    :precondition 
      (in r989)
    :effect
      (and
        (in r988)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r989))
      )
    )
  (:action move-right-from-r990
    :parameters ()
    :precondition 
      (in r990)
    :effect
      (and
        (in r991)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r990))
      )
    )
  (:action move-left-from-r990
    :parameters ()
    :precondition 
      (in r990)
    :effect
      (and
        (in r989)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r990))
      )
    )
  (:action move-right-from-r991
    :parameters ()
    :precondition 
      (in r991)
    :effect
      (and
        (in r992)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r991))
      )
    )
  (:action move-left-from-r991
    :parameters ()
    :precondition 
      (in r991)
    :effect
      (and
        (in r990)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r991))
      )
    )
  (:action move-right-from-r992
    :parameters ()
    :precondition 
      (in r992)
    :effect
      (and
        (in r993)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r992))
      )
    )
  (:action move-left-from-r992
    :parameters ()
    :precondition 
      (in r992)
    :effect
      (and
        (in r991)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r992))
      )
    )
  (:action move-right-from-r993
    :parameters ()
    :precondition 
      (in r993)
    :effect
      (and
        (in r994)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r993))
      )
    )
  (:action move-left-from-r993
    :parameters ()
    :precondition 
      (in r993)
    :effect
      (and
        (in r992)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r993))
      )
    )
  (:action move-right-from-r994
    :parameters ()
    :precondition 
      (in r994)
    :effect
      (and
        (in r995)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r994))
      )
    )
  (:action move-left-from-r994
    :parameters ()
    :precondition 
      (in r994)
    :effect
      (and
        (in r993)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r994))
      )
    )
  (:action move-right-from-r995
    :parameters ()
    :precondition 
      (in r995)
    :effect
      (and
        (in r996)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r995))
      )
    )
  (:action move-left-from-r995
    :parameters ()
    :precondition 
      (in r995)
    :effect
      (and
        (in r994)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r995))
      )
    )
  (:action move-right-from-r996
    :parameters ()
    :precondition 
      (in r996)
    :effect
      (and
        (in r997)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r996))
      )
    )
  (:action move-left-from-r996
    :parameters ()
    :precondition 
      (in r996)
    :effect
      (and
        (in r995)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r996))
      )
    )
  (:action move-right-from-r997
    :parameters ()
    :precondition 
      (in r997)
    :effect
      (and
        (in r998)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r997))
      )
    )
  (:action move-left-from-r997
    :parameters ()
    :precondition 
      (in r997)
    :effect
      (and
        (in r996)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r997))
      )
    )
  (:action move-right-from-r998
    :parameters ()
    :precondition 
      (in r998)
    :effect
      (and
        (in r999)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r998))
      )
    )
  (:action move-left-from-r998
    :parameters ()
    :precondition 
      (in r998)
    :effect
      (and
        (in r997)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r998))
      )
    )
  (:action move-right-from-r999_detdup_0
    :parameters ()
    :precondition 
      (in r999)
    :effect
      (and
        (in r1000)
        (not 
          (in r999))
        (not 
          (seen))
      )
    )
  (:action move-right-from-r999_detdup_1
    :parameters ()
    :precondition 
      (in r999)
    :effect
      (and
        (in r1000)
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
        (not 
          (in r999))
      )
    )
  (:action move-left-from-r999
    :parameters ()
    :precondition 
      (in r999)
    :effect
      (and
        (in r998)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r999))
      )
    )
  (:action move-right-from-r1000
    :parameters ()
    :precondition 
      (and
        (in r1000)
        (not 
          (seen)))
    :effect
      (and
        (in r1)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r1000))
      )
    )
  (:action move-left-from-r1000
    :parameters ()
    :precondition 
      (and
        (in r1000)
        (not 
          (seen)))
    :effect
      (and
        (in r999)
        (when
          (and
            (autstate_1_2)
            (seen))
          (autstate_1_1))
        (not 
          (in r1000))
      )
    )
  (:action stay
    :parameters ()
    :precondition 
      (and
        (not 
          (in r500))
        (not 
          (in r1000)))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r500-seen
    :parameters ()
    :precondition 
      (and
        (in r500)
        (seen))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r500-not-seen_detdup_0
    :parameters ()
    :precondition 
      (and
        (in r500)
        (not 
          (seen)))
    :effect
(not 
        (seen))    )
  (:action stay-in-r500-not-seen_detdup_1
    :parameters ()
    :precondition 
      (and
        (in r500)
        (not 
          (seen)))
    :effect
      (and
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
      )
    )
  (:action stay-in-r1000-seen
    :parameters ()
    :precondition 
      (and
        (in r1000)
        (seen))
    :effect
(when
        (and
          (autstate_1_2)
          (seen))
        (autstate_1_1))    )
  (:action stay-in-r1000-not-seen_detdup_0
    :parameters ()
    :precondition 
      (and
        (in r1000)
        (not 
          (seen)))
    :effect
(not 
        (seen))    )
  (:action stay-in-r1000-not-seen_detdup_1
    :parameters ()
    :precondition 
      (and
        (in r1000)
        (not 
          (seen)))
    :effect
      (and
        (seen)
        (when
          (autstate_1_2)
          (autstate_1_1))
      )
    )
)