(define (domain blocks-world-domain)
  (:types
    default_object - none
  )

  (:predicates
    (on ?x0 ?x1)
    (clear ?x0)
    (block ?x0)
    (autstate_1_2)
    (autstate_1_1)
  )
  (:action puton_detdup_0
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 ?x1)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (on b c)
                (= ?x0 a)
                (= ?x1 b))
              (and
                (on a b)
                (= ?x0 b)
                (= ?x1 c))
              (and
                (on b c)
                (on a b)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 c)))
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 b))))))
          (autstate_1_1))
        (not 
          (on ?x0 ?x2))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
  (:action puton_detdup_1
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 table)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_2)
            (on b c)
            (on a b)
            (or
              (not 
                (= ?x0 b))
              (not 
                (= ?x2 c))
              (= c table))
            (or
              (not 
                (= ?x0 a))
              (not 
                (= ?x2 b))
              (= b table)))
          (autstate_1_1))
        (when
          (not 
            (= ?x2 table))
          (not 
            (on ?x0 ?x2)))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
)