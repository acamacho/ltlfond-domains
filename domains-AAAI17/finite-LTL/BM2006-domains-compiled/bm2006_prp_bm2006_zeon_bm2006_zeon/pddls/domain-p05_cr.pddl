(define (domain zeon)
  (:types
    default_object - none
  )

  (:predicates
    (aircraft ?x0)
    (person ?x0)
    (city ?x0)
    (flevel ?x0)
    (at ?x0 ?x1)
    (in ?x0 ?x1)
    (fuel_level ?x0 ?x1)
    (next_to ?x0 ?x1)
    (autstate_1_2)
    (autstate_1_1)
    (autstate_2_2)
    (autstate_2_1)
  )
  (:action board
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (person ?x0)
        (aircraft ?x1)
        (city ?x2)
        (at ?x0 ?x2)
        (at ?x1 ?x2))
    :effect
      (and
        (in ?x0 ?x1)
        (when
          (and
            (autstate_1_2)
            (at person1 city3)
            (or
              (not 
                (= ?x0 person1))
              (not 
                (= ?x2 city3))))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (at person2 city3)
            (or
              (not 
                (= ?x0 person2))
              (not 
                (= ?x2 city3))))
          (autstate_2_1))
        (not 
          (at ?x0 ?x2))
        (when
          (or
            (not 
              (autstate_1_2))
            (not 
              (at person1 city3))
            (and
              (= ?x0 person1)
              (= ?x2 city3)))
          (not 
            (autstate_1_1)))
        (when
          (or
            (not 
              (autstate_2_2))
            (not 
              (at person2 city3))
            (and
              (= ?x0 person2)
              (= ?x2 city3)))
          (not 
            (autstate_2_1)))
      )
    )
  (:action debark
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (person ?x0)
        (aircraft ?x1)
        (city ?x2)
        (in ?x0 ?x1)
        (at ?x1 ?x2))
    :effect
      (and
        (at ?x0 ?x2)
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 person1)
                (= ?x2 city3))
              (at person1 city3)))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (or
              (and
                (= ?x0 person2)
                (= ?x2 city3))
              (at person2 city3)))
          (autstate_2_1))
        (not 
          (in ?x0 ?x1))
        (when
          (or
            (not 
              (autstate_1_2))
            (and
              (or
                (not 
                  (= ?x0 person1))
                (not 
                  (= ?x2 city3)))
              (not 
                (at person1 city3))))
          (not 
            (autstate_1_1)))
        (when
          (or
            (not 
              (autstate_2_2))
            (and
              (or
                (not 
                  (= ?x0 person2))
                (not 
                  (= ?x2 city3)))
              (not 
                (at person2 city3))))
          (not 
            (autstate_2_1)))
      )
    )
  (:action fly
    :parameters (?x0 ?x1 ?x2 ?x3 ?x4)
    :precondition 
      (and
        (aircraft ?x0)
        (city ?x2)
        (at ?x0 ?x1)
        (fuel_level ?x0 ?x3)
        (next_to ?x4 ?x3))
    :effect
      (and
        (at ?x0 ?x2)
        (fuel_level ?x0 ?x4)
        (when
          (and
            (autstate_1_2)
            (at person1 city3))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (at person2 city3))
          (autstate_2_1))
        (not 
          (fuel_level ?x0 ?x3))
        (not 
          (at ?x0 ?x1))
        (when
          (or
            (not 
              (autstate_1_2))
            (not 
              (at person1 city3)))
          (not 
            (autstate_1_1)))
        (when
          (or
            (not 
              (autstate_2_2))
            (not 
              (at person2 city3)))
          (not 
            (autstate_2_1)))
      )
    )
  (:action refuel
    :parameters (?x0 ?x1 ?x2 ?x3)
    :precondition 
      (and
        (aircraft ?x0)
        (city ?x1)
        (fuel_level ?x0 ?x2)
        (next_to ?x2 ?x3))
    :effect
      (and
        (fuel_level ?x0 ?x3)
        (when
          (and
            (autstate_1_2)
            (at person1 city3))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (at person2 city3))
          (autstate_2_1))
        (not 
          (fuel_level ?x0 ?x2))
        (when
          (or
            (not 
              (autstate_1_2))
            (not 
              (at person1 city3)))
          (not 
            (autstate_1_1)))
        (when
          (or
            (not 
              (autstate_2_2))
            (not 
              (at person2 city3)))
          (not 
            (autstate_2_1)))
      )
    )
)