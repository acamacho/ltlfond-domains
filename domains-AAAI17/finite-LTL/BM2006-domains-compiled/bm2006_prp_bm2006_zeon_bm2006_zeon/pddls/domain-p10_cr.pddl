(define (domain zeon)
  (:types
    default_object - none
  )

  (:predicates
    (aircraft ?x0)
    (person ?x0)
    (city ?x0)
    (flevel ?x0)
    (at ?x0 ?x1)
    (in ?x0 ?x1)
    (fuel_level ?x0 ?x1)
    (next_to ?x0 ?x1)
    (autstate_1_2)
    (autstate_1_3)
    (autstate_1_1)
    (autstate_2_2)
    (autstate_2_3)
    (autstate_2_1)
    (autstate_3_2)
    (autstate_3_3)
    (autstate_3_1)
  )
  (:action board
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (person ?x0)
        (aircraft ?x1)
        (city ?x2)
        (at ?x0 ?x2)
        (at ?x1 ?x2))
    :effect
      (and
        (in ?x0 ?x1)
        (when
          (and
            (autstate_1_2)
            (at plane2 city2))
          (autstate_1_3))
        (when
          (and
            (at plane2 city3)
            (or
              (autstate_1_3)
              (and
                (autstate_1_2)
                (at plane2 city2))))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (at plane1 city2))
          (autstate_2_3))
        (when
          (and
            (at plane1 city0)
            (or
              (autstate_2_3)
              (and
                (autstate_2_2)
                (at plane1 city2))))
          (autstate_2_1))
        (when
          (and
            (autstate_3_2)
            (at plane3 city4))
          (autstate_3_3))
        (when
          (and
            (at plane3 city0)
            (or
              (autstate_3_3)
              (and
                (autstate_3_2)
                (at plane3 city4))))
          (autstate_3_1))
        (not 
          (at ?x0 ?x2))
        (when
          (and
            (not 
              (at plane2 city3))
            (autstate_1_2))
          (not 
            (autstate_1_2)))
        (when
          (and
            (not 
              (at plane2 city2))
            (autstate_1_3))
          (not 
            (autstate_1_3)))
        (when
          (and
            (not 
              (at plane1 city0))
            (autstate_2_2))
          (not 
            (autstate_2_2)))
        (when
          (and
            (not 
              (at plane1 city2))
            (autstate_2_3))
          (not 
            (autstate_2_3)))
        (when
          (and
            (not 
              (at plane3 city0))
            (autstate_3_2))
          (not 
            (autstate_3_2)))
        (when
          (and
            (not 
              (at plane3 city4))
            (autstate_3_3))
          (not 
            (autstate_3_3)))
      )
    )
  (:action debark
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (person ?x0)
        (aircraft ?x1)
        (city ?x2)
        (in ?x0 ?x1)
        (at ?x1 ?x2))
    :effect
      (and
        (at ?x0 ?x2)
        (when
          (and
            (autstate_1_2)
            (at plane2 city2))
          (autstate_1_3))
        (when
          (and
            (at plane2 city3)
            (or
              (autstate_1_3)
              (and
                (autstate_1_2)
                (at plane2 city2))))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (at plane1 city2))
          (autstate_2_3))
        (when
          (and
            (at plane1 city0)
            (or
              (autstate_2_3)
              (and
                (autstate_2_2)
                (at plane1 city2))))
          (autstate_2_1))
        (when
          (and
            (autstate_3_2)
            (at plane3 city4))
          (autstate_3_3))
        (when
          (and
            (at plane3 city0)
            (or
              (autstate_3_3)
              (and
                (autstate_3_2)
                (at plane3 city4))))
          (autstate_3_1))
        (not 
          (in ?x0 ?x1))
        (when
          (and
            (not 
              (at plane2 city3))
            (autstate_1_2))
          (not 
            (autstate_1_2)))
        (when
          (and
            (not 
              (at plane2 city2))
            (autstate_1_3))
          (not 
            (autstate_1_3)))
        (when
          (and
            (not 
              (at plane1 city0))
            (autstate_2_2))
          (not 
            (autstate_2_2)))
        (when
          (and
            (not 
              (at plane1 city2))
            (autstate_2_3))
          (not 
            (autstate_2_3)))
        (when
          (and
            (not 
              (at plane3 city0))
            (autstate_3_2))
          (not 
            (autstate_3_2)))
        (when
          (and
            (not 
              (at plane3 city4))
            (autstate_3_3))
          (not 
            (autstate_3_3)))
      )
    )
  (:action fly
    :parameters (?x0 ?x1 ?x2 ?x3 ?x4)
    :precondition 
      (and
        (aircraft ?x0)
        (city ?x2)
        (at ?x0 ?x1)
        (fuel_level ?x0 ?x3)
        (next_to ?x4 ?x3))
    :effect
      (and
        (at ?x0 ?x2)
        (fuel_level ?x0 ?x4)
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 plane2)
                (= ?x2 city2))
              (and
                (at plane2 city2)
                (or
                  (not 
                    (= ?x0 plane2))
                  (not 
                    (= ?x1 city2))))))
          (autstate_1_3))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 plane2)
                  (= ?x2 city3))
                (and
                  (at plane2 city3)
                  (or
                    (not 
                      (= ?x0 plane2))
                    (not 
                      (= ?x1 city3))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x0 plane2)
                  (or
                    (and
                      (at plane2 city2)
                      (not 
                        (= ?x1 city2))
                      (= ?x2 city3))
                    (and
                      (at plane2 city3)
                      (not 
                        (= ?x1 city3))
                      (= ?x2 city2))))
                (and
                  (at plane2 city2)
                  (at plane2 city3)
                  (or
                    (not 
                      (= ?x0 plane2))
                    (and
                      (not 
                        (= ?x1 city2))
                      (not 
                        (= ?x1 city3))))))))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (or
              (and
                (= ?x0 plane1)
                (= ?x2 city2))
              (and
                (at plane1 city2)
                (or
                  (not 
                    (= ?x0 plane1))
                  (not 
                    (= ?x1 city2))))))
          (autstate_2_3))
        (when
          (or
            (and
              (autstate_2_3)
              (or
                (and
                  (= ?x0 plane1)
                  (= ?x2 city0))
                (and
                  (at plane1 city0)
                  (or
                    (not 
                      (= ?x0 plane1))
                    (not 
                      (= ?x1 city0))))))
            (and
              (autstate_2_2)
              (or
                (and
                  (= ?x0 plane1)
                  (or
                    (and
                      (at plane1 city2)
                      (not 
                        (= ?x1 city2))
                      (= ?x2 city0))
                    (and
                      (at plane1 city0)
                      (not 
                        (= ?x1 city0))
                      (= ?x2 city2))))
                (and
                  (at plane1 city2)
                  (at plane1 city0)
                  (or
                    (not 
                      (= ?x0 plane1))
                    (and
                      (not 
                        (= ?x1 city2))
                      (not 
                        (= ?x1 city0))))))))
          (autstate_2_1))
        (when
          (and
            (autstate_3_2)
            (or
              (and
                (= ?x0 plane3)
                (= ?x2 city4))
              (and
                (at plane3 city4)
                (or
                  (not 
                    (= ?x0 plane3))
                  (not 
                    (= ?x1 city4))))))
          (autstate_3_3))
        (when
          (or
            (and
              (autstate_3_3)
              (or
                (and
                  (= ?x0 plane3)
                  (= ?x2 city0))
                (and
                  (at plane3 city0)
                  (or
                    (not 
                      (= ?x0 plane3))
                    (not 
                      (= ?x1 city0))))))
            (and
              (autstate_3_2)
              (or
                (and
                  (= ?x0 plane3)
                  (or
                    (and
                      (at plane3 city4)
                      (not 
                        (= ?x1 city4))
                      (= ?x2 city0))
                    (and
                      (at plane3 city0)
                      (not 
                        (= ?x1 city0))
                      (= ?x2 city4))))
                (and
                  (at plane3 city4)
                  (at plane3 city0)
                  (or
                    (not 
                      (= ?x0 plane3))
                    (and
                      (not 
                        (= ?x1 city4))
                      (not 
                        (= ?x1 city0))))))))
          (autstate_3_1))
        (not 
          (fuel_level ?x0 ?x3))
        (not 
          (at ?x0 ?x1))
        (when
          (and
            (or
              (not 
                (= ?x0 plane2))
              (not 
                (= ?x2 city3)))
            (or
              (not 
                (at plane2 city3))
              (and
                (= ?x0 plane2)
                (= ?x1 city3)))
            (autstate_1_2))
          (not 
            (autstate_1_2)))
        (when
          (and
            (or
              (not 
                (autstate_1_2))
              (and
                (or
                  (not 
                    (= ?x0 plane2))
                  (not 
                    (= ?x2 city2)))
                (or
                  (not 
                    (at plane2 city2))
                  (and
                    (= ?x0 plane2)
                    (= ?x1 city2)))))
            (or
              (not 
                (= ?x0 plane2))
              (not 
                (= ?x2 city2)))
            (or
              (not 
                (at plane2 city2))
              (and
                (= ?x0 plane2)
                (= ?x1 city2)))
            (autstate_1_3))
          (not 
            (autstate_1_3)))
        (when
          (and
            (or
              (not 
                (= ?x0 plane1))
              (not 
                (= ?x2 city0)))
            (or
              (not 
                (at plane1 city0))
              (and
                (= ?x0 plane1)
                (= ?x1 city0)))
            (autstate_2_2))
          (not 
            (autstate_2_2)))
        (when
          (and
            (or
              (not 
                (autstate_2_2))
              (and
                (or
                  (not 
                    (= ?x0 plane1))
                  (not 
                    (= ?x2 city2)))
                (or
                  (not 
                    (at plane1 city2))
                  (and
                    (= ?x0 plane1)
                    (= ?x1 city2)))))
            (or
              (not 
                (= ?x0 plane1))
              (not 
                (= ?x2 city2)))
            (or
              (not 
                (at plane1 city2))
              (and
                (= ?x0 plane1)
                (= ?x1 city2)))
            (autstate_2_3))
          (not 
            (autstate_2_3)))
        (when
          (and
            (or
              (not 
                (= ?x0 plane3))
              (not 
                (= ?x2 city0)))
            (or
              (not 
                (at plane3 city0))
              (and
                (= ?x0 plane3)
                (= ?x1 city0)))
            (autstate_3_2))
          (not 
            (autstate_3_2)))
        (when
          (and
            (or
              (not 
                (autstate_3_2))
              (and
                (or
                  (not 
                    (= ?x0 plane3))
                  (not 
                    (= ?x2 city4)))
                (or
                  (not 
                    (at plane3 city4))
                  (and
                    (= ?x0 plane3)
                    (= ?x1 city4)))))
            (or
              (not 
                (= ?x0 plane3))
              (not 
                (= ?x2 city4)))
            (or
              (not 
                (at plane3 city4))
              (and
                (= ?x0 plane3)
                (= ?x1 city4)))
            (autstate_3_3))
          (not 
            (autstate_3_3)))
      )
    )
  (:action refuel
    :parameters (?x0 ?x1 ?x2 ?x3)
    :precondition 
      (and
        (aircraft ?x0)
        (city ?x1)
        (fuel_level ?x0 ?x2)
        (next_to ?x2 ?x3))
    :effect
      (and
        (fuel_level ?x0 ?x3)
        (when
          (and
            (autstate_1_2)
            (at plane2 city2))
          (autstate_1_3))
        (when
          (and
            (at plane2 city3)
            (or
              (autstate_1_3)
              (and
                (autstate_1_2)
                (at plane2 city2))))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (at plane1 city2))
          (autstate_2_3))
        (when
          (and
            (at plane1 city0)
            (or
              (autstate_2_3)
              (and
                (autstate_2_2)
                (at plane1 city2))))
          (autstate_2_1))
        (when
          (and
            (autstate_3_2)
            (at plane3 city4))
          (autstate_3_3))
        (when
          (and
            (at plane3 city0)
            (or
              (autstate_3_3)
              (and
                (autstate_3_2)
                (at plane3 city4))))
          (autstate_3_1))
        (not 
          (fuel_level ?x0 ?x2))
        (when
          (and
            (not 
              (at plane2 city3))
            (autstate_1_2))
          (not 
            (autstate_1_2)))
        (when
          (and
            (not 
              (at plane2 city2))
            (autstate_1_3))
          (not 
            (autstate_1_3)))
        (when
          (and
            (not 
              (at plane1 city0))
            (autstate_2_2))
          (not 
            (autstate_2_2)))
        (when
          (and
            (not 
              (at plane1 city2))
            (autstate_2_3))
          (not 
            (autstate_2_3)))
        (when
          (and
            (not 
              (at plane3 city0))
            (autstate_3_2))
          (not 
            (autstate_3_2)))
        (when
          (and
            (not 
              (at plane3 city4))
            (autstate_3_3))
          (not 
            (autstate_3_3)))
      )
    )
)