(define (domain blocks-world-domain)
  (:types
    default_object - none
  )

  (:predicates
    (on ?x0 ?x1)
    (clear ?x0)
    (block ?x0)
    (autstate_1_2)
    (autstate_1_3)
    (autstate_1_4)
    (autstate_1_5)
    (autstate_1_6)
    (autstate_1_7)
    (autstate_1_8)
    (autstate_1_9)
    (autstate_1_1)
  )
  (:action puton
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 ?x1)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 d)
                (= ?x1 table))
              (and
                (on d table)
                (or
                  (not 
                    (= ?x0 d))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_3))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on d table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 d))))
                (and
                  (on d table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_5))
        (when
          (or
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on d table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on d table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on d table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on d table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 d))
                        (and
                          (on d table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on d table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_9))
        (when
          (or
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on d table)
                              (= ?x0 c))
                            (and
                              (on c table)
                              (= ?x0 d))))
                        (and
                          (on d table)
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on d table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on d table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_1))
        (not 
          (on ?x0 ?x2))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
)