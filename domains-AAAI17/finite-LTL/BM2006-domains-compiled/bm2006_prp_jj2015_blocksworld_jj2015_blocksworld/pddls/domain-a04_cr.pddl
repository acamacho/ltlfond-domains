(define (domain blocks-world-domain)
  (:types
    default_object - none
  )

  (:predicates
    (on ?x0 ?x1)
    (clear ?x0)
    (block ?x0)
    (autstate_1_18)
    (autstate_1_19)
    (autstate_1_20)
    (autstate_1_21)
    (autstate_1_22)
    (autstate_1_23)
    (autstate_1_24)
    (autstate_1_25)
    (autstate_1_26)
    (autstate_1_27)
    (autstate_1_28)
    (autstate_1_29)
    (autstate_1_30)
    (autstate_1_31)
    (autstate_1_32)
    (autstate_1_1)
  )
  (:action puton
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 ?x1)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_18)
            (or
              (and
                (= ?x0 e)
                (= ?x1 table))
              (and
                (on e table)
                (or
                  (not 
                    (= ?x0 e))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_19))
        (when
          (and
            (autstate_1_18)
            (or
              (and
                (= ?x0 c)
                (= ?x1 table))
              (and
                (on c table)
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_20))
        (when
          (or
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_21))
        (when
          (and
            (autstate_1_18)
            (or
              (and
                (= ?x0 b)
                (= ?x1 table))
              (and
                (on b table)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_22))
        (when
          (or
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_23))
        (when
          (or
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_24))
        (when
          (or
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_25))
        (when
          (and
            (autstate_1_18)
            (or
              (and
                (= ?x0 a)
                (= ?x1 table))
              (and
                (on a table)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_26))
        (when
          (or
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_27))
        (when
          (or
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_28))
        (when
          (or
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_29))
        (when
          (or
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))))))))
          (autstate_1_30))
        (when
          (or
            (and
              (autstate_1_30)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_31))
        (when
          (or
            (and
              (autstate_1_30)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_32))
        (when
          (or
            (and
              (autstate_1_32)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_31)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_30)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_29)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_25)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (= ?x0 e))
                            (and
                              (on e table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_1))
        (not 
          (on ?x0 ?x2))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
)