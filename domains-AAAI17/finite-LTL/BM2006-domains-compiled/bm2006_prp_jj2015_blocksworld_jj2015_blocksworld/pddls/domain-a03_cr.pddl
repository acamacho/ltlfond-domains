(define (domain blocks-world-domain)
  (:types
    default_object - none
  )

  (:predicates
    (on ?x0 ?x1)
    (clear ?x0)
    (block ?x0)
    (autstate_1_10)
    (autstate_1_11)
    (autstate_1_12)
    (autstate_1_13)
    (autstate_1_14)
    (autstate_1_15)
    (autstate_1_16)
    (autstate_1_1)
  )
  (:action puton
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 ?x1)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_10)
            (or
              (and
                (= ?x0 c)
                (= ?x1 table))
              (and
                (on c table)
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_11))
        (when
          (and
            (autstate_1_10)
            (or
              (and
                (= ?x0 b)
                (= ?x1 table))
              (and
                (on b table)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_13))
        (when
          (and
            (autstate_1_10)
            (or
              (and
                (= ?x0 a)
                (= ?x1 table))
              (and
                (on a table)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_1))
        (not 
          (on ?x0 ?x2))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
)