(define (domain blocks-world-domain)
  (:types
    default_object - none
  )

  (:predicates
    (on ?x0 ?x1)
    (clear ?x0)
    (block ?x0)
    (autstate_1_34)
    (autstate_1_35)
    (autstate_1_36)
    (autstate_1_37)
    (autstate_1_38)
    (autstate_1_39)
    (autstate_1_40)
    (autstate_1_41)
    (autstate_1_42)
    (autstate_1_43)
    (autstate_1_44)
    (autstate_1_45)
    (autstate_1_46)
    (autstate_1_47)
    (autstate_1_48)
    (autstate_1_49)
    (autstate_1_50)
    (autstate_1_51)
    (autstate_1_52)
    (autstate_1_53)
    (autstate_1_54)
    (autstate_1_55)
    (autstate_1_56)
    (autstate_1_57)
    (autstate_1_58)
    (autstate_1_59)
    (autstate_1_60)
    (autstate_1_61)
    (autstate_1_62)
    (autstate_1_63)
    (autstate_1_64)
    (autstate_1_1)
  )
  (:action puton_detdup_0
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 ?x1)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_34)
            (or
              (and
                (= ?x0 f)
                (= ?x1 table))
              (and
                (on f table)
                (or
                  (not 
                    (= ?x0 f))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_35))
        (when
          (and
            (autstate_1_34)
            (or
              (and
                (= ?x0 e)
                (= ?x1 table))
              (and
                (on e table)
                (or
                  (not 
                    (= ?x0 e))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_36))
        (when
          (or
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_37))
        (when
          (and
            (autstate_1_34)
            (or
              (and
                (= ?x0 c)
                (= ?x1 table))
              (and
                (on c table)
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_38))
        (when
          (or
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_39))
        (when
          (or
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_40))
        (when
          (or
            (and
              (autstate_1_40)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_41))
        (when
          (and
            (autstate_1_34)
            (or
              (and
                (= ?x0 b)
                (= ?x1 table))
              (and
                (on b table)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_42))
        (when
          (or
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_43))
        (when
          (or
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_44))
        (when
          (or
            (and
              (autstate_1_44)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_45))
        (when
          (or
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_46))
        (when
          (or
            (and
              (autstate_1_46)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_47))
        (when
          (or
            (and
              (autstate_1_46)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_48))
        (when
          (or
            (and
              (autstate_1_48)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_47)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_46)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_45)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_41)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_49))
        (when
          (and
            (autstate_1_34)
            (or
              (and
                (= ?x0 a)
                (= ?x1 table))
              (and
                (on a table)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_50))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_51))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_52))
        (when
          (or
            (and
              (autstate_1_52)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_53))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_54))
        (when
          (or
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_55))
        (when
          (or
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_56))
        (when
          (or
            (and
              (autstate_1_56)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_55)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_53)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_41)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_57))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))))))))
          (autstate_1_58))
        (when
          (or
            (and
              (autstate_1_58)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_59))
        (when
          (or
            (and
              (autstate_1_58)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_60))
        (when
          (or
            (and
              (autstate_1_60)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_59)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_58)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_53)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_45)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_61))
        (when
          (or
            (and
              (autstate_1_58)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_46)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_62))
        (when
          (or
            (and
              (autstate_1_62)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_59)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_58)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_55)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_47)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_46)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_63))
        (when
          (or
            (and
              (autstate_1_62)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_60)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_58)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_56)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_48)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_46)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (= ?x0 e))
                            (and
                              (on e table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_64))
        (when
          (or
            (and
              (autstate_1_64)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_63)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_62)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_61)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_60)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_59)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_58)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_57)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_56)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_55)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_53)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_49)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_48)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_47)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_46)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_45)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_41)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (= ?x0 e))
                            (and
                              (on e table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (or
                                (not 
                                  (= ?x0 c))
                                (not 
                                  (= ?x2 table)))
                              (or
                                (and
                                  (on e table)
                                  (= ?x0 f))
                                (and
                                  (on f table)
                                  (= ?x0 e))))
                            (and
                              (on e table)
                              (on f table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on e table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_1))
        (not 
          (on ?x0 ?x2))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
  (:action puton_detdup_1
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 table)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_34)
            (or
              (= ?x0 f)
              (and
                (on f table)
                (or
                  (not 
                    (= ?x0 f))
                  (not 
                    (= ?x2 table))
                  (= table table)))))
          (autstate_1_35))
        (when
          (and
            (autstate_1_34)
            (or
              (= ?x0 e)
              (and
                (on e table)
                (or
                  (not 
                    (= ?x0 e))
                  (not 
                    (= ?x2 table))
                  (= table table)))))
          (autstate_1_36))
        (when
          (or
            (and
              (autstate_1_36)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_35)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_37))
        (when
          (and
            (autstate_1_34)
            (or
              (= ?x0 c)
              (and
                (on c table)
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 table))
                  (= table table)))))
          (autstate_1_38))
        (when
          (or
            (and
              (autstate_1_38)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_35)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_39))
        (when
          (or
            (and
              (autstate_1_38)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_36)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_40))
        (when
          (or
            (and
              (autstate_1_40)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_39)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_41))
        (when
          (and
            (autstate_1_34)
            (or
              (= ?x0 b)
              (and
                (on b table)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 table))
                  (= table table)))))
          (autstate_1_42))
        (when
          (or
            (and
              (autstate_1_42)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_35)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_43))
        (when
          (or
            (and
              (autstate_1_42)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_36)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_44))
        (when
          (or
            (and
              (autstate_1_44)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_43)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_45))
        (when
          (or
            (and
              (autstate_1_42)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_38)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_46))
        (when
          (or
            (and
              (autstate_1_46)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_43)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_47))
        (when
          (or
            (and
              (autstate_1_46)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_44)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_40)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_48))
        (when
          (or
            (and
              (autstate_1_48)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_47)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_46)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_45)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_44)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_41)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_40)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_49))
        (when
          (and
            (autstate_1_34)
            (or
              (= ?x0 a)
              (and
                (on a table)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 table))
                  (= table table)))))
          (autstate_1_50))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_35)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_51))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_36)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_52))
        (when
          (or
            (and
              (autstate_1_52)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_51)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_53))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_38)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_54))
        (when
          (or
            (and
              (autstate_1_54)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_51)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_55))
        (when
          (or
            (and
              (autstate_1_54)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_52)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_40)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_56))
        (when
          (or
            (and
              (autstate_1_56)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_55)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_54)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_53)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_52)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_41)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_40)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_57))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_42)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))))))))
          (autstate_1_58))
        (when
          (or
            (and
              (autstate_1_58)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_51)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_43)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_59))
        (when
          (or
            (and
              (autstate_1_58)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_52)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_44)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_60))
        (when
          (or
            (and
              (autstate_1_60)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_59)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_58)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_53)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_52)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_45)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_44)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_61))
        (when
          (or
            (and
              (autstate_1_58)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_54)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_46)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_62))
        (when
          (or
            (and
              (autstate_1_62)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_59)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_58)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_55)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_54)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_47)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_46)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_63))
        (when
          (or
            (and
              (autstate_1_62)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_60)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_58)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_56)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_54)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_48)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_46)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))))))))
          (autstate_1_64))
        (when
          (or
            (and
              (autstate_1_64)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_63)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_62)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_61)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_60)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_59)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_58)
              (or
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_57)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_56)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_55)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_54)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_53)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_49)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_48)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_47)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_46)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_45)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_41)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table))
                            (= table table))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_1))
        (when
          (not 
            (= ?x2 table))
          (not 
            (on ?x0 ?x2)))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
)