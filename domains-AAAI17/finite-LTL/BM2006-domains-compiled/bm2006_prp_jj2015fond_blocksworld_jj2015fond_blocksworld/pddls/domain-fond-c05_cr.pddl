(define (domain blocks-world-domain)
  (:types
    default_object - none
  )

  (:predicates
    (on ?x0 ?x1)
    (clear ?x0)
    (block ?x0)
    (autstate_1_2)
    (autstate_1_3)
    (autstate_1_4)
    (autstate_1_5)
    (autstate_1_6)
    (autstate_1_7)
    (autstate_1_8)
    (autstate_1_9)
    (autstate_1_10)
    (autstate_1_11)
    (autstate_1_12)
    (autstate_1_13)
    (autstate_1_14)
    (autstate_1_15)
    (autstate_1_16)
    (autstate_1_17)
    (autstate_1_18)
    (autstate_1_19)
    (autstate_1_20)
    (autstate_1_21)
    (autstate_1_22)
    (autstate_1_23)
    (autstate_1_24)
    (autstate_1_25)
    (autstate_1_26)
    (autstate_1_27)
    (autstate_1_28)
    (autstate_1_29)
    (autstate_1_30)
    (autstate_1_31)
    (autstate_1_32)
    (autstate_1_33)
    (autstate_1_1)
  )
  (:action puton_detdup_0
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 ?x1)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 d)
                (= ?x1 table))
              (and
                (on d table)
                (or
                  (not 
                    (= ?x0 d))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_3))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on d table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 d))))
                (and
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_5))
        (when
          (or
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (or
                        (not 
                          (= ?x0 e))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on d table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on d table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 d))
                        (and
                          (on d table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_9))
        (when
          (or
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (or
                            (not 
                              (= ?x0 e))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on d table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 d))))
                        (and
                          (on d table)
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on d table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 d))
                        (and
                          (on d table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (or
                            (not 
                              (= ?x0 e))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on d table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 d))))
                        (and
                          (on d table)
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 d))
                        (and
                          (on d table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on d table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on d table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 d))))
                        (and
                          (on d table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on d table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 d))
                            (and
                              (on d table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on d table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on d table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_17))
        (when
          (or
            (and
              (autstate_1_17)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (or
                                (not 
                                  (= ?x0 e))
                                (not 
                                  (= ?x2 table)))
                              (or
                                (and
                                  (on d table)
                                  (= ?x0 f))
                                (and
                                  (on f table)
                                  (= ?x0 d))))
                            (and
                              (on d table)
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on d table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on d table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_18))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_19))
        (when
          (or
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on d table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_20))
        (when
          (or
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 d))
                        (and
                          (on d table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_21))
        (when
          (or
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (or
                            (not 
                              (= ?x0 e))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on d table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 d))))
                        (and
                          (on d table)
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_22))
        (when
          (or
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 d))
                        (and
                          (on d table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on d table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_23))
        (when
          (or
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on d table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 d))))
                        (and
                          (on d table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on d table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_24))
        (when
          (or
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 d))
                            (and
                              (on d table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on d table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on d table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_25))
        (when
          (or
            (and
              (autstate_1_25)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (or
                                (not 
                                  (= ?x0 e))
                                (not 
                                  (= ?x2 table)))
                              (or
                                (and
                                  (on d table)
                                  (= ?x0 f))
                                (and
                                  (on f table)
                                  (= ?x0 d))))
                            (and
                              (on d table)
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on d table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on d table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_26))
        (when
          (or
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 d))
                        (and
                          (on d table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on d table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_27))
        (when
          (or
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on d table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 d))))
                        (and
                          (on d table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on d table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_28))
        (when
          (or
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 d))
                            (and
                              (on d table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on d table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (on d table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_29))
        (when
          (or
            (and
              (autstate_1_29)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (or
                                (not 
                                  (= ?x0 e))
                                (not 
                                  (= ?x2 table)))
                              (or
                                (and
                                  (on d table)
                                  (= ?x0 f))
                                (and
                                  (on f table)
                                  (= ?x0 d))))
                            (and
                              (on d table)
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on d table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (on d table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_30))
        (when
          (or
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (= ?x0 d))
                            (and
                              (on d table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on d table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on d table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_31))
        (when
          (or
            (and
              (autstate_1_31)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (or
                                (not 
                                  (= ?x0 c))
                                (not 
                                  (= ?x2 table)))
                              (or
                                (and
                                  (on d table)
                                  (= ?x0 f))
                                (and
                                  (on f table)
                                  (= ?x0 d))))
                            (and
                              (on d table)
                              (on f table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on d table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on d table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_32))
        (when
          (or
            (and
              (autstate_1_31)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_29)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_25)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_17)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (= ?x0 e))
                            (and
                              (on e table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (or
                                (not 
                                  (= ?x0 c))
                                (not 
                                  (= ?x2 table)))
                              (or
                                (and
                                  (on e table)
                                  (= ?x0 d))
                                (and
                                  (on d table)
                                  (= ?x0 e))))
                            (and
                              (on e table)
                              (on d table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on e table)
                          (on d table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on e table)
                      (on d table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_33))
        (when
          (or
            (and
              (autstate_1_33)
              (or
                (and
                  (= ?x0 f)
                  (= ?x1 table))
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_32)
              (or
                (and
                  (= ?x0 e)
                  (= ?x1 table))
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_31)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_30)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 table))
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_29)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 table))
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_25)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 table))
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))))))
            (and
              (autstate_1_17)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (= ?x0 b))
                    (and
                      (on b table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (= ?x0 c))
                        (and
                          (on c table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (= ?x0 e))
                            (and
                              (on e table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on e table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on e table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (or
                                (not 
                                  (= ?x0 c))
                                (not 
                                  (= ?x2 table)))
                              (or
                                (and
                                  (on e table)
                                  (= ?x0 f))
                                (and
                                  (on f table)
                                  (= ?x0 e))))
                            (and
                              (on e table)
                              (on f table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on e table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 table)
                  (or
                    (and
                      (on a table)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 table)))
                      (or
                        (and
                          (on b table)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 table)))
                          (or
                            (and
                              (on c table)
                              (or
                                (not 
                                  (= ?x0 c))
                                (not 
                                  (= ?x2 table)))
                              (or
                                (and
                                  (on e table)
                                  (or
                                    (not 
                                      (= ?x0 e))
                                    (not 
                                      (= ?x2 table)))
                                  (or
                                    (and
                                      (on d table)
                                      (= ?x0 f))
                                    (and
                                      (on f table)
                                      (= ?x0 d))))
                                (and
                                  (on d table)
                                  (on f table)
                                  (= ?x0 e))))
                            (and
                              (on e table)
                              (on d table)
                              (on f table)
                              (= ?x0 c))))
                        (and
                          (on c table)
                          (on e table)
                          (on d table)
                          (on f table)
                          (= ?x0 b))))
                    (and
                      (on b table)
                      (on c table)
                      (on e table)
                      (on d table)
                      (on f table)
                      (= ?x0 a))))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_1))
        (not 
          (on ?x0 ?x2))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
  (:action puton_detdup_1
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 table)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x0 d)
              (and
                (on d table)
                (or
                  (not 
                    (= ?x0 d))
                  (not 
                    (= ?x2 table))
                  (= table table)))))
          (autstate_1_3))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on d table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 d))
                (and
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on e table)
                  (= ?x0 d))
                (and
                  (on d table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_5))
        (when
          (or
            (and
              (autstate_1_5)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_4)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on d table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 d))))
                (and
                  (on d table)
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c table)
                  (= ?x0 d))
                (and
                  (on d table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_7)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_4)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on d table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 d))))
                (and
                  (on d table)
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_7)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_5)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on d table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_9))
        (when
          (or
            (and
              (autstate_1_9)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_8)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (or
                        (not 
                          (= ?x0 e))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on d table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on d table)
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b table)
                  (= ?x0 d))
                (and
                  (on d table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_11)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_4)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on d table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 d))))
                (and
                  (on d table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_11)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_5)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on d table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_13)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_12)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (or
                        (not 
                          (= ?x0 e))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on d table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on d table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_11)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_7)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on d table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_15)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_12)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on d table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on d table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_15)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_13)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_9)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 d))
                        (and
                          (on d table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on d table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_17))
        (when
          (or
            (and
              (autstate_1_17)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_16)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_15)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_14)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_13)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_10)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_9)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (or
                            (not 
                              (= ?x0 e))
                            (not 
                              (= ?x2 table))
                            (= table table))
                          (or
                            (and
                              (on d table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 d))))
                        (and
                          (on d table)
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_18))
        (when
          (or
            (and
              (autstate_1_3)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (= ?x0 d))
                (and
                  (on d table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_19))
        (when
          (or
            (and
              (autstate_1_19)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_4)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on d table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 d))))
                (and
                  (on d table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_20))
        (when
          (or
            (and
              (autstate_1_19)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_5)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on d table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_21))
        (when
          (or
            (and
              (autstate_1_21)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_20)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (or
                        (not 
                          (= ?x0 e))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on d table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on d table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_22))
        (when
          (or
            (and
              (autstate_1_19)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_7)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on d table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_23))
        (when
          (or
            (and
              (autstate_1_23)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_20)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on d table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on d table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_24))
        (when
          (or
            (and
              (autstate_1_23)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_21)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_9)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 d))
                        (and
                          (on d table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on d table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_25))
        (when
          (or
            (and
              (autstate_1_25)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_24)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_23)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_22)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_21)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_10)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_9)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (or
                            (not 
                              (= ?x0 e))
                            (not 
                              (= ?x2 table))
                            (= table table))
                          (or
                            (and
                              (on d table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 d))))
                        (and
                          (on d table)
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_26))
        (when
          (or
            (and
              (autstate_1_19)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_11)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 d))
                    (and
                      (on d table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on d table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_27))
        (when
          (or
            (and
              (autstate_1_27)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_20)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_12)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on d table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 d))))
                    (and
                      (on d table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on d table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_28))
        (when
          (or
            (and
              (autstate_1_27)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_21)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_13)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 d))
                        (and
                          (on d table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on d table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_29))
        (when
          (or
            (and
              (autstate_1_29)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_28)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_27)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_22)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_21)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_14)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_13)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (or
                            (not 
                              (= ?x0 e))
                            (not 
                              (= ?x2 table))
                            (= table table))
                          (or
                            (and
                              (on d table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 d))))
                        (and
                          (on d table)
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on d table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_30))
        (when
          (or
            (and
              (autstate_1_27)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_23)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_15)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (= ?x0 d))
                        (and
                          (on d table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on d table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on d table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_31))
        (when
          (or
            (and
              (autstate_1_31)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_28)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_27)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_24)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_23)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_16)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_15)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table))
                            (= table table))
                          (or
                            (and
                              (on d table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 d))))
                        (and
                          (on d table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on d table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on d table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_32))
        (when
          (or
            (and
              (autstate_1_31)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_29)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_27)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_25)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_23)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_17)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_15)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table))
                            (= table table))
                          (or
                            (and
                              (on e table)
                              (= ?x0 d))
                            (and
                              (on d table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on d table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on d table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))))))))
          (autstate_1_33))
        (when
          (or
            (and
              (autstate_1_33)
              (or
                (= ?x0 f)
                (and
                  (on f table)
                  (or
                    (not 
                      (= ?x0 f))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_32)
              (or
                (= ?x0 e)
                (and
                  (on e table)
                  (or
                    (not 
                      (= ?x0 e))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_31)
              (or
                (and
                  (on e table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_30)
              (or
                (= ?x0 c)
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_29)
              (or
                (and
                  (on c table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (on c table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (on c table)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_26)
              (or
                (= ?x0 b)
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_25)
              (or
                (and
                  (on b table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (on b table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on b table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on b table)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_18)
              (or
                (= ?x0 a)
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table)))))
            (and
              (autstate_1_17)
              (or
                (and
                  (on a table)
                  (= ?x0 f))
                (and
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (on a table)
                  (= ?x0 e))
                (and
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on e table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 e))))
                (and
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on a table)
                  (= ?x0 c))
                (and
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on c table)
                      (or
                        (not 
                          (= ?x0 c))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 c))))
                (and
                  (on c table)
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on a table)
                  (= ?x0 b))
                (and
                  (on b table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 f))
                    (and
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 e))
                    (and
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on e table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 e))))
                    (and
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (= ?x0 c))
                    (and
                      (on c table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (= ?x0 f))
                        (and
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (= ?x0 e))
                        (and
                          (on e table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table))
                            (= table table))
                          (or
                            (and
                              (on e table)
                              (= ?x0 f))
                            (and
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 f)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a table)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 table))
                    (= table table))
                  (or
                    (and
                      (on b table)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 table))
                        (= table table))
                      (or
                        (and
                          (on c table)
                          (or
                            (not 
                              (= ?x0 c))
                            (not 
                              (= ?x2 table))
                            (= table table))
                          (or
                            (and
                              (on e table)
                              (or
                                (not 
                                  (= ?x0 e))
                                (not 
                                  (= ?x2 table))
                                (= table table))
                              (or
                                (and
                                  (on d table)
                                  (= ?x0 f))
                                (and
                                  (on f table)
                                  (= ?x0 d))))
                            (and
                              (on d table)
                              (on f table)
                              (= ?x0 e))))
                        (and
                          (on e table)
                          (on d table)
                          (on f table)
                          (= ?x0 c))))
                    (and
                      (on c table)
                      (on e table)
                      (on d table)
                      (on f table)
                      (= ?x0 b))))
                (and
                  (on b table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (= ?x0 a))
                (and
                  (on a table)
                  (on b table)
                  (on c table)
                  (on e table)
                  (on d table)
                  (on f table)
                  (or
                    (not 
                      (= ?x2 table))
                    (= table table)
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 e))
                      (not 
                        (= ?x0 d))
                      (not 
                        (= ?x0 f))))))))
          (autstate_1_1))
        (when
          (not 
            (= ?x2 table))
          (not 
            (on ?x0 ?x2)))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
)