(define (domain blocks-world-domain)
  (:types
    default_object - none
  )

  (:predicates
    (on ?x0 ?x1)
    (clear ?x0)
    (block ?x0)
    (autstate_1_2)
    (autstate_1_1)
    (autstate_2_2)
    (autstate_2_1)
    (autstate_3_2)
    (autstate_3_1)
    (autstate_4_2)
    (autstate_4_1)
  )
  (:action puton_detdup_0
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 ?x1)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 a)
                (= ?x1 table))
              (and
                (on a table)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 table))))))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (or
              (and
                (= ?x0 b)
                (= ?x1 table))
              (and
                (on b table)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 table))))))
          (autstate_2_1))
        (when
          (and
            (autstate_3_2)
            (or
              (and
                (= ?x0 c)
                (= ?x1 table))
              (and
                (on c table)
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 table))))))
          (autstate_3_1))
        (when
          (and
            (autstate_4_2)
            (or
              (and
                (= ?x0 e)
                (= ?x1 table))
              (and
                (on e table)
                (or
                  (not 
                    (= ?x0 e))
                  (not 
                    (= ?x2 table))))))
          (autstate_4_1))
        (not 
          (on ?x0 ?x2))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
  (:action puton_detdup_1
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 table)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x0 a)
              (and
                (on a table)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 table))
                  (= table table)))))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (or
              (= ?x0 b)
              (and
                (on b table)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 table))
                  (= table table)))))
          (autstate_2_1))
        (when
          (and
            (autstate_3_2)
            (or
              (= ?x0 c)
              (and
                (on c table)
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 table))
                  (= table table)))))
          (autstate_3_1))
        (when
          (and
            (autstate_4_2)
            (or
              (= ?x0 e)
              (and
                (on e table)
                (or
                  (not 
                    (= ?x0 e))
                  (not 
                    (= ?x2 table))
                  (= table table)))))
          (autstate_4_1))
        (when
          (not 
            (= ?x2 table))
          (not 
            (on ?x0 ?x2)))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
)