(define (problem 'sussman-anomaly')

  (:domain blocks-world-domain)
  (:objects a b c d table)
  (:init 
    (block a)
    (block b)
    (block c)
    (block table)
    (block d)
    (on c a)
    (on a table)
    (on b table)
    (on d c)
    (clear d)
    (clear b)
    (clear table)
    (autstate_1_10)
    (autstate_1_12)
    (autstate_1_14)
    (autstate_1_16)
  )
  (:goal (autstate_1_1))
)