(define (domain logistics)
  (:types
    default_object - none
  )

  (:predicates
    (package ?x0)
    (truck ?x0)
    (airplane ?x0)
    (airport ?x0)
    (location ?x0)
    (in-city ?x0 ?x1)
    (city ?x0)
    (at ?x0 ?x1)
    (in ?x0 ?x1)
    (autstate_1_2)
    (autstate_1_3)
    (autstate_1_4)
    (autstate_1_5)
    (autstate_1_6)
    (autstate_1_7)
    (autstate_1_8)
    (autstate_1_9)
    (autstate_1_10)
    (autstate_1_11)
    (autstate_1_12)
    (autstate_1_13)
    (autstate_1_14)
    (autstate_1_15)
    (autstate_1_16)
    (autstate_1_1)
  )
  (:action load-truck
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (truck ?x1)
        (location ?x2)
        (at ?x1 ?x2)
        (at ?x0 ?x2))
    :effect
      (and
        (in ?x0 ?x1)
        (when
          (and
            (autstate_1_2)
            (at truck1 city2_1))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (at truck3 city3_1))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (at truck1 city2_1))
            (and
              (at truck3 city3_1)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x2 city2_1)))))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (at truck2 city1_1))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (at truck1 city2_1))
            (and
              (at truck2 city1_1)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x2 city2_1)))))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (at truck3 city3_1))
            (and
              (at truck2 city1_1)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x2 city3_1)))))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (at truck1 city2_1))
            (and
              (autstate_1_7)
              (at truck3 city3_1))
            (and
              (autstate_1_6)
              (at truck3 city3_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (at truck2 city1_1)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x2 city2_1))))
                (and
                  (at truck3 city3_1)
                  (or
                    (and
                      (autstate_1_3)
                      (or
                        (not 
                          (= ?x0 truck3))
                        (not 
                          (= ?x2 city3_1))))
                    (and
                      (autstate_1_2)
                      (at truck1 city2_1)
                      (or
                        (not 
                          (= ?x0 truck3))
                        (not 
                          (= ?x2 city3_1)))
                      (or
                        (not 
                          (= ?x0 truck1))
                        (not 
                          (= ?x2 city2_1)))))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (at truck3 city2_1))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck1 city2_1))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x2 city2_1))
                    (not 
                      (= ?x0 truck1)))))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck3 city3_1))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (at truck3 city3_1)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (at truck1 city2_1))
            (and
              (autstate_1_11)
              (at truck3 city3_1))
            (and
              (autstate_1_10)
              (at truck3 city3_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x2 city2_1))
                    (not 
                      (= ?x0 truck1))))
                (and
                  (at truck3 city3_1)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (at truck1 city2_1)
                      (or
                        (not 
                          (= ?x0 truck1))
                        (not 
                          (= ?x2 city2_1)))))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck2 city1_1))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x2 city1_1)))))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (at truck1 city2_1))
            (and
              (autstate_1_11)
              (at truck2 city1_1))
            (and
              (autstate_1_10)
              (at truck2 city1_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x2 city2_1))
                    (not 
                      (= ?x0 truck1))))
                (and
                  (at truck2 city1_1)
                  (or
                    (and
                      (autstate_1_3)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1))))
                    (and
                      (autstate_1_2)
                      (at truck1 city2_1)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1)))
                      (or
                        (not 
                          (= ?x0 truck1))
                        (not 
                          (= ?x2 city2_1)))))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (at truck3 city3_1))
            (and
              (autstate_1_12)
              (at truck2 city1_1))
            (and
              (autstate_1_10)
              (at truck2 city1_1)
              (at truck3 city3_1)
              (or
                (not 
                  (= ?x0 truck3))
                (not 
                  (= ?x2 city3_1))))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (at truck3 city3_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (and
                      (autstate_1_4)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1))))
                    (and
                      (autstate_1_2)
                      (at truck3 city3_1)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1)))))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (at truck1 city2_1))
            (and
              (autstate_1_15)
              (at truck3 city3_1))
            (and
              (autstate_1_14)
              (at truck3 city3_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (autstate_1_13)
              (at truck2 city1_1))
            (and
              (autstate_1_12)
              (at truck2 city1_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (autstate_1_11)
              (at truck2 city1_1)
              (at truck3 city3_1)
              (or
                (not 
                  (= ?x0 truck3))
                (not 
                  (= ?x2 city3_1))))
            (and
              (autstate_1_10)
              (at truck2 city1_1)
              (at truck3 city3_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck3))
                (not 
                  (= ?x2 city3_1)))
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x2 city2_1))
                    (not 
                      (= ?x0 truck1))))
                (and
                  (autstate_1_7)
                  (at truck3 city3_1))
                (and
                  (autstate_1_6)
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x2 city2_1))))
                (and
                  (at truck2 city1_1)
                  (or
                    (and
                      (autstate_1_5)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1))))
                    (and
                      (autstate_1_4)
                      (at truck1 city2_1)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1)))
                      (or
                        (not 
                          (= ?x0 truck1))
                        (not 
                          (= ?x2 city2_1))))
                    (and
                      (at truck3 city3_1)
                      (or
                        (and
                          (autstate_1_3)
                          (or
                            (not 
                              (= ?x0 truck2))
                            (not 
                              (= ?x2 city1_1))))
                        (and
                          (autstate_1_2)
                          (at truck1 city2_1)
                          (or
                            (not 
                              (= ?x0 truck2))
                            (not 
                              (= ?x2 city1_1)))
                          (or
                            (not 
                              (= ?x0 truck1))
                            (not 
                              (= ?x2 city2_1)))))))))))
          (autstate_1_1))
        (not 
          (at ?x0 ?x2))
      )
    )
  (:action load-airplane
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (airplane ?x1)
        (location ?x2)
        (at ?x0 ?x2)
        (at ?x1 ?x2))
    :effect
      (and
        (in ?x0 ?x1)
        (when
          (and
            (autstate_1_2)
            (at truck1 city2_1))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (at truck3 city3_1))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (at truck1 city2_1))
            (and
              (at truck3 city3_1)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x2 city2_1)))))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (at truck2 city1_1))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (at truck1 city2_1))
            (and
              (at truck2 city1_1)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x2 city2_1)))))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (at truck3 city3_1))
            (and
              (at truck2 city1_1)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x2 city3_1)))))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (at truck1 city2_1))
            (and
              (autstate_1_7)
              (at truck3 city3_1))
            (and
              (autstate_1_6)
              (at truck3 city3_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (at truck2 city1_1)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x2 city2_1))))
                (and
                  (at truck3 city3_1)
                  (or
                    (and
                      (autstate_1_3)
                      (or
                        (not 
                          (= ?x0 truck3))
                        (not 
                          (= ?x2 city3_1))))
                    (and
                      (autstate_1_2)
                      (at truck1 city2_1)
                      (or
                        (not 
                          (= ?x0 truck3))
                        (not 
                          (= ?x2 city3_1)))
                      (or
                        (not 
                          (= ?x0 truck1))
                        (not 
                          (= ?x2 city2_1)))))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (at truck3 city2_1))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck1 city2_1))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x2 city2_1))
                    (not 
                      (= ?x0 truck1)))))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck3 city3_1))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (at truck3 city3_1)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (at truck1 city2_1))
            (and
              (autstate_1_11)
              (at truck3 city3_1))
            (and
              (autstate_1_10)
              (at truck3 city3_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x2 city2_1))
                    (not 
                      (= ?x0 truck1))))
                (and
                  (at truck3 city3_1)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (at truck1 city2_1)
                      (or
                        (not 
                          (= ?x0 truck1))
                        (not 
                          (= ?x2 city2_1)))))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck2 city1_1))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x2 city1_1)))))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (at truck1 city2_1))
            (and
              (autstate_1_11)
              (at truck2 city1_1))
            (and
              (autstate_1_10)
              (at truck2 city1_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x2 city2_1))
                    (not 
                      (= ?x0 truck1))))
                (and
                  (at truck2 city1_1)
                  (or
                    (and
                      (autstate_1_3)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1))))
                    (and
                      (autstate_1_2)
                      (at truck1 city2_1)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1)))
                      (or
                        (not 
                          (= ?x0 truck1))
                        (not 
                          (= ?x2 city2_1)))))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (at truck3 city3_1))
            (and
              (autstate_1_12)
              (at truck2 city1_1))
            (and
              (autstate_1_10)
              (at truck2 city1_1)
              (at truck3 city3_1)
              (or
                (not 
                  (= ?x0 truck3))
                (not 
                  (= ?x2 city3_1))))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (at truck3 city3_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (and
                      (autstate_1_4)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1))))
                    (and
                      (autstate_1_2)
                      (at truck3 city3_1)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1)))))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (at truck1 city2_1))
            (and
              (autstate_1_15)
              (at truck3 city3_1))
            (and
              (autstate_1_14)
              (at truck3 city3_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (autstate_1_13)
              (at truck2 city1_1))
            (and
              (autstate_1_12)
              (at truck2 city1_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (autstate_1_11)
              (at truck2 city1_1)
              (at truck3 city3_1)
              (or
                (not 
                  (= ?x0 truck3))
                (not 
                  (= ?x2 city3_1))))
            (and
              (autstate_1_10)
              (at truck2 city1_1)
              (at truck3 city3_1)
              (at truck1 city2_1)
              (or
                (not 
                  (= ?x0 truck3))
                (not 
                  (= ?x2 city3_1)))
              (or
                (not 
                  (= ?x0 truck1))
                (not 
                  (= ?x2 city2_1))))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x2 city2_1))
                    (not 
                      (= ?x0 truck1))))
                (and
                  (autstate_1_7)
                  (at truck3 city3_1))
                (and
                  (autstate_1_6)
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x2 city2_1))))
                (and
                  (at truck2 city1_1)
                  (or
                    (and
                      (autstate_1_5)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1))))
                    (and
                      (autstate_1_4)
                      (at truck1 city2_1)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x2 city1_1)))
                      (or
                        (not 
                          (= ?x0 truck1))
                        (not 
                          (= ?x2 city2_1))))
                    (and
                      (at truck3 city3_1)
                      (or
                        (and
                          (autstate_1_3)
                          (or
                            (not 
                              (= ?x0 truck2))
                            (not 
                              (= ?x2 city1_1))))
                        (and
                          (autstate_1_2)
                          (at truck1 city2_1)
                          (or
                            (not 
                              (= ?x0 truck2))
                            (not 
                              (= ?x2 city1_1)))
                          (or
                            (not 
                              (= ?x0 truck1))
                            (not 
                              (= ?x2 city2_1)))))))))))
          (autstate_1_1))
        (not 
          (at ?x0 ?x2))
      )
    )
  (:action unload-truck
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (truck ?x1)
        (location ?x2)
        (at ?x1 ?x2)
        (in ?x0 ?x1))
    :effect
      (and
        (at ?x0 ?x2)
        (when
          (and
            (autstate_1_2)
            (at truck1 city2_1))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (at truck3 city3_1))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (at truck1 city2_1))
            (and
              (autstate_1_3)
              (at truck3 city3_1))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (at truck3 city3_1))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (at truck2 city1_1))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (at truck1 city2_1))
            (and
              (autstate_1_3)
              (at truck2 city1_1))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (at truck3 city3_1))
            (and
              (autstate_1_4)
              (at truck2 city1_1))
            (and
              (autstate_1_2)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (at truck1 city2_1))
            (and
              (autstate_1_7)
              (at truck3 city3_1))
            (and
              (autstate_1_6)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (at truck3 city3_1)))
            (and
              (autstate_1_5)
              (at truck2 city1_1))
            (and
              (autstate_1_4)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_3)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (at truck3 city2_1))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck1 city2_1))
            (and
              (autstate_1_3)
              (at truck3 city2_1))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x2 city2_1)
                  (= ?x0 truck3))
                (at truck3 city2_1))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck3 city3_1))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (at truck3 city3_1)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (at truck1 city2_1))
            (and
              (autstate_1_11)
              (at truck3 city3_1))
            (and
              (autstate_1_10)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (at truck3 city3_1)))
            (and
              (autstate_1_5)
              (at truck3 city2_1))
            (and
              (autstate_1_4)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x2 city2_1)
                  (= ?x0 truck3))
                (at truck3 city2_1)))
            (and
              (autstate_1_3)
              (at truck3 city2_1)
              (at truck3 city3_1))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (or
                    (and
                      (at truck3 city2_1)
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (= ?x2 city2_1))))
                (and
                  (at truck3 city2_1)
                  (at truck3 city3_1)))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck2 city1_1))
            (and
              (autstate_1_6)
              (at truck3 city2_1))
            (and
              (autstate_1_2)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (at truck3 city2_1))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (at truck1 city2_1))
            (and
              (autstate_1_11)
              (at truck2 city1_1))
            (and
              (autstate_1_10)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_7)
              (at truck3 city2_1))
            (and
              (autstate_1_6)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x2 city2_1)
                  (= ?x0 truck3))
                (at truck3 city2_1)))
            (and
              (autstate_1_3)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (at truck3 city2_1)))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (at truck3 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (at truck3 city3_1))
            (and
              (autstate_1_12)
              (at truck2 city1_1))
            (and
              (autstate_1_10)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_8)
              (at truck3 city2_1))
            (and
              (autstate_1_6)
              (at truck3 city2_1)
              (at truck3 city3_1))
            (and
              (autstate_1_4)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (at truck3 city2_1)))
            (and
              (autstate_1_2)
              (at truck3 city3_1)
              (at truck3 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (at truck1 city2_1))
            (and
              (autstate_1_15)
              (at truck3 city3_1))
            (and
              (autstate_1_14)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (at truck3 city3_1)))
            (and
              (autstate_1_13)
              (at truck2 city1_1))
            (and
              (autstate_1_12)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_11)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_10)
              (at truck1 city2_1)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1))))
            (and
              (autstate_1_9)
              (at truck3 city2_1))
            (and
              (autstate_1_8)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x2 city2_1)
                  (= ?x0 truck3))
                (at truck3 city2_1)))
            (and
              (autstate_1_7)
              (at truck3 city2_1)
              (at truck3 city3_1))
            (and
              (autstate_1_6)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (or
                    (and
                      (at truck3 city2_1)
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (= ?x2 city2_1))))
                (and
                  (at truck3 city2_1)
                  (at truck3 city3_1))))
            (and
              (autstate_1_5)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (at truck3 city2_1)))
            (and
              (autstate_1_4)
              (at truck1 city2_1)
              (or
                (and
                  (at truck3 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1))))
            (and
              (autstate_1_3)
              (at truck3 city3_1)
              (at truck3 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (at truck3 city2_1)
                  (or
                    (and
                      (at truck2 city1_1)
                      (= ?x0 truck3)
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (= ?x0 truck2)
                      (= ?x2 city1_1))))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)
                  (at truck3 city3_1)))))
          (autstate_1_1))
        (not 
          (in ?x0 ?x1))
      )
    )
  (:action unload-airplane
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (package ?x0)
        (airplane ?x1)
        (location ?x2)
        (in ?x0 ?x1)
        (at ?x1 ?x2))
    :effect
      (and
        (at ?x0 ?x2)
        (when
          (and
            (autstate_1_2)
            (at truck1 city2_1))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (at truck3 city3_1))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (at truck1 city2_1))
            (and
              (autstate_1_3)
              (at truck3 city3_1))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (at truck3 city3_1))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (at truck2 city1_1))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (at truck1 city2_1))
            (and
              (autstate_1_3)
              (at truck2 city1_1))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (at truck3 city3_1))
            (and
              (autstate_1_4)
              (at truck2 city1_1))
            (and
              (autstate_1_2)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (at truck1 city2_1))
            (and
              (autstate_1_7)
              (at truck3 city3_1))
            (and
              (autstate_1_6)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (at truck3 city3_1)))
            (and
              (autstate_1_5)
              (at truck2 city1_1))
            (and
              (autstate_1_4)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_3)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (at truck3 city2_1))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck1 city2_1))
            (and
              (autstate_1_3)
              (at truck3 city2_1))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x2 city2_1)
                  (= ?x0 truck3))
                (at truck3 city2_1))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck3 city3_1))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (at truck3 city3_1)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (at truck1 city2_1))
            (and
              (autstate_1_11)
              (at truck3 city3_1))
            (and
              (autstate_1_10)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (at truck3 city3_1)))
            (and
              (autstate_1_5)
              (at truck3 city2_1))
            (and
              (autstate_1_4)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x2 city2_1)
                  (= ?x0 truck3))
                (at truck3 city2_1)))
            (and
              (autstate_1_3)
              (at truck3 city2_1)
              (at truck3 city3_1))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (or
                    (and
                      (at truck3 city2_1)
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (= ?x2 city2_1))))
                (and
                  (at truck3 city2_1)
                  (at truck3 city3_1)))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck2 city1_1))
            (and
              (autstate_1_6)
              (at truck3 city2_1))
            (and
              (autstate_1_2)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (at truck3 city2_1))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (at truck1 city2_1))
            (and
              (autstate_1_11)
              (at truck2 city1_1))
            (and
              (autstate_1_10)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_7)
              (at truck3 city2_1))
            (and
              (autstate_1_6)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x2 city2_1)
                  (= ?x0 truck3))
                (at truck3 city2_1)))
            (and
              (autstate_1_3)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (at truck3 city2_1)))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (at truck3 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (at truck3 city3_1))
            (and
              (autstate_1_12)
              (at truck2 city1_1))
            (and
              (autstate_1_10)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_8)
              (at truck3 city2_1))
            (and
              (autstate_1_6)
              (at truck3 city2_1)
              (at truck3 city3_1))
            (and
              (autstate_1_4)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (at truck3 city2_1)))
            (and
              (autstate_1_2)
              (at truck3 city3_1)
              (at truck3 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (at truck1 city2_1))
            (and
              (autstate_1_15)
              (at truck3 city3_1))
            (and
              (autstate_1_14)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (at truck3 city3_1)))
            (and
              (autstate_1_13)
              (at truck2 city1_1))
            (and
              (autstate_1_12)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_11)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_10)
              (at truck1 city2_1)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1))))
            (and
              (autstate_1_9)
              (at truck3 city2_1))
            (and
              (autstate_1_8)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x2 city2_1)
                  (= ?x0 truck3))
                (at truck3 city2_1)))
            (and
              (autstate_1_7)
              (at truck3 city2_1)
              (at truck3 city3_1))
            (and
              (autstate_1_6)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (or
                    (and
                      (at truck3 city2_1)
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (= ?x2 city2_1))))
                (and
                  (at truck3 city2_1)
                  (at truck3 city3_1))))
            (and
              (autstate_1_5)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (at truck3 city2_1)))
            (and
              (autstate_1_4)
              (at truck1 city2_1)
              (or
                (and
                  (at truck3 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1))))
            (and
              (autstate_1_3)
              (at truck3 city3_1)
              (at truck3 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (at truck2 city1_1)))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (at truck3 city2_1)
                  (or
                    (and
                      (at truck2 city1_1)
                      (= ?x0 truck3)
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (= ?x0 truck2)
                      (= ?x2 city1_1))))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)
                  (at truck3 city3_1)))))
          (autstate_1_1))
        (not 
          (in ?x0 ?x1))
      )
    )
  (:action drive-truck
    :parameters (?x0 ?x1 ?x2 ?x3)
    :precondition 
      (and
        (truck ?x0)
        (location ?x1)
        (location ?x2)
        (city ?x3)
        (at ?x0 ?x1)
        (in-city ?x1 ?x3)
        (in-city ?x2 ?x3))
    :effect
      (and
        (at ?x0 ?x2)
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 truck1)
                (= ?x2 city2_1))
              (and
                (at truck1 city2_1)
                (or
                  (not 
                    (= ?x0 truck1))
                  (not 
                    (= ?x1 city2_1))))))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 truck3)
                (= ?x2 city3_1))
              (and
                (at truck3 city3_1)
                (or
                  (not 
                    (= ?x0 truck3))
                  (not 
                    (= ?x1 city3_1))))))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1)))))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 truck2)
                (= ?x2 city1_1))
              (and
                (at truck2 city1_1)
                (or
                  (not 
                    (= ?x0 truck2))
                  (not 
                    (= ?x1 city1_1))))))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1)))))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (and
                      (at truck3 city3_1)
                      (= ?x0 truck1)
                      (= ?x2 city2_1))
                    (and
                      (at truck1 city2_1)
                      (= ?x0 truck3)
                      (= ?x2 city3_1))))
                (and
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 truck3)
                (= ?x2 city2_1))
              (and
                (at truck3 city2_1)
                (or
                  (not 
                    (= ?x0 truck3))
                  (not 
                    (= ?x1 city2_1))))))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x2 city2_1)
                  (or
                    (and
                      (at truck3 city2_1)
                      (= ?x0 truck1))
                    (and
                      (at truck1 city2_1)
                      (= ?x0 truck3))))
                (and
                  (at truck3 city2_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x1 city2_1))
                    (and
                      (not 
                        (= ?x0 truck3))
                      (not 
                        (= ?x0 truck1))))))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x0 truck3)
                  (or
                    (and
                      (at truck3 city2_1)
                      (not 
                        (= ?x1 city2_1))
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (not 
                        (= ?x1 city3_1))
                      (= ?x2 city2_1))))
                (and
                  (at truck3 city2_1)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (and
                      (not 
                        (= ?x1 city2_1))
                      (not 
                        (= ?x1 city3_1))))))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x2 city2_1)
                  (or
                    (and
                      (at truck3 city2_1)
                      (= ?x0 truck1))
                    (and
                      (at truck1 city2_1)
                      (= ?x0 truck3))))
                (and
                  (at truck3 city2_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x1 city2_1))
                    (and
                      (not 
                        (= ?x0 truck3))
                      (not 
                        (= ?x0 truck1)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 truck3)
                  (or
                    (and
                      (at truck3 city2_1)
                      (not 
                        (= ?x1 city2_1))
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (not 
                        (= ?x1 city3_1))
                      (= ?x2 city2_1))))
                (and
                  (at truck3 city2_1)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (and
                      (not 
                        (= ?x1 city2_1))
                      (not 
                        (= ?x1 city3_1)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (and
                      (at truck3 city3_1)
                      (= ?x0 truck1)
                      (= ?x2 city2_1))
                    (and
                      (at truck1 city2_1)
                      (= ?x0 truck3)
                      (= ?x2 city3_1))))
                (and
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (not 
                    (= ?x1 city3_1))
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (at truck3 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x2 city2_1)
                  (or
                    (and
                      (at truck3 city2_1)
                      (= ?x0 truck1))
                    (and
                      (at truck1 city2_1)
                      (= ?x0 truck3))))
                (and
                  (at truck3 city2_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x1 city2_1))
                    (and
                      (not 
                        (= ?x0 truck3))
                      (not 
                        (= ?x0 truck1)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (at truck3 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (and
                      (at truck2 city1_1)
                      (= ?x0 truck1)
                      (= ?x2 city2_1))
                    (and
                      (at truck1 city2_1)
                      (= ?x0 truck2)
                      (= ?x2 city1_1))))
                (and
                  (at truck2 city1_1)
                  (at truck1 city2_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 truck3)
                  (or
                    (and
                      (at truck3 city2_1)
                      (not 
                        (= ?x1 city2_1))
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (not 
                        (= ?x1 city3_1))
                      (= ?x2 city2_1))))
                (and
                  (at truck3 city2_1)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (and
                      (not 
                        (= ?x1 city2_1))
                      (not 
                        (= ?x1 city3_1)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (at truck3 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (and
                      (at truck2 city1_1)
                      (= ?x0 truck3)
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (= ?x0 truck2)
                      (= ?x2 city1_1))))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (not 
                    (= ?x1 city3_1))
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck1)
                  (= ?x2 city2_1))
                (and
                  (at truck1 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (and
                      (at truck3 city3_1)
                      (= ?x0 truck1)
                      (= ?x2 city2_1))
                    (and
                      (at truck1 city2_1)
                      (= ?x0 truck3)
                      (= ?x2 city3_1))))
                (and
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x2 city2_1)
                  (or
                    (and
                      (at truck3 city2_1)
                      (= ?x0 truck1))
                    (and
                      (at truck1 city2_1)
                      (= ?x0 truck3))))
                (and
                  (at truck3 city2_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x1 city2_1))
                    (and
                      (not 
                        (= ?x0 truck3))
                      (not 
                        (= ?x0 truck1)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 truck3)
                  (or
                    (and
                      (at truck3 city2_1)
                      (not 
                        (= ?x1 city2_1))
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (not 
                        (= ?x1 city3_1))
                      (= ?x2 city2_1))))
                (and
                  (at truck3 city2_1)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (and
                      (not 
                        (= ?x1 city2_1))
                      (not 
                        (= ?x1 city3_1)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (and
                      (at truck3 city3_1)
                      (= ?x0 truck1)
                      (= ?x2 city2_1))
                    (and
                      (at truck1 city2_1)
                      (= ?x0 truck3)
                      (= ?x2 city3_1))))
                (and
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (not 
                    (= ?x1 city3_1))
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (at truck3 city2_1)
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (and
                      (at truck2 city1_1)
                      (= ?x0 truck1)
                      (= ?x2 city2_1))
                    (and
                      (at truck1 city2_1)
                      (= ?x0 truck2)
                      (= ?x2 city1_1))))
                (and
                  (at truck2 city1_1)
                  (at truck1 city2_1)
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (and
                      (at truck2 city1_1)
                      (= ?x0 truck3)
                      (= ?x2 city3_1))
                    (and
                      (at truck3 city3_1)
                      (= ?x0 truck2)
                      (= ?x2 city1_1))))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (not 
                    (= ?x1 city3_1))
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (and
                      (at truck2 city1_1)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x1 city1_1)))
                      (or
                        (and
                          (at truck3 city3_1)
                          (= ?x0 truck1)
                          (= ?x2 city2_1))
                        (and
                          (at truck1 city2_1)
                          (= ?x0 truck3)
                          (= ?x2 city3_1))))
                    (and
                      (at truck3 city3_1)
                      (at truck1 city2_1)
                      (= ?x0 truck2)
                      (= ?x2 city1_1))))
                (and
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (not 
                    (= ?x1 city3_1))
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (at truck2 city1_1)
                  (at truck3 city3_1)
                  (at truck1 city2_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city2_1)))
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1)))))))
          (autstate_1_1))
        (not 
          (at ?x0 ?x1))
      )
    )
  (:action fly-airplane
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (airplane ?x0)
        (airport ?x1)
        (airport ?x2)
        (at ?x0 ?x1))
    :effect
      (and
        (at ?x0 ?x2)
        (when
          (and
            (autstate_1_2)
            (at truck1 city2_1))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (at truck3 city3_1))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (at truck1 city2_1))
            (and
              (autstate_1_3)
              (at truck3 city3_1))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1)))))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (at truck2 city1_1))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (at truck1 city2_1))
            (and
              (autstate_1_3)
              (at truck2 city1_1))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1)))))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (at truck3 city3_1))
            (and
              (autstate_1_4)
              (at truck2 city1_1))
            (and
              (autstate_1_2)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (at truck1 city2_1))
            (and
              (autstate_1_7)
              (at truck3 city3_1))
            (and
              (autstate_1_6)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_5)
              (at truck2 city1_1))
            (and
              (autstate_1_4)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_3)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (at truck3 city2_1))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck1 city2_1))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (at truck1 city2_1)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck3 city3_1))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (at truck3 city3_1)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (at truck1 city2_1))
            (and
              (autstate_1_11)
              (at truck3 city3_1))
            (and
              (autstate_1_10)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (at truck3 city2_1)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (at truck1 city2_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (at truck1 city2_1)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (at truck2 city1_1))
            (and
              (autstate_1_6)
              (at truck3 city2_1))
            (and
              (autstate_1_2)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (at truck1 city2_1))
            (and
              (autstate_1_11)
              (at truck2 city1_1))
            (and
              (autstate_1_10)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_7)
              (at truck3 city2_1))
            (and
              (autstate_1_6)
              (at truck3 city2_1)
              (at truck1 city2_1))
            (and
              (autstate_1_3)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_2)
              (at truck1 city2_1)
              (at truck3 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (at truck3 city3_1))
            (and
              (autstate_1_12)
              (at truck2 city1_1))
            (and
              (autstate_1_10)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_8)
              (at truck3 city2_1))
            (and
              (autstate_1_6)
              (at truck3 city2_1)
              (at truck3 city3_1))
            (and
              (autstate_1_4)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (autstate_1_2)
              (at truck3 city3_1)
              (at truck3 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (at truck1 city2_1))
            (and
              (autstate_1_15)
              (at truck3 city3_1))
            (and
              (autstate_1_14)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_13)
              (at truck2 city1_1))
            (and
              (autstate_1_12)
              (at truck1 city2_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_11)
              (at truck3 city3_1)
              (or
                (and
                  (= ?x0 truck2)
                  (= ?x2 city1_1))
                (and
                  (at truck2 city1_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1))))))
            (and
              (autstate_1_10)
              (at truck1 city2_1)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city3_1))
                (and
                  (at truck3 city3_1)
                  (or
                    (not 
                      (= ?x0 truck3))
                    (not 
                      (= ?x1 city3_1)))
                  (or
                    (not 
                      (= ?x0 truck1))
                    (not 
                      (= ?x1 city2_1))))))
            (and
              (autstate_1_9)
              (at truck3 city2_1))
            (and
              (autstate_1_8)
              (at truck3 city2_1)
              (at truck1 city2_1))
            (and
              (autstate_1_7)
              (at truck3 city2_1)
              (at truck3 city3_1))
            (and
              (autstate_1_6)
              (at truck3 city2_1)
              (at truck3 city3_1)
              (at truck1 city2_1))
            (and
              (autstate_1_5)
              (at truck2 city1_1)
              (or
                (and
                  (= ?x0 truck3)
                  (= ?x2 city2_1))
                (and
                  (at truck3 city2_1)
                  (or
                    (not 
                      (= ?x0 truck2))
                    (not 
                      (= ?x1 city1_1))))))
            (and
              (at truck3 city2_1)
              (or
                (and
                  (autstate_1_4)
                  (at truck1 city2_1)
                  (or
                    (and
                      (= ?x0 truck2)
                      (= ?x2 city1_1))
                    (and
                      (at truck2 city1_1)
                      (or
                        (not 
                          (= ?x0 truck2))
                        (not 
                          (= ?x1 city1_1))))))
                (and
                  (at truck3 city3_1)
                  (or
                    (and
                      (autstate_1_3)
                      (or
                        (and
                          (= ?x0 truck2)
                          (= ?x2 city1_1))
                        (and
                          (at truck2 city1_1)
                          (or
                            (not 
                              (= ?x0 truck2))
                            (not 
                              (= ?x1 city1_1))))))
                    (and
                      (autstate_1_2)
                      (at truck1 city2_1)
                      (or
                        (and
                          (= ?x0 truck2)
                          (= ?x2 city1_1))
                        (and
                          (at truck2 city1_1)
                          (or
                            (not 
                              (= ?x0 truck2))
                            (not 
                              (= ?x1 city1_1)))))))))))
          (autstate_1_1))
        (not 
          (at ?x0 ?x1))
      )
    )
)