(define (problem coffee1)

  (:domain robot_coffee)
  (:objects c - kitchen lab - office o1 - office o2 - office o3 - office)
  (:init 
    (robot_at lab)
    (connected lab o1)
    (connected o1 lab)
    (connected o1 o2)
    (connected o2 o1)
    (connected o1 o3)
    (connected o3 o1)
    (connected o3 o2)
    (connected o2 o3)
    (connected c o3)
    (connected o3 c)
    (autstate_1_2)
    (autstate_2_2)
    (autstate_3_2)
  )
  (:goal (and
    (autstate_1_1)
    (autstate_2_1)
    (autstate_3_1)))
)