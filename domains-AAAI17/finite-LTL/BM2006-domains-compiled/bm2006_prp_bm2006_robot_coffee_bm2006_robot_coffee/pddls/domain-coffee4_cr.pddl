(define (domain robot_coffee)
  (:types
    office - room
    kitchen - room
  )

  (:predicates
    (robot_at ?x0 - room)
    (coffee_at ?x0 - room)
    (connected ?x0 - room ?x1 - room)
    (has_coffee)
    (autstate_1_2)
    (autstate_1_1)
    (autstate_2_2)
    (autstate_2_1)
    (autstate_3_2)
    (autstate_3_1)
    (autstate_4_2)
    (autstate_4_3)
    (autstate_4_1)
    (autstate_5_2)
    (autstate_5_3)
    (autstate_5_1)
  )
  (:action move_to
    :parameters (?x0 - room ?x1 - room)
    :precondition 
      (and
        (robot_at ?x0)
        (connected ?x0 ?x1))
    :effect
      (and
        (robot_at ?x1)
        (when
          (and
            (autstate_1_2)
            (coffee_at o1))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (coffee_at o2))
          (autstate_2_1))
        (when
          (and
            (autstate_3_2)
            (coffee_at o3))
          (autstate_3_1))
        (when
          (and
            (autstate_4_3)
            (or
              (and
                (not 
                  (robot_at o3))
                (not 
                  (= ?x1 o3))
                (= ?x0 o2))
              (and
                (not 
                  (robot_at o2))
                (not 
                  (= ?x1 o2))
                (= ?x0 o3))
              (and
                (not 
                  (robot_at o3))
                (not 
                  (robot_at o2))
                (not 
                  (= ?x1 o3))
                (not 
                  (= ?x1 o2)))))
          (autstate_4_2))
        (when
          (autstate_4_2)
          (autstate_4_3))
        (when
          (or
            (and
              (autstate_4_3)
              (or
                (and
                  (not 
                    (robot_at o3))
                  (not 
                    (= ?x1 o3))
                  (= ?x0 o2))
                (and
                  (not 
                    (robot_at o2))
                  (not 
                    (= ?x1 o2))
                  (= ?x0 o3))
                (and
                  (not 
                    (robot_at o3))
                  (not 
                    (robot_at o2))
                  (not 
                    (= ?x1 o3))
                  (not 
                    (= ?x1 o2)))))
            (and
              (autstate_4_2)
              (or
                (= ?x0 o2)
                (and
                  (not 
                    (robot_at o2))
                  (not 
                    (= ?x1 o2))))))
          (autstate_4_1))
        (when
          (and
            (autstate_5_3)
            (or
              (and
                (not 
                  (robot_at o2))
                (not 
                  (= ?x1 o2))
                (= ?x0 o3))
              (and
                (not 
                  (robot_at o3))
                (not 
                  (= ?x1 o3))
                (= ?x0 o2))
              (and
                (not 
                  (robot_at o2))
                (not 
                  (robot_at o3))
                (not 
                  (= ?x1 o2))
                (not 
                  (= ?x1 o3)))))
          (autstate_5_2))
        (when
          (autstate_5_2)
          (autstate_5_3))
        (when
          (or
            (and
              (autstate_5_3)
              (or
                (and
                  (not 
                    (robot_at o2))
                  (not 
                    (= ?x1 o2))
                  (= ?x0 o3))
                (and
                  (not 
                    (robot_at o3))
                  (not 
                    (= ?x1 o3))
                  (= ?x0 o2))
                (and
                  (not 
                    (robot_at o2))
                  (not 
                    (robot_at o3))
                  (not 
                    (= ?x1 o2))
                  (not 
                    (= ?x1 o3)))))
            (and
              (autstate_5_2)
              (or
                (= ?x0 o3)
                (and
                  (not 
                    (robot_at o3))
                  (not 
                    (= ?x1 o3))))))
          (autstate_5_1))
        (not 
          (robot_at ?x0))
        (when
          (and
            (or
              (not 
                (autstate_4_3))
              (and
                (or
                  (robot_at o2)
                  (= ?x1 o2)
                  (not 
                    (= ?x0 o3)))
                (or
                  (robot_at o3)
                  (robot_at o2)
                  (= ?x1 o3)
                  (= ?x1 o2))))
            (not 
              (= ?x0 o2))
            (or
              (robot_at o2)
              (= ?x1 o2))
            (autstate_4_2))
          (not 
            (autstate_4_2)))
        (when
          (and
            (not 
              (autstate_4_2))
            (not 
              (= ?x0 o3))
            (or
              (robot_at o3)
              (= ?x1 o3))
            (autstate_4_3))
          (not 
            (autstate_4_3)))
        (when
          (and
            (or
              (not 
                (autstate_4_3))
              (and
                (or
                  (robot_at o3)
                  (= ?x1 o3)
                  (not 
                    (= ?x0 o2)))
                (or
                  (robot_at o2)
                  (= ?x1 o2)
                  (not 
                    (= ?x0 o3)))
                (or
                  (robot_at o3)
                  (robot_at o2)
                  (= ?x1 o3)
                  (= ?x1 o2))))
            (or
              (not 
                (autstate_4_2))
              (and
                (not 
                  (= ?x0 o2))
                (or
                  (robot_at o2)
                  (= ?x1 o2)))))
          (not 
            (autstate_4_1)))
        (when
          (and
            (or
              (not 
                (autstate_5_3))
              (and
                (or
                  (robot_at o3)
                  (= ?x1 o3)
                  (not 
                    (= ?x0 o2)))
                (or
                  (robot_at o2)
                  (robot_at o3)
                  (= ?x1 o2)
                  (= ?x1 o3))))
            (not 
              (= ?x0 o3))
            (or
              (robot_at o3)
              (= ?x1 o3))
            (autstate_5_2))
          (not 
            (autstate_5_2)))
        (when
          (and
            (not 
              (autstate_5_2))
            (not 
              (= ?x0 o2))
            (or
              (robot_at o2)
              (= ?x1 o2))
            (autstate_5_3))
          (not 
            (autstate_5_3)))
        (when
          (and
            (or
              (not 
                (autstate_5_3))
              (and
                (or
                  (robot_at o2)
                  (= ?x1 o2)
                  (not 
                    (= ?x0 o3)))
                (or
                  (robot_at o3)
                  (= ?x1 o3)
                  (not 
                    (= ?x0 o2)))
                (or
                  (robot_at o2)
                  (robot_at o3)
                  (= ?x1 o2)
                  (= ?x1 o3))))
            (or
              (not 
                (autstate_5_2))
              (and
                (not 
                  (= ?x0 o3))
                (or
                  (robot_at o3)
                  (= ?x1 o3)))))
          (not 
            (autstate_5_1)))
      )
    )
  (:action prepare_coffee
    :parameters (?x0 - kitchen)
    :precondition 
      (and
        (robot_at ?x0)
        (not 
          (has_coffee)))
    :effect
      (and
        (has_coffee)
        (when
          (and
            (autstate_1_2)
            (coffee_at o1))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (coffee_at o2))
          (autstate_2_1))
        (when
          (and
            (autstate_3_2)
            (coffee_at o3))
          (autstate_3_1))
        (when
          (and
            (autstate_4_3)
            (not 
              (robot_at o3))
            (not 
              (robot_at o2)))
          (autstate_4_2))
        (when
          (autstate_4_2)
          (autstate_4_3))
        (when
          (and
            (not 
              (robot_at o2))
            (or
              (and
                (autstate_4_3)
                (not 
                  (robot_at o3)))
              (autstate_4_2)))
          (autstate_4_1))
        (when
          (and
            (autstate_5_3)
            (not 
              (robot_at o2))
            (not 
              (robot_at o3)))
          (autstate_5_2))
        (when
          (autstate_5_2)
          (autstate_5_3))
        (when
          (and
            (not 
              (robot_at o3))
            (or
              (and
                (autstate_5_3)
                (not 
                  (robot_at o2)))
              (autstate_5_2)))
          (autstate_5_1))
        (when
          (and
            (robot_at o2)
            (autstate_4_2))
          (not 
            (autstate_4_2)))
        (when
          (and
            (not 
              (autstate_4_2))
            (robot_at o3)
            (autstate_4_3))
          (not 
            (autstate_4_3)))
        (when
          (or
            (robot_at o2)
            (and
              (or
                (not 
                  (autstate_4_3))
                (robot_at o3))
              (not 
                (autstate_4_2))))
          (not 
            (autstate_4_1)))
        (when
          (and
            (robot_at o3)
            (autstate_5_2))
          (not 
            (autstate_5_2)))
        (when
          (and
            (not 
              (autstate_5_2))
            (robot_at o2)
            (autstate_5_3))
          (not 
            (autstate_5_3)))
        (when
          (or
            (robot_at o3)
            (and
              (or
                (not 
                  (autstate_5_3))
                (robot_at o2))
              (not 
                (autstate_5_2))))
          (not 
            (autstate_5_1)))
      )
    )
  (:action put_coffee
    :parameters (?x0 - office)
    :precondition 
      (and
        (robot_at ?x0)
        (has_coffee))
    :effect
      (and
        (coffee_at ?x0)
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x0 o1)
              (coffee_at o1)))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (or
              (= ?x0 o2)
              (coffee_at o2)))
          (autstate_2_1))
        (when
          (and
            (autstate_3_2)
            (or
              (= ?x0 o3)
              (coffee_at o3)))
          (autstate_3_1))
        (when
          (and
            (autstate_4_3)
            (not 
              (robot_at o3))
            (not 
              (robot_at o2)))
          (autstate_4_2))
        (when
          (autstate_4_2)
          (autstate_4_3))
        (when
          (and
            (not 
              (robot_at o2))
            (or
              (and
                (autstate_4_3)
                (not 
                  (robot_at o3)))
              (autstate_4_2)))
          (autstate_4_1))
        (when
          (and
            (autstate_5_3)
            (not 
              (robot_at o2))
            (not 
              (robot_at o3)))
          (autstate_5_2))
        (when
          (autstate_5_2)
          (autstate_5_3))
        (when
          (and
            (not 
              (robot_at o3))
            (or
              (and
                (autstate_5_3)
                (not 
                  (robot_at o2)))
              (autstate_5_2)))
          (autstate_5_1))
        (not 
          (has_coffee))
        (when
          (and
            (robot_at o2)
            (autstate_4_2))
          (not 
            (autstate_4_2)))
        (when
          (and
            (not 
              (autstate_4_2))
            (robot_at o3)
            (autstate_4_3))
          (not 
            (autstate_4_3)))
        (when
          (or
            (robot_at o2)
            (and
              (or
                (not 
                  (autstate_4_3))
                (robot_at o3))
              (not 
                (autstate_4_2))))
          (not 
            (autstate_4_1)))
        (when
          (and
            (robot_at o3)
            (autstate_5_2))
          (not 
            (autstate_5_2)))
        (when
          (and
            (not 
              (autstate_5_2))
            (robot_at o2)
            (autstate_5_3))
          (not 
            (autstate_5_3)))
        (when
          (or
            (robot_at o3)
            (and
              (or
                (not 
                  (autstate_5_3))
                (robot_at o2))
              (not 
                (autstate_5_2))))
          (not 
            (autstate_5_1)))
      )
    )
)