(define (domain robot_coffee)
  (:types
    office - room
    kitchen - room
  )

  (:predicates
    (robot_at ?x0 - room)
    (coffee_at ?x0 - room)
    (connected ?x0 - room ?x1 - room)
    (has_coffee)
    (autstate_1_2)
    (autstate_1_1)
  )
  (:action move_to
    :parameters (?x0 - room ?x1 - room)
    :precondition 
      (and
        (robot_at ?x0)
        (connected ?x0 ?x1))
    :effect
      (and
        (robot_at ?x1)
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x0 lab)
              (and
                (not 
                  (robot_at lab))
                (not 
                  (= ?x1 lab)))))
          (autstate_1_1))
        (not 
          (robot_at ?x0))
      )
    )
  (:action prepare_coffee
    :parameters (?x0 - kitchen)
    :precondition 
      (and
        (robot_at ?x0)
        (not 
          (has_coffee)))
    :effect
      (and
        (has_coffee)
        (when
          (and
            (autstate_1_2)
            (not 
              (robot_at lab)))
          (autstate_1_1))
      )
    )
  (:action put_coffee
    :parameters (?x0 - office)
    :precondition 
      (and
        (robot_at ?x0)
        (has_coffee))
    :effect
      (and
        (coffee_at ?x0)
        (when
          (and
            (autstate_1_2)
            (not 
              (robot_at lab)))
          (autstate_1_1))
        (not 
          (has_coffee))
      )
    )
)