(define (domain robot_coffee)
  (:types
    office - room
    kitchen - room
  )

  (:predicates
    (robot_at ?x0 - room)
    (coffee_at ?x0 - room)
    (connected ?x0 - room ?x1 - room)
    (has_coffee)
    (autstate_1_2)
    (autstate_1_3)
    (autstate_1_4)
    (autstate_1_5)
    (autstate_1_6)
    (autstate_1_1)
  )
  (:action move_to
    :parameters (?x0 - room ?x1 - room)
    :precondition 
      (and
        (robot_at ?x0)
        (connected ?x0 ?x1))
    :effect
      (and
        (robot_at ?x1)
        (when
          (autstate_1_2)
          (autstate_1_3))
        (when
          (autstate_1_3)
          (autstate_1_4))
        (when
          (autstate_1_4)
          (autstate_1_5))
        (when
          (autstate_1_5)
          (autstate_1_6))
        (when
          (and
            (autstate_1_6)
            (or
              (= ?x1 c)
              (and
                (robot_at c)
                (not 
                  (= ?x0 c)))))
          (autstate_1_1))
        (not 
          (robot_at ?x0))
        (not 
          (autstate_1_2))
        (when
          (not 
            (autstate_1_2))
          (not 
            (autstate_1_3)))
        (when
          (not 
            (autstate_1_3))
          (not 
            (autstate_1_4)))
        (when
          (not 
            (autstate_1_4))
          (not 
            (autstate_1_5)))
        (when
          (not 
            (autstate_1_5))
          (not 
            (autstate_1_6)))
      )
    )
  (:action prepare_coffee
    :parameters (?x0 - kitchen)
    :precondition 
      (and
        (robot_at ?x0)
        (not 
          (has_coffee)))
    :effect
      (and
        (has_coffee)
        (when
          (autstate_1_2)
          (autstate_1_3))
        (when
          (autstate_1_3)
          (autstate_1_4))
        (when
          (autstate_1_4)
          (autstate_1_5))
        (when
          (autstate_1_5)
          (autstate_1_6))
        (when
          (and
            (autstate_1_6)
            (robot_at c))
          (autstate_1_1))
        (not 
          (autstate_1_2))
        (when
          (not 
            (autstate_1_2))
          (not 
            (autstate_1_3)))
        (when
          (not 
            (autstate_1_3))
          (not 
            (autstate_1_4)))
        (when
          (not 
            (autstate_1_4))
          (not 
            (autstate_1_5)))
        (when
          (not 
            (autstate_1_5))
          (not 
            (autstate_1_6)))
      )
    )
  (:action put_coffee
    :parameters (?x0 - office)
    :precondition 
      (and
        (robot_at ?x0)
        (has_coffee))
    :effect
      (and
        (coffee_at ?x0)
        (when
          (autstate_1_2)
          (autstate_1_3))
        (when
          (autstate_1_3)
          (autstate_1_4))
        (when
          (autstate_1_4)
          (autstate_1_5))
        (when
          (autstate_1_5)
          (autstate_1_6))
        (when
          (and
            (autstate_1_6)
            (robot_at c))
          (autstate_1_1))
        (not 
          (has_coffee))
        (not 
          (autstate_1_2))
        (when
          (not 
            (autstate_1_2))
          (not 
            (autstate_1_3)))
        (when
          (not 
            (autstate_1_3))
          (not 
            (autstate_1_4)))
        (when
          (not 
            (autstate_1_4))
          (not 
            (autstate_1_5)))
        (when
          (not 
            (autstate_1_5))
          (not 
            (autstate_1_6)))
      )
    )
)