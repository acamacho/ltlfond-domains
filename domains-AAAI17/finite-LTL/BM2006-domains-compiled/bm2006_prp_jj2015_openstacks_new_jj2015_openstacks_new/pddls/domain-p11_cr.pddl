(define (domain dom)
  (:types
    count - no_type
    product - no_type
    order - no_type
  )

  (:predicates
    (includes ?x0 - order ?x1 - product)
    (waiting ?x0 - order)
    (started ?x0 - order)
    (shipped ?x0 - order)
    (made ?x0 - product)
    (machine-available)
    (machine-configured ?x0 - product)
    (stacks-avail ?x0 - count)
    (next-count ?x0 - count ?x1 - count)
    (autstate_1_2)
    (autstate_1_3)
    (autstate_1_4)
    (autstate_1_5)
    (autstate_1_6)
    (autstate_1_7)
    (autstate_1_8)
    (autstate_1_9)
    (autstate_1_10)
    (autstate_1_11)
    (autstate_1_12)
    (autstate_1_13)
    (autstate_1_14)
    (autstate_1_15)
    (autstate_1_16)
    (autstate_1_1)
  )
  (:action setup-machine
    :parameters (?x0 - product ?x1 - count)
    :precondition 
      (and
        (machine-available)
        (not 
          (made ?x0))
        (stacks-avail ?x1))
    :effect
      (and
        (machine-configured ?x0)
        (when
          (and
            (autstate_1_2)
            (shipped o4))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (shipped o3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (shipped o4))
            (and
              (shipped o3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (shipped o2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (shipped o4))
            (and
              (shipped o2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (shipped o3))
            (and
              (shipped o2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (shipped o3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (shipped o4))
            (and
              (autstate_1_7)
              (shipped o3))
            (and
              (autstate_1_6)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (shipped o4))
                (and
                  (shipped o3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (shipped o1))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o3))
            (and
              (shipped o1)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (shipped o3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o3))
            (and
              (autstate_1_10)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (shipped o4))
                (and
                  (shipped o3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o2))
            (and
              (shipped o1)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (shipped o2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o2))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (shipped o4))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (shipped o3))
            (and
              (autstate_1_12)
              (shipped o2))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o3))
            (and
              (shipped o1)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (shipped o3))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (shipped o3)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (shipped o4))
            (and
              (autstate_1_15)
              (shipped o3))
            (and
              (autstate_1_14)
              (shipped o3)
              (shipped o4))
            (and
              (autstate_1_13)
              (shipped o2))
            (and
              (autstate_1_12)
              (shipped o2)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o2)
              (shipped o3))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (shipped o4))
                (and
                  (autstate_1_7)
                  (shipped o3))
                (and
                  (autstate_1_6)
                  (shipped o3)
                  (shipped o4))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (shipped o4))
                    (and
                      (shipped o3)
                      (or
                        (autstate_1_3)
                        (and
                          (autstate_1_2)
                          (shipped o4)))))))))
          (autstate_1_1))
        (not 
          (machine-available))
      )
    )
  (:action make-product
    :parameters (?x0 - product ?x1 - count)
    :precondition 
      (and
        (machine-configured ?x0)
        (forall (?x2 - order)
          (imply
            (includes)
            (started)))

        (stacks-avail ?x1))
    :effect
      (and
        (machine-available)
        (made ?x0)
        (when
          (and
            (autstate_1_2)
            (shipped o4))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (shipped o3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (shipped o4))
            (and
              (shipped o3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (shipped o2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (shipped o4))
            (and
              (shipped o2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (shipped o3))
            (and
              (shipped o2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (shipped o3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (shipped o4))
            (and
              (autstate_1_7)
              (shipped o3))
            (and
              (autstate_1_6)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (shipped o4))
                (and
                  (shipped o3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (shipped o1))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o3))
            (and
              (shipped o1)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (shipped o3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o3))
            (and
              (autstate_1_10)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (shipped o4))
                (and
                  (shipped o3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o2))
            (and
              (shipped o1)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (shipped o2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o2))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (shipped o4))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (shipped o3))
            (and
              (autstate_1_12)
              (shipped o2))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o3))
            (and
              (shipped o1)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (shipped o3))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (shipped o3)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (shipped o4))
            (and
              (autstate_1_15)
              (shipped o3))
            (and
              (autstate_1_14)
              (shipped o3)
              (shipped o4))
            (and
              (autstate_1_13)
              (shipped o2))
            (and
              (autstate_1_12)
              (shipped o2)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o2)
              (shipped o3))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (shipped o4))
                (and
                  (autstate_1_7)
                  (shipped o3))
                (and
                  (autstate_1_6)
                  (shipped o3)
                  (shipped o4))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (shipped o4))
                    (and
                      (shipped o3)
                      (or
                        (autstate_1_3)
                        (and
                          (autstate_1_2)
                          (shipped o4)))))))))
          (autstate_1_1))
        (not 
          (machine-configured ?x0))
      )
    )
  (:action start-order
    :parameters (?x0 - order ?x1 - count ?x2 - count)
    :precondition 
      (and
        (waiting ?x0)
        (stacks-avail ?x1)
        (next-count ?x2 ?x1))
    :effect
      (and
        (started ?x0)
        (stacks-avail ?x2)
        (when
          (and
            (autstate_1_2)
            (shipped o4))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (shipped o3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (shipped o4))
            (and
              (shipped o3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (shipped o2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (shipped o4))
            (and
              (shipped o2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (shipped o3))
            (and
              (shipped o2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (shipped o3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (shipped o4))
            (and
              (autstate_1_7)
              (shipped o3))
            (and
              (autstate_1_6)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (shipped o4))
                (and
                  (shipped o3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (shipped o1))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o3))
            (and
              (shipped o1)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (shipped o3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o3))
            (and
              (autstate_1_10)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (shipped o4))
                (and
                  (shipped o3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o2))
            (and
              (shipped o1)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (shipped o2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o2))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (shipped o4))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (shipped o3))
            (and
              (autstate_1_12)
              (shipped o2))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o3))
            (and
              (shipped o1)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (shipped o3))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (shipped o3)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (shipped o4))
            (and
              (autstate_1_15)
              (shipped o3))
            (and
              (autstate_1_14)
              (shipped o3)
              (shipped o4))
            (and
              (autstate_1_13)
              (shipped o2))
            (and
              (autstate_1_12)
              (shipped o2)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o2)
              (shipped o3))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (shipped o4))
                (and
                  (autstate_1_7)
                  (shipped o3))
                (and
                  (autstate_1_6)
                  (shipped o3)
                  (shipped o4))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (shipped o4))
                    (and
                      (shipped o3)
                      (or
                        (autstate_1_3)
                        (and
                          (autstate_1_2)
                          (shipped o4)))))))))
          (autstate_1_1))
        (not 
          (waiting ?x0))
        (not 
          (stacks-avail ?x1))
      )
    )
  (:action ship-order
    :parameters (?x0 - order ?x1 - count ?x2 - count)
    :precondition 
      (and
        (started ?x0)
        (forall (?x3 - product)
          (imply
            (includes)
            (made)))

        (stacks-avail ?x1)
        (next-count ?x1 ?x2))
    :effect
      (and
        (shipped ?x0)
        (stacks-avail ?x2)
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x0 o4)
              (shipped o4)))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x0 o3)
              (shipped o3)))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (or
                (= ?x0 o4)
                (shipped o4)))
            (and
              (autstate_1_3)
              (or
                (= ?x0 o3)
                (shipped o3)))
            (and
              (autstate_1_2)
              (or
                (and
                  (shipped o3)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (shipped o4)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x0 o2)
              (shipped o2)))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (= ?x0 o4)
                (shipped o4)))
            (and
              (autstate_1_3)
              (or
                (= ?x0 o2)
                (shipped o2)))
            (and
              (autstate_1_2)
              (or
                (and
                  (shipped o2)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (shipped o4)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (= ?x0 o3)
                (shipped o3)))
            (and
              (autstate_1_4)
              (or
                (= ?x0 o2)
                (shipped o2)))
            (and
              (autstate_1_2)
              (or
                (and
                  (shipped o2)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (shipped o3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (or
                (= ?x0 o4)
                (shipped o4)))
            (and
              (autstate_1_7)
              (or
                (= ?x0 o3)
                (shipped o3)))
            (and
              (autstate_1_6)
              (or
                (and
                  (shipped o3)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (shipped o4))))
            (and
              (autstate_1_5)
              (or
                (= ?x0 o2)
                (shipped o2)))
            (and
              (autstate_1_4)
              (or
                (and
                  (shipped o2)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (shipped o4))))
            (and
              (autstate_1_3)
              (or
                (and
                  (shipped o2)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (shipped o3))))
            (and
              (autstate_1_2)
              (or
                (and
                  (shipped o2)
                  (or
                    (and
                      (shipped o3)
                      (= ?x0 o4))
                    (and
                      (shipped o4)
                      (= ?x0 o3))))
                (and
                  (shipped o3)
                  (shipped o4)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (shipped o3)
                  (shipped o4)))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x0 o1)
              (shipped o1)))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (= ?x0 o4)
                (shipped o4)))
            (and
              (autstate_1_3)
              (or
                (= ?x0 o1)
                (shipped o1)))
            (and
              (autstate_1_2)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o4)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (= ?x0 o3)
                (shipped o3)))
            (and
              (autstate_1_4)
              (or
                (= ?x0 o1)
                (shipped o1)))
            (and
              (autstate_1_2)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (or
                (= ?x0 o4)
                (shipped o4)))
            (and
              (autstate_1_11)
              (or
                (= ?x0 o3)
                (shipped o3)))
            (and
              (autstate_1_10)
              (or
                (and
                  (shipped o3)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (shipped o4))))
            (and
              (autstate_1_5)
              (or
                (= ?x0 o1)
                (shipped o1)))
            (and
              (autstate_1_4)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o4))))
            (and
              (autstate_1_3)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o3))))
            (and
              (autstate_1_2)
              (or
                (and
                  (shipped o1)
                  (or
                    (and
                      (shipped o3)
                      (= ?x0 o4))
                    (and
                      (shipped o4)
                      (= ?x0 o3))))
                (and
                  (shipped o3)
                  (shipped o4)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o3)
                  (shipped o4)))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (= ?x0 o2)
                (shipped o2)))
            (and
              (autstate_1_6)
              (or
                (= ?x0 o1)
                (shipped o1)))
            (and
              (autstate_1_2)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (= ?x0 o4)
                (shipped o4)))
            (and
              (autstate_1_11)
              (or
                (= ?x0 o2)
                (shipped o2)))
            (and
              (autstate_1_10)
              (or
                (and
                  (shipped o2)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (shipped o4))))
            (and
              (autstate_1_7)
              (or
                (= ?x0 o1)
                (shipped o1)))
            (and
              (autstate_1_6)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o4))))
            (and
              (autstate_1_3)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o2))))
            (and
              (autstate_1_2)
              (or
                (and
                  (shipped o1)
                  (or
                    (and
                      (shipped o2)
                      (= ?x0 o4))
                    (and
                      (shipped o4)
                      (= ?x0 o2))))
                (and
                  (shipped o2)
                  (shipped o4)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o2)
                  (shipped o4)))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (= ?x0 o3)
                (shipped o3)))
            (and
              (autstate_1_12)
              (or
                (= ?x0 o2)
                (shipped o2)))
            (and
              (autstate_1_10)
              (or
                (and
                  (shipped o2)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (shipped o3))))
            (and
              (autstate_1_8)
              (or
                (= ?x0 o1)
                (shipped o1)))
            (and
              (autstate_1_6)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o3))))
            (and
              (autstate_1_4)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o2))))
            (and
              (autstate_1_2)
              (or
                (and
                  (shipped o1)
                  (or
                    (and
                      (shipped o2)
                      (= ?x0 o3))
                    (and
                      (shipped o3)
                      (= ?x0 o2))))
                (and
                  (shipped o2)
                  (shipped o3)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o2)
                  (shipped o3)))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (or
                (= ?x0 o4)
                (shipped o4)))
            (and
              (autstate_1_15)
              (or
                (= ?x0 o3)
                (shipped o3)))
            (and
              (autstate_1_14)
              (or
                (and
                  (shipped o3)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (shipped o4))))
            (and
              (autstate_1_13)
              (or
                (= ?x0 o2)
                (shipped o2)))
            (and
              (autstate_1_12)
              (or
                (and
                  (shipped o2)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (shipped o4))))
            (and
              (autstate_1_11)
              (or
                (and
                  (shipped o2)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (shipped o3))))
            (and
              (autstate_1_10)
              (or
                (and
                  (shipped o2)
                  (or
                    (and
                      (shipped o3)
                      (= ?x0 o4))
                    (and
                      (shipped o4)
                      (= ?x0 o3))))
                (and
                  (shipped o3)
                  (shipped o4)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (shipped o3)
                  (shipped o4))))
            (and
              (autstate_1_9)
              (or
                (= ?x0 o1)
                (shipped o1)))
            (and
              (autstate_1_8)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o4))
                (and
                  (shipped o4)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o4))))
            (and
              (autstate_1_7)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o3))
                (and
                  (shipped o3)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o3))))
            (and
              (autstate_1_6)
              (or
                (and
                  (shipped o1)
                  (or
                    (and
                      (shipped o3)
                      (= ?x0 o4))
                    (and
                      (shipped o4)
                      (= ?x0 o3))))
                (and
                  (shipped o3)
                  (shipped o4)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o3)
                  (shipped o4))))
            (and
              (autstate_1_5)
              (or
                (and
                  (shipped o1)
                  (= ?x0 o2))
                (and
                  (shipped o2)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o2))))
            (and
              (autstate_1_4)
              (or
                (and
                  (shipped o1)
                  (or
                    (and
                      (shipped o2)
                      (= ?x0 o4))
                    (and
                      (shipped o4)
                      (= ?x0 o2))))
                (and
                  (shipped o2)
                  (shipped o4)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o2)
                  (shipped o4))))
            (and
              (autstate_1_3)
              (or
                (and
                  (shipped o1)
                  (or
                    (and
                      (shipped o2)
                      (= ?x0 o3))
                    (and
                      (shipped o3)
                      (= ?x0 o2))))
                (and
                  (shipped o2)
                  (shipped o3)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o2)
                  (shipped o3))))
            (and
              (autstate_1_2)
              (or
                (and
                  (shipped o1)
                  (or
                    (and
                      (shipped o2)
                      (or
                        (and
                          (shipped o3)
                          (= ?x0 o4))
                        (and
                          (shipped o4)
                          (= ?x0 o3))))
                    (and
                      (shipped o3)
                      (shipped o4)
                      (= ?x0 o2))))
                (and
                  (shipped o2)
                  (shipped o3)
                  (shipped o4)
                  (= ?x0 o1))
                (and
                  (shipped o1)
                  (shipped o2)
                  (shipped o3)
                  (shipped o4)))))
          (autstate_1_1))
        (not 
          (started ?x0))
        (not 
          (stacks-avail ?x1))
      )
    )
  (:action open-new-stack
    :parameters (?x0 - count ?x1 - count)
    :precondition 
      (and
        (stacks-avail ?x0)
        (next-count ?x0 ?x1))
    :effect
      (and
        (stacks-avail ?x1)
        (when
          (and
            (autstate_1_2)
            (shipped o4))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (shipped o3))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (shipped o4))
            (and
              (shipped o3)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (shipped o2))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (shipped o4))
            (and
              (shipped o2)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (shipped o3))
            (and
              (shipped o2)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (shipped o3)))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (shipped o4))
            (and
              (autstate_1_7)
              (shipped o3))
            (and
              (autstate_1_6)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o2)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (shipped o4))
                (and
                  (shipped o3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (shipped o1))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_3)
                (and
                  (autstate_1_2)
                  (shipped o4)))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o3))
            (and
              (shipped o1)
              (or
                (autstate_1_4)
                (and
                  (autstate_1_2)
                  (shipped o3)))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o3))
            (and
              (autstate_1_10)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_5)
                (and
                  (autstate_1_4)
                  (shipped o4))
                (and
                  (shipped o3)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (shipped o2))
            (and
              (shipped o1)
              (or
                (autstate_1_6)
                (and
                  (autstate_1_2)
                  (shipped o2)))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o2))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_7)
                (and
                  (autstate_1_6)
                  (shipped o4))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_3)
                    (and
                      (autstate_1_2)
                      (shipped o4)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (shipped o3))
            (and
              (autstate_1_12)
              (shipped o2))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o3))
            (and
              (shipped o1)
              (or
                (autstate_1_8)
                (and
                  (autstate_1_6)
                  (shipped o3))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_4)
                    (and
                      (autstate_1_2)
                      (shipped o3)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (shipped o4))
            (and
              (autstate_1_15)
              (shipped o3))
            (and
              (autstate_1_14)
              (shipped o3)
              (shipped o4))
            (and
              (autstate_1_13)
              (shipped o2))
            (and
              (autstate_1_12)
              (shipped o2)
              (shipped o4))
            (and
              (autstate_1_11)
              (shipped o2)
              (shipped o3))
            (and
              (autstate_1_10)
              (shipped o2)
              (shipped o3)
              (shipped o4))
            (and
              (shipped o1)
              (or
                (autstate_1_9)
                (and
                  (autstate_1_8)
                  (shipped o4))
                (and
                  (autstate_1_7)
                  (shipped o3))
                (and
                  (autstate_1_6)
                  (shipped o3)
                  (shipped o4))
                (and
                  (shipped o2)
                  (or
                    (autstate_1_5)
                    (and
                      (autstate_1_4)
                      (shipped o4))
                    (and
                      (shipped o3)
                      (or
                        (autstate_1_3)
                        (and
                          (autstate_1_2)
                          (shipped o4)))))))))
          (autstate_1_1))
        (not 
          (stacks-avail ?x0))
      )
    )
)