(define (domain dom)
  (:types
    count - no_type
    product - no_type
    order - no_type
  )

  (:predicates
    (includes ?x0 - order ?x1 - product)
    (waiting ?x0 - order)
    (started ?x0 - order)
    (shipped ?x0 - order)
    (made ?x0 - product)
    (machine-available)
    (machine-configured ?x0 - product)
    (stacks-avail ?x0 - count)
    (next-count ?x0 - count ?x1 - count)
    (autstate_1_2)
    (autstate_1_1)
  )
  (:action setup-machine
    :parameters (?x0 - product ?x1 - count)
    :precondition 
      (and
        (machine-available)
        (not 
          (made ?x0))
        (stacks-avail ?x1))
    :effect
      (and
        (machine-configured ?x0)
        (when
          (and
            (autstate_1_2)
            (started o4))
          (autstate_1_1))
        (not 
          (machine-available))
        (when
          (or
            (not 
              (autstate_1_2))
            (not 
              (started o4)))
          (not 
            (autstate_1_1)))
      )
    )
  (:action make-product
    :parameters (?x0 - product ?x1 - count)
    :precondition 
      (and
        (machine-configured ?x0)
        (forall (?x2 - order)
          (imply
            (includes)
            (started)))

        (stacks-avail ?x1))
    :effect
      (and
        (machine-available)
        (made ?x0)
        (when
          (and
            (autstate_1_2)
            (started o4))
          (autstate_1_1))
        (not 
          (machine-configured ?x0))
        (when
          (or
            (not 
              (autstate_1_2))
            (not 
              (started o4)))
          (not 
            (autstate_1_1)))
      )
    )
  (:action start-order
    :parameters (?x0 - order ?x1 - count ?x2 - count)
    :precondition 
      (and
        (waiting ?x0)
        (stacks-avail ?x1)
        (next-count ?x2 ?x1))
    :effect
      (and
        (started ?x0)
        (stacks-avail ?x2)
        (when
          (and
            (autstate_1_2)
            (or
              (= ?x0 o4)
              (started o4)))
          (autstate_1_1))
        (not 
          (waiting ?x0))
        (not 
          (stacks-avail ?x1))
        (when
          (or
            (not 
              (autstate_1_2))
            (and
              (not 
                (= ?x0 o4))
              (not 
                (started o4))))
          (not 
            (autstate_1_1)))
      )
    )
  (:action ship-order
    :parameters (?x0 - order ?x1 - count ?x2 - count)
    :precondition 
      (and
        (started ?x0)
        (forall (?x3 - product)
          (imply
            (includes)
            (made)))

        (stacks-avail ?x1)
        (next-count ?x1 ?x2))
    :effect
      (and
        (shipped ?x0)
        (stacks-avail ?x2)
        (when
          (and
            (autstate_1_2)
            (started o4)
            (not 
              (= ?x0 o4)))
          (autstate_1_1))
        (not 
          (started ?x0))
        (not 
          (stacks-avail ?x1))
        (when
          (or
            (not 
              (autstate_1_2))
            (not 
              (started o4))
            (= ?x0 o4))
          (not 
            (autstate_1_1)))
      )
    )
  (:action open-new-stack
    :parameters (?x0 - count ?x1 - count)
    :precondition 
      (and
        (stacks-avail ?x0)
        (next-count ?x0 ?x1))
    :effect
      (and
        (stacks-avail ?x1)
        (when
          (and
            (autstate_1_2)
            (started o4))
          (autstate_1_1))
        (not 
          (stacks-avail ?x0))
        (when
          (or
            (not 
              (autstate_1_2))
            (not 
              (started o4)))
          (not 
            (autstate_1_1)))
      )
    )
)