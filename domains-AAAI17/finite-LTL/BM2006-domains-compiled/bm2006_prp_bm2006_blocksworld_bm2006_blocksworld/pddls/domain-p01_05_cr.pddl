(define (domain blocks-world-domain)
  (:types
    default_object - none
  )

  (:predicates
    (on ?x0 ?x1)
    (clear ?x0)
    (block ?x0)
    (autstate_1_2)
    (autstate_1_1)
    (autstate_2_2)
    (autstate_2_1)
    (autstate_3_2)
    (autstate_3_1)
    (autstate_4_2)
    (autstate_4_1)
  )
  (:action puton
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 ?x1)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (on c a)
                (= ?x0 a)
                (= ?x1 b))
              (and
                (on a b)
                (= ?x0 c)
                (= ?x1 a))
              (and
                (on c a)
                (on a b)
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 a)))
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 b))))))
          (autstate_1_1))
        (when
          (and
            (autstate_2_2)
            (or
              (and
                (on b c)
                (= ?x0 a)
                (= ?x1 b))
              (and
                (on a b)
                (= ?x0 b)
                (= ?x1 c))
              (and
                (on b c)
                (on a b)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 c)))
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 b))))))
          (autstate_2_1))
        (when
          (and
            (autstate_3_2)
            (or
              (and
                (on a c)
                (= ?x0 c)
                (= ?x1 b))
              (and
                (on c b)
                (= ?x0 a)
                (= ?x1 c))
              (and
                (on a c)
                (on c b)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 c)))
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 b))))))
          (autstate_3_1))
        (when
          (and
            (autstate_4_2)
            (or
              (and
                (on b a)
                (= ?x0 a)
                (= ?x1 c))
              (and
                (on a c)
                (= ?x0 b)
                (= ?x1 a))
              (and
                (on b a)
                (on a c)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 a)))
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 c))))))
          (autstate_4_1))
        (not 
          (on ?x0 ?x2))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
)