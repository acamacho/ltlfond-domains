(define (domain blocks-world-domain)
  (:types
    default_object - none
  )

  (:predicates
    (on ?x0 ?x1)
    (clear ?x0)
    (block ?x0)
    (autstate_1_2)
    (autstate_1_3)
    (autstate_1_4)
    (autstate_1_5)
    (autstate_1_6)
    (autstate_1_7)
    (autstate_1_8)
    (autstate_1_9)
    (autstate_1_10)
    (autstate_1_11)
    (autstate_1_12)
    (autstate_1_13)
    (autstate_1_14)
    (autstate_1_15)
    (autstate_1_16)
    (autstate_1_17)
    (autstate_1_18)
    (autstate_1_19)
    (autstate_1_20)
    (autstate_1_21)
    (autstate_1_22)
    (autstate_1_23)
    (autstate_1_24)
    (autstate_1_25)
    (autstate_1_26)
    (autstate_1_27)
    (autstate_1_28)
    (autstate_1_29)
    (autstate_1_30)
    (autstate_1_31)
    (autstate_1_32)
    (autstate_1_1)
  )
  (:action puton
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 ?x1)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 a)
                (= ?x1 b))
              (and
                (on a b)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 b))))))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 b)
                (= ?x1 a))
              (and
                (on b a)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 a))))))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 a)
                (= ?x1 c))
              (and
                (on a c)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 c))))))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b))))))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 b)
                (= ?x1 c))
              (and
                (on b c)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 c))))))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a))))))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a))))))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b))))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_17))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 c)
                (= ?x1 a))
              (and
                (on c a)
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 a))))))
          (autstate_1_18))
        (when
          (or
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_19))
        (when
          (or
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b))))))))
          (autstate_1_20))
        (when
          (or
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_21))
        (when
          (or
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))))))
          (autstate_1_22))
        (when
          (or
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b))))))))
          (autstate_1_23))
        (when
          (or
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))))))
          (autstate_1_24))
        (when
          (or
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_25))
        (when
          (or
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))))))
          (autstate_1_26))
        (when
          (or
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_27))
        (when
          (or
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a))))))))
          (autstate_1_28))
        (when
          (or
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_29))
        (when
          (or
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a))))))))
          (autstate_1_30))
        (when
          (or
            (and
              (autstate_1_30)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (or
                        (and
                          (on a c)
                          (not 
                            (= ?x2 c))
                          (= ?x1 b))
                        (and
                          (on a b)
                          (not 
                            (= ?x2 b))
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b))))))))
          (autstate_1_31))
        (when
          (or
            (and
              (autstate_1_30)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (= ?x0 b)
                          (= ?x1 a))
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))))))
          (autstate_1_32))
        (when
          (or
            (and
              (autstate_1_32)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_31)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_30)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_29)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_25)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_17)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (or
                        (and
                          (on a c)
                          (not 
                            (= ?x2 c))
                          (= ?x1 b))
                        (and
                          (on a b)
                          (not 
                            (= ?x2 b))
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (= ?x0 b)
                          (= ?x1 a))
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (or
                            (not 
                              (= ?x0 a))
                            (not 
                              (= ?x2 c)))
                          (or
                            (and
                              (on b a)
                              (= ?x0 a)
                              (= ?x1 b))
                            (and
                              (on a b)
                              (= ?x0 b)
                              (= ?x1 a))))
                        (and
                          (on b a)
                          (on a b)
                          (not 
                            (= ?x2 b))
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_1))
        (not 
          (on ?x0 ?x2))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
)