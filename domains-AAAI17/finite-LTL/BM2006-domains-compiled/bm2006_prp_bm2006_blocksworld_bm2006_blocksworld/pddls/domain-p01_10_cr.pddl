(define (domain blocks-world-domain)
  (:types
    default_object - none
  )

  (:predicates
    (on ?x0 ?x1)
    (clear ?x0)
    (block ?x0)
    (autstate_1_2)
    (autstate_1_3)
    (autstate_1_4)
    (autstate_1_5)
    (autstate_1_6)
    (autstate_1_7)
    (autstate_1_8)
    (autstate_1_9)
    (autstate_1_10)
    (autstate_1_11)
    (autstate_1_12)
    (autstate_1_13)
    (autstate_1_14)
    (autstate_1_15)
    (autstate_1_16)
    (autstate_1_17)
    (autstate_1_18)
    (autstate_1_19)
    (autstate_1_20)
    (autstate_1_21)
    (autstate_1_22)
    (autstate_1_23)
    (autstate_1_24)
    (autstate_1_25)
    (autstate_1_26)
    (autstate_1_27)
    (autstate_1_28)
    (autstate_1_29)
    (autstate_1_30)
    (autstate_1_31)
    (autstate_1_32)
    (autstate_1_33)
    (autstate_1_34)
    (autstate_1_35)
    (autstate_1_36)
    (autstate_1_37)
    (autstate_1_38)
    (autstate_1_39)
    (autstate_1_40)
    (autstate_1_41)
    (autstate_1_42)
    (autstate_1_43)
    (autstate_1_44)
    (autstate_1_45)
    (autstate_1_46)
    (autstate_1_47)
    (autstate_1_48)
    (autstate_1_49)
    (autstate_1_50)
    (autstate_1_51)
    (autstate_1_52)
    (autstate_1_53)
    (autstate_1_54)
    (autstate_1_55)
    (autstate_1_56)
    (autstate_1_57)
    (autstate_1_58)
    (autstate_1_59)
    (autstate_1_60)
    (autstate_1_61)
    (autstate_1_62)
    (autstate_1_63)
    (autstate_1_64)
    (autstate_1_1)
  )
  (:action puton
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 ?x1)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 c)
                (= ?x1 b))
              (and
                (on c b)
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 b))))))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 a)
                (= ?x1 b))
              (and
                (on a b)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 b))))))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 b)
                (= ?x1 a))
              (and
                (on b a)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 a))))))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_9))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 a)
                (= ?x1 c))
              (and
                (on a c)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 c))))))
          (autstate_1_10))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_11))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b))))))))
          (autstate_1_12))
        (when
          (or
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_13))
        (when
          (or
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))))))
          (autstate_1_14))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_15))
        (when
          (or
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_16))
        (when
          (or
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_17))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 b)
                (= ?x1 c))
              (and
                (on b c)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 c))))))
          (autstate_1_18))
        (when
          (or
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_19))
        (when
          (or
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_20))
        (when
          (or
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_21))
        (when
          (or
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a))))))))
          (autstate_1_22))
        (when
          (or
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_23))
        (when
          (or
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_24))
        (when
          (or
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_25))
        (when
          (or
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a))))))))
          (autstate_1_26))
        (when
          (or
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_27))
        (when
          (or
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b))))))))
          (autstate_1_28))
        (when
          (or
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_29))
        (when
          (or
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))))))
          (autstate_1_30))
        (when
          (or
            (and
              (autstate_1_30)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_31))
        (when
          (or
            (and
              (autstate_1_30)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_32))
        (when
          (or
            (and
              (autstate_1_32)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_31)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_30)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_29)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_25)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_17)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 a)))
                          (= ?x1 b)
                          (or
                            (and
                              (on a b)
                              (= ?x0 c))
                            (and
                              (on c b)
                              (= ?x0 a))))
                        (and
                          (on a b)
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_33))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 c)
                (= ?x1 a))
              (and
                (on c a)
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 a))))))
          (autstate_1_34))
        (when
          (or
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b))))))))
          (autstate_1_35))
        (when
          (or
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_36))
        (when
          (or
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_37))
        (when
          (or
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b))))))))
          (autstate_1_38))
        (when
          (or
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_39))
        (when
          (or
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_40))
        (when
          (or
            (and
              (autstate_1_40)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_41))
        (when
          (or
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))))))
          (autstate_1_42))
        (when
          (or
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_43))
        (when
          (or
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b))))))))
          (autstate_1_44))
        (when
          (or
            (and
              (autstate_1_44)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_45))
        (when
          (or
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))))))
          (autstate_1_46))
        (when
          (or
            (and
              (autstate_1_46)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_47))
        (when
          (or
            (and
              (autstate_1_46)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_48))
        (when
          (or
            (and
              (autstate_1_48)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_47)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_46)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_45)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_41)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_17)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 a)))
                          (= ?x1 b)
                          (or
                            (and
                              (on a b)
                              (= ?x0 c))
                            (and
                              (on c b)
                              (= ?x0 a))))
                        (and
                          (on a b)
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_49))
        (when
          (or
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))))))
          (autstate_1_50))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_51))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_52))
        (when
          (or
            (and
              (autstate_1_52)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_53))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a))))))))
          (autstate_1_54))
        (when
          (or
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_55))
        (when
          (or
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_56))
        (when
          (or
            (and
              (autstate_1_56)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_55)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_53)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_41)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_25)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 a)))
                          (= ?x1 b)
                          (or
                            (and
                              (on a b)
                              (= ?x0 c))
                            (and
                              (on c b)
                              (= ?x0 a))))
                        (and
                          (on a b)
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_57))
        (when
          (or
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a))))))))
          (autstate_1_58))
        (when
          (or
            (and
              (autstate_1_58)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_59))
        (when
          (or
            (and
              (autstate_1_58)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (or
                        (and
                          (on a c)
                          (not 
                            (= ?x2 c))
                          (= ?x1 b))
                        (and
                          (on a b)
                          (not 
                            (= ?x2 b))
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b))))))))
          (autstate_1_60))
        (when
          (or
            (and
              (autstate_1_60)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_59)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_58)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_53)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_45)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_29)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (or
                        (and
                          (on a c)
                          (not 
                            (= ?x2 c))
                          (= ?x1 b))
                        (and
                          (on a b)
                          (not 
                            (= ?x2 b))
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (or
                            (not 
                              (= ?x0 a))
                            (not 
                              (= ?x2 c)))
                          (= ?x1 b)
                          (or
                            (and
                              (on a b)
                              (= ?x0 c))
                            (and
                              (on c b)
                              (= ?x0 a))))
                        (and
                          (on a b)
                          (on c b)
                          (not 
                            (= ?x2 b))
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_61))
        (when
          (or
            (and
              (autstate_1_58)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_46)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_30)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (= ?x0 b)
                          (= ?x1 a))
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))))))
          (autstate_1_62))
        (when
          (or
            (and
              (autstate_1_62)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_59)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_58)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_55)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_54)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_47)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_46)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_31)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_30)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (= ?x0 b)
                          (= ?x1 a))
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (or
                            (not 
                              (= ?x0 a))
                            (not 
                              (= ?x2 c)))
                          (or
                            (and
                              (on b a)
                              (= ?x0 c)
                              (= ?x1 b))
                            (and
                              (on c b)
                              (= ?x0 b)
                              (= ?x1 a))))
                        (and
                          (on b a)
                          (on c b)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (on c b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_63))
        (when
          (or
            (and
              (autstate_1_62)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_60)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_58)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_56)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_54)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_48)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_46)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_32)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_30)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (or
                        (and
                          (on a c)
                          (not 
                            (= ?x2 c))
                          (= ?x1 b))
                        (and
                          (on a b)
                          (not 
                            (= ?x2 b))
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (= ?x0 b)
                          (= ?x1 a))
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (or
                            (not 
                              (= ?x0 a))
                            (not 
                              (= ?x2 c)))
                          (or
                            (and
                              (on b a)
                              (= ?x0 a)
                              (= ?x1 b))
                            (and
                              (on a b)
                              (= ?x0 b)
                              (= ?x1 a))))
                        (and
                          (on b a)
                          (on a b)
                          (not 
                            (= ?x2 b))
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b)))))))
          (autstate_1_64))
        (when
          (or
            (and
              (autstate_1_64)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_63)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_62)
              (or
                (and
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_61)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_60)
              (or
                (and
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_59)
              (or
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_58)
              (or
                (and
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_57)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_56)
              (or
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_55)
              (or
                (and
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_54)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_53)
              (or
                (and
                  (on a c)
                  (= ?x0 b)
                  (= ?x1 a))
                (and
                  (on b a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_52)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_51)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_50)
              (or
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_49)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_48)
              (or
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 b))
                (and
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_47)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_46)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_45)
              (or
                (and
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_44)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_43)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_42)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_41)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_40)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_39)
              (or
                (and
                  (on b c)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_38)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_37)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_36)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_35)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_34)
              (or
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 a)))
                          (= ?x1 b)
                          (or
                            (and
                              (on a b)
                              (= ?x0 c))
                            (and
                              (on c b)
                              (= ?x0 a))))
                        (and
                          (on a b)
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 a))
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_33)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_32)
              (or
                (and
                  (= ?x0 c)
                  (or
                    (and
                      (on c a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 b))
                    (and
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 a))))
                (and
                  (on c a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (and
                      (not 
                        (= ?x2 a))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_31)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 b))
                (and
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_30)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 b)
                  (or
                    (and
                      (on a b)
                      (= ?x0 c))
                    (and
                      (on c b)
                      (= ?x0 a))))
                (and
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_29)
              (or
                (and
                  (= ?x1 a)
                  (or
                    (and
                      (on c a)
                      (= ?x0 b))
                    (and
                      (on b a)
                      (= ?x0 c))))
                (and
                  (on c a)
                  (on b a)
                  (or
                    (not 
                      (= ?x2 a))
                    (and
                      (not 
                        (= ?x0 c))
                      (not 
                        (= ?x0 b)))))))
            (and
              (autstate_1_28)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_27)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_26)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b a)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 a)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 a))))
                (and
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_25)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_24)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_23)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (or
                    (and
                      (on a c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 b))
                    (and
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_22)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_21)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (= ?x0 b)
                      (= ?x1 a))
                    (and
                      (on b a)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_20)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_19)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_18)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on a c)
                      (or
                        (not 
                          (= ?x0 a))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 a)))
                          (= ?x1 b)
                          (or
                            (and
                              (on a b)
                              (= ?x0 c))
                            (and
                              (on c b)
                              (= ?x0 a))))
                        (and
                          (on a b)
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 b))
                      (= ?x0 a)
                      (= ?x1 c))))
                (and
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_17)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_16)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 c)
                      (= ?x1 b))
                    (and
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_15)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (= ?x1 b))
                    (and
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_14)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (= ?x1 b)
                      (or
                        (and
                          (on a b)
                          (= ?x0 c))
                        (and
                          (on c b)
                          (= ?x0 a))))
                    (and
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_13)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (or
                    (and
                      (on b c)
                      (not 
                        (= ?x2 c))
                      (= ?x1 a))
                    (and
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 a)))))))
            (and
              (autstate_1_12)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on c b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_11)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 b))
                        (and
                          (on a b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_10)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on b a)
                          (or
                            (not 
                              (= ?x0 b))
                            (not 
                              (= ?x2 a)))
                          (= ?x1 b)
                          (or
                            (and
                              (on a b)
                              (= ?x0 c))
                            (and
                              (on c b)
                              (= ?x0 a))))
                        (and
                          (on a b)
                          (on c b)
                          (= ?x0 b)
                          (= ?x1 a))))
                    (and
                      (on b a)
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_9)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_8)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (= ?x0 c)
                          (= ?x1 b))
                        (and
                          (on c b)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (= ?x0 a)
                      (or
                        (and
                          (on a c)
                          (not 
                            (= ?x2 c))
                          (= ?x1 b))
                        (and
                          (on a b)
                          (not 
                            (= ?x2 b))
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on a b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (and
                      (not 
                        (= ?x2 c))
                      (not 
                        (= ?x2 b)))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (or
                            (not 
                              (= ?x0 a))
                            (not 
                              (= ?x2 c)))
                          (= ?x1 b)
                          (or
                            (and
                              (on a b)
                              (= ?x0 c))
                            (and
                              (on c b)
                              (= ?x0 a))))
                        (and
                          (on a b)
                          (on c b)
                          (not 
                            (= ?x2 b))
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on a b)
                      (on c b)
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (= ?x0 b)
                          (= ?x1 a))
                        (and
                          (on b a)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (or
                            (not 
                              (= ?x0 a))
                            (not 
                              (= ?x2 c)))
                          (or
                            (and
                              (on b a)
                              (= ?x0 c)
                              (= ?x1 b))
                            (and
                              (on c b)
                              (= ?x0 b)
                              (= ?x1 a))))
                        (and
                          (on b a)
                          (on c b)
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (on c b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (or
                            (not 
                              (= ?x0 a))
                            (not 
                              (= ?x2 c)))
                          (or
                            (and
                              (on b a)
                              (= ?x0 a)
                              (= ?x1 b))
                            (and
                              (on a b)
                              (= ?x0 b)
                              (= ?x1 a))))
                        (and
                          (on b a)
                          (on a b)
                          (not 
                            (= ?x2 b))
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (on a b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 b))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (and
                      (on b c)
                      (or
                        (not 
                          (= ?x0 b))
                        (not 
                          (= ?x2 c)))
                      (or
                        (and
                          (on a c)
                          (or
                            (not 
                              (= ?x0 a))
                            (not 
                              (= ?x2 c)))
                          (or
                            (and
                              (on b a)
                              (or
                                (not 
                                  (= ?x0 b))
                                (not 
                                  (= ?x2 a)))
                              (= ?x1 b)
                              (or
                                (and
                                  (on a b)
                                  (= ?x0 c))
                                (and
                                  (on c b)
                                  (= ?x0 a))))
                            (and
                              (on a b)
                              (on c b)
                              (= ?x0 b)
                              (= ?x1 a))))
                        (and
                          (on b a)
                          (on a b)
                          (on c b)
                          (not 
                            (= ?x2 b))
                          (= ?x0 a)
                          (= ?x1 c))))
                    (and
                      (on a c)
                      (on b a)
                      (on a b)
                      (on c b)
                      (not 
                        (= ?x2 a))
                      (= ?x0 b)
                      (= ?x1 c))))
                (and
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (not 
                    (= ?x2 b))
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (on b a)
                  (on a b)
                  (on c b)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 b))
                    (and
                      (not 
                        (= ?x0 a))
                      (not 
                        (= ?x0 c))))))))
          (autstate_1_1))
        (not 
          (on ?x0 ?x2))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
)