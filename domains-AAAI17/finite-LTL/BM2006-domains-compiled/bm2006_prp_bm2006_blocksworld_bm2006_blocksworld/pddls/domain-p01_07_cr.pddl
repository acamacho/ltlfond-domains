(define (domain blocks-world-domain)
  (:types
    default_object - none
  )

  (:predicates
    (on ?x0 ?x1)
    (clear ?x0)
    (block ?x0)
    (autstate_1_2)
    (autstate_1_3)
    (autstate_1_4)
    (autstate_1_5)
    (autstate_1_6)
    (autstate_1_7)
    (autstate_1_8)
    (autstate_1_1)
  )
  (:action puton
    :parameters (?x0 ?x1 ?x2)
    :precondition 
      (and
        (on ?x0 ?x2)
        (clear ?x0)
        (clear ?x1)
        (not 
          (= ?x1 ?x2))
        (not 
          (= ?x0 ?x2))
        (not 
          (= ?x0 ?x1))
        (not 
          (= ?x0 table)))
    :effect
      (and
        (on ?x0 ?x1)
        (when
          (not 
            (= ?x2 table))
          (clear ?x2))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 a)
                (= ?x1 c))
              (and
                (on a c)
                (or
                  (not 
                    (= ?x0 a))
                  (not 
                    (= ?x2 c))))))
          (autstate_1_3))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 b)
                (= ?x1 c))
              (and
                (on b c)
                (or
                  (not 
                    (= ?x0 b))
                  (not 
                    (= ?x2 c))))))
          (autstate_1_4))
        (when
          (or
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a))))))))
          (autstate_1_5))
        (when
          (and
            (autstate_1_2)
            (or
              (and
                (= ?x0 c)
                (= ?x1 a))
              (and
                (on c a)
                (or
                  (not 
                    (= ?x0 c))
                  (not 
                    (= ?x2 a))))))
          (autstate_1_6))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c)))))))
          (autstate_1_7))
        (when
          (or
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c)))))))
          (autstate_1_8))
        (when
          (or
            (and
              (autstate_1_8)
              (or
                (and
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_7)
              (or
                (and
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_6)
              (or
                (and
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a)))))))
            (and
              (autstate_1_5)
              (or
                (and
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a))))))
            (and
              (autstate_1_4)
              (or
                (and
                  (on c a)
                  (= ?x0 a)
                  (= ?x1 c))
                (and
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 a))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_3)
              (or
                (and
                  (on c a)
                  (= ?x0 b)
                  (= ?x1 c))
                (and
                  (on b c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x0 b))
                    (not 
                      (= ?x2 c))))))
            (and
              (autstate_1_2)
              (or
                (and
                  (on c a)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (= ?x1 c)
                  (or
                    (and
                      (on b c)
                      (= ?x0 a))
                    (and
                      (on a c)
                      (= ?x0 b))))
                (and
                  (on b c)
                  (on a c)
                  (= ?x0 c)
                  (= ?x1 a))
                (and
                  (on c a)
                  (on b c)
                  (on a c)
                  (or
                    (not 
                      (= ?x0 c))
                    (not 
                      (= ?x2 a)))
                  (or
                    (not 
                      (= ?x2 c))
                    (and
                      (not 
                        (= ?x0 b))
                      (not 
                        (= ?x0 a))))))))
          (autstate_1_1))
        (not 
          (on ?x0 ?x2))
        (when
          (not 
            (= ?x1 table))
          (not 
            (clear ?x1)))
      )
    )
)