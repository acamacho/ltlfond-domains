(define (domain waldo)
	(:requirements :strips :typing :equality)
	(:constants r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 r11 r12 r13 r14 r15 r16 r17 r18 r19 r20 - room)
	(:types room qstate)
	(:predicates (seen) (search_again) (in ?r - room) (searched_in ?r - room) (oldautstate ?q - qstate) (newautstate ?q - qstate) (world_mode) (trans_mode) (dummy_goal))
	(:action move-right-from-r1
		:precondition (and (in r1) (world_mode) )
		:effect (and (in r2) (trans_mode) (not (search_again)) (not (in r1)) (not (world_mode)))
	)
	(:action move-left-from-r1
		:precondition (and (in r1) (world_mode) (not (searched_in r20)))
		:effect (and (in r20) 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r20) (trans_mode) (not (search_again)) (not (in r1)) (not (world_mode)))
	)
	(:action move-right-from-r2
		:precondition (and (in r2) (world_mode) )
		:effect (and (in r3) (trans_mode) (not (search_again)) (not (in r2)) (not (world_mode)))
	)
	(:action move-left-from-r2
		:precondition (and (in r2) (world_mode) )
		:effect (and (in r1) (trans_mode) (not (search_again)) (not (in r2)) (not (world_mode)))
	)
	(:action move-right-from-r3
		:precondition (and (in r3) (world_mode) )
		:effect (and (in r4) (trans_mode) (not (search_again)) (not (in r3)) (not (world_mode)))
	)
	(:action move-left-from-r3
		:precondition (and (in r3) (world_mode) )
		:effect (and (in r2) (trans_mode) (not (search_again)) (not (in r3)) (not (world_mode)))
	)
	(:action move-right-from-r4
		:precondition (and (in r4) (world_mode) )
		:effect (and (in r5) (trans_mode) (not (search_again)) (not (in r4)) (not (world_mode)))
	)
	(:action move-left-from-r4
		:precondition (and (in r4) (world_mode) )
		:effect (and (in r3) (trans_mode) (not (search_again)) (not (in r4)) (not (world_mode)))
	)
	(:action move-right-from-r5
		:precondition (and (in r5) (world_mode) )
		:effect (and (in r6) (trans_mode) (not (search_again)) (not (in r5)) (not (world_mode)))
	)
	(:action move-left-from-r5
		:precondition (and (in r5) (world_mode) )
		:effect (and (in r4) (trans_mode) (not (search_again)) (not (in r5)) (not (world_mode)))
	)
	(:action move-right-from-r6
		:precondition (and (in r6) (world_mode) )
		:effect (and (in r7) (trans_mode) (not (search_again)) (not (in r6)) (not (world_mode)))
	)
	(:action move-left-from-r6
		:precondition (and (in r6) (world_mode) )
		:effect (and (in r5) (trans_mode) (not (search_again)) (not (in r6)) (not (world_mode)))
	)
	(:action move-right-from-r7
		:precondition (and (in r7) (world_mode) )
		:effect (and (in r8) (trans_mode) (not (search_again)) (not (in r7)) (not (world_mode)))
	)
	(:action move-left-from-r7
		:precondition (and (in r7) (world_mode) )
		:effect (and (in r6) (trans_mode) (not (search_again)) (not (in r7)) (not (world_mode)))
	)
	(:action move-right-from-r8
		:precondition (and (in r8) (world_mode) )
		:effect (and (in r9) (trans_mode) (not (search_again)) (not (in r8)) (not (world_mode)))
	)
	(:action move-left-from-r8
		:precondition (and (in r8) (world_mode) )
		:effect (and (in r7) (trans_mode) (not (search_again)) (not (in r8)) (not (world_mode)))
	)
	(:action move-right-from-r9
		:precondition (and (in r9) (world_mode) )
		:effect (and 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r10) (in r10) (trans_mode) (not (search_again)) (not (in r9)) (not (world_mode)))
	)
	(:action move-left-from-r9
		:precondition (and (in r9) (world_mode) )
		:effect (and (in r8) (trans_mode) (not (search_again)) (not (in r9)) (not (world_mode)))
	)
	(:action move-right-from-r10
		:precondition (and (in r10) (world_mode) )
		:effect (and (in r11) (trans_mode) (not (search_again)) (not (in r10)) (not (world_mode)))
	)
	(:action move-left-from-r10
		:precondition (and (in r10) (world_mode) )
		:effect (and (in r9) (trans_mode) (not (search_again)) (not (in r10)) (not (world_mode)))
	)
	(:action move-right-from-r11
		:precondition (and (in r11) (world_mode) )
		:effect (and (in r12) (trans_mode) (not (search_again)) (not (in r11)) (not (world_mode)))
	)
	(:action move-left-from-r11
		:precondition (and (in r11) (world_mode) )
		:effect (and (in r10) 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r10) (trans_mode) (not (search_again)) (not (in r11)) (not (world_mode)))
	)
	(:action move-right-from-r12
		:precondition (and (in r12) (world_mode) )
		:effect (and (in r13) (trans_mode) (not (search_again)) (not (in r12)) (not (world_mode)))
	)
	(:action move-left-from-r12
		:precondition (and (in r12) (world_mode) )
		:effect (and (in r11) (trans_mode) (not (search_again)) (not (in r12)) (not (world_mode)))
	)
	(:action move-right-from-r13
		:precondition (and (in r13) (world_mode) )
		:effect (and (in r14) (trans_mode) (not (search_again)) (not (in r13)) (not (world_mode)))
	)
	(:action move-left-from-r13
		:precondition (and (in r13) (world_mode) )
		:effect (and (in r12) (trans_mode) (not (search_again)) (not (in r13)) (not (world_mode)))
	)
	(:action move-right-from-r14
		:precondition (and (in r14) (world_mode) )
		:effect (and (in r15) (trans_mode) (not (search_again)) (not (in r14)) (not (world_mode)))
	)
	(:action move-left-from-r14
		:precondition (and (in r14) (world_mode) )
		:effect (and (in r13) (trans_mode) (not (search_again)) (not (in r14)) (not (world_mode)))
	)
	(:action move-right-from-r15
		:precondition (and (in r15) (world_mode) )
		:effect (and (in r16) (trans_mode) (not (search_again)) (not (in r15)) (not (world_mode)))
	)
	(:action move-left-from-r15
		:precondition (and (in r15) (world_mode) )
		:effect (and (in r14) (trans_mode) (not (search_again)) (not (in r15)) (not (world_mode)))
	)
	(:action move-right-from-r16
		:precondition (and (in r16) (world_mode) )
		:effect (and (in r17) (trans_mode) (not (search_again)) (not (in r16)) (not (world_mode)))
	)
	(:action move-left-from-r16
		:precondition (and (in r16) (world_mode) )
		:effect (and (in r15) (trans_mode) (not (search_again)) (not (in r16)) (not (world_mode)))
	)
	(:action move-right-from-r17
		:precondition (and (in r17) (world_mode) )
		:effect (and (in r18) (trans_mode) (not (search_again)) (not (in r17)) (not (world_mode)))
	)
	(:action move-left-from-r17
		:precondition (and (in r17) (world_mode) )
		:effect (and (in r16) (trans_mode) (not (search_again)) (not (in r17)) (not (world_mode)))
	)
	(:action move-right-from-r18
		:precondition (and (in r18) (world_mode) )
		:effect (and (in r19) (trans_mode) (not (search_again)) (not (in r18)) (not (world_mode)))
	)
	(:action move-left-from-r18
		:precondition (and (in r18) (world_mode) )
		:effect (and (in r17) (trans_mode) (not (search_again)) (not (in r18)) (not (world_mode)))
	)
	(:action move-right-from-r19
		:precondition (and (in r19) (world_mode) )
		:effect (and (in r20) 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r20) (trans_mode) (not (search_again)) (not (in r19)) (not (world_mode)))
	)
	(:action move-left-from-r19
		:precondition (and (in r19) (world_mode) )
		:effect (and (in r18) (trans_mode) (not (search_again)) (not (in r19)) (not (world_mode)))
	)
	(:action move-right-from-r20
		:precondition (and (in r20) (world_mode) )
		:effect (and (in r1) (trans_mode) (not (search_again)) (not (in r20)) (not (world_mode)))
	)
	(:action move-left-from-r20
		:precondition (and (in r20) (world_mode) )
		:effect (and (in r19) (trans_mode) (not (search_again)) (not (in r20)) (not (world_mode)))
	)
	(:action stay
		:precondition (and (f_ok) (world_mode) )
		:effect (and (trans_mode) (not (world_mode)))
	)
	(:action searching_again
		:precondition (and (searched_in r10) (searched_in r20) (world_mode) (not (seen)))
		:effect (and (search_again) (trans_mode) (not (searched_in r10)) (not (searched_in r20)) (not (world_mode)))
	)
	(:action trans_aut_q0_t0
		:precondition (and (trans_mode) (oldautstate q0) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q0_t1
		:precondition (and (trans_mode) (oldautstate q0) (search_again) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q0_t2
		:precondition (and (trans_mode) (oldautstate q0) )
		:effect (newautstate q2)
	)
	(:action trans_aut_q0_t3
		:precondition (and (trans_mode) (oldautstate q0) (seen) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q0_t4
		:precondition (and (trans_mode) (oldautstate q0) (search_again) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q0_t5
		:precondition (and (trans_mode) (oldautstate q0) (seen) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q1_t6
		:precondition (and (trans_mode) (oldautstate q1) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q1_t7
		:precondition (and (trans_mode) (oldautstate q1) (search_again) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q1_t8
		:precondition (and (trans_mode) (oldautstate q1) (search_again) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q2_t9
		:precondition (and (trans_mode) (oldautstate q2) )
		:effect (newautstate q2)
	)
	(:action trans_aut_q2_t10
		:precondition (and (trans_mode) (oldautstate q2) (seen) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q2_t11
		:precondition (and (trans_mode) (oldautstate q2) (seen) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action start_sync
		:precondition (and (trans_mode) )
		:effect (and (sync_mode) (not (trans_mode)) (not (oldautstate q0)) (not (oldautstate q1)) (not (oldautstate q2)) (not (oldautstate q3)))
	)
	(:action sync_q0_pos
		:precondition (and (sync_mode) (newautstate q0) )
		:effect (and (oldautstate q0) (not (newautstate q0)))
	)
	(:action sync_q1_pos
		:precondition (and (sync_mode) (newautstate q1) (not (newautstate q0)))
		:effect (and (oldautstate q1) (not (newautstate q1)))
	)
	(:action sync_q2_pos
		:precondition (and (sync_mode) (newautstate q2) (not (newautstate q0)) (not (newautstate q1)))
		:effect (and (oldautstate q2) (not (newautstate q2)))
	)
	(:action sync_q3_pos
		:precondition (and (sync_mode) (newautstate q3) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)))
		:effect (and (oldautstate q3) (not (newautstate q3)))
	)
	(:action continue
		:precondition (and (sync_mode) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)) (not (newautstate q3)))
		:effect (and (world_mode) (not (sync_mode)) (not (dummy_goal)))
	)
)
