(define (domain packages)
	(:requirements :strips :typing :equality)
	(:constants p1 - pkg l1 desk - loc)
	(:types pkg loc qstate)
	(:predicates (in_store ?p - pkg) (want ?p - pkg) (active_request) (pkg_served) (pkg_requested) (pkg_arrived) (pkg_stored) (holding ?p - pkg) (pkg_at ?l - loc) (robot_at ?l - loc) (oldautstate ?q - qstate) (newautstate ?q - qstate) (world_mode) (trans_mode) (dummy_goal))
	(:action serve-p1
		:precondition (and (in_store p1) (want p1) (holding p1) (robot_at desk) (world_mode) (not (pkg_served)))
		:effect (and (pkg_served) (trans_mode) (not (pkg_requested)) (not (active_request)) (not (in_store p1)) (not (want p1)) (not (holding p1)) (not (world_mode)))
	)
	(:action pickup-p1
		:precondition (and (in_store p1) (want p1) (robot_at l1) (world_mode) (not (pkg_served)))
		:effect (and (holding p1) (trans_mode) (not (pkg_at l1)) (not (world_mode)))
	)
	(:action putdown-p1
		:precondition (and (pkg_arrived) (holding p1) (robot_at l1) (world_mode) )
		:effect (and (in_store p1) (pkg_at l1) (pkg_stored) (trans_mode) (not (holding p1)) (not (pkg_arrived)) (not (active_request)) (not (world_mode)))
	)
	(:action restock-p1
		:precondition (and (want p1) (world_mode) (not (pkg_served)) (not (in_store p1)))
		:effect (and (in_store p1) (trans_mode) (not (world_mode)))
	)
	(:action movedesk_l1
		:precondition (and (robot_at desk) (world_mode) )
		:effect (and (robot_at l1) (trans_mode) (not (robot_at desk)) (not (world_mode)))
	)
	(:action movel1_desk
		:precondition (and (robot_at l1) (world_mode) )
		:effect (and (robot_at desk) (trans_mode) (not (robot_at l1)) (not (world_mode)))
	)
	(:action request
		:precondition (and (robot_at desk) (world_mode) (not (active_request)))
		:effect (and (active_request) 
			(oneof
			 (and (pkg_requested) (want p1))
			 (and (pkg_arrived) (holding p1))
			) (trans_mode) (not (pkg_served)) (not (pkg_stored)) (not (world_mode)))
	)
	(:action trans_aut_q0_t0
		:precondition (and (trans_mode) (oldautstate q0) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q0_t1
		:precondition (and (trans_mode) (oldautstate q0) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q0_t2
		:precondition (and (trans_mode) (oldautstate q0) (pkg_served) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q0_t3
		:precondition (and (trans_mode) (oldautstate q0) )
		:effect (newautstate q2)
	)
	(:action trans_aut_q0_t4
		:precondition (and (trans_mode) (oldautstate q0) (pkg_stored) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q0_t5
		:precondition (and (trans_mode) (oldautstate q0) (active_request) (pkg_served) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q0_t6
		:precondition (and (trans_mode) (oldautstate q0) (active_request) (pkg_stored) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q1_t7
		:precondition (and (trans_mode) (oldautstate q1) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q1_t8
		:precondition (and (trans_mode) (oldautstate q1) (pkg_served) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q1_t9
		:precondition (and (trans_mode) (oldautstate q1) (active_request) (pkg_served) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q2_t10
		:precondition (and (trans_mode) (oldautstate q2) )
		:effect (newautstate q2)
	)
	(:action trans_aut_q2_t11
		:precondition (and (trans_mode) (oldautstate q2) (pkg_stored) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q2_t12
		:precondition (and (trans_mode) (oldautstate q2) (active_request) (pkg_stored) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action start_sync
		:precondition (and (trans_mode) )
		:effect (and (sync_mode) (not (trans_mode)) (not (oldautstate q0)) (not (oldautstate q1)) (not (oldautstate q2)) (not (oldautstate q3)))
	)
	(:action sync_q0_pos
		:precondition (and (sync_mode) (newautstate q0) )
		:effect (and (oldautstate q0) (not (newautstate q0)))
	)
	(:action sync_q1_pos
		:precondition (and (sync_mode) (newautstate q1) (not (newautstate q0)))
		:effect (and (oldautstate q1) (not (newautstate q1)))
	)
	(:action sync_q2_pos
		:precondition (and (sync_mode) (newautstate q2) (not (newautstate q0)) (not (newautstate q1)))
		:effect (and (oldautstate q2) (not (newautstate q2)))
	)
	(:action sync_q3_pos
		:precondition (and (sync_mode) (newautstate q3) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)))
		:effect (and (oldautstate q3) (not (newautstate q3)))
	)
	(:action continue
		:precondition (and (sync_mode) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)) (not (newautstate q3)))
		:effect (and (world_mode) (not (sync_mode)) (not (dummy_goal)))
	)
)
