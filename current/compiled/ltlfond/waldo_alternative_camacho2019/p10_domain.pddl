(define (domain waldo)
	(:requirements :strips :typing :equality)
	(:constants r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 - room)
	(:types room qstate)
	(:predicates (seen) (search_again) (in ?r - room) (searched_in ?r - room) (oldautstate ?q - qstate) (newautstate ?q - qstate) (oldcnt_idx0 ?q - qstate) (oldcnt_idx1 ?q - qstate) (oldcnt_idx2 ?q - qstate) (oldcnt_idx3 ?q - qstate) (oldcnt_idx4 ?q - qstate) (oldcnt_idx5 ?q - qstate) (oldcnt_idx6 ?q - qstate) (oldcnt_idx7 ?q - qstate) (newcnt_idx0 ?q - qstate) (newcnt_idx1 ?q - qstate) (newcnt_idx2 ?q - qstate) (newcnt_idx3 ?q - qstate) (newcnt_idx4 ?q - qstate) (newcnt_idx5 ?q - qstate) (newcnt_idx6 ?q - qstate) (newcnt_idx7 ?q - qstate) (newcnt_idx8 ?q - qstate) (world_mode) (trans_mode) (sync_mode) (dummy_goal))
	(:action move-right-from-r1
		:precondition (and (in r1) (world_mode) )
		:effect (and (in r2) (trans_mode) (not (search_again)) (not (in r1)) (not (world_mode)))
	)
	(:action move-left-from-r1
		:precondition (and (in r1) (world_mode) (not (searched_in r10)))
		:effect (and (in r10) 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r10) (trans_mode) (not (search_again)) (not (in r1)) (not (world_mode)))
	)
	(:action move-right-from-r2
		:precondition (and (in r2) (world_mode) )
		:effect (and (in r3) (trans_mode) (not (search_again)) (not (in r2)) (not (world_mode)))
	)
	(:action move-left-from-r2
		:precondition (and (in r2) (world_mode) )
		:effect (and (in r1) (trans_mode) (not (search_again)) (not (in r2)) (not (world_mode)))
	)
	(:action move-right-from-r3
		:precondition (and (in r3) (world_mode) )
		:effect (and (in r4) (trans_mode) (not (search_again)) (not (in r3)) (not (world_mode)))
	)
	(:action move-left-from-r3
		:precondition (and (in r3) (world_mode) )
		:effect (and (in r2) (trans_mode) (not (search_again)) (not (in r3)) (not (world_mode)))
	)
	(:action move-right-from-r4
		:precondition (and (in r4) (world_mode) )
		:effect (and 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r5) (in r5) (trans_mode) (not (search_again)) (not (in r4)) (not (world_mode)))
	)
	(:action move-left-from-r4
		:precondition (and (in r4) (world_mode) )
		:effect (and (in r3) (trans_mode) (not (search_again)) (not (in r4)) (not (world_mode)))
	)
	(:action move-right-from-r5
		:precondition (and (in r5) (world_mode) )
		:effect (and (in r6) (trans_mode) (not (search_again)) (not (in r5)) (not (world_mode)))
	)
	(:action move-left-from-r5
		:precondition (and (in r5) (world_mode) )
		:effect (and (in r4) (trans_mode) (not (search_again)) (not (in r5)) (not (world_mode)))
	)
	(:action move-right-from-r6
		:precondition (and (in r6) (world_mode) )
		:effect (and (in r7) (trans_mode) (not (search_again)) (not (in r6)) (not (world_mode)))
	)
	(:action move-left-from-r6
		:precondition (and (in r6) (world_mode) )
		:effect (and 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r5) (in r5) (trans_mode) (not (search_again)) (not (in r6)) (not (world_mode)))
	)
	(:action move-right-from-r7
		:precondition (and (in r7) (world_mode) )
		:effect (and (in r8) (trans_mode) (not (search_again)) (not (in r7)) (not (world_mode)))
	)
	(:action move-left-from-r7
		:precondition (and (in r7) (world_mode) )
		:effect (and (in r6) (trans_mode) (not (search_again)) (not (in r7)) (not (world_mode)))
	)
	(:action move-right-from-r8
		:precondition (and (in r8) (world_mode) )
		:effect (and (in r9) (trans_mode) (not (search_again)) (not (in r8)) (not (world_mode)))
	)
	(:action move-left-from-r8
		:precondition (and (in r8) (world_mode) )
		:effect (and (in r7) (trans_mode) (not (search_again)) (not (in r8)) (not (world_mode)))
	)
	(:action move-right-from-r9
		:precondition (and (in r9) (world_mode) )
		:effect (and 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r10) (in r10) (trans_mode) (not (search_again)) (not (in r9)) (not (world_mode)))
	)
	(:action move-left-from-r9
		:precondition (and (in r9) (world_mode) )
		:effect (and (in r8) (trans_mode) (not (search_again)) (not (in r9)) (not (world_mode)))
	)
	(:action move-right-from-r10
		:precondition (and (in r10) (world_mode) )
		:effect (and (in r1) (trans_mode) (not (search_again)) (not (in r10)) (not (world_mode)))
	)
	(:action move-left-from-r10
		:precondition (and (in r10) (world_mode) )
		:effect (and (in r9) (trans_mode) (not (search_again)) (not (in r10)) (not (world_mode)))
	)
	(:action stay
		:precondition (and (f_ok) (world_mode) )
		:effect (and (trans_mode) (not (world_mode)))
	)
	(:action searching_again
		:precondition (and (searched_in r5) (searched_in r10) (world_mode) (not (seen)))
		:effect (and (search_again) (trans_mode) (not (searched_in r5)) (not (searched_in r10)) (not (world_mode)))
	)
	(:action trans_aut_q0_idx0
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx0 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx1 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx1
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx1 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx2 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx2
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx2 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx3 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx3
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx3 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx4 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx4
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx4 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx5 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx5
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx5 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx6 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx6
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx6 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx7 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx7
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx7 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx8 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q1_idx0
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx0 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx1 q0))
			) (and (newautstate q1) (newcnt_idx0 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx1
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx1 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx2 q0))
			) (and (newautstate q1) (newcnt_idx1 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx2
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx2 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx3 q0))
			) (and (newautstate q1) (newcnt_idx2 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx3
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx3 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx4 q0))
			) (and (newautstate q1) (newcnt_idx3 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx4
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx4 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx5 q0))
			) (and (newautstate q1) (newcnt_idx4 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx5
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx5 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx6 q0))
			) (and (newautstate q1) (newcnt_idx5 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx6
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx6 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx7 q0))
			) (and (newautstate q1) (newcnt_idx6 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx7
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx7 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx8 q0))
			) (and (newautstate q1) (newcnt_idx7 q1)) (not (oldautstate q1)))
	)
	(:action start_sync
		:precondition (and (trans_mode) (not (oldautstate q0)) (not (oldautstate q1)))
		:effect (and (sync_mode) (not (trans_mode)) (not (oldcnt_idx0 q0)) (not (oldcnt_idx1 q0)) (not (oldcnt_idx2 q0)) (not (oldcnt_idx3 q0)) (not (oldcnt_idx4 q0)) (not (oldcnt_idx5 q0)) (not (oldcnt_idx6 q0)) (not (oldcnt_idx7 q0)) (not (oldcnt_idx0 q1)) (not (oldcnt_idx1 q1)) (not (oldcnt_idx2 q1)) (not (oldcnt_idx3 q1)) (not (oldcnt_idx4 q1)) (not (oldcnt_idx5 q1)) (not (oldcnt_idx6 q1)) (not (oldcnt_idx7 q1)))
	)
	(:action sync_aut_idx0
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx0 ?q) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx0 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
	)
	(:action sync_aut_idx1
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx1 ?q) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx1 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
	)
	(:action sync_aut_idx2
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx2 ?q) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx2 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
	)
	(:action sync_aut_idx3
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx3 ?q) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx3 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
	)
	(:action sync_aut_idx4
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx4 ?q) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx4 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
	)
	(:action sync_aut_idx5
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx5 ?q) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx5 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
	)
	(:action sync_aut_idx6
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx6 ?q) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx6 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
	)
	(:action sync_aut_idx7
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx7 ?q) (not (newcnt_idx8 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx7 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)))
	)
	(:action continue
		:precondition (and (sync_mode) (not (newautstate q0)) (not (newautstate q1)))
		:effect (and 
			(oneof
			 (dummy_goal)
			 (world_mode)
			) (not (sync_mode)))
	)
)
