(define (domain waldo)
	(:requirements :strips :typing :equality)
	(:constants r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 r11 r12 r13 r14 r15 r16 r17 r18 r19 r20 r21 r22 r23 r24 r25 r26 r27 r28 r29 r30 r31 r32 r33 r34 r35 r36 r37 r38 r39 r40 - room)
	(:types room qstate)
	(:predicates (seen) (search_again) (in ?r - room) (searched_in ?r - room) (oldautstate ?q - qstate) (newautstate ?q - qstate) (oldcnt_idx0 ?q - qstate) (oldcnt_idx1 ?q - qstate) (oldcnt_idx2 ?q - qstate) (oldcnt_idx3 ?q - qstate) (oldcnt_idx4 ?q - qstate) (oldcnt_idx5 ?q - qstate) (oldcnt_idx6 ?q - qstate) (oldcnt_idx7 ?q - qstate) (oldcnt_idx8 ?q - qstate) (oldcnt_idx9 ?q - qstate) (oldcnt_idx10 ?q - qstate) (oldcnt_idx11 ?q - qstate) (oldcnt_idx12 ?q - qstate) (oldcnt_idx13 ?q - qstate) (oldcnt_idx14 ?q - qstate) (oldcnt_idx15 ?q - qstate) (oldcnt_idx16 ?q - qstate) (oldcnt_idx17 ?q - qstate) (oldcnt_idx18 ?q - qstate) (oldcnt_idx19 ?q - qstate) (oldcnt_idx20 ?q - qstate) (oldcnt_idx21 ?q - qstate) (oldcnt_idx22 ?q - qstate) (newcnt_idx0 ?q - qstate) (newcnt_idx1 ?q - qstate) (newcnt_idx2 ?q - qstate) (newcnt_idx3 ?q - qstate) (newcnt_idx4 ?q - qstate) (newcnt_idx5 ?q - qstate) (newcnt_idx6 ?q - qstate) (newcnt_idx7 ?q - qstate) (newcnt_idx8 ?q - qstate) (newcnt_idx9 ?q - qstate) (newcnt_idx10 ?q - qstate) (newcnt_idx11 ?q - qstate) (newcnt_idx12 ?q - qstate) (newcnt_idx13 ?q - qstate) (newcnt_idx14 ?q - qstate) (newcnt_idx15 ?q - qstate) (newcnt_idx16 ?q - qstate) (newcnt_idx17 ?q - qstate) (newcnt_idx18 ?q - qstate) (newcnt_idx19 ?q - qstate) (newcnt_idx20 ?q - qstate) (newcnt_idx21 ?q - qstate) (newcnt_idx22 ?q - qstate) (newcnt_idx23 ?q - qstate) (world_mode) (trans_mode) (sync_mode) (dummy_goal))
	(:action move-right-from-r1
		:precondition (and (in r1) (world_mode) )
		:effect (and (in r2) (trans_mode) (not (search_again)) (not (in r1)) (not (world_mode)))
	)
	(:action move-left-from-r1
		:precondition (and (in r1) (world_mode) (not (searched_in r100)))
		:effect (and (in r40) 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r40) (trans_mode) (not (search_again)) (not (in r1)) (not (world_mode)))
	)
	(:action move-right-from-r2
		:precondition (and (in r2) (world_mode) )
		:effect (and (in r3) (trans_mode) (not (search_again)) (not (in r2)) (not (world_mode)))
	)
	(:action move-left-from-r2
		:precondition (and (in r2) (world_mode) )
		:effect (and (in r1) (trans_mode) (not (search_again)) (not (in r2)) (not (world_mode)))
	)
	(:action move-right-from-r3
		:precondition (and (in r3) (world_mode) )
		:effect (and (in r4) (trans_mode) (not (search_again)) (not (in r3)) (not (world_mode)))
	)
	(:action move-left-from-r3
		:precondition (and (in r3) (world_mode) )
		:effect (and (in r2) (trans_mode) (not (search_again)) (not (in r3)) (not (world_mode)))
	)
	(:action move-right-from-r4
		:precondition (and (in r4) (world_mode) )
		:effect (and (in r5) (trans_mode) (not (search_again)) (not (in r4)) (not (world_mode)))
	)
	(:action move-left-from-r4
		:precondition (and (in r4) (world_mode) )
		:effect (and (in r3) (trans_mode) (not (search_again)) (not (in r4)) (not (world_mode)))
	)
	(:action move-right-from-r5
		:precondition (and (in r5) (world_mode) )
		:effect (and (in r6) (trans_mode) (not (search_again)) (not (in r5)) (not (world_mode)))
	)
	(:action move-left-from-r5
		:precondition (and (in r5) (world_mode) )
		:effect (and (in r4) (trans_mode) (not (search_again)) (not (in r5)) (not (world_mode)))
	)
	(:action move-right-from-r6
		:precondition (and (in r6) (world_mode) )
		:effect (and (in r7) (trans_mode) (not (search_again)) (not (in r6)) (not (world_mode)))
	)
	(:action move-left-from-r6
		:precondition (and (in r6) (world_mode) )
		:effect (and (in r5) (trans_mode) (not (search_again)) (not (in r6)) (not (world_mode)))
	)
	(:action move-right-from-r7
		:precondition (and (in r7) (world_mode) )
		:effect (and (in r8) (trans_mode) (not (search_again)) (not (in r7)) (not (world_mode)))
	)
	(:action move-left-from-r7
		:precondition (and (in r7) (world_mode) )
		:effect (and (in r6) (trans_mode) (not (search_again)) (not (in r7)) (not (world_mode)))
	)
	(:action move-right-from-r8
		:precondition (and (in r8) (world_mode) )
		:effect (and (in r9) (trans_mode) (not (search_again)) (not (in r8)) (not (world_mode)))
	)
	(:action move-left-from-r8
		:precondition (and (in r8) (world_mode) )
		:effect (and (in r7) (trans_mode) (not (search_again)) (not (in r8)) (not (world_mode)))
	)
	(:action move-right-from-r9
		:precondition (and (in r9) (world_mode) )
		:effect (and (in r10) (trans_mode) (not (search_again)) (not (in r9)) (not (world_mode)))
	)
	(:action move-left-from-r9
		:precondition (and (in r9) (world_mode) )
		:effect (and (in r8) (trans_mode) (not (search_again)) (not (in r9)) (not (world_mode)))
	)
	(:action move-right-from-r10
		:precondition (and (in r10) (world_mode) )
		:effect (and (in r11) (trans_mode) (not (search_again)) (not (in r10)) (not (world_mode)))
	)
	(:action move-left-from-r10
		:precondition (and (in r10) (world_mode) )
		:effect (and (in r9) (trans_mode) (not (search_again)) (not (in r10)) (not (world_mode)))
	)
	(:action move-right-from-r11
		:precondition (and (in r11) (world_mode) )
		:effect (and (in r12) (trans_mode) (not (search_again)) (not (in r11)) (not (world_mode)))
	)
	(:action move-left-from-r11
		:precondition (and (in r11) (world_mode) )
		:effect (and (in r10) (trans_mode) (not (search_again)) (not (in r11)) (not (world_mode)))
	)
	(:action move-right-from-r12
		:precondition (and (in r12) (world_mode) )
		:effect (and (in r13) (trans_mode) (not (search_again)) (not (in r12)) (not (world_mode)))
	)
	(:action move-left-from-r12
		:precondition (and (in r12) (world_mode) )
		:effect (and (in r11) (trans_mode) (not (search_again)) (not (in r12)) (not (world_mode)))
	)
	(:action move-right-from-r13
		:precondition (and (in r13) (world_mode) )
		:effect (and (in r14) (trans_mode) (not (search_again)) (not (in r13)) (not (world_mode)))
	)
	(:action move-left-from-r13
		:precondition (and (in r13) (world_mode) )
		:effect (and (in r12) (trans_mode) (not (search_again)) (not (in r13)) (not (world_mode)))
	)
	(:action move-right-from-r14
		:precondition (and (in r14) (world_mode) )
		:effect (and (in r15) (trans_mode) (not (search_again)) (not (in r14)) (not (world_mode)))
	)
	(:action move-left-from-r14
		:precondition (and (in r14) (world_mode) )
		:effect (and (in r13) (trans_mode) (not (search_again)) (not (in r14)) (not (world_mode)))
	)
	(:action move-right-from-r15
		:precondition (and (in r15) (world_mode) )
		:effect (and (in r16) (trans_mode) (not (search_again)) (not (in r15)) (not (world_mode)))
	)
	(:action move-left-from-r15
		:precondition (and (in r15) (world_mode) )
		:effect (and (in r14) (trans_mode) (not (search_again)) (not (in r15)) (not (world_mode)))
	)
	(:action move-right-from-r16
		:precondition (and (in r16) (world_mode) )
		:effect (and (in r17) (trans_mode) (not (search_again)) (not (in r16)) (not (world_mode)))
	)
	(:action move-left-from-r16
		:precondition (and (in r16) (world_mode) )
		:effect (and (in r15) (trans_mode) (not (search_again)) (not (in r16)) (not (world_mode)))
	)
	(:action move-right-from-r17
		:precondition (and (in r17) (world_mode) )
		:effect (and (in r18) (trans_mode) (not (search_again)) (not (in r17)) (not (world_mode)))
	)
	(:action move-left-from-r17
		:precondition (and (in r17) (world_mode) )
		:effect (and (in r16) (trans_mode) (not (search_again)) (not (in r17)) (not (world_mode)))
	)
	(:action move-right-from-r18
		:precondition (and (in r18) (world_mode) )
		:effect (and (in r19) (trans_mode) (not (search_again)) (not (in r18)) (not (world_mode)))
	)
	(:action move-left-from-r18
		:precondition (and (in r18) (world_mode) )
		:effect (and (in r17) (trans_mode) (not (search_again)) (not (in r18)) (not (world_mode)))
	)
	(:action move-right-from-r19
		:precondition (and (in r19) (world_mode) )
		:effect (and (in r20) 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r20) (trans_mode) (not (search_again)) (not (in r19)) (not (world_mode)))
	)
	(:action move-left-from-r19
		:precondition (and (in r19) (world_mode) )
		:effect (and (in r18) (trans_mode) (not (search_again)) (not (in r19)) (not (world_mode)))
	)
	(:action move-right-from-r20
		:precondition (and (in r20) (world_mode) )
		:effect (and (in r21) (trans_mode) (not (search_again)) (not (in r20)) (not (world_mode)))
	)
	(:action move-left-from-r20
		:precondition (and (in r20) (world_mode) )
		:effect (and (in r19) (trans_mode) (not (search_again)) (not (in r20)) (not (world_mode)))
	)
	(:action move-right-from-r21
		:precondition (and (in r21) (world_mode) )
		:effect (and (in r22) (trans_mode) (not (search_again)) (not (in r21)) (not (world_mode)))
	)
	(:action move-left-from-r21
		:precondition (and (in r21) (world_mode) )
		:effect (and (in r20) 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r20) (trans_mode) (not (search_again)) (not (in r21)) (not (world_mode)))
	)
	(:action move-right-from-r22
		:precondition (and (in r22) (world_mode) )
		:effect (and (in r23) (trans_mode) (not (search_again)) (not (in r22)) (not (world_mode)))
	)
	(:action move-left-from-r22
		:precondition (and (in r22) (world_mode) )
		:effect (and (in r21) (trans_mode) (not (search_again)) (not (in r22)) (not (world_mode)))
	)
	(:action move-right-from-r23
		:precondition (and (in r23) (world_mode) )
		:effect (and (in r24) (trans_mode) (not (search_again)) (not (in r23)) (not (world_mode)))
	)
	(:action move-left-from-r23
		:precondition (and (in r23) (world_mode) )
		:effect (and (in r22) (trans_mode) (not (search_again)) (not (in r23)) (not (world_mode)))
	)
	(:action move-right-from-r24
		:precondition (and (in r24) (world_mode) )
		:effect (and (in r25) (trans_mode) (not (search_again)) (not (in r24)) (not (world_mode)))
	)
	(:action move-left-from-r24
		:precondition (and (in r24) (world_mode) )
		:effect (and (in r23) (trans_mode) (not (search_again)) (not (in r24)) (not (world_mode)))
	)
	(:action move-right-from-r25
		:precondition (and (in r25) (world_mode) )
		:effect (and (in r26) (trans_mode) (not (search_again)) (not (in r25)) (not (world_mode)))
	)
	(:action move-left-from-r25
		:precondition (and (in r25) (world_mode) )
		:effect (and (in r24) (trans_mode) (not (search_again)) (not (in r25)) (not (world_mode)))
	)
	(:action move-right-from-r26
		:precondition (and (in r26) (world_mode) )
		:effect (and (in r27) (trans_mode) (not (search_again)) (not (in r26)) (not (world_mode)))
	)
	(:action move-left-from-r26
		:precondition (and (in r26) (world_mode) )
		:effect (and (in r25) (trans_mode) (not (search_again)) (not (in r26)) (not (world_mode)))
	)
	(:action move-right-from-r27
		:precondition (and (in r27) (world_mode) )
		:effect (and (in r28) (trans_mode) (not (search_again)) (not (in r27)) (not (world_mode)))
	)
	(:action move-left-from-r27
		:precondition (and (in r27) (world_mode) )
		:effect (and (in r26) (trans_mode) (not (search_again)) (not (in r27)) (not (world_mode)))
	)
	(:action move-right-from-r28
		:precondition (and (in r28) (world_mode) )
		:effect (and (in r29) (trans_mode) (not (search_again)) (not (in r28)) (not (world_mode)))
	)
	(:action move-left-from-r28
		:precondition (and (in r28) (world_mode) )
		:effect (and (in r27) (trans_mode) (not (search_again)) (not (in r28)) (not (world_mode)))
	)
	(:action move-right-from-r29
		:precondition (and (in r29) (world_mode) )
		:effect (and (in r30) (trans_mode) (not (search_again)) (not (in r29)) (not (world_mode)))
	)
	(:action move-left-from-r29
		:precondition (and (in r29) (world_mode) )
		:effect (and (in r28) (trans_mode) (not (search_again)) (not (in r29)) (not (world_mode)))
	)
	(:action move-right-from-r30
		:precondition (and (in r30) (world_mode) )
		:effect (and (in r31) (trans_mode) (not (search_again)) (not (in r30)) (not (world_mode)))
	)
	(:action move-left-from-r30
		:precondition (and (in r30) (world_mode) )
		:effect (and (in r29) (trans_mode) (not (search_again)) (not (in r30)) (not (world_mode)))
	)
	(:action move-right-from-r31
		:precondition (and (in r31) (world_mode) )
		:effect (and (in r32) (trans_mode) (not (search_again)) (not (in r31)) (not (world_mode)))
	)
	(:action move-left-from-r31
		:precondition (and (in r31) (world_mode) )
		:effect (and (in r30) (trans_mode) (not (search_again)) (not (in r31)) (not (world_mode)))
	)
	(:action move-right-from-r32
		:precondition (and (in r32) (world_mode) )
		:effect (and (in r33) (trans_mode) (not (search_again)) (not (in r32)) (not (world_mode)))
	)
	(:action move-left-from-r32
		:precondition (and (in r32) (world_mode) )
		:effect (and (in r31) (trans_mode) (not (search_again)) (not (in r32)) (not (world_mode)))
	)
	(:action move-right-from-r33
		:precondition (and (in r33) (world_mode) )
		:effect (and (in r34) (trans_mode) (not (search_again)) (not (in r33)) (not (world_mode)))
	)
	(:action move-left-from-r33
		:precondition (and (in r33) (world_mode) )
		:effect (and (in r32) (trans_mode) (not (search_again)) (not (in r33)) (not (world_mode)))
	)
	(:action move-right-from-r34
		:precondition (and (in r34) (world_mode) )
		:effect (and (in r35) (trans_mode) (not (search_again)) (not (in r34)) (not (world_mode)))
	)
	(:action move-left-from-r34
		:precondition (and (in r34) (world_mode) )
		:effect (and (in r33) (trans_mode) (not (search_again)) (not (in r34)) (not (world_mode)))
	)
	(:action move-right-from-r35
		:precondition (and (in r35) (world_mode) )
		:effect (and (in r36) (trans_mode) (not (search_again)) (not (in r35)) (not (world_mode)))
	)
	(:action move-left-from-r35
		:precondition (and (in r35) (world_mode) )
		:effect (and (in r34) (trans_mode) (not (search_again)) (not (in r35)) (not (world_mode)))
	)
	(:action move-right-from-r36
		:precondition (and (in r36) (world_mode) )
		:effect (and (in r37) (trans_mode) (not (search_again)) (not (in r36)) (not (world_mode)))
	)
	(:action move-left-from-r36
		:precondition (and (in r36) (world_mode) )
		:effect (and (in r35) (trans_mode) (not (search_again)) (not (in r36)) (not (world_mode)))
	)
	(:action move-right-from-r37
		:precondition (and (in r37) (world_mode) )
		:effect (and (in r38) (trans_mode) (not (search_again)) (not (in r37)) (not (world_mode)))
	)
	(:action move-left-from-r37
		:precondition (and (in r37) (world_mode) )
		:effect (and (in r36) (trans_mode) (not (search_again)) (not (in r37)) (not (world_mode)))
	)
	(:action move-right-from-r38
		:precondition (and (in r38) (world_mode) )
		:effect (and (in r39) (trans_mode) (not (search_again)) (not (in r38)) (not (world_mode)))
	)
	(:action move-left-from-r38
		:precondition (and (in r38) (world_mode) )
		:effect (and (in r37) (trans_mode) (not (search_again)) (not (in r38)) (not (world_mode)))
	)
	(:action move-right-from-r39
		:precondition (and (in r39) (world_mode) )
		:effect (and (in r40) 
			(oneof
			 (not (seen))
			 (seen)
			) (searched_in r40) (trans_mode) (not (search_again)) (not (in r39)) (not (world_mode)))
	)
	(:action move-left-from-r39
		:precondition (and (in r39) (world_mode) )
		:effect (and (in r38) (trans_mode) (not (search_again)) (not (in r39)) (not (world_mode)))
	)
	(:action move-right-from-r40
		:precondition (and (in r40) (world_mode) )
		:effect (and (in r1) (trans_mode) (not (search_again)) (not (in r40)) (not (world_mode)))
	)
	(:action move-left-from-r40
		:precondition (and (in r40) (world_mode) )
		:effect (and (in r39) (trans_mode) (not (search_again)) (not (in r40)) (not (world_mode)))
	)
	(:action stay
		:precondition (and (f_ok) (world_mode) )
		:effect (and (trans_mode) (not (world_mode)))
	)
	(:action searching_again
		:precondition (and (searched_in r20) (searched_in r40) (world_mode) (not (seen)))
		:effect (and (search_again) (trans_mode) (not (searched_in r20)) (not (searched_in r40)) (not (world_mode)))
	)
	(:action trans_aut_q0_idx0
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx0 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx1 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx1
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx1 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx2 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx2
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx2 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx3 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx3
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx3 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx4 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx4
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx4 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx5 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx5
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx5 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx6 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx6
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx6 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx7 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx7
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx7 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx8 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx8
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx8 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx9 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx9
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx9 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx10 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx10
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx10 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx11 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx11
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx11 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx12 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx12
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx12 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx13 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx13
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx13 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx14 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx14
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx14 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx15 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx15
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx15 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx16 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx16
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx16 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx17 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx17
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx17 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx18 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx18
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx18 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx19 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx19
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx19 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx20 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx20
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx20 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx21 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx21
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx21 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx22 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx22
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx22 q0) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx23 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q1_idx0
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx0 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx1 q0))
			) (and (newautstate q1) (newcnt_idx0 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx1
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx1 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx2 q0))
			) (and (newautstate q1) (newcnt_idx1 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx2
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx2 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx3 q0))
			) (and (newautstate q1) (newcnt_idx2 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx3
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx3 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx4 q0))
			) (and (newautstate q1) (newcnt_idx3 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx4
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx4 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx5 q0))
			) (and (newautstate q1) (newcnt_idx4 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx5
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx5 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx6 q0))
			) (and (newautstate q1) (newcnt_idx5 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx6
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx6 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx7 q0))
			) (and (newautstate q1) (newcnt_idx6 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx7
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx7 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx8 q0))
			) (and (newautstate q1) (newcnt_idx7 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx8
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx8 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx9 q0))
			) (and (newautstate q1) (newcnt_idx8 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx9
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx9 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx10 q0))
			) (and (newautstate q1) (newcnt_idx9 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx10
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx10 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx11 q0))
			) (and (newautstate q1) (newcnt_idx10 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx11
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx11 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx12 q0))
			) (and (newautstate q1) (newcnt_idx11 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx12
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx12 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx13 q0))
			) (and (newautstate q1) (newcnt_idx12 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx13
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx13 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx14 q0))
			) (and (newautstate q1) (newcnt_idx13 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx14
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx14 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx15 q0))
			) (and (newautstate q1) (newcnt_idx14 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx15
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx15 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx16 q0))
			) (and (newautstate q1) (newcnt_idx15 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx16
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx16 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx17 q0))
			) (and (newautstate q1) (newcnt_idx16 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx17
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx17 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx18 q0))
			) (and (newautstate q1) (newcnt_idx17 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx18
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx18 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx19 q0))
			) (and (newautstate q1) (newcnt_idx18 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx19
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx19 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx20 q0))
			) (and (newautstate q1) (newcnt_idx19 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx20
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx20 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx21 q0))
			) (and (newautstate q1) (newcnt_idx20 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx21
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx21 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx22 q0))
			) (and (newautstate q1) (newcnt_idx21 q1)) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx22
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx22 q1) )
		:effect (and 
			(when (and (not (search_again)) (not (seen)))
			 (and (newautstate q0) (newcnt_idx23 q0))
			) (and (newautstate q1) (newcnt_idx22 q1)) (not (oldautstate q1)))
	)
	(:action start_sync
		:precondition (and (trans_mode) (not (oldautstate q0)) (not (oldautstate q1)))
		:effect (and (sync_mode) (not (trans_mode)) (not (oldcnt_idx0 q0)) (not (oldcnt_idx1 q0)) (not (oldcnt_idx2 q0)) (not (oldcnt_idx3 q0)) (not (oldcnt_idx4 q0)) (not (oldcnt_idx5 q0)) (not (oldcnt_idx6 q0)) (not (oldcnt_idx7 q0)) (not (oldcnt_idx8 q0)) (not (oldcnt_idx9 q0)) (not (oldcnt_idx10 q0)) (not (oldcnt_idx11 q0)) (not (oldcnt_idx12 q0)) (not (oldcnt_idx13 q0)) (not (oldcnt_idx14 q0)) (not (oldcnt_idx15 q0)) (not (oldcnt_idx16 q0)) (not (oldcnt_idx17 q0)) (not (oldcnt_idx18 q0)) (not (oldcnt_idx19 q0)) (not (oldcnt_idx20 q0)) (not (oldcnt_idx21 q0)) (not (oldcnt_idx22 q0)) (not (oldcnt_idx0 q1)) (not (oldcnt_idx1 q1)) (not (oldcnt_idx2 q1)) (not (oldcnt_idx3 q1)) (not (oldcnt_idx4 q1)) (not (oldcnt_idx5 q1)) (not (oldcnt_idx6 q1)) (not (oldcnt_idx7 q1)) (not (oldcnt_idx8 q1)) (not (oldcnt_idx9 q1)) (not (oldcnt_idx10 q1)) (not (oldcnt_idx11 q1)) (not (oldcnt_idx12 q1)) (not (oldcnt_idx13 q1)) (not (oldcnt_idx14 q1)) (not (oldcnt_idx15 q1)) (not (oldcnt_idx16 q1)) (not (oldcnt_idx17 q1)) (not (oldcnt_idx18 q1)) (not (oldcnt_idx19 q1)) (not (oldcnt_idx20 q1)) (not (oldcnt_idx21 q1)) (not (oldcnt_idx22 q1)))
	)
	(:action sync_aut_idx0
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx0 ?q) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx0 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx1
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx1 ?q) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx1 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx2
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx2 ?q) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx2 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx3
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx3 ?q) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx3 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx4
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx4 ?q) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx4 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx5
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx5 ?q) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx5 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx6
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx6 ?q) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx6 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx7
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx7 ?q) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx7 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx8
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx8 ?q) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx8 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx9
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx9 ?q) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx9 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx10
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx10 ?q) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx10 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx11
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx11 ?q) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx11 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx12
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx12 ?q) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx12 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx13
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx13 ?q) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx13 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx14
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx14 ?q) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx14 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx15
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx15 ?q) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx15 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx16
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx16 ?q) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx16 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx17
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx17 ?q) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx17 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx18
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx18 ?q) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx18 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx19
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx19 ?q) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx19 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx20
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx20 ?q) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx20 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx21
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx21 ?q) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx21 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action sync_aut_idx22
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx22 ?q) (not (newcnt_idx23 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx22 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)) (not (newcnt_idx7 ?q)) (not (newcnt_idx8 ?q)) (not (newcnt_idx9 ?q)) (not (newcnt_idx10 ?q)) (not (newcnt_idx11 ?q)) (not (newcnt_idx12 ?q)) (not (newcnt_idx13 ?q)) (not (newcnt_idx14 ?q)) (not (newcnt_idx15 ?q)) (not (newcnt_idx16 ?q)) (not (newcnt_idx17 ?q)) (not (newcnt_idx18 ?q)) (not (newcnt_idx19 ?q)) (not (newcnt_idx20 ?q)) (not (newcnt_idx21 ?q)) (not (newcnt_idx22 ?q)) (not (newcnt_idx23 ?q)))
	)
	(:action continue
		:precondition (and (sync_mode) (not (newautstate q0)) (not (newautstate q1)))
		:effect (and 
			(oneof
			 (dummy_goal)
			 (world_mode)
			) (not (sync_mode)))
	)
)
