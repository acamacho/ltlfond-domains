(define (domain packages)
	(:requirements :strips :typing :equality)
	(:constants p1 - pkg l1 desk - loc)
	(:types pkg loc qstate)
	(:predicates (in_store ?p - pkg) (want ?p - pkg) (active_request) (pkg_served) (pkg_requested) (pkg_arrived) (pkg_stored) (holding ?p - pkg) (pkg_at ?l - loc) (robot_at ?l - loc) (oldautstate ?q - qstate) (newautstate ?q - qstate) (oldcnt_idx0 ?q - qstate) (oldcnt_idx1 ?q - qstate) (oldcnt_idx2 ?q - qstate) (oldcnt_idx3 ?q - qstate) (oldcnt_idx4 ?q - qstate) (oldcnt_idx5 ?q - qstate) (newcnt_idx0 ?q - qstate) (newcnt_idx1 ?q - qstate) (newcnt_idx2 ?q - qstate) (newcnt_idx3 ?q - qstate) (newcnt_idx4 ?q - qstate) (newcnt_idx5 ?q - qstate) (newcnt_idx6 ?q - qstate) (world_mode) (trans_mode) (sync_mode) (dummy_goal))
	(:action serve-p1
		:precondition (and (in_store p1) (want p1) (holding p1) (robot_at desk) (world_mode) (not (pkg_served)))
		:effect (and (pkg_served) (trans_mode) (not (pkg_requested)) (not (active_request)) (not (in_store p1)) (not (want p1)) (not (holding p1)) (not (world_mode)))
	)
	(:action pickup-p1
		:precondition (and (in_store p1) (want p1) (robot_at l1) (world_mode) (not (pkg_served)))
		:effect (and (holding p1) (trans_mode) (not (pkg_at l1)) (not (world_mode)))
	)
	(:action putdown-p1
		:precondition (and (pkg_arrived) (holding p1) (robot_at l1) (world_mode) )
		:effect (and (in_store p1) (pkg_at l1) (pkg_stored) (trans_mode) (not (holding p1)) (not (pkg_arrived)) (not (active_request)) (not (world_mode)))
	)
	(:action restock-p1
		:precondition (and (want p1) (world_mode) (not (pkg_served)) (not (in_store p1)))
		:effect (and (in_store p1) (trans_mode) (not (world_mode)))
	)
	(:action movedesk_l1
		:precondition (and (robot_at desk) (world_mode) )
		:effect (and (robot_at l1) (trans_mode) (not (robot_at desk)) (not (world_mode)))
	)
	(:action movel1_desk
		:precondition (and (robot_at l1) (world_mode) )
		:effect (and (robot_at desk) (trans_mode) (not (robot_at l1)) (not (world_mode)))
	)
	(:action request
		:precondition (and (robot_at desk) (world_mode) (not (active_request)))
		:effect (and (active_request) 
			(oneof
			 (and (pkg_requested) (want p1))
			 (and (pkg_arrived) (holding p1))
			) (trans_mode) (not (pkg_served)) (not (pkg_stored)) (not (world_mode)))
	)
	(:action trans_aut_q0_idx0
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx0 q0) )
		:effect (and 
			(when (and (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx1 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx1
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx1 q0) )
		:effect (and 
			(when (and (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx2 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx2
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx2 q0) )
		:effect (and 
			(when (and (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx3 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx3
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx3 q0) )
		:effect (and 
			(when (and (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx4 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx4
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx4 q0) )
		:effect (and 
			(when (and (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx5 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q0_idx5
		:precondition (and (trans_mode) (oldautstate q0) (oldcnt_idx5 q0) )
		:effect (and 
			(when (and (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx6 q0))
			) (not (oldautstate q0)))
	)
	(:action trans_aut_q1_idx0
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx0 q1) )
		:effect (and 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx1 q1))
			) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx1
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx1 q1) )
		:effect (and 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx2 q1))
			) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx2
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx2 q1) )
		:effect (and 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx3 q1))
			) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx3
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx3 q1) )
		:effect (and 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx4 q1))
			) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx4
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx4 q1) )
		:effect (and 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx5 q1))
			) (not (oldautstate q1)))
	)
	(:action trans_aut_q1_idx5
		:precondition (and (trans_mode) (oldautstate q1) (oldcnt_idx5 q1) )
		:effect (and 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx6 q1))
			) (not (oldautstate q1)))
	)
	(:action trans_aut_q2_idx0
		:precondition (and (trans_mode) (oldautstate q2) (oldcnt_idx0 q2) )
		:effect (and 
			(when (and (active_request) (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx1 q0))
			) 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx1 q1))
			) (and (newautstate q2) (newcnt_idx0 q2)) (not (oldautstate q2)))
	)
	(:action trans_aut_q2_idx1
		:precondition (and (trans_mode) (oldautstate q2) (oldcnt_idx1 q2) )
		:effect (and 
			(when (and (active_request) (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx2 q0))
			) 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx2 q1))
			) (and (newautstate q2) (newcnt_idx1 q2)) (not (oldautstate q2)))
	)
	(:action trans_aut_q2_idx2
		:precondition (and (trans_mode) (oldautstate q2) (oldcnt_idx2 q2) )
		:effect (and 
			(when (and (active_request) (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx3 q0))
			) 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx3 q1))
			) (and (newautstate q2) (newcnt_idx2 q2)) (not (oldautstate q2)))
	)
	(:action trans_aut_q2_idx3
		:precondition (and (trans_mode) (oldautstate q2) (oldcnt_idx3 q2) )
		:effect (and 
			(when (and (active_request) (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx4 q0))
			) 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx4 q1))
			) (and (newautstate q2) (newcnt_idx3 q2)) (not (oldautstate q2)))
	)
	(:action trans_aut_q2_idx4
		:precondition (and (trans_mode) (oldautstate q2) (oldcnt_idx4 q2) )
		:effect (and 
			(when (and (active_request) (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx5 q0))
			) 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx5 q1))
			) (and (newautstate q2) (newcnt_idx4 q2)) (not (oldautstate q2)))
	)
	(:action trans_aut_q2_idx5
		:precondition (and (trans_mode) (oldautstate q2) (oldcnt_idx5 q2) )
		:effect (and 
			(when (and (active_request) (not (pkg_served)) (not (pkg_stored)))
			 (and (newautstate q0) (newcnt_idx6 q0))
			) 
			(when (not (active_request))
			 (and (newautstate q1) (newcnt_idx6 q1))
			) (and (newautstate q2) (newcnt_idx5 q2)) (not (oldautstate q2)))
	)
	(:action start_sync
		:precondition (and (trans_mode) (not (oldautstate q0)) (not (oldautstate q1)) (not (oldautstate q2)))
		:effect (and (sync_mode) (not (trans_mode)) (not (oldcnt_idx0 q0)) (not (oldcnt_idx1 q0)) (not (oldcnt_idx2 q0)) (not (oldcnt_idx3 q0)) (not (oldcnt_idx4 q0)) (not (oldcnt_idx5 q0)) (not (oldcnt_idx0 q1)) (not (oldcnt_idx1 q1)) (not (oldcnt_idx2 q1)) (not (oldcnt_idx3 q1)) (not (oldcnt_idx4 q1)) (not (oldcnt_idx5 q1)) (not (oldcnt_idx0 q2)) (not (oldcnt_idx1 q2)) (not (oldcnt_idx2 q2)) (not (oldcnt_idx3 q2)) (not (oldcnt_idx4 q2)) (not (oldcnt_idx5 q2)))
	)
	(:action sync_aut_idx0
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx0 ?q) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx0 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)))
	)
	(:action sync_aut_idx1
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx1 ?q) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx1 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)))
	)
	(:action sync_aut_idx2
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx2 ?q) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx2 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)))
	)
	(:action sync_aut_idx3
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx3 ?q) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx3 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)))
	)
	(:action sync_aut_idx4
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx4 ?q) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx4 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)))
	)
	(:action sync_aut_idx5
		:parameters (?q - qstate)
		:precondition (and (sync_mode) (newautstate ?q) (newcnt_idx5 ?q) (not (newcnt_idx6 ?q)))
		:effect (and (oldautstate ?q) (oldcnt_idx5 ?q) (not (newautstate ?q)) (not (newcnt_idx0 ?q)) (not (newcnt_idx1 ?q)) (not (newcnt_idx2 ?q)) (not (newcnt_idx3 ?q)) (not (newcnt_idx4 ?q)) (not (newcnt_idx5 ?q)) (not (newcnt_idx6 ?q)))
	)
	(:action continue
		:precondition (and (sync_mode) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)))
		:effect (and 
			(oneof
			 (dummy_goal)
			 (world_mode)
			) (not (sync_mode)))
	)
)
