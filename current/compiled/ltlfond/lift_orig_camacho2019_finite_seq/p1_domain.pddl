(define (domain lift)
	(:requirements :strips :typing :equality)
	(:constants f1 - floor)
	(:types floor qstate)
	(:predicates (at ?f - floor) (req ?f - floor) (turn ?f - floor) (check) (called) (move) (oldautstate ?q - qstate) (newautstate ?q - qstate) (world_mode) (trans_mode) (dummy_goal))
	(:action push_f1
		:precondition (and (turn f1) (world_mode) (not (check)))
		:effect (and (check) (turn f1) 
			(oneof
			 (and)
			 (req f1)
			) (trans_mode) (not (turn f1)) (not (world_mode)))
	)
	(:action check_called_true_f1
		:precondition (and (check) (turn f1) (req f1) (world_mode) )
		:effect (and (called) (move) (trans_mode) (not (turn f1)) (not (check)) (not (world_mode)))
	)
	(:action check_called_false_f1
		:precondition (and (check) (turn f1) (world_mode) (not (req f1)))
		:effect (and (move) (trans_mode) (not (turn f1)) (not (check)) (not (world_mode)))
	)
	(:action stay_and_serve_at_f1
		:precondition (and (at f1) (req f1) (called) (move) (world_mode) )
		:effect (and (turn f1) (trans_mode) (not (req f1)) (not (move)) (not (world_mode)))
	)
	(:action no_op
		:precondition (and (move) (world_mode) )
		:effect (and (trans_mode) (not (move)) (not (world_mode)))
	)
	(:action restablish_mode
		:precondition (and (world_mode) (not (turn f1)) (not (check)) (not (move)))
		:effect (and (turn f1) (trans_mode) (not (called)) (not (world_mode)))
	)
	(:action trans_aut_q0_t0
		:precondition (and (trans_mode) (oldautstate q0) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q0_t1
		:precondition (and (trans_mode) (oldautstate q0) (at f1) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q0_t2
		:precondition (and (trans_mode) (oldautstate q0) )
		:effect (newautstate q6)
	)
	(:action trans_aut_q0_t3
		:precondition (and (trans_mode) (oldautstate q0) (called) )
		:effect (newautstate q5)
	)
	(:action trans_aut_q0_t4
		:precondition (and (trans_mode) (oldautstate q0) )
		:effect (newautstate q3)
	)
	(:action trans_aut_q0_t5
		:precondition (and (trans_mode) (oldautstate q0) (called) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q0_t6
		:precondition (and (trans_mode) (oldautstate q0) (at f1) )
		:effect (and (newautstate q7) (dummy_goal))
	)
	(:action trans_aut_q0_t7
		:precondition (and (trans_mode) (oldautstate q0) (called) )
		:effect (and (newautstate q7) (dummy_goal))
	)
	(:action trans_aut_q1_t8
		:precondition (and (trans_mode) (oldautstate q1) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q1_t9
		:precondition (and (trans_mode) (oldautstate q1) (at f1) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q1_t10
		:precondition (and (trans_mode) (oldautstate q1) (at f1) )
		:effect (and (newautstate q7) (dummy_goal))
	)
	(:action trans_aut_q2_t11
		:precondition (and (trans_mode) (oldautstate q2) )
		:effect (newautstate q2)
	)
	(:action trans_aut_q2_t12
		:precondition (and (trans_mode) (oldautstate q2) (called) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q2_t13
		:precondition (and (trans_mode) (oldautstate q2) (at f1) )
		:effect (newautstate q3)
	)
	(:action trans_aut_q2_t14
		:precondition (and (trans_mode) (oldautstate q2) (at f1) (called) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q2_t15
		:precondition (and (trans_mode) (oldautstate q2) (at f1) (called) )
		:effect (and (newautstate q7) (dummy_goal))
	)
	(:action trans_aut_q3_t16
		:precondition (and (trans_mode) (oldautstate q3) )
		:effect (newautstate q2)
	)
	(:action trans_aut_q3_t17
		:precondition (and (trans_mode) (oldautstate q3) (called) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q3_t18
		:precondition (and (trans_mode) (oldautstate q3) (at f1) )
		:effect (newautstate q3)
	)
	(:action trans_aut_q3_t19
		:precondition (and (trans_mode) (oldautstate q3) (at f1) (called) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q3_t20
		:precondition (and (trans_mode) (oldautstate q3) )
		:effect (newautstate q6)
	)
	(:action trans_aut_q3_t21
		:precondition (and (trans_mode) (oldautstate q3) (called) )
		:effect (newautstate q5)
	)
	(:action trans_aut_q3_t22
		:precondition (and (trans_mode) (oldautstate q3) )
		:effect (newautstate q3)
	)
	(:action trans_aut_q3_t23
		:precondition (and (trans_mode) (oldautstate q3) (called) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q3_t24
		:precondition (and (trans_mode) (oldautstate q3) (at f1) (called) )
		:effect (and (newautstate q7) (dummy_goal))
	)
	(:action trans_aut_q3_t25
		:precondition (and (trans_mode) (oldautstate q3) (called) )
		:effect (and (newautstate q7) (dummy_goal))
	)
	(:action trans_aut_q4_t26
		:precondition (and (trans_mode) (oldautstate q4) )
		:effect (newautstate q4)
	)
	(:action trans_aut_q4_t27
		:precondition (and (trans_mode) (oldautstate q4) (at f1) )
		:effect (newautstate q5)
	)
	(:action trans_aut_q4_t28
		:precondition (and (trans_mode) (oldautstate q4) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q4_t29
		:precondition (and (trans_mode) (oldautstate q4) (at f1) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q4_t30
		:precondition (and (trans_mode) (oldautstate q4) (at f1) )
		:effect (and (newautstate q7) (dummy_goal))
	)
	(:action trans_aut_q5_t31
		:precondition (and (trans_mode) (oldautstate q5) )
		:effect (newautstate q4)
	)
	(:action trans_aut_q5_t32
		:precondition (and (trans_mode) (oldautstate q5) (at f1) )
		:effect (newautstate q5)
	)
	(:action trans_aut_q5_t33
		:precondition (and (trans_mode) (oldautstate q5) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q5_t34
		:precondition (and (trans_mode) (oldautstate q5) (at f1) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q5_t35
		:precondition (and (trans_mode) (oldautstate q5) )
		:effect (newautstate q6)
	)
	(:action trans_aut_q5_t36
		:precondition (and (trans_mode) (oldautstate q5) (called) )
		:effect (newautstate q5)
	)
	(:action trans_aut_q5_t37
		:precondition (and (trans_mode) (oldautstate q5) )
		:effect (newautstate q3)
	)
	(:action trans_aut_q5_t38
		:precondition (and (trans_mode) (oldautstate q5) (called) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q5_t39
		:precondition (and (trans_mode) (oldautstate q5) (at f1) )
		:effect (and (newautstate q7) (dummy_goal))
	)
	(:action trans_aut_q5_t40
		:precondition (and (trans_mode) (oldautstate q5) (called) )
		:effect (and (newautstate q7) (dummy_goal))
	)
	(:action trans_aut_q6_t41
		:precondition (and (trans_mode) (oldautstate q6) )
		:effect (newautstate q6)
	)
	(:action trans_aut_q6_t42
		:precondition (and (trans_mode) (oldautstate q6) (called) )
		:effect (newautstate q5)
	)
	(:action trans_aut_q6_t43
		:precondition (and (trans_mode) (oldautstate q6) )
		:effect (newautstate q3)
	)
	(:action trans_aut_q6_t44
		:precondition (and (trans_mode) (oldautstate q6) (called) )
		:effect (newautstate q0)
	)
	(:action trans_aut_q6_t45
		:precondition (and (trans_mode) (oldautstate q6) (called) )
		:effect (and (newautstate q7) (dummy_goal))
	)
	(:action start_sync
		:precondition (and (trans_mode) )
		:effect (and (sync_mode) (not (trans_mode)) (not (oldautstate q0)) (not (oldautstate q1)) (not (oldautstate q2)) (not (oldautstate q3)) (not (oldautstate q4)) (not (oldautstate q5)) (not (oldautstate q6)) (not (oldautstate q7)))
	)
	(:action sync_q0_pos
		:precondition (and (sync_mode) (newautstate q0) )
		:effect (and (oldautstate q0) (not (newautstate q0)))
	)
	(:action sync_q1_pos
		:precondition (and (sync_mode) (newautstate q1) (not (newautstate q0)))
		:effect (and (oldautstate q1) (not (newautstate q1)))
	)
	(:action sync_q2_pos
		:precondition (and (sync_mode) (newautstate q2) (not (newautstate q0)) (not (newautstate q1)))
		:effect (and (oldautstate q2) (not (newautstate q2)))
	)
	(:action sync_q3_pos
		:precondition (and (sync_mode) (newautstate q3) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)))
		:effect (and (oldautstate q3) (not (newautstate q3)))
	)
	(:action sync_q4_pos
		:precondition (and (sync_mode) (newautstate q4) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)) (not (newautstate q3)))
		:effect (and (oldautstate q4) (not (newautstate q4)))
	)
	(:action sync_q5_pos
		:precondition (and (sync_mode) (newautstate q5) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)) (not (newautstate q3)) (not (newautstate q4)))
		:effect (and (oldautstate q5) (not (newautstate q5)))
	)
	(:action sync_q6_pos
		:precondition (and (sync_mode) (newautstate q6) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)) (not (newautstate q3)) (not (newautstate q4)) (not (newautstate q5)))
		:effect (and (oldautstate q6) (not (newautstate q6)))
	)
	(:action sync_q7_pos
		:precondition (and (sync_mode) (newautstate q7) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)) (not (newautstate q3)) (not (newautstate q4)) (not (newautstate q5)) (not (newautstate q6)))
		:effect (and (oldautstate q7) (not (newautstate q7)))
	)
	(:action continue
		:precondition (and (sync_mode) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)) (not (newautstate q3)) (not (newautstate q4)) (not (newautstate q5)) (not (newautstate q6)) (not (newautstate q7)))
		:effect (and (world_mode) (not (sync_mode)) (not (dummy_goal)))
	)
)
