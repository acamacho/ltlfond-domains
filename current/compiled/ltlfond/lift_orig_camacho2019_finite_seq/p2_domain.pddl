(define (domain lift)
	(:requirements :strips :typing :equality)
	(:constants f1 f2 - floor)
	(:types floor qstate)
	(:predicates (at ?f - floor) (req ?f - floor) (turn ?f - floor) (check) (called) (move) (oldautstate ?q - qstate) (newautstate ?q - qstate) (world_mode) (trans_mode) (dummy_goal))
	(:action push_f1
		:precondition (and (turn f1) (world_mode) (not (check)))
		:effect (and (turn f2) 
			(oneof
			 (and)
			 (req f1)
			) (trans_mode) (not (turn f1)) (not (world_mode)))
	)
	(:action push_f2
		:precondition (and (turn f2) (world_mode) (not (check)))
		:effect (and (check) (turn f1) 
			(oneof
			 (and)
			 (req f2)
			) (trans_mode) (not (turn f2)) (not (world_mode)))
	)
	(:action check_called_true_f1
		:precondition (and (check) (turn f1) (req f1) (world_mode) )
		:effect (and (called) (turn f2) (trans_mode) (not (turn f1)) (not (world_mode)))
	)
	(:action check_called_false_f1
		:precondition (and (check) (turn f1) (world_mode) (not (req f1)))
		:effect (and (turn f2) (trans_mode) (not (turn f1)) (not (world_mode)))
	)
	(:action check_called_true_f2
		:precondition (and (check) (turn f2) (req f2) (world_mode) )
		:effect (and (called) (move) (trans_mode) (not (turn f2)) (not (check)) (not (world_mode)))
	)
	(:action check_called_false_f2
		:precondition (and (check) (turn f2) (world_mode) (not (req f2)))
		:effect (and (move) (trans_mode) (not (turn f2)) (not (check)) (not (world_mode)))
	)
	(:action move_up_from_f1
		:precondition (and (at f1) (called) (move) (world_mode) (not (req f2)))
		:effect (and (at f2) (trans_mode) (not (at f1)) (not (move)) (not (world_mode)))
	)
	(:action move_up_and_serve_from_f1
		:precondition (and (at f1) (req f2) (called) (move) (world_mode) )
		:effect (and (at f2) (trans_mode) (not (at f1)) (not (req f2)) (not (move)) (not (world_mode)))
	)
	(:action move_down_from_f2
		:precondition (and (at f2) (move) (world_mode) (not (req f1)))
		:effect (and (at f1) (trans_mode) (not (at f2)) (not (move)) (not (world_mode)))
	)
	(:action move_down_and_serve_from_f2
		:precondition (and (at f2) (req f1) (move) (world_mode) )
		:effect (and (at f1) (trans_mode) (not (at f2)) (not (req f1)) (not (move)) (not (world_mode)))
	)
	(:action stay_and_serve_at_f1
		:precondition (and (at f1) (req f1) (called) (move) (world_mode) )
		:effect (and (turn f1) (trans_mode) (not (req f1)) (not (move)) (not (world_mode)))
	)
	(:action stay_and_serve_at_f2
		:precondition (and (at f2) (req f2) (called) (move) (world_mode) )
		:effect (and (turn f1) (trans_mode) (not (req f2)) (not (move)) (not (world_mode)))
	)
	(:action no_op
		:precondition (and (move) (world_mode) )
		:effect (and (trans_mode) (not (move)) (not (world_mode)))
	)
	(:action restablish_mode
		:precondition (and (world_mode) (not (turn f1)) (not (turn f2)) (not (check)) (not (move)))
		:effect (and (turn f1) (trans_mode) (not (called)) (not (world_mode)))
	)
	(:action trans_aut_q0_t0
		:precondition (and (trans_mode) (oldautstate q0) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q1_t1
		:precondition (and (trans_mode) (oldautstate q1) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q1_t2
		:precondition (and (trans_mode) (oldautstate q1) (called) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q1_t3
		:precondition (and (trans_mode) (oldautstate q1) (req f2) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q1_t4
		:precondition (and (trans_mode) (oldautstate q1) (at f2) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q1_t5
		:precondition (and (trans_mode) (oldautstate q1) (at f2) (called) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q1_t6
		:precondition (and (trans_mode) (oldautstate q1) (req f1) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q1_t7
		:precondition (and (trans_mode) (oldautstate q1) (at f1) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q1_t8
		:precondition (and (trans_mode) (oldautstate q1) (at f1) (req f2) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q1_t9
		:precondition (and (trans_mode) (oldautstate q1) (at f1) (at f2) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q2_t10
		:precondition (and (trans_mode) (oldautstate q2) )
		:effect (newautstate q2)
	)
	(:action trans_aut_q3_t11
		:precondition (and (trans_mode) (oldautstate q3) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q3_t12
		:precondition (and (trans_mode) (oldautstate q3) (called) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q3_t13
		:precondition (and (trans_mode) (oldautstate q3) (req f2) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q3_t14
		:precondition (and (trans_mode) (oldautstate q3) (at f2) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q3_t15
		:precondition (and (trans_mode) (oldautstate q3) (at f2) (called) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q3_t16
		:precondition (and (trans_mode) (oldautstate q3) (req f1) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q3_t17
		:precondition (and (trans_mode) (oldautstate q3) (at f1) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q3_t18
		:precondition (and (trans_mode) (oldautstate q3) (at f1) (req f2) )
		:effect (newautstate q1)
	)
	(:action trans_aut_q3_t19
		:precondition (and (trans_mode) (oldautstate q3) (at f1) (at f2) )
		:effect (and (newautstate q3) (dummy_goal))
	)
	(:action trans_aut_q4_t20
		:precondition (and (trans_mode) (oldautstate q4) )
		:effect (newautstate q2)
	)
	(:action start_sync
		:precondition (and (trans_mode) )
		:effect (and (sync_mode) (not (trans_mode)) (not (oldautstate q0)) (not (oldautstate q1)) (not (oldautstate q2)) (not (oldautstate q3)) (not (oldautstate q4)))
	)
	(:action sync_q0_pos
		:precondition (and (sync_mode) (newautstate q0) )
		:effect (and (oldautstate q0) (not (newautstate q0)))
	)
	(:action sync_q1_pos
		:precondition (and (sync_mode) (newautstate q1) (not (newautstate q0)))
		:effect (and (oldautstate q1) (not (newautstate q1)))
	)
	(:action sync_q2_pos
		:precondition (and (sync_mode) (newautstate q2) (not (newautstate q0)) (not (newautstate q1)))
		:effect (and (oldautstate q2) (not (newautstate q2)))
	)
	(:action sync_q3_pos
		:precondition (and (sync_mode) (newautstate q3) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)))
		:effect (and (oldautstate q3) (not (newautstate q3)))
	)
	(:action sync_q4_pos
		:precondition (and (sync_mode) (newautstate q4) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)) (not (newautstate q3)))
		:effect (and (oldautstate q4) (not (newautstate q4)))
	)
	(:action continue
		:precondition (and (sync_mode) (not (newautstate q0)) (not (newautstate q1)) (not (newautstate q2)) (not (newautstate q3)) (not (newautstate q4)))
		:effect (and (world_mode) (not (sync_mode)) (not (dummy_goal)))
	)
)
