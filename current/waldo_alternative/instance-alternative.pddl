(define (problem waldo-problem)
	(:domain waldo)
	(:objects )
	(:INIT
		(in r1) (f_ok)
	)
	(:goal (always 
			(eventually  
				(or 
					(search_again) 
					(seen) 
				)
			)
			) 
	)

)
