
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Waldo example for 10 rooms
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Taken from
;; Kress-Gazit, H., Fainekos, G.E., Pappas, G.J.
;; Where's Waldo? Sensor-Based Temporal Logic Motion Planning.
;; ICRA 2007: 3116-3121
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Goal formula:
;;
;; AE(r50 || seen) && AE(r10 || seen)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; []<> (search_again | seen)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain waldo)
	(:requirements :strips :typing :equality)
	(:types 
		room
	)
	(:constants
		r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 - room
	)
	(:predicates
		(seen) ;; found Waldo
		(search_again) ;; searching for Waldo
		(in ?r  - room)
		(searched_in ?r  - room)
	)

	(:action move-right-from-r1
		:precondition
			(and 
				(in r1)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1))
				(in r2)
			)
	)


	(:action move-left-from-r1
		:precondition
			(and 
				(in r1)
				(not (searched_in r10))
			)
		:effect
			(and
				(not (search_again))
				(not (in r1))
				(in r10)
				(oneof (not (seen)) (seen))
				(searched_in r10)
			)
	)

	(:action move-right-from-r2
		:precondition
			(and 
				(in r2)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2))
				(in r3)
			)
	)


	(:action move-left-from-r2
		:precondition
			(and 
				(in r2)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2))
				(in r1)
			)
	)

	(:action move-right-from-r3
		:precondition
			(and 
				(in r3)
			)
		:effect
			(and
				(not (search_again))
				(not (in r3))
				(in r4)
			)
	)


	(:action move-left-from-r3
		:precondition
			(and 
				(in r3)
			)
		:effect
			(and
				(not (search_again))
				(not (in r3))
				(in r2)
			)
	)

	(:action move-right-from-r4
		:precondition
			(and 
				(in r4)
			)
		:effect
			(and
				(not (search_again))
				(not (in r4))
				(oneof (not (seen)) (seen))
				(searched_in r5)
				(in r5)
			)
	)


	(:action move-left-from-r4
		:precondition
			(and 
				(in r4)
			)
		:effect
			(and
				(not (search_again))
				(not (in r4))
				(in r3)
			)
	)

	(:action move-right-from-r5
		:precondition
			(and 
				(in r5)
			)
		:effect
			(and
				(not (search_again))
				(not (in r5))
				(in r6)
			)
	)


	(:action move-left-from-r5
		:precondition
			(and 
				(in r5)
			)
		:effect
			(and
				(not (search_again))
				(not (in r5))
				(in r4)
			)
	)

	(:action move-right-from-r6
		:precondition
			(and 
				(in r6)
			)
		:effect
			(and
				(not (search_again))
				(not (in r6))
				(in r7)
			)
	)


	(:action move-left-from-r6
		:precondition
			(and 
				(in r6)
			)
		:effect
			(and
				(not (search_again))
				(not (in r6))
				(oneof (not (seen)) (seen))
				(searched_in r5)
				(in r5)
			)
	)

	(:action move-right-from-r7
		:precondition
			(and 
				(in r7)
			)
		:effect
			(and
				(not (search_again))
				(not (in r7))
				(in r8)
			)
	)


	(:action move-left-from-r7
		:precondition
			(and 
				(in r7)
			)
		:effect
			(and
				(not (search_again))
				(not (in r7))
				(in r6)
			)
	)

	(:action move-right-from-r8
		:precondition
			(and 
				(in r8)
			)
		:effect
			(and
				(not (search_again))
				(not (in r8))
				(in r9)
			)
	)


	(:action move-left-from-r8
		:precondition
			(and 
				(in r8)
			)
		:effect
			(and
				(not (search_again))
				(not (in r8))
				(in r7)
			)
	)

	(:action move-right-from-r9
		:precondition
			(and 
				(in r9)
			)
		:effect
			(and
				(not (search_again))
				(not (in r9))
				(oneof (not (seen)) (seen))
				(searched_in r10)
				(in r10)
			)
	)

	(:action move-left-from-r9
		:precondition
			(and 
				(in r9)
			)
		:effect
			(and
				(not (search_again))
				(not (in r9))
				(in r8)
			)
	)

	(:action move-right-from-r10
		:precondition
			(and 
				(in r10)
			)
		:effect
			(and
				(not (search_again))
				(not (in r10))
				(in r1)
			)
	)


	(:action move-left-from-r10
		:precondition
			(and 
				(in r10)
			)
		:effect
			(and
				(not (search_again))
				(not (in r10))
				(in r9)
			)
	)

	(:action stay
		:precondition
			(f_ok 
			)
		:effect
			(and
			)
	)


	(:action searching_again
		:precondition
			(and 
				(searched_in r5)
				(searched_in r10)
				(not (seen))
			)
		:effect
			(and
				(not (searched_in r5))
				(not (searched_in r10))
				(search_again)
			)
	)

)

