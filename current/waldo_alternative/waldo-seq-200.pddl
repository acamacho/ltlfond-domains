
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Waldo example for 200 rooms
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Taken from
;; Kress-Gazit, H., Fainekos, G.E., Pappas, G.J.
;; Where's Waldo? Sensor-Based Temporal Logic Motion Planning.
;; ICRA 2007: 3116-3121
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Goal formula:
;;
;; AE(r100 || seen) && AE(r200 || seen)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; []<> (search_again | seen)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain waldo)
	(:requirements :strips :typing :equality)
	(:types 
		room
	)
	(:constants
		r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 r11 r12 r13 r14 r15 r16 r17 r18 r19 r20 r21 r22 r23 r24 r25 r26 r27 r28 r29 r30 r31 r32 r33 r34 r35 r36 r37 r38 r39 r40 r41 r42 r43 r44 r45 r46 r47 r48 r49 r50 r51 r52 r53 r54 r55 r56 r57 r58 r59 r60 r61 r62 r63 r64 r65 r66 r67 r68 r69 r70 r71 r72 r73 r74 r75 r76 r77 r78 r79 r80 r81 r82 r83 r84 r85 r86 r87 r88 r89 r90 r91 r92 r93 r94 r95 r96 r97 r98 r99 r100 r101 r102 r103 r104 r105 r106 r107 r108 r109 r110 r111 r112 r113 r114 r115 r116 r117 r118 r119 r120 r121 r122 r123 r124 r125 r126 r127 r128 r129 r130 r131 r132 r133 r134 r135 r136 r137 r138 r139 r140 r141 r142 r143 r144 r145 r146 r147 r148 r149 r150 r151 r152 r153 r154 r155 r156 r157 r158 r159 r160 r161 r162 r163 r164 r165 r166 r167 r168 r169 r170 r171 r172 r173 r174 r175 r176 r177 r178 r179 r180 r181 r182 r183 r184 r185 r186 r187 r188 r189 r190 r191 r192 r193 r194 r195 r196 r197 r198 r199 r200 - room
	)
	(:predicates
		(seen) ;; found Waldo
		(search_again) ;; searching for Waldo
		(in ?r  - room)
		(searched_in ?r  - room)
	)

	(:action move-right-from-r1
		:precondition
			(and 
				(in r1)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1))
				(in r2)
			)
	)


	(:action move-left-from-r1
		:precondition
			(and 
				(in r1)
				(not (searched_in r200))
			)
		:effect
			(and
				(not (search_again))
				(not (in r1))
				(in r200)
				(oneof (not (seen)) (seen))
				(searched_in r200)
			)
	)

	(:action move-right-from-r2
		:precondition
			(and 
				(in r2)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2))
				(in r3)
			)
	)


	(:action move-left-from-r2
		:precondition
			(and 
				(in r2)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2))
				(in r1)
			)
	)

	(:action move-right-from-r3
		:precondition
			(and 
				(in r3)
			)
		:effect
			(and
				(not (search_again))
				(not (in r3))
				(in r4)
			)
	)


	(:action move-left-from-r3
		:precondition
			(and 
				(in r3)
			)
		:effect
			(and
				(not (search_again))
				(not (in r3))
				(in r2)
			)
	)

	(:action move-right-from-r4
		:precondition
			(and 
				(in r4)
			)
		:effect
			(and
				(not (search_again))
				(not (in r4))
				(in r5)
			)
	)


	(:action move-left-from-r4
		:precondition
			(and 
				(in r4)
			)
		:effect
			(and
				(not (search_again))
				(not (in r4))
				(in r3)
			)
	)

	(:action move-right-from-r5
		:precondition
			(and 
				(in r5)
			)
		:effect
			(and
				(not (search_again))
				(not (in r5))
				(in r6)
			)
	)


	(:action move-left-from-r5
		:precondition
			(and 
				(in r5)
			)
		:effect
			(and
				(not (search_again))
				(not (in r5))
				(in r4)
			)
	)

	(:action move-right-from-r6
		:precondition
			(and 
				(in r6)
			)
		:effect
			(and
				(not (search_again))
				(not (in r6))
				(in r7)
			)
	)


	(:action move-left-from-r6
		:precondition
			(and 
				(in r6)
			)
		:effect
			(and
				(not (search_again))
				(not (in r6))
				(in r5)
			)
	)

	(:action move-right-from-r7
		:precondition
			(and 
				(in r7)
			)
		:effect
			(and
				(not (search_again))
				(not (in r7))
				(in r8)
			)
	)


	(:action move-left-from-r7
		:precondition
			(and 
				(in r7)
			)
		:effect
			(and
				(not (search_again))
				(not (in r7))
				(in r6)
			)
	)

	(:action move-right-from-r8
		:precondition
			(and 
				(in r8)
			)
		:effect
			(and
				(not (search_again))
				(not (in r8))
				(in r9)
			)
	)


	(:action move-left-from-r8
		:precondition
			(and 
				(in r8)
			)
		:effect
			(and
				(not (search_again))
				(not (in r8))
				(in r7)
			)
	)

	(:action move-right-from-r9
		:precondition
			(and 
				(in r9)
			)
		:effect
			(and
				(not (search_again))
				(not (in r9))
				(in r10)
			)
	)


	(:action move-left-from-r9
		:precondition
			(and 
				(in r9)
			)
		:effect
			(and
				(not (search_again))
				(not (in r9))
				(in r8)
			)
	)

	(:action move-right-from-r10
		:precondition
			(and 
				(in r10)
			)
		:effect
			(and
				(not (search_again))
				(not (in r10))
				(in r11)
			)
	)


	(:action move-left-from-r10
		:precondition
			(and 
				(in r10)
			)
		:effect
			(and
				(not (search_again))
				(not (in r10))
				(in r9)
			)
	)

	(:action move-right-from-r11
		:precondition
			(and 
				(in r11)
			)
		:effect
			(and
				(not (search_again))
				(not (in r11))
				(in r12)
			)
	)


	(:action move-left-from-r11
		:precondition
			(and 
				(in r11)
			)
		:effect
			(and
				(not (search_again))
				(not (in r11))
				(in r10)
			)
	)

	(:action move-right-from-r12
		:precondition
			(and 
				(in r12)
			)
		:effect
			(and
				(not (search_again))
				(not (in r12))
				(in r13)
			)
	)


	(:action move-left-from-r12
		:precondition
			(and 
				(in r12)
			)
		:effect
			(and
				(not (search_again))
				(not (in r12))
				(in r11)
			)
	)

	(:action move-right-from-r13
		:precondition
			(and 
				(in r13)
			)
		:effect
			(and
				(not (search_again))
				(not (in r13))
				(in r14)
			)
	)


	(:action move-left-from-r13
		:precondition
			(and 
				(in r13)
			)
		:effect
			(and
				(not (search_again))
				(not (in r13))
				(in r12)
			)
	)

	(:action move-right-from-r14
		:precondition
			(and 
				(in r14)
			)
		:effect
			(and
				(not (search_again))
				(not (in r14))
				(in r15)
			)
	)


	(:action move-left-from-r14
		:precondition
			(and 
				(in r14)
			)
		:effect
			(and
				(not (search_again))
				(not (in r14))
				(in r13)
			)
	)

	(:action move-right-from-r15
		:precondition
			(and 
				(in r15)
			)
		:effect
			(and
				(not (search_again))
				(not (in r15))
				(in r16)
			)
	)


	(:action move-left-from-r15
		:precondition
			(and 
				(in r15)
			)
		:effect
			(and
				(not (search_again))
				(not (in r15))
				(in r14)
			)
	)

	(:action move-right-from-r16
		:precondition
			(and 
				(in r16)
			)
		:effect
			(and
				(not (search_again))
				(not (in r16))
				(in r17)
			)
	)


	(:action move-left-from-r16
		:precondition
			(and 
				(in r16)
			)
		:effect
			(and
				(not (search_again))
				(not (in r16))
				(in r15)
			)
	)

	(:action move-right-from-r17
		:precondition
			(and 
				(in r17)
			)
		:effect
			(and
				(not (search_again))
				(not (in r17))
				(in r18)
			)
	)


	(:action move-left-from-r17
		:precondition
			(and 
				(in r17)
			)
		:effect
			(and
				(not (search_again))
				(not (in r17))
				(in r16)
			)
	)

	(:action move-right-from-r18
		:precondition
			(and 
				(in r18)
			)
		:effect
			(and
				(not (search_again))
				(not (in r18))
				(in r19)
			)
	)


	(:action move-left-from-r18
		:precondition
			(and 
				(in r18)
			)
		:effect
			(and
				(not (search_again))
				(not (in r18))
				(in r17)
			)
	)

	(:action move-right-from-r19
		:precondition
			(and 
				(in r19)
			)
		:effect
			(and
				(not (search_again))
				(not (in r19))
				(in r20)
			)
	)


	(:action move-left-from-r19
		:precondition
			(and 
				(in r19)
			)
		:effect
			(and
				(not (search_again))
				(not (in r19))
				(in r18)
			)
	)

	(:action move-right-from-r20
		:precondition
			(and 
				(in r20)
			)
		:effect
			(and
				(not (search_again))
				(not (in r20))
				(in r21)
			)
	)


	(:action move-left-from-r20
		:precondition
			(and 
				(in r20)
			)
		:effect
			(and
				(not (search_again))
				(not (in r20))
				(in r19)
			)
	)

	(:action move-right-from-r21
		:precondition
			(and 
				(in r21)
			)
		:effect
			(and
				(not (search_again))
				(not (in r21))
				(in r22)
			)
	)


	(:action move-left-from-r21
		:precondition
			(and 
				(in r21)
			)
		:effect
			(and
				(not (search_again))
				(not (in r21))
				(in r20)
			)
	)

	(:action move-right-from-r22
		:precondition
			(and 
				(in r22)
			)
		:effect
			(and
				(not (search_again))
				(not (in r22))
				(in r23)
			)
	)


	(:action move-left-from-r22
		:precondition
			(and 
				(in r22)
			)
		:effect
			(and
				(not (search_again))
				(not (in r22))
				(in r21)
			)
	)

	(:action move-right-from-r23
		:precondition
			(and 
				(in r23)
			)
		:effect
			(and
				(not (search_again))
				(not (in r23))
				(in r24)
			)
	)


	(:action move-left-from-r23
		:precondition
			(and 
				(in r23)
			)
		:effect
			(and
				(not (search_again))
				(not (in r23))
				(in r22)
			)
	)

	(:action move-right-from-r24
		:precondition
			(and 
				(in r24)
			)
		:effect
			(and
				(not (search_again))
				(not (in r24))
				(in r25)
			)
	)


	(:action move-left-from-r24
		:precondition
			(and 
				(in r24)
			)
		:effect
			(and
				(not (search_again))
				(not (in r24))
				(in r23)
			)
	)

	(:action move-right-from-r25
		:precondition
			(and 
				(in r25)
			)
		:effect
			(and
				(not (search_again))
				(not (in r25))
				(in r26)
			)
	)


	(:action move-left-from-r25
		:precondition
			(and 
				(in r25)
			)
		:effect
			(and
				(not (search_again))
				(not (in r25))
				(in r24)
			)
	)

	(:action move-right-from-r26
		:precondition
			(and 
				(in r26)
			)
		:effect
			(and
				(not (search_again))
				(not (in r26))
				(in r27)
			)
	)


	(:action move-left-from-r26
		:precondition
			(and 
				(in r26)
			)
		:effect
			(and
				(not (search_again))
				(not (in r26))
				(in r25)
			)
	)

	(:action move-right-from-r27
		:precondition
			(and 
				(in r27)
			)
		:effect
			(and
				(not (search_again))
				(not (in r27))
				(in r28)
			)
	)


	(:action move-left-from-r27
		:precondition
			(and 
				(in r27)
			)
		:effect
			(and
				(not (search_again))
				(not (in r27))
				(in r26)
			)
	)

	(:action move-right-from-r28
		:precondition
			(and 
				(in r28)
			)
		:effect
			(and
				(not (search_again))
				(not (in r28))
				(in r29)
			)
	)


	(:action move-left-from-r28
		:precondition
			(and 
				(in r28)
			)
		:effect
			(and
				(not (search_again))
				(not (in r28))
				(in r27)
			)
	)

	(:action move-right-from-r29
		:precondition
			(and 
				(in r29)
			)
		:effect
			(and
				(not (search_again))
				(not (in r29))
				(in r30)
			)
	)


	(:action move-left-from-r29
		:precondition
			(and 
				(in r29)
			)
		:effect
			(and
				(not (search_again))
				(not (in r29))
				(in r28)
			)
	)

	(:action move-right-from-r30
		:precondition
			(and 
				(in r30)
			)
		:effect
			(and
				(not (search_again))
				(not (in r30))
				(in r31)
			)
	)


	(:action move-left-from-r30
		:precondition
			(and 
				(in r30)
			)
		:effect
			(and
				(not (search_again))
				(not (in r30))
				(in r29)
			)
	)

	(:action move-right-from-r31
		:precondition
			(and 
				(in r31)
			)
		:effect
			(and
				(not (search_again))
				(not (in r31))
				(in r32)
			)
	)


	(:action move-left-from-r31
		:precondition
			(and 
				(in r31)
			)
		:effect
			(and
				(not (search_again))
				(not (in r31))
				(in r30)
			)
	)

	(:action move-right-from-r32
		:precondition
			(and 
				(in r32)
			)
		:effect
			(and
				(not (search_again))
				(not (in r32))
				(in r33)
			)
	)


	(:action move-left-from-r32
		:precondition
			(and 
				(in r32)
			)
		:effect
			(and
				(not (search_again))
				(not (in r32))
				(in r31)
			)
	)

	(:action move-right-from-r33
		:precondition
			(and 
				(in r33)
			)
		:effect
			(and
				(not (search_again))
				(not (in r33))
				(in r34)
			)
	)


	(:action move-left-from-r33
		:precondition
			(and 
				(in r33)
			)
		:effect
			(and
				(not (search_again))
				(not (in r33))
				(in r32)
			)
	)

	(:action move-right-from-r34
		:precondition
			(and 
				(in r34)
			)
		:effect
			(and
				(not (search_again))
				(not (in r34))
				(in r35)
			)
	)


	(:action move-left-from-r34
		:precondition
			(and 
				(in r34)
			)
		:effect
			(and
				(not (search_again))
				(not (in r34))
				(in r33)
			)
	)

	(:action move-right-from-r35
		:precondition
			(and 
				(in r35)
			)
		:effect
			(and
				(not (search_again))
				(not (in r35))
				(in r36)
			)
	)


	(:action move-left-from-r35
		:precondition
			(and 
				(in r35)
			)
		:effect
			(and
				(not (search_again))
				(not (in r35))
				(in r34)
			)
	)

	(:action move-right-from-r36
		:precondition
			(and 
				(in r36)
			)
		:effect
			(and
				(not (search_again))
				(not (in r36))
				(in r37)
			)
	)


	(:action move-left-from-r36
		:precondition
			(and 
				(in r36)
			)
		:effect
			(and
				(not (search_again))
				(not (in r36))
				(in r35)
			)
	)

	(:action move-right-from-r37
		:precondition
			(and 
				(in r37)
			)
		:effect
			(and
				(not (search_again))
				(not (in r37))
				(in r38)
			)
	)


	(:action move-left-from-r37
		:precondition
			(and 
				(in r37)
			)
		:effect
			(and
				(not (search_again))
				(not (in r37))
				(in r36)
			)
	)

	(:action move-right-from-r38
		:precondition
			(and 
				(in r38)
			)
		:effect
			(and
				(not (search_again))
				(not (in r38))
				(in r39)
			)
	)


	(:action move-left-from-r38
		:precondition
			(and 
				(in r38)
			)
		:effect
			(and
				(not (search_again))
				(not (in r38))
				(in r37)
			)
	)

	(:action move-right-from-r39
		:precondition
			(and 
				(in r39)
			)
		:effect
			(and
				(not (search_again))
				(not (in r39))
				(in r40)
			)
	)


	(:action move-left-from-r39
		:precondition
			(and 
				(in r39)
			)
		:effect
			(and
				(not (search_again))
				(not (in r39))
				(in r38)
			)
	)

	(:action move-right-from-r40
		:precondition
			(and 
				(in r40)
			)
		:effect
			(and
				(not (search_again))
				(not (in r40))
				(in r41)
			)
	)


	(:action move-left-from-r40
		:precondition
			(and 
				(in r40)
			)
		:effect
			(and
				(not (search_again))
				(not (in r40))
				(in r39)
			)
	)

	(:action move-right-from-r41
		:precondition
			(and 
				(in r41)
			)
		:effect
			(and
				(not (search_again))
				(not (in r41))
				(in r42)
			)
	)


	(:action move-left-from-r41
		:precondition
			(and 
				(in r41)
			)
		:effect
			(and
				(not (search_again))
				(not (in r41))
				(in r40)
			)
	)

	(:action move-right-from-r42
		:precondition
			(and 
				(in r42)
			)
		:effect
			(and
				(not (search_again))
				(not (in r42))
				(in r43)
			)
	)


	(:action move-left-from-r42
		:precondition
			(and 
				(in r42)
			)
		:effect
			(and
				(not (search_again))
				(not (in r42))
				(in r41)
			)
	)

	(:action move-right-from-r43
		:precondition
			(and 
				(in r43)
			)
		:effect
			(and
				(not (search_again))
				(not (in r43))
				(in r44)
			)
	)


	(:action move-left-from-r43
		:precondition
			(and 
				(in r43)
			)
		:effect
			(and
				(not (search_again))
				(not (in r43))
				(in r42)
			)
	)

	(:action move-right-from-r44
		:precondition
			(and 
				(in r44)
			)
		:effect
			(and
				(not (search_again))
				(not (in r44))
				(in r45)
			)
	)


	(:action move-left-from-r44
		:precondition
			(and 
				(in r44)
			)
		:effect
			(and
				(not (search_again))
				(not (in r44))
				(in r43)
			)
	)

	(:action move-right-from-r45
		:precondition
			(and 
				(in r45)
			)
		:effect
			(and
				(not (search_again))
				(not (in r45))
				(in r46)
			)
	)


	(:action move-left-from-r45
		:precondition
			(and 
				(in r45)
			)
		:effect
			(and
				(not (search_again))
				(not (in r45))
				(in r44)
			)
	)

	(:action move-right-from-r46
		:precondition
			(and 
				(in r46)
			)
		:effect
			(and
				(not (search_again))
				(not (in r46))
				(in r47)
			)
	)


	(:action move-left-from-r46
		:precondition
			(and 
				(in r46)
			)
		:effect
			(and
				(not (search_again))
				(not (in r46))
				(in r45)
			)
	)

	(:action move-right-from-r47
		:precondition
			(and 
				(in r47)
			)
		:effect
			(and
				(not (search_again))
				(not (in r47))
				(in r48)
			)
	)


	(:action move-left-from-r47
		:precondition
			(and 
				(in r47)
			)
		:effect
			(and
				(not (search_again))
				(not (in r47))
				(in r46)
			)
	)

	(:action move-right-from-r48
		:precondition
			(and 
				(in r48)
			)
		:effect
			(and
				(not (search_again))
				(not (in r48))
				(in r49)
			)
	)


	(:action move-left-from-r48
		:precondition
			(and 
				(in r48)
			)
		:effect
			(and
				(not (search_again))
				(not (in r48))
				(in r47)
			)
	)

	(:action move-right-from-r49
		:precondition
			(and 
				(in r49)
			)
		:effect
			(and
				(not (search_again))
				(not (in r49))
				(in r50)
			)
	)


	(:action move-left-from-r49
		:precondition
			(and 
				(in r49)
			)
		:effect
			(and
				(not (search_again))
				(not (in r49))
				(in r48)
			)
	)

	(:action move-right-from-r50
		:precondition
			(and 
				(in r50)
			)
		:effect
			(and
				(not (search_again))
				(not (in r50))
				(in r51)
			)
	)


	(:action move-left-from-r50
		:precondition
			(and 
				(in r50)
			)
		:effect
			(and
				(not (search_again))
				(not (in r50))
				(in r49)
			)
	)

	(:action move-right-from-r51
		:precondition
			(and 
				(in r51)
			)
		:effect
			(and
				(not (search_again))
				(not (in r51))
				(in r52)
			)
	)


	(:action move-left-from-r51
		:precondition
			(and 
				(in r51)
			)
		:effect
			(and
				(not (search_again))
				(not (in r51))
				(in r50)
			)
	)

	(:action move-right-from-r52
		:precondition
			(and 
				(in r52)
			)
		:effect
			(and
				(not (search_again))
				(not (in r52))
				(in r53)
			)
	)


	(:action move-left-from-r52
		:precondition
			(and 
				(in r52)
			)
		:effect
			(and
				(not (search_again))
				(not (in r52))
				(in r51)
			)
	)

	(:action move-right-from-r53
		:precondition
			(and 
				(in r53)
			)
		:effect
			(and
				(not (search_again))
				(not (in r53))
				(in r54)
			)
	)


	(:action move-left-from-r53
		:precondition
			(and 
				(in r53)
			)
		:effect
			(and
				(not (search_again))
				(not (in r53))
				(in r52)
			)
	)

	(:action move-right-from-r54
		:precondition
			(and 
				(in r54)
			)
		:effect
			(and
				(not (search_again))
				(not (in r54))
				(in r55)
			)
	)


	(:action move-left-from-r54
		:precondition
			(and 
				(in r54)
			)
		:effect
			(and
				(not (search_again))
				(not (in r54))
				(in r53)
			)
	)

	(:action move-right-from-r55
		:precondition
			(and 
				(in r55)
			)
		:effect
			(and
				(not (search_again))
				(not (in r55))
				(in r56)
			)
	)


	(:action move-left-from-r55
		:precondition
			(and 
				(in r55)
			)
		:effect
			(and
				(not (search_again))
				(not (in r55))
				(in r54)
			)
	)

	(:action move-right-from-r56
		:precondition
			(and 
				(in r56)
			)
		:effect
			(and
				(not (search_again))
				(not (in r56))
				(in r57)
			)
	)


	(:action move-left-from-r56
		:precondition
			(and 
				(in r56)
			)
		:effect
			(and
				(not (search_again))
				(not (in r56))
				(in r55)
			)
	)

	(:action move-right-from-r57
		:precondition
			(and 
				(in r57)
			)
		:effect
			(and
				(not (search_again))
				(not (in r57))
				(in r58)
			)
	)


	(:action move-left-from-r57
		:precondition
			(and 
				(in r57)
			)
		:effect
			(and
				(not (search_again))
				(not (in r57))
				(in r56)
			)
	)

	(:action move-right-from-r58
		:precondition
			(and 
				(in r58)
			)
		:effect
			(and
				(not (search_again))
				(not (in r58))
				(in r59)
			)
	)


	(:action move-left-from-r58
		:precondition
			(and 
				(in r58)
			)
		:effect
			(and
				(not (search_again))
				(not (in r58))
				(in r57)
			)
	)

	(:action move-right-from-r59
		:precondition
			(and 
				(in r59)
			)
		:effect
			(and
				(not (search_again))
				(not (in r59))
				(in r60)
			)
	)


	(:action move-left-from-r59
		:precondition
			(and 
				(in r59)
			)
		:effect
			(and
				(not (search_again))
				(not (in r59))
				(in r58)
			)
	)

	(:action move-right-from-r60
		:precondition
			(and 
				(in r60)
			)
		:effect
			(and
				(not (search_again))
				(not (in r60))
				(in r61)
			)
	)


	(:action move-left-from-r60
		:precondition
			(and 
				(in r60)
			)
		:effect
			(and
				(not (search_again))
				(not (in r60))
				(in r59)
			)
	)

	(:action move-right-from-r61
		:precondition
			(and 
				(in r61)
			)
		:effect
			(and
				(not (search_again))
				(not (in r61))
				(in r62)
			)
	)


	(:action move-left-from-r61
		:precondition
			(and 
				(in r61)
			)
		:effect
			(and
				(not (search_again))
				(not (in r61))
				(in r60)
			)
	)

	(:action move-right-from-r62
		:precondition
			(and 
				(in r62)
			)
		:effect
			(and
				(not (search_again))
				(not (in r62))
				(in r63)
			)
	)


	(:action move-left-from-r62
		:precondition
			(and 
				(in r62)
			)
		:effect
			(and
				(not (search_again))
				(not (in r62))
				(in r61)
			)
	)

	(:action move-right-from-r63
		:precondition
			(and 
				(in r63)
			)
		:effect
			(and
				(not (search_again))
				(not (in r63))
				(in r64)
			)
	)


	(:action move-left-from-r63
		:precondition
			(and 
				(in r63)
			)
		:effect
			(and
				(not (search_again))
				(not (in r63))
				(in r62)
			)
	)

	(:action move-right-from-r64
		:precondition
			(and 
				(in r64)
			)
		:effect
			(and
				(not (search_again))
				(not (in r64))
				(in r65)
			)
	)


	(:action move-left-from-r64
		:precondition
			(and 
				(in r64)
			)
		:effect
			(and
				(not (search_again))
				(not (in r64))
				(in r63)
			)
	)

	(:action move-right-from-r65
		:precondition
			(and 
				(in r65)
			)
		:effect
			(and
				(not (search_again))
				(not (in r65))
				(in r66)
			)
	)


	(:action move-left-from-r65
		:precondition
			(and 
				(in r65)
			)
		:effect
			(and
				(not (search_again))
				(not (in r65))
				(in r64)
			)
	)

	(:action move-right-from-r66
		:precondition
			(and 
				(in r66)
			)
		:effect
			(and
				(not (search_again))
				(not (in r66))
				(in r67)
			)
	)


	(:action move-left-from-r66
		:precondition
			(and 
				(in r66)
			)
		:effect
			(and
				(not (search_again))
				(not (in r66))
				(in r65)
			)
	)

	(:action move-right-from-r67
		:precondition
			(and 
				(in r67)
			)
		:effect
			(and
				(not (search_again))
				(not (in r67))
				(in r68)
			)
	)


	(:action move-left-from-r67
		:precondition
			(and 
				(in r67)
			)
		:effect
			(and
				(not (search_again))
				(not (in r67))
				(in r66)
			)
	)

	(:action move-right-from-r68
		:precondition
			(and 
				(in r68)
			)
		:effect
			(and
				(not (search_again))
				(not (in r68))
				(in r69)
			)
	)


	(:action move-left-from-r68
		:precondition
			(and 
				(in r68)
			)
		:effect
			(and
				(not (search_again))
				(not (in r68))
				(in r67)
			)
	)

	(:action move-right-from-r69
		:precondition
			(and 
				(in r69)
			)
		:effect
			(and
				(not (search_again))
				(not (in r69))
				(in r70)
			)
	)


	(:action move-left-from-r69
		:precondition
			(and 
				(in r69)
			)
		:effect
			(and
				(not (search_again))
				(not (in r69))
				(in r68)
			)
	)

	(:action move-right-from-r70
		:precondition
			(and 
				(in r70)
			)
		:effect
			(and
				(not (search_again))
				(not (in r70))
				(in r71)
			)
	)


	(:action move-left-from-r70
		:precondition
			(and 
				(in r70)
			)
		:effect
			(and
				(not (search_again))
				(not (in r70))
				(in r69)
			)
	)

	(:action move-right-from-r71
		:precondition
			(and 
				(in r71)
			)
		:effect
			(and
				(not (search_again))
				(not (in r71))
				(in r72)
			)
	)


	(:action move-left-from-r71
		:precondition
			(and 
				(in r71)
			)
		:effect
			(and
				(not (search_again))
				(not (in r71))
				(in r70)
			)
	)

	(:action move-right-from-r72
		:precondition
			(and 
				(in r72)
			)
		:effect
			(and
				(not (search_again))
				(not (in r72))
				(in r73)
			)
	)


	(:action move-left-from-r72
		:precondition
			(and 
				(in r72)
			)
		:effect
			(and
				(not (search_again))
				(not (in r72))
				(in r71)
			)
	)

	(:action move-right-from-r73
		:precondition
			(and 
				(in r73)
			)
		:effect
			(and
				(not (search_again))
				(not (in r73))
				(in r74)
			)
	)


	(:action move-left-from-r73
		:precondition
			(and 
				(in r73)
			)
		:effect
			(and
				(not (search_again))
				(not (in r73))
				(in r72)
			)
	)

	(:action move-right-from-r74
		:precondition
			(and 
				(in r74)
			)
		:effect
			(and
				(not (search_again))
				(not (in r74))
				(in r75)
			)
	)


	(:action move-left-from-r74
		:precondition
			(and 
				(in r74)
			)
		:effect
			(and
				(not (search_again))
				(not (in r74))
				(in r73)
			)
	)

	(:action move-right-from-r75
		:precondition
			(and 
				(in r75)
			)
		:effect
			(and
				(not (search_again))
				(not (in r75))
				(in r76)
			)
	)


	(:action move-left-from-r75
		:precondition
			(and 
				(in r75)
			)
		:effect
			(and
				(not (search_again))
				(not (in r75))
				(in r74)
			)
	)

	(:action move-right-from-r76
		:precondition
			(and 
				(in r76)
			)
		:effect
			(and
				(not (search_again))
				(not (in r76))
				(in r77)
			)
	)


	(:action move-left-from-r76
		:precondition
			(and 
				(in r76)
			)
		:effect
			(and
				(not (search_again))
				(not (in r76))
				(in r75)
			)
	)

	(:action move-right-from-r77
		:precondition
			(and 
				(in r77)
			)
		:effect
			(and
				(not (search_again))
				(not (in r77))
				(in r78)
			)
	)


	(:action move-left-from-r77
		:precondition
			(and 
				(in r77)
			)
		:effect
			(and
				(not (search_again))
				(not (in r77))
				(in r76)
			)
	)

	(:action move-right-from-r78
		:precondition
			(and 
				(in r78)
			)
		:effect
			(and
				(not (search_again))
				(not (in r78))
				(in r79)
			)
	)


	(:action move-left-from-r78
		:precondition
			(and 
				(in r78)
			)
		:effect
			(and
				(not (search_again))
				(not (in r78))
				(in r77)
			)
	)

	(:action move-right-from-r79
		:precondition
			(and 
				(in r79)
			)
		:effect
			(and
				(not (search_again))
				(not (in r79))
				(in r80)
			)
	)


	(:action move-left-from-r79
		:precondition
			(and 
				(in r79)
			)
		:effect
			(and
				(not (search_again))
				(not (in r79))
				(in r78)
			)
	)

	(:action move-right-from-r80
		:precondition
			(and 
				(in r80)
			)
		:effect
			(and
				(not (search_again))
				(not (in r80))
				(in r81)
			)
	)


	(:action move-left-from-r80
		:precondition
			(and 
				(in r80)
			)
		:effect
			(and
				(not (search_again))
				(not (in r80))
				(in r79)
			)
	)

	(:action move-right-from-r81
		:precondition
			(and 
				(in r81)
			)
		:effect
			(and
				(not (search_again))
				(not (in r81))
				(in r82)
			)
	)


	(:action move-left-from-r81
		:precondition
			(and 
				(in r81)
			)
		:effect
			(and
				(not (search_again))
				(not (in r81))
				(in r80)
			)
	)

	(:action move-right-from-r82
		:precondition
			(and 
				(in r82)
			)
		:effect
			(and
				(not (search_again))
				(not (in r82))
				(in r83)
			)
	)


	(:action move-left-from-r82
		:precondition
			(and 
				(in r82)
			)
		:effect
			(and
				(not (search_again))
				(not (in r82))
				(in r81)
			)
	)

	(:action move-right-from-r83
		:precondition
			(and 
				(in r83)
			)
		:effect
			(and
				(not (search_again))
				(not (in r83))
				(in r84)
			)
	)


	(:action move-left-from-r83
		:precondition
			(and 
				(in r83)
			)
		:effect
			(and
				(not (search_again))
				(not (in r83))
				(in r82)
			)
	)

	(:action move-right-from-r84
		:precondition
			(and 
				(in r84)
			)
		:effect
			(and
				(not (search_again))
				(not (in r84))
				(in r85)
			)
	)


	(:action move-left-from-r84
		:precondition
			(and 
				(in r84)
			)
		:effect
			(and
				(not (search_again))
				(not (in r84))
				(in r83)
			)
	)

	(:action move-right-from-r85
		:precondition
			(and 
				(in r85)
			)
		:effect
			(and
				(not (search_again))
				(not (in r85))
				(in r86)
			)
	)


	(:action move-left-from-r85
		:precondition
			(and 
				(in r85)
			)
		:effect
			(and
				(not (search_again))
				(not (in r85))
				(in r84)
			)
	)

	(:action move-right-from-r86
		:precondition
			(and 
				(in r86)
			)
		:effect
			(and
				(not (search_again))
				(not (in r86))
				(in r87)
			)
	)


	(:action move-left-from-r86
		:precondition
			(and 
				(in r86)
			)
		:effect
			(and
				(not (search_again))
				(not (in r86))
				(in r85)
			)
	)

	(:action move-right-from-r87
		:precondition
			(and 
				(in r87)
			)
		:effect
			(and
				(not (search_again))
				(not (in r87))
				(in r88)
			)
	)


	(:action move-left-from-r87
		:precondition
			(and 
				(in r87)
			)
		:effect
			(and
				(not (search_again))
				(not (in r87))
				(in r86)
			)
	)

	(:action move-right-from-r88
		:precondition
			(and 
				(in r88)
			)
		:effect
			(and
				(not (search_again))
				(not (in r88))
				(in r89)
			)
	)


	(:action move-left-from-r88
		:precondition
			(and 
				(in r88)
			)
		:effect
			(and
				(not (search_again))
				(not (in r88))
				(in r87)
			)
	)

	(:action move-right-from-r89
		:precondition
			(and 
				(in r89)
			)
		:effect
			(and
				(not (search_again))
				(not (in r89))
				(in r90)
			)
	)


	(:action move-left-from-r89
		:precondition
			(and 
				(in r89)
			)
		:effect
			(and
				(not (search_again))
				(not (in r89))
				(in r88)
			)
	)

	(:action move-right-from-r90
		:precondition
			(and 
				(in r90)
			)
		:effect
			(and
				(not (search_again))
				(not (in r90))
				(in r91)
			)
	)


	(:action move-left-from-r90
		:precondition
			(and 
				(in r90)
			)
		:effect
			(and
				(not (search_again))
				(not (in r90))
				(in r89)
			)
	)

	(:action move-right-from-r91
		:precondition
			(and 
				(in r91)
			)
		:effect
			(and
				(not (search_again))
				(not (in r91))
				(in r92)
			)
	)


	(:action move-left-from-r91
		:precondition
			(and 
				(in r91)
			)
		:effect
			(and
				(not (search_again))
				(not (in r91))
				(in r90)
			)
	)

	(:action move-right-from-r92
		:precondition
			(and 
				(in r92)
			)
		:effect
			(and
				(not (search_again))
				(not (in r92))
				(in r93)
			)
	)


	(:action move-left-from-r92
		:precondition
			(and 
				(in r92)
			)
		:effect
			(and
				(not (search_again))
				(not (in r92))
				(in r91)
			)
	)

	(:action move-right-from-r93
		:precondition
			(and 
				(in r93)
			)
		:effect
			(and
				(not (search_again))
				(not (in r93))
				(in r94)
			)
	)


	(:action move-left-from-r93
		:precondition
			(and 
				(in r93)
			)
		:effect
			(and
				(not (search_again))
				(not (in r93))
				(in r92)
			)
	)

	(:action move-right-from-r94
		:precondition
			(and 
				(in r94)
			)
		:effect
			(and
				(not (search_again))
				(not (in r94))
				(in r95)
			)
	)


	(:action move-left-from-r94
		:precondition
			(and 
				(in r94)
			)
		:effect
			(and
				(not (search_again))
				(not (in r94))
				(in r93)
			)
	)

	(:action move-right-from-r95
		:precondition
			(and 
				(in r95)
			)
		:effect
			(and
				(not (search_again))
				(not (in r95))
				(in r96)
			)
	)


	(:action move-left-from-r95
		:precondition
			(and 
				(in r95)
			)
		:effect
			(and
				(not (search_again))
				(not (in r95))
				(in r94)
			)
	)

	(:action move-right-from-r96
		:precondition
			(and 
				(in r96)
			)
		:effect
			(and
				(not (search_again))
				(not (in r96))
				(in r97)
			)
	)


	(:action move-left-from-r96
		:precondition
			(and 
				(in r96)
			)
		:effect
			(and
				(not (search_again))
				(not (in r96))
				(in r95)
			)
	)

	(:action move-right-from-r97
		:precondition
			(and 
				(in r97)
			)
		:effect
			(and
				(not (search_again))
				(not (in r97))
				(in r98)
			)
	)


	(:action move-left-from-r97
		:precondition
			(and 
				(in r97)
			)
		:effect
			(and
				(not (search_again))
				(not (in r97))
				(in r96)
			)
	)

	(:action move-right-from-r98
		:precondition
			(and 
				(in r98)
			)
		:effect
			(and
				(not (search_again))
				(not (in r98))
				(in r99)
			)
	)


	(:action move-left-from-r98
		:precondition
			(and 
				(in r98)
			)
		:effect
			(and
				(not (search_again))
				(not (in r98))
				(in r97)
			)
	)

	(:action move-right-from-r99
		:precondition
			(and 
				(in r99)
				(not (searched_in r100))
			)
		:effect
			(and
				(not (search_again))
				(not (in r99))
				(in r100)
				(oneof (not (seen)) (seen))
				(searched_in r100)
			)
	)


	(:action move-left-from-r99
		:precondition
			(and 
				(in r99)
			)
		:effect
			(and
				(not (search_again))
				(not (in r99))
				(in r98)
			)
	)

	(:action move-right-from-r100
		:precondition
			(and 
				(in r100)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r100))
				(in r101)
			)
	)


	(:action move-left-from-r100
		:precondition
			(and 
				(in r100)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r100))
				(in r99)
			)
	)

	(:action move-right-from-r101
		:precondition
			(and 
				(in r101)
			)
		:effect
			(and
				(not (search_again))
				(not (in r101))
				(in r102)
			)
	)


	(:action move-left-from-r101
		:precondition
			(and 
				(in r101)
				(not (searched_in r100))
			)
		:effect
			(and
				(not (search_again))
				(not (in r101))
				(in r100)
				(oneof (not (seen)) (seen))
				(searched_in r100)
			)
	)

	(:action move-right-from-r102
		:precondition
			(and 
				(in r102)
			)
		:effect
			(and
				(not (search_again))
				(not (in r102))
				(in r103)
			)
	)


	(:action move-left-from-r102
		:precondition
			(and 
				(in r102)
			)
		:effect
			(and
				(not (search_again))
				(not (in r102))
				(in r101)
			)
	)

	(:action move-right-from-r103
		:precondition
			(and 
				(in r103)
			)
		:effect
			(and
				(not (search_again))
				(not (in r103))
				(in r104)
			)
	)


	(:action move-left-from-r103
		:precondition
			(and 
				(in r103)
			)
		:effect
			(and
				(not (search_again))
				(not (in r103))
				(in r102)
			)
	)

	(:action move-right-from-r104
		:precondition
			(and 
				(in r104)
			)
		:effect
			(and
				(not (search_again))
				(not (in r104))
				(in r105)
			)
	)


	(:action move-left-from-r104
		:precondition
			(and 
				(in r104)
			)
		:effect
			(and
				(not (search_again))
				(not (in r104))
				(in r103)
			)
	)

	(:action move-right-from-r105
		:precondition
			(and 
				(in r105)
			)
		:effect
			(and
				(not (search_again))
				(not (in r105))
				(in r106)
			)
	)


	(:action move-left-from-r105
		:precondition
			(and 
				(in r105)
			)
		:effect
			(and
				(not (search_again))
				(not (in r105))
				(in r104)
			)
	)

	(:action move-right-from-r106
		:precondition
			(and 
				(in r106)
			)
		:effect
			(and
				(not (search_again))
				(not (in r106))
				(in r107)
			)
	)


	(:action move-left-from-r106
		:precondition
			(and 
				(in r106)
			)
		:effect
			(and
				(not (search_again))
				(not (in r106))
				(in r105)
			)
	)

	(:action move-right-from-r107
		:precondition
			(and 
				(in r107)
			)
		:effect
			(and
				(not (search_again))
				(not (in r107))
				(in r108)
			)
	)


	(:action move-left-from-r107
		:precondition
			(and 
				(in r107)
			)
		:effect
			(and
				(not (search_again))
				(not (in r107))
				(in r106)
			)
	)

	(:action move-right-from-r108
		:precondition
			(and 
				(in r108)
			)
		:effect
			(and
				(not (search_again))
				(not (in r108))
				(in r109)
			)
	)


	(:action move-left-from-r108
		:precondition
			(and 
				(in r108)
			)
		:effect
			(and
				(not (search_again))
				(not (in r108))
				(in r107)
			)
	)

	(:action move-right-from-r109
		:precondition
			(and 
				(in r109)
			)
		:effect
			(and
				(not (search_again))
				(not (in r109))
				(in r110)
			)
	)


	(:action move-left-from-r109
		:precondition
			(and 
				(in r109)
			)
		:effect
			(and
				(not (search_again))
				(not (in r109))
				(in r108)
			)
	)

	(:action move-right-from-r110
		:precondition
			(and 
				(in r110)
			)
		:effect
			(and
				(not (search_again))
				(not (in r110))
				(in r111)
			)
	)


	(:action move-left-from-r110
		:precondition
			(and 
				(in r110)
			)
		:effect
			(and
				(not (search_again))
				(not (in r110))
				(in r109)
			)
	)

	(:action move-right-from-r111
		:precondition
			(and 
				(in r111)
			)
		:effect
			(and
				(not (search_again))
				(not (in r111))
				(in r112)
			)
	)


	(:action move-left-from-r111
		:precondition
			(and 
				(in r111)
			)
		:effect
			(and
				(not (search_again))
				(not (in r111))
				(in r110)
			)
	)

	(:action move-right-from-r112
		:precondition
			(and 
				(in r112)
			)
		:effect
			(and
				(not (search_again))
				(not (in r112))
				(in r113)
			)
	)


	(:action move-left-from-r112
		:precondition
			(and 
				(in r112)
			)
		:effect
			(and
				(not (search_again))
				(not (in r112))
				(in r111)
			)
	)

	(:action move-right-from-r113
		:precondition
			(and 
				(in r113)
			)
		:effect
			(and
				(not (search_again))
				(not (in r113))
				(in r114)
			)
	)


	(:action move-left-from-r113
		:precondition
			(and 
				(in r113)
			)
		:effect
			(and
				(not (search_again))
				(not (in r113))
				(in r112)
			)
	)

	(:action move-right-from-r114
		:precondition
			(and 
				(in r114)
			)
		:effect
			(and
				(not (search_again))
				(not (in r114))
				(in r115)
			)
	)


	(:action move-left-from-r114
		:precondition
			(and 
				(in r114)
			)
		:effect
			(and
				(not (search_again))
				(not (in r114))
				(in r113)
			)
	)

	(:action move-right-from-r115
		:precondition
			(and 
				(in r115)
			)
		:effect
			(and
				(not (search_again))
				(not (in r115))
				(in r116)
			)
	)


	(:action move-left-from-r115
		:precondition
			(and 
				(in r115)
			)
		:effect
			(and
				(not (search_again))
				(not (in r115))
				(in r114)
			)
	)

	(:action move-right-from-r116
		:precondition
			(and 
				(in r116)
			)
		:effect
			(and
				(not (search_again))
				(not (in r116))
				(in r117)
			)
	)


	(:action move-left-from-r116
		:precondition
			(and 
				(in r116)
			)
		:effect
			(and
				(not (search_again))
				(not (in r116))
				(in r115)
			)
	)

	(:action move-right-from-r117
		:precondition
			(and 
				(in r117)
			)
		:effect
			(and
				(not (search_again))
				(not (in r117))
				(in r118)
			)
	)


	(:action move-left-from-r117
		:precondition
			(and 
				(in r117)
			)
		:effect
			(and
				(not (search_again))
				(not (in r117))
				(in r116)
			)
	)

	(:action move-right-from-r118
		:precondition
			(and 
				(in r118)
			)
		:effect
			(and
				(not (search_again))
				(not (in r118))
				(in r119)
			)
	)


	(:action move-left-from-r118
		:precondition
			(and 
				(in r118)
			)
		:effect
			(and
				(not (search_again))
				(not (in r118))
				(in r117)
			)
	)

	(:action move-right-from-r119
		:precondition
			(and 
				(in r119)
			)
		:effect
			(and
				(not (search_again))
				(not (in r119))
				(in r120)
			)
	)


	(:action move-left-from-r119
		:precondition
			(and 
				(in r119)
			)
		:effect
			(and
				(not (search_again))
				(not (in r119))
				(in r118)
			)
	)

	(:action move-right-from-r120
		:precondition
			(and 
				(in r120)
			)
		:effect
			(and
				(not (search_again))
				(not (in r120))
				(in r121)
			)
	)


	(:action move-left-from-r120
		:precondition
			(and 
				(in r120)
			)
		:effect
			(and
				(not (search_again))
				(not (in r120))
				(in r119)
			)
	)

	(:action move-right-from-r121
		:precondition
			(and 
				(in r121)
			)
		:effect
			(and
				(not (search_again))
				(not (in r121))
				(in r122)
			)
	)


	(:action move-left-from-r121
		:precondition
			(and 
				(in r121)
			)
		:effect
			(and
				(not (search_again))
				(not (in r121))
				(in r120)
			)
	)

	(:action move-right-from-r122
		:precondition
			(and 
				(in r122)
			)
		:effect
			(and
				(not (search_again))
				(not (in r122))
				(in r123)
			)
	)


	(:action move-left-from-r122
		:precondition
			(and 
				(in r122)
			)
		:effect
			(and
				(not (search_again))
				(not (in r122))
				(in r121)
			)
	)

	(:action move-right-from-r123
		:precondition
			(and 
				(in r123)
			)
		:effect
			(and
				(not (search_again))
				(not (in r123))
				(in r124)
			)
	)


	(:action move-left-from-r123
		:precondition
			(and 
				(in r123)
			)
		:effect
			(and
				(not (search_again))
				(not (in r123))
				(in r122)
			)
	)

	(:action move-right-from-r124
		:precondition
			(and 
				(in r124)
			)
		:effect
			(and
				(not (search_again))
				(not (in r124))
				(in r125)
			)
	)


	(:action move-left-from-r124
		:precondition
			(and 
				(in r124)
			)
		:effect
			(and
				(not (search_again))
				(not (in r124))
				(in r123)
			)
	)

	(:action move-right-from-r125
		:precondition
			(and 
				(in r125)
			)
		:effect
			(and
				(not (search_again))
				(not (in r125))
				(in r126)
			)
	)


	(:action move-left-from-r125
		:precondition
			(and 
				(in r125)
			)
		:effect
			(and
				(not (search_again))
				(not (in r125))
				(in r124)
			)
	)

	(:action move-right-from-r126
		:precondition
			(and 
				(in r126)
			)
		:effect
			(and
				(not (search_again))
				(not (in r126))
				(in r127)
			)
	)


	(:action move-left-from-r126
		:precondition
			(and 
				(in r126)
			)
		:effect
			(and
				(not (search_again))
				(not (in r126))
				(in r125)
			)
	)

	(:action move-right-from-r127
		:precondition
			(and 
				(in r127)
			)
		:effect
			(and
				(not (search_again))
				(not (in r127))
				(in r128)
			)
	)


	(:action move-left-from-r127
		:precondition
			(and 
				(in r127)
			)
		:effect
			(and
				(not (search_again))
				(not (in r127))
				(in r126)
			)
	)

	(:action move-right-from-r128
		:precondition
			(and 
				(in r128)
			)
		:effect
			(and
				(not (search_again))
				(not (in r128))
				(in r129)
			)
	)


	(:action move-left-from-r128
		:precondition
			(and 
				(in r128)
			)
		:effect
			(and
				(not (search_again))
				(not (in r128))
				(in r127)
			)
	)

	(:action move-right-from-r129
		:precondition
			(and 
				(in r129)
			)
		:effect
			(and
				(not (search_again))
				(not (in r129))
				(in r130)
			)
	)


	(:action move-left-from-r129
		:precondition
			(and 
				(in r129)
			)
		:effect
			(and
				(not (search_again))
				(not (in r129))
				(in r128)
			)
	)

	(:action move-right-from-r130
		:precondition
			(and 
				(in r130)
			)
		:effect
			(and
				(not (search_again))
				(not (in r130))
				(in r131)
			)
	)


	(:action move-left-from-r130
		:precondition
			(and 
				(in r130)
			)
		:effect
			(and
				(not (search_again))
				(not (in r130))
				(in r129)
			)
	)

	(:action move-right-from-r131
		:precondition
			(and 
				(in r131)
			)
		:effect
			(and
				(not (search_again))
				(not (in r131))
				(in r132)
			)
	)


	(:action move-left-from-r131
		:precondition
			(and 
				(in r131)
			)
		:effect
			(and
				(not (search_again))
				(not (in r131))
				(in r130)
			)
	)

	(:action move-right-from-r132
		:precondition
			(and 
				(in r132)
			)
		:effect
			(and
				(not (search_again))
				(not (in r132))
				(in r133)
			)
	)


	(:action move-left-from-r132
		:precondition
			(and 
				(in r132)
			)
		:effect
			(and
				(not (search_again))
				(not (in r132))
				(in r131)
			)
	)

	(:action move-right-from-r133
		:precondition
			(and 
				(in r133)
			)
		:effect
			(and
				(not (search_again))
				(not (in r133))
				(in r134)
			)
	)


	(:action move-left-from-r133
		:precondition
			(and 
				(in r133)
			)
		:effect
			(and
				(not (search_again))
				(not (in r133))
				(in r132)
			)
	)

	(:action move-right-from-r134
		:precondition
			(and 
				(in r134)
			)
		:effect
			(and
				(not (search_again))
				(not (in r134))
				(in r135)
			)
	)


	(:action move-left-from-r134
		:precondition
			(and 
				(in r134)
			)
		:effect
			(and
				(not (search_again))
				(not (in r134))
				(in r133)
			)
	)

	(:action move-right-from-r135
		:precondition
			(and 
				(in r135)
			)
		:effect
			(and
				(not (search_again))
				(not (in r135))
				(in r136)
			)
	)


	(:action move-left-from-r135
		:precondition
			(and 
				(in r135)
			)
		:effect
			(and
				(not (search_again))
				(not (in r135))
				(in r134)
			)
	)

	(:action move-right-from-r136
		:precondition
			(and 
				(in r136)
			)
		:effect
			(and
				(not (search_again))
				(not (in r136))
				(in r137)
			)
	)


	(:action move-left-from-r136
		:precondition
			(and 
				(in r136)
			)
		:effect
			(and
				(not (search_again))
				(not (in r136))
				(in r135)
			)
	)

	(:action move-right-from-r137
		:precondition
			(and 
				(in r137)
			)
		:effect
			(and
				(not (search_again))
				(not (in r137))
				(in r138)
			)
	)


	(:action move-left-from-r137
		:precondition
			(and 
				(in r137)
			)
		:effect
			(and
				(not (search_again))
				(not (in r137))
				(in r136)
			)
	)

	(:action move-right-from-r138
		:precondition
			(and 
				(in r138)
			)
		:effect
			(and
				(not (search_again))
				(not (in r138))
				(in r139)
			)
	)


	(:action move-left-from-r138
		:precondition
			(and 
				(in r138)
			)
		:effect
			(and
				(not (search_again))
				(not (in r138))
				(in r137)
			)
	)

	(:action move-right-from-r139
		:precondition
			(and 
				(in r139)
			)
		:effect
			(and
				(not (search_again))
				(not (in r139))
				(in r140)
			)
	)


	(:action move-left-from-r139
		:precondition
			(and 
				(in r139)
			)
		:effect
			(and
				(not (search_again))
				(not (in r139))
				(in r138)
			)
	)

	(:action move-right-from-r140
		:precondition
			(and 
				(in r140)
			)
		:effect
			(and
				(not (search_again))
				(not (in r140))
				(in r141)
			)
	)


	(:action move-left-from-r140
		:precondition
			(and 
				(in r140)
			)
		:effect
			(and
				(not (search_again))
				(not (in r140))
				(in r139)
			)
	)

	(:action move-right-from-r141
		:precondition
			(and 
				(in r141)
			)
		:effect
			(and
				(not (search_again))
				(not (in r141))
				(in r142)
			)
	)


	(:action move-left-from-r141
		:precondition
			(and 
				(in r141)
			)
		:effect
			(and
				(not (search_again))
				(not (in r141))
				(in r140)
			)
	)

	(:action move-right-from-r142
		:precondition
			(and 
				(in r142)
			)
		:effect
			(and
				(not (search_again))
				(not (in r142))
				(in r143)
			)
	)


	(:action move-left-from-r142
		:precondition
			(and 
				(in r142)
			)
		:effect
			(and
				(not (search_again))
				(not (in r142))
				(in r141)
			)
	)

	(:action move-right-from-r143
		:precondition
			(and 
				(in r143)
			)
		:effect
			(and
				(not (search_again))
				(not (in r143))
				(in r144)
			)
	)


	(:action move-left-from-r143
		:precondition
			(and 
				(in r143)
			)
		:effect
			(and
				(not (search_again))
				(not (in r143))
				(in r142)
			)
	)

	(:action move-right-from-r144
		:precondition
			(and 
				(in r144)
			)
		:effect
			(and
				(not (search_again))
				(not (in r144))
				(in r145)
			)
	)


	(:action move-left-from-r144
		:precondition
			(and 
				(in r144)
			)
		:effect
			(and
				(not (search_again))
				(not (in r144))
				(in r143)
			)
	)

	(:action move-right-from-r145
		:precondition
			(and 
				(in r145)
			)
		:effect
			(and
				(not (search_again))
				(not (in r145))
				(in r146)
			)
	)


	(:action move-left-from-r145
		:precondition
			(and 
				(in r145)
			)
		:effect
			(and
				(not (search_again))
				(not (in r145))
				(in r144)
			)
	)

	(:action move-right-from-r146
		:precondition
			(and 
				(in r146)
			)
		:effect
			(and
				(not (search_again))
				(not (in r146))
				(in r147)
			)
	)


	(:action move-left-from-r146
		:precondition
			(and 
				(in r146)
			)
		:effect
			(and
				(not (search_again))
				(not (in r146))
				(in r145)
			)
	)

	(:action move-right-from-r147
		:precondition
			(and 
				(in r147)
			)
		:effect
			(and
				(not (search_again))
				(not (in r147))
				(in r148)
			)
	)


	(:action move-left-from-r147
		:precondition
			(and 
				(in r147)
			)
		:effect
			(and
				(not (search_again))
				(not (in r147))
				(in r146)
			)
	)

	(:action move-right-from-r148
		:precondition
			(and 
				(in r148)
			)
		:effect
			(and
				(not (search_again))
				(not (in r148))
				(in r149)
			)
	)


	(:action move-left-from-r148
		:precondition
			(and 
				(in r148)
			)
		:effect
			(and
				(not (search_again))
				(not (in r148))
				(in r147)
			)
	)

	(:action move-right-from-r149
		:precondition
			(and 
				(in r149)
			)
		:effect
			(and
				(not (search_again))
				(not (in r149))
				(in r150)
			)
	)


	(:action move-left-from-r149
		:precondition
			(and 
				(in r149)
			)
		:effect
			(and
				(not (search_again))
				(not (in r149))
				(in r148)
			)
	)

	(:action move-right-from-r150
		:precondition
			(and 
				(in r150)
			)
		:effect
			(and
				(not (search_again))
				(not (in r150))
				(in r151)
			)
	)


	(:action move-left-from-r150
		:precondition
			(and 
				(in r150)
			)
		:effect
			(and
				(not (search_again))
				(not (in r150))
				(in r149)
			)
	)

	(:action move-right-from-r151
		:precondition
			(and 
				(in r151)
			)
		:effect
			(and
				(not (search_again))
				(not (in r151))
				(in r152)
			)
	)


	(:action move-left-from-r151
		:precondition
			(and 
				(in r151)
			)
		:effect
			(and
				(not (search_again))
				(not (in r151))
				(in r150)
			)
	)

	(:action move-right-from-r152
		:precondition
			(and 
				(in r152)
			)
		:effect
			(and
				(not (search_again))
				(not (in r152))
				(in r153)
			)
	)


	(:action move-left-from-r152
		:precondition
			(and 
				(in r152)
			)
		:effect
			(and
				(not (search_again))
				(not (in r152))
				(in r151)
			)
	)

	(:action move-right-from-r153
		:precondition
			(and 
				(in r153)
			)
		:effect
			(and
				(not (search_again))
				(not (in r153))
				(in r154)
			)
	)


	(:action move-left-from-r153
		:precondition
			(and 
				(in r153)
			)
		:effect
			(and
				(not (search_again))
				(not (in r153))
				(in r152)
			)
	)

	(:action move-right-from-r154
		:precondition
			(and 
				(in r154)
			)
		:effect
			(and
				(not (search_again))
				(not (in r154))
				(in r155)
			)
	)


	(:action move-left-from-r154
		:precondition
			(and 
				(in r154)
			)
		:effect
			(and
				(not (search_again))
				(not (in r154))
				(in r153)
			)
	)

	(:action move-right-from-r155
		:precondition
			(and 
				(in r155)
			)
		:effect
			(and
				(not (search_again))
				(not (in r155))
				(in r156)
			)
	)


	(:action move-left-from-r155
		:precondition
			(and 
				(in r155)
			)
		:effect
			(and
				(not (search_again))
				(not (in r155))
				(in r154)
			)
	)

	(:action move-right-from-r156
		:precondition
			(and 
				(in r156)
			)
		:effect
			(and
				(not (search_again))
				(not (in r156))
				(in r157)
			)
	)


	(:action move-left-from-r156
		:precondition
			(and 
				(in r156)
			)
		:effect
			(and
				(not (search_again))
				(not (in r156))
				(in r155)
			)
	)

	(:action move-right-from-r157
		:precondition
			(and 
				(in r157)
			)
		:effect
			(and
				(not (search_again))
				(not (in r157))
				(in r158)
			)
	)


	(:action move-left-from-r157
		:precondition
			(and 
				(in r157)
			)
		:effect
			(and
				(not (search_again))
				(not (in r157))
				(in r156)
			)
	)

	(:action move-right-from-r158
		:precondition
			(and 
				(in r158)
			)
		:effect
			(and
				(not (search_again))
				(not (in r158))
				(in r159)
			)
	)


	(:action move-left-from-r158
		:precondition
			(and 
				(in r158)
			)
		:effect
			(and
				(not (search_again))
				(not (in r158))
				(in r157)
			)
	)

	(:action move-right-from-r159
		:precondition
			(and 
				(in r159)
			)
		:effect
			(and
				(not (search_again))
				(not (in r159))
				(in r160)
			)
	)


	(:action move-left-from-r159
		:precondition
			(and 
				(in r159)
			)
		:effect
			(and
				(not (search_again))
				(not (in r159))
				(in r158)
			)
	)

	(:action move-right-from-r160
		:precondition
			(and 
				(in r160)
			)
		:effect
			(and
				(not (search_again))
				(not (in r160))
				(in r161)
			)
	)


	(:action move-left-from-r160
		:precondition
			(and 
				(in r160)
			)
		:effect
			(and
				(not (search_again))
				(not (in r160))
				(in r159)
			)
	)

	(:action move-right-from-r161
		:precondition
			(and 
				(in r161)
			)
		:effect
			(and
				(not (search_again))
				(not (in r161))
				(in r162)
			)
	)


	(:action move-left-from-r161
		:precondition
			(and 
				(in r161)
			)
		:effect
			(and
				(not (search_again))
				(not (in r161))
				(in r160)
			)
	)

	(:action move-right-from-r162
		:precondition
			(and 
				(in r162)
			)
		:effect
			(and
				(not (search_again))
				(not (in r162))
				(in r163)
			)
	)


	(:action move-left-from-r162
		:precondition
			(and 
				(in r162)
			)
		:effect
			(and
				(not (search_again))
				(not (in r162))
				(in r161)
			)
	)

	(:action move-right-from-r163
		:precondition
			(and 
				(in r163)
			)
		:effect
			(and
				(not (search_again))
				(not (in r163))
				(in r164)
			)
	)


	(:action move-left-from-r163
		:precondition
			(and 
				(in r163)
			)
		:effect
			(and
				(not (search_again))
				(not (in r163))
				(in r162)
			)
	)

	(:action move-right-from-r164
		:precondition
			(and 
				(in r164)
			)
		:effect
			(and
				(not (search_again))
				(not (in r164))
				(in r165)
			)
	)


	(:action move-left-from-r164
		:precondition
			(and 
				(in r164)
			)
		:effect
			(and
				(not (search_again))
				(not (in r164))
				(in r163)
			)
	)

	(:action move-right-from-r165
		:precondition
			(and 
				(in r165)
			)
		:effect
			(and
				(not (search_again))
				(not (in r165))
				(in r166)
			)
	)


	(:action move-left-from-r165
		:precondition
			(and 
				(in r165)
			)
		:effect
			(and
				(not (search_again))
				(not (in r165))
				(in r164)
			)
	)

	(:action move-right-from-r166
		:precondition
			(and 
				(in r166)
			)
		:effect
			(and
				(not (search_again))
				(not (in r166))
				(in r167)
			)
	)


	(:action move-left-from-r166
		:precondition
			(and 
				(in r166)
			)
		:effect
			(and
				(not (search_again))
				(not (in r166))
				(in r165)
			)
	)

	(:action move-right-from-r167
		:precondition
			(and 
				(in r167)
			)
		:effect
			(and
				(not (search_again))
				(not (in r167))
				(in r168)
			)
	)


	(:action move-left-from-r167
		:precondition
			(and 
				(in r167)
			)
		:effect
			(and
				(not (search_again))
				(not (in r167))
				(in r166)
			)
	)

	(:action move-right-from-r168
		:precondition
			(and 
				(in r168)
			)
		:effect
			(and
				(not (search_again))
				(not (in r168))
				(in r169)
			)
	)


	(:action move-left-from-r168
		:precondition
			(and 
				(in r168)
			)
		:effect
			(and
				(not (search_again))
				(not (in r168))
				(in r167)
			)
	)

	(:action move-right-from-r169
		:precondition
			(and 
				(in r169)
			)
		:effect
			(and
				(not (search_again))
				(not (in r169))
				(in r170)
			)
	)


	(:action move-left-from-r169
		:precondition
			(and 
				(in r169)
			)
		:effect
			(and
				(not (search_again))
				(not (in r169))
				(in r168)
			)
	)

	(:action move-right-from-r170
		:precondition
			(and 
				(in r170)
			)
		:effect
			(and
				(not (search_again))
				(not (in r170))
				(in r171)
			)
	)


	(:action move-left-from-r170
		:precondition
			(and 
				(in r170)
			)
		:effect
			(and
				(not (search_again))
				(not (in r170))
				(in r169)
			)
	)

	(:action move-right-from-r171
		:precondition
			(and 
				(in r171)
			)
		:effect
			(and
				(not (search_again))
				(not (in r171))
				(in r172)
			)
	)


	(:action move-left-from-r171
		:precondition
			(and 
				(in r171)
			)
		:effect
			(and
				(not (search_again))
				(not (in r171))
				(in r170)
			)
	)

	(:action move-right-from-r172
		:precondition
			(and 
				(in r172)
			)
		:effect
			(and
				(not (search_again))
				(not (in r172))
				(in r173)
			)
	)


	(:action move-left-from-r172
		:precondition
			(and 
				(in r172)
			)
		:effect
			(and
				(not (search_again))
				(not (in r172))
				(in r171)
			)
	)

	(:action move-right-from-r173
		:precondition
			(and 
				(in r173)
			)
		:effect
			(and
				(not (search_again))
				(not (in r173))
				(in r174)
			)
	)


	(:action move-left-from-r173
		:precondition
			(and 
				(in r173)
			)
		:effect
			(and
				(not (search_again))
				(not (in r173))
				(in r172)
			)
	)

	(:action move-right-from-r174
		:precondition
			(and 
				(in r174)
			)
		:effect
			(and
				(not (search_again))
				(not (in r174))
				(in r175)
			)
	)


	(:action move-left-from-r174
		:precondition
			(and 
				(in r174)
			)
		:effect
			(and
				(not (search_again))
				(not (in r174))
				(in r173)
			)
	)

	(:action move-right-from-r175
		:precondition
			(and 
				(in r175)
			)
		:effect
			(and
				(not (search_again))
				(not (in r175))
				(in r176)
			)
	)


	(:action move-left-from-r175
		:precondition
			(and 
				(in r175)
			)
		:effect
			(and
				(not (search_again))
				(not (in r175))
				(in r174)
			)
	)

	(:action move-right-from-r176
		:precondition
			(and 
				(in r176)
			)
		:effect
			(and
				(not (search_again))
				(not (in r176))
				(in r177)
			)
	)


	(:action move-left-from-r176
		:precondition
			(and 
				(in r176)
			)
		:effect
			(and
				(not (search_again))
				(not (in r176))
				(in r175)
			)
	)

	(:action move-right-from-r177
		:precondition
			(and 
				(in r177)
			)
		:effect
			(and
				(not (search_again))
				(not (in r177))
				(in r178)
			)
	)


	(:action move-left-from-r177
		:precondition
			(and 
				(in r177)
			)
		:effect
			(and
				(not (search_again))
				(not (in r177))
				(in r176)
			)
	)

	(:action move-right-from-r178
		:precondition
			(and 
				(in r178)
			)
		:effect
			(and
				(not (search_again))
				(not (in r178))
				(in r179)
			)
	)


	(:action move-left-from-r178
		:precondition
			(and 
				(in r178)
			)
		:effect
			(and
				(not (search_again))
				(not (in r178))
				(in r177)
			)
	)

	(:action move-right-from-r179
		:precondition
			(and 
				(in r179)
			)
		:effect
			(and
				(not (search_again))
				(not (in r179))
				(in r180)
			)
	)


	(:action move-left-from-r179
		:precondition
			(and 
				(in r179)
			)
		:effect
			(and
				(not (search_again))
				(not (in r179))
				(in r178)
			)
	)

	(:action move-right-from-r180
		:precondition
			(and 
				(in r180)
			)
		:effect
			(and
				(not (search_again))
				(not (in r180))
				(in r181)
			)
	)


	(:action move-left-from-r180
		:precondition
			(and 
				(in r180)
			)
		:effect
			(and
				(not (search_again))
				(not (in r180))
				(in r179)
			)
	)

	(:action move-right-from-r181
		:precondition
			(and 
				(in r181)
			)
		:effect
			(and
				(not (search_again))
				(not (in r181))
				(in r182)
			)
	)


	(:action move-left-from-r181
		:precondition
			(and 
				(in r181)
			)
		:effect
			(and
				(not (search_again))
				(not (in r181))
				(in r180)
			)
	)

	(:action move-right-from-r182
		:precondition
			(and 
				(in r182)
			)
		:effect
			(and
				(not (search_again))
				(not (in r182))
				(in r183)
			)
	)


	(:action move-left-from-r182
		:precondition
			(and 
				(in r182)
			)
		:effect
			(and
				(not (search_again))
				(not (in r182))
				(in r181)
			)
	)

	(:action move-right-from-r183
		:precondition
			(and 
				(in r183)
			)
		:effect
			(and
				(not (search_again))
				(not (in r183))
				(in r184)
			)
	)


	(:action move-left-from-r183
		:precondition
			(and 
				(in r183)
			)
		:effect
			(and
				(not (search_again))
				(not (in r183))
				(in r182)
			)
	)

	(:action move-right-from-r184
		:precondition
			(and 
				(in r184)
			)
		:effect
			(and
				(not (search_again))
				(not (in r184))
				(in r185)
			)
	)


	(:action move-left-from-r184
		:precondition
			(and 
				(in r184)
			)
		:effect
			(and
				(not (search_again))
				(not (in r184))
				(in r183)
			)
	)

	(:action move-right-from-r185
		:precondition
			(and 
				(in r185)
			)
		:effect
			(and
				(not (search_again))
				(not (in r185))
				(in r186)
			)
	)


	(:action move-left-from-r185
		:precondition
			(and 
				(in r185)
			)
		:effect
			(and
				(not (search_again))
				(not (in r185))
				(in r184)
			)
	)

	(:action move-right-from-r186
		:precondition
			(and 
				(in r186)
			)
		:effect
			(and
				(not (search_again))
				(not (in r186))
				(in r187)
			)
	)


	(:action move-left-from-r186
		:precondition
			(and 
				(in r186)
			)
		:effect
			(and
				(not (search_again))
				(not (in r186))
				(in r185)
			)
	)

	(:action move-right-from-r187
		:precondition
			(and 
				(in r187)
			)
		:effect
			(and
				(not (search_again))
				(not (in r187))
				(in r188)
			)
	)


	(:action move-left-from-r187
		:precondition
			(and 
				(in r187)
			)
		:effect
			(and
				(not (search_again))
				(not (in r187))
				(in r186)
			)
	)

	(:action move-right-from-r188
		:precondition
			(and 
				(in r188)
			)
		:effect
			(and
				(not (search_again))
				(not (in r188))
				(in r189)
			)
	)


	(:action move-left-from-r188
		:precondition
			(and 
				(in r188)
			)
		:effect
			(and
				(not (search_again))
				(not (in r188))
				(in r187)
			)
	)

	(:action move-right-from-r189
		:precondition
			(and 
				(in r189)
			)
		:effect
			(and
				(not (search_again))
				(not (in r189))
				(in r190)
			)
	)


	(:action move-left-from-r189
		:precondition
			(and 
				(in r189)
			)
		:effect
			(and
				(not (search_again))
				(not (in r189))
				(in r188)
			)
	)

	(:action move-right-from-r190
		:precondition
			(and 
				(in r190)
			)
		:effect
			(and
				(not (search_again))
				(not (in r190))
				(in r191)
			)
	)


	(:action move-left-from-r190
		:precondition
			(and 
				(in r190)
			)
		:effect
			(and
				(not (search_again))
				(not (in r190))
				(in r189)
			)
	)

	(:action move-right-from-r191
		:precondition
			(and 
				(in r191)
			)
		:effect
			(and
				(not (search_again))
				(not (in r191))
				(in r192)
			)
	)


	(:action move-left-from-r191
		:precondition
			(and 
				(in r191)
			)
		:effect
			(and
				(not (search_again))
				(not (in r191))
				(in r190)
			)
	)

	(:action move-right-from-r192
		:precondition
			(and 
				(in r192)
			)
		:effect
			(and
				(not (search_again))
				(not (in r192))
				(in r193)
			)
	)


	(:action move-left-from-r192
		:precondition
			(and 
				(in r192)
			)
		:effect
			(and
				(not (search_again))
				(not (in r192))
				(in r191)
			)
	)

	(:action move-right-from-r193
		:precondition
			(and 
				(in r193)
			)
		:effect
			(and
				(not (search_again))
				(not (in r193))
				(in r194)
			)
	)


	(:action move-left-from-r193
		:precondition
			(and 
				(in r193)
			)
		:effect
			(and
				(not (search_again))
				(not (in r193))
				(in r192)
			)
	)

	(:action move-right-from-r194
		:precondition
			(and 
				(in r194)
			)
		:effect
			(and
				(not (search_again))
				(not (in r194))
				(in r195)
			)
	)


	(:action move-left-from-r194
		:precondition
			(and 
				(in r194)
			)
		:effect
			(and
				(not (search_again))
				(not (in r194))
				(in r193)
			)
	)

	(:action move-right-from-r195
		:precondition
			(and 
				(in r195)
			)
		:effect
			(and
				(not (search_again))
				(not (in r195))
				(in r196)
			)
	)


	(:action move-left-from-r195
		:precondition
			(and 
				(in r195)
			)
		:effect
			(and
				(not (search_again))
				(not (in r195))
				(in r194)
			)
	)

	(:action move-right-from-r196
		:precondition
			(and 
				(in r196)
			)
		:effect
			(and
				(not (search_again))
				(not (in r196))
				(in r197)
			)
	)


	(:action move-left-from-r196
		:precondition
			(and 
				(in r196)
			)
		:effect
			(and
				(not (search_again))
				(not (in r196))
				(in r195)
			)
	)

	(:action move-right-from-r197
		:precondition
			(and 
				(in r197)
			)
		:effect
			(and
				(not (search_again))
				(not (in r197))
				(in r198)
			)
	)


	(:action move-left-from-r197
		:precondition
			(and 
				(in r197)
			)
		:effect
			(and
				(not (search_again))
				(not (in r197))
				(in r196)
			)
	)

	(:action move-right-from-r198
		:precondition
			(and 
				(in r198)
			)
		:effect
			(and
				(not (search_again))
				(not (in r198))
				(in r199)
			)
	)


	(:action move-left-from-r198
		:precondition
			(and 
				(in r198)
			)
		:effect
			(and
				(not (search_again))
				(not (in r198))
				(in r197)
			)
	)

	(:action move-right-from-r199
		:precondition
			(and 
				(in r199)
				(not (searched_in r200))
			)
		:effect
			(and
				(not (search_again))
				(not (in r199))
				(in r200)
				(oneof (not (seen)) (seen))
				(searched_in r200)
			)
	)


	(:action move-left-from-r199
		:precondition
			(and 
				(in r199)
			)
		:effect
			(and
				(not (search_again))
				(not (in r199))
				(in r198)
			)
	)

	(:action move-right-from-r200
		:precondition
			(and 
				(in r200)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r200))
				(in r1)
			)
	)


	(:action move-left-from-r200
		:precondition
			(and 
				(in r200)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r200))
				(in r199)
			)
	)


	(:action stay
		:precondition
			(f_ok 
			)
		:effect
			(and
			)
	)


	(:action searching_again
		:precondition
			(and 
				(searched_in r100)
				(searched_in r200)
				(not (seen))
			)
		:effect
			(and
				(not (searched_in r100))
				(not (searched_in r200))
				(search_again)
			)
	)

)

