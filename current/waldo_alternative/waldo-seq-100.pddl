
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Waldo example for 100 rooms
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Taken from
;; Kress-Gazit, H., Fainekos, G.E., Pappas, G.J.
;; Where's Waldo? Sensor-Based Temporal Logic Motion Planning.
;; ICRA 2007: 3116-3121
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Goal formula:
;;
;; AE(r50 || seen) && AE(r100 || seen)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; []<> (search_again | seen)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain waldo)
	(:requirements :strips :typing :equality)
	(:types 
		room
	)
	(:constants
		r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 r11 r12 r13 r14 r15 r16 r17 r18 r19 r20 r21 r22 r23 r24 r25 r26 r27 r28 r29 r30 r31 r32 r33 r34 r35 r36 r37 r38 r39 r40 r41 r42 r43 r44 r45 r46 r47 r48 r49 r50 r51 r52 r53 r54 r55 r56 r57 r58 r59 r60 r61 r62 r63 r64 r65 r66 r67 r68 r69 r70 r71 r72 r73 r74 r75 r76 r77 r78 r79 r80 r81 r82 r83 r84 r85 r86 r87 r88 r89 r90 r91 r92 r93 r94 r95 r96 r97 r98 r99 r100 - room
	)
	(:predicates
		(seen) ;; found Waldo
		(search_again) ;; searching for Waldo
		(in ?r  - room)
		(searched_in ?r  - room)
	)

	(:action move-right-from-r1
		:precondition
			(and 
				(in r1)
			)
		:effect
			(and
				(not (search_again))
				(not (in r1))
				(in r2)
			)
	)


	(:action move-left-from-r1
		:precondition
			(and 
				(in r1)
				(not (searched_in r100))
			)
		:effect
			(and
				(not (search_again))
				(not (in r1))
				(in r100)
				(oneof (not (seen)) (seen))
				(searched_in r100)
			)
	)

	(:action move-right-from-r2
		:precondition
			(and 
				(in r2)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2))
				(in r3)
			)
	)


	(:action move-left-from-r2
		:precondition
			(and 
				(in r2)
			)
		:effect
			(and
				(not (search_again))
				(not (in r2))
				(in r1)
			)
	)

	(:action move-right-from-r3
		:precondition
			(and 
				(in r3)
			)
		:effect
			(and
				(not (search_again))
				(not (in r3))
				(in r4)
			)
	)


	(:action move-left-from-r3
		:precondition
			(and 
				(in r3)
			)
		:effect
			(and
				(not (search_again))
				(not (in r3))
				(in r2)
			)
	)

	(:action move-right-from-r4
		:precondition
			(and 
				(in r4)
			)
		:effect
			(and
				(not (search_again))
				(not (in r4))
				(in r5)
			)
	)


	(:action move-left-from-r4
		:precondition
			(and 
				(in r4)
			)
		:effect
			(and
				(not (search_again))
				(not (in r4))
				(in r3)
			)
	)

	(:action move-right-from-r5
		:precondition
			(and 
				(in r5)
			)
		:effect
			(and
				(not (search_again))
				(not (in r5))
				(in r6)
			)
	)


	(:action move-left-from-r5
		:precondition
			(and 
				(in r5)
			)
		:effect
			(and
				(not (search_again))
				(not (in r5))
				(in r4)
			)
	)

	(:action move-right-from-r6
		:precondition
			(and 
				(in r6)
			)
		:effect
			(and
				(not (search_again))
				(not (in r6))
				(in r7)
			)
	)


	(:action move-left-from-r6
		:precondition
			(and 
				(in r6)
			)
		:effect
			(and
				(not (search_again))
				(not (in r6))
				(in r5)
			)
	)

	(:action move-right-from-r7
		:precondition
			(and 
				(in r7)
			)
		:effect
			(and
				(not (search_again))
				(not (in r7))
				(in r8)
			)
	)


	(:action move-left-from-r7
		:precondition
			(and 
				(in r7)
			)
		:effect
			(and
				(not (search_again))
				(not (in r7))
				(in r6)
			)
	)

	(:action move-right-from-r8
		:precondition
			(and 
				(in r8)
			)
		:effect
			(and
				(not (search_again))
				(not (in r8))
				(in r9)
			)
	)


	(:action move-left-from-r8
		:precondition
			(and 
				(in r8)
			)
		:effect
			(and
				(not (search_again))
				(not (in r8))
				(in r7)
			)
	)

	(:action move-right-from-r9
		:precondition
			(and 
				(in r9)
			)
		:effect
			(and
				(not (search_again))
				(not (in r9))
				(in r10)
			)
	)


	(:action move-left-from-r9
		:precondition
			(and 
				(in r9)
			)
		:effect
			(and
				(not (search_again))
				(not (in r9))
				(in r8)
			)
	)

	(:action move-right-from-r10
		:precondition
			(and 
				(in r10)
			)
		:effect
			(and
				(not (search_again))
				(not (in r10))
				(in r11)
			)
	)


	(:action move-left-from-r10
		:precondition
			(and 
				(in r10)
			)
		:effect
			(and
				(not (search_again))
				(not (in r10))
				(in r9)
			)
	)

	(:action move-right-from-r11
		:precondition
			(and 
				(in r11)
			)
		:effect
			(and
				(not (search_again))
				(not (in r11))
				(in r12)
			)
	)


	(:action move-left-from-r11
		:precondition
			(and 
				(in r11)
			)
		:effect
			(and
				(not (search_again))
				(not (in r11))
				(in r10)
			)
	)

	(:action move-right-from-r12
		:precondition
			(and 
				(in r12)
			)
		:effect
			(and
				(not (search_again))
				(not (in r12))
				(in r13)
			)
	)


	(:action move-left-from-r12
		:precondition
			(and 
				(in r12)
			)
		:effect
			(and
				(not (search_again))
				(not (in r12))
				(in r11)
			)
	)

	(:action move-right-from-r13
		:precondition
			(and 
				(in r13)
			)
		:effect
			(and
				(not (search_again))
				(not (in r13))
				(in r14)
			)
	)


	(:action move-left-from-r13
		:precondition
			(and 
				(in r13)
			)
		:effect
			(and
				(not (search_again))
				(not (in r13))
				(in r12)
			)
	)

	(:action move-right-from-r14
		:precondition
			(and 
				(in r14)
			)
		:effect
			(and
				(not (search_again))
				(not (in r14))
				(in r15)
			)
	)


	(:action move-left-from-r14
		:precondition
			(and 
				(in r14)
			)
		:effect
			(and
				(not (search_again))
				(not (in r14))
				(in r13)
			)
	)

	(:action move-right-from-r15
		:precondition
			(and 
				(in r15)
			)
		:effect
			(and
				(not (search_again))
				(not (in r15))
				(in r16)
			)
	)


	(:action move-left-from-r15
		:precondition
			(and 
				(in r15)
			)
		:effect
			(and
				(not (search_again))
				(not (in r15))
				(in r14)
			)
	)

	(:action move-right-from-r16
		:precondition
			(and 
				(in r16)
			)
		:effect
			(and
				(not (search_again))
				(not (in r16))
				(in r17)
			)
	)


	(:action move-left-from-r16
		:precondition
			(and 
				(in r16)
			)
		:effect
			(and
				(not (search_again))
				(not (in r16))
				(in r15)
			)
	)

	(:action move-right-from-r17
		:precondition
			(and 
				(in r17)
			)
		:effect
			(and
				(not (search_again))
				(not (in r17))
				(in r18)
			)
	)


	(:action move-left-from-r17
		:precondition
			(and 
				(in r17)
			)
		:effect
			(and
				(not (search_again))
				(not (in r17))
				(in r16)
			)
	)

	(:action move-right-from-r18
		:precondition
			(and 
				(in r18)
			)
		:effect
			(and
				(not (search_again))
				(not (in r18))
				(in r19)
			)
	)


	(:action move-left-from-r18
		:precondition
			(and 
				(in r18)
			)
		:effect
			(and
				(not (search_again))
				(not (in r18))
				(in r17)
			)
	)

	(:action move-right-from-r19
		:precondition
			(and 
				(in r19)
			)
		:effect
			(and
				(not (search_again))
				(not (in r19))
				(in r20)
			)
	)


	(:action move-left-from-r19
		:precondition
			(and 
				(in r19)
			)
		:effect
			(and
				(not (search_again))
				(not (in r19))
				(in r18)
			)
	)

	(:action move-right-from-r20
		:precondition
			(and 
				(in r20)
			)
		:effect
			(and
				(not (search_again))
				(not (in r20))
				(in r21)
			)
	)


	(:action move-left-from-r20
		:precondition
			(and 
				(in r20)
			)
		:effect
			(and
				(not (search_again))
				(not (in r20))
				(in r19)
			)
	)

	(:action move-right-from-r21
		:precondition
			(and 
				(in r21)
			)
		:effect
			(and
				(not (search_again))
				(not (in r21))
				(in r22)
			)
	)


	(:action move-left-from-r21
		:precondition
			(and 
				(in r21)
			)
		:effect
			(and
				(not (search_again))
				(not (in r21))
				(in r20)
			)
	)

	(:action move-right-from-r22
		:precondition
			(and 
				(in r22)
			)
		:effect
			(and
				(not (search_again))
				(not (in r22))
				(in r23)
			)
	)


	(:action move-left-from-r22
		:precondition
			(and 
				(in r22)
			)
		:effect
			(and
				(not (search_again))
				(not (in r22))
				(in r21)
			)
	)

	(:action move-right-from-r23
		:precondition
			(and 
				(in r23)
			)
		:effect
			(and
				(not (search_again))
				(not (in r23))
				(in r24)
			)
	)


	(:action move-left-from-r23
		:precondition
			(and 
				(in r23)
			)
		:effect
			(and
				(not (search_again))
				(not (in r23))
				(in r22)
			)
	)

	(:action move-right-from-r24
		:precondition
			(and 
				(in r24)
			)
		:effect
			(and
				(not (search_again))
				(not (in r24))
				(in r25)
			)
	)


	(:action move-left-from-r24
		:precondition
			(and 
				(in r24)
			)
		:effect
			(and
				(not (search_again))
				(not (in r24))
				(in r23)
			)
	)

	(:action move-right-from-r25
		:precondition
			(and 
				(in r25)
			)
		:effect
			(and
				(not (search_again))
				(not (in r25))
				(in r26)
			)
	)


	(:action move-left-from-r25
		:precondition
			(and 
				(in r25)
			)
		:effect
			(and
				(not (search_again))
				(not (in r25))
				(in r24)
			)
	)

	(:action move-right-from-r26
		:precondition
			(and 
				(in r26)
			)
		:effect
			(and
				(not (search_again))
				(not (in r26))
				(in r27)
			)
	)


	(:action move-left-from-r26
		:precondition
			(and 
				(in r26)
			)
		:effect
			(and
				(not (search_again))
				(not (in r26))
				(in r25)
			)
	)

	(:action move-right-from-r27
		:precondition
			(and 
				(in r27)
			)
		:effect
			(and
				(not (search_again))
				(not (in r27))
				(in r28)
			)
	)


	(:action move-left-from-r27
		:precondition
			(and 
				(in r27)
			)
		:effect
			(and
				(not (search_again))
				(not (in r27))
				(in r26)
			)
	)

	(:action move-right-from-r28
		:precondition
			(and 
				(in r28)
			)
		:effect
			(and
				(not (search_again))
				(not (in r28))
				(in r29)
			)
	)


	(:action move-left-from-r28
		:precondition
			(and 
				(in r28)
			)
		:effect
			(and
				(not (search_again))
				(not (in r28))
				(in r27)
			)
	)

	(:action move-right-from-r29
		:precondition
			(and 
				(in r29)
			)
		:effect
			(and
				(not (search_again))
				(not (in r29))
				(in r30)
			)
	)


	(:action move-left-from-r29
		:precondition
			(and 
				(in r29)
			)
		:effect
			(and
				(not (search_again))
				(not (in r29))
				(in r28)
			)
	)

	(:action move-right-from-r30
		:precondition
			(and 
				(in r30)
			)
		:effect
			(and
				(not (search_again))
				(not (in r30))
				(in r31)
			)
	)


	(:action move-left-from-r30
		:precondition
			(and 
				(in r30)
			)
		:effect
			(and
				(not (search_again))
				(not (in r30))
				(in r29)
			)
	)

	(:action move-right-from-r31
		:precondition
			(and 
				(in r31)
			)
		:effect
			(and
				(not (search_again))
				(not (in r31))
				(in r32)
			)
	)


	(:action move-left-from-r31
		:precondition
			(and 
				(in r31)
			)
		:effect
			(and
				(not (search_again))
				(not (in r31))
				(in r30)
			)
	)

	(:action move-right-from-r32
		:precondition
			(and 
				(in r32)
			)
		:effect
			(and
				(not (search_again))
				(not (in r32))
				(in r33)
			)
	)


	(:action move-left-from-r32
		:precondition
			(and 
				(in r32)
			)
		:effect
			(and
				(not (search_again))
				(not (in r32))
				(in r31)
			)
	)

	(:action move-right-from-r33
		:precondition
			(and 
				(in r33)
			)
		:effect
			(and
				(not (search_again))
				(not (in r33))
				(in r34)
			)
	)


	(:action move-left-from-r33
		:precondition
			(and 
				(in r33)
			)
		:effect
			(and
				(not (search_again))
				(not (in r33))
				(in r32)
			)
	)

	(:action move-right-from-r34
		:precondition
			(and 
				(in r34)
			)
		:effect
			(and
				(not (search_again))
				(not (in r34))
				(in r35)
			)
	)


	(:action move-left-from-r34
		:precondition
			(and 
				(in r34)
			)
		:effect
			(and
				(not (search_again))
				(not (in r34))
				(in r33)
			)
	)

	(:action move-right-from-r35
		:precondition
			(and 
				(in r35)
			)
		:effect
			(and
				(not (search_again))
				(not (in r35))
				(in r36)
			)
	)


	(:action move-left-from-r35
		:precondition
			(and 
				(in r35)
			)
		:effect
			(and
				(not (search_again))
				(not (in r35))
				(in r34)
			)
	)

	(:action move-right-from-r36
		:precondition
			(and 
				(in r36)
			)
		:effect
			(and
				(not (search_again))
				(not (in r36))
				(in r37)
			)
	)


	(:action move-left-from-r36
		:precondition
			(and 
				(in r36)
			)
		:effect
			(and
				(not (search_again))
				(not (in r36))
				(in r35)
			)
	)

	(:action move-right-from-r37
		:precondition
			(and 
				(in r37)
			)
		:effect
			(and
				(not (search_again))
				(not (in r37))
				(in r38)
			)
	)


	(:action move-left-from-r37
		:precondition
			(and 
				(in r37)
			)
		:effect
			(and
				(not (search_again))
				(not (in r37))
				(in r36)
			)
	)

	(:action move-right-from-r38
		:precondition
			(and 
				(in r38)
			)
		:effect
			(and
				(not (search_again))
				(not (in r38))
				(in r39)
			)
	)


	(:action move-left-from-r38
		:precondition
			(and 
				(in r38)
			)
		:effect
			(and
				(not (search_again))
				(not (in r38))
				(in r37)
			)
	)

	(:action move-right-from-r39
		:precondition
			(and 
				(in r39)
			)
		:effect
			(and
				(not (search_again))
				(not (in r39))
				(in r40)
			)
	)


	(:action move-left-from-r39
		:precondition
			(and 
				(in r39)
			)
		:effect
			(and
				(not (search_again))
				(not (in r39))
				(in r38)
			)
	)

	(:action move-right-from-r40
		:precondition
			(and 
				(in r40)
			)
		:effect
			(and
				(not (search_again))
				(not (in r40))
				(in r41)
			)
	)


	(:action move-left-from-r40
		:precondition
			(and 
				(in r40)
			)
		:effect
			(and
				(not (search_again))
				(not (in r40))
				(in r39)
			)
	)

	(:action move-right-from-r41
		:precondition
			(and 
				(in r41)
			)
		:effect
			(and
				(not (search_again))
				(not (in r41))
				(in r42)
			)
	)


	(:action move-left-from-r41
		:precondition
			(and 
				(in r41)
			)
		:effect
			(and
				(not (search_again))
				(not (in r41))
				(in r40)
			)
	)

	(:action move-right-from-r42
		:precondition
			(and 
				(in r42)
			)
		:effect
			(and
				(not (search_again))
				(not (in r42))
				(in r43)
			)
	)


	(:action move-left-from-r42
		:precondition
			(and 
				(in r42)
			)
		:effect
			(and
				(not (search_again))
				(not (in r42))
				(in r41)
			)
	)

	(:action move-right-from-r43
		:precondition
			(and 
				(in r43)
			)
		:effect
			(and
				(not (search_again))
				(not (in r43))
				(in r44)
			)
	)


	(:action move-left-from-r43
		:precondition
			(and 
				(in r43)
			)
		:effect
			(and
				(not (search_again))
				(not (in r43))
				(in r42)
			)
	)

	(:action move-right-from-r44
		:precondition
			(and 
				(in r44)
			)
		:effect
			(and
				(not (search_again))
				(not (in r44))
				(in r45)
			)
	)


	(:action move-left-from-r44
		:precondition
			(and 
				(in r44)
			)
		:effect
			(and
				(not (search_again))
				(not (in r44))
				(in r43)
			)
	)

	(:action move-right-from-r45
		:precondition
			(and 
				(in r45)
			)
		:effect
			(and
				(not (search_again))
				(not (in r45))
				(in r46)
			)
	)


	(:action move-left-from-r45
		:precondition
			(and 
				(in r45)
			)
		:effect
			(and
				(not (search_again))
				(not (in r45))
				(in r44)
			)
	)

	(:action move-right-from-r46
		:precondition
			(and 
				(in r46)
			)
		:effect
			(and
				(not (search_again))
				(not (in r46))
				(in r47)
			)
	)


	(:action move-left-from-r46
		:precondition
			(and 
				(in r46)
			)
		:effect
			(and
				(not (search_again))
				(not (in r46))
				(in r45)
			)
	)

	(:action move-right-from-r47
		:precondition
			(and 
				(in r47)
			)
		:effect
			(and
				(not (search_again))
				(not (in r47))
				(in r48)
			)
	)


	(:action move-left-from-r47
		:precondition
			(and 
				(in r47)
			)
		:effect
			(and
				(not (search_again))
				(not (in r47))
				(in r46)
			)
	)

	(:action move-right-from-r48
		:precondition
			(and 
				(in r48)
			)
		:effect
			(and
				(not (search_again))
				(not (in r48))
				(in r49)
			)
	)


	(:action move-left-from-r48
		:precondition
			(and 
				(in r48)
			)
		:effect
			(and
				(not (search_again))
				(not (in r48))
				(in r47)
			)
	)

	(:action move-right-from-r49
		:precondition
			(and 
				(in r49)
				(not (searched_in r50))
			)
		:effect
			(and
				(not (search_again))
				(not (in r49))
				(in r50)
				(oneof (not (seen)) (seen))
				(searched_in r50)
			)
	)


	(:action move-left-from-r49
		:precondition
			(and 
				(in r49)
			)
		:effect
			(and
				(not (search_again))
				(not (in r49))
				(in r48)
			)
	)

	(:action move-right-from-r50
		:precondition
			(and 
				(in r50)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r50))
				(in r51)
			)
	)


	(:action move-left-from-r50
		:precondition
			(and 
				(in r50)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r50))
				(in r49)
			)
	)

	(:action move-right-from-r51
		:precondition
			(and 
				(in r51)
			)
		:effect
			(and
				(not (search_again))
				(not (in r51))
				(in r52)
			)
	)


	(:action move-left-from-r51
		:precondition
			(and 
				(in r51)
				(not (searched_in r50))
			)
		:effect
			(and
				(not (search_again))
				(not (in r51))
				(in r50)
				(oneof (not (seen)) (seen))
				(searched_in r50)
			)
	)

	(:action move-right-from-r52
		:precondition
			(and 
				(in r52)
			)
		:effect
			(and
				(not (search_again))
				(not (in r52))
				(in r53)
			)
	)


	(:action move-left-from-r52
		:precondition
			(and 
				(in r52)
			)
		:effect
			(and
				(not (search_again))
				(not (in r52))
				(in r51)
			)
	)

	(:action move-right-from-r53
		:precondition
			(and 
				(in r53)
			)
		:effect
			(and
				(not (search_again))
				(not (in r53))
				(in r54)
			)
	)


	(:action move-left-from-r53
		:precondition
			(and 
				(in r53)
			)
		:effect
			(and
				(not (search_again))
				(not (in r53))
				(in r52)
			)
	)

	(:action move-right-from-r54
		:precondition
			(and 
				(in r54)
			)
		:effect
			(and
				(not (search_again))
				(not (in r54))
				(in r55)
			)
	)


	(:action move-left-from-r54
		:precondition
			(and 
				(in r54)
			)
		:effect
			(and
				(not (search_again))
				(not (in r54))
				(in r53)
			)
	)

	(:action move-right-from-r55
		:precondition
			(and 
				(in r55)
			)
		:effect
			(and
				(not (search_again))
				(not (in r55))
				(in r56)
			)
	)


	(:action move-left-from-r55
		:precondition
			(and 
				(in r55)
			)
		:effect
			(and
				(not (search_again))
				(not (in r55))
				(in r54)
			)
	)

	(:action move-right-from-r56
		:precondition
			(and 
				(in r56)
			)
		:effect
			(and
				(not (search_again))
				(not (in r56))
				(in r57)
			)
	)


	(:action move-left-from-r56
		:precondition
			(and 
				(in r56)
			)
		:effect
			(and
				(not (search_again))
				(not (in r56))
				(in r55)
			)
	)

	(:action move-right-from-r57
		:precondition
			(and 
				(in r57)
			)
		:effect
			(and
				(not (search_again))
				(not (in r57))
				(in r58)
			)
	)


	(:action move-left-from-r57
		:precondition
			(and 
				(in r57)
			)
		:effect
			(and
				(not (search_again))
				(not (in r57))
				(in r56)
			)
	)

	(:action move-right-from-r58
		:precondition
			(and 
				(in r58)
			)
		:effect
			(and
				(not (search_again))
				(not (in r58))
				(in r59)
			)
	)


	(:action move-left-from-r58
		:precondition
			(and 
				(in r58)
			)
		:effect
			(and
				(not (search_again))
				(not (in r58))
				(in r57)
			)
	)

	(:action move-right-from-r59
		:precondition
			(and 
				(in r59)
			)
		:effect
			(and
				(not (search_again))
				(not (in r59))
				(in r60)
			)
	)


	(:action move-left-from-r59
		:precondition
			(and 
				(in r59)
			)
		:effect
			(and
				(not (search_again))
				(not (in r59))
				(in r58)
			)
	)

	(:action move-right-from-r60
		:precondition
			(and 
				(in r60)
			)
		:effect
			(and
				(not (search_again))
				(not (in r60))
				(in r61)
			)
	)


	(:action move-left-from-r60
		:precondition
			(and 
				(in r60)
			)
		:effect
			(and
				(not (search_again))
				(not (in r60))
				(in r59)
			)
	)

	(:action move-right-from-r61
		:precondition
			(and 
				(in r61)
			)
		:effect
			(and
				(not (search_again))
				(not (in r61))
				(in r62)
			)
	)


	(:action move-left-from-r61
		:precondition
			(and 
				(in r61)
			)
		:effect
			(and
				(not (search_again))
				(not (in r61))
				(in r60)
			)
	)

	(:action move-right-from-r62
		:precondition
			(and 
				(in r62)
			)
		:effect
			(and
				(not (search_again))
				(not (in r62))
				(in r63)
			)
	)


	(:action move-left-from-r62
		:precondition
			(and 
				(in r62)
			)
		:effect
			(and
				(not (search_again))
				(not (in r62))
				(in r61)
			)
	)

	(:action move-right-from-r63
		:precondition
			(and 
				(in r63)
			)
		:effect
			(and
				(not (search_again))
				(not (in r63))
				(in r64)
			)
	)


	(:action move-left-from-r63
		:precondition
			(and 
				(in r63)
			)
		:effect
			(and
				(not (search_again))
				(not (in r63))
				(in r62)
			)
	)

	(:action move-right-from-r64
		:precondition
			(and 
				(in r64)
			)
		:effect
			(and
				(not (search_again))
				(not (in r64))
				(in r65)
			)
	)


	(:action move-left-from-r64
		:precondition
			(and 
				(in r64)
			)
		:effect
			(and
				(not (search_again))
				(not (in r64))
				(in r63)
			)
	)

	(:action move-right-from-r65
		:precondition
			(and 
				(in r65)
			)
		:effect
			(and
				(not (search_again))
				(not (in r65))
				(in r66)
			)
	)


	(:action move-left-from-r65
		:precondition
			(and 
				(in r65)
			)
		:effect
			(and
				(not (search_again))
				(not (in r65))
				(in r64)
			)
	)

	(:action move-right-from-r66
		:precondition
			(and 
				(in r66)
			)
		:effect
			(and
				(not (search_again))
				(not (in r66))
				(in r67)
			)
	)


	(:action move-left-from-r66
		:precondition
			(and 
				(in r66)
			)
		:effect
			(and
				(not (search_again))
				(not (in r66))
				(in r65)
			)
	)

	(:action move-right-from-r67
		:precondition
			(and 
				(in r67)
			)
		:effect
			(and
				(not (search_again))
				(not (in r67))
				(in r68)
			)
	)


	(:action move-left-from-r67
		:precondition
			(and 
				(in r67)
			)
		:effect
			(and
				(not (search_again))
				(not (in r67))
				(in r66)
			)
	)

	(:action move-right-from-r68
		:precondition
			(and 
				(in r68)
			)
		:effect
			(and
				(not (search_again))
				(not (in r68))
				(in r69)
			)
	)


	(:action move-left-from-r68
		:precondition
			(and 
				(in r68)
			)
		:effect
			(and
				(not (search_again))
				(not (in r68))
				(in r67)
			)
	)

	(:action move-right-from-r69
		:precondition
			(and 
				(in r69)
			)
		:effect
			(and
				(not (search_again))
				(not (in r69))
				(in r70)
			)
	)


	(:action move-left-from-r69
		:precondition
			(and 
				(in r69)
			)
		:effect
			(and
				(not (search_again))
				(not (in r69))
				(in r68)
			)
	)

	(:action move-right-from-r70
		:precondition
			(and 
				(in r70)
			)
		:effect
			(and
				(not (search_again))
				(not (in r70))
				(in r71)
			)
	)


	(:action move-left-from-r70
		:precondition
			(and 
				(in r70)
			)
		:effect
			(and
				(not (search_again))
				(not (in r70))
				(in r69)
			)
	)

	(:action move-right-from-r71
		:precondition
			(and 
				(in r71)
			)
		:effect
			(and
				(not (search_again))
				(not (in r71))
				(in r72)
			)
	)


	(:action move-left-from-r71
		:precondition
			(and 
				(in r71)
			)
		:effect
			(and
				(not (search_again))
				(not (in r71))
				(in r70)
			)
	)

	(:action move-right-from-r72
		:precondition
			(and 
				(in r72)
			)
		:effect
			(and
				(not (search_again))
				(not (in r72))
				(in r73)
			)
	)


	(:action move-left-from-r72
		:precondition
			(and 
				(in r72)
			)
		:effect
			(and
				(not (search_again))
				(not (in r72))
				(in r71)
			)
	)

	(:action move-right-from-r73
		:precondition
			(and 
				(in r73)
			)
		:effect
			(and
				(not (search_again))
				(not (in r73))
				(in r74)
			)
	)


	(:action move-left-from-r73
		:precondition
			(and 
				(in r73)
			)
		:effect
			(and
				(not (search_again))
				(not (in r73))
				(in r72)
			)
	)

	(:action move-right-from-r74
		:precondition
			(and 
				(in r74)
			)
		:effect
			(and
				(not (search_again))
				(not (in r74))
				(in r75)
			)
	)


	(:action move-left-from-r74
		:precondition
			(and 
				(in r74)
			)
		:effect
			(and
				(not (search_again))
				(not (in r74))
				(in r73)
			)
	)

	(:action move-right-from-r75
		:precondition
			(and 
				(in r75)
			)
		:effect
			(and
				(not (search_again))
				(not (in r75))
				(in r76)
			)
	)


	(:action move-left-from-r75
		:precondition
			(and 
				(in r75)
			)
		:effect
			(and
				(not (search_again))
				(not (in r75))
				(in r74)
			)
	)

	(:action move-right-from-r76
		:precondition
			(and 
				(in r76)
			)
		:effect
			(and
				(not (search_again))
				(not (in r76))
				(in r77)
			)
	)


	(:action move-left-from-r76
		:precondition
			(and 
				(in r76)
			)
		:effect
			(and
				(not (search_again))
				(not (in r76))
				(in r75)
			)
	)

	(:action move-right-from-r77
		:precondition
			(and 
				(in r77)
			)
		:effect
			(and
				(not (search_again))
				(not (in r77))
				(in r78)
			)
	)


	(:action move-left-from-r77
		:precondition
			(and 
				(in r77)
			)
		:effect
			(and
				(not (search_again))
				(not (in r77))
				(in r76)
			)
	)

	(:action move-right-from-r78
		:precondition
			(and 
				(in r78)
			)
		:effect
			(and
				(not (search_again))
				(not (in r78))
				(in r79)
			)
	)


	(:action move-left-from-r78
		:precondition
			(and 
				(in r78)
			)
		:effect
			(and
				(not (search_again))
				(not (in r78))
				(in r77)
			)
	)

	(:action move-right-from-r79
		:precondition
			(and 
				(in r79)
			)
		:effect
			(and
				(not (search_again))
				(not (in r79))
				(in r80)
			)
	)


	(:action move-left-from-r79
		:precondition
			(and 
				(in r79)
			)
		:effect
			(and
				(not (search_again))
				(not (in r79))
				(in r78)
			)
	)

	(:action move-right-from-r80
		:precondition
			(and 
				(in r80)
			)
		:effect
			(and
				(not (search_again))
				(not (in r80))
				(in r81)
			)
	)


	(:action move-left-from-r80
		:precondition
			(and 
				(in r80)
			)
		:effect
			(and
				(not (search_again))
				(not (in r80))
				(in r79)
			)
	)

	(:action move-right-from-r81
		:precondition
			(and 
				(in r81)
			)
		:effect
			(and
				(not (search_again))
				(not (in r81))
				(in r82)
			)
	)


	(:action move-left-from-r81
		:precondition
			(and 
				(in r81)
			)
		:effect
			(and
				(not (search_again))
				(not (in r81))
				(in r80)
			)
	)

	(:action move-right-from-r82
		:precondition
			(and 
				(in r82)
			)
		:effect
			(and
				(not (search_again))
				(not (in r82))
				(in r83)
			)
	)


	(:action move-left-from-r82
		:precondition
			(and 
				(in r82)
			)
		:effect
			(and
				(not (search_again))
				(not (in r82))
				(in r81)
			)
	)

	(:action move-right-from-r83
		:precondition
			(and 
				(in r83)
			)
		:effect
			(and
				(not (search_again))
				(not (in r83))
				(in r84)
			)
	)


	(:action move-left-from-r83
		:precondition
			(and 
				(in r83)
			)
		:effect
			(and
				(not (search_again))
				(not (in r83))
				(in r82)
			)
	)

	(:action move-right-from-r84
		:precondition
			(and 
				(in r84)
			)
		:effect
			(and
				(not (search_again))
				(not (in r84))
				(in r85)
			)
	)


	(:action move-left-from-r84
		:precondition
			(and 
				(in r84)
			)
		:effect
			(and
				(not (search_again))
				(not (in r84))
				(in r83)
			)
	)

	(:action move-right-from-r85
		:precondition
			(and 
				(in r85)
			)
		:effect
			(and
				(not (search_again))
				(not (in r85))
				(in r86)
			)
	)


	(:action move-left-from-r85
		:precondition
			(and 
				(in r85)
			)
		:effect
			(and
				(not (search_again))
				(not (in r85))
				(in r84)
			)
	)

	(:action move-right-from-r86
		:precondition
			(and 
				(in r86)
			)
		:effect
			(and
				(not (search_again))
				(not (in r86))
				(in r87)
			)
	)


	(:action move-left-from-r86
		:precondition
			(and 
				(in r86)
			)
		:effect
			(and
				(not (search_again))
				(not (in r86))
				(in r85)
			)
	)

	(:action move-right-from-r87
		:precondition
			(and 
				(in r87)
			)
		:effect
			(and
				(not (search_again))
				(not (in r87))
				(in r88)
			)
	)


	(:action move-left-from-r87
		:precondition
			(and 
				(in r87)
			)
		:effect
			(and
				(not (search_again))
				(not (in r87))
				(in r86)
			)
	)

	(:action move-right-from-r88
		:precondition
			(and 
				(in r88)
			)
		:effect
			(and
				(not (search_again))
				(not (in r88))
				(in r89)
			)
	)


	(:action move-left-from-r88
		:precondition
			(and 
				(in r88)
			)
		:effect
			(and
				(not (search_again))
				(not (in r88))
				(in r87)
			)
	)

	(:action move-right-from-r89
		:precondition
			(and 
				(in r89)
			)
		:effect
			(and
				(not (search_again))
				(not (in r89))
				(in r90)
			)
	)


	(:action move-left-from-r89
		:precondition
			(and 
				(in r89)
			)
		:effect
			(and
				(not (search_again))
				(not (in r89))
				(in r88)
			)
	)

	(:action move-right-from-r90
		:precondition
			(and 
				(in r90)
			)
		:effect
			(and
				(not (search_again))
				(not (in r90))
				(in r91)
			)
	)


	(:action move-left-from-r90
		:precondition
			(and 
				(in r90)
			)
		:effect
			(and
				(not (search_again))
				(not (in r90))
				(in r89)
			)
	)

	(:action move-right-from-r91
		:precondition
			(and 
				(in r91)
			)
		:effect
			(and
				(not (search_again))
				(not (in r91))
				(in r92)
			)
	)


	(:action move-left-from-r91
		:precondition
			(and 
				(in r91)
			)
		:effect
			(and
				(not (search_again))
				(not (in r91))
				(in r90)
			)
	)

	(:action move-right-from-r92
		:precondition
			(and 
				(in r92)
			)
		:effect
			(and
				(not (search_again))
				(not (in r92))
				(in r93)
			)
	)


	(:action move-left-from-r92
		:precondition
			(and 
				(in r92)
			)
		:effect
			(and
				(not (search_again))
				(not (in r92))
				(in r91)
			)
	)

	(:action move-right-from-r93
		:precondition
			(and 
				(in r93)
			)
		:effect
			(and
				(not (search_again))
				(not (in r93))
				(in r94)
			)
	)


	(:action move-left-from-r93
		:precondition
			(and 
				(in r93)
			)
		:effect
			(and
				(not (search_again))
				(not (in r93))
				(in r92)
			)
	)

	(:action move-right-from-r94
		:precondition
			(and 
				(in r94)
			)
		:effect
			(and
				(not (search_again))
				(not (in r94))
				(in r95)
			)
	)


	(:action move-left-from-r94
		:precondition
			(and 
				(in r94)
			)
		:effect
			(and
				(not (search_again))
				(not (in r94))
				(in r93)
			)
	)

	(:action move-right-from-r95
		:precondition
			(and 
				(in r95)
			)
		:effect
			(and
				(not (search_again))
				(not (in r95))
				(in r96)
			)
	)


	(:action move-left-from-r95
		:precondition
			(and 
				(in r95)
			)
		:effect
			(and
				(not (search_again))
				(not (in r95))
				(in r94)
			)
	)

	(:action move-right-from-r96
		:precondition
			(and 
				(in r96)
			)
		:effect
			(and
				(not (search_again))
				(not (in r96))
				(in r97)
			)
	)


	(:action move-left-from-r96
		:precondition
			(and 
				(in r96)
			)
		:effect
			(and
				(not (search_again))
				(not (in r96))
				(in r95)
			)
	)

	(:action move-right-from-r97
		:precondition
			(and 
				(in r97)
			)
		:effect
			(and
				(not (search_again))
				(not (in r97))
				(in r98)
			)
	)


	(:action move-left-from-r97
		:precondition
			(and 
				(in r97)
			)
		:effect
			(and
				(not (search_again))
				(not (in r97))
				(in r96)
			)
	)

	(:action move-right-from-r98
		:precondition
			(and 
				(in r98)
			)
		:effect
			(and
				(not (search_again))
				(not (in r98))
				(in r99)
			)
	)


	(:action move-left-from-r98
		:precondition
			(and 
				(in r98)
			)
		:effect
			(and
				(not (search_again))
				(not (in r98))
				(in r97)
			)
	)

	(:action move-right-from-r99
		:precondition
			(and 
				(in r99)
				(not (searched_in r100))
			)
		:effect
			(and
				(not (search_again))
				(not (in r99))
				(in r100)
				(oneof (not (seen)) (seen))
				(searched_in r100)
			)
	)


	(:action move-left-from-r99
		:precondition
			(and 
				(in r99)
			)
		:effect
			(and
				(not (search_again))
				(not (in r99))
				(in r98)
			)
	)

	(:action move-right-from-r100
		:precondition
			(and 
				(in r100)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r100))
				(in r1)
			)
	)


	(:action move-left-from-r100
		:precondition
			(and 
				(in r100)
				(not (seen))
			)
		:effect
			(and
				(not (search_again))
				(not (in r100))
				(in r99)
			)
	)


	(:action stay
		:precondition
			(f_ok 
			)
		:effect
			(and
			)
	)


	(:action searching_again
		:precondition
			(and 
				(searched_in r50)
				(searched_in r100)
				(not (seen))
			)
		:effect
			(and
				(not (searched_in r50))
				(not (searched_in r100))
				(search_again)
			)
	)

)

