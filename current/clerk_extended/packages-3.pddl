(define (problem packages)
	(:domain packages)
	(:objects )
	(:INIT
		(in_store p1)
		(pkg_at l1)
		(in_store p2)
		(pkg_at l2)
		(in_store p3)
		(pkg_at l3)
		(robot_at desk)
	)
	(:goal 
		(and
			(always
				(eventually
					(active_request) 
				)
			)
			(always 
				(or 
					(not 
						(active_request) 
					) 
					(eventually 
						(or 
							(pkg_served) 
							(pkg_stored) 
						) 
					) 
				) 
			) 
		)
	)
)
