
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Store example for 8 packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Goal formula:
;;
;; A( (pkg_requested) OR (pkg_arrived) --> E (pkg_served) OR E (pkg_stored ) )
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LTL format for ltl2pddl tool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; [](  active_request -> ( <> pkg_stored | <> pkg_served) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain packages)
	(:requirements :strips :typing :equality)
	(:types 
		pkg
		loc
	)
	(:constants
		p1 p2 p3 p4 p5 p6 p7 p8 - pkg
		l1 l2 l3 l4 l5 l6 l7 l8 desk - loc
	)
	(:predicates
		(in_store ?p  - pkg)
		(want ?p  - pkg)
		(active_request)
		(pkg_served)
		(pkg_requested)
		(pkg_arrived)
		(pkg_stored)
		(holding ?p - pkg)
		(pkg_at ?l - loc)
		(robot_at ?l - loc)
	)

	(:action serve-p1
		:precondition
			(and 
				(not (pkg_served))
				(in_store p1)
				(want p1)
				(holding p1)
				(robot_at desk)
			)
		:effect
			(and
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p1))
				(not (want p1))
				(not (holding p1))
			)
	)


	(:action pickup-p1
		:precondition
			(and 
				(not (pkg_served))
				(in_store p1)
				(want p1)
				(robot_at l1)
			)
		:effect
			(and
				(not (pkg_at l1))
				(holding p1)
			)
	)


	(:action putdown-p1
		:precondition
			(and 
				(pkg_arrived)
				(holding p1)
				(robot_at l1)
			)
		:effect
			(and
				(in_store p1)
				(pkg_at l1)
				(not (holding p1))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
			)
	)


	(:action restock-p1
		:precondition
			(and 
				(not (pkg_served))
				(not (in_store p1))
				(want p1)
			)
		:effect
			(and
				(in_store p1)
			)
	)


	(:action serve-p2
		:precondition
			(and 
				(not (pkg_served))
				(in_store p2)
				(want p2)
				(holding p2)
				(robot_at desk)
			)
		:effect
			(and
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p2))
				(not (want p2))
				(not (holding p2))
			)
	)


	(:action pickup-p2
		:precondition
			(and 
				(not (pkg_served))
				(in_store p2)
				(want p2)
				(robot_at l2)
			)
		:effect
			(and
				(not (pkg_at l2))
				(holding p2)
			)
	)


	(:action putdown-p2
		:precondition
			(and 
				(pkg_arrived)
				(holding p2)
				(robot_at l2)
			)
		:effect
			(and
				(in_store p2)
				(pkg_at l2)
				(not (holding p2))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
			)
	)


	(:action restock-p2
		:precondition
			(and 
				(not (pkg_served))
				(not (in_store p2))
				(want p2)
			)
		:effect
			(and
				(in_store p2)
			)
	)


	(:action serve-p3
		:precondition
			(and 
				(not (pkg_served))
				(in_store p3)
				(want p3)
				(holding p3)
				(robot_at desk)
			)
		:effect
			(and
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p3))
				(not (want p3))
				(not (holding p3))
			)
	)


	(:action pickup-p3
		:precondition
			(and 
				(not (pkg_served))
				(in_store p3)
				(want p3)
				(robot_at l3)
			)
		:effect
			(and
				(not (pkg_at l3))
				(holding p3)
			)
	)


	(:action putdown-p3
		:precondition
			(and 
				(pkg_arrived)
				(holding p3)
				(robot_at l3)
			)
		:effect
			(and
				(in_store p3)
				(pkg_at l3)
				(not (holding p3))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
			)
	)


	(:action restock-p3
		:precondition
			(and 
				(not (pkg_served))
				(not (in_store p3))
				(want p3)
			)
		:effect
			(and
				(in_store p3)
			)
	)


	(:action serve-p4
		:precondition
			(and 
				(not (pkg_served))
				(in_store p4)
				(want p4)
				(holding p4)
				(robot_at desk)
			)
		:effect
			(and
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p4))
				(not (want p4))
				(not (holding p4))
			)
	)


	(:action pickup-p4
		:precondition
			(and 
				(not (pkg_served))
				(in_store p4)
				(want p4)
				(robot_at l4)
			)
		:effect
			(and
				(not (pkg_at l4))
				(holding p4)
			)
	)


	(:action putdown-p4
		:precondition
			(and 
				(pkg_arrived)
				(holding p4)
				(robot_at l4)
			)
		:effect
			(and
				(in_store p4)
				(pkg_at l4)
				(not (holding p4))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
			)
	)


	(:action restock-p4
		:precondition
			(and 
				(not (pkg_served))
				(not (in_store p4))
				(want p4)
			)
		:effect
			(and
				(in_store p4)
			)
	)


	(:action serve-p5
		:precondition
			(and 
				(not (pkg_served))
				(in_store p5)
				(want p5)
				(holding p5)
				(robot_at desk)
			)
		:effect
			(and
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p5))
				(not (want p5))
				(not (holding p5))
			)
	)


	(:action pickup-p5
		:precondition
			(and 
				(not (pkg_served))
				(in_store p5)
				(want p5)
				(robot_at l5)
			)
		:effect
			(and
				(not (pkg_at l5))
				(holding p5)
			)
	)


	(:action putdown-p5
		:precondition
			(and 
				(pkg_arrived)
				(holding p5)
				(robot_at l5)
			)
		:effect
			(and
				(in_store p5)
				(pkg_at l5)
				(not (holding p5))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
			)
	)


	(:action restock-p5
		:precondition
			(and 
				(not (pkg_served))
				(not (in_store p5))
				(want p5)
			)
		:effect
			(and
				(in_store p5)
			)
	)


	(:action serve-p6
		:precondition
			(and 
				(not (pkg_served))
				(in_store p6)
				(want p6)
				(holding p6)
				(robot_at desk)
			)
		:effect
			(and
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p6))
				(not (want p6))
				(not (holding p6))
			)
	)


	(:action pickup-p6
		:precondition
			(and 
				(not (pkg_served))
				(in_store p6)
				(want p6)
				(robot_at l6)
			)
		:effect
			(and
				(not (pkg_at l6))
				(holding p6)
			)
	)


	(:action putdown-p6
		:precondition
			(and 
				(pkg_arrived)
				(holding p6)
				(robot_at l6)
			)
		:effect
			(and
				(in_store p6)
				(pkg_at l6)
				(not (holding p6))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
			)
	)


	(:action restock-p6
		:precondition
			(and 
				(not (pkg_served))
				(not (in_store p6))
				(want p6)
			)
		:effect
			(and
				(in_store p6)
			)
	)


	(:action serve-p7
		:precondition
			(and 
				(not (pkg_served))
				(in_store p7)
				(want p7)
				(holding p7)
				(robot_at desk)
			)
		:effect
			(and
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p7))
				(not (want p7))
				(not (holding p7))
			)
	)


	(:action pickup-p7
		:precondition
			(and 
				(not (pkg_served))
				(in_store p7)
				(want p7)
				(robot_at l7)
			)
		:effect
			(and
				(not (pkg_at l7))
				(holding p7)
			)
	)


	(:action putdown-p7
		:precondition
			(and 
				(pkg_arrived)
				(holding p7)
				(robot_at l7)
			)
		:effect
			(and
				(in_store p7)
				(pkg_at l7)
				(not (holding p7))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
			)
	)


	(:action restock-p7
		:precondition
			(and 
				(not (pkg_served))
				(not (in_store p7))
				(want p7)
			)
		:effect
			(and
				(in_store p7)
			)
	)


	(:action serve-p8
		:precondition
			(and 
				(not (pkg_served))
				(in_store p8)
				(want p8)
				(holding p8)
				(robot_at desk)
			)
		:effect
			(and
				(not (pkg_requested))
				(not (active_request))
				(pkg_served)
				(not (in_store p8))
				(not (want p8))
				(not (holding p8))
			)
	)


	(:action pickup-p8
		:precondition
			(and 
				(not (pkg_served))
				(in_store p8)
				(want p8)
				(robot_at l8)
			)
		:effect
			(and
				(not (pkg_at l8))
				(holding p8)
			)
	)


	(:action putdown-p8
		:precondition
			(and 
				(pkg_arrived)
				(holding p8)
				(robot_at l8)
			)
		:effect
			(and
				(in_store p8)
				(pkg_at l8)
				(not (holding p8))
				(pkg_stored)
				(not (pkg_arrived))
				(not (active_request))
			)
	)


	(:action restock-p8
		:precondition
			(and 
				(not (pkg_served))
				(not (in_store p8))
				(want p8)
			)
		:effect
			(and
				(in_store p8)
			)
	)


	(:action move1_2
		:precondition
			(and 
				(robot_at l1)
			)
		:effect
			(and
				(robot_at l2)
				(not (robot_at l1))
			)
	)


	(:action move2_1
		:precondition
			(and 
				(robot_at l2)
			)
		:effect
			(and
				(robot_at l1)
				(not (robot_at l2))
			)
	)


	(:action move2_3
		:precondition
			(and 
				(robot_at l2)
			)
		:effect
			(and
				(robot_at l3)
				(not (robot_at l2))
			)
	)


	(:action move3_2
		:precondition
			(and 
				(robot_at l3)
			)
		:effect
			(and
				(robot_at l2)
				(not (robot_at l3))
			)
	)


	(:action move3_4
		:precondition
			(and 
				(robot_at l3)
			)
		:effect
			(and
				(robot_at l4)
				(not (robot_at l3))
			)
	)


	(:action move4_3
		:precondition
			(and 
				(robot_at l4)
			)
		:effect
			(and
				(robot_at l3)
				(not (robot_at l4))
			)
	)


	(:action move4_5
		:precondition
			(and 
				(robot_at l4)
			)
		:effect
			(and
				(robot_at l5)
				(not (robot_at l4))
			)
	)


	(:action move5_4
		:precondition
			(and 
				(robot_at l5)
			)
		:effect
			(and
				(robot_at l4)
				(not (robot_at l5))
			)
	)


	(:action move5_6
		:precondition
			(and 
				(robot_at l5)
			)
		:effect
			(and
				(robot_at l6)
				(not (robot_at l5))
			)
	)


	(:action move6_5
		:precondition
			(and 
				(robot_at l6)
			)
		:effect
			(and
				(robot_at l5)
				(not (robot_at l6))
			)
	)


	(:action move6_7
		:precondition
			(and 
				(robot_at l6)
			)
		:effect
			(and
				(robot_at l7)
				(not (robot_at l6))
			)
	)


	(:action move7_6
		:precondition
			(and 
				(robot_at l7)
			)
		:effect
			(and
				(robot_at l6)
				(not (robot_at l7))
			)
	)


	(:action move7_8
		:precondition
			(and 
				(robot_at l7)
			)
		:effect
			(and
				(robot_at l8)
				(not (robot_at l7))
			)
	)


	(:action move8_7
		:precondition
			(and 
				(robot_at l8)
			)
		:effect
			(and
				(robot_at l7)
				(not (robot_at l8))
			)
	)


	(:action movedesk_l1
		:precondition
			(and 
				(robot_at desk)
			)
		:effect
			(and
				(robot_at l1)
				(not (robot_at desk))
			)
	)


	(:action movel1_desk
		:precondition
			(and 
				(robot_at l1)
			)
		:effect
			(and
				(robot_at desk)
				(not (robot_at l1))
			)
	)


	(:action request
		:precondition
			(and 
				(robot_at desk)
				(not (active_request))
			)
		:effect
			(and
				(not (pkg_served))
				(not (pkg_stored))
				(active_request)
				(oneof
					(and
						(pkg_requested)
						(want p1)
					)
					(and
						(pkg_requested)
						(want p2)
					)
					(and
						(pkg_requested)
						(want p3)
					)
					(and
						(pkg_requested)
						(want p4)
					)
					(and
						(pkg_requested)
						(want p5)
					)
					(and
						(pkg_requested)
						(want p6)
					)
					(and
						(pkg_requested)
						(want p7)
					)
					(and
						(pkg_requested)
						(want p8)
					)
					(and
						(pkg_arrived)
						(holding p1)
					)
					(and
						(pkg_arrived)
						(holding p2)
					)
					(and
						(pkg_arrived)
						(holding p3)
					)
					(and
						(pkg_arrived)
						(holding p4)
					)
					(and
						(pkg_arrived)
						(holding p5)
					)
					(and
						(pkg_arrived)
						(holding p6)
					)
					(and
						(pkg_arrived)
						(holding p7)
					)
					(and
						(pkg_arrived)
						(holding p8)
					)
				)
			)
	)


)

