(define (problem packages)
	(:domain packages)
	(:objects )
	(:INIT
		(in_store p1)
		(pkg_at l1)
		(in_store p2)
		(pkg_at l2)
		(in_store p3)
		(pkg_at l3)
		(in_store p4)
		(pkg_at l4)
		(in_store p5)
		(pkg_at l5)
		(in_store p6)
		(pkg_at l6)
		(in_store p7)
		(pkg_at l7)
		(in_store p8)
		(pkg_at l8)
		(in_store p9)
		(pkg_at l9)
		(robot_at desk)
	)
	(:goal 
		(and
			(always
				(eventually
					(active_request) 
				)
			)
			(always 
				(or 
					(not 
						(active_request) 
					) 
					(eventually 
						(or 
							(pkg_served) 
							(pkg_stored) 
						) 
					) 
				) 
			) 
		)
	)
)
